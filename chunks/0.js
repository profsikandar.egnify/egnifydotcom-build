require("source-map-support").install();
exports.ids = [0];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Footer/Footer.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "footer {\n  background: #fff;\n  padding-top: 40px;\n  padding-top: 2.5rem;\n  padding-bottom: 40px;\n  padding-bottom: 2.5rem;\n}\n\n.Footer-addGray-3ecfx {\n  background: #f7f7f7;\n}\n\n.Footer-logo-1W3uT {\n  height: 56px;\n  height: 3.5rem;\n  width: 113.984px;\n  width: 7.124rem;\n  margin-left: 64px;\n  margin-bottom: 24px;\n}\n\n.Footer-mobile_logo_wrapper-1VMhP {\n  display: none;\n  visibility: hidden;\n  max-width: 124px;\n  max-height: 60px;\n  margin: auto;\n}\n\n.Footer-linksSectionTitle-DmwjI {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 24px;\n  color: #25282b;\n  margin-bottom: 4px;\n  text-transform: capitalize;\n}\n\n.Footer-importantLinkContainer-ArOTF {\n  margin-bottom: 0;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  padding: 0 64px;\n  // flex-wrap: wrap;\n}\n\n.Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n    display: -ms-flexbox;\n    display: flex;\n    padding-right: 24px;\n    -ms-flex-order: 2;\n        order: 2;\n  }\n\n.Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE section {\n      width: 100%;\n    }\n\n.Footer-addressDiv-YTXxe {\n  left: 0;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n    width: 192px;\n  }\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO {\n      width: 100%;\n      font-weight: normal;\n      font-style: normal;\n      font-stretch: normal;\n      letter-spacing: normal;\n      color: #5f6368;\n    }\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO p {\n        font-size: 14px;\n        line-height: 24px;\n        margin: 0;\n      }\n\n.Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n    width: 1px;\n    height: 100%;\n    background-color: #25282b;\n    opacity: 0.1;\n    -webkit-transform: rotate(90);\n        -ms-transform: rotate(90);\n            transform: rotate(90);\n    margin: 0 32px;\n  }\n\n.Footer-importantLink-3aQPB {\n  margin: 6px 0;\n}\n\n.Footer-importantLink-3aQPB a {\n    font-size: 14px;\n    line-height: 1.5;\n    text-transform: capitalize;\n  }\n\n.Footer-importantLink-3aQPB a:hover {\n    text-decoration: underline;\n  }\n\n.Footer-social-l2arY {\n  padding-left: 32px;\n  border-left: 1px solid rgba(37, 40, 43, 0.1);\n  margin-left: 0;\n  -ms-flex-order: 3;\n      order: 3;\n}\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-linksSectionTitle1-34zFu {\n      font-size: 16px;\n      margin-bottom: 16px;\n      line-height: 24px;\n      color: #000;\n      font-weight: 600;\n    }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n      display: grid;\n      grid-template-columns: repeat(3, 1fr);\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n    }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv {\n        float: left;\n        margin-right: 24px;\n        margin-bottom: 12px;\n        width: 32px;\n        height: 32px;\n      }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv i {\n          font-size: 32px;\n          font-size: 2rem;\n          color: #5f6368;\n        }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv:last-child {\n        margin-right: 0;\n      }\n\n@media only screen and (max-width: 1290px) {\n  .Footer-logo-1W3uT {\n    margin-left: 32px;\n  }\n\n  .Footer-importantLinkContainer-ArOTF {\n    padding: 0 32px;\n  }\n\n    .Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n      padding-right: 12px;\n    }\n    .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n      width: 172px;\n    }\n\n    .Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n      margin: 0 12px;\n    }\n\n  .Footer-social-l2arY {\n    padding-left: 12px;\n    -ms-flex: 1 1;\n        flex: 1 1;\n  }\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        display: grid;\n        grid-template-columns: repeat(4, 1fr);\n        margin-left: 0;\n        margin-right: 0;\n      }\n\n        .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv {\n          margin-right: 12px;\n          margin-bottom: 8px;\n        }\n}\n\n@media only screen and (max-width: 1110px) {\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        display: grid;\n        grid-template-columns: repeat(2, 1fr);\n      }\n}\n\n@media only screen and (max-width: 990px) {\n  .Footer-logo-1W3uT {\n    display: none;\n  }\n\n  footer {\n    padding: 0;\n  }\n\n  .Footer-mobile_logo_wrapper-1VMhP {\n    margin-top: 48px;\n    width: 7.75rem;\n    height: 3.75rem;\n    display: -ms-flexbox;\n    display: flex;\n    visibility: visible;\n    padding-left: 0;\n    max-width: none;\n    max-height: none;\n  }\n\n  .Footer-mobile_logo-jUB-e {\n    width: 100%;\n    height: 100%;\n  }\n\n  .Footer-linksSectionTitle-DmwjI {\n    margin-bottom: 0.5rem;\n    font-size: 14px;\n    font-weight: 600;\n    text-align: center;\n    margin-top: 42px;\n    width: 100%;\n  }\n\n  .Footer-importantLinkContainer-ArOTF {\n    display: block;\n    padding: 32px 20px;\n  }\n\n    .Footer-importantLinkContainer-ArOTF section {\n      margin: -48px auto 0 auto;\n      // background-color: #fff !important;\n    }\n\n    .Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n      margin: auto;\n      display: block;\n      padding-right: 0;\n    }\n\n  .Footer-social-l2arY {\n    padding-left: 0;\n    border-left: none;\n  }\n\n    .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ {\n      width: 200px;\n      margin: auto;\n      margin-top: 1.5rem !important;\n    }\n\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        grid-template-columns: repeat(5, 1fr);\n      }\n\n  .Footer-socialLink-1QvRv i {\n    font-size: 2.5rem;\n  }\n\n  .Footer-addressDiv-YTXxe {\n    width: 100%;\n    margin: auto;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 16px;\n  }\n\n    .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n      width: 100%;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      -ms-flex-align: center;\n          align-items: center;\n      margin: 0 auto 24px auto;\n      max-width: 280px;\n    }\n\n      .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-linksSectionTitle-DmwjI {\n        margin-top: 0;\n      }\n\n      .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO {\n        width: 100%;\n        text-align: center;\n        font-size: 13px;\n        line-height: 1.71;\n        opacity: 0.6;\n      }\n\n    .Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n      display: none;\n    }\n\n  .Footer-importantLink-3aQPB {\n    text-align: center;\n    margin: 1rem 0;\n  }\n\n    .Footer-importantLink-3aQPB a {\n      font-weight: normal;\n    }\n\n  .Footer-linksSectionTitle1-34zFu {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Footer/Footer.scss"],"names":[],"mappings":"AAAA;EACE,iBAAiB;EACjB,kBAAkB;EAClB,oBAAoB;EACpB,qBAAqB;EACrB,uBAAuB;CACxB;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,aAAa;EACb,eAAe;EACf,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,cAAc;EACd,mBAAmB;EACnB,iBAAiB;EACjB,iBAAiB;EACjB,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,eAAe;EACf,mBAAmB;EACnB,2BAA2B;CAC5B;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,oBAAoB;IACpB,kBAAkB;QACd,SAAS;GACd;;AAEH;MACM,YAAY;KACb;;AAEL;EACE,QAAQ;EACR,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,kBAAkB;MACd,SAAS;CACd;;AAED;IACI,aAAa;GACd;;AAEH;MACM,YAAY;MACZ,oBAAoB;MACpB,mBAAmB;MACnB,qBAAqB;MACrB,uBAAuB;MACvB,eAAe;KAChB;;AAEL;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,UAAU;OACX;;AAEP;IACI,WAAW;IACX,aAAa;IACb,0BAA0B;IAC1B,aAAa;IACb,8BAA8B;QAC1B,0BAA0B;YACtB,sBAAsB;IAC9B,eAAe;GAChB;;AAEH;EACE,cAAc;CACf;;AAED;IACI,gBAAgB;IAChB,iBAAiB;IACjB,2BAA2B;GAC5B;;AAEH;IACI,2BAA2B;GAC5B;;AAEH;EACE,mBAAmB;EACnB,6CAA6C;EAC7C,eAAe;EACf,kBAAkB;MACd,SAAS;CACd;;AAED;MACM,gBAAgB;MAChB,oBAAoB;MACpB,kBAAkB;MAClB,YAAY;MACZ,iBAAiB;KAClB;;AAEL;MACM,cAAc;MACd,sCAAsC;MACtC,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;KACpB;;AAEL;QACQ,YAAY;QACZ,mBAAmB;QACnB,oBAAoB;QACpB,YAAY;QACZ,aAAa;OACd;;AAEP;UACU,YAAY;UACZ,aAAa;UACb,uBAAuB;aACpB,oBAAoB;SACxB;;AAET;UACU,gBAAgB;UAChB,gBAAgB;UAChB,eAAe;SAChB;;AAET;QACQ,gBAAgB;OACjB;;AAEP;EACE;IACE,kBAAkB;GACnB;;EAED;IACE,gBAAgB;GACjB;;IAEC;MACE,oBAAoB;KACrB;IACD;MACE,aAAa;KACd;;IAED;MACE,eAAe;KAChB;;EAEH;IACE,mBAAmB;IACnB,cAAc;QACV,UAAU;GACf;MACG;QACE,cAAc;QACd,sCAAsC;QACtC,eAAe;QACf,gBAAgB;OACjB;;QAEC;UACE,mBAAmB;UACnB,mBAAmB;SACpB;CACR;;AAED;MACM;QACE,cAAc;QACd,sCAAsC;OACvC;CACN;;AAED;EACE;IACE,cAAc;GACf;;EAED;IACE,WAAW;GACZ;;EAED;IACE,iBAAiB;IACjB,eAAe;IACf,gBAAgB;IAChB,qBAAqB;IACrB,cAAc;IACd,oBAAoB;IACpB,gBAAgB;IAChB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,sBAAsB;IACtB,gBAAgB;IAChB,iBAAiB;IACjB,mBAAmB;IACnB,iBAAiB;IACjB,YAAY;GACb;;EAED;IACE,eAAe;IACf,mBAAmB;GACpB;;IAEC;MACE,0BAA0B;MAC1B,qCAAqC;KACtC;;IAED;MACE,aAAa;MACb,eAAe;MACf,iBAAiB;KAClB;;EAEH;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;IAEC;MACE,aAAa;MACb,aAAa;MACb,8BAA8B;KAC/B;;MAEC;QACE,sCAAsC;OACvC;;EAEL;IACE,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,aAAa;IACb,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;GAClB;;IAEC;MACE,YAAY;MACZ,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,sBAAsB;UAClB,wBAAwB;MAC5B,uBAAuB;UACnB,oBAAoB;MACxB,yBAAyB;MACzB,iBAAiB;KAClB;;MAEC;QACE,cAAc;OACf;;MAED;QACE,YAAY;QACZ,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;QAClB,aAAa;OACd;;IAEH;MACE,cAAc;KACf;;EAEH;IACE,mBAAmB;IACnB,eAAe;GAChB;;IAEC;MACE,oBAAoB;KACrB;;EAEH;IACE,cAAc;GACf;CACF","file":"Footer.scss","sourcesContent":["footer {\n  background: #fff;\n  padding-top: 40px;\n  padding-top: 2.5rem;\n  padding-bottom: 40px;\n  padding-bottom: 2.5rem;\n}\n\n.addGray {\n  background: #f7f7f7;\n}\n\n.logo {\n  height: 56px;\n  height: 3.5rem;\n  width: 113.984px;\n  width: 7.124rem;\n  margin-left: 64px;\n  margin-bottom: 24px;\n}\n\n.mobile_logo_wrapper {\n  display: none;\n  visibility: hidden;\n  max-width: 124px;\n  max-height: 60px;\n  margin: auto;\n}\n\n.linksSectionTitle {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 24px;\n  color: #25282b;\n  margin-bottom: 4px;\n  text-transform: capitalize;\n}\n\n.importantLinkContainer {\n  margin-bottom: 0;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  padding: 0 64px;\n  // flex-wrap: wrap;\n}\n\n.importantLinkContainer .sectionDiv {\n    display: -ms-flexbox;\n    display: flex;\n    padding-right: 24px;\n    -ms-flex-order: 2;\n        order: 2;\n  }\n\n.importantLinkContainer .sectionDiv section {\n      width: 100%;\n    }\n\n.addressDiv {\n  left: 0;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n.addressDiv .addressContainer {\n    width: 192px;\n  }\n\n.addressDiv .addressContainer .address {\n      width: 100%;\n      font-weight: normal;\n      font-style: normal;\n      font-stretch: normal;\n      letter-spacing: normal;\n      color: #5f6368;\n    }\n\n.addressDiv .addressContainer .address p {\n        font-size: 14px;\n        line-height: 24px;\n        margin: 0;\n      }\n\n.addressDiv .verticalLine {\n    width: 1px;\n    height: 100%;\n    background-color: #25282b;\n    opacity: 0.1;\n    -webkit-transform: rotate(90);\n        -ms-transform: rotate(90);\n            transform: rotate(90);\n    margin: 0 32px;\n  }\n\n.importantLink {\n  margin: 6px 0;\n}\n\n.importantLink a {\n    font-size: 14px;\n    line-height: 1.5;\n    text-transform: capitalize;\n  }\n\n.importantLink a:hover {\n    text-decoration: underline;\n  }\n\n.social {\n  padding-left: 32px;\n  border-left: 1px solid rgba(37, 40, 43, 0.1);\n  margin-left: 0;\n  -ms-flex-order: 3;\n      order: 3;\n}\n\n.social .socialLinkContainer .linksSectionTitle1 {\n      font-size: 16px;\n      margin-bottom: 16px;\n      line-height: 24px;\n      color: #000;\n      font-weight: 600;\n    }\n\n.social .socialLinkContainer .socialLinksWrapper {\n      display: grid;\n      grid-template-columns: repeat(3, 1fr);\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n    }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink {\n        float: left;\n        margin-right: 24px;\n        margin-bottom: 12px;\n        width: 32px;\n        height: 32px;\n      }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink i {\n          font-size: 32px;\n          font-size: 2rem;\n          color: #5f6368;\n        }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink:last-child {\n        margin-right: 0;\n      }\n\n@media only screen and (max-width: 1290px) {\n  .logo {\n    margin-left: 32px;\n  }\n\n  .importantLinkContainer {\n    padding: 0 32px;\n  }\n\n    .importantLinkContainer .sectionDiv {\n      padding-right: 12px;\n    }\n    .addressDiv .addressContainer {\n      width: 172px;\n    }\n\n    .addressDiv .verticalLine {\n      margin: 0 12px;\n    }\n\n  .social {\n    padding-left: 12px;\n    -ms-flex: 1 1;\n        flex: 1 1;\n  }\n      .social .socialLinkContainer .socialLinksWrapper {\n        display: grid;\n        grid-template-columns: repeat(4, 1fr);\n        margin-left: 0;\n        margin-right: 0;\n      }\n\n        .social .socialLinkContainer .socialLinksWrapper .socialLink {\n          margin-right: 12px;\n          margin-bottom: 8px;\n        }\n}\n\n@media only screen and (max-width: 1110px) {\n      .social .socialLinkContainer .socialLinksWrapper {\n        display: grid;\n        grid-template-columns: repeat(2, 1fr);\n      }\n}\n\n@media only screen and (max-width: 990px) {\n  .logo {\n    display: none;\n  }\n\n  footer {\n    padding: 0;\n  }\n\n  .mobile_logo_wrapper {\n    margin-top: 48px;\n    width: 7.75rem;\n    height: 3.75rem;\n    display: -ms-flexbox;\n    display: flex;\n    visibility: visible;\n    padding-left: 0;\n    max-width: none;\n    max-height: none;\n  }\n\n  .mobile_logo {\n    width: 100%;\n    height: 100%;\n  }\n\n  .linksSectionTitle {\n    margin-bottom: 0.5rem;\n    font-size: 14px;\n    font-weight: 600;\n    text-align: center;\n    margin-top: 42px;\n    width: 100%;\n  }\n\n  .importantLinkContainer {\n    display: block;\n    padding: 32px 20px;\n  }\n\n    .importantLinkContainer section {\n      margin: -48px auto 0 auto;\n      // background-color: #fff !important;\n    }\n\n    .importantLinkContainer .sectionDiv {\n      margin: auto;\n      display: block;\n      padding-right: 0;\n    }\n\n  .social {\n    padding-left: 0;\n    border-left: none;\n  }\n\n    .social .socialLinkContainer {\n      width: 200px;\n      margin: auto;\n      margin-top: 1.5rem !important;\n    }\n\n      .social .socialLinkContainer .socialLinksWrapper {\n        grid-template-columns: repeat(5, 1fr);\n      }\n\n  .socialLink i {\n    font-size: 2.5rem;\n  }\n\n  .addressDiv {\n    width: 100%;\n    margin: auto;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 16px;\n  }\n\n    .addressDiv .addressContainer {\n      width: 100%;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      -ms-flex-align: center;\n          align-items: center;\n      margin: 0 auto 24px auto;\n      max-width: 280px;\n    }\n\n      .addressDiv .addressContainer .linksSectionTitle {\n        margin-top: 0;\n      }\n\n      .addressDiv .addressContainer .address {\n        width: 100%;\n        text-align: center;\n        font-size: 13px;\n        line-height: 1.71;\n        opacity: 0.6;\n      }\n\n    .addressDiv .verticalLine {\n      display: none;\n    }\n\n  .importantLink {\n    text-align: center;\n    margin: 1rem 0;\n  }\n\n    .importantLink a {\n      font-weight: normal;\n    }\n\n  .linksSectionTitle1 {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"addGray": "Footer-addGray-3ecfx",
	"logo": "Footer-logo-1W3uT",
	"mobile_logo_wrapper": "Footer-mobile_logo_wrapper-1VMhP",
	"linksSectionTitle": "Footer-linksSectionTitle-DmwjI",
	"importantLinkContainer": "Footer-importantLinkContainer-ArOTF",
	"sectionDiv": "Footer-sectionDiv-1JpSE",
	"addressDiv": "Footer-addressDiv-YTXxe",
	"addressContainer": "Footer-addressContainer-2ZY-s",
	"address": "Footer-address-rFyBO",
	"verticalLine": "Footer-verticalLine-A1fI5",
	"importantLink": "Footer-importantLink-3aQPB",
	"social": "Footer-social-l2arY",
	"socialLinkContainer": "Footer-socialLinkContainer-1Bjq_",
	"linksSectionTitle1": "Footer-linksSectionTitle1-34zFu",
	"socialLinksWrapper": "Footer-socialLinksWrapper-21o9E",
	"socialLink": "Footer-socialLink-1QvRv",
	"mobile_logo": "Footer-mobile_logo-jUB-e"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/HeaderV2/HeaderV2.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/* .root {\n  height: 100%;\n  width: 100%;\n  background-color: #fff;\n  box-sizing: border-box;\n  overflow-x: hidden;\n} */\n\n/* .bodywrapper {\n  position: relative;\n  padding-top: 70px;\n} */\n\n/*\n.navbar{\n  width: 100%;\n  background-color: #fff;\n  min-height: 75px;\n  padding: 2rem 4rem;\n  justify-content: space-between;\n\n} */\n\n/* .navbar-collapse {\n  min-height: 100vh;\n  flex-grow: 0;\n}\n.dropdown-toggle::after {\n  display:none\n}\n.dropdown-menu {\n  border: none;\n\n}\n.navbar-toggler {\n  border: none;\n}\n.navbar-toggler:focus {\n  box-shadow: none;\n}\n@media only screen and (min-width : 992px) {\n  .navbar-expand-lg {\n    justify-content: space-between;\n    padding:1rem 4rem;\n  }\n  .navbar-collapse {\n    min-height: auto;\n  }\n} */\n\n/* .headerRoot {\n  height: 88px;\n  width: 100%;\n  padding: 27px 5%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.left {\n  height: 51px;\n  width: 105px;\n  display: flex;\n}\n\n.right {\n  height: 100%;\n  margin-right: 14px;\n}\n\n.requestDemoContainer {\n  width: 83px;\n  height: 40px;\n  border-radius: 4px;\n  background-color: #ffebf0;\n  padding: 8px 16px;\n  cursor: pointer;\n  float: right;\n} */\n\n/* .requestDemo {\n//   font-weight: 600;\n//   font-size: 16px;\n//   line-height: 1.5;\n//   color: #f36;\n// } */\n\n.HeaderV2-scheduleDemo-h_CeP {\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 1.5;\n  color: #f36;\n  border: 1px solid #f36;\n  padding: 8px 12px;\n  cursor: pointer;\n  border-radius: 4px;\n  text-transform: uppercase;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-right: 12px;\n  background: none;\n  -ms-flex-order: 0;\n      order: 0;\n}\n\n.HeaderV2-scheduleDemo-h_CeP:hover {\n  background-color: rgb(255, 51, 102);\n  color: #fff;\n  -webkit-transition: 0.5s ease-in-out;\n  -o-transition: 0.5s ease-in-out;\n  transition: 0.5s ease-in-out;\n}\n\n.HeaderV2-navbarWrapper-3MC6c {\n  width: 100%;\n  top: 0%;\n  left: 0%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  background-color: #fff;\n  opacity: 1;\n  z-index: 2;\n  margin: auto;\n  position: fixed;\n  -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n  padding: 16px;\n  padding: 1rem;\n}\n\n.HeaderV2-navbar-1ZdUK {\n  width: 100%;\n  max-width: 1700px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-item-align: center;\n      align-self: center;\n}\n\n/* .featureitem img {\n  width: 20px;\n  height: 20px;\n} */\n\n/* .featureheading img {\n  width: 24px;\n  height: 24px;\n} */\n\n.HeaderV2-menubtn-2MJna {\n  color: #000;\n  font-size: 22.4px;\n  font-size: 1.4rem;\n  margin-left: 0;\n  -ms-flex-order: 2;\n      order: 2;\n}\n\n.HeaderV2-menubtn-2MJna img {\n    width: 20px;\n    height: 20px;\n  }\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI {\n  height: 40px;\n  height: 2.5rem;\n  width: 80px;\n  width: 5rem;\n  cursor: pointer;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI img {\n    width: 100%;\n    height: 100%;\n  }\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH {\n  display: block;\n  position: fixed;\n  left: -100%;\n  top: 69px;\n  width: 100%;\n  height: 100vh;\n  list-style: none;\n  padding: 24px 0;\n  padding: 1.5rem 0;\n  padding-left: 16px;\n  padding-left: 1rem;\n  padding-right: 16px;\n  padding-right: 1rem;\n  -webkit-transition: 0.4s;\n  -o-transition: 0.4s;\n  transition: 0.4s;\n  background-color: #fff;\n  overflow: auto;\n  z-index: 1;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH.HeaderV2-active-vZX84 {\n  left: 0%;\n}\n\n/* .navbar .menu li .featuresdropdown {\n  display: none;\n  flex-direction: column;\n} */\n\n/* .navbar .menu li .featuresdropdown.activefeatures {\n  display: flex;\n} */\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D {\n  display: none;\n  margin-left: 10px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D.HeaderV2-activedropdowns-_cvw6 {\n  display: block;\n  opacity: 1;\n  visibility: visible;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  cursor: pointer;\n  position: relative;\n  -webkit-transition: all 0.4s;\n  -o-transition: all 0.4s;\n  transition: all 0.4s;\n  margin: 0 12px 24px;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.HeaderV2-btngroup-1738m {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* featureheading span {\n  margin-left: 8px;\n  font-size: 16px;\n  font-weight: 600;\n} */\n\n/* .featureitem span {\n  margin-left: 8px;\n  color: #25282b;\n  line-height: 20px;\n  font-size: 14px;\n} */\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n  color: #000;\n  opacity: 0.6;\n  font-size: 14px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span > img {\n  margin-top: 3px;\n  margin-left: 5px;\n  border-bottom: none;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > a {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul {\n  width: 100%;\n  list-style: none;\n  display: none;\n  opacity: 0;\n  z-index: 2;\n}\n\n.HeaderV2-activedropdowns-_cvw6 {\n  display: block;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li,\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a {\n  width: 100%;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a {\n  color: #000;\n  opacity: 0.6;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 0;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a img {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:nth-child(1) a {\n  padding: 24px 0 12px 0;\n}\n\n.HeaderV2-menubtn-2MJna.HeaderV2-menuactive-1ufv2 {\n  z-index: 3;\n}\n\n.HeaderV2-login-31QGs {\n  padding: 8px 12px;\n  font-weight: 600;\n  text-transform: uppercase;\n  border-radius: 4px;\n  margin-right: 12px;\n  line-height: 20px;\n  background: #ffebf0;\n  color: #f36;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n/* .featurewrapper {\n  display: flex;\n  flex-direction: row;\n  align-items: flex-end;\n  justify-content: space-between;\n  padding: 24px 0;\n  border-bottom: 1px solid rgba(37, 40, 43, 0.1);\n\n  .feature {\n    height: 100%;\n    width: 60%;\n\n    .featureitem {\n      display: flex;\n      align-items: center;\n      margin: 24px 0;\n    }\n\n    .featureitem:last-child {\n      margin-bottom: 0;\n    }\n  }\n} */\n\n/* .featurewrapper:last-child {\n  border-bottom: none;\n} */\n\n/* .viewmorewrapper {\n  height: 27px;\n  position: relative;\n  width: 95px;\n} */\n\n/* .featureheading {\n  display: flex;\n  align-items: center;\n} */\n\n/* .viewmore {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: none;\n  color: #0076ff;\n  padding: 0;\n  text-transform: none;\n  font-size: 14px;\n  line-height: 20px;\n  font-weight: 400;\n  text-align: center;\n} */\n\n/* .search-icon {\n  font-size: 1.4rem;\n   margin-left: 6rem;\n  margin-right: 15px;\n   color: #fff;\n   justify-content: flex-end;\n\n.btngroup {\n  margin-left: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.login {\n  border: none;\n}\n\n@media only screen and (max-width: 1200px) {\n  .navbar {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n*/\n\n@media only screen and (min-width: 990px) {\n  /* .bodywrapper {\n    padding-top: 0;\n  } */\n\n  /* .featureheading img {\n    width: 24px;\n    height: 24px;\n    margin-right: 5px;\n  } */\n\n  .HeaderV2-scheduleDemo-h_CeP {\n    padding: 8px 16px;\n    -ms-flex-order: 0;\n        order: 0;\n    margin: 0 12px;\n  }\n\n  .HeaderV2-login-31QGs {\n    padding: 8px 16px;\n    -ms-flex-order: 1;\n        order: 1;\n    margin: 0 12px;\n  }\n\n  .HeaderV2-navbarWrapper-3MC6c {\n    width: 100%;\n    -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n            box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n    padding: 16px 64px;\n    position: fixed;\n  }\n\n  .HeaderV2-navbar-1ZdUK {\n    width: 100%;\n    max-width: 1700px;\n    margin: auto;\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n    .HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI {\n      width: 6.125rem;\n      height: 3rem;\n    }\n\n      .HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI img {\n        width: 6.125rem;\n        height: 3rem;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH {\n    position: relative;\n    top: 0;\n    left: 0;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    background: none;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n    padding: 0;\n    //margin-right: 30px;\n    overflow: initial;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li {\n    border: none;\n    padding: 0;\n    margin-bottom: 0;\n  }\n\n  /* .featureheading span {\n    margin-left: 5px;\n    margin-top: 0;\n  } */\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span {\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li a {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D {\n    position: absolute;\n    top: 0%;\n    right: 0;\n    width: 190px;\n    border-radius: 8px;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    display: block;\n    margin-top: 30px;\n    margin-left: 0;\n    background: #fff;\n    opacity: 0;\n    visibility: hidden;\n    overflow: hidden;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li {\n    border-radius: 4px;\n    margin: 4px;\n    width: auto;\n    padding: 0 12px;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:hover {\n    background-color: rgba(236, 76, 111, 0.1);\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:nth-child(1) a {\n    padding-top: 12px;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D.HeaderV2-activedropdowns-_cvw6 {\n    opacity: 1;\n    visibility: visible;\n  }\n\n  /* .featuresdropdown {\n    flex-direction: row-reverse;\n    flex-wrap: wrap;\n  } */\n\n  /* .navbar .menu li .featuresdropdown {\n    position: absolute;\n    top: 40%;\n    left: -150%;\n    width: 888px;\n    height: 356px;\n    list-style: none;\n    display: flex;\n    // justify-content: space-between;\n    opacity: 0;\n    visibility: hidden;\n    background-color: #fff;\n    margin-top: 30px;\n    padding: 42px;\n    //border-radius: 8px;\n    box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  } */\n\n  /* .navbar .menu li .featuresdropdown.activefeatures {\n    opacity: 1;\n    visibility: visible;\n    flex-direction: row;\n  } */\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li > span {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n  }\n\n  .HeaderV2-featureName-3l-J0:hover {\n    text-decoration: underline;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span > .HeaderV2-featureName-3l-J0:hover {\n    text-decoration: underline;\n    -webkit-text-decoration-color: rgba(0, 0, 0, 0.6);\n            text-decoration-color: rgba(0, 0, 0, 0.6);\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n  }\n\n  /* .featurewrapper {\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 0;\n    max-width: 156px;\n    border-bottom: none;\n    margin-right: 31px;\n\n    .feature {\n      width: 100%;\n      height: 100%;\n      padding-right: 31px;\n      border-right: 1px solid rgb(37, 40, 43, 0.1);\n    }\n  }\n\n  .featurewrapper:nth-child(2) {\n    .feature {\n      padding-right: 48px;\n    }\n  } */\n\n  /* .featurewrapper:last-child {\n    margin-right: 0;\n\n    .feature {\n      padding: 0;\n      border-right: none;\n    }\n  } */\n\n  /* .viewmorewrapper {\n    width: auto;\n    height: fit-content;\n    //margin-top: -15px;\n    //padding-bottom: 10px;\n\n    .viewmore {\n      padding: 0;\n    }\n  } */\n\n  /* .featureitem {\n    display: flex;\n    align-items: center;\n    justify-content: flex-start;\n    margin: 15px 0;\n    font-size: 12px;\n  }\n\n  .featureitem:hover {\n    text-decoration: underline;\n    text-decoration-color: #25282b;\n  } */\n\n  /* .featureheading {\n    font-size: 16px;\n    display: flex;\n    justify-content: flex-start;\n  } */\n\n  /* .navbar .menu li:hover .featuresdropdown {\n  //   opacity: 1;\n  //   visibility: visible;\n  // } */\n  .HeaderV2-menubtn-2MJna {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/HeaderV2/HeaderV2.scss"],"names":[],"mappings":"AAAA;;;;;;IAMI;;AAEJ;;;IAGI;;AAEJ;;;;;;;;IAQI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;;IAyBI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;;;;IA2BI;;AAEJ;;;;;OAKO;;AAEP;EACE,iBAAiB;EACjB,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,uBAAuB;EACvB,kBAAkB;EAClB,gBAAgB;EAChB,mBAAmB;EACnB,0BAA0B;EAC1B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;MACd,SAAS;CACd;;AAED;EACE,oCAAoC;EACpC,YAAY;EACZ,qCAAqC;EACrC,gCAAgC;EAChC,6BAA6B;CAC9B;;AAED;EACE,YAAY;EACZ,QAAQ;EACR,SAAS;EACT,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,uBAAuB;EACvB,WAAW;EACX,WAAW;EACX,aAAa;EACb,gBAAgB;EAChB,qDAAqD;UAC7C,6CAA6C;EACrD,cAAc;EACd,cAAc;CACf;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,uBAAuB;MACnB,oBAAoB;EACxB,4BAA4B;MACxB,mBAAmB;CACxB;;AAED;;;IAGI;;AAEJ;;;IAGI;;AAEJ;EACE,YAAY;EACZ,kBAAkB;EAClB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;MACd,SAAS;CACd;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,aAAa;EACb,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,gBAAgB;EAChB,yCAAyC;CAC1C;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,eAAe;EACf,gBAAgB;EAChB,YAAY;EACZ,UAAU;EACV,YAAY;EACZ,cAAc;EACd,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;EACnB,mBAAmB;EACnB,oBAAoB;EACpB,oBAAoB;EACpB,yBAAyB;EACzB,oBAAoB;EACpB,iBAAiB;EACjB,uBAAuB;EACvB,eAAe;EACf,WAAW;CACZ;;AAED;EACE,SAAS;CACV;;AAED;;;IAGI;;AAEJ;;IAEI;;AAEJ;EACE,cAAc;EACd,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,WAAW;EACX,oBAAoB;CACrB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,+BAA+B;EACnC,gBAAgB;EAChB,mBAAmB;EACnB,6BAA6B;EAC7B,wBAAwB;EACxB,qBAAqB;EACrB,oBAAoB;EACpB,yCAAyC;CAC1C;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;;;;IAII;;AAEJ;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,8BAA8B;EAC9B,oBAAoB;EACpB,YAAY;EACZ,aAAa;EACb,gBAAgB;CACjB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,8BAA8B;EAC9B,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,iBAAiB;EACjB,cAAc;EACd,WAAW;EACX,WAAW;CACZ;;AAED;EACE,eAAe;CAChB;;AAED;;EAEE,YAAY;CACb;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,uBAAuB;MACnB,oBAAoB;EACxB,gBAAgB;CACjB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,kBAAkB;CACnB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,WAAW;CACZ;;AAED;EACE,kBAAkB;EAClB,iBAAiB;EACjB,0BAA0B;EAC1B,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,oBAAoB;EACpB,YAAY;EACZ,kBAAkB;MACd,SAAS;CACd;;AAED;;;;;;;;;;;;;;;;;;;;;;IAsBI;;AAEJ;;IAEI;;AAEJ;;;;IAII;;AAEJ;;;IAGI;;AAEJ;;;;;;;;;;;;IAYI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;EAwBE;;AAEF;EACE;;MAEI;;EAEJ;;;;MAII;;EAEJ;IACE,kBAAkB;IAClB,kBAAkB;QACd,SAAS;IACb,eAAe;GAChB;;EAED;IACE,kBAAkB;IAClB,kBAAkB;QACd,SAAS;IACb,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,gBAAgB;GACjB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,aAAa;IACb,qBAAqB;IACrB,cAAc;GACf;;IAEC;MACE,gBAAgB;MAChB,aAAa;KACd;;MAEC;QACE,gBAAgB;QAChB,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;EAEL;IACE,mBAAmB;IACnB,OAAO;IACP,QAAQ;IACR,4BAA4B;IAC5B,yBAAyB;IACzB,oBAAoB;IACpB,iBAAiB;IACjB,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;QACf,0BAA0B;IAC9B,WAAW;IACX,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,aAAa;IACb,WAAW;IACX,iBAAiB;GAClB;;EAED;;;MAGI;;EAEJ;IACE,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,SAAS;IACT,aAAa;IACb,mBAAmB;IACnB,qDAAqD;YAC7C,6CAA6C;IACrD,eAAe;IACf,iBAAiB;IACjB,eAAe;IACf,iBAAiB;IACjB,WAAW;IACX,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,YAAY;IACZ,gBAAgB;GACjB;;EAED;IACE,0CAA0C;GAC3C;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,WAAW;IACX,oBAAoB;GACrB;;EAED;;;MAGI;;EAEJ;;;;;;;;;;;;;;;;MAgBI;;EAEJ;;;;MAII;;EAEJ;IACE,gBAAgB;IAChB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,2BAA2B;GAC5B;;EAED;IACE,2BAA2B;IAC3B,kDAAkD;YAC1C,0CAA0C;IAClD,+BAA+B;YACvB,uBAAuB;GAChC;;EAED;;;;;;;;;;;;;;;;;;;;MAoBI;;EAEJ;;;;;;;MAOI;;EAEJ;;;;;;;;;MASI;;EAEJ;;;;;;;;;;;MAWI;;EAEJ;;;;MAII;;EAEJ;;;SAGO;EACP;IACE,cAAc;GACf;CACF","file":"HeaderV2.scss","sourcesContent":["/* .root {\n  height: 100%;\n  width: 100%;\n  background-color: #fff;\n  box-sizing: border-box;\n  overflow-x: hidden;\n} */\n\n/* .bodywrapper {\n  position: relative;\n  padding-top: 70px;\n} */\n\n/*\n.navbar{\n  width: 100%;\n  background-color: #fff;\n  min-height: 75px;\n  padding: 2rem 4rem;\n  justify-content: space-between;\n\n} */\n\n/* .navbar-collapse {\n  min-height: 100vh;\n  flex-grow: 0;\n}\n.dropdown-toggle::after {\n  display:none\n}\n.dropdown-menu {\n  border: none;\n\n}\n.navbar-toggler {\n  border: none;\n}\n.navbar-toggler:focus {\n  box-shadow: none;\n}\n@media only screen and (min-width : 992px) {\n  .navbar-expand-lg {\n    justify-content: space-between;\n    padding:1rem 4rem;\n  }\n  .navbar-collapse {\n    min-height: auto;\n  }\n} */\n\n/* .headerRoot {\n  height: 88px;\n  width: 100%;\n  padding: 27px 5%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.left {\n  height: 51px;\n  width: 105px;\n  display: flex;\n}\n\n.right {\n  height: 100%;\n  margin-right: 14px;\n}\n\n.requestDemoContainer {\n  width: 83px;\n  height: 40px;\n  border-radius: 4px;\n  background-color: #ffebf0;\n  padding: 8px 16px;\n  cursor: pointer;\n  float: right;\n} */\n\n/* .requestDemo {\n//   font-weight: 600;\n//   font-size: 16px;\n//   line-height: 1.5;\n//   color: #f36;\n// } */\n\n.scheduleDemo {\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 1.5;\n  color: #f36;\n  border: 1px solid #f36;\n  padding: 8px 12px;\n  cursor: pointer;\n  border-radius: 4px;\n  text-transform: uppercase;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-right: 12px;\n  background: none;\n  -ms-flex-order: 0;\n      order: 0;\n}\n\n.scheduleDemo:hover {\n  background-color: rgb(255, 51, 102);\n  color: #fff;\n  -webkit-transition: 0.5s ease-in-out;\n  -o-transition: 0.5s ease-in-out;\n  transition: 0.5s ease-in-out;\n}\n\n.navbarWrapper {\n  width: 100%;\n  top: 0%;\n  left: 0%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  background-color: #fff;\n  opacity: 1;\n  z-index: 2;\n  margin: auto;\n  position: fixed;\n  -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n  padding: 16px;\n  padding: 1rem;\n}\n\n.navbar {\n  width: 100%;\n  max-width: 1700px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-item-align: center;\n      align-self: center;\n}\n\n/* .featureitem img {\n  width: 20px;\n  height: 20px;\n} */\n\n/* .featureheading img {\n  width: 24px;\n  height: 24px;\n} */\n\n.menubtn {\n  color: #000;\n  font-size: 22.4px;\n  font-size: 1.4rem;\n  margin-left: 0;\n  -ms-flex-order: 2;\n      order: 2;\n}\n\n.menubtn img {\n    width: 20px;\n    height: 20px;\n  }\n\n.navbar .logo {\n  height: 40px;\n  height: 2.5rem;\n  width: 80px;\n  width: 5rem;\n  cursor: pointer;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.navbar .logo img {\n    width: 100%;\n    height: 100%;\n  }\n\n.navbar .menu {\n  display: block;\n  position: fixed;\n  left: -100%;\n  top: 69px;\n  width: 100%;\n  height: 100vh;\n  list-style: none;\n  padding: 24px 0;\n  padding: 1.5rem 0;\n  padding-left: 16px;\n  padding-left: 1rem;\n  padding-right: 16px;\n  padding-right: 1rem;\n  -webkit-transition: 0.4s;\n  -o-transition: 0.4s;\n  transition: 0.4s;\n  background-color: #fff;\n  overflow: auto;\n  z-index: 1;\n}\n\n.navbar .menu.active {\n  left: 0%;\n}\n\n/* .navbar .menu li .featuresdropdown {\n  display: none;\n  flex-direction: column;\n} */\n\n/* .navbar .menu li .featuresdropdown.activefeatures {\n  display: flex;\n} */\n\n.navbar .menu li .companydropdown {\n  display: none;\n  margin-left: 10px;\n}\n\n.navbar .menu li .companydropdown.activedropdowns {\n  display: block;\n  opacity: 1;\n  visibility: visible;\n}\n\n.navbar .menu > li {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  cursor: pointer;\n  position: relative;\n  -webkit-transition: all 0.4s;\n  -o-transition: all 0.4s;\n  transition: all 0.4s;\n  margin: 0 12px 24px;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.btngroup {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* featureheading span {\n  margin-left: 8px;\n  font-size: 16px;\n  font-weight: 600;\n} */\n\n/* .featureitem span {\n  margin-left: 8px;\n  color: #25282b;\n  line-height: 20px;\n  font-size: 14px;\n} */\n\n.navbar .menu > li > span {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n  color: #000;\n  opacity: 0.6;\n  font-size: 14px;\n}\n\n.navbar .menu > li > span > img {\n  margin-top: 3px;\n  margin-left: 5px;\n  border-bottom: none;\n}\n\n.navbar .menu > li > a {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n}\n\n.navbar .menu li ul {\n  width: 100%;\n  list-style: none;\n  display: none;\n  opacity: 0;\n  z-index: 2;\n}\n\n.activedropdowns {\n  display: block;\n}\n\n.navbar .menu li ul li,\n.navbar .menu li ul li a {\n  width: 100%;\n}\n\n.navbar .menu li ul li a {\n  color: #000;\n  opacity: 0.6;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 0;\n}\n\n.navbar .menu li ul li a img {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n}\n\n.navbar .menu li ul li:nth-child(1) a {\n  padding: 24px 0 12px 0;\n}\n\n.menubtn.menuactive {\n  z-index: 3;\n}\n\n.login {\n  padding: 8px 12px;\n  font-weight: 600;\n  text-transform: uppercase;\n  border-radius: 4px;\n  margin-right: 12px;\n  line-height: 20px;\n  background: #ffebf0;\n  color: #f36;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n/* .featurewrapper {\n  display: flex;\n  flex-direction: row;\n  align-items: flex-end;\n  justify-content: space-between;\n  padding: 24px 0;\n  border-bottom: 1px solid rgba(37, 40, 43, 0.1);\n\n  .feature {\n    height: 100%;\n    width: 60%;\n\n    .featureitem {\n      display: flex;\n      align-items: center;\n      margin: 24px 0;\n    }\n\n    .featureitem:last-child {\n      margin-bottom: 0;\n    }\n  }\n} */\n\n/* .featurewrapper:last-child {\n  border-bottom: none;\n} */\n\n/* .viewmorewrapper {\n  height: 27px;\n  position: relative;\n  width: 95px;\n} */\n\n/* .featureheading {\n  display: flex;\n  align-items: center;\n} */\n\n/* .viewmore {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: none;\n  color: #0076ff;\n  padding: 0;\n  text-transform: none;\n  font-size: 14px;\n  line-height: 20px;\n  font-weight: 400;\n  text-align: center;\n} */\n\n/* .search-icon {\n  font-size: 1.4rem;\n   margin-left: 6rem;\n  margin-right: 15px;\n   color: #fff;\n   justify-content: flex-end;\n\n.btngroup {\n  margin-left: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.login {\n  border: none;\n}\n\n@media only screen and (max-width: 1200px) {\n  .navbar {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n*/\n\n@media only screen and (min-width: 990px) {\n  /* .bodywrapper {\n    padding-top: 0;\n  } */\n\n  /* .featureheading img {\n    width: 24px;\n    height: 24px;\n    margin-right: 5px;\n  } */\n\n  .scheduleDemo {\n    padding: 8px 16px;\n    -ms-flex-order: 0;\n        order: 0;\n    margin: 0 12px;\n  }\n\n  .login {\n    padding: 8px 16px;\n    -ms-flex-order: 1;\n        order: 1;\n    margin: 0 12px;\n  }\n\n  .navbarWrapper {\n    width: 100%;\n    -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n            box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n    padding: 16px 64px;\n    position: fixed;\n  }\n\n  .navbar {\n    width: 100%;\n    max-width: 1700px;\n    margin: auto;\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n    .navbar .logo {\n      width: 6.125rem;\n      height: 3rem;\n    }\n\n      .navbar .logo img {\n        width: 6.125rem;\n        height: 3rem;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .navbar .menu {\n    position: relative;\n    top: 0;\n    left: 0;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    background: none;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n    padding: 0;\n    //margin-right: 30px;\n    overflow: initial;\n  }\n\n  .navbar .menu li {\n    border: none;\n    padding: 0;\n    margin-bottom: 0;\n  }\n\n  /* .featureheading span {\n    margin-left: 5px;\n    margin-top: 0;\n  } */\n\n  .navbar .menu > li > span {\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .navbar .menu li a {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .navbar .menu li .companydropdown {\n    position: absolute;\n    top: 0%;\n    right: 0;\n    width: 190px;\n    border-radius: 8px;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    display: block;\n    margin-top: 30px;\n    margin-left: 0;\n    background: #fff;\n    opacity: 0;\n    visibility: hidden;\n    overflow: hidden;\n  }\n\n  .navbar .menu li ul li {\n    border-radius: 4px;\n    margin: 4px;\n    width: auto;\n    padding: 0 12px;\n  }\n\n  .navbar .menu li ul li:hover {\n    background-color: rgba(236, 76, 111, 0.1);\n  }\n\n  .navbar .menu li ul li:nth-child(1) a {\n    padding-top: 12px;\n  }\n\n  .navbar .menu li .companydropdown.activedropdowns {\n    opacity: 1;\n    visibility: visible;\n  }\n\n  /* .featuresdropdown {\n    flex-direction: row-reverse;\n    flex-wrap: wrap;\n  } */\n\n  /* .navbar .menu li .featuresdropdown {\n    position: absolute;\n    top: 40%;\n    left: -150%;\n    width: 888px;\n    height: 356px;\n    list-style: none;\n    display: flex;\n    // justify-content: space-between;\n    opacity: 0;\n    visibility: hidden;\n    background-color: #fff;\n    margin-top: 30px;\n    padding: 42px;\n    //border-radius: 8px;\n    box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  } */\n\n  /* .navbar .menu li .featuresdropdown.activefeatures {\n    opacity: 1;\n    visibility: visible;\n    flex-direction: row;\n  } */\n\n  .navbar .menu li > span {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n  }\n\n  .featureName:hover {\n    text-decoration: underline;\n  }\n\n  .navbar .menu > li > span > .featureName:hover {\n    text-decoration: underline;\n    -webkit-text-decoration-color: rgba(0, 0, 0, 0.6);\n            text-decoration-color: rgba(0, 0, 0, 0.6);\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n  }\n\n  /* .featurewrapper {\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 0;\n    max-width: 156px;\n    border-bottom: none;\n    margin-right: 31px;\n\n    .feature {\n      width: 100%;\n      height: 100%;\n      padding-right: 31px;\n      border-right: 1px solid rgb(37, 40, 43, 0.1);\n    }\n  }\n\n  .featurewrapper:nth-child(2) {\n    .feature {\n      padding-right: 48px;\n    }\n  } */\n\n  /* .featurewrapper:last-child {\n    margin-right: 0;\n\n    .feature {\n      padding: 0;\n      border-right: none;\n    }\n  } */\n\n  /* .viewmorewrapper {\n    width: auto;\n    height: fit-content;\n    //margin-top: -15px;\n    //padding-bottom: 10px;\n\n    .viewmore {\n      padding: 0;\n    }\n  } */\n\n  /* .featureitem {\n    display: flex;\n    align-items: center;\n    justify-content: flex-start;\n    margin: 15px 0;\n    font-size: 12px;\n  }\n\n  .featureitem:hover {\n    text-decoration: underline;\n    text-decoration-color: #25282b;\n  } */\n\n  /* .featureheading {\n    font-size: 16px;\n    display: flex;\n    justify-content: flex-start;\n  } */\n\n  /* .navbar .menu li:hover .featuresdropdown {\n  //   opacity: 1;\n  //   visibility: visible;\n  // } */\n  .menubtn {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"scheduleDemo": "HeaderV2-scheduleDemo-h_CeP",
	"navbarWrapper": "HeaderV2-navbarWrapper-3MC6c",
	"navbar": "HeaderV2-navbar-1ZdUK",
	"menubtn": "HeaderV2-menubtn-2MJna",
	"logo": "HeaderV2-logo-34AJI",
	"menu": "HeaderV2-menu-3tVbH",
	"active": "HeaderV2-active-vZX84",
	"companydropdown": "HeaderV2-companydropdown-3kG4D",
	"activedropdowns": "HeaderV2-activedropdowns-_cvw6",
	"btngroup": "HeaderV2-btngroup-1738m",
	"menuactive": "HeaderV2-menuactive-1ufv2",
	"login": "HeaderV2-login-31QGs",
	"featureName": "HeaderV2-featureName-3l-J0"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*\n * Base styles\n * ========================================================================== */\n\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n::selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n/*\n * A better looking default horizontal rule\n */\n\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0;\n}\n\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\n\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle;\n}\n\n/*\n * Remove default fieldset styles.\n */\n\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0;\n}\n\n/*\n * Allow only vertical resizing of textareas.\n */\n\ntextarea {\n  resize: vertical;\n}\n\n.Layout-body-daVCC {\n  margin-top: 77px;\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA;;gFAEgF;;AAEhF;;;;;;GAMG;;AAEH;EACE,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;EACE,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;;GAEG;;AAEH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW;CACZ;;AAED;;;;GAIG;;AAEH;;;;;;EAME,uBAAuB;CACxB;;AAED;;GAEG;;AAEH;EACE,UAAU;EACV,UAAU;EACV,WAAW;CACZ;;AAED;;GAEG;;AAEH;EACE,iBAAiB;CAClB;;AAED;EACE,iBAAiB;CAClB","file":"Layout.css","sourcesContent":["/*\n * Base styles\n * ========================================================================== */\n\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n::selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n/*\n * A better looking default horizontal rule\n */\n\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0;\n}\n\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\n\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle;\n}\n\n/*\n * Remove default fieldset styles.\n */\n\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0;\n}\n\n/*\n * Allow only vertical resizing of textareas.\n */\n\ntextarea {\n  resize: vertical;\n}\n\n.body {\n  margin-top: 77px;\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"body": "Layout-body-daVCC"
};

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Footer_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Footer/Footer.scss");
/* harmony import */ var _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Footer_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Footer/Footer.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const FOOTER_LINKS = [
/* {
  title: 'Company',
  links: [
    { label: 'About Us', url: '/about-us' },
    { label: 'Team', url: '/team' },
    { label: 'Culture', url: '/culture' },
    { label: 'Careers', url: '/careers' },
  ],
},
{
  title: 'Product',
  links: [
    { label: 'GetRanks', url: '/products/jeet' },
    { label: 'ACADS', url: '/products/acads' },
    { label: 'Prep', url: '/products/prep' },
    { label: 'QMS', url: '/products/qms' },
  ],
}, */
{
  title: 'Links',
  links: [{
    label: 'About Us',
    url: '/company/aboutus'
  }, {
    label: 'Pricing Plan',
    url: '/pricing'
  }, {
    label: 'Contact us',
    url: '/request-demo'
  }, {
    label: 'Privacy Policy',
    url: '/privacy-and-terms'
  }, {
    label: 'Cancellation & Return Policy',
    url: '/cancellation-and-return'
  }, {
    label: 'Terms & Conditions',
    url: '/terms-and-conditions'
  }]
}];
const SOCIAL_LINKS = [{
  key: 'facebook',
  url: 'https://www.facebook.com/Egnify/',
  icon: '/images/footer/facebook.svg'
}, {
  key: 'youtube',
  url: 'https://www.youtube.com/user/yerranagu',
  icon: '/images/footer/youtube.svg'
}, {
  key: 'linkedin',
  url: 'https://www.linkedin.com/company/egnify',
  icon: '/images/footer/linkedin.svg'
}, {
  key: 'instagram',
  url: 'https://www.instagram.com/egnify',
  icon: '/images/footer/instagram.svg'
}, {
  key: 'twitter',
  url: 'https://twitter.com/egnify',
  icon: '/images/footer/twitter.svg'
}];

class Footer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    const {
      isAsh
    } = this.props;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
      className: isAsh ? _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addGray : null,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/getranks-marketing-new.svg",
      alt: "getranks by egnify",
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.logo,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `row ${_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.importantLinkContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.sectionDiv,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, FOOTER_LINKS.map(section => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }, section.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, section.links.map(sectionLink => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.importantLink,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: sectionLink.url,
      target: sectionLink.label === 'Blog' ? '_blank' : '',
      rel: "noreferrer noopener",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, sectionLink.label))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.mobile_logo_wrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/getranks-marketing-new.svg",
      alt: "getranks by egnify",
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.mobile_logo,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressDiv,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, "Hyderabad Office-1"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, "Krishe Emerald, Kondapur Main Road, Laxmi Cyber City, Whitefields, Kondapur, Hyderabad, Telangana 500081"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, "Hyderabad Office-2"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }, "1-2/1/24/A/1,7, Plot No 17 Opp. Bharat Petrol Bunk JNTU Rd, HI-TECH City Hyderabad, Telangana 500084"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 136
      },
      __self: this
    }, "Bengaluru Office"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138
      },
      __self: this
    }, "Prestige Atlanta, 80 Feet Main Road, Koramangala 1A Block, Bengaluru,Karnataka 560034"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.social,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `row ${_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLinkContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle1,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }, "Social"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `row ${_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLinksWrapper}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149
      },
      __self: this
    }, SOCIAL_LINKS.map(socialLink => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLink,
      href: socialLink.url,
      target: "_blank",
      rel: "noreferrer noopener",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: socialLink.icon,
      alt: socialLink.key,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }))))))))));
  }

}

_defineProperty(Footer, "propTypes", {
  isAsh: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool.isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Footer.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Footer/Footer.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/HeaderV2/HeaderV2.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/HeaderV2/HeaderV2.scss");
/* harmony import */ var _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/HeaderV2/HeaderV2.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class HeaderV2 extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    _defineProperty(this, "handlesize", () => {
      if (window.innerWidth < 990) {
        this.setState({
          mobile: true
        });
      } else {
        this.setState({
          mobile: false
        });
      }
    });

    _defineProperty(this, "toggleNavbar", (close = false) => {
      if (close) {
        this.setState({
          navOpen: false
        });
      } else {
        this.setState({
          navOpen: !this.state.navOpen
        });
      }
    });

    _defineProperty(this, "handleshowcompany", () => {
      this.setState({
        showcompany: !this.state.showcompany,
        showProducts: false // showresources: false,

      });
    });

    _defineProperty(this, "handleshowProduct", () => {
      this.setState({
        showcompany: false,
        showProducts: !this.state.showProducts
      });
    });

    this.state = {
      navOpen: false,
      mobile: false,
      showcompany: false,
      showProducts: false
    };
  }

  componentDidMount() {
    this.handlesize();
    window.addEventListener('resize', this.handlesize);
    document.addEventListener('click', event => {
      if (
      /*! document.getElementById('featureslabel').contains(event.target) && */
      document.getElementById('companylabel') && !document.getElementById('companylabel').contains(event.target) // document.getElementById('productslabel') &&
      // !document.getElementById('productslabel').contains(event.target)

      /* !document.getElementById('resourceslabel').contains(event.target) */
      ) {
          this.setState({
            // showfeatures: false,
            showcompany: false // showProducts: false,
            // showresources: false,

          });
        }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handlesize);
    document.removeEventListener('click', event => {
      if ( // !document.getElementById('productslabel').contains(event.target)
      !document.getElementById('companylabel').contains(event.target) // !document.getElementById('resourceslabel').contains(event.target)
      ) {
          this.setState({
            // showfeatures: false,
            showcompany: false // showProducts: false,
            // showresources: false,

          });
        }
    });
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navbarWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.logo,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/getranks-marketing-new.svg",
      alt: "getranks",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: this.state.navOpen ? `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menu} ${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.active}` : `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menu}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      id: "productslabel",
      onClick: this.handleshowProduct,
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 180
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 185
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 186
      },
      __self: this
    }, "Products"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: this.state.showProducts ? '/images/icons/chevron-up.svg' : '/images/icons/chevron-down.svg',
      alt: "down-angle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 187
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: this.state.showProducts ? `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown} ${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.activedropdowns}` : `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 196
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 203
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/modules/tests",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Test/module_test.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 209
      },
      __self: this
    }, "Online Tests"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 212
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/modules/liveclasses",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Teach/old_Live.svg",
      alt: "live-classes",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 218
      },
      __self: this
    }, "Live Classes"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/modules/doubts",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 222
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Teach/old_Doubts.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 223
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 227
      },
      __self: this
    }, "Doubts"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 230
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/modules/assignments",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 231
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Teach/old_Assignments.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 232
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 236
      },
      __self: this
    }, "Assignments"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 239
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/modules/connect",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Connect/new_connect.svg",
      alt: "connect",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 241
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 245
      },
      __self: this
    }, "Connect"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 250
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/customers",
      onClick: () => this.toggleNavbar(false),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 251
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 252
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 253
      },
      __self: this
    }, "Customers")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 257
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/pricing",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 258
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 259
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 260
      },
      __self: this
    }, "Pricing")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      id: "companylabel",
      onClick: this.handleshowcompany,
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 264
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 269
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 270
      },
      __self: this
    }, "Company"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: this.state.showcompany ? '/images/icons/chevron-up.svg' : '/images/icons/chevron-down.svg',
      alt: "down-angle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 271
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: this.state.showcompany ? `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown} ${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.activedropdowns}` : `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 280
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 288
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/company/aboutus",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 289
      },
      __self: this
    }, "About Us")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 293
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/company/press",
      onClick: this.toggleNavbar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 294
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 295
      },
      __self: this
    }, "Press"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 298
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/company/culture",
      onClick: () => this.toggleNavbar(false),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 299
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 303
      },
      __self: this
    }, "Culture"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 306
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "/company/careers",
      onClick: () => this.toggleNavbar(false),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 307
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 311
      },
      __self: this
    }, "Careers"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 347
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      role: "presentation",
      onClick: () => {
        window.open('https://blog.egnify.com', 'blank');
      },
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 348
      },
      __self: this
    }, "Blog"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.btngroup,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 360
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/request-demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 361
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.scheduleDemo,
      role: "presentation",
      onClick: () => this.toggleNavbar(true),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 362
      },
      __self: this
    }, this.state.mobile ? 'Demo' : 'SCHEDULE DEMO')), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://account.getranks.in/signin?host=https%3A%2F%2Fgetranks.in%2F",
      className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.login,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 370
      },
      __self: this
    }, "Login"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      onClick: () => this.toggleNavbar(false),
      role: "presentation",
      className: this.state.navOpen ? `${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menubtn} ${_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menuactive}` : _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menubtn,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 376
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: !this.state.navOpen ? '/images/icons/drawer.svg' : '/images/icons/close.svg',
      alt: "toggle button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 383
      },
      __self: this
    })))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(HeaderV2));

/***/ }),

/***/ "./src/components/HeaderV2/HeaderV2.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/HeaderV2/HeaderV2.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Layout_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var _Layout_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Layout_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _HeaderV2_HeaderV2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/HeaderV2/HeaderV2.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/Footer/Footer.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Layout/Layout.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



 // external-global styles must be imported in your JS.





class Layout extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HeaderV2_HeaderV2__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `row ${_Layout_css__WEBPACK_IMPORTED_MODULE_3___default.a.body}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, this.props.children), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      isAsh: this.props.footerAsh,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }));
  }

}

_defineProperty(Layout, "propTypes", {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  footerAsh: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
});

Layout.defaultProps = {
  footerAsh: false
};
/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Layout_css__WEBPACK_IMPORTED_MODULE_3___default.a)(Layout));

/***/ }),

/***/ "./src/components/Link/Link.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _history__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/history.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Link/Link.js";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

class Link extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "handleClick", event => {
      if (this.props.onClick) {
        this.props.onClick(event);
      }

      if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
        return;
      }

      if (event.defaultPrevented === true) {
        return;
      }

      event.preventDefault();
      _history__WEBPACK_IMPORTED_MODULE_2__["default"].push(this.props.to);
    });
  }

  render() {
    const {
      to,
      children,
      ...props
    } = this.props;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", _extends({
      href: to
    }, props, {
      onClick: this.handleClick,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }), children);
  }

}

_defineProperty(Link, "propTypes", {
  to: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
});

_defineProperty(Link, "defaultProps", {
  onClick: null
});

/* harmony default export */ __webpack_exports__["default"] = (Link);

/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("history/createBrowserHistory");
/* harmony import */ var history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0__);
 // Navigation manager, e.g. history.push('/home')
// https://github.com/mjackson/history

/* harmony default export */ __webpack_exports__["default"] = ( false && false);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzLzAuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvSGVhZGVyVjIvSGVhZGVyVjIuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9Gb290ZXIvRm9vdGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuc2Nzcz8yYzZhIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0hlYWRlclYyL0hlYWRlclYyLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0hlYWRlclYyL0hlYWRlclYyLnNjc3M/ZTQ0YiIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzcz8xNzY2IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvTGluay9MaW5rLmpzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9oaXN0b3J5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJmb290ZXIge1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgcGFkZGluZy10b3A6IDIuNXJlbTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDIuNXJlbTtcXG59XFxuXFxuLkZvb3Rlci1hZGRHcmF5LTNlY2Z4IHtcXG4gIGJhY2tncm91bmQ6ICNmN2Y3Zjc7XFxufVxcblxcbi5Gb290ZXItbG9nby0xVzN1VCB7XFxuICBoZWlnaHQ6IDU2cHg7XFxuICBoZWlnaHQ6IDMuNXJlbTtcXG4gIHdpZHRoOiAxMTMuOTg0cHg7XFxuICB3aWR0aDogNy4xMjRyZW07XFxuICBtYXJnaW4tbGVmdDogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxufVxcblxcbi5Gb290ZXItbW9iaWxlX2xvZ29fd3JhcHBlci0xVk1oUCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xcbiAgbWF4LXdpZHRoOiAxMjRweDtcXG4gIG1heC1oZWlnaHQ6IDYwcHg7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5Gb290ZXItbGlua3NTZWN0aW9uVGl0bGUtRG13akkge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tYm90dG9tOiA0cHg7XFxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcXG59XFxuXFxuLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIHtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDAgNjRweDtcXG4gIC8vIGZsZXgtd3JhcDogd3JhcDtcXG59XFxuXFxuLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIC5Gb290ZXItc2VjdGlvbkRpdi0xSnBTRSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAyNHB4O1xcbiAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgIG9yZGVyOiAyO1xcbiAgfVxcblxcbi5Gb290ZXItaW1wb3J0YW50TGlua0NvbnRhaW5lci1Bck9URiAuRm9vdGVyLXNlY3Rpb25EaXYtMUpwU0Ugc2VjdGlvbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgIH1cXG5cXG4uRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUge1xcbiAgbGVmdDogMDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgb3JkZXI6IDE7XFxufVxcblxcbi5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLWFkZHJlc3NDb250YWluZXItMlpZLXMge1xcbiAgICB3aWR0aDogMTkycHg7XFxuICB9XFxuXFxuLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItYWRkcmVzc0NvbnRhaW5lci0yWlktcyAuRm9vdGVyLWFkZHJlc3MtckZ5Qk8ge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xcbiAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xcbiAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XFxuICAgICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIH1cXG5cXG4uRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zIC5Gb290ZXItYWRkcmVzcy1yRnlCTyBwIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgIH1cXG5cXG4uRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci12ZXJ0aWNhbExpbmUtQTFmSTUge1xcbiAgICB3aWR0aDogMXB4O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmI7XFxuICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MCk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoOTApO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwKTtcXG4gICAgbWFyZ2luOiAwIDMycHg7XFxuICB9XFxuXFxuLkZvb3Rlci1pbXBvcnRhbnRMaW5rLTNhUVBCIHtcXG4gIG1hcmdpbjogNnB4IDA7XFxufVxcblxcbi5Gb290ZXItaW1wb3J0YW50TGluay0zYVFQQiBhIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcXG4gIH1cXG5cXG4uRm9vdGVyLWltcG9ydGFudExpbmstM2FRUEIgYTpob3ZlciB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgfVxcblxcbi5Gb290ZXItc29jaWFsLWwyYXJZIHtcXG4gIHBhZGRpbmctbGVmdDogMzJweDtcXG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgzNywgNDAsIDQzLCAwLjEpO1xcbiAgbWFyZ2luLWxlZnQ6IDA7XFxuICAtbXMtZmxleC1vcmRlcjogMztcXG4gICAgICBvcmRlcjogMztcXG59XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItbGlua3NTZWN0aW9uVGl0bGUxLTM0ekZ1IHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICBjb2xvcjogIzAwMDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB9XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIHtcXG4gICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICB9XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIC5Gb290ZXItc29jaWFsTGluay0xUXZSdiB7XFxuICAgICAgICBmbG9hdDogbGVmdDtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICB9XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIC5Gb290ZXItc29jaWFsTGluay0xUXZSdiBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgfVxcblxcbi5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSAuRm9vdGVyLXNvY2lhbExpbmstMVF2UnYgaSB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgICAgZm9udC1zaXplOiAycmVtO1xcbiAgICAgICAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgICAgIH1cXG5cXG4uRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUUgLkZvb3Rlci1zb2NpYWxMaW5rLTFRdlJ2Omxhc3QtY2hpbGQge1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICAgIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyOTBweCkge1xcbiAgLkZvb3Rlci1sb2dvLTFXM3VUIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDMycHg7XFxuICB9XFxuXFxuICAuRm9vdGVyLWltcG9ydGFudExpbmtDb250YWluZXItQXJPVEYge1xcbiAgICBwYWRkaW5nOiAwIDMycHg7XFxuICB9XFxuXFxuICAgIC5Gb290ZXItaW1wb3J0YW50TGlua0NvbnRhaW5lci1Bck9URiAuRm9vdGVyLXNlY3Rpb25EaXYtMUpwU0Uge1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XFxuICAgIH1cXG4gICAgLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItYWRkcmVzc0NvbnRhaW5lci0yWlktcyB7XFxuICAgICAgd2lkdGg6IDE3MnB4O1xcbiAgICB9XFxuXFxuICAgIC5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLXZlcnRpY2FsTGluZS1BMWZJNSB7XFxuICAgICAgbWFyZ2luOiAwIDEycHg7XFxuICAgIH1cXG5cXG4gIC5Gb290ZXItc29jaWFsLWwyYXJZIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAxMnB4O1xcbiAgICAtbXMtZmxleDogMSAxO1xcbiAgICAgICAgZmxleDogMSAxO1xcbiAgfVxcbiAgICAgIC5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSB7XFxuICAgICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSAuRm9vdGVyLXNvY2lhbExpbmstMVF2UnYge1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICAgIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMTEwcHgpIHtcXG4gICAgICAuRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUUge1xcbiAgICAgICAgZGlzcGxheTogZ3JpZDtcXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuRm9vdGVyLWxvZ28tMVczdVQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgZm9vdGVyIHtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5Gb290ZXItbW9iaWxlX2xvZ29fd3JhcHBlci0xVk1oUCB7XFxuICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIHdpZHRoOiA3Ljc1cmVtO1xcbiAgICBoZWlnaHQ6IDMuNzVyZW07XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgbWF4LWhlaWdodDogbm9uZTtcXG4gIH1cXG5cXG4gIC5Gb290ZXItbW9iaWxlX2xvZ28talVCLWUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbiAgLkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZS1EbXdqSSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDQycHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHBhZGRpbmc6IDMycHggMjBweDtcXG4gIH1cXG5cXG4gICAgLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIHNlY3Rpb24ge1xcbiAgICAgIG1hcmdpbjogLTQ4cHggYXV0byAwIGF1dG87XFxuICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xcbiAgICB9XFxuXFxuICAgIC5Gb290ZXItaW1wb3J0YW50TGlua0NvbnRhaW5lci1Bck9URiAuRm9vdGVyLXNlY3Rpb25EaXYtMUpwU0Uge1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgICB9XFxuXFxuICAuRm9vdGVyLXNvY2lhbC1sMmFyWSB7XFxuICAgIHBhZGRpbmctbGVmdDogMDtcXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XFxuICB9XFxuXFxuICAgIC5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyB7XFxuICAgICAgd2lkdGg6IDIwMHB4O1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICBtYXJnaW4tdG9wOiAxLjVyZW0gIWltcG9ydGFudDtcXG4gICAgfVxcblxcbiAgICAgIC5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSB7XFxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg1LCAxZnIpO1xcbiAgICAgIH1cXG5cXG4gIC5Gb290ZXItc29jaWFsTGluay0xUXZSdiBpIHtcXG4gICAgZm9udC1zaXplOiAyLjVyZW07XFxuICB9XFxuXFxuICAuRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgfVxcblxcbiAgICAuRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICBtYXJnaW46IDAgYXV0byAyNHB4IGF1dG87XFxuICAgICAgbWF4LXdpZHRoOiAyODBweDtcXG4gICAgfVxcblxcbiAgICAgIC5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLWFkZHJlc3NDb250YWluZXItMlpZLXMgLkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZS1EbXdqSSB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xcbiAgICAgIH1cXG5cXG4gICAgICAuRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zIC5Gb290ZXItYWRkcmVzcy1yRnlCTyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjcxO1xcbiAgICAgICAgb3BhY2l0eTogMC42O1xcbiAgICAgIH1cXG5cXG4gICAgLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItdmVydGljYWxMaW5lLUExZkk1IHtcXG4gICAgICBkaXNwbGF5OiBub25lO1xcbiAgICB9XFxuXFxuICAuRm9vdGVyLWltcG9ydGFudExpbmstM2FRUEIge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogMXJlbSAwO1xcbiAgfVxcblxcbiAgICAuRm9vdGVyLWltcG9ydGFudExpbmstM2FRUEIgYSB7XFxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgfVxcblxcbiAgLkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZTEtMzR6RnUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtFQUNyQix1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsbUJBQW1CO0NBQ3BCOztBQUVEO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIsa0JBQWtCO1FBQ2QsU0FBUztHQUNkOztBQUVIO01BQ00sWUFBWTtLQUNiOztBQUVMO0VBQ0UsUUFBUTtFQUNSLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQixrQkFBa0I7TUFDZCxTQUFTO0NBQ2Q7O0FBRUQ7SUFDSSxhQUFhO0dBQ2Q7O0FBRUg7TUFDTSxZQUFZO01BQ1osb0JBQW9CO01BQ3BCLG1CQUFtQjtNQUNuQixxQkFBcUI7TUFDckIsdUJBQXVCO01BQ3ZCLGVBQWU7S0FDaEI7O0FBRUw7UUFDUSxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLFVBQVU7T0FDWDs7QUFFUDtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsMEJBQTBCO0lBQzFCLGFBQWE7SUFDYiw4QkFBOEI7UUFDMUIsMEJBQTBCO1lBQ3RCLHNCQUFzQjtJQUM5QixlQUFlO0dBQ2hCOztBQUVIO0VBQ0UsY0FBYztDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQiwyQkFBMkI7R0FDNUI7O0FBRUg7SUFDSSwyQkFBMkI7R0FDNUI7O0FBRUg7RUFDRSxtQkFBbUI7RUFDbkIsNkNBQTZDO0VBQzdDLGVBQWU7RUFDZixrQkFBa0I7TUFDZCxTQUFTO0NBQ2Q7O0FBRUQ7TUFDTSxnQkFBZ0I7TUFDaEIsb0JBQW9CO01BQ3BCLGtCQUFrQjtNQUNsQixZQUFZO01BQ1osaUJBQWlCO0tBQ2xCOztBQUVMO01BQ00sY0FBYztNQUNkLHNDQUFzQztNQUN0QywyQkFBMkI7TUFDM0Isd0JBQXdCO01BQ3hCLG1CQUFtQjtLQUNwQjs7QUFFTDtRQUNRLFlBQVk7UUFDWixtQkFBbUI7UUFDbkIsb0JBQW9CO1FBQ3BCLFlBQVk7UUFDWixhQUFhO09BQ2Q7O0FBRVA7VUFDVSxZQUFZO1VBQ1osYUFBYTtVQUNiLHVCQUF1QjthQUNwQixvQkFBb0I7U0FDeEI7O0FBRVQ7VUFDVSxnQkFBZ0I7VUFDaEIsZ0JBQWdCO1VBQ2hCLGVBQWU7U0FDaEI7O0FBRVQ7UUFDUSxnQkFBZ0I7T0FDakI7O0FBRVA7RUFDRTtJQUNFLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7SUFFQztNQUNFLG9CQUFvQjtLQUNyQjtJQUNEO01BQ0UsYUFBYTtLQUNkOztJQUVEO01BQ0UsZUFBZTtLQUNoQjs7RUFFSDtJQUNFLG1CQUFtQjtJQUNuQixjQUFjO1FBQ1YsVUFBVTtHQUNmO01BQ0c7UUFDRSxjQUFjO1FBQ2Qsc0NBQXNDO1FBQ3RDLGVBQWU7UUFDZixnQkFBZ0I7T0FDakI7O1FBRUM7VUFDRSxtQkFBbUI7VUFDbkIsbUJBQW1CO1NBQ3BCO0NBQ1I7O0FBRUQ7TUFDTTtRQUNFLGNBQWM7UUFDZCxzQ0FBc0M7T0FDdkM7Q0FDTjs7QUFFRDtFQUNFO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0Usc0JBQXNCO0lBQ3RCLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0UsMEJBQTBCO01BQzFCLHFDQUFxQztLQUN0Qzs7SUFFRDtNQUNFLGFBQWE7TUFDYixlQUFlO01BQ2YsaUJBQWlCO0tBQ2xCOztFQUVIO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7SUFFQztNQUNFLGFBQWE7TUFDYixhQUFhO01BQ2IsOEJBQThCO0tBQy9COztNQUVDO1FBQ0Usc0NBQXNDO09BQ3ZDOztFQUVMO0lBQ0Usa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsaUJBQWlCO0dBQ2xCOztJQUVDO01BQ0UsWUFBWTtNQUNaLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQixzQkFBc0I7VUFDbEIsd0JBQXdCO01BQzVCLHVCQUF1QjtVQUNuQixvQkFBb0I7TUFDeEIseUJBQXlCO01BQ3pCLGlCQUFpQjtLQUNsQjs7TUFFQztRQUNFLGNBQWM7T0FDZjs7TUFFRDtRQUNFLFlBQVk7UUFDWixtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixhQUFhO09BQ2Q7O0lBRUg7TUFDRSxjQUFjO0tBQ2Y7O0VBRUg7SUFDRSxtQkFBbUI7SUFDbkIsZUFBZTtHQUNoQjs7SUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7RUFFSDtJQUNFLGNBQWM7R0FDZjtDQUNGXCIsXCJmaWxlXCI6XCJGb290ZXIuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCJmb290ZXIge1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgcGFkZGluZy10b3A6IDIuNXJlbTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDIuNXJlbTtcXG59XFxuXFxuLmFkZEdyYXkge1xcbiAgYmFja2dyb3VuZDogI2Y3ZjdmNztcXG59XFxuXFxuLmxvZ28ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgaGVpZ2h0OiAzLjVyZW07XFxuICB3aWR0aDogMTEzLjk4NHB4O1xcbiAgd2lkdGg6IDcuMTI0cmVtO1xcbiAgbWFyZ2luLWxlZnQ6IDY0cHg7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbn1cXG5cXG4ubW9iaWxlX2xvZ29fd3JhcHBlciB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xcbiAgbWF4LXdpZHRoOiAxMjRweDtcXG4gIG1heC1oZWlnaHQ6IDYwcHg7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5saW5rc1NlY3Rpb25UaXRsZSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1ib3R0b206IDRweDtcXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xcbn1cXG5cXG4uaW1wb3J0YW50TGlua0NvbnRhaW5lciB7XFxuICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiAwIDY0cHg7XFxuICAvLyBmbGV4LXdyYXA6IHdyYXA7XFxufVxcblxcbi5pbXBvcnRhbnRMaW5rQ29udGFpbmVyIC5zZWN0aW9uRGl2IHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDI0cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAyO1xcbiAgICAgICAgb3JkZXI6IDI7XFxuICB9XFxuXFxuLmltcG9ydGFudExpbmtDb250YWluZXIgLnNlY3Rpb25EaXYgc2VjdGlvbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgIH1cXG5cXG4uYWRkcmVzc0RpdiB7XFxuICBsZWZ0OiAwO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICBvcmRlcjogMTtcXG59XFxuXFxuLmFkZHJlc3NEaXYgLmFkZHJlc3NDb250YWluZXIge1xcbiAgICB3aWR0aDogMTkycHg7XFxuICB9XFxuXFxuLmFkZHJlc3NEaXYgLmFkZHJlc3NDb250YWluZXIgLmFkZHJlc3Mge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xcbiAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xcbiAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XFxuICAgICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIH1cXG5cXG4uYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciAuYWRkcmVzcyBwIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgIH1cXG5cXG4uYWRkcmVzc0RpdiAudmVydGljYWxMaW5lIHtcXG4gICAgd2lkdGg6IDFweDtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiO1xcbiAgICBvcGFjaXR5OiAwLjE7XFxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTApO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDkwKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MCk7XFxuICAgIG1hcmdpbjogMCAzMnB4O1xcbiAgfVxcblxcbi5pbXBvcnRhbnRMaW5rIHtcXG4gIG1hcmdpbjogNnB4IDA7XFxufVxcblxcbi5pbXBvcnRhbnRMaW5rIGEge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xcbiAgfVxcblxcbi5pbXBvcnRhbnRMaW5rIGE6aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIH1cXG5cXG4uc29jaWFsIHtcXG4gIHBhZGRpbmctbGVmdDogMzJweDtcXG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgzNywgNDAsIDQzLCAwLjEpO1xcbiAgbWFyZ2luLWxlZnQ6IDA7XFxuICAtbXMtZmxleC1vcmRlcjogMztcXG4gICAgICBvcmRlcjogMztcXG59XFxuXFxuLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAubGlua3NTZWN0aW9uVGl0bGUxIHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICBjb2xvcjogIzAwMDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB9XFxuXFxuLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIHtcXG4gICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICB9XFxuXFxuLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIC5zb2NpYWxMaW5rIHtcXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgIH1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIgLnNvY2lhbExpbmsgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgIH1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIgLnNvY2lhbExpbmsgaSB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgICAgZm9udC1zaXplOiAycmVtO1xcbiAgICAgICAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgICAgIH1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIgLnNvY2lhbExpbms6bGFzdC1jaGlsZCB7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTI5MHB4KSB7XFxuICAubG9nbyB7XFxuICAgIG1hcmdpbi1sZWZ0OiAzMnB4O1xcbiAgfVxcblxcbiAgLmltcG9ydGFudExpbmtDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAwIDMycHg7XFxuICB9XFxuXFxuICAgIC5pbXBvcnRhbnRMaW5rQ29udGFpbmVyIC5zZWN0aW9uRGl2IHtcXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMnB4O1xcbiAgICB9XFxuICAgIC5hZGRyZXNzRGl2IC5hZGRyZXNzQ29udGFpbmVyIHtcXG4gICAgICB3aWR0aDogMTcycHg7XFxuICAgIH1cXG5cXG4gICAgLmFkZHJlc3NEaXYgLnZlcnRpY2FsTGluZSB7XFxuICAgICAgbWFyZ2luOiAwIDEycHg7XFxuICAgIH1cXG5cXG4gIC5zb2NpYWwge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XFxuICAgIC1tcy1mbGV4OiAxIDE7XFxuICAgICAgICBmbGV4OiAxIDE7XFxuICB9XFxuICAgICAgLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIHtcXG4gICAgICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIC5zb2NpYWxMaW5rIHtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICAgICAgICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTExMHB4KSB7XFxuICAgICAgLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIHtcXG4gICAgICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICAgIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLmxvZ28ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgZm9vdGVyIHtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVfbG9nb193cmFwcGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgd2lkdGg6IDcuNzVyZW07XFxuICAgIGhlaWdodDogMy43NXJlbTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICAgIHBhZGRpbmctbGVmdDogMDtcXG4gICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICBtYXgtaGVpZ2h0OiBub25lO1xcbiAgfVxcblxcbiAgLm1vYmlsZV9sb2dvIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4gIC5saW5rc1NlY3Rpb25UaXRsZSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDQycHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLmltcG9ydGFudExpbmtDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgcGFkZGluZzogMzJweCAyMHB4O1xcbiAgfVxcblxcbiAgICAuaW1wb3J0YW50TGlua0NvbnRhaW5lciBzZWN0aW9uIHtcXG4gICAgICBtYXJnaW46IC00OHB4IGF1dG8gMCBhdXRvO1xcbiAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNmZmYgIWltcG9ydGFudDtcXG4gICAgfVxcblxcbiAgICAuaW1wb3J0YW50TGlua0NvbnRhaW5lciAuc2VjdGlvbkRpdiB7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICAgIH1cXG5cXG4gIC5zb2NpYWwge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIGJvcmRlci1sZWZ0OiBub25lO1xcbiAgfVxcblxcbiAgICAuc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIHtcXG4gICAgICB3aWR0aDogMjAwcHg7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIG1hcmdpbi10b3A6IDEuNXJlbSAhaW1wb3J0YW50O1xcbiAgICB9XFxuXFxuICAgICAgLnNvY2lhbCAuc29jaWFsTGlua0NvbnRhaW5lciAuc29jaWFsTGlua3NXcmFwcGVyIHtcXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDUsIDFmcik7XFxuICAgICAgfVxcblxcbiAgLnNvY2lhbExpbmsgaSB7XFxuICAgIGZvbnQtc2l6ZTogMi41cmVtO1xcbiAgfVxcblxcbiAgLmFkZHJlc3NEaXYge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgfVxcblxcbiAgICAuYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMjRweCBhdXRvO1xcbiAgICAgIG1heC13aWR0aDogMjgwcHg7XFxuICAgIH1cXG5cXG4gICAgICAuYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciAubGlua3NTZWN0aW9uVGl0bGUge1xcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcXG4gICAgICB9XFxuXFxuICAgICAgLmFkZHJlc3NEaXYgLmFkZHJlc3NDb250YWluZXIgLmFkZHJlc3Mge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDEzcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMS43MTtcXG4gICAgICAgIG9wYWNpdHk6IDAuNjtcXG4gICAgICB9XFxuXFxuICAgIC5hZGRyZXNzRGl2IC52ZXJ0aWNhbExpbmUge1xcbiAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIH1cXG5cXG4gIC5pbXBvcnRhbnRMaW5rIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW46IDFyZW0gMDtcXG4gIH1cXG5cXG4gICAgLmltcG9ydGFudExpbmsgYSB7XFxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgfVxcblxcbiAgLmxpbmtzU2VjdGlvblRpdGxlMSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJhZGRHcmF5XCI6IFwiRm9vdGVyLWFkZEdyYXktM2VjZnhcIixcblx0XCJsb2dvXCI6IFwiRm9vdGVyLWxvZ28tMVczdVRcIixcblx0XCJtb2JpbGVfbG9nb193cmFwcGVyXCI6IFwiRm9vdGVyLW1vYmlsZV9sb2dvX3dyYXBwZXItMVZNaFBcIixcblx0XCJsaW5rc1NlY3Rpb25UaXRsZVwiOiBcIkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZS1EbXdqSVwiLFxuXHRcImltcG9ydGFudExpbmtDb250YWluZXJcIjogXCJGb290ZXItaW1wb3J0YW50TGlua0NvbnRhaW5lci1Bck9URlwiLFxuXHRcInNlY3Rpb25EaXZcIjogXCJGb290ZXItc2VjdGlvbkRpdi0xSnBTRVwiLFxuXHRcImFkZHJlc3NEaXZcIjogXCJGb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZVwiLFxuXHRcImFkZHJlc3NDb250YWluZXJcIjogXCJGb290ZXItYWRkcmVzc0NvbnRhaW5lci0yWlktc1wiLFxuXHRcImFkZHJlc3NcIjogXCJGb290ZXItYWRkcmVzcy1yRnlCT1wiLFxuXHRcInZlcnRpY2FsTGluZVwiOiBcIkZvb3Rlci12ZXJ0aWNhbExpbmUtQTFmSTVcIixcblx0XCJpbXBvcnRhbnRMaW5rXCI6IFwiRm9vdGVyLWltcG9ydGFudExpbmstM2FRUEJcIixcblx0XCJzb2NpYWxcIjogXCJGb290ZXItc29jaWFsLWwyYXJZXCIsXG5cdFwic29jaWFsTGlua0NvbnRhaW5lclwiOiBcIkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfXCIsXG5cdFwibGlua3NTZWN0aW9uVGl0bGUxXCI6IFwiRm9vdGVyLWxpbmtzU2VjdGlvblRpdGxlMS0zNHpGdVwiLFxuXHRcInNvY2lhbExpbmtzV3JhcHBlclwiOiBcIkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUVcIixcblx0XCJzb2NpYWxMaW5rXCI6IFwiRm9vdGVyLXNvY2lhbExpbmstMVF2UnZcIixcblx0XCJtb2JpbGVfbG9nb1wiOiBcIkZvb3Rlci1tb2JpbGVfbG9nby1qVUItZVwiXG59OyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIvKiAucm9vdCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbn0gKi9cXG5cXG4vKiAuYm9keXdyYXBwZXIge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgcGFkZGluZy10b3A6IDcwcHg7XFxufSAqL1xcblxcbi8qXFxuLm5hdmJhcntcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG1pbi1oZWlnaHQ6IDc1cHg7XFxuICBwYWRkaW5nOiAycmVtIDRyZW07XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuXFxufSAqL1xcblxcbi8qIC5uYXZiYXItY29sbGFwc2Uge1xcbiAgbWluLWhlaWdodDogMTAwdmg7XFxuICBmbGV4LWdyb3c6IDA7XFxufVxcbi5kcm9wZG93bi10b2dnbGU6OmFmdGVyIHtcXG4gIGRpc3BsYXk6bm9uZVxcbn1cXG4uZHJvcGRvd24tbWVudSB7XFxuICBib3JkZXI6IG5vbmU7XFxuXFxufVxcbi5uYXZiYXItdG9nZ2xlciB7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbi5uYXZiYXItdG9nZ2xlcjpmb2N1cyB7XFxuICBib3gtc2hhZG93OiBub25lO1xcbn1cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5OTJweCkge1xcbiAgLm5hdmJhci1leHBhbmQtbGcge1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAgIHBhZGRpbmc6MXJlbSA0cmVtO1xcbiAgfVxcbiAgLm5hdmJhci1jb2xsYXBzZSB7XFxuICAgIG1pbi1oZWlnaHQ6IGF1dG87XFxuICB9XFxufSAqL1xcblxcbi8qIC5oZWFkZXJSb290IHtcXG4gIGhlaWdodDogODhweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogMjdweCA1JTtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5sZWZ0IHtcXG4gIGhlaWdodDogNTFweDtcXG4gIHdpZHRoOiAxMDVweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5yaWdodCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtYXJnaW4tcmlnaHQ6IDE0cHg7XFxufVxcblxcbi5yZXF1ZXN0RGVtb0NvbnRhaW5lciB7XFxuICB3aWR0aDogODNweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmViZjA7XFxuICBwYWRkaW5nOiA4cHggMTZweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZsb2F0OiByaWdodDtcXG59ICovXFxuXFxuLyogLnJlcXVlc3REZW1vIHtcXG4vLyAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuLy8gICBmb250LXNpemU6IDE2cHg7XFxuLy8gICBsaW5lLWhlaWdodDogMS41O1xcbi8vICAgY29sb3I6ICNmMzY7XFxuLy8gfSAqL1xcblxcbi5IZWFkZXJWMi1zY2hlZHVsZURlbW8taF9DZVAge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogI2YzNjtcXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmMzY7XFxuICBwYWRkaW5nOiA4cHggMTJweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgYmFja2dyb3VuZDogbm9uZTtcXG4gIC1tcy1mbGV4LW9yZGVyOiAwO1xcbiAgICAgIG9yZGVyOiAwO1xcbn1cXG5cXG4uSGVhZGVyVjItc2NoZWR1bGVEZW1vLWhfQ2VQOmhvdmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDUxLCAxMDIpO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNXMgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiAwLjVzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogMC41cyBlYXNlLWluLW91dDtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhcldyYXBwZXItM01DNmMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICB0b3A6IDAlO1xcbiAgbGVmdDogMCU7XFxuICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG9wYWNpdHk6IDE7XFxuICB6LWluZGV4OiAyO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMjVweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICBwYWRkaW5nOiAxNnB4O1xcbiAgcGFkZGluZzogMXJlbTtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTcwMHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1pdGVtLWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xcbn1cXG5cXG4vKiAuZmVhdHVyZWl0ZW0gaW1nIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZWhlYWRpbmcgaW1nIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbn0gKi9cXG5cXG4uSGVhZGVyVjItbWVudWJ0bi0yTUpuYSB7XFxuICBjb2xvcjogIzAwMDtcXG4gIGZvbnQtc2l6ZTogMjIuNHB4O1xcbiAgZm9udC1zaXplOiAxLjRyZW07XFxuICBtYXJnaW4tbGVmdDogMDtcXG4gIC1tcy1mbGV4LW9yZGVyOiAyO1xcbiAgICAgIG9yZGVyOiAyO1xcbn1cXG5cXG4uSGVhZGVyVjItbWVudWJ0bi0yTUpuYSBpbWcge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLWxvZ28tMzRBSkkge1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgaGVpZ2h0OiAyLjVyZW07XFxuICB3aWR0aDogODBweDtcXG4gIHdpZHRoOiA1cmVtO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbG9nby0zNEFKSSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkgge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBsZWZ0OiAtMTAwJTtcXG4gIHRvcDogNjlweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICBwYWRkaW5nOiAyNHB4IDA7XFxuICBwYWRkaW5nOiAxLjVyZW0gMDtcXG4gIHBhZGRpbmctbGVmdDogMTZweDtcXG4gIHBhZGRpbmctbGVmdDogMXJlbTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XFxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xcbiAgLW8tdHJhbnNpdGlvbjogMC40cztcXG4gIHRyYW5zaXRpb246IDAuNHM7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgb3ZlcmZsb3c6IGF1dG87XFxuICB6LWluZGV4OiAxO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJILkhlYWRlclYyLWFjdGl2ZS12Wlg4NCB7XFxuICBsZWZ0OiAwJTtcXG59XFxuXFxuLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93biB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59ICovXFxuXFxuLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93bi5hY3RpdmVmZWF0dXJlcyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn0gKi9cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIC5IZWFkZXJWMi1jb21wYW55ZHJvcGRvd24tM2tHNEQge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIC5IZWFkZXJWMi1jb21wYW55ZHJvcGRvd24tM2tHNEQuSGVhZGVyVjItYWN0aXZlZHJvcGRvd25zLV9jdnc2IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgb3BhY2l0eTogMTtcXG4gIHZpc2liaWxpdHk6IHZpc2libGU7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggPiBsaSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC40cztcXG4gIG1hcmdpbjogMCAxMnB4IDI0cHg7XFxuICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHRyYW5zcGFyZW50O1xcbn1cXG5cXG4uSGVhZGVyVjItYnRuZ3JvdXAtMTczOG0ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4vKiBmZWF0dXJlaGVhZGluZyBzcGFuIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZWl0ZW0gc3BhbiB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG59ICovXFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCA+IGxpID4gc3BhbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xcbiAgcGFkZGluZzogMCAwIDI0cHggMDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIID4gbGkgPiBzcGFuID4gaW1nIHtcXG4gIG1hcmdpbi10b3A6IDNweDtcXG4gIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIID4gbGkgPiBhIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XFxuICBwYWRkaW5nOiAwIDAgMjRweCAwO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBvcGFjaXR5OiAwO1xcbiAgei1pbmRleDogMjtcXG59XFxuXFxuLkhlYWRlclYyLWFjdGl2ZWRyb3Bkb3ducy1fY3Z3NiB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaSxcXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIGxpIGEge1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwgbGkgYSB7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogMTJweCAwO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIGxpIGEgaW1nIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwgbGk6bnRoLWNoaWxkKDEpIGEge1xcbiAgcGFkZGluZzogMjRweCAwIDEycHggMDtcXG59XFxuXFxuLkhlYWRlclYyLW1lbnVidG4tMk1KbmEuSGVhZGVyVjItbWVudWFjdGl2ZS0xdWZ2MiB7XFxuICB6LWluZGV4OiAzO1xcbn1cXG5cXG4uSGVhZGVyVjItbG9naW4tMzFRR3Mge1xcbiAgcGFkZGluZzogOHB4IDEycHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIG1hcmdpbi1yaWdodDogMTJweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgYmFja2dyb3VuZDogI2ZmZWJmMDtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgb3JkZXI6IDE7XFxufVxcblxcbi8qIC5mZWF0dXJld3JhcHBlciB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHBhZGRpbmc6IDI0cHggMDtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMSk7XFxuXFxuICAuZmVhdHVyZSB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDYwJTtcXG5cXG4gICAgLmZlYXR1cmVpdGVtIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luOiAyNHB4IDA7XFxuICAgIH1cXG5cXG4gICAgLmZlYXR1cmVpdGVtOmxhc3QtY2hpbGQge1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG4gIH1cXG59ICovXFxuXFxuLyogLmZlYXR1cmV3cmFwcGVyOmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59ICovXFxuXFxuLyogLnZpZXdtb3Jld3JhcHBlciB7XFxuICBoZWlnaHQ6IDI3cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogOTVweDtcXG59ICovXFxuXFxuLyogLmZlYXR1cmVoZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn0gKi9cXG5cXG4vKiAudmlld21vcmUge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQ6IG5vbmU7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIHBhZGRpbmc6IDA7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDQwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59ICovXFxuXFxuLyogLnNlYXJjaC1pY29uIHtcXG4gIGZvbnQtc2l6ZTogMS40cmVtO1xcbiAgIG1hcmdpbi1sZWZ0OiA2cmVtO1xcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xcbiAgIGNvbG9yOiAjZmZmO1xcbiAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxuXFxuLmJ0bmdyb3VwIHtcXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLmxvZ2luIHtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcXG4gIC5uYXZiYXIge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW07XFxuICAgIHBhZGRpbmctcmlnaHQ6IDJyZW07XFxuICB9XFxufVxcbiovXFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTBweCkge1xcbiAgLyogLmJvZHl3cmFwcGVyIHtcXG4gICAgcGFkZGluZy10b3A6IDA7XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZWhlYWRpbmcgaW1nIHtcXG4gICAgd2lkdGg6IDI0cHg7XFxuICAgIGhlaWdodDogMjRweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XFxuICB9ICovXFxuXFxuICAuSGVhZGVyVjItc2NoZWR1bGVEZW1vLWhfQ2VQIHtcXG4gICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAwO1xcbiAgICAgICAgb3JkZXI6IDA7XFxuICAgIG1hcmdpbjogMCAxMnB4O1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLWxvZ2luLTMxUUdzIHtcXG4gICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAxO1xcbiAgICAgICAgb3JkZXI6IDE7XFxuICAgIG1hcmdpbjogMCAxMnB4O1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhcldyYXBwZXItM01DNmMge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgcGFkZGluZzogMTZweCA2NHB4O1xcbiAgICBwb3NpdGlvbjogZml4ZWQ7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogMTcwMHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgfVxcblxcbiAgICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1sb2dvLTM0QUpJIHtcXG4gICAgICB3aWR0aDogNi4xMjVyZW07XFxuICAgICAgaGVpZ2h0OiAzcmVtO1xcbiAgICB9XFxuXFxuICAgICAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbG9nby0zNEFKSSBpbWcge1xcbiAgICAgICAgd2lkdGg6IDYuMTI1cmVtO1xcbiAgICAgICAgaGVpZ2h0OiAzcmVtO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCB7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgdG9wOiAwO1xcbiAgICBsZWZ0OiAwO1xcbiAgICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogLW1vei1maXQtY29udGVudDtcXG4gICAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICAvL21hcmdpbi1yaWdodDogMzBweDtcXG4gICAgb3ZlcmZsb3c6IGluaXRpYWw7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHtcXG4gICAgYm9yZGVyOiBub25lO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLyogLmZlYXR1cmVoZWFkaW5nIHNwYW4ge1xcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfSAqL1xcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCA+IGxpID4gc3BhbiB7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSBhIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzAwMDtcXG4gICAgb3BhY2l0eTogMC42O1xcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgLkhlYWRlclYyLWNvbXBhbnlkcm9wZG93bi0za0c0RCB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiAwJTtcXG4gICAgcmlnaHQ6IDA7XFxuICAgIHdpZHRoOiAxOTBweDtcXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgbWFyZ2luLXRvcDogMzBweDtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgIGJhY2tncm91bmQ6ICNmZmY7XFxuICAgIG9wYWNpdHk6IDA7XFxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIH1cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwgbGkge1xcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgIG1hcmdpbjogNHB4O1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgcGFkZGluZzogMCAxMnB4O1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaTpob3ZlciB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjM2LCA3NiwgMTExLCAwLjEpO1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaTpudGgtY2hpbGQoMSkgYSB7XFxuICAgIHBhZGRpbmctdG9wOiAxMnB4O1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSAuSGVhZGVyVjItY29tcGFueWRyb3Bkb3duLTNrRzRELkhlYWRlclYyLWFjdGl2ZWRyb3Bkb3ducy1fY3Z3NiB7XFxuICAgIG9wYWNpdHk6IDE7XFxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICB9XFxuXFxuICAvKiAuZmVhdHVyZXNkcm9wZG93biB7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgZmxleC13cmFwOiB3cmFwO1xcbiAgfSAqL1xcblxcbiAgLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93biB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA0MCU7XFxuICAgIGxlZnQ6IC0xNTAlO1xcbiAgICB3aWR0aDogODg4cHg7XFxuICAgIGhlaWdodDogMzU2cHg7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC8vIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gICAgb3BhY2l0eTogMDtcXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xcbiAgICBwYWRkaW5nOiA0MnB4O1xcbiAgICAvL2JvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICB9ICovXFxuXFxuICAvKiAubmF2YmFyIC5tZW51IGxpIC5mZWF0dXJlc2Ryb3Bkb3duLmFjdGl2ZWZlYXR1cmVzIHtcXG4gICAgb3BhY2l0eTogMTtcXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIH0gKi9cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgPiBzcGFuIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzAwMDtcXG4gICAgb3BhY2l0eTogMC42O1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLWZlYXR1cmVOYW1lLTNsLUowOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIID4gbGkgPiBzcGFuID4gLkhlYWRlclYyLWZlYXR1cmVOYW1lLTNsLUowOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAgIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgfVxcblxcbiAgLyogLmZlYXR1cmV3cmFwcGVyIHtcXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1heC13aWR0aDogMTU2cHg7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIG1hcmdpbi1yaWdodDogMzFweDtcXG5cXG4gICAgLmZlYXR1cmUge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAzMXB4O1xcbiAgICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYigzNywgNDAsIDQzLCAwLjEpO1xcbiAgICB9XFxuICB9XFxuXFxuICAuZmVhdHVyZXdyYXBwZXI6bnRoLWNoaWxkKDIpIHtcXG4gICAgLmZlYXR1cmUge1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDQ4cHg7XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC8qIC5mZWF0dXJld3JhcHBlcjpsYXN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcblxcbiAgICAuZmVhdHVyZSB7XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC8qIC52aWV3bW9yZXdyYXBwZXIge1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gICAgLy9tYXJnaW4tdG9wOiAtMTVweDtcXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMTBweDtcXG5cXG4gICAgLnZpZXdtb3JlIHtcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZWl0ZW0ge1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIG1hcmdpbjogMTVweCAwO1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICB9XFxuXFxuICAuZmVhdHVyZWl0ZW06aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gICAgdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiAjMjUyODJiO1xcbiAgfSAqL1xcblxcbiAgLyogLmZlYXR1cmVoZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICB9ICovXFxuXFxuICAvKiAubmF2YmFyIC5tZW51IGxpOmhvdmVyIC5mZWF0dXJlc2Ryb3Bkb3duIHtcXG4gIC8vICAgb3BhY2l0eTogMTtcXG4gIC8vICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gIC8vIH0gKi9cXG4gIC5IZWFkZXJWMi1tZW51YnRuLTJNSm5hIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9IZWFkZXJWMi9IZWFkZXJWMi5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBOzs7Ozs7SUFNSTs7QUFFSjs7O0lBR0k7O0FBRUo7Ozs7Ozs7O0lBUUk7O0FBRUo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF5Qkk7O0FBRUo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTJCSTs7QUFFSjs7Ozs7T0FLTzs7QUFFUDtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO01BQ2QsU0FBUztDQUNkOztBQUVEO0VBQ0Usb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixxQ0FBcUM7RUFDckMsZ0NBQWdDO0VBQ2hDLDZCQUE2QjtDQUM5Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixRQUFRO0VBQ1IsU0FBUztFQUNULDRCQUE0QjtFQUM1Qix5QkFBeUI7RUFDekIsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsV0FBVztFQUNYLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxjQUFjO0VBQ2QsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4Qiw0QkFBNEI7TUFDeEIsbUJBQW1CO0NBQ3hCOztBQUVEOzs7SUFHSTs7QUFFSjs7O0lBR0k7O0FBRUo7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO01BQ2QsU0FBUztDQUNkOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2YsWUFBWTtFQUNaLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIseUNBQXlDO0NBQzFDOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLFVBQVU7RUFDVixZQUFZO0VBQ1osY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQix5QkFBeUI7RUFDekIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLFdBQVc7Q0FDWjs7QUFFRDtFQUNFLFNBQVM7Q0FDVjs7QUFFRDs7O0lBR0k7O0FBRUo7O0lBRUk7O0FBRUo7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3Qix3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQix5Q0FBeUM7Q0FDMUM7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7Ozs7SUFJSTs7QUFFSjs7Ozs7SUFLSTs7QUFFSjtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyw4QkFBOEI7RUFDOUIsb0JBQW9CO0VBQ3BCLFlBQVk7RUFDWixhQUFhO0VBQ2IsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYiw4QkFBOEI7RUFDOUIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsV0FBVztFQUNYLFdBQVc7Q0FDWjs7QUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7O0VBRUUsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLFlBQVk7RUFDWix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2Isa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsV0FBVztDQUNaOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLFlBQVk7RUFDWixrQkFBa0I7TUFDZCxTQUFTO0NBQ2Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFzQkk7O0FBRUo7O0lBRUk7O0FBRUo7Ozs7SUFJSTs7QUFFSjs7O0lBR0k7O0FBRUo7Ozs7Ozs7Ozs7OztJQVlJOztBQUVKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUF3QkU7O0FBRUY7RUFDRTs7TUFFSTs7RUFFSjs7OztNQUlJOztFQUVKO0lBQ0Usa0JBQWtCO0lBQ2xCLGtCQUFrQjtRQUNkLFNBQVM7SUFDYixlQUFlO0dBQ2hCOztFQUVEO0lBQ0Usa0JBQWtCO0lBQ2xCLGtCQUFrQjtRQUNkLFNBQVM7SUFDYixlQUFlO0dBQ2hCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLHFEQUFxRDtZQUM3Qyw2Q0FBNkM7SUFDckQsbUJBQW1CO0lBQ25CLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0dBQ2Y7O0lBRUM7TUFDRSxnQkFBZ0I7TUFDaEIsYUFBYTtLQUNkOztNQUVDO1FBQ0UsZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYix1QkFBdUI7V0FDcEIsb0JBQW9CO09BQ3hCOztFQUVMO0lBQ0UsbUJBQW1CO0lBQ25CLE9BQU87SUFDUCxRQUFRO0lBQ1IsNEJBQTRCO0lBQzVCLHlCQUF5QjtJQUN6QixvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixtQkFBbUI7UUFDZiwwQkFBMEI7SUFDOUIsV0FBVztJQUNYLHFCQUFxQjtJQUNyQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsV0FBVztJQUNYLGlCQUFpQjtHQUNsQjs7RUFFRDs7O01BR0k7O0VBRUo7SUFDRSxvQkFBb0I7SUFDcEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixhQUFhO0lBQ2Isb0JBQW9CO0lBQ3BCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixRQUFRO0lBQ1IsU0FBUztJQUNULGFBQWE7SUFDYixtQkFBbUI7SUFDbkIscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsV0FBVztJQUNYLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7SUFDWixnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSwwQ0FBMEM7R0FDM0M7O0VBRUQ7SUFDRSxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxXQUFXO0lBQ1gsb0JBQW9CO0dBQ3JCOztFQUVEOzs7TUFHSTs7RUFFSjs7Ozs7Ozs7Ozs7Ozs7OztNQWdCSTs7RUFFSjs7OztNQUlJOztFQUVKO0lBQ0UsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSwyQkFBMkI7R0FDNUI7O0VBRUQ7SUFDRSwyQkFBMkI7SUFDM0Isa0RBQWtEO1lBQzFDLDBDQUEwQztJQUNsRCwrQkFBK0I7WUFDdkIsdUJBQXVCO0dBQ2hDOztFQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQW9CSTs7RUFFSjs7Ozs7OztNQU9JOztFQUVKOzs7Ozs7Ozs7TUFTSTs7RUFFSjs7Ozs7Ozs7Ozs7TUFXSTs7RUFFSjs7OztNQUlJOztFQUVKOzs7U0FHTztFQUNQO0lBQ0UsY0FBYztHQUNmO0NBQ0ZcIixcImZpbGVcIjpcIkhlYWRlclYyLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLyogLnJvb3Qge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcXG59ICovXFxuXFxuLyogLmJvZHl3cmFwcGVyIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHBhZGRpbmctdG9wOiA3MHB4O1xcbn0gKi9cXG5cXG4vKlxcbi5uYXZiYXJ7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBtaW4taGVpZ2h0OiA3NXB4O1xcbiAgcGFkZGluZzogMnJlbSA0cmVtO1xcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcblxcbn0gKi9cXG5cXG4vKiAubmF2YmFyLWNvbGxhcHNlIHtcXG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xcbiAgZmxleC1ncm93OiAwO1xcbn1cXG4uZHJvcGRvd24tdG9nZ2xlOjphZnRlciB7XFxuICBkaXNwbGF5Om5vbmVcXG59XFxuLmRyb3Bkb3duLW1lbnUge1xcbiAgYm9yZGVyOiBub25lO1xcblxcbn1cXG4ubmF2YmFyLXRvZ2dsZXIge1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG4ubmF2YmFyLXRvZ2dsZXI6Zm9jdXMge1xcbiAgYm94LXNoYWRvdzogbm9uZTtcXG59XFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogOTkycHgpIHtcXG4gIC5uYXZiYXItZXhwYW5kLWxnIHtcXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgICBwYWRkaW5nOjFyZW0gNHJlbTtcXG4gIH1cXG4gIC5uYXZiYXItY29sbGFwc2Uge1xcbiAgICBtaW4taGVpZ2h0OiBhdXRvO1xcbiAgfVxcbn0gKi9cXG5cXG4vKiAuaGVhZGVyUm9vdCB7XFxuICBoZWlnaHQ6IDg4cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDI3cHggNSU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbn1cXG5cXG4ubGVmdCB7XFxuICBoZWlnaHQ6IDUxcHg7XFxuICB3aWR0aDogMTA1cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4ucmlnaHQge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWFyZ2luLXJpZ2h0OiAxNHB4O1xcbn1cXG5cXG4ucmVxdWVzdERlbW9Db250YWluZXIge1xcbiAgd2lkdGg6IDgzcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbiAgcGFkZGluZzogOHB4IDE2cHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBmbG9hdDogcmlnaHQ7XFxufSAqL1xcblxcbi8qIC5yZXF1ZXN0RGVtbyB7XFxuLy8gICBmb250LXdlaWdodDogNjAwO1xcbi8vICAgZm9udC1zaXplOiAxNnB4O1xcbi8vICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4vLyAgIGNvbG9yOiAjZjM2O1xcbi8vIH0gKi9cXG5cXG4uc2NoZWR1bGVEZW1vIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICNmMzY7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjZjM2O1xcbiAgcGFkZGluZzogOHB4IDEycHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xcbiAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1tYXgtY29udGVudDtcXG4gIHdpZHRoOiBtYXgtY29udGVudDtcXG4gIG1hcmdpbi1yaWdodDogMTJweDtcXG4gIGJhY2tncm91bmQ6IG5vbmU7XFxuICAtbXMtZmxleC1vcmRlcjogMDtcXG4gICAgICBvcmRlcjogMDtcXG59XFxuXFxuLnNjaGVkdWxlRGVtbzpob3ZlciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCA1MSwgMTAyKTtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjVzIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogMC41cyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IDAuNXMgZWFzZS1pbi1vdXQ7XFxufVxcblxcbi5uYXZiYXJXcmFwcGVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgdG9wOiAwJTtcXG4gIGxlZnQ6IDAlO1xcbiAgaGVpZ2h0OiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiAtbW96LWZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBvcGFjaXR5OiAxO1xcbiAgei1pbmRleDogMjtcXG4gIG1hcmdpbjogYXV0bztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMjVweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgcGFkZGluZzogMTZweDtcXG4gIHBhZGRpbmc6IDFyZW07XFxufVxcblxcbi5uYXZiYXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtaXRlbS1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcXG59XFxuXFxuLyogLmZlYXR1cmVpdGVtIGltZyB7XFxuICB3aWR0aDogMjBweDtcXG4gIGhlaWdodDogMjBweDtcXG59ICovXFxuXFxuLyogLmZlYXR1cmVoZWFkaW5nIGltZyB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG59ICovXFxuXFxuLm1lbnVidG4ge1xcbiAgY29sb3I6ICMwMDA7XFxuICBmb250LXNpemU6IDIyLjRweDtcXG4gIGZvbnQtc2l6ZTogMS40cmVtO1xcbiAgbWFyZ2luLWxlZnQ6IDA7XFxuICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICBvcmRlcjogMjtcXG59XFxuXFxuLm1lbnVidG4gaW1nIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH1cXG5cXG4ubmF2YmFyIC5sb2dvIHtcXG4gIGhlaWdodDogNDBweDtcXG4gIGhlaWdodDogMi41cmVtO1xcbiAgd2lkdGg6IDgwcHg7XFxuICB3aWR0aDogNXJlbTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxufVxcblxcbi5uYXZiYXIgLmxvZ28gaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4ubmF2YmFyIC5tZW51IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgbGVmdDogLTEwMCU7XFxuICB0b3A6IDY5cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwdmg7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbiAgcGFkZGluZzogMjRweCAwO1xcbiAgcGFkZGluZzogMS41cmVtIDA7XFxuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDFyZW07XFxuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcXG4gIC1vLXRyYW5zaXRpb246IDAuNHM7XFxuICB0cmFuc2l0aW9uOiAwLjRzO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG92ZXJmbG93OiBhdXRvO1xcbiAgei1pbmRleDogMTtcXG59XFxuXFxuLm5hdmJhciAubWVudS5hY3RpdmUge1xcbiAgbGVmdDogMCU7XFxufVxcblxcbi8qIC5uYXZiYXIgLm1lbnUgbGkgLmZlYXR1cmVzZHJvcGRvd24ge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufSAqL1xcblxcbi8qIC5uYXZiYXIgLm1lbnUgbGkgLmZlYXR1cmVzZHJvcGRvd24uYWN0aXZlZmVhdHVyZXMge1xcbiAgZGlzcGxheTogZmxleDtcXG59ICovXFxuXFxuLm5hdmJhciAubWVudSBsaSAuY29tcGFueWRyb3Bkb3duIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBtYXJnaW4tbGVmdDogMTBweDtcXG59XFxuXFxuLm5hdmJhciAubWVudSBsaSAuY29tcGFueWRyb3Bkb3duLmFjdGl2ZWRyb3Bkb3ducyB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG9wYWNpdHk6IDE7XFxuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51ID4gbGkge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC40cztcXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICBtYXJnaW46IDAgMTJweCAyNHB4O1xcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcXG59XFxuXFxuLmJ0bmdyb3VwIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLyogZmVhdHVyZWhlYWRpbmcgc3BhbiB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59ICovXFxuXFxuLyogLmZlYXR1cmVpdGVtIHNwYW4ge1xcbiAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBmb250LXNpemU6IDE0cHg7XFxufSAqL1xcblxcbi5uYXZiYXIgLm1lbnUgPiBsaSA+IHNwYW4ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcXG4gIHBhZGRpbmc6IDAgMCAyNHB4IDA7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG59XFxuXFxuLm5hdmJhciAubWVudSA+IGxpID4gc3BhbiA+IGltZyB7XFxuICBtYXJnaW4tdG9wOiAzcHg7XFxuICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59XFxuXFxuLm5hdmJhciAubWVudSA+IGxpID4gYSB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xcbiAgcGFkZGluZzogMCAwIDI0cHggMDtcXG59XFxuXFxuLm5hdmJhciAubWVudSBsaSB1bCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgb3BhY2l0eTogMDtcXG4gIHotaW5kZXg6IDI7XFxufVxcblxcbi5hY3RpdmVkcm9wZG93bnMge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcblxcbi5uYXZiYXIgLm1lbnUgbGkgdWwgbGksXFxuLm5hdmJhciAubWVudSBsaSB1bCBsaSBhIHtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51IGxpIHVsIGxpIGEge1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB3aWR0aDogMTAwJTtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDEycHggMDtcXG59XFxuXFxuLm5hdmJhciAubWVudSBsaSB1bCBsaSBhIGltZyB7XFxuICB3aWR0aDogMjBweDtcXG4gIGhlaWdodDogMjBweDtcXG4gIG1hcmdpbi1yaWdodDogOHB4O1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51IGxpIHVsIGxpOm50aC1jaGlsZCgxKSBhIHtcXG4gIHBhZGRpbmc6IDI0cHggMCAxMnB4IDA7XFxufVxcblxcbi5tZW51YnRuLm1lbnVhY3RpdmUge1xcbiAgei1pbmRleDogMztcXG59XFxuXFxuLmxvZ2luIHtcXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGJhY2tncm91bmQ6ICNmZmViZjA7XFxuICBjb2xvcjogI2YzNjtcXG4gIC1tcy1mbGV4LW9yZGVyOiAxO1xcbiAgICAgIG9yZGVyOiAxO1xcbn1cXG5cXG4vKiAuZmVhdHVyZXdyYXBwZXIge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBwYWRkaW5nOiAyNHB4IDA7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgzNywgNDAsIDQzLCAwLjEpO1xcblxcbiAgLmZlYXR1cmUge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHdpZHRoOiA2MCU7XFxuXFxuICAgIC5mZWF0dXJlaXRlbSB7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbjogMjRweCAwO1xcbiAgICB9XFxuXFxuICAgIC5mZWF0dXJlaXRlbTpsYXN0LWNoaWxkIHtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICB9XFxuICB9XFxufSAqL1xcblxcbi8qIC5mZWF0dXJld3JhcHBlcjpsYXN0LWNoaWxkIHtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxufSAqL1xcblxcbi8qIC52aWV3bW9yZXdyYXBwZXIge1xcbiAgaGVpZ2h0OiAyN3B4O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDk1cHg7XFxufSAqL1xcblxcbi8qIC5mZWF0dXJlaGVhZGluZyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59ICovXFxuXFxuLyogLnZpZXdtb3JlIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kOiBub25lO1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICBwYWRkaW5nOiAwO1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufSAqL1xcblxcbi8qIC5zZWFyY2gtaWNvbiB7XFxuICBmb250LXNpemU6IDEuNHJlbTtcXG4gICBtYXJnaW4tbGVmdDogNnJlbTtcXG4gIG1hcmdpbi1yaWdodDogMTVweDtcXG4gICBjb2xvcjogI2ZmZjtcXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcblxcbi5idG5ncm91cCB7XFxuICBtYXJnaW4tbGVmdDogMjBweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5sb2dpbiB7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XFxuICAubmF2YmFyIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAycmVtO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAycmVtO1xcbiAgfVxcbn1cXG4qL1xcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkwcHgpIHtcXG4gIC8qIC5ib2R5d3JhcHBlciB7XFxuICAgIHBhZGRpbmctdG9wOiAwO1xcbiAgfSAqL1xcblxcbiAgLyogLmZlYXR1cmVoZWFkaW5nIGltZyB7XFxuICAgIHdpZHRoOiAyNHB4O1xcbiAgICBoZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xcbiAgfSAqL1xcblxcbiAgLnNjaGVkdWxlRGVtbyB7XFxuICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAtbXMtZmxleC1vcmRlcjogMDtcXG4gICAgICAgIG9yZGVyOiAwO1xcbiAgICBtYXJnaW46IDAgMTJweDtcXG4gIH1cXG5cXG4gIC5sb2dpbiB7XFxuICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICAgIG9yZGVyOiAxO1xcbiAgICBtYXJnaW46IDAgMTJweDtcXG4gIH1cXG5cXG4gIC5uYXZiYXJXcmFwcGVyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMjVweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMjVweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgIHBhZGRpbmc6IDE2cHggNjRweDtcXG4gICAgcG9zaXRpb246IGZpeGVkO1xcbiAgfVxcblxcbiAgLm5hdmJhciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gIH1cXG5cXG4gICAgLm5hdmJhciAubG9nbyB7XFxuICAgICAgd2lkdGg6IDYuMTI1cmVtO1xcbiAgICAgIGhlaWdodDogM3JlbTtcXG4gICAgfVxcblxcbiAgICAgIC5uYXZiYXIgLmxvZ28gaW1nIHtcXG4gICAgICAgIHdpZHRoOiA2LjEyNXJlbTtcXG4gICAgICAgIGhlaWdodDogM3JlbTtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUge1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIHRvcDogMDtcXG4gICAgbGVmdDogMDtcXG4gICAgaGVpZ2h0OiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICAgIGJhY2tncm91bmQ6IG5vbmU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgLy9tYXJnaW4tcmlnaHQ6IDMwcHg7XFxuICAgIG92ZXJmbG93OiBpbml0aWFsO1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSBsaSB7XFxuICAgIGJvcmRlcjogbm9uZTtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC8qIC5mZWF0dXJlaGVhZGluZyBzcGFuIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgbWFyZ2luLXRvcDogMDtcXG4gIH0gKi9cXG5cXG4gIC5uYXZiYXIgLm1lbnUgPiBsaSA+IHNwYW4ge1xcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgYSB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICMwMDA7XFxuICAgIG9wYWNpdHk6IDAuNjtcXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gICAgcGFkZGluZy1ib3R0b206IDA7XFxuICB9XFxuXFxuICAubmF2YmFyIC5tZW51IGxpIC5jb21wYW55ZHJvcGRvd24ge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogMCU7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICB3aWR0aDogMTkwcHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIG1hcmdpbi10b3A6IDMwcHg7XFxuICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgICBvcGFjaXR5OiAwO1xcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuICAubmF2YmFyIC5tZW51IGxpIHVsIGxpIHtcXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICBtYXJnaW46IDRweDtcXG4gICAgd2lkdGg6IGF1dG87XFxuICAgIHBhZGRpbmc6IDAgMTJweDtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgdWwgbGk6aG92ZXIge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIzNiwgNzYsIDExMSwgMC4xKTtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgdWwgbGk6bnRoLWNoaWxkKDEpIGEge1xcbiAgICBwYWRkaW5nLXRvcDogMTJweDtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgLmNvbXBhbnlkcm9wZG93bi5hY3RpdmVkcm9wZG93bnMge1xcbiAgICBvcGFjaXR5OiAxO1xcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xcbiAgfVxcblxcbiAgLyogLmZlYXR1cmVzZHJvcGRvd24ge1xcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIH0gKi9cXG5cXG4gIC8qIC5uYXZiYXIgLm1lbnUgbGkgLmZlYXR1cmVzZHJvcGRvd24ge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogNDAlO1xcbiAgICBsZWZ0OiAtMTUwJTtcXG4gICAgd2lkdGg6IDg4OHB4O1xcbiAgICBoZWlnaHQ6IDM1NnB4O1xcbiAgICBsaXN0LXN0eWxlOiBub25lO1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAgIG9wYWNpdHk6IDA7XFxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgbWFyZ2luLXRvcDogMzBweDtcXG4gICAgcGFkZGluZzogNDJweDtcXG4gICAgLy9ib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgfSAqL1xcblxcbiAgLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93bi5hY3RpdmVmZWF0dXJlcyB7XFxuICAgIG9wYWNpdHk6IDE7XFxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICB9ICovXFxuXFxuICAubmF2YmFyIC5tZW51IGxpID4gc3BhbiB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICMwMDA7XFxuICAgIG9wYWNpdHk6IDAuNjtcXG4gIH1cXG5cXG4gIC5mZWF0dXJlTmFtZTpob3ZlciB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSA+IGxpID4gc3BhbiA+IC5mZWF0dXJlTmFtZTpob3ZlciB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgICAtd2Via2l0LXRleHQtZGVjb3JhdGlvbi1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjYpO1xcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbi1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjYpO1xcbiAgICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIH1cXG5cXG4gIC8qIC5mZWF0dXJld3JhcHBlciB7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXgtd2lkdGg6IDE1NnB4O1xcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICBtYXJnaW4tcmlnaHQ6IDMxcHg7XFxuXFxuICAgIC5mZWF0dXJlIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgcGFkZGluZy1yaWdodDogMzFweDtcXG4gICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2IoMzcsIDQwLCA0MywgMC4xKTtcXG4gICAgfVxcbiAgfVxcblxcbiAgLmZlYXR1cmV3cmFwcGVyOm50aC1jaGlsZCgyKSB7XFxuICAgIC5mZWF0dXJlIHtcXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA0OHB4O1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZXdyYXBwZXI6bGFzdC1jaGlsZCB7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG5cXG4gICAgLmZlYXR1cmUge1xcbiAgICAgIHBhZGRpbmc6IDA7XFxuICAgICAgYm9yZGVyLXJpZ2h0OiBub25lO1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAvKiAudmlld21vcmV3cmFwcGVyIHtcXG4gICAgd2lkdGg6IGF1dG87XFxuICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICAgIC8vbWFyZ2luLXRvcDogLTE1cHg7XFxuICAgIC8vcGFkZGluZy1ib3R0b206IDEwcHg7XFxuXFxuICAgIC52aWV3bW9yZSB7XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLyogLmZlYXR1cmVpdGVtIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICBtYXJnaW46IDE1cHggMDtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgfVxcblxcbiAgLmZlYXR1cmVpdGVtOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAgIHRleHQtZGVjb3JhdGlvbi1jb2xvcjogIzI1MjgyYjtcXG4gIH0gKi9cXG5cXG4gIC8qIC5mZWF0dXJlaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgfSAqL1xcblxcbiAgLyogLm5hdmJhciAubWVudSBsaTpob3ZlciAuZmVhdHVyZXNkcm9wZG93biB7XFxuICAvLyAgIG9wYWNpdHk6IDE7XFxuICAvLyAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICAvLyB9ICovXFxuICAubWVudWJ0biB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJzY2hlZHVsZURlbW9cIjogXCJIZWFkZXJWMi1zY2hlZHVsZURlbW8taF9DZVBcIixcblx0XCJuYXZiYXJXcmFwcGVyXCI6IFwiSGVhZGVyVjItbmF2YmFyV3JhcHBlci0zTUM2Y1wiLFxuXHRcIm5hdmJhclwiOiBcIkhlYWRlclYyLW5hdmJhci0xWmRVS1wiLFxuXHRcIm1lbnVidG5cIjogXCJIZWFkZXJWMi1tZW51YnRuLTJNSm5hXCIsXG5cdFwibG9nb1wiOiBcIkhlYWRlclYyLWxvZ28tMzRBSklcIixcblx0XCJtZW51XCI6IFwiSGVhZGVyVjItbWVudS0zdFZiSFwiLFxuXHRcImFjdGl2ZVwiOiBcIkhlYWRlclYyLWFjdGl2ZS12Wlg4NFwiLFxuXHRcImNvbXBhbnlkcm9wZG93blwiOiBcIkhlYWRlclYyLWNvbXBhbnlkcm9wZG93bi0za0c0RFwiLFxuXHRcImFjdGl2ZWRyb3Bkb3duc1wiOiBcIkhlYWRlclYyLWFjdGl2ZWRyb3Bkb3ducy1fY3Z3NlwiLFxuXHRcImJ0bmdyb3VwXCI6IFwiSGVhZGVyVjItYnRuZ3JvdXAtMTczOG1cIixcblx0XCJtZW51YWN0aXZlXCI6IFwiSGVhZGVyVjItbWVudWFjdGl2ZS0xdWZ2MlwiLFxuXHRcImxvZ2luXCI6IFwiSGVhZGVyVjItbG9naW4tMzFRR3NcIixcblx0XCJmZWF0dXJlTmFtZVwiOiBcIkhlYWRlclYyLWZlYXR1cmVOYW1lLTNsLUowXCJcbn07IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qXFxuICogQmFzZSBzdHlsZXNcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbi8qXFxuICogUmVtb3ZlIHRleHQtc2hhZG93IGluIHNlbGVjdGlvbiBoaWdobGlnaHQ6XFxuICogaHR0cHM6Ly90d2l0dGVyLmNvbS9taWtldGF5bHIvc3RhdHVzLzEyMjI4ODA1MzAxXFxuICpcXG4gKiBUaGVzZSBzZWxlY3Rpb24gcnVsZSBzZXRzIGhhdmUgdG8gYmUgc2VwYXJhdGUuXFxuICogQ3VzdG9taXplIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIHRvIG1hdGNoIHlvdXIgZGVzaWduLlxcbiAqL1xcblxcbjo6LW1vei1zZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lO1xcbn1cXG5cXG46OnNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7XFxufVxcblxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcblxcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwO1xcbn1cXG5cXG4vKlxcbiAqIFJlbW92ZSB0aGUgZ2FwIGJldHdlZW4gYXVkaW8sIGNhbnZhcywgaWZyYW1lcyxcXG4gKiBpbWFnZXMsIHZpZGVvcyBhbmQgdGhlIGJvdHRvbSBvZiB0aGVpciBjb250YWluZXJzOlxcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9oNWJwL2h0bWw1LWJvaWxlcnBsYXRlL2lzc3Vlcy80NDBcXG4gKi9cXG5cXG5hdWRpbyxcXG5jYW52YXMsXFxuaWZyYW1lLFxcbmltZyxcXG5zdmcsXFxudmlkZW8ge1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcXG59XFxuXFxuLypcXG4gKiBSZW1vdmUgZGVmYXVsdCBmaWVsZHNldCBzdHlsZXMuXFxuICovXFxuXFxuZmllbGRzZXQge1xcbiAgYm9yZGVyOiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDtcXG59XFxuXFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG5cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsO1xcbn1cXG5cXG4uTGF5b3V0LWJvZHktZGFWQ0Mge1xcbiAgbWFyZ2luLXRvcDogNzdweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0LmNzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTs7Z0ZBRWdGOztBQUVoRjs7Ozs7O0dBTUc7O0FBRUg7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtDQUNuQjs7QUFFRDs7R0FFRzs7QUFFSDtFQUNFLGVBQWU7RUFDZixZQUFZO0VBQ1osVUFBVTtFQUNWLDJCQUEyQjtFQUMzQixjQUFjO0VBQ2QsV0FBVztDQUNaOztBQUVEOzs7O0dBSUc7O0FBRUg7Ozs7OztFQU1FLHVCQUF1QjtDQUN4Qjs7QUFFRDs7R0FFRzs7QUFFSDtFQUNFLFVBQVU7RUFDVixVQUFVO0VBQ1YsV0FBVztDQUNaOztBQUVEOztHQUVHOztBQUVIO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCXCIsXCJmaWxlXCI6XCJMYXlvdXQuY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi8qXFxuICogQmFzZSBzdHlsZXNcXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xcblxcbi8qXFxuICogUmVtb3ZlIHRleHQtc2hhZG93IGluIHNlbGVjdGlvbiBoaWdobGlnaHQ6XFxuICogaHR0cHM6Ly90d2l0dGVyLmNvbS9taWtldGF5bHIvc3RhdHVzLzEyMjI4ODA1MzAxXFxuICpcXG4gKiBUaGVzZSBzZWxlY3Rpb24gcnVsZSBzZXRzIGhhdmUgdG8gYmUgc2VwYXJhdGUuXFxuICogQ3VzdG9taXplIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIHRvIG1hdGNoIHlvdXIgZGVzaWduLlxcbiAqL1xcblxcbjo6LW1vei1zZWxlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogI2IzZDRmYztcXG4gIHRleHQtc2hhZG93OiBub25lO1xcbn1cXG5cXG46OnNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7XFxufVxcblxcbi8qXFxuICogQSBiZXR0ZXIgbG9va2luZyBkZWZhdWx0IGhvcml6b250YWwgcnVsZVxcbiAqL1xcblxcbmhyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBib3JkZXI6IDA7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcXG4gIG1hcmdpbjogMWVtIDA7XFxuICBwYWRkaW5nOiAwO1xcbn1cXG5cXG4vKlxcbiAqIFJlbW92ZSB0aGUgZ2FwIGJldHdlZW4gYXVkaW8sIGNhbnZhcywgaWZyYW1lcyxcXG4gKiBpbWFnZXMsIHZpZGVvcyBhbmQgdGhlIGJvdHRvbSBvZiB0aGVpciBjb250YWluZXJzOlxcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9oNWJwL2h0bWw1LWJvaWxlcnBsYXRlL2lzc3Vlcy80NDBcXG4gKi9cXG5cXG5hdWRpbyxcXG5jYW52YXMsXFxuaWZyYW1lLFxcbmltZyxcXG5zdmcsXFxudmlkZW8ge1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcXG59XFxuXFxuLypcXG4gKiBSZW1vdmUgZGVmYXVsdCBmaWVsZHNldCBzdHlsZXMuXFxuICovXFxuXFxuZmllbGRzZXQge1xcbiAgYm9yZGVyOiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDtcXG59XFxuXFxuLypcXG4gKiBBbGxvdyBvbmx5IHZlcnRpY2FsIHJlc2l6aW5nIG9mIHRleHRhcmVhcy5cXG4gKi9cXG5cXG50ZXh0YXJlYSB7XFxuICByZXNpemU6IHZlcnRpY2FsO1xcbn1cXG5cXG4uYm9keSB7XFxuICBtYXJnaW4tdG9wOiA3N3B4O1xcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwiYm9keVwiOiBcIkxheW91dC1ib2R5LWRhVkNDXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgcyBmcm9tICcuL0Zvb3Rlci5zY3NzJztcblxuY29uc3QgRk9PVEVSX0xJTktTID0gW1xuICAvKiB7XG4gICAgdGl0bGU6ICdDb21wYW55JyxcbiAgICBsaW5rczogW1xuICAgICAgeyBsYWJlbDogJ0Fib3V0IFVzJywgdXJsOiAnL2Fib3V0LXVzJyB9LFxuICAgICAgeyBsYWJlbDogJ1RlYW0nLCB1cmw6ICcvdGVhbScgfSxcbiAgICAgIHsgbGFiZWw6ICdDdWx0dXJlJywgdXJsOiAnL2N1bHR1cmUnIH0sXG4gICAgICB7IGxhYmVsOiAnQ2FyZWVycycsIHVybDogJy9jYXJlZXJzJyB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ1Byb2R1Y3QnLFxuICAgIGxpbmtzOiBbXG4gICAgICB7IGxhYmVsOiAnR2V0UmFua3MnLCB1cmw6ICcvcHJvZHVjdHMvamVldCcgfSxcbiAgICAgIHsgbGFiZWw6ICdBQ0FEUycsIHVybDogJy9wcm9kdWN0cy9hY2FkcycgfSxcbiAgICAgIHsgbGFiZWw6ICdQcmVwJywgdXJsOiAnL3Byb2R1Y3RzL3ByZXAnIH0sXG4gICAgICB7IGxhYmVsOiAnUU1TJywgdXJsOiAnL3Byb2R1Y3RzL3FtcycgfSxcbiAgICBdLFxuICB9LCAqL1xuICB7XG4gICAgdGl0bGU6ICdMaW5rcycsXG4gICAgbGlua3M6IFtcbiAgICAgIHsgbGFiZWw6ICdBYm91dCBVcycsIHVybDogJy9jb21wYW55L2Fib3V0dXMnIH0sXG4gICAgICB7IGxhYmVsOiAnUHJpY2luZyBQbGFuJywgdXJsOiAnL3ByaWNpbmcnIH0sXG4gICAgICB7IGxhYmVsOiAnQ29udGFjdCB1cycsIHVybDogJy9yZXF1ZXN0LWRlbW8nIH0sXG4gICAgICB7IGxhYmVsOiAnUHJpdmFjeSBQb2xpY3knLCB1cmw6ICcvcHJpdmFjeS1hbmQtdGVybXMnIH0sXG4gICAgICB7XG4gICAgICAgIGxhYmVsOiAnQ2FuY2VsbGF0aW9uICYgUmV0dXJuIFBvbGljeScsXG4gICAgICAgIHVybDogJy9jYW5jZWxsYXRpb24tYW5kLXJldHVybicsXG4gICAgICB9LFxuICAgICAgeyBsYWJlbDogJ1Rlcm1zICYgQ29uZGl0aW9ucycsIHVybDogJy90ZXJtcy1hbmQtY29uZGl0aW9ucycgfSxcbiAgICBdLFxuICB9LFxuXTtcblxuY29uc3QgU09DSUFMX0xJTktTID0gW1xuICB7XG4gICAga2V5OiAnZmFjZWJvb2snLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS9FZ25pZnkvJyxcbiAgICBpY29uOiAnL2ltYWdlcy9mb290ZXIvZmFjZWJvb2suc3ZnJyxcbiAgfSxcbiAge1xuICAgIGtleTogJ3lvdXR1YmUnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3VzZXIveWVycmFuYWd1JyxcbiAgICBpY29uOiAnL2ltYWdlcy9mb290ZXIveW91dHViZS5zdmcnLFxuICB9LFxuICB7XG4gICAga2V5OiAnbGlua2VkaW4nLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9jb21wYW55L2VnbmlmeScsXG4gICAgaWNvbjogJy9pbWFnZXMvZm9vdGVyL2xpbmtlZGluLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBrZXk6ICdpbnN0YWdyYW0nLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3Lmluc3RhZ3JhbS5jb20vZWduaWZ5JyxcbiAgICBpY29uOiAnL2ltYWdlcy9mb290ZXIvaW5zdGFncmFtLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBrZXk6ICd0d2l0dGVyJyxcbiAgICB1cmw6ICdodHRwczovL3R3aXR0ZXIuY29tL2VnbmlmeScsXG4gICAgaWNvbjogJy9pbWFnZXMvZm9vdGVyL3R3aXR0ZXIuc3ZnJyxcbiAgfSxcbl07XG5cbmNsYXNzIEZvb3RlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgaXNBc2g6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIH07XG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGlzQXNoIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Zm9vdGVyIGNsYXNzTmFtZT17aXNBc2ggPyBzLmFkZEdyYXkgOiBudWxsfT5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvZ2V0cmFua3MtbWFya2V0aW5nLW5ldy5zdmdcIlxuICAgICAgICAgIGFsdD1cImdldHJhbmtzIGJ5IGVnbmlmeVwiXG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmxvZ299XG4gICAgICAgIC8+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgcm93ICR7cy5pbXBvcnRhbnRMaW5rQ29udGFpbmVyfWB9PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uRGl2fT5cbiAgICAgICAgICAgICAgICB7Rk9PVEVSX0xJTktTLm1hcChzZWN0aW9uID0+IChcbiAgICAgICAgICAgICAgICAgIDxzZWN0aW9uPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5saW5rc1NlY3Rpb25UaXRsZX0+e3NlY3Rpb24udGl0bGV9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgICB7c2VjdGlvbi5saW5rcy5tYXAoc2VjdGlvbkxpbmsgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT17cy5pbXBvcnRhbnRMaW5rfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmPXtzZWN0aW9uTGluay51cmx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0PXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlY3Rpb25MaW5rLmxhYmVsID09PSAnQmxvZycgPyAnX2JsYW5rJyA6ICcnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3NlY3Rpb25MaW5rLmxhYmVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9iaWxlX2xvZ29fd3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9nZXRyYW5rcy1tYXJrZXRpbmctbmV3LnN2Z1wiXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJnZXRyYW5rcyBieSBlZ25pZnlcIlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLm1vYmlsZV9sb2dvfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hZGRyZXNzRGl2fT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hZGRyZXNzQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxpbmtzU2VjdGlvblRpdGxlfT5IeWRlcmFiYWQgT2ZmaWNlLTE8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3N9PlxuICAgICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgICBLcmlzaGUgRW1lcmFsZCwgS29uZGFwdXIgTWFpbiBSb2FkLCBMYXhtaSBDeWJlciBDaXR5LFxuICAgICAgICAgICAgICAgICAgICAgIFdoaXRlZmllbGRzLCBLb25kYXB1ciwgSHlkZXJhYmFkLCBUZWxhbmdhbmEgNTAwMDgxXG4gICAgICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZlcnRpY2FsTGluZX0gLz5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hZGRyZXNzQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxpbmtzU2VjdGlvblRpdGxlfT5IeWRlcmFiYWQgT2ZmaWNlLTI8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3N9PlxuICAgICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgICAxLTIvMS8yNC9BLzEsNywgUGxvdCBObyAxNyBPcHAuIEJoYXJhdCBQZXRyb2wgQnVuayBKTlRVXG4gICAgICAgICAgICAgICAgICAgICAgUmQsIEhJLVRFQ0ggQ2l0eSBIeWRlcmFiYWQsIFRlbGFuZ2FuYSA1MDAwODRcbiAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmVydGljYWxMaW5lfSAvPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3NDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGlua3NTZWN0aW9uVGl0bGV9PkJlbmdhbHVydSBPZmZpY2U8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3N9PlxuICAgICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgICBQcmVzdGlnZSBBdGxhbnRhLCA4MCBGZWV0IE1haW4gUm9hZCwgS29yYW1hbmdhbGEgMUEgQmxvY2ssXG4gICAgICAgICAgICAgICAgICAgICAgQmVuZ2FsdXJ1LEthcm5hdGFrYSA1NjAwMzRcbiAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmVydGljYWxMaW5lfSAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc29jaWFsfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YHJvdyAke3Muc29jaWFsTGlua0NvbnRhaW5lcn1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxpbmtzU2VjdGlvblRpdGxlMX0+U29jaWFsPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YHJvdyAke3Muc29jaWFsTGlua3NXcmFwcGVyfWB9PlxuICAgICAgICAgICAgICAgICAgICB7U09DSUFMX0xJTktTLm1hcChzb2NpYWxMaW5rID0+IChcbiAgICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnNvY2lhbExpbmt9XG4gICAgICAgICAgICAgICAgICAgICAgICBocmVmPXtzb2NpYWxMaW5rLnVybH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCJcbiAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17c29jaWFsTGluay5pY29ufSBhbHQ9e3NvY2lhbExpbmsua2V5fSAvPlxuICAgICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Zvb3Rlcj5cbiAgICApO1xuICB9XG59XG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEZvb3Rlcik7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0Zvb3Rlci5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0Zvb3Rlci5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vRm9vdGVyLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluay9MaW5rJztcbmltcG9ydCBzIGZyb20gJy4vSGVhZGVyVjIuc2Nzcyc7XG5cbmNsYXNzIEhlYWRlclYyIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG5hdk9wZW46IGZhbHNlLFxuICAgICAgbW9iaWxlOiBmYWxzZSxcbiAgICAgIHNob3djb21wYW55OiBmYWxzZSxcbiAgICAgIHNob3dQcm9kdWN0czogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlc2l6ZSgpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZXNpemUpO1xuXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7XG4gICAgICBpZiAoXG4gICAgICAgIC8qISBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZmVhdHVyZXNsYWJlbCcpLmNvbnRhaW5zKGV2ZW50LnRhcmdldCkgJiYgKi9cbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NvbXBhbnlsYWJlbCcpICYmXG4gICAgICAgICFkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29tcGFueWxhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KVxuICAgICAgICAvLyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncHJvZHVjdHNsYWJlbCcpICYmXG4gICAgICAgIC8vICFkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncHJvZHVjdHNsYWJlbCcpLmNvbnRhaW5zKGV2ZW50LnRhcmdldClcbiAgICAgICAgLyogIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZXNvdXJjZXNsYWJlbCcpLmNvbnRhaW5zKGV2ZW50LnRhcmdldCkgKi9cbiAgICAgICkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAvLyBzaG93ZmVhdHVyZXM6IGZhbHNlLFxuICAgICAgICAgIHNob3djb21wYW55OiBmYWxzZSxcbiAgICAgICAgICAvLyBzaG93UHJvZHVjdHM6IGZhbHNlLFxuICAgICAgICAgIC8vIHNob3dyZXNvdXJjZXM6IGZhbHNlLFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5oYW5kbGVzaXplKTtcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgIGlmIChcbiAgICAgICAgLy8gIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwcm9kdWN0c2xhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KVxuICAgICAgICAhZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NvbXBhbnlsYWJlbCcpLmNvbnRhaW5zKGV2ZW50LnRhcmdldClcbiAgICAgICAgLy8gIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZXNvdXJjZXNsYWJlbCcpLmNvbnRhaW5zKGV2ZW50LnRhcmdldClcbiAgICAgICkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAvLyBzaG93ZmVhdHVyZXM6IGZhbHNlLFxuICAgICAgICAgIHNob3djb21wYW55OiBmYWxzZSxcbiAgICAgICAgICAvLyBzaG93UHJvZHVjdHM6IGZhbHNlLFxuICAgICAgICAgIC8vIHNob3dyZXNvdXJjZXM6IGZhbHNlLFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZXNpemUgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgOTkwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgbW9iaWxlOiB0cnVlIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgbW9iaWxlOiBmYWxzZSB9KTtcbiAgICB9XG4gIH07XG5cbiAgdG9nZ2xlTmF2YmFyID0gKGNsb3NlID0gZmFsc2UpID0+IHtcbiAgICBpZiAoY2xvc2UpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBuYXZPcGVuOiBmYWxzZSB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG5hdk9wZW46ICF0aGlzLnN0YXRlLm5hdk9wZW4gfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZXNob3djb21wYW55ID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd2NvbXBhbnk6ICF0aGlzLnN0YXRlLnNob3djb21wYW55LFxuICAgICAgc2hvd1Byb2R1Y3RzOiBmYWxzZSxcbiAgICAgIC8vIHNob3dyZXNvdXJjZXM6IGZhbHNlLFxuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZXNob3dQcm9kdWN0ID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd2NvbXBhbnk6IGZhbHNlLFxuICAgICAgc2hvd1Byb2R1Y3RzOiAhdGhpcy5zdGF0ZS5zaG93UHJvZHVjdHMsXG4gICAgfSk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5uYXZiYXJXcmFwcGVyfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubmF2YmFyfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sb2dvfT5cbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL1wiPlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9nZXRyYW5rcy1tYXJrZXRpbmctbmV3LnN2Z1wiXG4gICAgICAgICAgICAgICAgYWx0PVwiZ2V0cmFua3NcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDx1bFxuICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5uYXZPcGVuID8gYCR7cy5tZW51fSAke3MuYWN0aXZlfWAgOiBgJHtzLm1lbnV9YFxuICAgICAgICAgICAgfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHsvKiA8bGlcbiAgICAgICAgICAgICAgaWQ9XCJmZWF0dXJlc2xhYmVsXCJcbiAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVmZWF0dXJlc31cbiAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5mZWF0dXJlTmFtZX0+RmVhdHVyZXM8L3NwYW4+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zaG93ZmVhdHVyZXNcbiAgICAgICAgICAgICAgICAgICAgICA/ICdpbWFnZXMvaWNvbnMvY2hldnJvbi11cC5zdmcnXG4gICAgICAgICAgICAgICAgICAgICAgOiAnaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmcnXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJkb3duLWFuZ2xlXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zaG93ZmVhdHVyZXNcbiAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmZlYXR1cmVzZHJvcGRvd259ICR7cy5hY3RpdmVmZWF0dXJlc31gXG4gICAgICAgICAgICAgICAgICAgIDogYCR7cy5mZWF0dXJlc2Ryb3Bkb3dufWBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7T0xEX0xJVkVDTEFTU0VTREFUQS5tYXAoZmVhdHVyZSA9PiAoXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5mZWF0dXJld3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmZlYXR1cmV9PlxuICAgICAgICAgICAgICAgICAgICAgIHtmZWF0dXJlLmZlYXR1cmVzLm1hcChpdGVtID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAhaXRlbS5pc0hlYWRpbmcgPyAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuZmVhdHVyZWl0ZW19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVyb3V0aW5nKGl0ZW0ucm91dGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PXtpdGVtLnRpdGxlfSAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntpdGVtLnRpdGxlfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmZlYXR1cmVoZWFkaW5nfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVyb3V0aW5nKGl0ZW0ucm91dGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PXtpdGVtLnRpdGxlfSAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntpdGVtLnRpdGxlfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnZpZXdtb3Jld3JhcHBlcn1cbiAgICAgICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZXJvdXRpbmcoZmVhdHVyZS5yb3V0ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPXtzLnZpZXdtb3JlfT5MZWFybiBtb3JlICYjODU5NDs8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvbGk+ICovfVxuICAgICAgICAgICAgey8qIDxsaT5cbiAgICAgICAgICAgICAgPExpbmsgdG89XCIvY3VzdG9tZXJzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5XaHkgZWduaWZ5PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgPC9saT4gKi99XG4gICAgICAgICAgICB7LyogPGxpPlxuICAgICAgICAgICAgICA8TGluayB0bz1cIi9ob3ctZWduaWZ5LXdvcmtzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5Ib3cgaXQgd29ya3M8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICA8L2xpPiAqL31cbiAgICAgICAgICAgIDxsaVxuICAgICAgICAgICAgICBpZD1cInByb2R1Y3RzbGFiZWxcIlxuICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZXNob3dQcm9kdWN0fVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5Qcm9kdWN0czwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dQcm9kdWN0c1xuICAgICAgICAgICAgICAgICAgICAgID8gJy9pbWFnZXMvaWNvbnMvY2hldnJvbi11cC5zdmcnXG4gICAgICAgICAgICAgICAgICAgICAgOiAnL2ltYWdlcy9pY29ucy9jaGV2cm9uLWRvd24uc3ZnJ1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYWx0PVwiZG93bi1hbmdsZVwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8dWxcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zaG93UHJvZHVjdHNcbiAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmNvbXBhbnlkcm9wZG93bn0gJHtzLmFjdGl2ZWRyb3Bkb3duc31gXG4gICAgICAgICAgICAgICAgICAgIDogYCR7cy5jb21wYW55ZHJvcGRvd259YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL21vZHVsZXMvdGVzdHNcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVzdC9tb2R1bGVfdGVzdC5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPk9ubGluZSBUZXN0czwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL21vZHVsZXMvbGl2ZWNsYXNzZXNcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvb2xkX0xpdmUuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJsaXZlLWNsYXNzZXNcIlxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj5MaXZlIENsYXNzZXM8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8TGluayB0bz1cIi9tb2R1bGVzL2RvdWJ0c1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfRG91YnRzLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwib25saW5lLXRlc3RzXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+RG91YnRzPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbW9kdWxlcy9hc3NpZ25tZW50c1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfQXNzaWdubWVudHMuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJvbmxpbmUtdGVzdHNcIlxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj5Bc3NpZ25tZW50czwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL21vZHVsZXMvY29ubmVjdFwiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L25ld19jb25uZWN0LnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiY29ubmVjdFwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPkNvbm5lY3Q8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgIDxMaW5rIHRvPVwiL2N1c3RvbWVyc1wiIG9uQ2xpY2s9eygpID0+IHRoaXMudG9nZ2xlTmF2YmFyKGZhbHNlKX0+XG4gICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PkN1c3RvbWVyczwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgIDxMaW5rIHRvPVwiL3ByaWNpbmdcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PlByaWNpbmc8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPGxpXG4gICAgICAgICAgICAgIGlkPVwiY29tcGFueWxhYmVsXCJcbiAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVzaG93Y29tcGFueX1cbiAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5mZWF0dXJlTmFtZX0+Q29tcGFueTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3djb21wYW55XG4gICAgICAgICAgICAgICAgICAgICAgPyAnL2ltYWdlcy9pY29ucy9jaGV2cm9uLXVwLnN2ZydcbiAgICAgICAgICAgICAgICAgICAgICA6ICcvaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmcnXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJkb3duLWFuZ2xlXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDx1bFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3djb21wYW55XG4gICAgICAgICAgICAgICAgICAgID8gYCR7cy5jb21wYW55ZHJvcGRvd259ICR7cy5hY3RpdmVkcm9wZG93bnN9YFxuICAgICAgICAgICAgICAgICAgICA6IGAke3MuY29tcGFueWRyb3Bkb3dufWBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7LyogPGxpPkFib3V0IFVzPC9saT4gKi99XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvY29tcGFueS9hYm91dHVzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgICAgICBBYm91dCBVc1xuICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvY29tcGFueS9wcmVzc1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+UHJlc3M8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8TGlua1xuICAgICAgICAgICAgICAgICAgICB0bz1cIi9jb21wYW55L2N1bHR1cmVcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLnRvZ2dsZU5hdmJhcihmYWxzZSl9XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPkN1bHR1cmU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICBocmVmPVwiL2NvbXBhbnkvY2FyZWVyc1wiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMudG9nZ2xlTmF2YmFyKGZhbHNlKX1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+Q2FyZWVyczwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIHsvKiA8bGlcbiAgICAgICAgICAgICAgaWQ9XCJyZXNvdXJjZXNsYWJlbFwiXG4gICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuaGFuZGxlcmVzb3VyY2VzfVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5SZXNvdXJjZXM8L3NwYW4+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zaG93cmVzb3VyY2VzXG4gICAgICAgICAgICAgICAgICAgICAgPyAnaW1hZ2VzL2ljb25zL2NoZXZyb24tdXAuc3ZnJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJ2ltYWdlcy9pY29ucy9jaGV2cm9uLWRvd24uc3ZnJ1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYWx0PVwiZG93bi1hbmdsZVwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8dWxcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zaG93cmVzb3VyY2VzXG4gICAgICAgICAgICAgICAgICAgID8gYCR7cy5jb21wYW55ZHJvcGRvd259ICR7cy5hY3RpdmVkcm9wZG93bnN9YFxuICAgICAgICAgICAgICAgICAgICA6IGAke3MuY29tcGFueWRyb3Bkb3dufWBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8bGlcbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVyb3V0aW5nKCdQcm9kdWN0VG91cicpfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIFByb2R1Y3QgVG91clxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICA8L2xpPiAqL31cbiAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICB3aW5kb3cub3BlbignaHR0cHM6Ly9ibG9nLmVnbmlmeS5jb20nLCAnYmxhbmsnKTtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5mZWF0dXJlTmFtZX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIEJsb2dcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICA8L3VsPlxuXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYnRuZ3JvdXB9PlxuICAgICAgICAgICAgPExpbmsgdG89XCIvcmVxdWVzdC1kZW1vXCI+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Muc2NoZWR1bGVEZW1vfVxuICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMudG9nZ2xlTmF2YmFyKHRydWUpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3RoaXMuc3RhdGUubW9iaWxlID8gJ0RlbW8nIDogJ1NDSEVEVUxFIERFTU8nfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL2FjY291bnQuZ2V0cmFua3MuaW4vc2lnbmluP2hvc3Q9aHR0cHMlM0ElMkYlMkZnZXRyYW5rcy5pbiUyRlwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5sb2dpbn1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgTG9naW5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy50b2dnbGVOYXZiYXIoZmFsc2UpfVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLm5hdk9wZW4gPyBgJHtzLm1lbnVidG59ICR7cy5tZW51YWN0aXZlfWAgOiBzLm1lbnVidG5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICF0aGlzLnN0YXRlLm5hdk9wZW5cbiAgICAgICAgICAgICAgICAgICAgPyAnL2ltYWdlcy9pY29ucy9kcmF3ZXIuc3ZnJ1xuICAgICAgICAgICAgICAgICAgICA6ICcvaW1hZ2VzL2ljb25zL2Nsb3NlLnN2ZydcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYWx0PVwidG9nZ2xlIGJ1dHRvblwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEhlYWRlclYyKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSGVhZGVyVjIuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9IZWFkZXJWMi5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSGVhZGVyVjIuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0xheW91dC5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vTGF5b3V0LmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0xheW91dC5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5cbi8vIGV4dGVybmFsLWdsb2JhbCBzdHlsZXMgbXVzdCBiZSBpbXBvcnRlZCBpbiB5b3VyIEpTLlxuaW1wb3J0IHMgZnJvbSAnLi9MYXlvdXQuY3NzJztcbmltcG9ydCBIZWFkZXJWMiBmcm9tICcuLi9IZWFkZXJWMi9IZWFkZXJWMic7XG5pbXBvcnQgRm9vdGVyIGZyb20gJy4uL0Zvb3Rlcic7XG5cbmNsYXNzIExheW91dCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLmlzUmVxdWlyZWQsXG4gICAgZm9vdGVyQXNoOiBQcm9wVHlwZXMuYm9vbCxcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxIZWFkZXJWMiAvPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YHJvdyAke3MuYm9keX1gfT57dGhpcy5wcm9wcy5jaGlsZHJlbn08L2Rpdj5cbiAgICAgICAgPEZvb3RlciBpc0FzaD17dGhpcy5wcm9wcy5mb290ZXJBc2h9IC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkxheW91dC5kZWZhdWx0UHJvcHMgPSB7XG4gIGZvb3RlckFzaDogZmFsc2UsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKExheW91dCk7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4uLy4uL2hpc3RvcnknO1xuXG5mdW5jdGlvbiBpc0xlZnRDbGlja0V2ZW50KGV2ZW50KSB7XG4gIHJldHVybiBldmVudC5idXR0b24gPT09IDA7XG59XG5cbmZ1bmN0aW9uIGlzTW9kaWZpZWRFdmVudChldmVudCkge1xuICByZXR1cm4gISEoZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5zaGlmdEtleSk7XG59XG5cbmNsYXNzIExpbmsgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIHRvOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLmlzUmVxdWlyZWQsXG4gICAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gIH07XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICBvbkNsaWNrOiBudWxsLFxuICB9O1xuXG4gIGhhbmRsZUNsaWNrID0gZXZlbnQgPT4ge1xuICAgIGlmICh0aGlzLnByb3BzLm9uQ2xpY2spIHtcbiAgICAgIHRoaXMucHJvcHMub25DbGljayhldmVudCk7XG4gICAgfVxuXG4gICAgaWYgKGlzTW9kaWZpZWRFdmVudChldmVudCkgfHwgIWlzTGVmdENsaWNrRXZlbnQoZXZlbnQpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgPT09IHRydWUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGhpc3RvcnkucHVzaCh0aGlzLnByb3BzLnRvKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyB0bywgY2hpbGRyZW4sIC4uLnByb3BzIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8YSBocmVmPXt0b30gey4uLnByb3BzfSBvbkNsaWNrPXt0aGlzLmhhbmRsZUNsaWNrfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9hPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTGluaztcbiIsImltcG9ydCBjcmVhdGVCcm93c2VySGlzdG9yeSBmcm9tICdoaXN0b3J5L2NyZWF0ZUJyb3dzZXJIaXN0b3J5JztcblxuLy8gTmF2aWdhdGlvbiBtYW5hZ2VyLCBlLmcuIGhpc3RvcnkucHVzaCgnL2hvbWUnKVxuLy8gaHR0cHM6Ly9naXRodWIuY29tL21qYWNrc29uL2hpc3RvcnlcbmV4cG9ydCBkZWZhdWx0IHByb2Nlc3MuZW52LkJST1dTRVIgJiYgY3JlYXRlQnJvd3Nlckhpc3RvcnkoKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQVhBO0FBZ0JBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBTUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFHQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFwR0E7QUFDQTtBQURBO0FBRUE7QUFEQTtBQUNBO0FBbUdBOzs7Ozs7O0FDektBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFtREE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQXpEQTtBQTJEQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBakVBO0FBbUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBekVBO0FBMkVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQTlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFnQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUZBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW9DQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUtBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQXZZQTtBQUNBO0FBdVlBOzs7Ozs7O0FDN1lBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQWZBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFGQTtBQUNBO0FBZUE7QUFDQTtBQURBO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7QUFDQTtBQTJCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFwQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUZBO0FBUUE7QUFEQTtBQUNBO0FBOEJBOzs7Ozs7OztBQ2xEQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTs7OztBIiwic291cmNlUm9vdCI6IiJ9