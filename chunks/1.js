require("source-map-support").install();
exports.ids = [1];
exports.modules = {

/***/ "./src/routes/products/get-ranks/GetRanksConstants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENTS", function() { return HOME_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENTS_UPDATED", function() { return HOME_CLIENTS_UPDATED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_BENEFITS", function() { return HOME_BENEFITS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_BENEFITS_DATA", function() { return HOME_BENEFITS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_AWARDS", function() { return HOME_AWARDS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_NEWS", function() { return HOME_NEWS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_MEDIA", function() { return HOME_MEDIA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_APPROACH", function() { return HOME_APPROACH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENT_SAY", function() { return HOME_CLIENT_SAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ASSESS_INDEPTH", function() { return ASSESS_INDEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_CONTENT_1", function() { return ANALYZE_CONTENT_1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_BEHAVIOUR_ANALYSIS", function() { return ANALYZE_BEHAVIOUR_ANALYSIS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_CONTENT_2", function() { return ANALYZE_CONTENT_2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRACTICE_CONTENT", function() { return PRACTICE_CONTENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STUDENTS_RECORDS", function() { return STUDENTS_RECORDS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SLIDESHOW", function() { return SLIDESHOW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEACHFOR", function() { return TEACHFOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLATFORMS", function() { return PLATFORMS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMERS_STATS", function() { return CUSTOMERS_STATS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMER_REVIEW", function() { return CUSTOMER_REVIEW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CLIENTS_SET1", function() { return CLIENTS_SET1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CLIENTS_SET2", function() { return CLIENTS_SET2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LIVECLASSESDATA", function() { return LIVECLASSESDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OLD_LIVECLASSESDATA", function() { return OLD_LIVECLASSESDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MEDIA_COVERAGE", function() { return MEDIA_COVERAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRESS_VIDEO_COVERAGE", function() { return PRESS_VIDEO_COVERAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VIEWMORE", function() { return VIEWMORE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TESTS_KEY_POINTS", function() { return TESTS_KEY_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newModulesData", function() { return newModulesData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "moduleKeyPoints", function() { return moduleKeyPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cloneDeeply", function() { return cloneDeeply; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOW_WORKS", function() { return HOW_WORKS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOW_WORKS_TITLES", function() { return HOW_WORKS_TITLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEST_VIDEOS", function() { return TEST_VIDEOS; });
const HOME_CLIENTS = [{
  icon: '/images/request-demo/clients/gov_of_telangana.png'
}, {
  icon: '/images/request-demo/clients/sri_chaithanya.png'
}, {
  icon: '/images/request-demo/clients/rankguru.png'
}, {
  icon: '/images/request-demo/clients/ms.png'
}, {
  icon: '/images/request-demo/clients/motion.png'
}, {
  icon: '/images/request-demo/clients/tirumala.png'
}, {
  icon: '/images/request-demo/clients/trinity.png'
}, {
  icon: '/images/request-demo/clients/sri_vishwa.png'
}, {
  icon: '/images/request-demo/clients/sri_bhavishya.png'
}, {
  icon: '/images/request-demo/clients/sarath_academy.png'
}, {
  icon: '/images/request-demo/clients/pragathi.png'
}, {
  icon: '/images/request-demo/clients/sri_gayathri.png'
}];
const HOME_CLIENTS_UPDATED = [{
  id: 'gov_of_telangana',
  icon: '/images/home/clients/gov_of_telangana_black.png'
}, {
  id: 'sri_chaithanya',
  icon: '/images/home/clients/sri_chaithanya_black.png'
}, {
  id: 'rank_guru',
  icon: '/images/home/clients/rankguru_black.png'
}, {
  id: 'ms',
  icon: '/images/home/clients/ms_black.png'
}, {
  id: 'motion',
  icon: '/images/home/clients/Motion_black.png'
}, {
  id: 'tirumala',
  icon: '/images/home/clients/Tirumala_black.png'
}, {
  id: 'trinity',
  icon: '/images/home/clients/trinity_black.png'
}, {
  id: 'sri_vishwa',
  icon: '/images/home/clients/sri_vishwa_black.png'
}, {
  id: 'sri_bhavishya',
  icon: '/images/home/clients/sri_bhavishya_black.png'
}, {
  id: 'sarath_Academy',
  icon: '/images/home/clients/sarath_Academy_black.png'
}, {
  id: 'pragathi',
  icon: '/images/home/clients/pragathi_black.png'
}, {
  id: 'sri_gayathri',
  icon: '/images/home/clients/sri_gayathri_black.png'
}];
const HOME_BENEFITS = [{
  heading: 'Institutes',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Teachers',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Students',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Parents',
  icon: '/images/products/get-ranks/dummy.svg'
}];
const HOME_BENEFITS_DATA = [{
  heading: 'Smarter Automation',
  content: 'GetRanks gets you up and running in no time and with our hands-on customer success team and automated processes you can conduct tests and analyse student data in minutes',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Intelligent Insights',
  content: 'Our detailed dashboards help you understand students’ performance and help them remediate in a personalised manner suited to their needs',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Online Testing Platform',
  content: 'GetRanks helps you simulate the actual test conditions and platform which helps your students prepare well for the actual exam',
  icon: '/images/products/get-ranks/dummy.svg'
}];
const HOME_AWARDS = [{
  icon: '/images/home/achievements/wec1.png',
  content: '50 Fabulous Edutech Leaders',
  icon1: '/images/home/achievements/wec.png'
}, {
  icon: '/images/home/achievements/iit-varanasi.png',
  content: 'Best Analytics App of 2017',
  icon1: '/images/home/achievements/bla.png'
}, {
  icon: '/images/home/achievements/brainfeed.png',
  content: 'Best Learning Analytics Company of 2018',
  icon1: '/images/home/achievements/bf.png'
}, {
  icon: '/images/home/achievements/higherEducation.png',
  content: 'Best Adaptive Technology Solution',
  icon1: '/images/home/achievements/hep.png'
}, {
  icon: '/images/home/achievements/is.png',
  content: 'Outstanding EdTech Startups',
  icon1: '/images/home/achievements/is.png'
}, {
  icon: '/images/home/achievements/cio.svg',
  content: '20 Most promising Ed-Tech startups of 2018',
  icon1: '/images/home/achievements/cio.png'
}];
const HOME_NEWS = [{
  icon: '/images/home/media/on_news/etv_telangana.webp',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M'
}, {
  icon: '/images/home/media/on_news/kiran_sir_on_T news.webp',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}, {
  icon: '/images/home/media/on_news/students_on_v6.webp',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}];
const HOME_MEDIA = [{
  icon: '/images/home/media/financial_Express.webp',
  name: 'financial_Express',
  url: 'https://www.financialexpress.com/industry/surprise-predictive-analysis-of-students-future-course-of-action-for-career-development-now-possible-egnify-rolls-out-solution/881282/'
}, {
  icon: '/images/home/media/yourstory.webp',
  name: 'yourstory',
  url: 'https://yourstory.com/2017/12/hyderabad-startup-students-exam/'
}, {
  icon: '/images/home/media/indian-express.webp',
  name: 'indian-express',
  url: 'http://www.newindianexpress.com/cities/hyderabad/2017/sep/19/impetus-to-excel-1659699.html'
}, {
  icon: '/images/home/media/asia500.png',
  name: 'indian-asia500',
  url: 'http://www.asiainc500.com/wp-content/uploads/2018/10/Asia-Inc.-500-Magazine-Oct-2018-D91.pdf'
}, {
  icon: '/images/home/media/deccan.webp',
  name: 'indian-deccan',
  url: 'https://www.deccanchronicle.com/nation/current-affairs/120118/pilot-project-to-mould-state-school-kids-in-telangana.html'
}, {
  icon: '/images/home/media/etv.webp',
  name: 'etv',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M'
}, {
  icon: '/images/home/media/news_minute.webp',
  name: 'news_minute',
  url: 'https://www.thenewsminute.com/article/telangana-ties-t-hub-startup-egnify-improve-education-govt-schools-74652'
}, {
  icon: '/images/home/media/telangana-today.webp',
  name: 'telangana-today',
  url: 'https://telanganatoday.com/hyderabad-based-startup-egnify-technologies-helps-students-excel'
}, {
  icon: '/images/home/media/andhrajyothi.webp',
  name: 'andhrajyothi',
  url: 'http://www.andhrajyothy.com/artical?SID=461827'
}, {
  icon: '/images/home/media/v6n1.webp',
  name: 'V6 news',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}, {
  icon: '/images/home/media/namasthe-telangana.webp',
  name: 'namasthe-telangana',
  url: 'http://epaper.ntnews.com/c/22959899'
}, {
  icon: '/images/home/media/t-news.webp',
  name: 't-news',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}];
const HOME_APPROACH = [{
  heading: 'For Institutes',
  content: 'Our platform helps institutes to conduct online and offline tests for their students across multiple test patterns. Our detailed data dashboards helps you understand your students performance and helps you guide them to success'
}, {
  heading: 'For Students',
  content: 'GetRanks empowers students with in-depth analysis of their test performance across Competence and Behavioural aspects to help them improve their performance and pave the way to their dream college'
}];
const HOME_CLIENT_SAY = [{
  content: `Our institute chose this app for sending us online lectures and for conducting exams and I gotta say,
    it really works well. The classes have high quality visual and you can change the playback speed as well.
     You can't take screenshots during the exam and after the exam we get individual marks for
     each subject and the total as well. We can even compare our previous scores. We can even analyse the toppers
     score along with ours. Overall, it is a wonderful app.`,
  name: 'Stacy Cordelia',
  designation: 'Student',
  institute: '/images/home/clients/sri-chaitanya.png',
  defaultimage: '/images/icons/person_blue.svg'
}, {
  content: `This app is so good and there is no problem in quality of classes posted in the app. The way of conducting exam
     in the app is so good that every student will be comfortable to write. This app is also good for studies. Online exams
      are also so good.Thank for supporting our institution in keeping exams and online classes So everybody download getranks
      and request your Institute to conduct exams and post classes in getranks`,
  name: `lechireddy Surekha`,
  designation: `Student`,
  institute: '/images/home/clients/pinegrove.jpeg',
  defaultimage: '/images/icons/person_blue.svg'
}, {
  content: `This is way too useful for JEE aspirants and the User Interface is too simple to use and this app is adhered
     to privacy of each and every user. Apart from the institution's main exams they also provide us with the practice
      tests which are efficient too. The idea of including zoom meetings in the app was too incredible👍.`,
  name: 'Hemubunny Kasireddy',
  designation: `Student`,
  institute: '/images/home/clients/Meluha.png',
  defaultimage: '/images/icons/person_blue.svg'
},
/* {
  content: `An Excellent app for academics .This app helps us to compare our performance with other people .It also
   helps us to compare our performance with the previous one. It helps us in understanding our strengths & weakness .
   There is graphical analysis to make comparison even more easier . The app also shows the time we have spent on each
    question & time spent for doing right & wrong answers .The app also enables us to watch video lectures uploaded
     by institute . Overall I think the app is awesome .`,
  name: `Sri Ranga`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/icons/person_blue.svg',
}, */
{
  content: `This app has excellent top notch features which helps students get up to the mark even in the pandemic.
     There are no errors and it works perfectly fine. Even when all the academics shifted to a whole online process,
      this app keeps its mark up. Good one and i really suggest`,
  name: `Srilakshmi kandula`,
  designation: `Parent`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/icons/person_blue.svg'
}
/* {
  content: `This app is very helpful to me to study. Now, it is very easy to me to listen my classes or to practice
   any subject.It is really a gud thing ,it has no ads.And very interesting tests.I used tbisapp
    very well I wish thip app will helpful to many of the students who are interestedto study.
     I congratulate the app for the work they made..... Thank you !.....👍`,
  name: `Vanaparthi Madhavi`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `The app is very convenient and helpful. The way it shows the Analysis of the exams and the review format
   of the exams are excellent.. Thanks for the app.`,
  name: ``,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `Best and excellent app. It clearly shows me percentage and the percentile of my performance in both the mains
  and advance exam. And the best thing of this app is, it had practice papers even.Thankyou for creating such an
  excellent app.`,
  name: `Satyavathi Nuni`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy_profile.jpg',
},
{
  content: `Getranks is a good app.where we can see and listen to lectures posted by our teachers and we can write the tests.
  once we completed our test ,we get our result immediately without any delay..we can also get the solutions for
  those questions in the test..`,
  name: `Seethala Srinidhi`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `This is one of the best app for eductional purpose, they were giving new features for every update as like
   conducting zoom meetings in app it self . thanks for all who were working under this app by giving your best for us.`,
  name: `HASITH VARDHAN`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `The best app for any student .This app is one of the most useful app .It really shows me whether I had
  improved or not in the examination by giving my accuracy and attempt for each subject . The option compare with the
  topper really shows me still how much I have to improve . Thanks for the creators who created such an wonderful app.`,
  name: `Surya Bhaskara Rao Chekuri`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `Nice app for jee mains preparation in these circumstances. There are invidual practice tests of subject and
  chapter practice . Which can help to strengthen our weak areas with jee main type mock test. Thank you team egnify`,
  name: `Rishitha s99`,
  designation: ``,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
}, */
];
const ASSESS_INDEPTH = [{
  heading: 'Marks Analysis',
  content: 'Our Marks Analysis helps you identify the at-risk and just-missed students and enables you to set customised goals and nudge them forward.',
  icon: '/images/products/get-ranks/assess-screenshot4.svg'
}, {
  heading: 'Error Analysis',
  content: 'Negative marking is the bane that pulls your students performance down. Pinpoint the areas that needs to be addressed and boost the performance of students by minimising errors',
  icon: '/images/products/get-ranks/assess-screenshot5.svg'
}, {
  heading: 'Concept Analysis',
  content: 'Students performance in Subjects is just not enough. GetRanks goes further and gives you a detailed picture of the health of you students across topics and subtopics covered in the test.',
  icon: '/images/products/get-ranks/assess-screenshot6.svg'
}];
const ANALYZE_CONTENT_1 = [{
  headerContent: 'Dedicated Student Profile',
  mainContent: 'The one stop that gives you the detailed student performance analysis with easy to understand dashboards and performance trends across your Marks, Topics and Behavioural aspects',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot1.svg'
}, {
  headerContent: 'Question Review',
  mainContent: 'Review your test performance question-by-question and get an understanding of how your peers performed in each of them - across competence and behavioural aspects',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot2.svg'
}, {
  headerContent: 'Mark Analysis',
  mainContent: 'Know your marks across each subject and difficulty levels. The attempt and accuracy rates help you understand your weak attempts improve the same going forward',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/analyze-screenshot3.svg'
}, {
  headerContent: 'Concept Analysis',
  mainContent: 'Understand the weightage of each topic and your performance in it across all the tests attempted during the year',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot4.svg'
}];
const ANALYZE_BEHAVIOUR_ANALYSIS = [{
  heading: 'Time Analysis',
  content: 'Time management is the most important aspect of test taking. GetRanks tracks your time during the test and provides an easily understandable time map to help you improve your time management skills'
}, {
  heading: 'Question Analysis',
  content: 'Every question is different and we treat it as such; we analyse how you performed in each question across various dimensions and nudge you to improve'
}, {
  heading: 'Difficulty Analysis',
  content: 'The difficulty of the paper has a direct impact on students performance and we help you understand your difficulty profile so that you can improve your accuracy and reduce negative marking'
}];
const ANALYZE_CONTENT_2 = [{
  headerContent: 'Compare ',
  mainContent: 'Compare your performance with the toppers and figure out their way of climbing to the top',
  button: 'COMPARE NOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot6.svg'
}, {
  headerContent: 'Rank Predictor',
  mainContent: 'Find out where you stand at the national level. Set goals and chart the path to your dream college!',
  button: 'KNOW MORE',
  screenImage: '/images/products/get-ranks/analyze-screenshot7.svg'
}];
const PRACTICE_CONTENT = [{
  headerContent: 'Ask Disha!',
  mainContent: 'Having trouble deciding what to practice? Don’t worry, just ask Disha and she’ll take you through the optimal learning path to achieve your goals.',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/practice-screenshot1.svg'
}, {
  headerContent: 'Custom Practice',
  mainContent: 'Practice at your own pace. Choose your own chapters, difficulty and time, feel comfortable in the test taking experience and targetedly improve your scores.',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/practice-screenshot2.svg'
}, {
  headerContent: 'All India Test Series',
  mainContent: 'Compare your performance with all your competitors across the country. Evaluate where you stand and target your approach accordingly.',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot3.svg'
}];
const STUDENTS_RECORDS = [{
  rank: 'AIR 354',
  imgUrl: '/images/home/students/navneet.png',
  name: 'V NAVANEETH',
  app_no: 'APP.NO.190310534282'
}, {
  rank: 'AIR 359',
  imgUrl: '/images/home/students/nityanand.png',
  name: 'P.S.S. NITHYANAND',
  app_no: 'APP.NO.190310298638'
}, {
  rank: 'AIR 441',
  imgUrl: '/images/home/students/rohitraj.png',
  name: 'R.ROHITRAJ',
  app_no: 'APP.NO.190310522538'
}, {
  rank: 'AIR 505',
  imgUrl: '/images/home/students/chandu.png',
  name: 'CH.CHANDU',
  app_no: 'APP.NO.190310150759'
}, {
  rank: 'AIR 618',
  imgUrl: '/images/home/students/saipavan.png',
  name: 'R SAI PAVAN',
  app_no: 'APP.NO.190310218940'
}, {
  rank: 'AIR 723',
  imgUrl: '/images/home/students/tejaswi.png',
  name: 'P.TEJASWI',
  app_no: 'APP.NO.190310117457'
}, {
  rank: 'AIR 972',
  imgUrl: '/images/home/students/premchand.png',
  name: 'G PREM CHAND',
  app_no: 'APP.NO.190310218630'
}, {
  rank: 'AIR 1039',
  imgUrl: '/images/home/students/saikrishna.png',
  name: 'A SAI KRISHNA',
  app_no: 'APP.NO.190310476368'
}, {
  rank: 'AIR 1118',
  imgUrl: '/images/home/students/abhilash.png',
  name: 'K.ABHILASH',
  app_no: 'APP.NO.190310218379'
}, {
  rank: 'AIR 1150',
  imgUrl: '/images/home/students/varsha.png',
  name: 'A.VARSHA',
  app_no: 'APP.NO.190310801602'
}];
const SLIDESHOW = [{
  title: 'JEE MAIN',
  doubleDigitRanks: 70,
  tripleDigitRanks: '401',
  bestRanks: '1, 11, 13, 14, 24'
}, {
  title: 'JEE ADV.',
  doubleDigitRanks: 30,
  tripleDigitRanks: '130',
  bestRanks: '1, 7, 17, 24, 28'
}];
const TEACHFOR = [{
  title: 'Academics',
  courses: ['CLASS 6', ' CLASS 7', ' CLASS 8', ' CLASS 9', ' CLASS 10', ' CLASS 11', ' CLASS 12']
}, {
  title: 'Competitive Exams',
  courses: ['IIT JEE', ' NEET', ' EAMCET', 'CBSE', 'BITSAT', 'COMED-K', 'VITEEE', 'AIIMS', 'JIPMER']
}, {
  title: 'Extracurricular',
  courses: ['MUSIC', 'PAINTING', 'DANCING', 'YOGA', 'MEDITATION', 'ABACUS', 'PHOTOGRAPHY']
}];
const PLATFORMS = [{
  label: 'Mobile',
  available: 'Android & iOS',
  icon: '/images/home/platforms/mobile.svg'
}, {
  label: 'Desktop',
  available: 'Chrome',
  icon: '/images/home/platforms/desktop.svg'
}, {
  label: 'Tablet',
  available: 'Android & iOS',
  icon: '/images/home/platforms/tablet.svg'
}];
const CUSTOMERS_STATS = [{
  icon: '/images/customers/customers_new.svg',
  number: '1 Million +',
  text: 'Satisfied Students',
  label: 'Customers Logo'
}, {
  icon: '/images/customers/institution_new.svg',
  number: '100+',
  text: 'Satisfied Institutions',
  label: 'Institutions Logo'
}];
const CUSTOMER_REVIEW = [{
  logo: 'images/customers/sriChaitanya_logo.jpg',
  pic: 'images/customers/srujan.jpg',
  label: 'Sri Chaitanya',
  text: `" Our institute chose this app for sending us online lectures and for
    conducting exams and I gotta say, it really works well. The classes
    have high quality visual and you can change the playback speed as well.
    You can't take screenshots during the exam and after the exam we get individual marks
    for each subject and the total as well. We can even compare our previous scores.
     We can even analyse the toppers score along with ours. Overall, it is a wonderful app."`,
  name: 'Stacy Cordelia',
  designation: 'Student'
}, {
  logo: 'images/customers/rankguru_logo.jpg',
  pic: 'images/customers/laxman.jpg',
  label: 'Rankguru',
  text: `"This app is so good and there is no problem in quality of classes posted in the app. The way of conducting exam
     in the app is so good that every student will be comfortable to write. This app is also good for studies. Online exams
      are also so good.Thank for supporting our institution in keeping exams and online classes So everybody download
       getranks and request your Institute to conduct exams and post classes in getranks"`,
  name: 'lechireddy Surekha',
  designation: 'Student'
}];
const CLIENTS_SET1 = [{
  icon: '/images/customers/ts.png',
  label: 'TS Govt.'
}, {
  icon: '/images/customers/sri_chaithanya.png',
  label: 'Sri Chaithanya'
}, {
  icon: '/images/customers/Rankguru.png',
  label: 'RankGuru'
}, {
  icon: '/images/customers/motion.png',
  label: 'Motion'
}, {
  icon: '/images/customers/ms.png',
  label: 'MS'
}, {
  icon: '/images/customers/tirumala.png',
  label: 'Tirumala'
}, {
  icon: '/images/customers/trinity.png',
  label: 'Trinity'
}, {
  icon: '/images/customers/viswa.png',
  label: 'Sri Viswa'
}, {
  icon: '/images/customers/bhavishya.png',
  label: 'Bhavishya'
}, {
  icon: '/images/customers/pragathi_jr.png',
  label: 'Prgathi Jr.'
}, {
  icon: '/images/customers/sarath.png',
  label: 'Sarath Academy'
}, {
  icon: '/images/customers/pragathi.png',
  label: 'Pragathi'
}, {
  icon: '/images/customers/saraswathi_groups.png',
  label: 'Saraswathi_Groups'
}, {
  icon: '/images/customers/shivani.png',
  label: 'Shivani'
}, {
  icon: '/images/customers/prathibha_jr.png',
  label: 'Prathibha_Jr'
}, {
  icon: '/images/customers/Resonance.png',
  label: 'Resonance'
}, {
  icon: '/images/customers/vijetha.png',
  label: 'Vijetha'
}, {
  icon: '/images/customers/toppers.png',
  label: 'Toppers'
}];
const CLIENTS_SET2 = [{
  icon: 'images/customers/abv.png',
  label: 'ABV'
}, {
  icon: 'images/customers/master_minds.png',
  label: 'Master Minds'
}, {
  icon: 'images/customers/sri_gayathri.png',
  label: 'Sri Gayathri'
}, {
  icon: 'images/customers/pinegrove.png',
  label: 'Pinegrove'
}, {
  icon: 'images/customers/sri_akshara.png',
  label: 'Sri Akshara'
}, {
  icon: 'images/customers/urbane.png',
  label: 'Urbane'
}, {
  icon: 'images/customers/sri_prakash.png',
  label: 'Sri Prakash'
}, {
  icon: 'images/customers/vishra.png',
  label: 'Vishra'
}, {
  icon: 'images/customers/navodaya.png',
  label: 'Navodaya'
}, {
  icon: 'images/customers/newera.png',
  label: 'New Era'
}, {
  icon: 'images/customers/akshaya.png',
  label: 'Akshaya'
}, {
  icon: 'images/customers/gyanam.png',
  label: 'Gyanam'
}, {
  icon: 'images/customers/elegance.png',
  label: 'Elegance'
}, {
  icon: 'images/customers/kakathiya.png',
  label: 'Kakathiya'
}, {
  icon: 'images/customers/ramabanam.png',
  label: 'RamaBanam'
}, {
  icon: 'images/customers/geetham.png',
  label: 'Geetham'
}, {
  icon: 'images/customers/sri_vaishnavi.png',
  label: 'Sri_Vaishnavi'
}, {
  icon: '/images/customers/dhiksha.png',
  label: 'Dhiksha'
}];
const LIVECLASSESDATA = [{
  route: 'Teach',
  features: [{
    icon: 'images/home/New SubMenu Items/Teach/teach.svg',
    title: 'Teach',
    isHeading: true,
    route: 'Teach'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Live.svg',
    title: 'Live',
    isHeading: false,
    route: 'Live'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Assignments.svg',
    title: 'Assignments',
    isHeading: false,
    route: 'Assignments'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Doubts.svg',
    title: 'Doubts',
    isHeading: false,
    route: 'Doubts'
  }]
}, {
  route: 'Test',
  features: [{
    icon: 'images/home/New SubMenu Items/Test/test.svg',
    title: 'Test',
    isHeading: true,
    route: 'Test'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Tests.svg',
    title: 'Tests',
    isHeading: false,
    route: 'Tests'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Analysis.svg',
    title: 'Analysis',
    isHeading: false,
    route: 'Analysis'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Practice.svg',
    title: 'Practice',
    isHeading: false,
    route: 'Practice'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Papers.svg',
    title: 'Papers',
    isHeading: false,
    route: 'Papers'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Reports.svg',
    title: 'Reports',
    isHeading: false,
    route: 'Reports'
  }]
}, {
  route: 'Connect',
  features: [{
    icon: 'images/home/New SubMenu Items/Connect/connect.svg',
    title: 'Connect',
    isHeading: true,
    route: 'Connect'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/schedule.svg',
    title: 'Schedule',
    isHeading: false,
    route: 'Schedule'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/messenger.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/notifications.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }]
}, {
  route: 'Collect',
  features: [{
    icon: 'images/home/New SubMenu Items/Collect/collect.svg',
    title: 'Collect',
    isHeading: true,
    route: 'Collect'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/fee_collection.svg',
    title: 'Fee Collection',
    isHeading: false,
    route: 'FeeCollect'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/E-Receipts.svg',
    title: 'E-Receipts',
    isHeading: false,
    route: 'Receipts'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/auto_payment_remainders.svg',
    title: 'Auto Payment Remainders',
    isHeading: false,
    route: 'AutoPayments'
  }]
}, {
  route: 'Manage',
  features: [{
    icon: 'images/home/New SubMenu Items/Manage/manage.svg',
    title: 'Manage',
    isHeading: true,
    route: 'Manage'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/admissions.svg',
    title: 'Admissions',
    isHeading: false,
    route: 'Admissions'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/attendences.svg',
    title: 'Attendance',
    isHeading: false,
    route: 'Attendance'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/students.svg',
    title: 'Students',
    isHeading: false,
    route: 'Students'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/roles.svg',
    title: 'Roles',
    isHeading: false,
    route: 'Roles'
  }]
}];
const OLD_LIVECLASSESDATA = [{
  route: 'Teach',
  features: [{
    icon: 'images/home/submenuitems/Teach/Teach.svg',
    title: 'Teach',
    isHeading: true,
    route: 'Teach'
  }, {
    icon: 'images/home/submenuitems/Teach/Live.svg',
    title: 'Live',
    isHeading: false,
    route: 'Live'
  }, {
    icon: 'images/home/submenuitems/Teach/Assignments.svg',
    title: 'Assignments',
    isHeading: false,
    route: 'Assignments'
  }, {
    icon: 'images/home/submenuitems/Teach/Doubts.svg',
    title: 'Doubts',
    isHeading: false,
    route: 'Doubts'
  }]
}, {
  route: 'Test',
  features: [{
    icon: 'images/home/submenuitems/Test/Test.svg',
    title: 'Test',
    isHeading: true,
    route: 'Test'
  }, {
    icon: 'images/home/submenuitems/Test/Tests.svg',
    title: 'Tests',
    isHeading: false,
    route: 'Tests'
  }, {
    icon: 'images/home/submenuitems/Test/Analysis.svg',
    title: 'Analysis',
    isHeading: false,
    route: 'Analysis'
  }, {
    icon: 'images/home/submenuitems/Test/Practice.svg',
    title: 'Practice',
    isHeading: false,
    route: 'Practice'
  }, {
    icon: 'images/home/submenuitems/Test/Papers.svg',
    title: 'Papers',
    isHeading: false,
    route: 'Papers'
  }, {
    icon: 'images/home/submenuitems/Test/Reports.svg',
    title: 'Reports',
    isHeading: false,
    route: 'Reports'
  }]
}, {
  route: 'Connect',
  features: [{
    icon: 'images/home/submenuitems/Connect/Connect.svg',
    title: 'Connect',
    isHeading: true,
    route: 'Connect'
  }, {
    icon: 'images/home/submenuitems/Connect/Schedule.svg',
    title: 'Schedule',
    isHeading: false,
    route: 'Schedule'
  }, {
    icon: 'images/home/submenuitems/Connect/Messenger.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }, {
    icon: 'images/home/submenuitems/Connect/Notifications.svg',
    title: 'Notifications',
    isHeading: false,
    route: 'Messenger'
  }]
}, {
  route: 'Collect',
  features: [{
    icon: 'images/home/submenuitems/Collect/Collect.svg',
    title: 'Collect',
    isHeading: true,
    route: 'Collect'
  }, {
    icon: 'images/home/submenuitems/Collect/FeeCollection.svg',
    title: 'Fee Collection',
    isHeading: false,
    route: 'FeeCollect'
  }, {
    icon: 'images/home/submenuitems/Collect/EReceipts.svg',
    title: 'E-Receipts',
    isHeading: false,
    route: 'EReceipts'
  }, {
    icon: 'images/home/submenuitems/Collect/AutoPaymentRemainders.svg',
    title: 'Auto Payment Remainders',
    isHeading: false,
    route: 'AutoPayments'
  }]
}, {
  route: 'Manage',
  features: [{
    icon: 'images/home/submenuitems/Manage/Manage.svg',
    title: 'Manage',
    isHeading: true,
    route: 'Manage'
  }, {
    icon: 'images/home/submenuitems/Manage/Admissions.svg',
    title: 'Admissions',
    isHeading: false,
    route: 'Admissions'
  }, {
    icon: 'images/home/submenuitems/Manage/Attendance.svg',
    title: 'Attendance',
    isHeading: false,
    route: 'Attendance'
  }, {
    icon: 'images/home/submenuitems/Manage/Students.svg',
    title: 'Students',
    isHeading: false,
    route: 'Students'
  }, {
    icon: 'images/home/submenuitems/Manage/Roles.svg',
    title: 'Roles',
    isHeading: false,
    route: 'Roles'
  }]
}];
const MEDIA_COVERAGE = [{
  icon: '/images/press/financial_express-1.png',
  title: 'Surprise! Predictive analysis of students future course of action for career development now possible; Egnify rolls out solution',
  author: 'By: BV Mahalakshmi',
  content: 'For students, it is always learn, relearn and unlearn to hit the top score. But neither the educators nor parents or the students can do a predictive analysis about their future course of action for career development and the dilemma continues for generations.',
  url: 'https://www.financialexpress.com/industry/surprise-predictive-analysis-of-students-future-course-of-action-for-career-development-now-possible-egnify-rolls-out-solution/881282/'
}, {
  icon: '/images/press/yourstory-1.png',
  title: 'This Hyderabad startup is helping students perform better in exams',
  author: 'By Think Change India',
  content: 'Started as an edutech startup and based out of T-Hub, Hyderabad, Egnify employs deep learning, Machine Learning and data science to make a keen analysis of the performance of students. Based on this report, the startup gets to describe where the students need to improve upon, along with a detailed breakdown of their marks.',
  url: 'https://yourstory.com/2017/12/hyderabad-startup-students-exam/'
}, {
  icon: '/images/press/indian_express-1.png',
  title: 'Impetus to excel',
  author: 'By Shyam Yadagiri, Express News Service',
  content: 'Founded in 2015 by Kiran Babu, Egnify is a cloud-based plug-and-play analytics and assessments platform for educational institutions.',
  url: 'http://www.newindianexpress.com/cities/hyderabad/2017/sep/19/impetus-to-excel-1659699.html'
}, {
  icon: '/images/press/asia-1.png',
  title: 'Egnify Improving Education using the deep-tech analytics: Performance',
  author: 'ASIA INC.500',
  content: 'Founded in 2015 by Kiran Babu, Egnify is a cloud-based Assessment and Learning Analytics platform integrated with world-class Analytics to enhance conceptual clarity and Exam Readiness of the student. It is a team of 20 people (IITs, IIITs, IIMs, NITs, Stanford) serving 4,27,000 students, 28,000+ teachers in 400+ institutes across 28 cities in 4 states. Our target is to reach 10 lakh students by end of the academic year 2018.',
  url: 'http://www.asiainc500.com/wp-content/uploads/2018/10/Asia-Inc.-500-Magazine-Oct-2018-D91.pdf'
}, {
  icon: '/images/press/deccan-1.png',
  title: 'Pilot project to mould state school kids in Telangana',
  author: 'DECCAN CHRONICLE. | NAVEENA GHANATE',
  content: 'Hyderabad: Emulating the model followed by private institutions, the Telangana state government is running a pilot project to strengthen the weak areas of students in government schools through technology. This would eventually raise the pass percentage.',
  url: 'https://www.deccanchronicle.com/nation/current-affairs/120118/pilot-project-to-mould-state-school-kids-in-telangana.html'
}, {
  icon: '/images/press/the_news_minute-1.png',
  title: 'Telangana ties up with T-Hub startup Egnify to improve education in govt schools',
  author: 'Shilpa S Ranipeta',
  content: 'The Telangana government is looking to increase quality of education in government schools across the state, and wants to use technology for this. In the latest development, it wants to measure the strengths and weaknesses of students in government schools across subjects, assess it and look for ways to improve the same using technology.',
  url: 'https://www.thenewsminute.com/article/telangana-ties-t-hub-startup-egnify-improve-education-govt-schools-74652'
}, {
  icon: '/images/press/telangana_today-1.png',
  title: 'Hyderabad-based startup Egnify Technologies helps students excel',
  author: 'By Sruti Venugopal',
  content: 'Working out of T-Hub, the startup is a cloud-based holistic student performance improvement platform that through data and artificial intelligence measures the weaknesses and strengths of a student and provides specific solution.',
  url: 'https://telanganatoday.com/hyderabad-based-startup-egnify-technologies-helps-students-excel'
}, {
  icon: '/images/press/andhrajyothi-1.png',
  title: 'మార్కుల విశ్లేషణకూ ఓ టెక్నిక్‌',
  content: 'పరీక్షలో ఒక విద్యార్థికి 52 మార్కులు వచ్చాయి. సరే, మరి తతిమా 48 మార్కులు ఎందుకు రాలేదు అన్నది వెంటనే కలగాల్సిన సందేహం. ఎక్కడ వీక్‌గా ఉన్నారో తెలిస్తే సవరించుకోవచ్చు. తద్వారా మార్కులను పెంచుకోవచ్చు. దీన్ని టెక్నాలజీ సహకారంతో విద్యార్థి ప్రతిభను విశ్లేషించే పనిని చేపట్టిన సంస్థ ఎగ్నిఫై.',
  url: 'http://www.andhrajyothy.com/artical?SID=461827'
}];
const PRESS_VIDEO_COVERAGE = [{
  icon: '/images/home/media/on_news/etv_telangana.webp',
  icon1: '/images/home/media/on_news/etv_telangana-1.png',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M',
  title: 'Govt Ties Up with Getranks by Egnify Startup to Analyze School Students Lagging Behind in Education'
}, {
  icon: '/images/home/media/on_news/kiran_sir_on_T 20news.webp',
  icon1: '/images/home/media/on_news/kiran_sir_on_T news-1.png',
  title: 'Getranks by Egnify_CEO Kiran Babu_Hyderabad_TNews_T Hub',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}, {
  icon: '/images/home/media/on_news/students_on_v6.webp',
  icon1: '/images/home/media/on_news/students_on_v6-1.png',
  title: 'Special Story on Getranks by Egnify in Hyderabad V6 News',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}];
const VIEWMORE = [{
  color: '#ff6400',
  icon: '/images/home/New SubMenu Items/arrows/teach-arrow.svg'
}, {
  color: '#8800fe',
  icon: '/images/home/New SubMenu Items/arrows/test-arrow.svg'
}, {
  color: '#00b800',
  icon: '/images/home/New SubMenu Items/arrows/connect-arrow.svg'
}, {
  color: '#be00cc',
  icon: '/images/home/New SubMenu Items/arrows/collect-arrow.svg'
}, {
  color: '#e5ac00',
  icon: '/images/home/New SubMenu Items/arrows/manage-arrow.svg'
}];
const TESTS_KEY_POINTS = [{
  id: 1,
  point: 'Unlimitted test creation'
}, {
  id: 2,
  point: 'Instant test result and Reports available online & offline - Rank list, Analysis, and Reports after every test.'
}, {
  id: 3,
  point: 'Pre-defined marking schemes'
}, {
  id: 4,
  point: 'Custom marking schemes'
}, {
  id: 5,
  point: 'Getranks supports all 22 types of JEE avanced Marking Schemes starting from 2009 - 2019'
}, {
  id: 6,
  point: 'Grace period option available'
}, {
  id: 7,
  point: 'Live Attendance'
}];
const newModulesData = [{
  name: 'Live',
  icon: 'images/home/New SubMenu Items/Teach/old_Live.svg'
}, {
  name: 'Assignments',
  icon: 'images/home/New SubMenu Items/Teach/old_Assignments.svg'
}, {
  name: 'Doubts',
  icon: 'images/home/New SubMenu Items/Teach/old_Doubts.svg'
}, {
  name: 'Tests',
  icon: 'images/home/New SubMenu Items/Test/module_test.svg'
}, {
  name: 'Connect',
  icon: 'images/home/New SubMenu Items/Connect/new_connect.svg'
}];
const moduleKeyPoints = {
  tests: {
    points: ['Unlimited test creation', '100,000 student concurrency', 'Conduct online and offline tests', 'Advanced Proctoring features', 'Pre-defined marking schemas and custom marking schemas', 'Grace period option avaialbe', 'Live attendance dashboard']
  },
  assignments: {
    points: ['Administer and grade online and in-class assessments', 'Grade multiple assessment types, including handwritten assessments', 'Evaluate using mobile app and in a web browser', 'Improve student feedback loop and provide an opportunity to enrich the learning experience through analytics']
  },
  doubts: {
    points: ['Doubts can turn up any time', 'Ask until it is resolved', 'Answer doubt anytime, anywhere', 'Access and Analysis doubts']
  },
  schedule: {
    points: ['Save time and make the most of every day', `Bring your student, teacher's timetable to life and make it easy to access what's ahead with links, attachments all other details`, 'All your activities in one place']
  }
};
const cloneDeeply = obj => {
  const newObj = JSON.parse(JSON.stringify(obj));
  return newObj;
};
const HOW_WORKS = [{
  title: 'Onboarding',
  content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.`,
  img: `/images/Test/demo.png`
}, {
  title: 'Institution Setup',
  content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.`,
  img: `/images/Test/demo.png`
}, {
  title: 'Add Hierarchies',
  content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.`,
  img: `/images/Test/demo.png`
}, {
  title: 'Add Students',
  content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.`,
  img: `/images/Test/demo.png`
}];
const HOW_WORKS_TITLES = ['Onboarding', 'Institution Setup', 'Add Hierarchies', 'Add Students'];
const TEST_VIDEOS = [{
  url: '/images/Test/videos/Conduct offline-online Tests.mp4',
  title: 'Conduct Offline/ Online Test'
}, {
  url: '/images/Test/videos/Create Own Question Papers.mp4',
  title: 'Upload Own Question Papers'
}, {
  url: '/images/Test/videos/Upload own question paper.mp4',
  title: 'Create Own Question Papers'
}, {
  url: '/images/Test/videos/Indepth Analysis.mp4',
  title: 'Indepth Analysis'
}];

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzLzEuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9HZXRSYW5rc0NvbnN0YW50cy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgSE9NRV9DTElFTlRTID0gW1xuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvZ292X29mX3RlbGFuZ2FuYS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvc3JpX2NoYWl0aGFueWEucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3JlcXVlc3QtZGVtby9jbGllbnRzL3JhbmtndXJ1LnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9tcy5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvbW90aW9uLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy90aXJ1bWFsYS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvdHJpbml0eS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvc3JpX3Zpc2h3YS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvc3JpX2JoYXZpc2h5YS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvc2FyYXRoX2FjYWRlbXkucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3JlcXVlc3QtZGVtby9jbGllbnRzL3ByYWdhdGhpLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9zcmlfZ2F5YXRocmkucG5nJyxcbiAgfSxcbl07XG5leHBvcnQgY29uc3QgSE9NRV9DTElFTlRTX1VQREFURUQgPSBbXG4gIHtcbiAgICBpZDogJ2dvdl9vZl90ZWxhbmdhbmEnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9nb3Zfb2ZfdGVsYW5nYW5hX2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3NyaV9jaGFpdGhhbnlhJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc3JpX2NoYWl0aGFueWFfYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAncmFua19ndXJ1JyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvcmFua2d1cnVfYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAnbXMnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9tc19ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdtb3Rpb24nLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9Nb3Rpb25fYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAndGlydW1hbGEnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9UaXJ1bWFsYV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICd0cmluaXR5JyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvdHJpbml0eV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdzcmlfdmlzaHdhJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc3JpX3Zpc2h3YV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdzcmlfYmhhdmlzaHlhJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc3JpX2JoYXZpc2h5YV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdzYXJhdGhfQWNhZGVteScsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9jbGllbnRzL3NhcmF0aF9BY2FkZW15X2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3ByYWdhdGhpJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvcHJhZ2F0aGlfYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAnc3JpX2dheWF0aHJpJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc3JpX2dheWF0aHJpX2JsYWNrLnBuZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9NRV9CRU5FRklUUyA9IFtcbiAge1xuICAgIGhlYWRpbmc6ICdJbnN0aXR1dGVzJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXkuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdUZWFjaGVycycsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnU3R1ZGVudHMnLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGluZzogJ1BhcmVudHMnLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEhPTUVfQkVORUZJVFNfREFUQSA9IFtcbiAge1xuICAgIGhlYWRpbmc6ICdTbWFydGVyIEF1dG9tYXRpb24nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnR2V0UmFua3MgZ2V0cyB5b3UgdXAgYW5kIHJ1bm5pbmcgaW4gbm8gdGltZSBhbmQgd2l0aCBvdXIgaGFuZHMtb24gY3VzdG9tZXIgc3VjY2VzcyB0ZWFtIGFuZCBhdXRvbWF0ZWQgcHJvY2Vzc2VzIHlvdSBjYW4gY29uZHVjdCB0ZXN0cyBhbmQgYW5hbHlzZSBzdHVkZW50IGRhdGEgaW4gbWludXRlcycsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnSW50ZWxsaWdlbnQgSW5zaWdodHMnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnT3VyIGRldGFpbGVkIGRhc2hib2FyZHMgaGVscCB5b3UgdW5kZXJzdGFuZCBzdHVkZW50c+KAmSBwZXJmb3JtYW5jZSBhbmQgaGVscCB0aGVtIHJlbWVkaWF0ZSBpbiBhIHBlcnNvbmFsaXNlZCBtYW5uZXIgc3VpdGVkIHRvIHRoZWlyIG5lZWRzJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXkuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdPbmxpbmUgVGVzdGluZyBQbGF0Zm9ybScsXG4gICAgY29udGVudDpcbiAgICAgICdHZXRSYW5rcyBoZWxwcyB5b3Ugc2ltdWxhdGUgdGhlIGFjdHVhbCB0ZXN0IGNvbmRpdGlvbnMgYW5kIHBsYXRmb3JtIHdoaWNoIGhlbHBzIHlvdXIgc3R1ZGVudHMgcHJlcGFyZSB3ZWxsIGZvciB0aGUgYWN0dWFsIGV4YW0nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEhPTUVfQVdBUkRTID0gW1xuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvd2VjMS5wbmcnLFxuICAgIGNvbnRlbnQ6ICc1MCBGYWJ1bG91cyBFZHV0ZWNoIExlYWRlcnMnLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy93ZWMucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2lpdC12YXJhbmFzaS5wbmcnLFxuICAgIGNvbnRlbnQ6ICdCZXN0IEFuYWx5dGljcyBBcHAgb2YgMjAxNycsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2JsYS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvYnJhaW5mZWVkLnBuZycsXG4gICAgY29udGVudDogJ0Jlc3QgTGVhcm5pbmcgQW5hbHl0aWNzIENvbXBhbnkgb2YgMjAxOCcsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2JmLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9oaWdoZXJFZHVjYXRpb24ucG5nJyxcbiAgICBjb250ZW50OiAnQmVzdCBBZGFwdGl2ZSBUZWNobm9sb2d5IFNvbHV0aW9uJyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvaGVwLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9pcy5wbmcnLFxuICAgIGNvbnRlbnQ6ICdPdXRzdGFuZGluZyBFZFRlY2ggU3RhcnR1cHMnLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9pcy5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvY2lvLnN2ZycsXG4gICAgY29udGVudDogJzIwIE1vc3QgcHJvbWlzaW5nIEVkLVRlY2ggc3RhcnR1cHMgb2YgMjAxOCcsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2Npby5wbmcnLFxuICB9LFxuXTtcbmV4cG9ydCBjb25zdCBIT01FX05FV1MgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3MvZXR2X3RlbGFuZ2FuYS53ZWJwJyxcbiAgICB1cmw6ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PXd1WmNlWVJyM19NJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9raXJhbl9zaXJfb25fVCBuZXdzLndlYnAnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9T2JzelA5dWpVeDQnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9vbl9uZXdzL3N0dWRlbnRzX29uX3Y2LndlYnAnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9aG1YMWp5emF0OVEnLFxuICB9LFxuXTtcbmV4cG9ydCBjb25zdCBIT01FX01FRElBID0gW1xuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9maW5hbmNpYWxfRXhwcmVzcy53ZWJwJyxcbiAgICBuYW1lOiAnZmluYW5jaWFsX0V4cHJlc3MnLFxuICAgIHVybDpcbiAgICAgICdodHRwczovL3d3dy5maW5hbmNpYWxleHByZXNzLmNvbS9pbmR1c3RyeS9zdXJwcmlzZS1wcmVkaWN0aXZlLWFuYWx5c2lzLW9mLXN0dWRlbnRzLWZ1dHVyZS1jb3Vyc2Utb2YtYWN0aW9uLWZvci1jYXJlZXItZGV2ZWxvcG1lbnQtbm93LXBvc3NpYmxlLWVnbmlmeS1yb2xscy1vdXQtc29sdXRpb24vODgxMjgyLycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL3lvdXJzdG9yeS53ZWJwJyxcbiAgICBuYW1lOiAneW91cnN0b3J5JyxcbiAgICB1cmw6ICdodHRwczovL3lvdXJzdG9yeS5jb20vMjAxNy8xMi9oeWRlcmFiYWQtc3RhcnR1cC1zdHVkZW50cy1leGFtLycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2luZGlhbi1leHByZXNzLndlYnAnLFxuICAgIG5hbWU6ICdpbmRpYW4tZXhwcmVzcycsXG4gICAgdXJsOlxuICAgICAgJ2h0dHA6Ly93d3cubmV3aW5kaWFuZXhwcmVzcy5jb20vY2l0aWVzL2h5ZGVyYWJhZC8yMDE3L3NlcC8xOS9pbXBldHVzLXRvLWV4Y2VsLTE2NTk2OTkuaHRtbCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2FzaWE1MDAucG5nJyxcbiAgICBuYW1lOiAnaW5kaWFuLWFzaWE1MDAnLFxuICAgIHVybDpcbiAgICAgICdodHRwOi8vd3d3LmFzaWFpbmM1MDAuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE4LzEwL0FzaWEtSW5jLi01MDAtTWFnYXppbmUtT2N0LTIwMTgtRDkxLnBkZicsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2RlY2Nhbi53ZWJwJyxcbiAgICBuYW1lOiAnaW5kaWFuLWRlY2NhbicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vd3d3LmRlY2NhbmNocm9uaWNsZS5jb20vbmF0aW9uL2N1cnJlbnQtYWZmYWlycy8xMjAxMTgvcGlsb3QtcHJvamVjdC10by1tb3VsZC1zdGF0ZS1zY2hvb2wta2lkcy1pbi10ZWxhbmdhbmEuaHRtbCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2V0di53ZWJwJyxcbiAgICBuYW1lOiAnZXR2JyxcbiAgICB1cmw6ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PXd1WmNlWVJyM19NJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvbmV3c19taW51dGUud2VicCcsXG4gICAgbmFtZTogJ25ld3NfbWludXRlJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly93d3cudGhlbmV3c21pbnV0ZS5jb20vYXJ0aWNsZS90ZWxhbmdhbmEtdGllcy10LWh1Yi1zdGFydHVwLWVnbmlmeS1pbXByb3ZlLWVkdWNhdGlvbi1nb3Z0LXNjaG9vbHMtNzQ2NTInLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS90ZWxhbmdhbmEtdG9kYXkud2VicCcsXG4gICAgbmFtZTogJ3RlbGFuZ2FuYS10b2RheScsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vdGVsYW5nYW5hdG9kYXkuY29tL2h5ZGVyYWJhZC1iYXNlZC1zdGFydHVwLWVnbmlmeS10ZWNobm9sb2dpZXMtaGVscHMtc3R1ZGVudHMtZXhjZWwnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9hbmRocmFqeW90aGkud2VicCcsXG4gICAgbmFtZTogJ2FuZGhyYWp5b3RoaScsXG4gICAgdXJsOiAnaHR0cDovL3d3dy5hbmRocmFqeW90aHkuY29tL2FydGljYWw/U0lEPTQ2MTgyNycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL3Y2bjEud2VicCcsXG4gICAgbmFtZTogJ1Y2IG5ld3MnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9aG1YMWp5emF0OVEnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9uYW1hc3RoZS10ZWxhbmdhbmEud2VicCcsXG4gICAgbmFtZTogJ25hbWFzdGhlLXRlbGFuZ2FuYScsXG4gICAgdXJsOiAnaHR0cDovL2VwYXBlci5udG5ld3MuY29tL2MvMjI5NTk4OTknLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS90LW5ld3Mud2VicCcsXG4gICAgbmFtZTogJ3QtbmV3cycsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1PYnN6UDl1alV4NCcsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9NRV9BUFBST0FDSCA9IFtcbiAge1xuICAgIGhlYWRpbmc6ICdGb3IgSW5zdGl0dXRlcycsXG4gICAgY29udGVudDpcbiAgICAgICdPdXIgcGxhdGZvcm0gaGVscHMgaW5zdGl0dXRlcyB0byBjb25kdWN0IG9ubGluZSBhbmQgb2ZmbGluZSB0ZXN0cyBmb3IgdGhlaXIgc3R1ZGVudHMgYWNyb3NzIG11bHRpcGxlIHRlc3QgcGF0dGVybnMuIE91ciBkZXRhaWxlZCBkYXRhIGRhc2hib2FyZHMgaGVscHMgeW91IHVuZGVyc3RhbmQgeW91ciBzdHVkZW50cyBwZXJmb3JtYW5jZSBhbmQgaGVscHMgeW91IGd1aWRlIHRoZW0gdG8gc3VjY2VzcycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnRm9yIFN0dWRlbnRzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0dldFJhbmtzIGVtcG93ZXJzIHN0dWRlbnRzIHdpdGggaW4tZGVwdGggYW5hbHlzaXMgb2YgdGhlaXIgdGVzdCBwZXJmb3JtYW5jZSBhY3Jvc3MgQ29tcGV0ZW5jZSBhbmQgQmVoYXZpb3VyYWwgYXNwZWN0cyB0byBoZWxwIHRoZW0gaW1wcm92ZSB0aGVpciBwZXJmb3JtYW5jZSBhbmQgcGF2ZSB0aGUgd2F5IHRvIHRoZWlyIGRyZWFtIGNvbGxlZ2UnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEhPTUVfQ0xJRU5UX1NBWSA9IFtcbiAge1xuICAgIGNvbnRlbnQ6IGBPdXIgaW5zdGl0dXRlIGNob3NlIHRoaXMgYXBwIGZvciBzZW5kaW5nIHVzIG9ubGluZSBsZWN0dXJlcyBhbmQgZm9yIGNvbmR1Y3RpbmcgZXhhbXMgYW5kIEkgZ290dGEgc2F5LFxuICAgIGl0IHJlYWxseSB3b3JrcyB3ZWxsLiBUaGUgY2xhc3NlcyBoYXZlIGhpZ2ggcXVhbGl0eSB2aXN1YWwgYW5kIHlvdSBjYW4gY2hhbmdlIHRoZSBwbGF5YmFjayBzcGVlZCBhcyB3ZWxsLlxuICAgICBZb3UgY2FuJ3QgdGFrZSBzY3JlZW5zaG90cyBkdXJpbmcgdGhlIGV4YW0gYW5kIGFmdGVyIHRoZSBleGFtIHdlIGdldCBpbmRpdmlkdWFsIG1hcmtzIGZvclxuICAgICBlYWNoIHN1YmplY3QgYW5kIHRoZSB0b3RhbCBhcyB3ZWxsLiBXZSBjYW4gZXZlbiBjb21wYXJlIG91ciBwcmV2aW91cyBzY29yZXMuIFdlIGNhbiBldmVuIGFuYWx5c2UgdGhlIHRvcHBlcnNcbiAgICAgc2NvcmUgYWxvbmcgd2l0aCBvdXJzLiBPdmVyYWxsLCBpdCBpcyBhIHdvbmRlcmZ1bCBhcHAuYCxcbiAgICBuYW1lOiAnU3RhY3kgQ29yZGVsaWEnLFxuICAgIGRlc2lnbmF0aW9uOiAnU3R1ZGVudCcsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc3JpLWNoYWl0YW55YS5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvaWNvbnMvcGVyc29uX2JsdWUuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6IGBUaGlzIGFwcCBpcyBzbyBnb29kIGFuZCB0aGVyZSBpcyBubyBwcm9ibGVtIGluIHF1YWxpdHkgb2YgY2xhc3NlcyBwb3N0ZWQgaW4gdGhlIGFwcC4gVGhlIHdheSBvZiBjb25kdWN0aW5nIGV4YW1cbiAgICAgaW4gdGhlIGFwcCBpcyBzbyBnb29kIHRoYXQgZXZlcnkgc3R1ZGVudCB3aWxsIGJlIGNvbWZvcnRhYmxlIHRvIHdyaXRlLiBUaGlzIGFwcCBpcyBhbHNvIGdvb2QgZm9yIHN0dWRpZXMuIE9ubGluZSBleGFtc1xuICAgICAgYXJlIGFsc28gc28gZ29vZC5UaGFuayBmb3Igc3VwcG9ydGluZyBvdXIgaW5zdGl0dXRpb24gaW4ga2VlcGluZyBleGFtcyBhbmQgb25saW5lIGNsYXNzZXMgU28gZXZlcnlib2R5IGRvd25sb2FkIGdldHJhbmtzXG4gICAgICBhbmQgcmVxdWVzdCB5b3VyIEluc3RpdHV0ZSB0byBjb25kdWN0IGV4YW1zIGFuZCBwb3N0IGNsYXNzZXMgaW4gZ2V0cmFua3NgLFxuICAgIG5hbWU6IGBsZWNoaXJlZGR5IFN1cmVraGFgLFxuICAgIGRlc2lnbmF0aW9uOiBgU3R1ZGVudGAsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvcGluZWdyb3ZlLmpwZWcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvaWNvbnMvcGVyc29uX2JsdWUuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6IGBUaGlzIGlzIHdheSB0b28gdXNlZnVsIGZvciBKRUUgYXNwaXJhbnRzIGFuZCB0aGUgVXNlciBJbnRlcmZhY2UgaXMgdG9vIHNpbXBsZSB0byB1c2UgYW5kIHRoaXMgYXBwIGlzIGFkaGVyZWRcbiAgICAgdG8gcHJpdmFjeSBvZiBlYWNoIGFuZCBldmVyeSB1c2VyLiBBcGFydCBmcm9tIHRoZSBpbnN0aXR1dGlvbidzIG1haW4gZXhhbXMgdGhleSBhbHNvIHByb3ZpZGUgdXMgd2l0aCB0aGUgcHJhY3RpY2VcbiAgICAgIHRlc3RzIHdoaWNoIGFyZSBlZmZpY2llbnQgdG9vLiBUaGUgaWRlYSBvZiBpbmNsdWRpbmcgem9vbSBtZWV0aW5ncyBpbiB0aGUgYXBwIHdhcyB0b28gaW5jcmVkaWJsZfCfkY0uYCxcbiAgICBuYW1lOiAnSGVtdWJ1bm55IEthc2lyZWRkeScsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NZWx1aGEucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL2ljb25zL3BlcnNvbl9ibHVlLnN2ZycsXG4gIH0sXG4gIC8qIHtcbiAgICBjb250ZW50OiBgQW4gRXhjZWxsZW50IGFwcCBmb3IgYWNhZGVtaWNzIC5UaGlzIGFwcCBoZWxwcyB1cyB0byBjb21wYXJlIG91ciBwZXJmb3JtYW5jZSB3aXRoIG90aGVyIHBlb3BsZSAuSXQgYWxzb1xuICAgICBoZWxwcyB1cyB0byBjb21wYXJlIG91ciBwZXJmb3JtYW5jZSB3aXRoIHRoZSBwcmV2aW91cyBvbmUuIEl0IGhlbHBzIHVzIGluIHVuZGVyc3RhbmRpbmcgb3VyIHN0cmVuZ3RocyAmIHdlYWtuZXNzIC5cbiAgICAgVGhlcmUgaXMgZ3JhcGhpY2FsIGFuYWx5c2lzIHRvIG1ha2UgY29tcGFyaXNvbiBldmVuIG1vcmUgZWFzaWVyIC4gVGhlIGFwcCBhbHNvIHNob3dzIHRoZSB0aW1lIHdlIGhhdmUgc3BlbnQgb24gZWFjaFxuICAgICAgcXVlc3Rpb24gJiB0aW1lIHNwZW50IGZvciBkb2luZyByaWdodCAmIHdyb25nIGFuc3dlcnMgLlRoZSBhcHAgYWxzbyBlbmFibGVzIHVzIHRvIHdhdGNoIHZpZGVvIGxlY3R1cmVzIHVwbG9hZGVkXG4gICAgICAgYnkgaW5zdGl0dXRlIC4gT3ZlcmFsbCBJIHRoaW5rIHRoZSBhcHAgaXMgYXdlc29tZSAuYCxcbiAgICBuYW1lOiBgU3JpIFJhbmdhYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvaWNvbnMvcGVyc29uX2JsdWUuc3ZnJyxcbiAgfSwgKi9cbiAge1xuICAgIGNvbnRlbnQ6IGBUaGlzIGFwcCBoYXMgZXhjZWxsZW50IHRvcCBub3RjaCBmZWF0dXJlcyB3aGljaCBoZWxwcyBzdHVkZW50cyBnZXQgdXAgdG8gdGhlIG1hcmsgZXZlbiBpbiB0aGUgcGFuZGVtaWMuXG4gICAgIFRoZXJlIGFyZSBubyBlcnJvcnMgYW5kIGl0IHdvcmtzIHBlcmZlY3RseSBmaW5lLiBFdmVuIHdoZW4gYWxsIHRoZSBhY2FkZW1pY3Mgc2hpZnRlZCB0byBhIHdob2xlIG9ubGluZSBwcm9jZXNzLFxuICAgICAgdGhpcyBhcHAga2VlcHMgaXRzIG1hcmsgdXAuIEdvb2Qgb25lIGFuZCBpIHJlYWxseSBzdWdnZXN0YCxcbiAgICBuYW1lOiBgU3JpbGFrc2htaSBrYW5kdWxhYCxcbiAgICBkZXNpZ25hdGlvbjogYFBhcmVudGAsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvTU9USU9OLnBuZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9pY29ucy9wZXJzb25fYmx1ZS5zdmcnLFxuICB9LFxuICAvKiB7XG4gICAgY29udGVudDogYFRoaXMgYXBwIGlzIHZlcnkgaGVscGZ1bCB0byBtZSB0byBzdHVkeS4gTm93LCBpdCBpcyB2ZXJ5IGVhc3kgdG8gbWUgdG8gbGlzdGVuIG15IGNsYXNzZXMgb3IgdG8gcHJhY3RpY2VcbiAgICAgYW55IHN1YmplY3QuSXQgaXMgcmVhbGx5IGEgZ3VkIHRoaW5nICxpdCBoYXMgbm8gYWRzLkFuZCB2ZXJ5IGludGVyZXN0aW5nIHRlc3RzLkkgdXNlZCB0YmlzYXBwXG4gICAgICB2ZXJ5IHdlbGwgSSB3aXNoIHRoaXAgYXBwIHdpbGwgaGVscGZ1bCB0byBtYW55IG9mIHRoZSBzdHVkZW50cyB3aG8gYXJlIGludGVyZXN0ZWR0byBzdHVkeS5cbiAgICAgICBJIGNvbmdyYXR1bGF0ZSB0aGUgYXBwIGZvciB0aGUgd29yayB0aGV5IG1hZGUuLi4uLiBUaGFuayB5b3UgIS4uLi4u8J+RjWAsXG4gICAgbmFtZTogYFZhbmFwYXJ0aGkgTWFkaGF2aWAsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYFRoZSBhcHAgaXMgdmVyeSBjb252ZW5pZW50IGFuZCBoZWxwZnVsLiBUaGUgd2F5IGl0IHNob3dzIHRoZSBBbmFseXNpcyBvZiB0aGUgZXhhbXMgYW5kIHRoZSByZXZpZXcgZm9ybWF0XG4gICAgIG9mIHRoZSBleGFtcyBhcmUgZXhjZWxsZW50Li4gVGhhbmtzIGZvciB0aGUgYXBwLmAsXG4gICAgbmFtZTogYGAsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYEJlc3QgYW5kIGV4Y2VsbGVudCBhcHAuIEl0IGNsZWFybHkgc2hvd3MgbWUgcGVyY2VudGFnZSBhbmQgdGhlIHBlcmNlbnRpbGUgb2YgbXkgcGVyZm9ybWFuY2UgaW4gYm90aCB0aGUgbWFpbnNcbiAgICBhbmQgYWR2YW5jZSBleGFtLiBBbmQgdGhlIGJlc3QgdGhpbmcgb2YgdGhpcyBhcHAgaXMsIGl0IGhhZCBwcmFjdGljZSBwYXBlcnMgZXZlbi5UaGFua3lvdSBmb3IgY3JlYXRpbmcgc3VjaCBhblxuICAgIGV4Y2VsbGVudCBhcHAuYCxcbiAgICBuYW1lOiBgU2F0eWF2YXRoaSBOdW5pYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15X3Byb2ZpbGUuanBnJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6IGBHZXRyYW5rcyBpcyBhIGdvb2QgYXBwLndoZXJlIHdlIGNhbiBzZWUgYW5kIGxpc3RlbiB0byBsZWN0dXJlcyBwb3N0ZWQgYnkgb3VyIHRlYWNoZXJzIGFuZCB3ZSBjYW4gd3JpdGUgdGhlIHRlc3RzLlxuICAgIG9uY2Ugd2UgY29tcGxldGVkIG91ciB0ZXN0ICx3ZSBnZXQgb3VyIHJlc3VsdCBpbW1lZGlhdGVseSB3aXRob3V0IGFueSBkZWxheS4ud2UgY2FuIGFsc28gZ2V0IHRoZSBzb2x1dGlvbnMgZm9yXG4gICAgdGhvc2UgcXVlc3Rpb25zIGluIHRoZSB0ZXN0Li5gLFxuICAgIG5hbWU6IGBTZWV0aGFsYSBTcmluaWRoaWAsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYFRoaXMgaXMgb25lIG9mIHRoZSBiZXN0IGFwcCBmb3IgZWR1Y3Rpb25hbCBwdXJwb3NlLCB0aGV5IHdlcmUgZ2l2aW5nIG5ldyBmZWF0dXJlcyBmb3IgZXZlcnkgdXBkYXRlIGFzIGxpa2VcbiAgICAgY29uZHVjdGluZyB6b29tIG1lZXRpbmdzIGluIGFwcCBpdCBzZWxmIC4gdGhhbmtzIGZvciBhbGwgd2hvIHdlcmUgd29ya2luZyB1bmRlciB0aGlzIGFwcCBieSBnaXZpbmcgeW91ciBiZXN0IGZvciB1cy5gLFxuICAgIG5hbWU6IGBIQVNJVEggVkFSREhBTmAsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYFRoZSBiZXN0IGFwcCBmb3IgYW55IHN0dWRlbnQgLlRoaXMgYXBwIGlzIG9uZSBvZiB0aGUgbW9zdCB1c2VmdWwgYXBwIC5JdCByZWFsbHkgc2hvd3MgbWUgd2hldGhlciBJIGhhZFxuICAgIGltcHJvdmVkIG9yIG5vdCBpbiB0aGUgZXhhbWluYXRpb24gYnkgZ2l2aW5nIG15IGFjY3VyYWN5IGFuZCBhdHRlbXB0IGZvciBlYWNoIHN1YmplY3QgLiBUaGUgb3B0aW9uIGNvbXBhcmUgd2l0aCB0aGVcbiAgICB0b3BwZXIgcmVhbGx5IHNob3dzIG1lIHN0aWxsIGhvdyBtdWNoIEkgaGF2ZSB0byBpbXByb3ZlIC4gVGhhbmtzIGZvciB0aGUgY3JlYXRvcnMgd2hvIGNyZWF0ZWQgc3VjaCBhbiB3b25kZXJmdWwgYXBwLmAsXG4gICAgbmFtZTogYFN1cnlhIEJoYXNrYXJhIFJhbyBDaGVrdXJpYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OiBgTmljZSBhcHAgZm9yIGplZSBtYWlucyBwcmVwYXJhdGlvbiBpbiB0aGVzZSBjaXJjdW1zdGFuY2VzLiBUaGVyZSBhcmUgaW52aWR1YWwgcHJhY3RpY2UgdGVzdHMgb2Ygc3ViamVjdCBhbmRcbiAgICBjaGFwdGVyIHByYWN0aWNlIC4gV2hpY2ggY2FuIGhlbHAgdG8gc3RyZW5ndGhlbiBvdXIgd2VhayBhcmVhcyB3aXRoIGplZSBtYWluIHR5cGUgbW9jayB0ZXN0LiBUaGFuayB5b3UgdGVhbSBlZ25pZnlgLFxuICAgIG5hbWU6IGBSaXNoaXRoYSBzOTlgLFxuICAgIGRlc2lnbmF0aW9uOiBgYCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LCAqL1xuXTtcblxuZXhwb3J0IGNvbnN0IEFTU0VTU19JTkRFUFRIID0gW1xuICB7XG4gICAgaGVhZGluZzogJ01hcmtzIEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ091ciBNYXJrcyBBbmFseXNpcyBoZWxwcyB5b3UgaWRlbnRpZnkgdGhlIGF0LXJpc2sgYW5kIGp1c3QtbWlzc2VkIHN0dWRlbnRzIGFuZCBlbmFibGVzIHlvdSB0byBzZXQgY3VzdG9taXNlZCBnb2FscyBhbmQgbnVkZ2UgdGhlbSBmb3J3YXJkLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2Fzc2Vzcy1zY3JlZW5zaG90NC5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGluZzogJ0Vycm9yIEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ05lZ2F0aXZlIG1hcmtpbmcgaXMgdGhlIGJhbmUgdGhhdCBwdWxscyB5b3VyIHN0dWRlbnRzIHBlcmZvcm1hbmNlIGRvd24uIFBpbnBvaW50IHRoZSBhcmVhcyB0aGF0IG5lZWRzIHRvIGJlIGFkZHJlc3NlZCBhbmQgYm9vc3QgdGhlIHBlcmZvcm1hbmNlIG9mIHN0dWRlbnRzIGJ5IG1pbmltaXNpbmcgZXJyb3JzJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYXNzZXNzLXNjcmVlbnNob3Q1LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnQ29uY2VwdCBBbmFseXNpcycsXG4gICAgY29udGVudDpcbiAgICAgICdTdHVkZW50cyBwZXJmb3JtYW5jZSBpbiBTdWJqZWN0cyBpcyBqdXN0IG5vdCBlbm91Z2guIEdldFJhbmtzIGdvZXMgZnVydGhlciBhbmQgZ2l2ZXMgeW91IGEgZGV0YWlsZWQgcGljdHVyZSBvZiB0aGUgaGVhbHRoIG9mIHlvdSBzdHVkZW50cyBhY3Jvc3MgdG9waWNzIGFuZCBzdWJ0b3BpY3MgY292ZXJlZCBpbiB0aGUgdGVzdC4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9hc3Nlc3Mtc2NyZWVuc2hvdDYuc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBBTkFMWVpFX0NPTlRFTlRfMSA9IFtcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdEZWRpY2F0ZWQgU3R1ZGVudCBQcm9maWxlJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdUaGUgb25lIHN0b3AgdGhhdCBnaXZlcyB5b3UgdGhlIGRldGFpbGVkIHN0dWRlbnQgcGVyZm9ybWFuY2UgYW5hbHlzaXMgd2l0aCBlYXN5IHRvIHVuZGVyc3RhbmQgZGFzaGJvYXJkcyBhbmQgcGVyZm9ybWFuY2UgdHJlbmRzIGFjcm9zcyB5b3VyIE1hcmtzLCBUb3BpY3MgYW5kIEJlaGF2aW91cmFsIGFzcGVjdHMnLFxuICAgIGJ1dHRvbjogJ0NMSUNLIFRPIEtOT1cnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90MS5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ1F1ZXN0aW9uIFJldmlldycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnUmV2aWV3IHlvdXIgdGVzdCBwZXJmb3JtYW5jZSBxdWVzdGlvbi1ieS1xdWVzdGlvbiBhbmQgZ2V0IGFuIHVuZGVyc3RhbmRpbmcgb2YgaG93IHlvdXIgcGVlcnMgcGVyZm9ybWVkIGluIGVhY2ggb2YgdGhlbSAtIGFjcm9zcyBjb21wZXRlbmNlIGFuZCBiZWhhdmlvdXJhbCBhc3BlY3RzJyxcbiAgICBidXR0b246ICdDTElDSyBUTyBLTk9XJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2FuYWx5emUtc2NyZWVuc2hvdDIuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdNYXJrIEFuYWx5c2lzJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdLbm93IHlvdXIgbWFya3MgYWNyb3NzIGVhY2ggc3ViamVjdCBhbmQgZGlmZmljdWx0eSBsZXZlbHMuIFRoZSBhdHRlbXB0IGFuZCBhY2N1cmFjeSByYXRlcyBoZWxwIHlvdSB1bmRlcnN0YW5kIHlvdXIgd2VhayBhdHRlbXB0cyBpbXByb3ZlIHRoZSBzYW1lIGdvaW5nIGZvcndhcmQnLFxuICAgIGJ1dHRvbjogJ1RBS0UgQSBURVNUJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2FuYWx5emUtc2NyZWVuc2hvdDMuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdDb25jZXB0IEFuYWx5c2lzJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdVbmRlcnN0YW5kIHRoZSB3ZWlnaHRhZ2Ugb2YgZWFjaCB0b3BpYyBhbmQgeW91ciBwZXJmb3JtYW5jZSBpbiBpdCBhY3Jvc3MgYWxsIHRoZSB0ZXN0cyBhdHRlbXB0ZWQgZHVyaW5nIHRoZSB5ZWFyJyxcbiAgICBidXR0b246ICdDTElDSyBUTyBLTk9XJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2FuYWx5emUtc2NyZWVuc2hvdDQuc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBBTkFMWVpFX0JFSEFWSU9VUl9BTkFMWVNJUyA9IFtcbiAge1xuICAgIGhlYWRpbmc6ICdUaW1lIEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ1RpbWUgbWFuYWdlbWVudCBpcyB0aGUgbW9zdCBpbXBvcnRhbnQgYXNwZWN0IG9mIHRlc3QgdGFraW5nLiBHZXRSYW5rcyB0cmFja3MgeW91ciB0aW1lIGR1cmluZyB0aGUgdGVzdCBhbmQgcHJvdmlkZXMgYW4gZWFzaWx5IHVuZGVyc3RhbmRhYmxlIHRpbWUgbWFwIHRvIGhlbHAgeW91IGltcHJvdmUgeW91ciB0aW1lIG1hbmFnZW1lbnQgc2tpbGxzJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdRdWVzdGlvbiBBbmFseXNpcycsXG4gICAgY29udGVudDpcbiAgICAgICdFdmVyeSBxdWVzdGlvbiBpcyBkaWZmZXJlbnQgYW5kIHdlIHRyZWF0IGl0IGFzIHN1Y2g7IHdlIGFuYWx5c2UgaG93IHlvdSBwZXJmb3JtZWQgaW4gZWFjaCBxdWVzdGlvbiBhY3Jvc3MgdmFyaW91cyBkaW1lbnNpb25zIGFuZCBudWRnZSB5b3UgdG8gaW1wcm92ZScsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnRGlmZmljdWx0eSBBbmFseXNpcycsXG4gICAgY29udGVudDpcbiAgICAgICdUaGUgZGlmZmljdWx0eSBvZiB0aGUgcGFwZXIgaGFzIGEgZGlyZWN0IGltcGFjdCBvbiBzdHVkZW50cyBwZXJmb3JtYW5jZSBhbmQgd2UgaGVscCB5b3UgdW5kZXJzdGFuZCB5b3VyIGRpZmZpY3VsdHkgcHJvZmlsZSBzbyB0aGF0IHlvdSBjYW4gaW1wcm92ZSB5b3VyIGFjY3VyYWN5IGFuZCByZWR1Y2UgbmVnYXRpdmUgbWFya2luZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgQU5BTFlaRV9DT05URU5UXzIgPSBbXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnQ29tcGFyZSAnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0NvbXBhcmUgeW91ciBwZXJmb3JtYW5jZSB3aXRoIHRoZSB0b3BwZXJzIGFuZCBmaWd1cmUgb3V0IHRoZWlyIHdheSBvZiBjbGltYmluZyB0byB0aGUgdG9wJyxcbiAgICBidXR0b246ICdDT01QQVJFIE5PVycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9hbmFseXplLXNjcmVlbnNob3Q2LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnUmFuayBQcmVkaWN0b3InLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0ZpbmQgb3V0IHdoZXJlIHlvdSBzdGFuZCBhdCB0aGUgbmF0aW9uYWwgbGV2ZWwuIFNldCBnb2FscyBhbmQgY2hhcnQgdGhlIHBhdGggdG8geW91ciBkcmVhbSBjb2xsZWdlIScsXG4gICAgYnV0dG9uOiAnS05PVyBNT1JFJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2FuYWx5emUtc2NyZWVuc2hvdDcuc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBQUkFDVElDRV9DT05URU5UID0gW1xuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0FzayBEaXNoYSEnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0hhdmluZyB0cm91YmxlIGRlY2lkaW5nIHdoYXQgdG8gcHJhY3RpY2U/IERvbuKAmXQgd29ycnksIGp1c3QgYXNrIERpc2hhIGFuZCBzaGXigJlsbCB0YWtlIHlvdSB0aHJvdWdoIHRoZSBvcHRpbWFsIGxlYXJuaW5nIHBhdGggdG8gYWNoaWV2ZSB5b3VyIGdvYWxzLicsXG4gICAgYnV0dG9uOiAnVEFLRSBBIFRFU1QnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJhY3RpY2Utc2NyZWVuc2hvdDEuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdDdXN0b20gUHJhY3RpY2UnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1ByYWN0aWNlIGF0IHlvdXIgb3duIHBhY2UuIENob29zZSB5b3VyIG93biBjaGFwdGVycywgZGlmZmljdWx0eSBhbmQgdGltZSwgZmVlbCBjb21mb3J0YWJsZSBpbiB0aGUgdGVzdCB0YWtpbmcgZXhwZXJpZW5jZSBhbmQgdGFyZ2V0ZWRseSBpbXByb3ZlIHlvdXIgc2NvcmVzLicsXG4gICAgYnV0dG9uOiAnVEFLRSBBIFRFU1QnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJhY3RpY2Utc2NyZWVuc2hvdDIuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdBbGwgSW5kaWEgVGVzdCBTZXJpZXMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0NvbXBhcmUgeW91ciBwZXJmb3JtYW5jZSB3aXRoIGFsbCB5b3VyIGNvbXBldGl0b3JzIGFjcm9zcyB0aGUgY291bnRyeS4gRXZhbHVhdGUgd2hlcmUgeW91IHN0YW5kIGFuZCB0YXJnZXQgeW91ciBhcHByb2FjaCBhY2NvcmRpbmdseS4nLFxuICAgIGJ1dHRvbjogJ0NMSUNLIFRPIEtOT1cnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90My5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IFNUVURFTlRTX1JFQ09SRFMgPSBbXG4gIHtcbiAgICByYW5rOiAnQUlSIDM1NCcsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL25hdm5lZXQucG5nJyxcbiAgICBuYW1lOiAnViBOQVZBTkVFVEgnLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTA1MzQyODInLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiAzNTknLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy9uaXR5YW5hbmQucG5nJyxcbiAgICBuYW1lOiAnUC5TLlMuIE5JVEhZQU5BTkQnLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTAyOTg2MzgnLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiA0NDEnLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy9yb2hpdHJhai5wbmcnLFxuICAgIG5hbWU6ICdSLlJPSElUUkFKJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwNTIyNTM4JyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgNTA1JyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvY2hhbmR1LnBuZycsXG4gICAgbmFtZTogJ0NILkNIQU5EVScsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDE1MDc1OScsXG4gIH0sXG4gIHtcbiAgICByYW5rOiAnQUlSIDYxOCcsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL3NhaXBhdmFuLnBuZycsXG4gICAgbmFtZTogJ1IgU0FJIFBBVkFOJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwMjE4OTQwJyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgNzIzJyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvdGVqYXN3aS5wbmcnLFxuICAgIG5hbWU6ICdQLlRFSkFTV0knLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTAxMTc0NTcnLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiA5NzInLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy9wcmVtY2hhbmQucG5nJyxcbiAgICBuYW1lOiAnRyBQUkVNIENIQU5EJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwMjE4NjMwJyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgMTAzOScsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL3NhaWtyaXNobmEucG5nJyxcbiAgICBuYW1lOiAnQSBTQUkgS1JJU0hOQScsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDQ3NjM2OCcsXG4gIH0sXG4gIHtcbiAgICByYW5rOiAnQUlSIDExMTgnLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy9hYmhpbGFzaC5wbmcnLFxuICAgIG5hbWU6ICdLLkFCSElMQVNIJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwMjE4Mzc5JyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgMTE1MCcsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL3ZhcnNoYS5wbmcnLFxuICAgIG5hbWU6ICdBLlZBUlNIQScsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDgwMTYwMicsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgU0xJREVTSE9XID0gW1xuICB7XG4gICAgdGl0bGU6ICdKRUUgTUFJTicsXG4gICAgZG91YmxlRGlnaXRSYW5rczogNzAsXG4gICAgdHJpcGxlRGlnaXRSYW5rczogJzQwMScsXG4gICAgYmVzdFJhbmtzOiAnMSwgMTEsIDEzLCAxNCwgMjQnLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdKRUUgQURWLicsXG4gICAgZG91YmxlRGlnaXRSYW5rczogMzAsXG4gICAgdHJpcGxlRGlnaXRSYW5rczogJzEzMCcsXG4gICAgYmVzdFJhbmtzOiAnMSwgNywgMTcsIDI0LCAyOCcsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgVEVBQ0hGT1IgPSBbXG4gIHtcbiAgICB0aXRsZTogJ0FjYWRlbWljcycsXG4gICAgY291cnNlczogW1xuICAgICAgJ0NMQVNTIDYnLFxuICAgICAgJyBDTEFTUyA3JyxcbiAgICAgICcgQ0xBU1MgOCcsXG4gICAgICAnIENMQVNTIDknLFxuICAgICAgJyBDTEFTUyAxMCcsXG4gICAgICAnIENMQVNTIDExJyxcbiAgICAgICcgQ0xBU1MgMTInLFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0NvbXBldGl0aXZlIEV4YW1zJyxcbiAgICBjb3Vyc2VzOiBbXG4gICAgICAnSUlUIEpFRScsXG4gICAgICAnIE5FRVQnLFxuICAgICAgJyBFQU1DRVQnLFxuICAgICAgJ0NCU0UnLFxuICAgICAgJ0JJVFNBVCcsXG4gICAgICAnQ09NRUQtSycsXG4gICAgICAnVklURUVFJyxcbiAgICAgICdBSUlNUycsXG4gICAgICAnSklQTUVSJyxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdFeHRyYWN1cnJpY3VsYXInLFxuICAgIGNvdXJzZXM6IFtcbiAgICAgICdNVVNJQycsXG4gICAgICAnUEFJTlRJTkcnLFxuICAgICAgJ0RBTkNJTkcnLFxuICAgICAgJ1lPR0EnLFxuICAgICAgJ01FRElUQVRJT04nLFxuICAgICAgJ0FCQUNVUycsXG4gICAgICAnUEhPVE9HUkFQSFknLFxuICAgIF0sXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgUExBVEZPUk1TID0gW1xuICB7XG4gICAgbGFiZWw6ICdNb2JpbGUnLFxuICAgIGF2YWlsYWJsZTogJ0FuZHJvaWQgJiBpT1MnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL21vYmlsZS5zdmcnLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdEZXNrdG9wJyxcbiAgICBhdmFpbGFibGU6ICdDaHJvbWUnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL2Rlc2t0b3Auc3ZnJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnVGFibGV0JyxcbiAgICBhdmFpbGFibGU6ICdBbmRyb2lkICYgaU9TJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL3BsYXRmb3Jtcy90YWJsZXQuc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBDVVNUT01FUlNfU1RBVFMgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvY3VzdG9tZXJzX25ldy5zdmcnLFxuICAgIG51bWJlcjogJzEgTWlsbGlvbiArJyxcbiAgICB0ZXh0OiAnU2F0aXNmaWVkIFN0dWRlbnRzJyxcbiAgICBsYWJlbDogJ0N1c3RvbWVycyBMb2dvJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9pbnN0aXR1dGlvbl9uZXcuc3ZnJyxcbiAgICBudW1iZXI6ICcxMDArJyxcbiAgICB0ZXh0OiAnU2F0aXNmaWVkIEluc3RpdHV0aW9ucycsXG4gICAgbGFiZWw6ICdJbnN0aXR1dGlvbnMgTG9nbycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgQ1VTVE9NRVJfUkVWSUVXID0gW1xuICB7XG4gICAgbG9nbzogJ2ltYWdlcy9jdXN0b21lcnMvc3JpQ2hhaXRhbnlhX2xvZ28uanBnJyxcbiAgICBwaWM6ICdpbWFnZXMvY3VzdG9tZXJzL3NydWphbi5qcGcnLFxuICAgIGxhYmVsOiAnU3JpIENoYWl0YW55YScsXG4gICAgdGV4dDogYFwiIE91ciBpbnN0aXR1dGUgY2hvc2UgdGhpcyBhcHAgZm9yIHNlbmRpbmcgdXMgb25saW5lIGxlY3R1cmVzIGFuZCBmb3JcbiAgICBjb25kdWN0aW5nIGV4YW1zIGFuZCBJIGdvdHRhIHNheSwgaXQgcmVhbGx5IHdvcmtzIHdlbGwuIFRoZSBjbGFzc2VzXG4gICAgaGF2ZSBoaWdoIHF1YWxpdHkgdmlzdWFsIGFuZCB5b3UgY2FuIGNoYW5nZSB0aGUgcGxheWJhY2sgc3BlZWQgYXMgd2VsbC5cbiAgICBZb3UgY2FuJ3QgdGFrZSBzY3JlZW5zaG90cyBkdXJpbmcgdGhlIGV4YW0gYW5kIGFmdGVyIHRoZSBleGFtIHdlIGdldCBpbmRpdmlkdWFsIG1hcmtzXG4gICAgZm9yIGVhY2ggc3ViamVjdCBhbmQgdGhlIHRvdGFsIGFzIHdlbGwuIFdlIGNhbiBldmVuIGNvbXBhcmUgb3VyIHByZXZpb3VzIHNjb3Jlcy5cbiAgICAgV2UgY2FuIGV2ZW4gYW5hbHlzZSB0aGUgdG9wcGVycyBzY29yZSBhbG9uZyB3aXRoIG91cnMuIE92ZXJhbGwsIGl0IGlzIGEgd29uZGVyZnVsIGFwcC5cImAsXG4gICAgbmFtZTogJ1N0YWN5IENvcmRlbGlhJyxcbiAgICBkZXNpZ25hdGlvbjogJ1N0dWRlbnQnLFxuICB9LFxuICB7XG4gICAgbG9nbzogJ2ltYWdlcy9jdXN0b21lcnMvcmFua2d1cnVfbG9nby5qcGcnLFxuICAgIHBpYzogJ2ltYWdlcy9jdXN0b21lcnMvbGF4bWFuLmpwZycsXG4gICAgbGFiZWw6ICdSYW5rZ3VydScsXG4gICAgdGV4dDogYFwiVGhpcyBhcHAgaXMgc28gZ29vZCBhbmQgdGhlcmUgaXMgbm8gcHJvYmxlbSBpbiBxdWFsaXR5IG9mIGNsYXNzZXMgcG9zdGVkIGluIHRoZSBhcHAuIFRoZSB3YXkgb2YgY29uZHVjdGluZyBleGFtXG4gICAgIGluIHRoZSBhcHAgaXMgc28gZ29vZCB0aGF0IGV2ZXJ5IHN0dWRlbnQgd2lsbCBiZSBjb21mb3J0YWJsZSB0byB3cml0ZS4gVGhpcyBhcHAgaXMgYWxzbyBnb29kIGZvciBzdHVkaWVzLiBPbmxpbmUgZXhhbXNcbiAgICAgIGFyZSBhbHNvIHNvIGdvb2QuVGhhbmsgZm9yIHN1cHBvcnRpbmcgb3VyIGluc3RpdHV0aW9uIGluIGtlZXBpbmcgZXhhbXMgYW5kIG9ubGluZSBjbGFzc2VzIFNvIGV2ZXJ5Ym9keSBkb3dubG9hZFxuICAgICAgIGdldHJhbmtzIGFuZCByZXF1ZXN0IHlvdXIgSW5zdGl0dXRlIHRvIGNvbmR1Y3QgZXhhbXMgYW5kIHBvc3QgY2xhc3NlcyBpbiBnZXRyYW5rc1wiYCxcbiAgICBuYW1lOiAnbGVjaGlyZWRkeSBTdXJla2hhJyxcbiAgICBkZXNpZ25hdGlvbjogJ1N0dWRlbnQnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IENMSUVOVFNfU0VUMSA9IFtcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvdHMucG5nJywgbGFiZWw6ICdUUyBHb3Z0LicgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvc3JpX2NoYWl0aGFueWEucG5nJywgbGFiZWw6ICdTcmkgQ2hhaXRoYW55YScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvUmFua2d1cnUucG5nJywgbGFiZWw6ICdSYW5rR3VydScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvbW90aW9uLnBuZycsIGxhYmVsOiAnTW90aW9uJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9tcy5wbmcnLCBsYWJlbDogJ01TJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy90aXJ1bWFsYS5wbmcnLCBsYWJlbDogJ1RpcnVtYWxhJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy90cmluaXR5LnBuZycsIGxhYmVsOiAnVHJpbml0eScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvdmlzd2EucG5nJywgbGFiZWw6ICdTcmkgVmlzd2EnIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL2JoYXZpc2h5YS5wbmcnLCBsYWJlbDogJ0JoYXZpc2h5YScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvcHJhZ2F0aGlfanIucG5nJywgbGFiZWw6ICdQcmdhdGhpIEpyLicgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvc2FyYXRoLnBuZycsIGxhYmVsOiAnU2FyYXRoIEFjYWRlbXknIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL3ByYWdhdGhpLnBuZycsIGxhYmVsOiAnUHJhZ2F0aGknIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvc2FyYXN3YXRoaV9ncm91cHMucG5nJyxcbiAgICBsYWJlbDogJ1NhcmFzd2F0aGlfR3JvdXBzJyxcbiAgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvc2hpdmFuaS5wbmcnLCBsYWJlbDogJ1NoaXZhbmknIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL3ByYXRoaWJoYV9qci5wbmcnLCBsYWJlbDogJ1ByYXRoaWJoYV9KcicgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvUmVzb25hbmNlLnBuZycsIGxhYmVsOiAnUmVzb25hbmNlJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy92aWpldGhhLnBuZycsIGxhYmVsOiAnVmlqZXRoYScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvdG9wcGVycy5wbmcnLCBsYWJlbDogJ1RvcHBlcnMnIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgQ0xJRU5UU19TRVQyID0gW1xuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL2Fidi5wbmcnLCBsYWJlbDogJ0FCVicgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9tYXN0ZXJfbWluZHMucG5nJywgbGFiZWw6ICdNYXN0ZXIgTWluZHMnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvc3JpX2dheWF0aHJpLnBuZycsIGxhYmVsOiAnU3JpIEdheWF0aHJpJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3BpbmVncm92ZS5wbmcnLCBsYWJlbDogJ1BpbmVncm92ZScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9zcmlfYWtzaGFyYS5wbmcnLCBsYWJlbDogJ1NyaSBBa3NoYXJhJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3VyYmFuZS5wbmcnLCBsYWJlbDogJ1VyYmFuZScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9zcmlfcHJha2FzaC5wbmcnLCBsYWJlbDogJ1NyaSBQcmFrYXNoJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3Zpc2hyYS5wbmcnLCBsYWJlbDogJ1Zpc2hyYScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9uYXZvZGF5YS5wbmcnLCBsYWJlbDogJ05hdm9kYXlhJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL25ld2VyYS5wbmcnLCBsYWJlbDogJ05ldyBFcmEnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvYWtzaGF5YS5wbmcnLCBsYWJlbDogJ0Frc2hheWEnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvZ3lhbmFtLnBuZycsIGxhYmVsOiAnR3lhbmFtJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL2VsZWdhbmNlLnBuZycsIGxhYmVsOiAnRWxlZ2FuY2UnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMva2FrYXRoaXlhLnBuZycsIGxhYmVsOiAnS2FrYXRoaXlhJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3JhbWFiYW5hbS5wbmcnLCBsYWJlbDogJ1JhbWFCYW5hbScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9nZWV0aGFtLnBuZycsIGxhYmVsOiAnR2VldGhhbScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9zcmlfdmFpc2huYXZpLnBuZycsIGxhYmVsOiAnU3JpX1ZhaXNobmF2aScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvZGhpa3NoYS5wbmcnLCBsYWJlbDogJ0RoaWtzaGEnIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgTElWRUNMQVNTRVNEQVRBID0gW1xuICB7XG4gICAgcm91dGU6ICdUZWFjaCcsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL3RlYWNoLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnVGVhY2gnLFxuICAgICAgICBpc0hlYWRpbmc6IHRydWUsXG4gICAgICAgIHJvdXRlOiAnVGVhY2gnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL0xpdmUuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdMaXZlJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdMaXZlJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9Bc3NpZ25tZW50cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBc3NpZ25tZW50cycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvRG91YnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnRG91YnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdEb3VidHMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdUZXN0JyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVzdC90ZXN0LnN2ZycsXG4gICAgICAgIHRpdGxlOiAnVGVzdCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdUZXN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L1Rlc3RzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnVGVzdHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1Rlc3RzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L0FuYWx5c2lzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQW5hbHlzaXMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0FuYWx5c2lzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L1ByYWN0aWNlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnUHJhY3RpY2UnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1ByYWN0aWNlJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L1BhcGVycy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1BhcGVycycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUGFwZXJzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L1JlcG9ydHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdSZXBvcnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdSZXBvcnRzJyxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHJvdXRlOiAnQ29ubmVjdCcsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0Nvbm5lY3QvY29ubmVjdC5zdmcnLFxuICAgICAgICB0aXRsZTogJ0Nvbm5lY3QnLFxuICAgICAgICBpc0hlYWRpbmc6IHRydWUsXG4gICAgICAgIHJvdXRlOiAnQ29ubmVjdCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29ubmVjdC9zY2hlZHVsZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ1NjaGVkdWxlJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdTY2hlZHVsZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29ubmVjdC9tZXNzZW5nZXIuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdNZXNzZW5nZXInLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ01lc3NlbmdlcicsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29ubmVjdC9ub3RpZmljYXRpb25zLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnTWVzc2VuZ2VyJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdNZXNzZW5nZXInLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdDb2xsZWN0JyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29sbGVjdC9jb2xsZWN0LnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQ29sbGVjdCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdDb2xsZWN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db2xsZWN0L2ZlZV9jb2xsZWN0aW9uLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnRmVlIENvbGxlY3Rpb24nLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0ZlZUNvbGxlY3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0NvbGxlY3QvRS1SZWNlaXB0cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0UtUmVjZWlwdHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1JlY2VpcHRzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246XG4gICAgICAgICAgJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0NvbGxlY3QvYXV0b19wYXltZW50X3JlbWFpbmRlcnMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdBdXRvIFBheW1lbnQgUmVtYWluZGVycycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQXV0b1BheW1lbnRzJyxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHJvdXRlOiAnTWFuYWdlJyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvTWFuYWdlL21hbmFnZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ01hbmFnZScsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdNYW5hZ2UnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL01hbmFnZS9hZG1pc3Npb25zLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQWRtaXNzaW9ucycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQWRtaXNzaW9ucycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvTWFuYWdlL2F0dGVuZGVuY2VzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQXR0ZW5kYW5jZScsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQXR0ZW5kYW5jZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvTWFuYWdlL3N0dWRlbnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnU3R1ZGVudHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1N0dWRlbnRzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9NYW5hZ2Uvcm9sZXMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdSb2xlcycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUm9sZXMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuXTtcbmV4cG9ydCBjb25zdCBPTERfTElWRUNMQVNTRVNEQVRBID0gW1xuICB7XG4gICAgcm91dGU6ICdUZWFjaCcsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZWFjaC9UZWFjaC5zdmcnLFxuICAgICAgICB0aXRsZTogJ1RlYWNoJyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ1RlYWNoJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvVGVhY2gvTGl2ZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ0xpdmUnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0xpdmUnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZWFjaC9Bc3NpZ25tZW50cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBc3NpZ25tZW50cycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1RlYWNoL0RvdWJ0cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0RvdWJ0cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnRG91YnRzJyxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHJvdXRlOiAnVGVzdCcsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZXN0L1Rlc3Quc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdUZXN0JyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ1Rlc3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZXN0L1Rlc3RzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnVGVzdHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1Rlc3RzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvVGVzdC9BbmFseXNpcy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0FuYWx5c2lzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBbmFseXNpcycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1Rlc3QvUHJhY3RpY2Uuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdQcmFjdGljZScsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUHJhY3RpY2UnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZXN0L1BhcGVycy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1BhcGVycycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUGFwZXJzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvVGVzdC9SZXBvcnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnUmVwb3J0cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUmVwb3J0cycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICByb3V0ZTogJ0Nvbm5lY3QnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29ubmVjdC9Db25uZWN0LnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQ29ubmVjdCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdDb25uZWN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29ubmVjdC9TY2hlZHVsZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ1NjaGVkdWxlJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdTY2hlZHVsZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL0Nvbm5lY3QvTWVzc2VuZ2VyLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnTWVzc2VuZ2VyJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdNZXNzZW5nZXInLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9Db25uZWN0L05vdGlmaWNhdGlvbnMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdOb3RpZmljYXRpb25zJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdNZXNzZW5nZXInLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdDb2xsZWN0JyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL0NvbGxlY3QvQ29sbGVjdC5zdmcnLFxuICAgICAgICB0aXRsZTogJ0NvbGxlY3QnLFxuICAgICAgICBpc0hlYWRpbmc6IHRydWUsXG4gICAgICAgIHJvdXRlOiAnQ29sbGVjdCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL0NvbGxlY3QvRmVlQ29sbGVjdGlvbi5zdmcnLFxuICAgICAgICB0aXRsZTogJ0ZlZSBDb2xsZWN0aW9uJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdGZWVDb2xsZWN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29sbGVjdC9FUmVjZWlwdHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdFLVJlY2VpcHRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdFUmVjZWlwdHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9Db2xsZWN0L0F1dG9QYXltZW50UmVtYWluZGVycy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0F1dG8gUGF5bWVudCBSZW1haW5kZXJzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBdXRvUGF5bWVudHMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdNYW5hZ2UnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvTWFuYWdlL01hbmFnZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ01hbmFnZScsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdNYW5hZ2UnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9NYW5hZ2UvQWRtaXNzaW9ucy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0FkbWlzc2lvbnMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0FkbWlzc2lvbnMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9NYW5hZ2UvQXR0ZW5kYW5jZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ0F0dGVuZGFuY2UnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0F0dGVuZGFuY2UnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9NYW5hZ2UvU3R1ZGVudHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdTdHVkZW50cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnU3R1ZGVudHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9NYW5hZ2UvUm9sZXMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdSb2xlcycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUm9sZXMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IE1FRElBX0NPVkVSQUdFID0gW1xuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcHJlc3MvZmluYW5jaWFsX2V4cHJlc3MtMS5wbmcnLFxuICAgIHRpdGxlOlxuICAgICAgJ1N1cnByaXNlISBQcmVkaWN0aXZlIGFuYWx5c2lzIG9mIHN0dWRlbnRzIGZ1dHVyZSBjb3Vyc2Ugb2YgYWN0aW9uIGZvciBjYXJlZXIgZGV2ZWxvcG1lbnQgbm93IHBvc3NpYmxlOyBFZ25pZnkgcm9sbHMgb3V0IHNvbHV0aW9uJyxcbiAgICBhdXRob3I6ICdCeTogQlYgTWFoYWxha3NobWknLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnRm9yIHN0dWRlbnRzLCBpdCBpcyBhbHdheXMgbGVhcm4sIHJlbGVhcm4gYW5kIHVubGVhcm4gdG8gaGl0IHRoZSB0b3Agc2NvcmUuIEJ1dCBuZWl0aGVyIHRoZSBlZHVjYXRvcnMgbm9yIHBhcmVudHMgb3IgdGhlIHN0dWRlbnRzIGNhbiBkbyBhIHByZWRpY3RpdmUgYW5hbHlzaXMgYWJvdXQgdGhlaXIgZnV0dXJlIGNvdXJzZSBvZiBhY3Rpb24gZm9yIGNhcmVlciBkZXZlbG9wbWVudCBhbmQgdGhlIGRpbGVtbWEgY29udGludWVzIGZvciBnZW5lcmF0aW9ucy4nLFxuICAgIHVybDpcbiAgICAgICdodHRwczovL3d3dy5maW5hbmNpYWxleHByZXNzLmNvbS9pbmR1c3RyeS9zdXJwcmlzZS1wcmVkaWN0aXZlLWFuYWx5c2lzLW9mLXN0dWRlbnRzLWZ1dHVyZS1jb3Vyc2Utb2YtYWN0aW9uLWZvci1jYXJlZXItZGV2ZWxvcG1lbnQtbm93LXBvc3NpYmxlLWVnbmlmeS1yb2xscy1vdXQtc29sdXRpb24vODgxMjgyLycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy95b3Vyc3RvcnktMS5wbmcnLFxuICAgIHRpdGxlOiAnVGhpcyBIeWRlcmFiYWQgc3RhcnR1cCBpcyBoZWxwaW5nIHN0dWRlbnRzIHBlcmZvcm0gYmV0dGVyIGluIGV4YW1zJyxcbiAgICBhdXRob3I6ICdCeSBUaGluayBDaGFuZ2UgSW5kaWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnU3RhcnRlZCBhcyBhbiBlZHV0ZWNoIHN0YXJ0dXAgYW5kIGJhc2VkIG91dCBvZiBULUh1YiwgSHlkZXJhYmFkLCBFZ25pZnkgZW1wbG95cyBkZWVwIGxlYXJuaW5nLCBNYWNoaW5lIExlYXJuaW5nIGFuZCBkYXRhIHNjaWVuY2UgdG8gbWFrZSBhIGtlZW4gYW5hbHlzaXMgb2YgdGhlIHBlcmZvcm1hbmNlIG9mIHN0dWRlbnRzLiBCYXNlZCBvbiB0aGlzIHJlcG9ydCwgdGhlIHN0YXJ0dXAgZ2V0cyB0byBkZXNjcmliZSB3aGVyZSB0aGUgc3R1ZGVudHMgbmVlZCB0byBpbXByb3ZlIHVwb24sIGFsb25nIHdpdGggYSBkZXRhaWxlZCBicmVha2Rvd24gb2YgdGhlaXIgbWFya3MuJyxcbiAgICB1cmw6ICdodHRwczovL3lvdXJzdG9yeS5jb20vMjAxNy8xMi9oeWRlcmFiYWQtc3RhcnR1cC1zdHVkZW50cy1leGFtLycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy9pbmRpYW5fZXhwcmVzcy0xLnBuZycsXG4gICAgdGl0bGU6ICdJbXBldHVzIHRvIGV4Y2VsJyxcbiAgICBhdXRob3I6ICdCeSBTaHlhbSBZYWRhZ2lyaSwgRXhwcmVzcyBOZXdzIFNlcnZpY2UnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnRm91bmRlZCBpbiAyMDE1IGJ5IEtpcmFuIEJhYnUsIEVnbmlmeSBpcyBhIGNsb3VkLWJhc2VkIHBsdWctYW5kLXBsYXkgYW5hbHl0aWNzIGFuZCBhc3Nlc3NtZW50cyBwbGF0Zm9ybSBmb3IgZWR1Y2F0aW9uYWwgaW5zdGl0dXRpb25zLicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHA6Ly93d3cubmV3aW5kaWFuZXhwcmVzcy5jb20vY2l0aWVzL2h5ZGVyYWJhZC8yMDE3L3NlcC8xOS9pbXBldHVzLXRvLWV4Y2VsLTE2NTk2OTkuaHRtbCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy9hc2lhLTEucG5nJyxcbiAgICB0aXRsZTpcbiAgICAgICdFZ25pZnkgSW1wcm92aW5nIEVkdWNhdGlvbiB1c2luZyB0aGUgZGVlcC10ZWNoIGFuYWx5dGljczogUGVyZm9ybWFuY2UnLFxuICAgIGF1dGhvcjogJ0FTSUEgSU5DLjUwMCcsXG4gICAgY29udGVudDpcbiAgICAgICdGb3VuZGVkIGluIDIwMTUgYnkgS2lyYW4gQmFidSwgRWduaWZ5IGlzIGEgY2xvdWQtYmFzZWQgQXNzZXNzbWVudCBhbmQgTGVhcm5pbmcgQW5hbHl0aWNzIHBsYXRmb3JtIGludGVncmF0ZWQgd2l0aCB3b3JsZC1jbGFzcyBBbmFseXRpY3MgdG8gZW5oYW5jZSBjb25jZXB0dWFsIGNsYXJpdHkgYW5kIEV4YW0gUmVhZGluZXNzIG9mIHRoZSBzdHVkZW50LiBJdCBpcyBhIHRlYW0gb2YgMjAgcGVvcGxlIChJSVRzLCBJSUlUcywgSUlNcywgTklUcywgU3RhbmZvcmQpIHNlcnZpbmcgNCwyNywwMDAgc3R1ZGVudHMsIDI4LDAwMCsgdGVhY2hlcnMgaW4gNDAwKyBpbnN0aXR1dGVzIGFjcm9zcyAyOCBjaXRpZXMgaW4gNCBzdGF0ZXMuIE91ciB0YXJnZXQgaXMgdG8gcmVhY2ggMTAgbGFraCBzdHVkZW50cyBieSBlbmQgb2YgdGhlIGFjYWRlbWljIHllYXIgMjAxOC4nLFxuICAgIHVybDpcbiAgICAgICdodHRwOi8vd3d3LmFzaWFpbmM1MDAuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE4LzEwL0FzaWEtSW5jLi01MDAtTWFnYXppbmUtT2N0LTIwMTgtRDkxLnBkZicsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy9kZWNjYW4tMS5wbmcnLFxuICAgIHRpdGxlOiAnUGlsb3QgcHJvamVjdCB0byBtb3VsZCBzdGF0ZSBzY2hvb2wga2lkcyBpbiBUZWxhbmdhbmEnLFxuICAgIGF1dGhvcjogJ0RFQ0NBTiBDSFJPTklDTEUuIHwgTkFWRUVOQSBHSEFOQVRFJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0h5ZGVyYWJhZDogRW11bGF0aW5nIHRoZSBtb2RlbCBmb2xsb3dlZCBieSBwcml2YXRlIGluc3RpdHV0aW9ucywgdGhlIFRlbGFuZ2FuYSBzdGF0ZSBnb3Zlcm5tZW50IGlzIHJ1bm5pbmcgYSBwaWxvdCBwcm9qZWN0IHRvIHN0cmVuZ3RoZW4gdGhlIHdlYWsgYXJlYXMgb2Ygc3R1ZGVudHMgaW4gZ292ZXJubWVudCBzY2hvb2xzIHRocm91Z2ggdGVjaG5vbG9neS4gVGhpcyB3b3VsZCBldmVudHVhbGx5IHJhaXNlIHRoZSBwYXNzIHBlcmNlbnRhZ2UuJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly93d3cuZGVjY2FuY2hyb25pY2xlLmNvbS9uYXRpb24vY3VycmVudC1hZmZhaXJzLzEyMDExOC9waWxvdC1wcm9qZWN0LXRvLW1vdWxkLXN0YXRlLXNjaG9vbC1raWRzLWluLXRlbGFuZ2FuYS5odG1sJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL3RoZV9uZXdzX21pbnV0ZS0xLnBuZycsXG4gICAgdGl0bGU6XG4gICAgICAnVGVsYW5nYW5hIHRpZXMgdXAgd2l0aCBULUh1YiBzdGFydHVwIEVnbmlmeSB0byBpbXByb3ZlIGVkdWNhdGlvbiBpbiBnb3Z0IHNjaG9vbHMnLFxuICAgIGF1dGhvcjogJ1NoaWxwYSBTIFJhbmlwZXRhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ1RoZSBUZWxhbmdhbmEgZ292ZXJubWVudCBpcyBsb29raW5nIHRvIGluY3JlYXNlIHF1YWxpdHkgb2YgZWR1Y2F0aW9uIGluIGdvdmVybm1lbnQgc2Nob29scyBhY3Jvc3MgdGhlIHN0YXRlLCBhbmQgd2FudHMgdG8gdXNlIHRlY2hub2xvZ3kgZm9yIHRoaXMuIEluIHRoZSBsYXRlc3QgZGV2ZWxvcG1lbnQsIGl0IHdhbnRzIHRvIG1lYXN1cmUgdGhlIHN0cmVuZ3RocyBhbmQgd2Vha25lc3NlcyBvZiBzdHVkZW50cyBpbiBnb3Zlcm5tZW50IHNjaG9vbHMgYWNyb3NzIHN1YmplY3RzLCBhc3Nlc3MgaXQgYW5kIGxvb2sgZm9yIHdheXMgdG8gaW1wcm92ZSB0aGUgc2FtZSB1c2luZyB0ZWNobm9sb2d5LicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vd3d3LnRoZW5ld3NtaW51dGUuY29tL2FydGljbGUvdGVsYW5nYW5hLXRpZXMtdC1odWItc3RhcnR1cC1lZ25pZnktaW1wcm92ZS1lZHVjYXRpb24tZ292dC1zY2hvb2xzLTc0NjUyJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL3RlbGFuZ2FuYV90b2RheS0xLnBuZycsXG4gICAgdGl0bGU6ICdIeWRlcmFiYWQtYmFzZWQgc3RhcnR1cCBFZ25pZnkgVGVjaG5vbG9naWVzIGhlbHBzIHN0dWRlbnRzIGV4Y2VsJyxcbiAgICBhdXRob3I6ICdCeSBTcnV0aSBWZW51Z29wYWwnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnV29ya2luZyBvdXQgb2YgVC1IdWIsIHRoZSBzdGFydHVwIGlzIGEgY2xvdWQtYmFzZWQgaG9saXN0aWMgc3R1ZGVudCBwZXJmb3JtYW5jZSBpbXByb3ZlbWVudCBwbGF0Zm9ybSB0aGF0IHRocm91Z2ggZGF0YSBhbmQgYXJ0aWZpY2lhbCBpbnRlbGxpZ2VuY2UgbWVhc3VyZXMgdGhlIHdlYWtuZXNzZXMgYW5kIHN0cmVuZ3RocyBvZiBhIHN0dWRlbnQgYW5kIHByb3ZpZGVzIHNwZWNpZmljIHNvbHV0aW9uLicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vdGVsYW5nYW5hdG9kYXkuY29tL2h5ZGVyYWJhZC1iYXNlZC1zdGFydHVwLWVnbmlmeS10ZWNobm9sb2dpZXMtaGVscHMtc3R1ZGVudHMtZXhjZWwnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcHJlc3MvYW5kaHJhanlvdGhpLTEucG5nJyxcbiAgICB0aXRsZTogJ+CwruCwvuCwsOCxjeCwleCxgeCwsiDgsLXgsL/gsLbgsY3gsLLgsYfgsLfgsKPgsJXgsYIg4LCTIOCwn+CxhuCwleCxjeCwqOCwv+CwleCxjeKAjCcsXG4gICAgY29udGVudDpcbiAgICAgICfgsKrgsLDgsYDgsJXgsY3gsLfgsLLgsYsg4LCS4LCVIOCwteCwv+CwpuCxjeCwr+CwvuCwsOCxjeCwpeCwv+CwleCwvyA1MiDgsK7gsL7gsLDgsY3gsJXgsYHgsLLgsYEg4LC14LCa4LGN4LCa4LC+4LCv4LC/LiDgsLjgsLDgsYcsIOCwruCwsOCwvyDgsKTgsKTgsL/gsK7gsL4gNDgg4LCu4LC+4LCw4LGN4LCV4LGB4LCy4LGBIOCwjuCwguCwpuCxgeCwleCxgSDgsLDgsL7gsLLgsYfgsKbgsYEg4LCF4LCo4LGN4LCo4LCm4LC/IOCwteCxhuCwguCwn+CwqOCxhyDgsJXgsLLgsJfgsL7gsLLgsY3gsLjgsL/gsKgg4LC44LCC4LCm4LGH4LC54LCCLiDgsI7gsJXgsY3gsJXgsKEg4LC14LGA4LCV4LGN4oCM4LCX4LC+IOCwieCwqOCxjeCwqOCwvuCwsOCxiyDgsKTgsYbgsLLgsL/gsLjgsY3gsKTgsYcg4LC44LC14LCw4LC/4LCC4LCa4LGB4LCV4LGL4LC14LCa4LGN4LCa4LGBLiDgsKTgsKbgsY3gsLXgsL7gsLDgsL4g4LCu4LC+4LCw4LGN4LCV4LGB4LCy4LCo4LGBIOCwquCxhuCwguCwmuCxgeCwleCxi+CwteCwmuCxjeCwmuCxgS4g4LCm4LGA4LCo4LGN4LCo4LC/IOCwn+CxhuCwleCxjeCwqOCwvuCwsuCwnOCxgCDgsLjgsLngsJXgsL7gsLDgsILgsKTgsYsg4LC14LC/4LCm4LGN4LCv4LC+4LCw4LGN4LCl4LC/IOCwquCxjeCwsOCwpOCwv+CwreCwqOCxgSDgsLXgsL/gsLbgsY3gsLLgsYfgsLfgsL/gsILgsJrgsYcg4LCq4LCo4LC/4LCo4LC/IOCwmuCxh+CwquCwn+CxjeCwn+Cwv+CwqCDgsLjgsILgsLjgsY3gsKUg4LCO4LCX4LGN4LCo4LC/4LCr4LGILicsXG4gICAgdXJsOiAnaHR0cDovL3d3dy5hbmRocmFqeW90aHkuY29tL2FydGljYWw/U0lEPTQ2MTgyNycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgUFJFU1NfVklERU9fQ09WRVJBR0UgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3MvZXR2X3RlbGFuZ2FuYS53ZWJwJyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9tZWRpYS9vbl9uZXdzL2V0dl90ZWxhbmdhbmEtMS5wbmcnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9d3VaY2VZUnIzX00nLFxuICAgIHRpdGxlOlxuICAgICAgJ0dvdnQgVGllcyBVcCB3aXRoIEdldHJhbmtzIGJ5IEVnbmlmeSBTdGFydHVwIHRvIEFuYWx5emUgU2Nob29sIFN0dWRlbnRzIExhZ2dpbmcgQmVoaW5kIGluIEVkdWNhdGlvbicsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3Mva2lyYW5fc2lyX29uX1QgMjBuZXdzLndlYnAnLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3Mva2lyYW5fc2lyX29uX1QgbmV3cy0xLnBuZycsXG4gICAgdGl0bGU6ICdHZXRyYW5rcyBieSBFZ25pZnlfQ0VPIEtpcmFuIEJhYnVfSHlkZXJhYmFkX1ROZXdzX1QgSHViJyxcbiAgICB1cmw6ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PU9ic3pQOXVqVXg0JyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9zdHVkZW50c19vbl92Ni53ZWJwJyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9tZWRpYS9vbl9uZXdzL3N0dWRlbnRzX29uX3Y2LTEucG5nJyxcbiAgICB0aXRsZTogJ1NwZWNpYWwgU3Rvcnkgb24gR2V0cmFua3MgYnkgRWduaWZ5IGluIEh5ZGVyYWJhZCBWNiBOZXdzJyxcbiAgICB1cmw6ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PWhtWDFqeXphdDlRJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBWSUVXTU9SRSA9IFtcbiAge1xuICAgIGNvbG9yOiAnI2ZmNjQwMCcsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9hcnJvd3MvdGVhY2gtYXJyb3cuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbG9yOiAnIzg4MDBmZScsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9hcnJvd3MvdGVzdC1hcnJvdy5zdmcnLFxuICB9LFxuICB7XG4gICAgY29sb3I6ICcjMDBiODAwJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL2Fycm93cy9jb25uZWN0LWFycm93LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb2xvcjogJyNiZTAwY2MnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvYXJyb3dzL2NvbGxlY3QtYXJyb3cuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbG9yOiAnI2U1YWMwMCcsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9hcnJvd3MvbWFuYWdlLWFycm93LnN2ZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgVEVTVFNfS0VZX1BPSU5UUyA9IFtcbiAge1xuICAgIGlkOiAxLFxuICAgIHBvaW50OiAnVW5saW1pdHRlZCB0ZXN0IGNyZWF0aW9uJyxcbiAgfSxcbiAge1xuICAgIGlkOiAyLFxuICAgIHBvaW50OlxuICAgICAgJ0luc3RhbnQgdGVzdCByZXN1bHQgYW5kIFJlcG9ydHMgYXZhaWxhYmxlIG9ubGluZSAmIG9mZmxpbmUgLSBSYW5rIGxpc3QsIEFuYWx5c2lzLCBhbmQgUmVwb3J0cyBhZnRlciBldmVyeSB0ZXN0LicsXG4gIH0sXG4gIHtcbiAgICBpZDogMyxcbiAgICBwb2ludDogJ1ByZS1kZWZpbmVkIG1hcmtpbmcgc2NoZW1lcycsXG4gIH0sXG4gIHtcbiAgICBpZDogNCxcbiAgICBwb2ludDogJ0N1c3RvbSBtYXJraW5nIHNjaGVtZXMnLFxuICB9LFxuICB7XG4gICAgaWQ6IDUsXG4gICAgcG9pbnQ6XG4gICAgICAnR2V0cmFua3Mgc3VwcG9ydHMgYWxsIDIyIHR5cGVzIG9mIEpFRSBhdmFuY2VkIE1hcmtpbmcgU2NoZW1lcyBzdGFydGluZyBmcm9tIDIwMDkgLSAyMDE5JyxcbiAgfSxcbiAge1xuICAgIGlkOiA2LFxuICAgIHBvaW50OiAnR3JhY2UgcGVyaW9kIG9wdGlvbiBhdmFpbGFibGUnLFxuICB9LFxuICB7XG4gICAgaWQ6IDcsXG4gICAgcG9pbnQ6ICdMaXZlIEF0dGVuZGFuY2UnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IG5ld01vZHVsZXNEYXRhID0gW1xuICB7XG4gICAgbmFtZTogJ0xpdmUnLFxuICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfTGl2ZS5zdmcnLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ0Fzc2lnbm1lbnRzJyxcbiAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvb2xkX0Fzc2lnbm1lbnRzLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnRG91YnRzJyxcbiAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvb2xkX0RvdWJ0cy5zdmcnLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ1Rlc3RzJyxcbiAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVzdC9tb2R1bGVfdGVzdC5zdmcnLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ0Nvbm5lY3QnLFxuICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L25ld19jb25uZWN0LnN2ZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgbW9kdWxlS2V5UG9pbnRzID0ge1xuICB0ZXN0czoge1xuICAgIHBvaW50czogW1xuICAgICAgJ1VubGltaXRlZCB0ZXN0IGNyZWF0aW9uJyxcbiAgICAgICcxMDAsMDAwIHN0dWRlbnQgY29uY3VycmVuY3knLFxuICAgICAgJ0NvbmR1Y3Qgb25saW5lIGFuZCBvZmZsaW5lIHRlc3RzJyxcbiAgICAgICdBZHZhbmNlZCBQcm9jdG9yaW5nIGZlYXR1cmVzJyxcbiAgICAgICdQcmUtZGVmaW5lZCBtYXJraW5nIHNjaGVtYXMgYW5kIGN1c3RvbSBtYXJraW5nIHNjaGVtYXMnLFxuICAgICAgJ0dyYWNlIHBlcmlvZCBvcHRpb24gYXZhaWFsYmUnLFxuICAgICAgJ0xpdmUgYXR0ZW5kYW5jZSBkYXNoYm9hcmQnLFxuICAgIF0sXG4gIH0sXG4gIGFzc2lnbm1lbnRzOiB7XG4gICAgcG9pbnRzOiBbXG4gICAgICAnQWRtaW5pc3RlciBhbmQgZ3JhZGUgb25saW5lIGFuZCBpbi1jbGFzcyBhc3Nlc3NtZW50cycsXG4gICAgICAnR3JhZGUgbXVsdGlwbGUgYXNzZXNzbWVudCB0eXBlcywgaW5jbHVkaW5nIGhhbmR3cml0dGVuIGFzc2Vzc21lbnRzJyxcbiAgICAgICdFdmFsdWF0ZSB1c2luZyBtb2JpbGUgYXBwIGFuZCBpbiBhIHdlYiBicm93c2VyJyxcbiAgICAgICdJbXByb3ZlIHN0dWRlbnQgZmVlZGJhY2sgbG9vcCBhbmQgcHJvdmlkZSBhbiBvcHBvcnR1bml0eSB0byBlbnJpY2ggdGhlIGxlYXJuaW5nIGV4cGVyaWVuY2UgdGhyb3VnaCBhbmFseXRpY3MnLFxuICAgIF0sXG4gIH0sXG4gIGRvdWJ0czoge1xuICAgIHBvaW50czogW1xuICAgICAgJ0RvdWJ0cyBjYW4gdHVybiB1cCBhbnkgdGltZScsXG4gICAgICAnQXNrIHVudGlsIGl0IGlzIHJlc29sdmVkJyxcbiAgICAgICdBbnN3ZXIgZG91YnQgYW55dGltZSwgYW55d2hlcmUnLFxuICAgICAgJ0FjY2VzcyBhbmQgQW5hbHlzaXMgZG91YnRzJyxcbiAgICBdLFxuICB9LFxuICBzY2hlZHVsZToge1xuICAgIHBvaW50czogW1xuICAgICAgJ1NhdmUgdGltZSBhbmQgbWFrZSB0aGUgbW9zdCBvZiBldmVyeSBkYXknLFxuICAgICAgYEJyaW5nIHlvdXIgc3R1ZGVudCwgdGVhY2hlcidzIHRpbWV0YWJsZSB0byBsaWZlIGFuZCBtYWtlIGl0IGVhc3kgdG8gYWNjZXNzIHdoYXQncyBhaGVhZCB3aXRoIGxpbmtzLCBhdHRhY2htZW50cyBhbGwgb3RoZXIgZGV0YWlsc2AsXG4gICAgICAnQWxsIHlvdXIgYWN0aXZpdGllcyBpbiBvbmUgcGxhY2UnLFxuICAgIF0sXG4gIH0sXG59O1xuXG5leHBvcnQgY29uc3QgY2xvbmVEZWVwbHkgPSBvYmogPT4ge1xuICBjb25zdCBuZXdPYmogPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KG9iaikpO1xuICByZXR1cm4gbmV3T2JqO1xufTtcblxuZXhwb3J0IGNvbnN0IEhPV19XT1JLUyA9IFtcbiAge1xuICAgIHRpdGxlOiAnT25ib2FyZGluZycsXG4gICAgY29udGVudDogYExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuYCxcbiAgICBpbWc6IGAvaW1hZ2VzL1Rlc3QvZGVtby5wbmdgLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdJbnN0aXR1dGlvbiBTZXR1cCcsXG4gICAgY29udGVudDogYExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuYCxcbiAgICBpbWc6IGAvaW1hZ2VzL1Rlc3QvZGVtby5wbmdgLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdBZGQgSGllcmFyY2hpZXMnLFxuICAgIGNvbnRlbnQ6IGBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LmAsXG4gICAgaW1nOiBgL2ltYWdlcy9UZXN0L2RlbW8ucG5nYCxcbiAgfSxcbiAge1xuICAgIHRpdGxlOiAnQWRkIFN0dWRlbnRzJyxcbiAgICBjb250ZW50OiBgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC5gLFxuICAgIGltZzogYC9pbWFnZXMvVGVzdC9kZW1vLnBuZ2AsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9XX1dPUktTX1RJVExFUyA9IFtcbiAgJ09uYm9hcmRpbmcnLFxuICAnSW5zdGl0dXRpb24gU2V0dXAnLFxuICAnQWRkIEhpZXJhcmNoaWVzJyxcbiAgJ0FkZCBTdHVkZW50cycsXG5dO1xuXG5leHBvcnQgY29uc3QgVEVTVF9WSURFT1MgPSBbXG4gIHtcbiAgICB1cmw6ICcvaW1hZ2VzL1Rlc3QvdmlkZW9zL0NvbmR1Y3Qgb2ZmbGluZS1vbmxpbmUgVGVzdHMubXA0JyxcbiAgICB0aXRsZTogJ0NvbmR1Y3QgT2ZmbGluZS8gT25saW5lIFRlc3QnLFxuICB9LFxuICB7XG4gICAgdXJsOiAnL2ltYWdlcy9UZXN0L3ZpZGVvcy9DcmVhdGUgT3duIFF1ZXN0aW9uIFBhcGVycy5tcDQnLFxuICAgIHRpdGxlOiAnVXBsb2FkIE93biBRdWVzdGlvbiBQYXBlcnMnLFxuICB9LFxuICB7XG4gICAgdXJsOiAnL2ltYWdlcy9UZXN0L3ZpZGVvcy9VcGxvYWQgb3duIHF1ZXN0aW9uIHBhcGVyLm1wNCcsXG4gICAgdGl0bGU6ICdDcmVhdGUgT3duIFF1ZXN0aW9uIFBhcGVycycsXG4gIH0sXG4gIHtcbiAgICB1cmw6ICcvaW1hZ2VzL1Rlc3QvdmlkZW9zL0luZGVwdGggQW5hbHlzaXMubXA0JyxcbiAgICB0aXRsZTogJ0luZGVwdGggQW5hbHlzaXMnLFxuICB9LFxuXTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQU1BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFFQTtBQUNBO0FBRUE7QUFKQTtBQU9BO0FBQ0E7QUFFQTtBQUpBO0FBT0E7QUFDQTtBQUVBO0FBSkE7QUFRQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUVBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU9BO0FBRUE7Ozs7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFZQTs7O0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7O0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTs7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW5EQTtBQWtIQTtBQUVBO0FBQ0E7QUFFQTtBQUpBO0FBT0E7QUFDQTtBQUVBO0FBSkE7QUFPQTtBQUNBO0FBRUE7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVNBO0FBRUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBT0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBU0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUZBO0FBYUE7QUFDQTtBQUZBO0FBZUE7QUFDQTtBQUZBO0FBY0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFKQTtBQVVBO0FBQ0E7QUFYQTtBQWNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFKQTtBQVFBO0FBQ0E7QUFUQTtBQWFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBckJBO0FBOEJBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFqQ0E7QUEwQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFyQkE7QUE4QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBTEE7QUFyQkE7QUErQkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBM0JBO0FBb0NBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFyQkE7QUE4QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQWpDQTtBQTBDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQXJCQTtBQThCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQXJCQTtBQThCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUEzQkE7QUFxQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBUEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTkE7QUFVQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBUEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTkE7QUFVQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBUEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTkE7QUFVQTtBQUNBO0FBQ0E7QUFFQTtBQUxBO0FBU0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQU1BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFEQTtBQVdBO0FBQ0E7QUFEQTtBQVFBO0FBQ0E7QUFEQTtBQVFBO0FBQ0E7QUFEQTtBQTVCQTtBQXFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQU9BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=