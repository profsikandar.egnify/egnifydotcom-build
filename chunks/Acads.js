require("source-map-support").install();
exports.ids = ["Acads"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/acads/Acads.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Acads-root-3-heU {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Acads-intro-Iq0xD {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Acads-introLeftPane-Ffbam {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Acads-jeet-14gOb {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Acads-coachingInstitutes-2phRA {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Acads-introContent-3XnFF {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Acads-introRightPane-2C-70 {\n  padding-top: 5% !important;\n}\n\n.Acads-introRightPane-2C-70 img {\n    width: 100%;\n  }\n\n.Acads-statsDiv-1tAWc {\n  width: 100%;\n  background-color: #3e3e5f;\n  display: block;\n  height: 366px;\n}\n\n.Acads-eachDiv-3xxQk {\n  height: 100%;\n}\n\n.Acads-statValue-dcJDg {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  font-size: 48px;\n  color: #ffab00;\n}\n\n.Acads-statName-1_Y1- {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n\n.Acads-statImage-yK9D_ {\n  height: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: end;\n      align-items: flex-end;\n}\n\n.Acads-content-1LDCG {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Acads-contentImageDiv-2dR0P {\n  height: 136px;\n  width: 136px;\n}\n\n.Acads-screenImage-1lkol {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Acads-headingContent-3xvoa {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Acads-mainContent-2MdSe {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Acads-testimonialsTitle-2qWlZ {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Acads-testimonialsTitle-2qWlZ::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Acads-contentSubdiv-2ZMdJ {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.Acads-list-1sB8o {\n  margin-bottom: 12px;\n  list-style-type: disc !important;\n  margin-left: 4%;\n}\n\n@media only screen and (max-width: 960px) {\n  .Acads-root-3-heU {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Acads-statsDiv-1tAWc {\n    height: 100%;\n    width: 100%;\n    display: block;\n    text-align: center;\n    margin-bottom: 2%;\n  }\n\n    .Acads-statsDiv-1tAWc .Acads-eachDiv-3xxQk {\n      width: 100%;\n      margin-bottom: 10%;\n      margin-top: 10%;\n    }\n\n  .Acads-statValue-dcJDg {\n    font-size: 32px;\n    padding: 6%;\n    border-right: none !important;\n  }\n\n  .Acads-contentForSmall-1jI7D {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Acads-contentImageDivSmall-32b99 {\n    width: 100%;\n  }\n\n    .Acads-contentImageDivSmall-32b99 img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Acads-screenImageSmall-2lGUc {\n    text-align: center;\n  }\n\n  .Acads-headingContentSmall-19R8B {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Acads-mainContentSmall-7EZPQ {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n    text-align: left;\n  }\n\n  .Acads-testimonialsTitle-2qWlZ {\n    margin-bottom: 18%;\n  }\n\n  .Acads-testimonialsTitle-2qWlZ::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Acads-introLeftPane-Ffbam {\n    width: 100%;\n  }\n\n  .Acads-jeet-14gOb {\n    text-align: center;\n  }\n\n  .Acads-coachingInstitutes-2phRA {\n    text-align: center;\n  }\n\n  .Acads-introContent-3XnFF {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Acads-link-1gCaS {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .Acads-statValue-dcJDg {\n    font-size: 60px;\n    height: 25%;\n  }\n\n  .Acads-statsDiv-1tAWc {\n    margin-bottom: 40px;\n  }\n\n  .Acads-introContent-3XnFF {\n    width: 300px;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Acads-introContent-3XnFF {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/Acads.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,YAAY;EACZ,0BAA0B;EAC1B,eAAe;EACf,cAAc;CACf;;AAED;EACE,aAAa;CACd;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;MAChB,sBAAsB;CAC3B;;AAED;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,oBAAoB;EACpB,iCAAiC;EACjC,gBAAgB;CACjB;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,eAAe;IACf,mBAAmB;IACnB,kBAAkB;GACnB;;IAEC;MACE,YAAY;MACZ,mBAAmB;MACnB,gBAAgB;KACjB;;EAEH;IACE,gBAAgB;IAChB,YAAY;IACZ,8BAA8B;GAC/B;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;IAClB,iBAAiB;GAClB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;CACF;;AAED;EACE;IACE,gBAAgB;IAChB,YAAY;GACb;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,aAAa;GACd;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;CACF","file":"Acads.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.statsDiv {\n  width: 100%;\n  background-color: #3e3e5f;\n  display: block;\n  height: 366px;\n}\n\n.eachDiv {\n  height: 100%;\n}\n\n.statValue {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  font-size: 48px;\n  color: #ffab00;\n}\n\n.statName {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n\n.statImage {\n  height: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: end;\n      align-items: flex-end;\n}\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.list {\n  margin-bottom: 12px;\n  list-style-type: disc !important;\n  margin-left: 4%;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .statsDiv {\n    height: 100%;\n    width: 100%;\n    display: block;\n    text-align: center;\n    margin-bottom: 2%;\n  }\n\n    .statsDiv .eachDiv {\n      width: 100%;\n      margin-bottom: 10%;\n      margin-top: 10%;\n    }\n\n  .statValue {\n    font-size: 32px;\n    padding: 6%;\n    border-right: none !important;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n    text-align: left;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .statValue {\n    font-size: 60px;\n    height: 25%;\n  }\n\n  .statsDiv {\n    margin-bottom: 40px;\n  }\n\n  .introContent {\n    width: 300px;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Acads-root-3-heU",
	"intro": "Acads-intro-Iq0xD",
	"introLeftPane": "Acads-introLeftPane-Ffbam",
	"jeet": "Acads-jeet-14gOb",
	"coachingInstitutes": "Acads-coachingInstitutes-2phRA",
	"introContent": "Acads-introContent-3XnFF",
	"introRightPane": "Acads-introRightPane-2C-70",
	"statsDiv": "Acads-statsDiv-1tAWc",
	"eachDiv": "Acads-eachDiv-3xxQk",
	"statValue": "Acads-statValue-dcJDg",
	"statName": "Acads-statName-1_Y1-",
	"statImage": "Acads-statImage-yK9D_",
	"content": "Acads-content-1LDCG",
	"contentImageDiv": "Acads-contentImageDiv-2dR0P",
	"screenImage": "Acads-screenImage-1lkol",
	"headingContent": "Acads-headingContent-3xvoa",
	"mainContent": "Acads-mainContent-2MdSe",
	"testimonialsTitle": "Acads-testimonialsTitle-2qWlZ",
	"contentSubdiv": "Acads-contentSubdiv-2ZMdJ",
	"list": "Acads-list-1sB8o",
	"contentForSmall": "Acads-contentForSmall-1jI7D",
	"contentImageDivSmall": "Acads-contentImageDivSmall-32b99",
	"screenImageSmall": "Acads-screenImageSmall-2lGUc",
	"headingContentSmall": "Acads-headingContentSmall-19R8B",
	"mainContentSmall": "Acads-mainContentSmall-7EZPQ",
	"link": "Acads-link-1gCaS"
};

/***/ }),

/***/ "./src/routes/products/acads/Acads.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Acads_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/acads/Acads.scss");
/* harmony import */ var _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Acads_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/Acads.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







const STATS = [{
  icon: '/images/products/jeet/institute.svg',
  stat: '150+',
  name: 'INSTITUTES'
}, {
  icon: '/images/products/jeet/mortar-board.svg',
  stat: '100k+',
  name: 'STUDENTS'
}, {
  icon: '/images/products/jeet/india.svg',
  stat: '4',
  name: 'STATES'
}, {
  icon: '/images/products/jeet/city.svg',
  stat: '7',
  name: 'CITIES'
}];
const CONTENT = [{
  headerContent: 'Re-imagine progress reports',
  mainContent: ['Holistic, Insightful and Informative reports.', 'Generate reports in less than 5 min for a class.', 'Actionable information in an understandable format.', 'Facilitates productive dialogue among educators, parents & students.'],
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/acads/01-1x.png'
}, {
  headerContent: 'Go Beyond Grades',
  mainContent: ['Data-driven diagnosis to identify student learning needs and identify areas for targeted instruction.', 'Actionable insights to spot patterns, suggest actions and make predictions.', 'Track the progress of a single student, see the overall class performance and compare classes.'],
  icon: '/images/products/jeet/blub.svg',
  screenImage: '/images/products/acads/02-1x.png'
}, {
  headerContent: 'Performance Management System',
  mainContent: ['Formative Assessments Marks.', 'Summative Assessments Marks.', 'Attendance Management.'],
  icon: '/images/products/jeet/weights.svg',
  screenImage: '/images/products/acads/4-1x.png'
} // {
//   headerContent: 'Co-Scholastics',
//   mainContent: [
//     'Formative Assessments Marks',
//     'Summative Assessments Marks',
//     'Attendance Management',
//   ],
//   icon: '/images/products/jeet/group.svg',
//   screenImage: '/images/products/acads/4-1x.png',
// },
];
const SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

class Acads extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "makeAlignment", (contentIndex, key) => {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });
  }

  // static propTypes = {
  //
  // };
  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_3___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_3___default.a.pageview(window.location.href);
  }
  /**
   * @description Returns what alignment should the content take eg. flex-start flex end center
   * @param contentIndex,key
   * @author Sushruth
   * */


  render() {
    const view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane} col m4`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 132
      },
      __self: this
    }, "ACADS"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }, "For Schools"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134
      },
      __self: this
    }, "ACADS is a performance management system for schools which helps improve exam performance and maximize learning outcomes of their students."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/request-demo",
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 139
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140
      },
      __self: this
    }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane} col m8`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/products/acads/hero.svg",
      alt: "",
      height: "100%",
      width: "100%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statsDiv} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, STATS.map((stat, statIndex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.eachDiv} col m3 `,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 154
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: stat.icon,
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statValue,
      style: {
        borderRight: statIndex === 3 ? 'none' : 'solid 1px rgba(139, 139, 223, 0.3)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }, stat.stat, ' '), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169
      },
      __self: this
    }, stat.name, " ")))), CONTENT.map((content, contentIndex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 174
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content} row hide-on-xs`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 175
      },
      __self: this
    }, [0, 1].map(key => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: contentIndex % 2 === key ? `col m5 s12 ${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}` : `col m7 s12 ${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}`,
      style: {
        justifyContent: this.makeAlignment(contentIndex, key)
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 177
      },
      __self: this
    }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 188
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 189
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 190
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 192
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 195
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 196
      },
      __self: this
    }, content.mainContent.map(eachPoint => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.list,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 198
      },
      __self: this
    }, eachPoint))))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall} row hide-on-m-and-up`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 211
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 212
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 218
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 219
      },
      __self: this
    }, content.mainContent.map(eachPoint => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.list,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221
      },
      __self: this
    }, eachPoint)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 225
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      width: "87.6%",
      height: "54.45%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 226
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        paddingTop: '64px',
        marginBottom: '5%'
      },
      className: "row hide",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 236
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    }, "Testimonials"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: SLIDESHOW,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 241
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 243
      },
      __self: this
    }));
    return view;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Acads));

/***/ }),

/***/ "./src/routes/products/acads/Acads.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/acads/Acads.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/acads/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Acads__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/acads/Acads.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/index.js";




async function action() {
  return {
    title: 'Egnify',
    chunks: ['Acads'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Acads__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0FjYWRzLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9hY2Fkcy9BY2Fkcy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvYWNhZHMvQWNhZHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9hY2Fkcy9BY2Fkcy5zY3NzPzM2YzIiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9hY2Fkcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLkFjYWRzLXJvb3QtMy1oZVUge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYzRjNmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbn1cXG5cXG4uQWNhZHMtaW50cm8tSXEweEQge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xcbiAgbWFyZ2luLWJvdHRvbTogNSU7XFxufVxcblxcbi5BY2Fkcy1pbnRyb0xlZnRQYW5lLUZmYmFtIHtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgcGFkZGluZy10b3A6IDUlICFpbXBvcnRhbnQ7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5BY2Fkcy1qZWV0LTE0Z09iIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5BY2Fkcy1jb2FjaGluZ0luc3RpdHV0ZXMtMnBoUkEge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLkFjYWRzLWludHJvQ29udGVudC0zWG5GRiB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICB3aWR0aDogNDIwcHg7XFxufVxcblxcbi5BY2Fkcy1pbnRyb1JpZ2h0UGFuZS0yQy03MCB7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG59XFxuXFxuLkFjYWRzLWludHJvUmlnaHRQYW5lLTJDLTcwIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbi5BY2Fkcy1zdGF0c0Rpdi0xdEFXYyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZTNlNWY7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGhlaWdodDogMzY2cHg7XFxufVxcblxcbi5BY2Fkcy1lYWNoRGl2LTN4eFFrIHtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLkFjYWRzLXN0YXRWYWx1ZS1kY0pEZyB7XFxuICBoZWlnaHQ6IDMwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGNvbG9yOiAjZmZhYjAwO1xcbn1cXG5cXG4uQWNhZHMtc3RhdE5hbWUtMV9ZMS0ge1xcbiAgaGVpZ2h0OiAzMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5BY2Fkcy1zdGF0SW1hZ2UteUs5RF8ge1xcbiAgaGVpZ2h0OiA0MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogZW5kO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcXG59XFxuXFxuLkFjYWRzLWNvbnRlbnQtMUxEQ0cge1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxufVxcblxcbi5BY2Fkcy1jb250ZW50SW1hZ2VEaXYtMmRSMFAge1xcbiAgaGVpZ2h0OiAxMzZweDtcXG4gIHdpZHRoOiAxMzZweDtcXG59XFxuXFxuLkFjYWRzLXNjcmVlbkltYWdlLTFsa29sIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctdG9wOiA2OHB4O1xcbn1cXG5cXG4uQWNhZHMtaGVhZGluZ0NvbnRlbnQtM3h2b2Ege1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5BY2Fkcy1tYWluQ29udGVudC0yTWRTZSB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLkFjYWRzLXRlc3RpbW9uaWFsc1RpdGxlLTJxV2xaIHtcXG4gIGhlaWdodDogMzhweDtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbiAgcGFkZGluZzogMiU7XFxufVxcblxcbi5BY2Fkcy10ZXN0aW1vbmlhbHNUaXRsZS0ycVdsWjo6YWZ0ZXIge1xcbiAgY29udGVudDogJyc7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgd2lkdGg6IDQ4cHg7XFxuICBoZWlnaHQ6IDJweDtcXG4gIGJvcmRlci10b3A6IHNvbGlkIDNweCAjZWM0YzZmO1xcbiAgcGFkZGluZy10b3A6IDEycHg7XFxufVxcblxcbi5BY2Fkcy1jb250ZW50U3ViZGl2LTJaTWRKIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLkFjYWRzLWxpc3QtMXNCOG8ge1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gIGxpc3Qtc3R5bGUtdHlwZTogZGlzYyAhaW1wb3J0YW50O1xcbiAgbWFyZ2luLWxlZnQ6IDQlO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk2MHB4KSB7XFxuICAuQWNhZHMtcm9vdC0zLWhlVSB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1zdGF0c0Rpdi0xdEFXYyB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbi1ib3R0b206IDIlO1xcbiAgfVxcblxcbiAgICAuQWNhZHMtc3RhdHNEaXYtMXRBV2MgLkFjYWRzLWVhY2hEaXYtM3h4UWsge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwJTtcXG4gICAgICBtYXJnaW4tdG9wOiAxMCU7XFxuICAgIH1cXG5cXG4gIC5BY2Fkcy1zdGF0VmFsdWUtZGNKRGcge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1jb250ZW50Rm9yU21hbGwtMWpJN0Qge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQWNhZHMtY29udGVudEltYWdlRGl2U21hbGwtMzJiOTkge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gICAgLkFjYWRzLWNvbnRlbnRJbWFnZURpdlNtYWxsLTMyYjk5IGltZyB7XFxuICAgICAgaGVpZ2h0OiAxMDBweDtcXG4gICAgICB3aWR0aDogMTAwcHg7XFxuICAgIH1cXG5cXG4gIC5BY2Fkcy1zY3JlZW5JbWFnZVNtYWxsLTJsR1VjIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkFjYWRzLWhlYWRpbmdDb250ZW50U21hbGwtMTlSOEIge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1tYWluQ29udGVudFNtYWxsLTdFWlBRIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS42O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICB9XFxuXFxuICAuQWNhZHMtdGVzdGltb25pYWxzVGl0bGUtMnFXbFoge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxOCU7XFxuICB9XFxuXFxuICAuQWNhZHMtdGVzdGltb25pYWxzVGl0bGUtMnFXbFo6OmFmdGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNHB4O1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5BY2Fkcy1pbnRyb0xlZnRQYW5lLUZmYmFtIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuQWNhZHMtamVldC0xNGdPYiB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1jb2FjaGluZ0luc3RpdHV0ZXMtMnBoUkEge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQWNhZHMtaW50cm9Db250ZW50LTNYbkZGIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmctbGVmdDogMTUlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1saW5rLTFnQ2FTIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDEwMjRweCkge1xcbiAgLkFjYWRzLXN0YXRWYWx1ZS1kY0pEZyB7XFxuICAgIGZvbnQtc2l6ZTogNjBweDtcXG4gICAgaGVpZ2h0OiAyNSU7XFxuICB9XFxuXFxuICAuQWNhZHMtc3RhdHNEaXYtMXRBV2Mge1xcbiAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgfVxcblxcbiAgLkFjYWRzLWludHJvQ29udGVudC0zWG5GRiB7XFxuICAgIHdpZHRoOiAzMDBweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xcbiAgLkFjYWRzLWludHJvQ29udGVudC0zWG5GRiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHBhZGRpbmctbGVmdDogMDtcXG4gICAgcGFkZGluZy1yaWdodDogMDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2FjYWRzL0FjYWRzLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWiw0REFBNEQ7VUFDcEQsb0RBQW9EO0NBQzdEOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZUFBZTtFQUNmLDJCQUEyQjtFQUMzQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7SUFDSSxZQUFZO0dBQ2I7O0FBRUg7RUFDRSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGVBQWU7RUFDZixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsZ0JBQWdCO0VBQ2hCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixvQkFBb0I7TUFDaEIsc0JBQXNCO0NBQzNCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsY0FBYztFQUNkLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osOEJBQThCO0VBQzlCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztDQUNmOztBQUVEO0VBQ0Usb0JBQW9CO0VBQ3BCLGlDQUFpQztFQUNqQyxnQkFBZ0I7Q0FDakI7O0FBRUQ7RUFDRTtJQUNFLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsWUFBWTtJQUNaLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsa0JBQWtCO0dBQ25COztJQUVDO01BQ0UsWUFBWTtNQUNaLG1CQUFtQjtNQUNuQixnQkFBZ0I7S0FDakI7O0VBRUg7SUFDRSxnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLDhCQUE4QjtHQUMvQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixZQUFZO0lBQ1osbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztJQUVDO01BQ0UsY0FBYztNQUNkLGFBQWE7S0FDZDs7RUFFSDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsZ0JBQWdCO0lBQ2hCLFlBQVk7R0FDYjs7RUFFRDtJQUNFLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGFBQWE7R0FDZDtDQUNGOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7R0FDbEI7Q0FDRlwiLFwiZmlsZVwiOlwiQWNhZHMuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucm9vdCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5pbnRybyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogMyU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTtcXG59XFxuXFxuLmludHJvTGVmdFBhbmUge1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLmplZXQge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uaW50cm9Db250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcXG4gIHdpZHRoOiA0MjBweDtcXG59XFxuXFxuLmludHJvUmlnaHRQYW5lIHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uaW50cm9SaWdodFBhbmUgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuLnN0YXRzRGl2IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNlM2U1ZjtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAzNjZweDtcXG59XFxuXFxuLmVhY2hEaXYge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uc3RhdFZhbHVlIHtcXG4gIGhlaWdodDogMzAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgY29sb3I6ICNmZmFiMDA7XFxufVxcblxcbi5zdGF0TmFtZSB7XFxuICBoZWlnaHQ6IDMwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLnN0YXRJbWFnZSB7XFxuICBoZWlnaHQ6IDQwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBlbmQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xcbn1cXG5cXG4uY29udGVudCB7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG59XFxuXFxuLmNvbnRlbnRJbWFnZURpdiB7XFxuICBoZWlnaHQ6IDEzNnB4O1xcbiAgd2lkdGg6IDEzNnB4O1xcbn1cXG5cXG4uc2NyZWVuSW1hZ2Uge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDY4cHg7XFxufVxcblxcbi5oZWFkaW5nQ29udGVudCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLm1haW5Db250ZW50IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4udGVzdGltb25pYWxzVGl0bGUge1xcbiAgaGVpZ2h0OiAzOHB4O1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxuICBwYWRkaW5nOiAyJTtcXG59XFxuXFxuLnRlc3RpbW9uaWFsc1RpdGxlOjphZnRlciB7XFxuICBjb250ZW50OiAnJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogMnB4O1xcbiAgYm9yZGVyLXRvcDogc29saWQgM3B4ICNlYzRjNmY7XFxuICBwYWRkaW5nLXRvcDogMTJweDtcXG59XFxuXFxuLmNvbnRlbnRTdWJkaXYge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4ubGlzdCB7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgbGlzdC1zdHlsZS10eXBlOiBkaXNjICFpbXBvcnRhbnQ7XFxuICBtYXJnaW4tbGVmdDogNCU7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5yb290IHtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLnN0YXRzRGl2IHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLWJvdHRvbTogMiU7XFxuICB9XFxuXFxuICAgIC5zdGF0c0RpdiAuZWFjaERpdiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICAgIG1hcmdpbi10b3A6IDEwJTtcXG4gICAgfVxcblxcbiAgLnN0YXRWYWx1ZSB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgfVxcblxcbiAgLmNvbnRlbnRGb3JTbWFsbCB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jb250ZW50SW1hZ2VEaXZTbWFsbCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgICAuY29udGVudEltYWdlRGl2U21hbGwgaW1nIHtcXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xcbiAgICAgIHdpZHRoOiAxMDBweDtcXG4gICAgfVxcblxcbiAgLnNjcmVlbkltYWdlU21hbGwge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuaGVhZGluZ0NvbnRlbnRTbWFsbCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLm1haW5Db250ZW50U21hbGwge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZTo6YWZ0ZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0cHg7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLmludHJvTGVmdFBhbmUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5qZWV0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgfVxcblxcbiAgLmxpbmsge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKGRldmljZS13aWR0aDogMTAyNHB4KSB7XFxuICAuc3RhdFZhbHVlIHtcXG4gICAgZm9udC1zaXplOiA2MHB4O1xcbiAgICBoZWlnaHQ6IDI1JTtcXG4gIH1cXG5cXG4gIC5zdGF0c0RpdiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDMwMHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicm9vdFwiOiBcIkFjYWRzLXJvb3QtMy1oZVVcIixcblx0XCJpbnRyb1wiOiBcIkFjYWRzLWludHJvLUlxMHhEXCIsXG5cdFwiaW50cm9MZWZ0UGFuZVwiOiBcIkFjYWRzLWludHJvTGVmdFBhbmUtRmZiYW1cIixcblx0XCJqZWV0XCI6IFwiQWNhZHMtamVldC0xNGdPYlwiLFxuXHRcImNvYWNoaW5nSW5zdGl0dXRlc1wiOiBcIkFjYWRzLWNvYWNoaW5nSW5zdGl0dXRlcy0ycGhSQVwiLFxuXHRcImludHJvQ29udGVudFwiOiBcIkFjYWRzLWludHJvQ29udGVudC0zWG5GRlwiLFxuXHRcImludHJvUmlnaHRQYW5lXCI6IFwiQWNhZHMtaW50cm9SaWdodFBhbmUtMkMtNzBcIixcblx0XCJzdGF0c0RpdlwiOiBcIkFjYWRzLXN0YXRzRGl2LTF0QVdjXCIsXG5cdFwiZWFjaERpdlwiOiBcIkFjYWRzLWVhY2hEaXYtM3h4UWtcIixcblx0XCJzdGF0VmFsdWVcIjogXCJBY2Fkcy1zdGF0VmFsdWUtZGNKRGdcIixcblx0XCJzdGF0TmFtZVwiOiBcIkFjYWRzLXN0YXROYW1lLTFfWTEtXCIsXG5cdFwic3RhdEltYWdlXCI6IFwiQWNhZHMtc3RhdEltYWdlLXlLOURfXCIsXG5cdFwiY29udGVudFwiOiBcIkFjYWRzLWNvbnRlbnQtMUxEQ0dcIixcblx0XCJjb250ZW50SW1hZ2VEaXZcIjogXCJBY2Fkcy1jb250ZW50SW1hZ2VEaXYtMmRSMFBcIixcblx0XCJzY3JlZW5JbWFnZVwiOiBcIkFjYWRzLXNjcmVlbkltYWdlLTFsa29sXCIsXG5cdFwiaGVhZGluZ0NvbnRlbnRcIjogXCJBY2Fkcy1oZWFkaW5nQ29udGVudC0zeHZvYVwiLFxuXHRcIm1haW5Db250ZW50XCI6IFwiQWNhZHMtbWFpbkNvbnRlbnQtMk1kU2VcIixcblx0XCJ0ZXN0aW1vbmlhbHNUaXRsZVwiOiBcIkFjYWRzLXRlc3RpbW9uaWFsc1RpdGxlLTJxV2xaXCIsXG5cdFwiY29udGVudFN1YmRpdlwiOiBcIkFjYWRzLWNvbnRlbnRTdWJkaXYtMlpNZEpcIixcblx0XCJsaXN0XCI6IFwiQWNhZHMtbGlzdC0xc0I4b1wiLFxuXHRcImNvbnRlbnRGb3JTbWFsbFwiOiBcIkFjYWRzLWNvbnRlbnRGb3JTbWFsbC0xakk3RFwiLFxuXHRcImNvbnRlbnRJbWFnZURpdlNtYWxsXCI6IFwiQWNhZHMtY29udGVudEltYWdlRGl2U21hbGwtMzJiOTlcIixcblx0XCJzY3JlZW5JbWFnZVNtYWxsXCI6IFwiQWNhZHMtc2NyZWVuSW1hZ2VTbWFsbC0ybEdVY1wiLFxuXHRcImhlYWRpbmdDb250ZW50U21hbGxcIjogXCJBY2Fkcy1oZWFkaW5nQ29udGVudFNtYWxsLTE5UjhCXCIsXG5cdFwibWFpbkNvbnRlbnRTbWFsbFwiOiBcIkFjYWRzLW1haW5Db250ZW50U21hbGwtN0VaUFFcIixcblx0XCJsaW5rXCI6IFwiQWNhZHMtbGluay0xZ0NhU1wiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG4vLyBpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExpbmsgZnJvbSAnY29tcG9uZW50cy9MaW5rJztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCBTbGlkZXNob3cgZnJvbSAnY29tcG9uZW50cy9TbGlkZXNob3cnO1xuaW1wb3J0IFJlcXVlc3REZW1vIGZyb20gJ2NvbXBvbmVudHMvUmVxdWVzdERlbW8nO1xuaW1wb3J0IHMgZnJvbSAnLi9BY2Fkcy5zY3NzJztcblxuY29uc3QgU1RBVFMgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2luc3RpdHV0ZS5zdmcnLFxuICAgIHN0YXQ6ICcxNTArJyxcbiAgICBuYW1lOiAnSU5TVElUVVRFUycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L21vcnRhci1ib2FyZC5zdmcnLFxuICAgIHN0YXQ6ICcxMDBrKycsXG4gICAgbmFtZTogJ1NUVURFTlRTJyxcbiAgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2luZGlhLnN2ZycsIHN0YXQ6ICc0JywgbmFtZTogJ1NUQVRFUycgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2NpdHkuc3ZnJywgc3RhdDogJzcnLCBuYW1lOiAnQ0lUSUVTJyB9LFxuXTtcblxuY29uc3QgQ09OVEVOVCA9IFtcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdSZS1pbWFnaW5lIHByb2dyZXNzIHJlcG9ydHMnLFxuICAgIG1haW5Db250ZW50OiBbXG4gICAgICAnSG9saXN0aWMsIEluc2lnaHRmdWwgYW5kIEluZm9ybWF0aXZlIHJlcG9ydHMuJyxcbiAgICAgICdHZW5lcmF0ZSByZXBvcnRzIGluIGxlc3MgdGhhbiA1IG1pbiBmb3IgYSBjbGFzcy4nLFxuICAgICAgJ0FjdGlvbmFibGUgaW5mb3JtYXRpb24gaW4gYW4gdW5kZXJzdGFuZGFibGUgZm9ybWF0LicsXG4gICAgICAnRmFjaWxpdGF0ZXMgcHJvZHVjdGl2ZSBkaWFsb2d1ZSBhbW9uZyBlZHVjYXRvcnMsIHBhcmVudHMgJiBzdHVkZW50cy4nLFxuICAgIF0sXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9wZXJjZW50LnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2FjYWRzLzAxLTF4LnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnR28gQmV5b25kIEdyYWRlcycsXG4gICAgbWFpbkNvbnRlbnQ6IFtcbiAgICAgICdEYXRhLWRyaXZlbiBkaWFnbm9zaXMgdG8gaWRlbnRpZnkgc3R1ZGVudCBsZWFybmluZyBuZWVkcyBhbmQgaWRlbnRpZnkgYXJlYXMgZm9yIHRhcmdldGVkIGluc3RydWN0aW9uLicsXG4gICAgICAnQWN0aW9uYWJsZSBpbnNpZ2h0cyB0byBzcG90IHBhdHRlcm5zLCBzdWdnZXN0IGFjdGlvbnMgYW5kIG1ha2UgcHJlZGljdGlvbnMuJyxcbiAgICAgICdUcmFjayB0aGUgcHJvZ3Jlc3Mgb2YgYSBzaW5nbGUgc3R1ZGVudCwgc2VlIHRoZSBvdmVyYWxsIGNsYXNzIHBlcmZvcm1hbmNlIGFuZCBjb21wYXJlIGNsYXNzZXMuJyxcbiAgICBdLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvYmx1Yi5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9hY2Fkcy8wMi0xeC5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ1BlcmZvcm1hbmNlIE1hbmFnZW1lbnQgU3lzdGVtJyxcbiAgICBtYWluQ29udGVudDogW1xuICAgICAgJ0Zvcm1hdGl2ZSBBc3Nlc3NtZW50cyBNYXJrcy4nLFxuICAgICAgJ1N1bW1hdGl2ZSBBc3Nlc3NtZW50cyBNYXJrcy4nLFxuICAgICAgJ0F0dGVuZGFuY2UgTWFuYWdlbWVudC4nLFxuICAgIF0sXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC93ZWlnaHRzLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2FjYWRzLzQtMXgucG5nJyxcbiAgfSxcbiAgLy8ge1xuICAvLyAgIGhlYWRlckNvbnRlbnQ6ICdDby1TY2hvbGFzdGljcycsXG4gIC8vICAgbWFpbkNvbnRlbnQ6IFtcbiAgLy8gICAgICdGb3JtYXRpdmUgQXNzZXNzbWVudHMgTWFya3MnLFxuICAvLyAgICAgJ1N1bW1hdGl2ZSBBc3Nlc3NtZW50cyBNYXJrcycsXG4gIC8vICAgICAnQXR0ZW5kYW5jZSBNYW5hZ2VtZW50JyxcbiAgLy8gICBdLFxuICAvLyAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvZ3JvdXAuc3ZnJyxcbiAgLy8gICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvYWNhZHMvNC0xeC5wbmcnLFxuICAvLyB9LFxuXTtcblxuY29uc3QgU0xJREVTSE9XID0gW1xuICB7XG4gICAgaW1hZ2U6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy90ZWxhbmdhbmEucG5nJyxcbiAgICBuYW1lOiAnTXIuIFNyaW5pdmFzIEthbm5hbicsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBTcmluaXZhcyBNdXJ0aHknLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MnLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICcnLFxuICAgIG5hbWU6ICdNci4gQW5kcmV3IFRhZycsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBNdXNzb2xpbmknLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MnLFxuICB9LFxuXTtcblxuY2xhc3MgQWNhZHMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAvLyBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAvL1xuICAvLyB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIFJlYWN0R0EuaW5pdGlhbGl6ZSh3aW5kb3cuQXBwLmdvb2dsZVRyYWNraW5nSWQsIHtcbiAgICAgIGRlYnVnOiBmYWxzZSxcbiAgICB9KTtcbiAgICBSZWFjdEdBLnBhZ2V2aWV3KHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAZGVzY3JpcHRpb24gUmV0dXJucyB3aGF0IGFsaWdubWVudCBzaG91bGQgdGhlIGNvbnRlbnQgdGFrZSBlZy4gZmxleC1zdGFydCBmbGV4IGVuZCBjZW50ZXJcbiAgICogQHBhcmFtIGNvbnRlbnRJbmRleCxrZXlcbiAgICogQGF1dGhvciBTdXNocnV0aFxuICAgKiAqL1xuICBtYWtlQWxpZ25tZW50ID0gKGNvbnRlbnRJbmRleCwga2V5KSA9PiB7XG4gICAgaWYgKGNvbnRlbnRJbmRleCAlIDIgPT09IGtleSkge1xuICAgICAgaWYgKGtleSA9PT0gMCkge1xuICAgICAgICByZXR1cm4gJ2ZsZXgtc3RhcnQnO1xuICAgICAgfVxuICAgICAgcmV0dXJuICdmbGV4LWVuZCc7XG4gICAgfVxuICAgIHJldHVybiAnY2VudGVyJztcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgdmlldyA9IChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvb3R9IHJvd2B9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb30gcm93YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW50cm9MZWZ0UGFuZX0gY29sIG00YH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5qZWV0fT5BQ0FEUzwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29hY2hpbmdJbnN0aXR1dGVzfT5Gb3IgU2Nob29sczwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW50cm9Db250ZW50fT5cbiAgICAgICAgICAgICAgQUNBRFMgaXMgYSBwZXJmb3JtYW5jZSBtYW5hZ2VtZW50IHN5c3RlbSBmb3Igc2Nob29scyB3aGljaCBoZWxwc1xuICAgICAgICAgICAgICBpbXByb3ZlIGV4YW0gcGVyZm9ybWFuY2UgYW5kIG1heGltaXplIGxlYXJuaW5nIG91dGNvbWVzIG9mIHRoZWlyXG4gICAgICAgICAgICAgIHN0dWRlbnRzLlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8TGluayB0bz1cIi9yZXF1ZXN0LWRlbW9cIiBjbGFzc05hbWU9e3MubGlua30+XG4gICAgICAgICAgICAgIDxidXR0b24+UkVRVUVTVCBBIERFTU88L2J1dHRvbj5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb1JpZ2h0UGFuZX0gY29sIG04YH0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvcHJvZHVjdHMvYWNhZHMvaGVyby5zdmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxMDAlXCJcbiAgICAgICAgICAgICAgd2lkdGg9XCIxMDAlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zdGF0c0Rpdn0gcm93YH0+XG4gICAgICAgICAge1NUQVRTLm1hcCgoc3RhdCwgc3RhdEluZGV4KSA9PiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5lYWNoRGl2fSBjb2wgbTMgYH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN0YXRJbWFnZX0+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e3N0YXQuaWNvbn0gYWx0PVwiXCIgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Muc3RhdFZhbHVlfVxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBib3JkZXJSaWdodDpcbiAgICAgICAgICAgICAgICAgICAgc3RhdEluZGV4ID09PSAzXG4gICAgICAgICAgICAgICAgICAgICAgPyAnbm9uZSdcbiAgICAgICAgICAgICAgICAgICAgICA6ICdzb2xpZCAxcHggcmdiYSgxMzksIDEzOSwgMjIzLCAwLjMpJyxcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3N0YXQuc3RhdH17JyAnfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3RhdE5hbWV9PntzdGF0Lm5hbWV9IDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7Q09OVEVOVC5tYXAoKGNvbnRlbnQsIGNvbnRlbnRJbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50fSByb3cgaGlkZS1vbi14c2B9PlxuICAgICAgICAgICAgICB7WzAsIDFdLm1hcChrZXkgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRJbmRleCAlIDIgPT09IGtleVxuICAgICAgICAgICAgICAgICAgICAgID8gYGNvbCBtNSBzMTIgJHtzLmNvbnRlbnRTdWJkaXZ9YFxuICAgICAgICAgICAgICAgICAgICAgIDogYGNvbCBtNyBzMTIgJHtzLmNvbnRlbnRTdWJkaXZ9YFxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeUNvbnRlbnQ6IHRoaXMubWFrZUFsaWdubWVudChjb250ZW50SW5kZXgsIGtleSksXG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIHtjb250ZW50SW5kZXggJSAyID09PSBrZXkgPyAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudEltYWdlRGl2fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50Lmljb259IC8+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ0NvbnRlbnR9PlxuICAgICAgICAgICAgICAgICAgICAgICAge2NvbnRlbnQuaGVhZGVyQ29udGVudH1cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tYWluQ29udGVudH0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtjb250ZW50Lm1haW5Db250ZW50Lm1hcChlYWNoUG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9e3MubGlzdH0+e2VhY2hQb2ludH08L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2NyZWVuSW1hZ2V9PlxuICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfSAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50Rm9yU21hbGx9IHJvdyBoaWRlLW9uLW0tYW5kLXVwYH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRJbWFnZURpdlNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17Y29udGVudC5pY29ufSAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ0NvbnRlbnRTbWFsbH0+XG4gICAgICAgICAgICAgICAge2NvbnRlbnQuaGVhZGVyQ29udGVudH1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1haW5Db250ZW50U21hbGx9PlxuICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgIHtjb250ZW50Lm1haW5Db250ZW50Lm1hcChlYWNoUG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPXtzLmxpc3R9PntlYWNoUG9pbnR9PC9saT5cbiAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zY3JlZW5JbWFnZVNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfVxuICAgICAgICAgICAgICAgICAgd2lkdGg9XCI4Ny42JVwiXG4gICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI1NC40NSVcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkpfVxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3sgcGFkZGluZ1RvcDogJzY0cHgnLCBtYXJnaW5Cb3R0b206ICc1JScgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJyb3cgaGlkZVwiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0aW1vbmlhbHNUaXRsZX0+VGVzdGltb25pYWxzPC9kaXY+XG4gICAgICAgICAgPFNsaWRlc2hvdyBkYXRhPXtTTElERVNIT1d9IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8UmVxdWVzdERlbW8gLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gICAgcmV0dXJuIHZpZXc7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShBY2Fkcyk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0FjYWRzLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQWNhZHMuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0FjYWRzLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBY2FkcyBmcm9tICcuL0FjYWRzJztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnknLFxuICAgIGNodW5rczogWydBY2FkcyddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPEFjYWRzIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQU1BO0FBQ0E7QUFUQTtBQVlBO0FBQ0E7QUFLQTtBQUNBO0FBUkE7QUFXQTtBQUNBO0FBS0E7QUFDQTtBQVJBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBekNBO0FBNENBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBUUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQXpCQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7Ozs7Ozs7QUFlQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUtBO0FBQ0E7QUFEQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBcEpBO0FBQ0E7QUFxSkE7Ozs7Ozs7QUN6UEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVNBO0FBQ0E7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9