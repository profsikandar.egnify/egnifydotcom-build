require("source-map-support").install();
exports.ids = ["Acads~Jeet~Otp"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slideshow/Slideshow.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Slideshow-slideShow-5SmdA {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Slideshow-leftArrow-2FbD- {\n  -webkit-transform: rotate(-180deg);\n      -ms-transform: rotate(-180deg);\n          transform: rotate(-180deg);\n}\n\n.Slideshow-profileImage-3R-QF {\n  height: 104px;\n  width: 104px;\n  border-radius: 52px;\n  background-color: lightgrey;\n  margin-top: 10px;\n  position: absolute;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Slideshow-slideCard-1Ri6F {\n  height: 100%;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 2px 6px 16px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 2px 6px 16px 0 rgba(0, 0, 0, 0.16);\n  margin-top: 60px;\n  margin-bottom: 100px;\n  padding-top: 58px;\n  padding-left: 78px;\n  padding-right: 78px;\n  padding-bottom: 58px;\n}\n\n.Slideshow-profileName-2KDeD {\n  font-size: 24px;\n  font-weight: 600;\n  text-align: center;\n  color: #3e3e5f;\n  margin-bottom: 8px;\n}\n\n.Slideshow-profileRole-p0Isj {\n  font-size: 20px;\n  font-weight: 300;\n  text-align: center;\n  color: #5f6368;\n  margin-bottom: 24px;\n}\n\n.Slideshow-profileContent-1iafo {\n  height: 100%;\n  font-size: 20px;\n  font-weight: 300;\n  line-height: 1.6;\n  text-align: center;\n  color: #5f6368;\n  white-space: pre-wrap;\n}\n\n.Slideshow-dotDiv-ptcVX {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Slideshow-dot-v6D88 {\n  width: 12px;\n  height: 12px;\n  border-radius: 6px;\n  -webkit-transition: all 0.5s ease-out;\n  -o-transition: all 0.5s ease-out;\n  transition: all 0.5s ease-out;\n}\n\n.Slideshow-slider-3DV21 {\n  position: relative;\n  width: 50%;\n  margin: 0 auto;\n  white-space: nowrap;\n  overflow: hidden;\n  margin-left: 40px;\n  margin-right: 40px;\n}\n\n.Slideshow-slideWrapper-1Cp3w {\n  position: relative;\n  height: 100%;\n  width: 100%;\n}\n\n.Slideshow-slide-1t3Rc {\n  display: inline-block;\n  height: 100%;\n  width: 100%;\n}\n\n@media only screen and (max-width: 960px) {\n  .Slideshow-slider-3DV21 {\n    width: 65%;\n  }\n\n  .Slideshow-slideCard-1Ri6F {\n    margin-top: 45%;\n    margin-bottom: 20%;\n    padding: 8%;\n  }\n\n  .Slideshow-profileName-2KDeD {\n    font-size: 20px;\n  }\n\n  .Slideshow-profileRole-p0Isj {\n    font-size: 18px;\n  }\n\n  .Slideshow-profileContent-1iafo {\n    font-size: 16px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .Slideshow-slider-3DV21 {\n    width: 65%;\n  }\n\n  .Slideshow-slideCard-1Ri6F {\n    margin-top: 55%;\n    margin-bottom: 20%;\n    padding: 8%;\n  }\n\n  .Slideshow-profileName-2KDeD {\n    font-size: 14px;\n  }\n\n  .Slideshow-profileRole-p0Isj {\n    font-size: 12px;\n  }\n\n  .Slideshow-profileContent-1iafo {\n    font-size: 10px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slideshow/Slideshow.scss"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mCAAmC;MAC/B,+BAA+B;UAC3B,2BAA2B;CACpC;;AAED;EACE,cAAc;EACd,aAAa;EACb,oBAAoB;EACpB,4BAA4B;EAC5B,iBAAiB;EACjB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,uDAAuD;UAC/C,+CAA+C;EACvD,iBAAiB;EACjB,qBAAqB;EACrB,kBAAkB;EAClB,mBAAmB;EACnB,oBAAoB;EACpB,qBAAqB;CACtB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;EACf,oBAAoB;CACrB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;EACf,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,sCAAsC;EACtC,iCAAiC;EACjC,8BAA8B;CAC/B;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,eAAe;EACf,oBAAoB;EACpB,iBAAiB;EACjB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,YAAY;CACb;;AAED;EACE,sBAAsB;EACtB,aAAa;EACb,YAAY;CACb;;AAED;EACE;IACE,WAAW;GACZ;;EAED;IACE,gBAAgB;IAChB,mBAAmB;IACnB,YAAY;GACb;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,WAAW;GACZ;;EAED;IACE,gBAAgB;IAChB,mBAAmB;IACnB,YAAY;GACb;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"Slideshow.scss","sourcesContent":[".slideShow {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.leftArrow {\n  -webkit-transform: rotate(-180deg);\n      -ms-transform: rotate(-180deg);\n          transform: rotate(-180deg);\n}\n\n.profileImage {\n  height: 104px;\n  width: 104px;\n  border-radius: 52px;\n  background-color: lightgrey;\n  margin-top: 10px;\n  position: absolute;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.slideCard {\n  height: 100%;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 2px 6px 16px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 2px 6px 16px 0 rgba(0, 0, 0, 0.16);\n  margin-top: 60px;\n  margin-bottom: 100px;\n  padding-top: 58px;\n  padding-left: 78px;\n  padding-right: 78px;\n  padding-bottom: 58px;\n}\n\n.profileName {\n  font-size: 24px;\n  font-weight: 600;\n  text-align: center;\n  color: #3e3e5f;\n  margin-bottom: 8px;\n}\n\n.profileRole {\n  font-size: 20px;\n  font-weight: 300;\n  text-align: center;\n  color: #5f6368;\n  margin-bottom: 24px;\n}\n\n.profileContent {\n  height: 100%;\n  font-size: 20px;\n  font-weight: 300;\n  line-height: 1.6;\n  text-align: center;\n  color: #5f6368;\n  white-space: pre-wrap;\n}\n\n.dotDiv {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.dot {\n  width: 12px;\n  height: 12px;\n  border-radius: 6px;\n  -webkit-transition: all 0.5s ease-out;\n  -o-transition: all 0.5s ease-out;\n  transition: all 0.5s ease-out;\n}\n\n.slider {\n  position: relative;\n  width: 50%;\n  margin: 0 auto;\n  white-space: nowrap;\n  overflow: hidden;\n  margin-left: 40px;\n  margin-right: 40px;\n}\n\n.slideWrapper {\n  position: relative;\n  height: 100%;\n  width: 100%;\n}\n\n.slide {\n  display: inline-block;\n  height: 100%;\n  width: 100%;\n}\n\n@media only screen and (max-width: 960px) {\n  .slider {\n    width: 65%;\n  }\n\n  .slideCard {\n    margin-top: 45%;\n    margin-bottom: 20%;\n    padding: 8%;\n  }\n\n  .profileName {\n    font-size: 20px;\n  }\n\n  .profileRole {\n    font-size: 18px;\n  }\n\n  .profileContent {\n    font-size: 16px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .slider {\n    width: 65%;\n  }\n\n  .slideCard {\n    margin-top: 55%;\n    margin-bottom: 20%;\n    padding: 8%;\n  }\n\n  .profileName {\n    font-size: 14px;\n  }\n\n  .profileRole {\n    font-size: 12px;\n  }\n\n  .profileContent {\n    font-size: 10px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"slideShow": "Slideshow-slideShow-5SmdA",
	"leftArrow": "Slideshow-leftArrow-2FbD-",
	"profileImage": "Slideshow-profileImage-3R-QF",
	"slideCard": "Slideshow-slideCard-1Ri6F",
	"profileName": "Slideshow-profileName-2KDeD",
	"profileRole": "Slideshow-profileRole-p0Isj",
	"profileContent": "Slideshow-profileContent-1iafo",
	"dotDiv": "Slideshow-dotDiv-ptcVX",
	"dot": "Slideshow-dot-v6D88",
	"slider": "Slideshow-slider-3DV21",
	"slideWrapper": "Slideshow-slideWrapper-1Cp3w",
	"slide": "Slideshow-slide-1t3Rc"
};

/***/ }),

/***/ "./src/components/Slideshow/Slideshow.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Slideshow/Slideshow.scss");
/* harmony import */ var _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Slideshow_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slideshow/Slideshow.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




 // monitor manual clicks on slideshow

let isClicked = false;

class Slideshow extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "goToPrevSlide", () => {
      if (this.state.currentIndex === 0) return;
      this.setState(prevState => ({
        currentIndex: prevState.currentIndex - 1,
        translateValue: prevState.translateValue + this.slideWidth()
      }));
    });

    _defineProperty(this, "goToNextSlide", () => {
      if (this.state.currentIndex === this.props.data.length - 1) {
        this.setState({
          currentIndex: 0,
          translateValue: 0
        });
      } else {
        this.setState(prevState => ({
          currentIndex: prevState.currentIndex + 1,
          translateValue: prevState.translateValue + -this.slideWidth()
        }));
      }
    });

    _defineProperty(this, "slideWidth", () => {
      if (document.querySelector('.slideComponent')) {
        return document.querySelector('.slideComponent').clientWidth;
      }

      return 0;
    });

    _defineProperty(this, "displaySlideCard", data => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slideCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, data.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileRole,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, data.role), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }, data.content)));

    _defineProperty(this, "displayDots", () => {
      const data = this.props.data; //eslint-disable-line

      const currentIndex = this.state.currentIndex; //eslint-disable-line

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotDiv,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, data.map((dot, dotIndex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
        style: {
          marginLeft: dotIndex !== 0 ? '20px' : '0',
          backgroundColor: currentIndex === dotIndex ? '#ec4c6f' : '#d8d8d8',
          cursor: currentIndex === dotIndex ? 'default' : 'pointer'
        },
        onClick: () => {
          isClicked = true;
          this.handleDotClick(dotIndex);
        },
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      })));
    });

    _defineProperty(this, "handleDotClick", index => {
      let currentIndex = this.state.currentIndex; //eslint-disable-line

      currentIndex = index;
      this.setState({
        currentIndex,
        translateValue: -index * this.slideWidth()
      });
    });

    _defineProperty(this, "automaticDotClick", () => {
      let index = this.state.currentIndex;

      if (index === this.props.data.length - 1) {
        index = -1;
      }

      this.handleDotClick(index + 1);
    });

    this.state = {
      currentIndex: 0,
      translateValue: 0
    };
  }

  componentDidMount() {
    setInterval(() => {
      if (isClicked === true) {
        isClicked = false;
        return;
      }

      this.automaticDotClick();
    }, 5000);
  }

  render() {
    const view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slideShow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.leftArrow,
      style: {
        cursor: 'pointer'
      },
      role: "presentation",
      onClick: () => {
        isClicked = true;
        this.goToPrevSlide();
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 136
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/arrow-forward.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slider,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slideWrapper,
      style: {
        transform: `translateX(${this.state.translateValue}px)`,
        transition: 'transform ease-out 0.45s'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    }, this.props.data.map(eachPerson => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slide} slideComponent`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        display: 'flex',
        justifyContent: 'center'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 159
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: eachPerson.image,
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.slideCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165
      },
      __self: this
    }, eachPerson.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileRole,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166
      },
      __self: this
    }, eachPerson.role), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profileContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 167
      },
      __self: this
    }, eachPerson.content))))), this.displayDots()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        cursor: 'pointer'
      },
      role: "presentation",
      onClick: () => {
        isClicked = true;
        this.goToNextSlide();
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 174
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/arrow-forward.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 184
      },
      __self: this
    })));
    return view;
  }

}

_defineProperty(Slideshow, "propTypes", {
  data: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object).isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Slideshow_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Slideshow));

/***/ }),

/***/ "./src/components/Slideshow/Slideshow.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slideshow/Slideshow.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0FjYWRzfkplZXR+T3RwLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvU2xpZGVzaG93L1NsaWRlc2hvdy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL1NsaWRlc2hvdy9TbGlkZXNob3cuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvU2xpZGVzaG93L1NsaWRlc2hvdy5zY3NzP2ZhMjEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5TbGlkZXNob3ctc2xpZGVTaG93LTVTbWRBIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlNsaWRlc2hvdy1sZWZ0QXJyb3ctMkZiRC0ge1xcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgtMTgwZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoLTE4MGRlZyk7XFxuICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC0xODBkZWcpO1xcbn1cXG5cXG4uU2xpZGVzaG93LXByb2ZpbGVJbWFnZS0zUi1RRiB7XFxuICBoZWlnaHQ6IDEwNHB4O1xcbiAgd2lkdGg6IDEwNHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTJweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JleTtcXG4gIG1hcmdpbi10b3A6IDEwcHg7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5TbGlkZXNob3ctc2xpZGVDYXJkLTFSaTZGIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA2cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDJweCA2cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxuICBtYXJnaW4tdG9wOiA2MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMTAwcHg7XFxuICBwYWRkaW5nLXRvcDogNThweDtcXG4gIHBhZGRpbmctbGVmdDogNzhweDtcXG4gIHBhZGRpbmctcmlnaHQ6IDc4cHg7XFxuICBwYWRkaW5nLWJvdHRvbTogNThweDtcXG59XFxuXFxuLlNsaWRlc2hvdy1wcm9maWxlTmFtZS0yS0RlRCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5TbGlkZXNob3ctcHJvZmlsZVJvbGUtcDBJc2oge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLlNsaWRlc2hvdy1wcm9maWxlQ29udGVudC0xaWFmbyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xcbn1cXG5cXG4uU2xpZGVzaG93LWRvdERpdi1wdGNWWCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5TbGlkZXNob3ctZG90LXY2RDg4IHtcXG4gIHdpZHRoOiAxMnB4O1xcbiAgaGVpZ2h0OiAxMnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2Utb3V0O1xcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1vdXQ7XFxufVxcblxcbi5TbGlkZXNob3ctc2xpZGVyLTNEVjIxIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiA1MCU7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgbWFyZ2luLWxlZnQ6IDQwcHg7XFxuICBtYXJnaW4tcmlnaHQ6IDQwcHg7XFxufVxcblxcbi5TbGlkZXNob3ctc2xpZGVXcmFwcGVyLTFDcDN3IHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uU2xpZGVzaG93LXNsaWRlLTF0M1JjIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIGhlaWdodDogMTAwJTtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk2MHB4KSB7XFxuICAuU2xpZGVzaG93LXNsaWRlci0zRFYyMSB7XFxuICAgIHdpZHRoOiA2NSU7XFxuICB9XFxuXFxuICAuU2xpZGVzaG93LXNsaWRlQ2FyZC0xUmk2RiB7XFxuICAgIG1hcmdpbi10b3A6IDQ1JTtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjAlO1xcbiAgICBwYWRkaW5nOiA4JTtcXG4gIH1cXG5cXG4gIC5TbGlkZXNob3ctcHJvZmlsZU5hbWUtMktEZUQge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICB9XFxuXFxuICAuU2xpZGVzaG93LXByb2ZpbGVSb2xlLXAwSXNqIHtcXG4gICAgZm9udC1zaXplOiAxOHB4O1xcbiAgfVxcblxcbiAgLlNsaWRlc2hvdy1wcm9maWxlQ29udGVudC0xaWFmbyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLlNsaWRlc2hvdy1zbGlkZXItM0RWMjEge1xcbiAgICB3aWR0aDogNjUlO1xcbiAgfVxcblxcbiAgLlNsaWRlc2hvdy1zbGlkZUNhcmQtMVJpNkYge1xcbiAgICBtYXJnaW4tdG9wOiA1NSU7XFxuICAgIG1hcmdpbi1ib3R0b206IDIwJTtcXG4gICAgcGFkZGluZzogOCU7XFxuICB9XFxuXFxuICAuU2xpZGVzaG93LXByb2ZpbGVOYW1lLTJLRGVEIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgfVxcblxcbiAgLlNsaWRlc2hvdy1wcm9maWxlUm9sZS1wMElzaiB7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gIH1cXG5cXG4gIC5TbGlkZXNob3ctcHJvZmlsZUNvbnRlbnQtMWlhZm8ge1xcbiAgICBmb250LXNpemU6IDEwcHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvU2xpZGVzaG93L1NsaWRlc2hvdy5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsbUNBQW1DO01BQy9CLCtCQUErQjtVQUMzQiwyQkFBMkI7Q0FDcEM7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsYUFBYTtFQUNiLG9CQUFvQjtFQUNwQiw0QkFBNEI7RUFDNUIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtDQUM3Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLHVEQUF1RDtVQUMvQywrQ0FBK0M7RUFDdkQsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtDQUM3Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHNDQUFzQztFQUN0QyxpQ0FBaUM7RUFDakMsOEJBQThCO0NBQy9COztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIsYUFBYTtFQUNiLFlBQVk7Q0FDYjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRlwiLFwiZmlsZVwiOlwiU2xpZGVzaG93LnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnNsaWRlU2hvdyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5sZWZ0QXJyb3cge1xcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgtMTgwZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoLTE4MGRlZyk7XFxuICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC0xODBkZWcpO1xcbn1cXG5cXG4ucHJvZmlsZUltYWdlIHtcXG4gIGhlaWdodDogMTA0cHg7XFxuICB3aWR0aDogMTA0cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MnB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xcbiAgbWFyZ2luLXRvcDogMTBweDtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLnNsaWRlQ2FyZCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAycHggNnB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNnB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbiAgbWFyZ2luLXRvcDogNjBweDtcXG4gIG1hcmdpbi1ib3R0b206IDEwMHB4O1xcbiAgcGFkZGluZy10b3A6IDU4cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDc4cHg7XFxuICBwYWRkaW5nLXJpZ2h0OiA3OHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDU4cHg7XFxufVxcblxcbi5wcm9maWxlTmFtZSB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5wcm9maWxlUm9sZSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbn1cXG5cXG4ucHJvZmlsZUNvbnRlbnQge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcXG59XFxuXFxuLmRvdERpdiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5kb3Qge1xcbiAgd2lkdGg6IDEycHg7XFxuICBoZWlnaHQ6IDEycHg7XFxuICBib3JkZXItcmFkaXVzOiA2cHg7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2Utb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1vdXQ7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLW91dDtcXG59XFxuXFxuLnNsaWRlciB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogNTAlO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIG1hcmdpbi1sZWZ0OiA0MHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiA0MHB4O1xcbn1cXG5cXG4uc2xpZGVXcmFwcGVyIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uc2xpZGUge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5zbGlkZXIge1xcbiAgICB3aWR0aDogNjUlO1xcbiAgfVxcblxcbiAgLnNsaWRlQ2FyZCB7XFxuICAgIG1hcmdpbi10b3A6IDQ1JTtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjAlO1xcbiAgICBwYWRkaW5nOiA4JTtcXG4gIH1cXG5cXG4gIC5wcm9maWxlTmFtZSB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gIH1cXG5cXG4gIC5wcm9maWxlUm9sZSB7XFxuICAgIGZvbnQtc2l6ZTogMThweDtcXG4gIH1cXG5cXG4gIC5wcm9maWxlQ29udGVudCB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xcbiAgLnNsaWRlciB7XFxuICAgIHdpZHRoOiA2NSU7XFxuICB9XFxuXFxuICAuc2xpZGVDYXJkIHtcXG4gICAgbWFyZ2luLXRvcDogNTUlO1xcbiAgICBtYXJnaW4tYm90dG9tOiAyMCU7XFxuICAgIHBhZGRpbmc6IDglO1xcbiAgfVxcblxcbiAgLnByb2ZpbGVOYW1lIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgfVxcblxcbiAgLnByb2ZpbGVSb2xlIHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgfVxcblxcbiAgLnByb2ZpbGVDb250ZW50IHtcXG4gICAgZm9udC1zaXplOiAxMHB4O1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwic2xpZGVTaG93XCI6IFwiU2xpZGVzaG93LXNsaWRlU2hvdy01U21kQVwiLFxuXHRcImxlZnRBcnJvd1wiOiBcIlNsaWRlc2hvdy1sZWZ0QXJyb3ctMkZiRC1cIixcblx0XCJwcm9maWxlSW1hZ2VcIjogXCJTbGlkZXNob3ctcHJvZmlsZUltYWdlLTNSLVFGXCIsXG5cdFwic2xpZGVDYXJkXCI6IFwiU2xpZGVzaG93LXNsaWRlQ2FyZC0xUmk2RlwiLFxuXHRcInByb2ZpbGVOYW1lXCI6IFwiU2xpZGVzaG93LXByb2ZpbGVOYW1lLTJLRGVEXCIsXG5cdFwicHJvZmlsZVJvbGVcIjogXCJTbGlkZXNob3ctcHJvZmlsZVJvbGUtcDBJc2pcIixcblx0XCJwcm9maWxlQ29udGVudFwiOiBcIlNsaWRlc2hvdy1wcm9maWxlQ29udGVudC0xaWFmb1wiLFxuXHRcImRvdERpdlwiOiBcIlNsaWRlc2hvdy1kb3REaXYtcHRjVlhcIixcblx0XCJkb3RcIjogXCJTbGlkZXNob3ctZG90LXY2RDg4XCIsXG5cdFwic2xpZGVyXCI6IFwiU2xpZGVzaG93LXNsaWRlci0zRFYyMVwiLFxuXHRcInNsaWRlV3JhcHBlclwiOiBcIlNsaWRlc2hvdy1zbGlkZVdyYXBwZXItMUNwM3dcIixcblx0XCJzbGlkZVwiOiBcIlNsaWRlc2hvdy1zbGlkZS0xdDNSY1wiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9TbGlkZXNob3cuc2Nzcyc7XG5cbi8vIG1vbml0b3IgbWFudWFsIGNsaWNrcyBvbiBzbGlkZXNob3dcbmxldCBpc0NsaWNrZWQgPSBmYWxzZTtcblxuY2xhc3MgU2xpZGVzaG93IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBkYXRhOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMub2JqZWN0KS5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBjdXJyZW50SW5kZXg6IDAsXG4gICAgICB0cmFuc2xhdGVWYWx1ZTogMCxcbiAgICB9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgaWYgKGlzQ2xpY2tlZCA9PT0gdHJ1ZSkge1xuICAgICAgICBpc0NsaWNrZWQgPSBmYWxzZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5hdXRvbWF0aWNEb3RDbGljaygpO1xuICAgIH0sIDUwMDApO1xuICB9XG5cbiAgZ29Ub1ByZXZTbGlkZSA9ICgpID0+IHtcbiAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50SW5kZXggPT09IDApIHJldHVybjtcblxuICAgIHRoaXMuc2V0U3RhdGUocHJldlN0YXRlID0+ICh7XG4gICAgICBjdXJyZW50SW5kZXg6IHByZXZTdGF0ZS5jdXJyZW50SW5kZXggLSAxLFxuICAgICAgdHJhbnNsYXRlVmFsdWU6IHByZXZTdGF0ZS50cmFuc2xhdGVWYWx1ZSArIHRoaXMuc2xpZGVXaWR0aCgpLFxuICAgIH0pKTtcbiAgfTtcblxuICBnb1RvTmV4dFNsaWRlID0gKCkgPT4ge1xuICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRJbmRleCA9PT0gdGhpcy5wcm9wcy5kYXRhLmxlbmd0aCAtIDEpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBjdXJyZW50SW5kZXg6IDAsXG4gICAgICAgIHRyYW5zbGF0ZVZhbHVlOiAwLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUocHJldlN0YXRlID0+ICh7XG4gICAgICAgIGN1cnJlbnRJbmRleDogcHJldlN0YXRlLmN1cnJlbnRJbmRleCArIDEsXG4gICAgICAgIHRyYW5zbGF0ZVZhbHVlOiBwcmV2U3RhdGUudHJhbnNsYXRlVmFsdWUgKyAtdGhpcy5zbGlkZVdpZHRoKCksXG4gICAgICB9KSk7XG4gICAgfVxuICB9O1xuXG4gIHNsaWRlV2lkdGggPSAoKSA9PiB7XG4gICAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZUNvbXBvbmVudCcpKSB7XG4gICAgICByZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNsaWRlQ29tcG9uZW50JykuY2xpZW50V2lkdGg7XG4gICAgfVxuICAgIHJldHVybiAwO1xuICB9O1xuXG4gIC8qKlxuICAgKiBAZGVzY3JpcHRpb24gZGlzcGxheXMgc2xpZGVcbiAgICogQHBhcmFtIGRhdGEgb2JqZWN0XG4gICAqIEBhdXRob3IgU3VzaHJ1dGhcbiAgICogKi9cblxuICBkaXNwbGF5U2xpZGVDYXJkID0gZGF0YSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2xpZGVDYXJkfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByb2ZpbGVOYW1lfT57ZGF0YS5uYW1lfTwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJvZmlsZVJvbGV9PntkYXRhLnJvbGV9PC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcm9maWxlQ29udGVudH0+e2RhdGEuY29udGVudH08L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICAvKipcbiAgICogQGRlc2NyaXB0aW9uIGRpc3BsYXlzIHNsaWRlXG4gICAqIEBhdXRob3IgU3VzaHJ1dGhcbiAgICogKi9cblxuICBkaXNwbGF5RG90cyA9ICgpID0+IHtcbiAgICBjb25zdCBkYXRhID0gdGhpcy5wcm9wcy5kYXRhOyAvL2VzbGludC1kaXNhYmxlLWxpbmVcbiAgICBjb25zdCBjdXJyZW50SW5kZXggPSB0aGlzLnN0YXRlLmN1cnJlbnRJbmRleDsgLy9lc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdERpdn0+XG4gICAgICAgIHtkYXRhLm1hcCgoZG90LCBkb3RJbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT17cy5kb3R9XG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICBtYXJnaW5MZWZ0OiBkb3RJbmRleCAhPT0gMCA/ICcyMHB4JyA6ICcwJyxcbiAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOlxuICAgICAgICAgICAgICAgIGN1cnJlbnRJbmRleCA9PT0gZG90SW5kZXggPyAnI2VjNGM2ZicgOiAnI2Q4ZDhkOCcsXG4gICAgICAgICAgICAgIGN1cnNvcjogY3VycmVudEluZGV4ID09PSBkb3RJbmRleCA/ICdkZWZhdWx0JyA6ICdwb2ludGVyJyxcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgIGlzQ2xpY2tlZCA9IHRydWU7XG4gICAgICAgICAgICAgIHRoaXMuaGFuZGxlRG90Q2xpY2soZG90SW5kZXgpO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgIC8+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICAvKipcbiAgICogQGRlc2NyaXB0aW9uIGhhbmRsZXMgaWYgZG90IGlzIGNsaWNrZWRcbiAgICogQHBhcmFtIGluZGV4IG51bWJlclxuICAgKiBAYXV0aG9yIFN1c2hydXRoXG4gICAqICovXG5cbiAgaGFuZGxlRG90Q2xpY2sgPSBpbmRleCA9PiB7XG4gICAgbGV0IGN1cnJlbnRJbmRleCA9IHRoaXMuc3RhdGUuY3VycmVudEluZGV4OyAvL2VzbGludC1kaXNhYmxlLWxpbmVcbiAgICBjdXJyZW50SW5kZXggPSBpbmRleDtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGN1cnJlbnRJbmRleCxcbiAgICAgIHRyYW5zbGF0ZVZhbHVlOiAtaW5kZXggKiB0aGlzLnNsaWRlV2lkdGgoKSxcbiAgICB9KTtcbiAgfTtcblxuICAvKipcbiAgICAgIEBkZXNjcmlwdGlvbiBhdXRvbWF0aWNhbGx5IHNsaWRlcyB0aGUgZGl2c1xuICAgICAgQGF1dGhvciBTdXNocnV0aCBCXG4gICAgKi9cbiAgYXV0b21hdGljRG90Q2xpY2sgPSAoKSA9PiB7XG4gICAgbGV0IGluZGV4ID0gdGhpcy5zdGF0ZS5jdXJyZW50SW5kZXg7XG4gICAgaWYgKGluZGV4ID09PSB0aGlzLnByb3BzLmRhdGEubGVuZ3RoIC0gMSkge1xuICAgICAgaW5kZXggPSAtMTtcbiAgICB9XG4gICAgdGhpcy5oYW5kbGVEb3RDbGljayhpbmRleCArIDEpO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB2aWV3ID0gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2xpZGVTaG93fT5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5sZWZ0QXJyb3d9XG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgICAgICAgIH19XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgaXNDbGlja2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuZ29Ub1ByZXZTbGlkZSgpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaWNvbnMvYXJyb3ctZm9yd2FyZC5wbmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2xpZGVyfT5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBjbGFzc05hbWU9e3Muc2xpZGVXcmFwcGVyfVxuICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgdHJhbnNmb3JtOiBgdHJhbnNsYXRlWCgke3RoaXMuc3RhdGUudHJhbnNsYXRlVmFsdWV9cHgpYCxcbiAgICAgICAgICAgICAgdHJhbnNpdGlvbjogJ3RyYW5zZm9ybSBlYXNlLW91dCAwLjQ1cycsXG4gICAgICAgICAgICB9fVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHt0aGlzLnByb3BzLmRhdGEubWFwKGVhY2hQZXJzb24gPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zbGlkZX0gc2xpZGVDb21wb25lbnRgfT5cbiAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGRpc3BsYXk6ICdmbGV4JywganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInIH19PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJvZmlsZUltYWdlfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2VhY2hQZXJzb24uaW1hZ2V9IGFsdD1cIlwiIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zbGlkZUNhcmR9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJvZmlsZU5hbWV9PntlYWNoUGVyc29uLm5hbWV9PC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcm9maWxlUm9sZX0+e2VhY2hQZXJzb24ucm9sZX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByb2ZpbGVDb250ZW50fT57ZWFjaFBlcnNvbi5jb250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkpfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIHt0aGlzLmRpc3BsYXlEb3RzKCl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgICAgICAgIH19XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgaXNDbGlja2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuZ29Ub05leHRTbGlkZSgpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaWNvbnMvYXJyb3ctZm9yd2FyZC5wbmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gICAgcmV0dXJuIHZpZXc7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShTbGlkZXNob3cpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9TbGlkZXNob3cuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9TbGlkZXNob3cuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1NsaWRlc2hvdy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBbUJBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBMUJBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBeENBO0FBMENBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBL0NBO0FBdURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEzREE7QUFvRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTtBQUNBO0FBM0ZBO0FBbUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQTFHQTtBQWdIQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFwSEE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQXNHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFyTEE7QUFDQTtBQURBO0FBRUE7QUFEQTtBQUNBO0FBcUxBOzs7Ozs7O0FDL0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9