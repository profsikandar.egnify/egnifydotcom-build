require("source-map-support").install();
exports.ids = ["CancellationAndReturn"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".CancellationAndReturn-header-2bs9o {\n  width: 100%;\n  height: 240px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n}\n\n.CancellationAndReturn-headerText-32yBV {\n  font-size: 48px;\n  line-height: 62px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.CancellationAndReturn-body-2xpfB {\n  padding: 40px 196px 30px 260px;\n}\n\n.CancellationAndReturn-definitionWrapper-2As2f {\n  margin-bottom: 24px;\n}\n\n.CancellationAndReturn-heading-3CYAo {\n  font-size: 20px;\n  line-height: 32px;\n  font-weight: bold;\n  display: block;\n}\n\n.CancellationAndReturn-text-bengf {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  margin-bottom: 8px;\n}\n\n.CancellationAndReturn-pointWrapper-14eoT {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 8px;\n}\n\n.CancellationAndReturn-dotWrapper-3Z1hF {\n  width: 8px;\n  margin-right: 24px;\n}\n\n.CancellationAndReturn-dot-26bTv {\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background-color: #f36;\n  margin-top: 11px;\n}\n\n.CancellationAndReturn-infoPoint-3Cu6J {\n  line-height: 32px;\n  margin-bottom: 0;\n}\n\n.CancellationAndReturn-subpointsWrapper-20a4a {\n  margin-left: 24px;\n}\n\n.CancellationAndReturn-subHeading-26VqC {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  font-weight: bold;\n}\n\n.CancellationAndReturn-subpoint-FzgLL {\n  line-height: 28px;\n  margin-bottom: 0;\n}\n\n@media only screen and (max-width: 990px) {\n  .CancellationAndReturn-header-2bs9o {\n    height: 104px;\n  }\n\n  .CancellationAndReturn-headerText-32yBV {\n    text-align: center;\n    font-size: 24px;\n    line-height: 32px;\n    padding: 16px;\n  }\n\n  .CancellationAndReturn-body-2xpfB {\n    padding: 24px 16px 40px 24px;\n  }\n\n  .CancellationAndReturn-definitionWrapper-2As2f {\n    margin-bottom: 20px;\n  }\n\n  .CancellationAndReturn-heading-3CYAo {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .CancellationAndReturn-text-bengf {\n    font-size: 14px;\n    line-height: 24px;\n    margin-bottom: 4px;\n  }\n\n  .CancellationAndReturn-dot-26bTv {\n    margin-top: 8px;\n  }\n\n  .CancellationAndReturn-infoPoint-3Cu6J {\n    margin-bottom: 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss"],"names":[],"mappings":"AAAA;EACE,YAAY;EACZ,cAAc;EACd,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;CAC7D;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,+BAA+B;CAChC;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,eAAe;CAChB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,mBAAmB;EACnB,uBAAuB;EACvB,iBAAiB;CAClB;;AAED;EACE,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE;IACE,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,cAAc;GACf;;EAED;IACE,6BAA6B;GAC9B;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,iBAAiB;GAClB;CACF","file":"CancellationAndReturn.scss","sourcesContent":[".header {\n  width: 100%;\n  height: 240px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n}\n\n.headerText {\n  font-size: 48px;\n  line-height: 62px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.body {\n  padding: 40px 196px 30px 260px;\n}\n\n.definitionWrapper {\n  margin-bottom: 24px;\n}\n\n.heading {\n  font-size: 20px;\n  line-height: 32px;\n  font-weight: bold;\n  display: block;\n}\n\n.text {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  margin-bottom: 8px;\n}\n\n.pointWrapper {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 8px;\n}\n\n.dotWrapper {\n  width: 8px;\n  margin-right: 24px;\n}\n\n.dot {\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background-color: #f36;\n  margin-top: 11px;\n}\n\n.infoPoint {\n  line-height: 32px;\n  margin-bottom: 0;\n}\n\n.subpointsWrapper {\n  margin-left: 24px;\n}\n\n.subHeading {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  font-weight: bold;\n}\n\n.subpoint {\n  line-height: 28px;\n  margin-bottom: 0;\n}\n\n@media only screen and (max-width: 990px) {\n  .header {\n    height: 104px;\n  }\n\n  .headerText {\n    text-align: center;\n    font-size: 24px;\n    line-height: 32px;\n    padding: 16px;\n  }\n\n  .body {\n    padding: 24px 16px 40px 24px;\n  }\n\n  .definitionWrapper {\n    margin-bottom: 20px;\n  }\n\n  .heading {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .text {\n    font-size: 14px;\n    line-height: 24px;\n    margin-bottom: 4px;\n  }\n\n  .dot {\n    margin-top: 8px;\n  }\n\n  .infoPoint {\n    margin-bottom: 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"header": "CancellationAndReturn-header-2bs9o",
	"headerText": "CancellationAndReturn-headerText-32yBV",
	"body": "CancellationAndReturn-body-2xpfB",
	"definitionWrapper": "CancellationAndReturn-definitionWrapper-2As2f",
	"heading": "CancellationAndReturn-heading-3CYAo",
	"text": "CancellationAndReturn-text-bengf",
	"pointWrapper": "CancellationAndReturn-pointWrapper-14eoT",
	"dotWrapper": "CancellationAndReturn-dotWrapper-3Z1hF",
	"dot": "CancellationAndReturn-dot-26bTv",
	"infoPoint": "CancellationAndReturn-infoPoint-3Cu6J",
	"subpointsWrapper": "CancellationAndReturn-subpointsWrapper-20a4a",
	"subHeading": "CancellationAndReturn-subHeading-26VqC",
	"subpoint": "CancellationAndReturn-subpoint-FzgLL"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/constants.js");
/* harmony import */ var _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss");
/* harmony import */ var _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class CancellationAndReturn extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "displayHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headerText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9
      },
      __self: this
    }, "Cancellation and Return Policy")));

    _defineProperty(this, "displayDefinition", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.definitionWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, "Definition:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "'Return' is defined as the action of giving back the Product purchased by the Buyer to the Seller on the Website."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, "'Replacement' is the action or process of replacing something in place of another"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "A Buyer can request for return/replacement of the purchased Product only if:"), _constants__WEBPACK_IMPORTED_MODULE_2__["definitionPoints"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text} ${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoPoint}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, point)))));

    _defineProperty(this, "displayNotedPoints", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, "Points to be noted:"), _constants__WEBPACK_IMPORTED_MODULE_2__["notedPoints"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text} ${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoPoint}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, point.point)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subpointsWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, point.subHeading ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, point.subHeading) : null, point.subpoints ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, point.subpoints.map(subpoint => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text} ${_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subpoint}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, subpoint)))) : null)))));

    _defineProperty(this, "displayBody", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.body,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, this.displayDefinition(), this.displayNotedPoints()));
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, this.displayHeader(), this.displayBody());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(CancellationAndReturn));

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "definitionPoints", function() { return definitionPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notedPoints", function() { return notedPoints; });
const definitionPoints = [`The Product is unopened and unused`, `The Product is defective/damaged`, `Product(s) was/were missing`, `Wrong Product was sent by the Seller`, `Unsatisfactory Products - Inauthentic/Low Quality/Expired`];
const notedPoints = [{
  point: 'Seller can always accept the return/replacement irrespective of the policy.'
}, {
  point: `Some Products sold on the Website cannot be returned/replaced. We encourage the Buyer to review the listing before making the purchase decision. In case the Buyer orders a wrong item or orders an item that is not eligible for return/replacement, the Buyer shall not be entitled to any return/replacement/refund.`
}, {
  point: `Buyer needs to return/request replacement of any purchased Product within, the later of, 3 days of the date of receipt of the Product or the return period identified on the Website for each Product listed therein.`,
  subHeading: `For Returns:`,
  subpoints: [`Except for Cash On Delivery transaction, refund, if any, shall be made at the same Issuing Bank from where Transaction Price was received.`, `For Cash On Delivery transactions, refunds, if any, will be made through payment facility using NEFT / RTGS or any other online banking / electronic funds transfer system approved by Reserve Bank India (RBI), 10 business days after the necessary banking/account details are provided by the Buyer).`, `Refund shall be made in Indian Rupees only and shall be equivalent to the Transaction Price received in Indian Rupees subject to any applicable adjustments.`, `For electronic payments, refunds shall be made through payment facility using NEFT /RTGS or any other online banking / electronic funds transfer system approved by Reserve Bank India (RBI).`]
}, {
  point: `Refund/replacement shall be conditional and shall be with recourse available to Egnify Consultants in case of any misuse by Buyer.`
}, {
  point: `Refund/replacement shall be subject to Buyer complying with Egnify Consultants Policies.`
}, {
  point: `Refund/replacement shall be subject to Buyer complying with Egnify Consultants Policies.`,
  subpoints: [`Buyer is asked for "Reason for Return/Replacement" - the Product is unopened and unused; the Product is defective/damaged; Product(s) was/were missing; wrong Product was sent by the Seller.`, `An intimation shall be provided to the Seller seeking either "approval" or "rejection" of the request.`, `In case the Seller accepts the return/replacement request, Buyer shall be required to return the product to the Seller and only after return of the Product in conformance with the conditions listed herein below, Seller shall be obliged to issue a refund to the Buyer or send a replacement Product to the Buyer, as applicable.`, `Incase Seller rejects the return/replacement request, Buyer can choose to raise a dispute by writing to kiran@egnify.com giving details of the order and the return request. Egnify  shall endeavour to respond to such emails within 48 hours of receipt.`, `In case the Seller doesn't pick up the item from the Buyer within three (3) days from the date of the return/replacement request, Buyer can choose to raise a dispute by writing to kiran@egnify.com giving details of the order and the request. Egnify shall endeavour to respond to such emails within 48 hours of receipt.`, `For returns/replacement request being made by Buyer to the Seller of the product, the returned Product(s) should be unopened and "UNUSED" and returned with original packaging, otherwise the Buyer shall not be entitled to any refund of money or replacement of the Product (as applicable)`]
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _CancellationAndReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/index.js";




async function action() {
  return {
    title: 'GetRanks by Egnify: Assessment & Analytics Platform',
    chunks: ['CancellationAndReturn'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CancellationAndReturn__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0NhbmNlbGxhdGlvbkFuZFJldHVybi5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2NhbmNlbGxhdGlvbkFuZFJldHVybi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYW5jZWxsYXRpb25BbmRSZXR1cm4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2NhbmNlbGxhdGlvbkFuZFJldHVybi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzcz8xYTAyIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2NhbmNlbGxhdGlvbkFuZFJldHVybi9jb25zdGFudHMuanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRlci0yYnM5byB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMjQwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCBib3R0b20sIGxlZnQgdG9wLCBmcm9tKCNlYTRjNzApLCB0bygjYjI0NTdjKSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1oZWFkZXJUZXh0LTMyeUJWIHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiA2MnB4O1xcbiAgY29sb3I6ICNmZmY7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWJvZHktMnhwZkIge1xcbiAgcGFkZGluZzogNDBweCAxOTZweCAzMHB4IDI2MHB4O1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRlZmluaXRpb25XcmFwcGVyLTJBczJmIHtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGluZy0zQ1lBbyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tdGV4dC1iZW5nZiB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXBvaW50V3JhcHBlci0xNGVvVCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tZG90V3JhcHBlci0zWjFoRiB7XFxuICB3aWR0aDogOHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRvdC0yNmJUdiB7XFxuICB3aWR0aDogOHB4O1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgbWFyZ2luLXRvcDogMTFweDtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1pbmZvUG9pbnQtM0N1Nkoge1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAwO1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXN1YnBvaW50c1dyYXBwZXItMjBhNGEge1xcbiAgbWFyZ2luLWxlZnQ6IDI0cHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tc3ViSGVhZGluZy0yNlZxQyB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tc3VicG9pbnQtRnpnTEwge1xcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRlci0yYnM5byB7XFxuICAgIGhlaWdodDogMTA0cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRlclRleHQtMzJ5QlYge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgIHBhZGRpbmc6IDE2cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWJvZHktMnhwZkIge1xcbiAgICBwYWRkaW5nOiAyNHB4IDE2cHggNDBweCAyNHB4O1xcbiAgfVxcblxcbiAgLkNhbmNlbGxhdGlvbkFuZFJldHVybi1kZWZpbml0aW9uV3JhcHBlci0yQXMyZiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRpbmctM0NZQW8ge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLkNhbmNlbGxhdGlvbkFuZFJldHVybi10ZXh0LWJlbmdmIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNHB4O1xcbiAgfVxcblxcbiAgLkNhbmNlbGxhdGlvbkFuZFJldHVybi1kb3QtMjZiVHYge1xcbiAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWluZm9Qb2ludC0zQ3U2SiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuL0NhbmNlbGxhdGlvbkFuZFJldHVybi5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsWUFBWTtFQUNaLGNBQWM7RUFDZCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4Qiw4RkFBOEY7RUFDOUYsb0VBQW9FO0VBQ3BFLCtEQUErRDtFQUMvRCw0REFBNEQ7Q0FDN0Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSwrQkFBK0I7Q0FDaEM7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGtCQUFrQjtFQUNsQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRTtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGNBQWM7R0FDZjs7RUFFRDtJQUNFLDZCQUE2QjtHQUM5Qjs7RUFFRDtJQUNFLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGlCQUFpQjtHQUNsQjtDQUNGXCIsXCJmaWxlXCI6XCJDYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIuaGVhZGVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAyNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IGJvdHRvbSwgbGVmdCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNlYTRjNzAsICNiMjQ1N2MpO1xcbn1cXG5cXG4uaGVhZGVyVGV4dCB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjJweDtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLmJvZHkge1xcbiAgcGFkZGluZzogNDBweCAxOTZweCAzMHB4IDI2MHB4O1xcbn1cXG5cXG4uZGVmaW5pdGlvbldyYXBwZXIge1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLmhlYWRpbmcge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG5cXG4udGV4dCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4ucG9pbnRXcmFwcGVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLmRvdFdyYXBwZXIge1xcbiAgd2lkdGg6IDhweDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuXFxuLmRvdCB7XFxuICB3aWR0aDogOHB4O1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgbWFyZ2luLXRvcDogMTFweDtcXG59XFxuXFxuLmluZm9Qb2ludCB7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxufVxcblxcbi5zdWJwb2ludHNXcmFwcGVyIHtcXG4gIG1hcmdpbi1sZWZ0OiAyNHB4O1xcbn1cXG5cXG4uc3ViSGVhZGluZyB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5zdWJwb2ludCB7XFxuICBsaW5lLWhlaWdodDogMjhweDtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5oZWFkZXIge1xcbiAgICBoZWlnaHQ6IDEwNHB4O1xcbiAgfVxcblxcbiAgLmhlYWRlclRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgIHBhZGRpbmc6IDE2cHg7XFxuICB9XFxuXFxuICAuYm9keSB7XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweCA0MHB4IDI0cHg7XFxuICB9XFxuXFxuICAuZGVmaW5pdGlvbldyYXBwZXIge1xcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLmhlYWRpbmcge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLnRleHQge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XFxuICB9XFxuXFxuICAuZG90IHtcXG4gICAgbWFyZ2luLXRvcDogOHB4O1xcbiAgfVxcblxcbiAgLmluZm9Qb2ludCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJoZWFkZXJcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGVyLTJiczlvXCIsXG5cdFwiaGVhZGVyVGV4dFwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1oZWFkZXJUZXh0LTMyeUJWXCIsXG5cdFwiYm9keVwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1ib2R5LTJ4cGZCXCIsXG5cdFwiZGVmaW5pdGlvbldyYXBwZXJcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tZGVmaW5pdGlvbldyYXBwZXItMkFzMmZcIixcblx0XCJoZWFkaW5nXCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRpbmctM0NZQW9cIixcblx0XCJ0ZXh0XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXRleHQtYmVuZ2ZcIixcblx0XCJwb2ludFdyYXBwZXJcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tcG9pbnRXcmFwcGVyLTE0ZW9UXCIsXG5cdFwiZG90V3JhcHBlclwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1kb3RXcmFwcGVyLTNaMWhGXCIsXG5cdFwiZG90XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRvdC0yNmJUdlwiLFxuXHRcImluZm9Qb2ludFwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1pbmZvUG9pbnQtM0N1NkpcIixcblx0XCJzdWJwb2ludHNXcmFwcGVyXCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXN1YnBvaW50c1dyYXBwZXItMjBhNGFcIixcblx0XCJzdWJIZWFkaW5nXCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXN1YkhlYWRpbmctMjZWcUNcIixcblx0XCJzdWJwb2ludFwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1zdWJwb2ludC1GemdMTFwiXG59OyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgeyBkZWZpbml0aW9uUG9pbnRzLCBub3RlZFBvaW50cyB9IGZyb20gJy4vY29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLnNjc3MnO1xuXG5jbGFzcyBDYW5jZWxsYXRpb25BbmRSZXR1cm4gZXh0ZW5kcyBDb21wb25lbnQge1xuICBkaXNwbGF5SGVhZGVyID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlcn0+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3MuaGVhZGVyVGV4dH0+Q2FuY2VsbGF0aW9uIGFuZCBSZXR1cm4gUG9saWN5PC9zcGFuPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlEZWZpbml0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRlZmluaXRpb25XcmFwcGVyfT5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5oZWFkaW5nfT5EZWZpbml0aW9uOjwvc3Bhbj5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0fT5cbiAgICAgICAgJmFwb3M7UmV0dXJuJmFwb3M7IGlzIGRlZmluZWQgYXMgdGhlIGFjdGlvbiBvZiBnaXZpbmcgYmFjayB0aGUgUHJvZHVjdFxuICAgICAgICBwdXJjaGFzZWQgYnkgdGhlIEJ1eWVyIHRvIHRoZSBTZWxsZXIgb24gdGhlIFdlYnNpdGUuXG4gICAgICA8L3NwYW4+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dH0+XG4gICAgICAgICZhcG9zO1JlcGxhY2VtZW50JmFwb3M7IGlzIHRoZSBhY3Rpb24gb3IgcHJvY2VzcyBvZiByZXBsYWNpbmcgc29tZXRoaW5nXG4gICAgICAgIGluIHBsYWNlIG9mIGFub3RoZXJcbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0fT5cbiAgICAgICAgQSBCdXllciBjYW4gcmVxdWVzdCBmb3IgcmV0dXJuL3JlcGxhY2VtZW50IG9mIHRoZSBwdXJjaGFzZWQgUHJvZHVjdCBvbmx5XG4gICAgICAgIGlmOlxuICAgICAgPC9zcGFuPlxuICAgICAge2RlZmluaXRpb25Qb2ludHMubWFwKHBvaW50ID0+IChcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRXcmFwcGVyfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MudGV4dH0gJHtzLmluZm9Qb2ludH1gfT57cG9pbnR9PC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICkpfVxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlOb3RlZFBvaW50cyA9ICgpID0+IChcbiAgICA8ZGl2PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmhlYWRpbmd9PlBvaW50cyB0byBiZSBub3RlZDo8L3NwYW4+XG4gICAgICB7bm90ZWRQb2ludHMubWFwKHBvaW50ID0+IChcbiAgICAgICAgPD5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdH0gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLnRleHR9ICR7cy5pbmZvUG9pbnR9YH0+e3BvaW50LnBvaW50fTwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJwb2ludHNXcmFwcGVyfT5cbiAgICAgICAgICAgIHtwb2ludC5zdWJIZWFkaW5nID8gKFxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc3ViSGVhZGluZ30+e3BvaW50LnN1YkhlYWRpbmd9PC9zcGFuPlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICB7cG9pbnQuc3VicG9pbnRzID8gKFxuICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgIHtwb2ludC5zdWJwb2ludHMubWFwKHN1YnBvaW50ID0+IChcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50V3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdFdyYXBwZXJ9PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdH0gLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy50ZXh0fSAke3Muc3VicG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICAgICAge3N1YnBvaW50fVxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC8+XG4gICAgICApKX1cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Qm9keSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib2R5fT5cbiAgICAgIHt0aGlzLmRpc3BsYXlEZWZpbml0aW9uKCl9XG4gICAgICB7dGhpcy5kaXNwbGF5Tm90ZWRQb2ludHMoKX1cbiAgICA8L2Rpdj5cbiAgKTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIHt0aGlzLmRpc3BsYXlIZWFkZXIoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUJvZHkoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoQ2FuY2VsbGF0aW9uQW5kUmV0dXJuKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJleHBvcnQgY29uc3QgZGVmaW5pdGlvblBvaW50cyA9IFtcbiAgYFRoZSBQcm9kdWN0IGlzIHVub3BlbmVkIGFuZCB1bnVzZWRgLFxuICBgVGhlIFByb2R1Y3QgaXMgZGVmZWN0aXZlL2RhbWFnZWRgLFxuICBgUHJvZHVjdChzKSB3YXMvd2VyZSBtaXNzaW5nYCxcbiAgYFdyb25nIFByb2R1Y3Qgd2FzIHNlbnQgYnkgdGhlIFNlbGxlcmAsXG4gIGBVbnNhdGlzZmFjdG9yeSBQcm9kdWN0cyAtIEluYXV0aGVudGljL0xvdyBRdWFsaXR5L0V4cGlyZWRgLFxuXTtcblxuZXhwb3J0IGNvbnN0IG5vdGVkUG9pbnRzID0gW1xuICB7XG4gICAgcG9pbnQ6XG4gICAgICAnU2VsbGVyIGNhbiBhbHdheXMgYWNjZXB0IHRoZSByZXR1cm4vcmVwbGFjZW1lbnQgaXJyZXNwZWN0aXZlIG9mIHRoZSBwb2xpY3kuJyxcbiAgfSxcbiAge1xuICAgIHBvaW50OiBgU29tZSBQcm9kdWN0cyBzb2xkIG9uIHRoZSBXZWJzaXRlIGNhbm5vdCBiZSByZXR1cm5lZC9yZXBsYWNlZC4gV2UgZW5jb3VyYWdlIHRoZSBCdXllciB0byByZXZpZXcgdGhlIGxpc3RpbmcgYmVmb3JlIG1ha2luZyB0aGUgcHVyY2hhc2UgZGVjaXNpb24uIEluIGNhc2UgdGhlIEJ1eWVyIG9yZGVycyBhIHdyb25nIGl0ZW0gb3Igb3JkZXJzIGFuIGl0ZW0gdGhhdCBpcyBub3QgZWxpZ2libGUgZm9yIHJldHVybi9yZXBsYWNlbWVudCwgdGhlIEJ1eWVyIHNoYWxsIG5vdCBiZSBlbnRpdGxlZCB0byBhbnkgcmV0dXJuL3JlcGxhY2VtZW50L3JlZnVuZC5gLFxuICB9LFxuICB7XG4gICAgcG9pbnQ6IGBCdXllciBuZWVkcyB0byByZXR1cm4vcmVxdWVzdCByZXBsYWNlbWVudCBvZiBhbnkgcHVyY2hhc2VkIFByb2R1Y3Qgd2l0aGluLCB0aGUgbGF0ZXIgb2YsIDMgZGF5cyBvZiB0aGUgZGF0ZSBvZiByZWNlaXB0IG9mIHRoZSBQcm9kdWN0IG9yIHRoZSByZXR1cm4gcGVyaW9kIGlkZW50aWZpZWQgb24gdGhlIFdlYnNpdGUgZm9yIGVhY2ggUHJvZHVjdCBsaXN0ZWQgdGhlcmVpbi5gLFxuICAgIHN1YkhlYWRpbmc6IGBGb3IgUmV0dXJuczpgLFxuICAgIHN1YnBvaW50czogW1xuICAgICAgYEV4Y2VwdCBmb3IgQ2FzaCBPbiBEZWxpdmVyeSB0cmFuc2FjdGlvbiwgcmVmdW5kLCBpZiBhbnksIHNoYWxsIGJlIG1hZGUgYXQgdGhlIHNhbWUgSXNzdWluZyBCYW5rIGZyb20gd2hlcmUgVHJhbnNhY3Rpb24gUHJpY2Ugd2FzIHJlY2VpdmVkLmAsXG4gICAgICBgRm9yIENhc2ggT24gRGVsaXZlcnkgdHJhbnNhY3Rpb25zLCByZWZ1bmRzLCBpZiBhbnksIHdpbGwgYmUgbWFkZSB0aHJvdWdoIHBheW1lbnQgZmFjaWxpdHkgdXNpbmcgTkVGVCAvIFJUR1Mgb3IgYW55IG90aGVyIG9ubGluZSBiYW5raW5nIC8gZWxlY3Ryb25pYyBmdW5kcyB0cmFuc2ZlciBzeXN0ZW0gYXBwcm92ZWQgYnkgUmVzZXJ2ZSBCYW5rIEluZGlhIChSQkkpLCAxMCBidXNpbmVzcyBkYXlzIGFmdGVyIHRoZSBuZWNlc3NhcnkgYmFua2luZy9hY2NvdW50IGRldGFpbHMgYXJlIHByb3ZpZGVkIGJ5IHRoZSBCdXllcikuYCxcbiAgICAgIGBSZWZ1bmQgc2hhbGwgYmUgbWFkZSBpbiBJbmRpYW4gUnVwZWVzIG9ubHkgYW5kIHNoYWxsIGJlIGVxdWl2YWxlbnQgdG8gdGhlIFRyYW5zYWN0aW9uIFByaWNlIHJlY2VpdmVkIGluIEluZGlhbiBSdXBlZXMgc3ViamVjdCB0byBhbnkgYXBwbGljYWJsZSBhZGp1c3RtZW50cy5gLFxuICAgICAgYEZvciBlbGVjdHJvbmljIHBheW1lbnRzLCByZWZ1bmRzIHNoYWxsIGJlIG1hZGUgdGhyb3VnaCBwYXltZW50IGZhY2lsaXR5IHVzaW5nIE5FRlQgL1JUR1Mgb3IgYW55IG90aGVyIG9ubGluZSBiYW5raW5nIC8gZWxlY3Ryb25pYyBmdW5kcyB0cmFuc2ZlciBzeXN0ZW0gYXBwcm92ZWQgYnkgUmVzZXJ2ZSBCYW5rIEluZGlhIChSQkkpLmAsXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHBvaW50OiBgUmVmdW5kL3JlcGxhY2VtZW50IHNoYWxsIGJlIGNvbmRpdGlvbmFsIGFuZCBzaGFsbCBiZSB3aXRoIHJlY291cnNlIGF2YWlsYWJsZSB0byBFZ25pZnkgQ29uc3VsdGFudHMgaW4gY2FzZSBvZiBhbnkgbWlzdXNlIGJ5IEJ1eWVyLmAsXG4gIH0sXG4gIHtcbiAgICBwb2ludDogYFJlZnVuZC9yZXBsYWNlbWVudCBzaGFsbCBiZSBzdWJqZWN0IHRvIEJ1eWVyIGNvbXBseWluZyB3aXRoIEVnbmlmeSBDb25zdWx0YW50cyBQb2xpY2llcy5gLFxuICB9LFxuICB7XG4gICAgcG9pbnQ6IGBSZWZ1bmQvcmVwbGFjZW1lbnQgc2hhbGwgYmUgc3ViamVjdCB0byBCdXllciBjb21wbHlpbmcgd2l0aCBFZ25pZnkgQ29uc3VsdGFudHMgUG9saWNpZXMuYCxcbiAgICBzdWJwb2ludHM6IFtcbiAgICAgIGBCdXllciBpcyBhc2tlZCBmb3IgXCJSZWFzb24gZm9yIFJldHVybi9SZXBsYWNlbWVudFwiIC0gdGhlIFByb2R1Y3QgaXMgdW5vcGVuZWQgYW5kIHVudXNlZDsgdGhlIFByb2R1Y3QgaXMgZGVmZWN0aXZlL2RhbWFnZWQ7IFByb2R1Y3Qocykgd2FzL3dlcmUgbWlzc2luZzsgd3JvbmcgUHJvZHVjdCB3YXMgc2VudCBieSB0aGUgU2VsbGVyLmAsXG4gICAgICBgQW4gaW50aW1hdGlvbiBzaGFsbCBiZSBwcm92aWRlZCB0byB0aGUgU2VsbGVyIHNlZWtpbmcgZWl0aGVyIFwiYXBwcm92YWxcIiBvciBcInJlamVjdGlvblwiIG9mIHRoZSByZXF1ZXN0LmAsXG4gICAgICBgSW4gY2FzZSB0aGUgU2VsbGVyIGFjY2VwdHMgdGhlIHJldHVybi9yZXBsYWNlbWVudCByZXF1ZXN0LCBCdXllciBzaGFsbCBiZSByZXF1aXJlZCB0byByZXR1cm4gdGhlIHByb2R1Y3QgdG8gdGhlIFNlbGxlciBhbmQgb25seSBhZnRlciByZXR1cm4gb2YgdGhlIFByb2R1Y3QgaW4gY29uZm9ybWFuY2Ugd2l0aCB0aGUgY29uZGl0aW9ucyBsaXN0ZWQgaGVyZWluIGJlbG93LCBTZWxsZXIgc2hhbGwgYmUgb2JsaWdlZCB0byBpc3N1ZSBhIHJlZnVuZCB0byB0aGUgQnV5ZXIgb3Igc2VuZCBhIHJlcGxhY2VtZW50IFByb2R1Y3QgdG8gdGhlIEJ1eWVyLCBhcyBhcHBsaWNhYmxlLmAsXG4gICAgICBgSW5jYXNlIFNlbGxlciByZWplY3RzIHRoZSByZXR1cm4vcmVwbGFjZW1lbnQgcmVxdWVzdCwgQnV5ZXIgY2FuIGNob29zZSB0byByYWlzZSBhIGRpc3B1dGUgYnkgd3JpdGluZyB0byBraXJhbkBlZ25pZnkuY29tIGdpdmluZyBkZXRhaWxzIG9mIHRoZSBvcmRlciBhbmQgdGhlIHJldHVybiByZXF1ZXN0LiBFZ25pZnkgIHNoYWxsIGVuZGVhdm91ciB0byByZXNwb25kIHRvIHN1Y2ggZW1haWxzIHdpdGhpbiA0OCBob3VycyBvZiByZWNlaXB0LmAsXG4gICAgICBgSW4gY2FzZSB0aGUgU2VsbGVyIGRvZXNuJ3QgcGljayB1cCB0aGUgaXRlbSBmcm9tIHRoZSBCdXllciB3aXRoaW4gdGhyZWUgKDMpIGRheXMgZnJvbSB0aGUgZGF0ZSBvZiB0aGUgcmV0dXJuL3JlcGxhY2VtZW50IHJlcXVlc3QsIEJ1eWVyIGNhbiBjaG9vc2UgdG8gcmFpc2UgYSBkaXNwdXRlIGJ5IHdyaXRpbmcgdG8ga2lyYW5AZWduaWZ5LmNvbSBnaXZpbmcgZGV0YWlscyBvZiB0aGUgb3JkZXIgYW5kIHRoZSByZXF1ZXN0LiBFZ25pZnkgc2hhbGwgZW5kZWF2b3VyIHRvIHJlc3BvbmQgdG8gc3VjaCBlbWFpbHMgd2l0aGluIDQ4IGhvdXJzIG9mIHJlY2VpcHQuYCxcbiAgICAgIGBGb3IgcmV0dXJucy9yZXBsYWNlbWVudCByZXF1ZXN0IGJlaW5nIG1hZGUgYnkgQnV5ZXIgdG8gdGhlIFNlbGxlciBvZiB0aGUgcHJvZHVjdCwgdGhlIHJldHVybmVkIFByb2R1Y3Qocykgc2hvdWxkIGJlIHVub3BlbmVkIGFuZCBcIlVOVVNFRFwiIGFuZCByZXR1cm5lZCB3aXRoIG9yaWdpbmFsIHBhY2thZ2luZywgb3RoZXJ3aXNlIHRoZSBCdXllciBzaGFsbCBub3QgYmUgZW50aXRsZWQgdG8gYW55IHJlZnVuZCBvZiBtb25leSBvciByZXBsYWNlbWVudCBvZiB0aGUgUHJvZHVjdCAoYXMgYXBwbGljYWJsZSlgLFxuICAgIF0sXG4gIH0sXG5dO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnY29tcG9uZW50cy9MYXlvdXQnO1xuaW1wb3J0IENhbmNlbGxhdGlvbkFuZFJldHVybiBmcm9tICcuL0NhbmNlbGxhdGlvbkFuZFJldHVybic7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0dldFJhbmtzIGJ5IEVnbmlmeTogQXNzZXNzbWVudCAmIEFuYWx5dGljcyBQbGF0Zm9ybScsXG4gICAgY2h1bmtzOiBbJ0NhbmNlbGxhdGlvbkFuZFJldHVybiddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPENhbmNlbGxhdGlvbkFuZFJldHVybiAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBNUJBO0FBa0NBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXhEQTtBQXFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJFQTtBQUNBO0FBMEVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBbkZBO0FBQ0E7QUFtRkE7Ozs7Ozs7QUN6RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7OztBQzdCQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFIQTtBQVdBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFDQTtBQUZBOzs7Ozs7Ozs7Ozs7OztBQ2hDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVNBO0FBQ0E7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9