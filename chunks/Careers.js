require("source-map-support").install();
exports.ids = ["Careers"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/careers/Careers.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Careers-root-1e3hD {\n  min-height: 100vh;\n  width: 100%;\n}\n\n.Careers-headerContainer-2GxEH {\n  width: 100%;\n  height: 320px;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Careers-heading-1Glw5 {\n  font-size: 48px;\n  line-height: 64px;\n  margin-bottom: 12px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.Careers-headingText-30NHl {\n  font-size: 24px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.Careers-bodySection-2QX-9 {\n  padding: 40px 64px;\n}\n\n.Careers-positionHeading-CRoF5 {\n  font-weight: 600;\n  font-size: 32px;\n  line-height: 48px;\n}\n\n.Careers-joinCommunity-1Qq-Y {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Careers-joinFbText-1bTCM {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.Careers-joinFbLink-10gsG {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.Careers-scrollTop-3LDI4 {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Careers-scrollTop-3LDI4 img {\n    width: 100%;\n    height: 100%;\n  }\n\n@media only screen and (max-width: 990px) {\n  .Careers-scrollTop-3LDI4 {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Careers-headerContainer-2GxEH {\n    height: 168px;\n  }\n\n  .Careers-heading-1Glw5 {\n    font-size: 24px;\n    line-height: 32px;\n  }\n\n  .Careers-headingText-30NHl {\n    font-size: 14px;\n    line-height: 24px;\n    max-width: 300px;\n    text-align: center;\n  }\n\n  .Careers-bodySection-2QX-9 {\n    padding: 20px 16px;\n  }\n\n  .Careers-positionHeading-CRoF5 {\n    font-size: 20px;\n    line-height: 32px;\n  }\n\n  .Careers-joinCommunity-1Qq-Y {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .Careers-joinFbText-1bTCM {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .Careers-joinFbLink-10gsG {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/careers/Careers.scss"],"names":[],"mappings":"AAAA;EACE,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,cAAc;EACd,4FAA4F;EAC5F,0EAA0E;EAC1E,qEAAqE;EACrE,sEAAsE;EACtE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,oBAAoB;EACpB,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,mBAAmB;CACpB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,cAAc;EACd,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;CACb;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;GACf;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,iBAAiB;IACjB,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;GACnB;CACF","file":"Careers.scss","sourcesContent":[".root {\n  min-height: 100vh;\n  width: 100%;\n}\n\n.headerContainer {\n  width: 100%;\n  height: 320px;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.heading {\n  font-size: 48px;\n  line-height: 64px;\n  margin-bottom: 12px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.headingText {\n  font-size: 24px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.bodySection {\n  padding: 40px 64px;\n}\n\n.positionHeading {\n  font-weight: 600;\n  font-size: 32px;\n  line-height: 48px;\n}\n\n.joinCommunity {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.joinFbText {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.joinFbLink {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n@media only screen and (max-width: 990px) {\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .headerContainer {\n    height: 168px;\n  }\n\n  .heading {\n    font-size: 24px;\n    line-height: 32px;\n  }\n\n  .headingText {\n    font-size: 14px;\n    line-height: 24px;\n    max-width: 300px;\n    text-align: center;\n  }\n\n  .bodySection {\n    padding: 20px 16px;\n  }\n\n  .positionHeading {\n    font-size: 20px;\n    line-height: 32px;\n  }\n\n  .joinCommunity {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .joinFbText {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .joinFbLink {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Careers-root-1e3hD",
	"headerContainer": "Careers-headerContainer-2GxEH",
	"heading": "Careers-heading-1Glw5",
	"headingText": "Careers-headingText-30NHl",
	"bodySection": "Careers-bodySection-2QX-9",
	"positionHeading": "Careers-positionHeading-CRoF5",
	"joinCommunity": "Careers-joinCommunity-1Qq-Y",
	"joinFbText": "Careers-joinFbText-1bTCM",
	"joinFbLink": "Careers-joinFbLink-10gsG",
	"scrollTop": "Careers-scrollTop-3LDI4"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/careers/Careers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Careers_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/careers/Careers.scss");
/* harmony import */ var _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Careers_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/careers/Careers.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const CAREERS_HTML = `
  <script
    type='text/javascript'
    src='https://static.smartrecruiters.com/job-widget/1.4.2/script/smart_widget.js'
  >
  </script>
  <script type='text/javascript' class='job_widget'>
    widget({
       "company_code": "EgnifyTechnologies",
       "bg_color_widget": "#ffffff",
       "bg_color_headers": "#969696",
       "bg_color_links": "#fff",
       "txt_color_headers": "#292929",
       "txt_color_job": "#3d3d3d",
       "bg_color_even_row": "#fff",
       "bg_color_odd_row": "#fff",
       "auto_width": "auto",
       "auto_height": "auto",
       "number": "on",
       "job_title": "true",
       "location": "true",
       "filter_locations": "",
       "trid": "998bc6c9-cfbe-4db9-af4b-d7bb8407f264",
       "api_url": "https://www.smartrecruiters.com",
       "custom_css_url": "/css/smart_widget.css"
    });
  </script>
`;

class Careers extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }));
    });

    _defineProperty(this, "displayHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headerContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, "Join Egnify Cult"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headingText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96
      },
      __self: this
    }, "If you're exceedingly good and passionate at what you do, start here.")));

    _defineProperty(this, "displayPositions", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bodySection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.positionHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, "Open Positions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      id: "positions",
      dangerouslySetInnerHTML: {
        __html: CAREERS_HTML
      } //eslint-disable-line
      ,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }))));

    _defineProperty(this, "displayJoinFB", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.joinCommunity,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.joinFbText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, "Join our FaceBook Community"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.joinFbLink,
      href: "https://www.facebook.com/Egnify/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, "Join Now")));

    this.state = {};
  }

  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.pageview(window.location.href);
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.root,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, this.displayHeader(), this.displayPositions(), this.displayJoinFB(), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Careers_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Careers));

/***/ }),

/***/ "./src/routes/products/get-ranks/careers/Careers.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/careers/Careers.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/careers/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Careers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/careers/Careers.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/careers/index.js";




async function action() {
  return {
    title: 'Egnify makes teaching easy',
    chunks: ['Careers'],
    content: 'Leverage the technology that powers Egnify - one of the best online teaching  platforms. See how Egnify for developer supports best teacher student interaction experience online.',
    keywords: 'Egnify, Analytics, Indian Education, Education, Schools, Tests, JEE, NEET, JEET, PREP, QMS, Online Test, grading, class, ranks, students, demo, request, institutes, kiran babu, yerranagu',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Careers__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0NhcmVlcnMuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYXJlZXJzL0NhcmVlcnMuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYXJlZXJzL0NhcmVlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FyZWVycy9DYXJlZXJzLnNjc3M/NmM5NiIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYXJlZXJzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuQ2FyZWVycy1yb290LTFlM2hEIHtcXG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi5DYXJlZXJzLWhlYWRlckNvbnRhaW5lci0yR3hFSCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMzIwcHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCB0b3AsIHJpZ2h0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLkNhcmVlcnMtaGVhZGluZy0xR2x3NSB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5DYXJlZXJzLWhlYWRpbmdUZXh0LTMwTkhsIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5DYXJlZXJzLWJvZHlTZWN0aW9uLTJRWC05IHtcXG4gIHBhZGRpbmc6IDQwcHggNjRweDtcXG59XFxuXFxuLkNhcmVlcnMtcG9zaXRpb25IZWFkaW5nLUNSb0Y1IHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG59XFxuXFxuLkNhcmVlcnMtam9pbkNvbW11bml0eS0xUXEtWSB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTYwcHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCBib3R0b20sIGxlZnQgdG9wLCBmcm9tKCNlYTRjNzApLCB0bygjYjI0NTdjKSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLkNhcmVlcnMtam9pbkZiVGV4dC0xYlRDTSB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tcmlnaHQ6IDY0cHg7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLkNhcmVlcnMtam9pbkZiTGluay0xMGdzRyB7XFxuICBwYWRkaW5nOiAxNnB4IDQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLkNhcmVlcnMtc2Nyb2xsVG9wLTNMREk0IHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uQ2FyZWVycy1zY3JvbGxUb3AtM0xESTQgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuQ2FyZWVycy1zY3JvbGxUb3AtM0xESTQge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLkNhcmVlcnMtaGVhZGVyQ29udGFpbmVyLTJHeEVIIHtcXG4gICAgaGVpZ2h0OiAxNjhweDtcXG4gIH1cXG5cXG4gIC5DYXJlZXJzLWhlYWRpbmctMUdsdzUge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgfVxcblxcbiAgLkNhcmVlcnMtaGVhZGluZ1RleHQtMzBOSGwge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQ2FyZWVycy1ib2R5U2VjdGlvbi0yUVgtOSB7XFxuICAgIHBhZGRpbmc6IDIwcHggMTZweDtcXG4gIH1cXG5cXG4gIC5DYXJlZXJzLXBvc2l0aW9uSGVhZGluZy1DUm9GNSB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICB9XFxuXFxuICAuQ2FyZWVycy1qb2luQ29tbXVuaXR5LTFRcS1ZIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgaGVpZ2h0OiAxNDRweDtcXG4gIH1cXG5cXG4gIC5DYXJlZXJzLWpvaW5GYlRleHQtMWJUQ00ge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5DYXJlZXJzLWpvaW5GYkxpbmstMTBnc0cge1xcbiAgICBwYWRkaW5nOiAxMnB4IDMycHg7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FyZWVycy9DYXJlZXJzLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGNBQWM7RUFDZCw0RkFBNEY7RUFDNUYsMEVBQTBFO0VBQzFFLHFFQUFxRTtFQUNyRSxzRUFBc0U7RUFDdEUscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtDQUM3Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLFlBQVk7RUFDWixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsWUFBWTtFQUNaLGNBQWM7RUFDZCw4RkFBOEY7RUFDOUYsb0VBQW9FO0VBQ3BFLCtEQUErRDtFQUMvRCw0REFBNEQ7RUFDNUQscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsY0FBYztHQUNmOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25CO0NBQ0ZcIixcImZpbGVcIjpcIkNhcmVlcnMuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucm9vdCB7XFxuICBtaW4taGVpZ2h0OiAxMDB2aDtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uaGVhZGVyQ29udGFpbmVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAzMjBweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IHRvcCwgcmlnaHQgdG9wLCBmcm9tKCNlYTRjNzApLCB0bygjYjI0NTdjKSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KGxlZnQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4uaGVhZGluZyB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5oZWFkaW5nVGV4dCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uYm9keVNlY3Rpb24ge1xcbiAgcGFkZGluZzogNDBweCA2NHB4O1xcbn1cXG5cXG4ucG9zaXRpb25IZWFkaW5nIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG59XFxuXFxuLmpvaW5Db21tdW5pdHkge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDE2MHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgYm90dG9tLCBsZWZ0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5qb2luRmJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1yaWdodDogNjRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uam9pbkZiTGluayB7XFxuICBwYWRkaW5nOiAxNnB4IDQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLnNjcm9sbFRvcCB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLnNjcm9sbFRvcCBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIGhlaWdodDogMTY4cHg7XFxuICB9XFxuXFxuICAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICB9XFxuXFxuICAuaGVhZGluZ1RleHQge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuYm9keVNlY3Rpb24ge1xcbiAgICBwYWRkaW5nOiAyMHB4IDE2cHg7XFxuICB9XFxuXFxuICAucG9zaXRpb25IZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gIH1cXG5cXG4gIC5qb2luQ29tbXVuaXR5IHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgaGVpZ2h0OiAxNDRweDtcXG4gIH1cXG5cXG4gIC5qb2luRmJUZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuam9pbkZiTGluayB7XFxuICAgIHBhZGRpbmc6IDEycHggMzJweDtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInJvb3RcIjogXCJDYXJlZXJzLXJvb3QtMWUzaERcIixcblx0XCJoZWFkZXJDb250YWluZXJcIjogXCJDYXJlZXJzLWhlYWRlckNvbnRhaW5lci0yR3hFSFwiLFxuXHRcImhlYWRpbmdcIjogXCJDYXJlZXJzLWhlYWRpbmctMUdsdzVcIixcblx0XCJoZWFkaW5nVGV4dFwiOiBcIkNhcmVlcnMtaGVhZGluZ1RleHQtMzBOSGxcIixcblx0XCJib2R5U2VjdGlvblwiOiBcIkNhcmVlcnMtYm9keVNlY3Rpb24tMlFYLTlcIixcblx0XCJwb3NpdGlvbkhlYWRpbmdcIjogXCJDYXJlZXJzLXBvc2l0aW9uSGVhZGluZy1DUm9GNVwiLFxuXHRcImpvaW5Db21tdW5pdHlcIjogXCJDYXJlZXJzLWpvaW5Db21tdW5pdHktMVFxLVlcIixcblx0XCJqb2luRmJUZXh0XCI6IFwiQ2FyZWVycy1qb2luRmJUZXh0LTFiVENNXCIsXG5cdFwiam9pbkZiTGlua1wiOiBcIkNhcmVlcnMtam9pbkZiTGluay0xMGdzR1wiLFxuXHRcInNjcm9sbFRvcFwiOiBcIkNhcmVlcnMtc2Nyb2xsVG9wLTNMREk0XCJcbn07IiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBzIGZyb20gJy4vQ2FyZWVycy5zY3NzJztcblxuY29uc3QgQ0FSRUVSU19IVE1MID0gYFxuICA8c2NyaXB0XG4gICAgdHlwZT0ndGV4dC9qYXZhc2NyaXB0J1xuICAgIHNyYz0naHR0cHM6Ly9zdGF0aWMuc21hcnRyZWNydWl0ZXJzLmNvbS9qb2Itd2lkZ2V0LzEuNC4yL3NjcmlwdC9zbWFydF93aWRnZXQuanMnXG4gID5cbiAgPC9zY3JpcHQ+XG4gIDxzY3JpcHQgdHlwZT0ndGV4dC9qYXZhc2NyaXB0JyBjbGFzcz0nam9iX3dpZGdldCc+XG4gICAgd2lkZ2V0KHtcbiAgICAgICBcImNvbXBhbnlfY29kZVwiOiBcIkVnbmlmeVRlY2hub2xvZ2llc1wiLFxuICAgICAgIFwiYmdfY29sb3Jfd2lkZ2V0XCI6IFwiI2ZmZmZmZlwiLFxuICAgICAgIFwiYmdfY29sb3JfaGVhZGVyc1wiOiBcIiM5Njk2OTZcIixcbiAgICAgICBcImJnX2NvbG9yX2xpbmtzXCI6IFwiI2ZmZlwiLFxuICAgICAgIFwidHh0X2NvbG9yX2hlYWRlcnNcIjogXCIjMjkyOTI5XCIsXG4gICAgICAgXCJ0eHRfY29sb3Jfam9iXCI6IFwiIzNkM2QzZFwiLFxuICAgICAgIFwiYmdfY29sb3JfZXZlbl9yb3dcIjogXCIjZmZmXCIsXG4gICAgICAgXCJiZ19jb2xvcl9vZGRfcm93XCI6IFwiI2ZmZlwiLFxuICAgICAgIFwiYXV0b193aWR0aFwiOiBcImF1dG9cIixcbiAgICAgICBcImF1dG9faGVpZ2h0XCI6IFwiYXV0b1wiLFxuICAgICAgIFwibnVtYmVyXCI6IFwib25cIixcbiAgICAgICBcImpvYl90aXRsZVwiOiBcInRydWVcIixcbiAgICAgICBcImxvY2F0aW9uXCI6IFwidHJ1ZVwiLFxuICAgICAgIFwiZmlsdGVyX2xvY2F0aW9uc1wiOiBcIlwiLFxuICAgICAgIFwidHJpZFwiOiBcIjk5OGJjNmM5LWNmYmUtNGRiOS1hZjRiLWQ3YmI4NDA3ZjI2NFwiLFxuICAgICAgIFwiYXBpX3VybFwiOiBcImh0dHBzOi8vd3d3LnNtYXJ0cmVjcnVpdGVycy5jb21cIixcbiAgICAgICBcImN1c3RvbV9jc3NfdXJsXCI6IFwiL2Nzcy9zbWFydF93aWRnZXQuY3NzXCJcbiAgICB9KTtcbiAgPC9zY3JpcHQ+XG5gO1xuXG5jbGFzcyBDYXJlZXJzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgUmVhY3RHQS5pbml0aWFsaXplKHdpbmRvdy5BcHAuZ29vZ2xlVHJhY2tpbmdJZCwge1xuICAgICAgZGVidWc6IGZhbHNlLFxuICAgIH0pO1xuICAgIFJlYWN0R0EucGFnZXZpZXcod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cblxuICBoYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5zY3JvbGxZID4gNTAwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlU2Nyb2xsVG9wID0gKCkgPT4ge1xuICAgIHdpbmRvdy5zY3JvbGxUbyh7XG4gICAgICB0b3A6IDAsXG4gICAgICBiZWhhdmlvcjogJ3Ntb290aCcsXG4gICAgfSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICB9KTtcbiAgfTtcblxuICBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBzaG93U2Nyb2xsIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5zY3JvbGxUb3B9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheUhlYWRlciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJDb250YWluZXJ9PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmhlYWRpbmd9PkpvaW4gRWduaWZ5IEN1bHQ8L3NwYW4+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3MuaGVhZGluZ1RleHR9PlxuICAgICAgICBJZiB5b3UmYXBvcztyZSBleGNlZWRpbmdseSBnb29kIGFuZCBwYXNzaW9uYXRlIGF0IHdoYXQgeW91IGRvLCBzdGFydFxuICAgICAgICBoZXJlLlxuICAgICAgPC9zcGFuPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlQb3NpdGlvbnMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuYm9keVNlY3Rpb259PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnBvc2l0aW9uSGVhZGluZ30+T3BlbiBQb3NpdGlvbnM8L3NwYW4+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgaWQ9XCJwb3NpdGlvbnNcIlxuICAgICAgICAgIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogQ0FSRUVSU19IVE1MIH19IC8vZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgICAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUpvaW5GQiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5qb2luQ29tbXVuaXR5fT5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5qb2luRmJUZXh0fT5Kb2luIG91ciBGYWNlQm9vayBDb21tdW5pdHk8L3NwYW4+XG4gICAgICA8YSBjbGFzc05hbWU9e3Muam9pbkZiTGlua30gaHJlZj1cImh0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS9FZ25pZnkvXCI+XG4gICAgICAgIEpvaW4gTm93XG4gICAgICA8L2E+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yb290fT5cbiAgICAgICAge3RoaXMuZGlzcGxheUhlYWRlcigpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5UG9zaXRpb25zKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlKb2luRkIoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKENhcmVlcnMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DYXJlZXJzLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ2FyZWVycy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ2FyZWVycy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgQ2FyZWVycyBmcm9tICcuL0NhcmVlcnMnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnkgbWFrZXMgdGVhY2hpbmcgZWFzeScsXG4gICAgY2h1bmtzOiBbJ0NhcmVlcnMnXSxcbiAgICBjb250ZW50OlxuICAgICAgJ0xldmVyYWdlIHRoZSB0ZWNobm9sb2d5IHRoYXQgcG93ZXJzIEVnbmlmeSAtIG9uZSBvZiB0aGUgYmVzdCBvbmxpbmUgdGVhY2hpbmcgIHBsYXRmb3Jtcy4gU2VlIGhvdyBFZ25pZnkgZm9yIGRldmVsb3BlciBzdXBwb3J0cyBiZXN0IHRlYWNoZXIgc3R1ZGVudCBpbnRlcmFjdGlvbiBleHBlcmllbmNlIG9ubGluZS4nLFxuICAgIGtleXdvcmRzOlxuICAgICAgJ0VnbmlmeSwgQW5hbHl0aWNzLCBJbmRpYW4gRWR1Y2F0aW9uLCBFZHVjYXRpb24sIFNjaG9vbHMsIFRlc3RzLCBKRUUsIE5FRVQsIEpFRVQsIFBSRVAsIFFNUywgT25saW5lIFRlc3QsIGdyYWRpbmcsIGNsYXNzLCByYW5rcywgc3R1ZGVudHMsIGRlbW8sIHJlcXVlc3QsIGluc3RpdHV0ZXMsIGtpcmFuIGJhYnUsIHllcnJhbmFndScsXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8Q2FyZWVycyAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQW1CQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBN0JBO0FBK0JBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQXZDQTtBQXlDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQXhEQTtBQTBEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQTdEQTtBQW9FQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF4RUE7QUFnRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWpGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXVFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBbkdBO0FBQ0E7QUFtR0E7Ozs7Ozs7QUN0SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVEE7QUFhQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==