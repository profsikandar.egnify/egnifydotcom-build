require("source-map-support").install();
exports.ids = ["Culture"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/culture/Culture.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Culture-header_container-2aO9f {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  height: 80vh;\n  width: 100%;\n  overflow: hidden;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n}\n\n.Culture-scrollTop-1szFV {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Culture-scrollTop-1szFV img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Culture-header_contents-2Zv0J {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  padding: 10% 2% 10% 10%;\n}\n\n.Culture-header_contents-2Zv0J h2 {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: left;\n  color: #fff;\n}\n\n.Culture-header_contents-2Zv0J p {\n  font-size: 20px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.Culture-header_image-3ZvP9 {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 10% 10% 10% 0%;\n}\n\n.Culture-header_image-3ZvP9 img {\n  width: 442px;\n  height: 315px;\n}\n\n.Culture-rightSideImageConatainer-2qt-l {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #fff;\n}\n\n.Culture-content-QdxRQ {\n  width: 50%;\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-left: 10%;\n  padding-right: 8%;\n}\n\n.Culture-content-QdxRQ h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.Culture-content-QdxRQ p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n.Culture-image-325bz {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Culture-image-325bz img {\n  width: 60%;\n}\n\n.Culture-leftSideImageConatainer-k1SrS {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.Culture-leftSideImageContent-ZxM8e {\n  width: 50%;\n  padding-right: 10%;\n  padding-left: 8%;\n}\n\n.Culture-leftSideImageContent-ZxM8e h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.Culture-leftSideImageContent-ZxM8e p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n@media only screen and (max-width: 989px) {\n  .Culture-scrollTop-1szFV {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Culture-header_container-2aO9f {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 560px;\n    padding: 32px 16px;\n  }\n\n  .Culture-header_contents-2Zv0J {\n    width: 100%;\n    padding: 0;\n  }\n\n  .Culture-header_contents-2Zv0J h2 {\n    font-size: 32px;\n    line-height: 40px;\n    text-align: center;\n    margin: 0;\n    margin-bottom: 16px;\n    padding: 0 24px;\n  }\n\n  .Culture-header_contents-2Zv0J p {\n    font-size: 14px;\n    line-height: 26px;\n    margin: 0;\n    text-align: center;\n    letter-spacing: -0.42px;\n  }\n\n  .Culture-header_image-3ZvP9 {\n    width: 292px;\n    height: 208px;\n    margin-top: 32px;\n    padding: 0 19px;\n  }\n\n    .Culture-header_image-3ZvP9 img {\n      width: 100%;\n      height: 100%;\n      -o-object-fit: cover;\n         object-fit: cover;\n    }\n\n  .Culture-rightSideImageConatainer-2qt-l {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ {\n      width: 100%;\n      height: auto;\n      padding: 0;\n    }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ h2 {\n        margin: 8px 0;\n        text-align: center;\n        font-size: 24px;\n        line-height: normal;\n      }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ p {\n        margin: 0;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n      }\n\n    .Culture-rightSideImageConatainer-2qt-l .Culture-image-325bz {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-image-325bz img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .Culture-leftSideImageConatainer-k1SrS {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e {\n      width: 100%;\n      height: auto;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      padding: 0;\n    }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e h2 {\n        text-align: center;\n        font-size: 24px;\n        padding: 0 35px;\n        margin-top: 0;\n        line-height: normal;\n      }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e p {\n        font-size: 14px;\n        text-align: center;\n        margin: 0;\n        line-height: 24px;\n      }\n\n    .Culture-leftSideImageConatainer-k1SrS .Culture-image-325bz {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-image-325bz img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/Culture.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;EACb,YAAY;EACZ,iBAAiB;EACjB,4FAA4F;EAC5F,0EAA0E;EAC1E,qEAAqE;EACrE,sEAAsE;CACvE;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,wBAAwB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,wBAAwB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;CACd;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,gCAAgC;MAC5B,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;EACxB,0BAA0B;CAC3B;;AAED;EACE,WAAW;EACX,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;CACd;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,0BAA0B;QACtB,8BAA8B;IAClC,uBAAuB;QACnB,oBAAoB;IACxB,YAAY;IACZ,cAAc;IACd,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;IACnB,UAAU;IACV,oBAAoB;IACpB,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,UAAU;IACV,mBAAmB;IACnB,wBAAwB;GACzB;;EAED;IACE,aAAa;IACb,cAAc;IACd,iBAAiB;IACjB,gBAAgB;GACjB;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,qBAAqB;SAClB,kBAAkB;KACtB;;EAEH;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,aAAa;GACd;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,WAAW;KACZ;;MAEC;QACE,cAAc;QACd,mBAAmB;QACnB,gBAAgB;QAChB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;OACnB;;IAEH;MACE,WAAW;MACX,iBAAiB;MACjB,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;EAEL;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,aAAa;GACd;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,sBAAsB;UAClB,wBAAwB;MAC5B,WAAW;KACZ;;MAEC;QACE,mBAAmB;QACnB,gBAAgB;QAChB,gBAAgB;QAChB,cAAc;QACd,oBAAoB;OACrB;;MAED;QACE,gBAAgB;QAChB,mBAAmB;QACnB,UAAU;QACV,kBAAkB;OACnB;;IAEH;MACE,WAAW;MACX,iBAAiB;MACjB,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;CACN","file":"Culture.scss","sourcesContent":[".header_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  height: 80vh;\n  width: 100%;\n  overflow: hidden;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.header_contents {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  padding: 10% 2% 10% 10%;\n}\n\n.header_contents h2 {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: left;\n  color: #fff;\n}\n\n.header_contents p {\n  font-size: 20px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.header_image {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 10% 10% 10% 0%;\n}\n\n.header_image img {\n  width: 442px;\n  height: 315px;\n}\n\n.rightSideImageConatainer {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #fff;\n}\n\n.content {\n  width: 50%;\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-left: 10%;\n  padding-right: 8%;\n}\n\n.content h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.content p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n.image {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.image img {\n  width: 60%;\n}\n\n.leftSideImageConatainer {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.leftSideImageContent {\n  width: 50%;\n  padding-right: 10%;\n  padding-left: 8%;\n}\n\n.leftSideImageContent h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.leftSideImageContent p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n@media only screen and (max-width: 989px) {\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .header_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 560px;\n    padding: 32px 16px;\n  }\n\n  .header_contents {\n    width: 100%;\n    padding: 0;\n  }\n\n  .header_contents h2 {\n    font-size: 32px;\n    line-height: 40px;\n    text-align: center;\n    margin: 0;\n    margin-bottom: 16px;\n    padding: 0 24px;\n  }\n\n  .header_contents p {\n    font-size: 14px;\n    line-height: 26px;\n    margin: 0;\n    text-align: center;\n    letter-spacing: -0.42px;\n  }\n\n  .header_image {\n    width: 292px;\n    height: 208px;\n    margin-top: 32px;\n    padding: 0 19px;\n  }\n\n    .header_image img {\n      width: 100%;\n      height: 100%;\n      -o-object-fit: cover;\n         object-fit: cover;\n    }\n\n  .rightSideImageConatainer {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .rightSideImageConatainer .content {\n      width: 100%;\n      height: auto;\n      padding: 0;\n    }\n\n      .rightSideImageConatainer .content h2 {\n        margin: 8px 0;\n        text-align: center;\n        font-size: 24px;\n        line-height: normal;\n      }\n\n      .rightSideImageConatainer .content p {\n        margin: 0;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n      }\n\n    .rightSideImageConatainer .image {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .rightSideImageConatainer .image img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .leftSideImageConatainer {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .leftSideImageConatainer .leftSideImageContent {\n      width: 100%;\n      height: auto;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      padding: 0;\n    }\n\n      .leftSideImageConatainer .leftSideImageContent h2 {\n        text-align: center;\n        font-size: 24px;\n        padding: 0 35px;\n        margin-top: 0;\n        line-height: normal;\n      }\n\n      .leftSideImageConatainer .leftSideImageContent p {\n        font-size: 14px;\n        text-align: center;\n        margin: 0;\n        line-height: 24px;\n      }\n\n    .leftSideImageConatainer .image {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .leftSideImageConatainer .image img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"header_container": "Culture-header_container-2aO9f",
	"scrollTop": "Culture-scrollTop-1szFV",
	"header_contents": "Culture-header_contents-2Zv0J",
	"header_image": "Culture-header_image-3ZvP9",
	"rightSideImageConatainer": "Culture-rightSideImageConatainer-2qt-l",
	"content": "Culture-content-QdxRQ",
	"image": "Culture-image-325bz",
	"leftSideImageConatainer": "Culture-leftSideImageConatainer-k1SrS",
	"leftSideImageContent": "Culture-leftSideImageContent-ZxM8e"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/Culture.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Culture_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/culture/Culture.scss");
/* harmony import */ var _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Culture_scss__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/Culture.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





class Tests extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      }));
    });

    _defineProperty(this, "displayHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, "Culture is a funny thing", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }), "to describe in words"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, "We believe culture is the most important driver of organizational success and of the satisfaction we feel at the end of each day. But when put into words, it can sound contrived. So we\u2019ll just tell you what you\u2019d observe as you wander the halls.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/culture-banner.svg",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }))));

    _defineProperty(this, "displayThinkBig", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, "Think Big - 10X not 10%"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }, "We believe culture is the most important driver of organizational success and of the satisfaction we feel at the end of each day. But when put into words,it can sound contrived. So we\u2019ll just tell you what you\u2019d observe as you wander the halls.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/think-big.svg",
      alt: "think-big",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }))));

    _defineProperty(this, "displayProblems", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, "Solve whole and hard", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }), " problems that matter"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, "Users have numerous small problems but we have learned to look at the bigger picture. We don\u2019t just believe in telling the user where they are weak at, we would offer you a learning plan to improve on it.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/problems.svg",
      alt: "problems",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }))));

    _defineProperty(this, "displayExperience", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, "Design the experience not the product"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, "A product that solves the user problem in a simple, easy and efficient manner. An amazing experience on the other hand creates customer delight and puts a smile on their face along with solving the problem.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/experience.svg",
      alt: "experience",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }))));

    _defineProperty(this, "displayAccountability", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }, "Accountability and", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }), " ownership"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 139
      },
      __self: this
    }, "We don\u2019t micromanage; period. We believe that when employees take ownership and have autonomy they achieve more. The accountability in-built in their success, and that\u2019s the way we want it..")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/accountability-and-ownership.svg",
      alt: "accountability and ownership",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146
      },
      __self: this
    }))));

    _defineProperty(this, "displaySmart", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }, "Develop and retain smart and interesting people"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }, "People are the key to any organisation and here at Egnify we hire the best, smart and interesting people. It\u2019s not just your resume that we\u2019re interested in, but we want to know the distance travelled and the wounds suffered along the way.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/smart.svg",
      alt: "smart",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166
      },
      __self: this
    }))));

    _defineProperty(this, "displayFailure", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 172
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 173
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 174
      },
      __self: this
    }, "Failure tolerance"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 175
      },
      __self: this
    }, "Failures are the pillars of success - we deeply believe in this. We want to move fast and so things will break and mistakes will be made, the idea is to learn from them and move faster.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 181
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/failure.svg",
      alt: "failure",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 182
      },
      __self: this
    }))));

    _defineProperty(this, "displayFun", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 188
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 189
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 190
      },
      __self: this
    }, "Have fun!"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 191
      },
      __self: this
    }, "We enjoy what we do, and we also enjoy a couple of beers after work in the middle of the week.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 196
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/culture/have-fun.svg",
      alt: "have-fun",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 197
      },
      __self: this
    }))));

    this.state = {};
  }

  componentDidMount() {
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }, this.displayHeader()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 206
      },
      __self: this
    }, this.displayThinkBig()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 207
      },
      __self: this
    }, this.displayProblems()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 208
      },
      __self: this
    }, this.displayExperience()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 209
      },
      __self: this
    }, this.displayAccountability()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 210
      },
      __self: this
    }, this.displaySmart()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 211
      },
      __self: this
    }, this.displayFailure()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 212
      },
      __self: this
    }, this.displayFun()), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a)(Tests));

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/Culture.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/culture/Culture.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Culture__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/culture/Culture.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/index.js";




async function action() {
  return {
    title: 'Culture - Organizational success',
    chunks: ['Culture'],
    content: 'We are with a strong culture defined with organizational sucess and steady progress.',
    keywords: 'digital evaluation, assessment App',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Culture__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0N1bHR1cmUuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdWx0dXJlL0N1bHR1cmUuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdWx0dXJlL0N1bHR1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY3VsdHVyZS9DdWx0dXJlLnNjc3M/YzdkMSIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdWx0dXJlL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuQ3VsdHVyZS1oZWFkZXJfY29udGFpbmVyLTJhTzlmIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBoZWlnaHQ6IDgwdmg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCB0b3AsIHJpZ2h0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbn1cXG5cXG4uQ3VsdHVyZS1zY3JvbGxUb3AtMXN6RlYge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5DdWx0dXJlLXNjcm9sbFRvcC0xc3pGViBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHBhZGRpbmc6IDEwJSAyJSAxMCUgMTAlO1xcbn1cXG5cXG4uQ3VsdHVyZS1oZWFkZXJfY29udGVudHMtMlp2MEogaDIge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDYwcHg7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiBwIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5DdWx0dXJlLWhlYWRlcl9pbWFnZS0zWnZQOSB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDEwJSAxMCUgMTAlIDAlO1xcbn1cXG5cXG4uQ3VsdHVyZS1oZWFkZXJfaW1hZ2UtM1p2UDkgaW1nIHtcXG4gIHdpZHRoOiA0NDJweDtcXG4gIGhlaWdodDogMzE1cHg7XFxufVxcblxcbi5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNjB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uQ3VsdHVyZS1jb250ZW50LVFkeFJRIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy1sZWZ0OiAxMCU7XFxuICBwYWRkaW5nLXJpZ2h0OiA4JTtcXG59XFxuXFxuLkN1bHR1cmUtY29udGVudC1RZHhSUSBoMiB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuXFxuLkN1bHR1cmUtY29udGVudC1RZHhSUSBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjc7XFxufVxcblxcbi5DdWx0dXJlLWltYWdlLTMyNWJ6IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4uQ3VsdHVyZS1pbWFnZS0zMjVieiBpbWcge1xcbiAgd2lkdGg6IDYwJTtcXG59XFxuXFxuLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDYwdmg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb250ZW50LVp4TThlIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDglO1xcbn1cXG5cXG4uQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29udGVudC1aeE04ZSBoMiB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuXFxuLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbnRlbnQtWnhNOGUgcCB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC43O1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk4OXB4KSB7XFxuICAuQ3VsdHVyZS1zY3JvbGxUb3AtMXN6RlYge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLkN1bHR1cmUtaGVhZGVyX2NvbnRhaW5lci0yYU85ZiB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDU2MHB4O1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICB9XFxuXFxuICAuQ3VsdHVyZS1oZWFkZXJfY29udGVudHMtMlp2MEoge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICBwYWRkaW5nOiAwIDI0cHg7XFxuICB9XFxuXFxuICAuQ3VsdHVyZS1oZWFkZXJfY29udGVudHMtMlp2MEogcCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuNDJweDtcXG4gIH1cXG5cXG4gIC5DdWx0dXJlLWhlYWRlcl9pbWFnZS0zWnZQOSB7XFxuICAgIHdpZHRoOiAyOTJweDtcXG4gICAgaGVpZ2h0OiAyMDhweDtcXG4gICAgbWFyZ2luLXRvcDogMzJweDtcXG4gICAgcGFkZGluZzogMCAxOXB4O1xcbiAgfVxcblxcbiAgICAuQ3VsdHVyZS1oZWFkZXJfaW1hZ2UtM1p2UDkgaW1nIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgIH1cXG5cXG4gIC5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBoZWlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAgIC5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCAuQ3VsdHVyZS1jb250ZW50LVFkeFJRIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IGF1dG87XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgfVxcblxcbiAgICAgIC5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCAuQ3VsdHVyZS1jb250ZW50LVFkeFJRIGgyIHtcXG4gICAgICAgIG1hcmdpbjogOHB4IDA7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAuQ3VsdHVyZS1yaWdodFNpZGVJbWFnZUNvbmF0YWluZXItMnF0LWwgLkN1bHR1cmUtY29udGVudC1RZHhSUSBwIHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIC5DdWx0dXJlLWltYWdlLTMyNWJ6IHtcXG4gICAgICB3aWR0aDogODAlO1xcbiAgICAgIG1pbi13aWR0aDogMjMycHg7XFxuICAgICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgfVxcblxcbiAgICAgIC5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCAuQ3VsdHVyZS1pbWFnZS0zMjVieiBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuICAuQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29uYXRhaW5lci1rMVNyUyB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBoZWlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAgIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb250ZW50LVp4TThlIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IGF1dG87XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIHBhZGRpbmc6IDA7XFxuICAgIH1cXG5cXG4gICAgICAuQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29uYXRhaW5lci1rMVNyUyAuQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29udGVudC1aeE04ZSBoMiB7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICBwYWRkaW5nOiAwIDM1cHg7XFxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICB9XFxuXFxuICAgICAgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclMgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbnRlbnQtWnhNOGUgcCB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB9XFxuXFxuICAgIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTIC5DdWx0dXJlLWltYWdlLTMyNWJ6IHtcXG4gICAgICB3aWR0aDogODAlO1xcbiAgICAgIG1pbi13aWR0aDogMjMycHg7XFxuICAgICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgfVxcblxcbiAgICAgIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTIC5DdWx0dXJlLWltYWdlLTMyNWJ6IGltZyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdWx0dXJlL0N1bHR1cmUuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsYUFBYTtFQUNiLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsNEZBQTRGO0VBQzVGLDBFQUEwRTtFQUMxRSxxRUFBcUU7RUFDckUsc0VBQXNFO0NBQ3ZFOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsYUFBYTtDQUNkOztBQUVEO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UsV0FBVztDQUNaOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLGdDQUFnQztNQUM1Qiw0QkFBNEI7RUFDaEMsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGFBQWE7Q0FDZDs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLDBCQUEwQjtRQUN0Qiw4QkFBOEI7SUFDbEMsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixZQUFZO0lBQ1osY0FBYztJQUNkLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1Ysb0JBQW9CO0lBQ3BCLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLG1CQUFtQjtJQUNuQix3QkFBd0I7R0FDekI7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsY0FBYztJQUNkLGlCQUFpQjtJQUNqQixnQkFBZ0I7R0FDakI7O0lBRUM7TUFDRSxZQUFZO01BQ1osYUFBYTtNQUNiLHFCQUFxQjtTQUNsQixrQkFBa0I7S0FDdEI7O0VBRUg7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLG1CQUFtQjtJQUNuQixhQUFhO0dBQ2Q7O0lBRUM7TUFDRSxZQUFZO01BQ1osYUFBYTtNQUNiLFdBQVc7S0FDWjs7TUFFQztRQUNFLGNBQWM7UUFDZCxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLFVBQVU7UUFDVixtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtPQUNuQjs7SUFFSDtNQUNFLFdBQVc7TUFDWCxpQkFBaUI7TUFDakIsY0FBYztLQUNmOztNQUVDO1FBQ0UsWUFBWTtRQUNaLGFBQWE7UUFDYix1QkFBdUI7V0FDcEIsb0JBQW9CO09BQ3hCOztFQUVMO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixtQkFBbUI7SUFDbkIsYUFBYTtHQUNkOztJQUVDO01BQ0UsWUFBWTtNQUNaLGFBQWE7TUFDYixxQkFBcUI7TUFDckIsY0FBYztNQUNkLDJCQUEyQjtVQUN2Qix1QkFBdUI7TUFDM0Isc0JBQXNCO1VBQ2xCLHdCQUF3QjtNQUM1QixXQUFXO0tBQ1o7O01BRUM7UUFDRSxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2Qsb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixVQUFVO1FBQ1Ysa0JBQWtCO09BQ25COztJQUVIO01BQ0UsV0FBVztNQUNYLGlCQUFpQjtNQUNqQixjQUFjO0tBQ2Y7O01BRUM7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLHVCQUF1QjtXQUNwQixvQkFBb0I7T0FDeEI7Q0FDTlwiLFwiZmlsZVwiOlwiQ3VsdHVyZS5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5oZWFkZXJfY29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBoZWlnaHQ6IDgwdmg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCB0b3AsIHJpZ2h0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbn1cXG5cXG4uc2Nyb2xsVG9wIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uc2Nyb2xsVG9wIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLmhlYWRlcl9jb250ZW50cyB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHBhZGRpbmc6IDEwJSAyJSAxMCUgMTAlO1xcbn1cXG5cXG4uaGVhZGVyX2NvbnRlbnRzIGgyIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA2MHB4O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uaGVhZGVyX2NvbnRlbnRzIHAge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLmhlYWRlcl9pbWFnZSB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDEwJSAxMCUgMTAlIDAlO1xcbn1cXG5cXG4uaGVhZGVyX2ltYWdlIGltZyB7XFxuICB3aWR0aDogNDQycHg7XFxuICBoZWlnaHQ6IDMxNXB4O1xcbn1cXG5cXG4ucmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA2MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5jb250ZW50IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy1sZWZ0OiAxMCU7XFxuICBwYWRkaW5nLXJpZ2h0OiA4JTtcXG59XFxuXFxuLmNvbnRlbnQgaDIge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblxcbi5jb250ZW50IHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLmltYWdlIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4uaW1hZ2UgaW1nIHtcXG4gIHdpZHRoOiA2MCU7XFxufVxcblxcbi5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNjB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLmxlZnRTaWRlSW1hZ2VDb250ZW50IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDglO1xcbn1cXG5cXG4ubGVmdFNpZGVJbWFnZUNvbnRlbnQgaDIge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblxcbi5sZWZ0U2lkZUltYWdlQ29udGVudCBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjc7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTg5cHgpIHtcXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLmhlYWRlcl9jb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiA1NjBweDtcXG4gICAgcGFkZGluZzogMzJweCAxNnB4O1xcbiAgfVxcblxcbiAgLmhlYWRlcl9jb250ZW50cyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLmhlYWRlcl9jb250ZW50cyBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICBwYWRkaW5nOiAwIDI0cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyX2NvbnRlbnRzIHAge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xcbiAgICBtYXJnaW46IDA7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjQycHg7XFxuICB9XFxuXFxuICAuaGVhZGVyX2ltYWdlIHtcXG4gICAgd2lkdGg6IDI5MnB4O1xcbiAgICBoZWlnaHQ6IDIwOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xcbiAgICBwYWRkaW5nOiAwIDE5cHg7XFxuICB9XFxuXFxuICAgIC5oZWFkZXJfaW1hZ2UgaW1nIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgIH1cXG5cXG4gIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweDtcXG4gICAgaGVpZ2h0OiBhdXRvO1xcbiAgfVxcblxcbiAgICAucmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyIC5jb250ZW50IHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IGF1dG87XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgfVxcblxcbiAgICAgIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIgLmNvbnRlbnQgaDIge1xcbiAgICAgICAgbWFyZ2luOiA4cHggMDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgfVxcblxcbiAgICAgIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIgLmNvbnRlbnQgcCB7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB9XFxuXFxuICAgIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIgLmltYWdlIHtcXG4gICAgICB3aWR0aDogODAlO1xcbiAgICAgIG1pbi13aWR0aDogMjMycHg7XFxuICAgICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgfVxcblxcbiAgICAgIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIgLmltYWdlIGltZyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG5cXG4gIC5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBoZWlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAgIC5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciAubGVmdFNpZGVJbWFnZUNvbnRlbnQge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogYXV0bztcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgfVxcblxcbiAgICAgIC5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciAubGVmdFNpZGVJbWFnZUNvbnRlbnQgaDIge1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgICAgcGFkZGluZzogMCAzNXB4O1xcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgfVxcblxcbiAgICAgIC5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciAubGVmdFNpZGVJbWFnZUNvbnRlbnQgcCB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB9XFxuXFxuICAgIC5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lciAuaW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA4MCU7XFxuICAgICAgbWluLXdpZHRoOiAyMzJweDtcXG4gICAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICB9XFxuXFxuICAgICAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIC5pbWFnZSBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJoZWFkZXJfY29udGFpbmVyXCI6IFwiQ3VsdHVyZS1oZWFkZXJfY29udGFpbmVyLTJhTzlmXCIsXG5cdFwic2Nyb2xsVG9wXCI6IFwiQ3VsdHVyZS1zY3JvbGxUb3AtMXN6RlZcIixcblx0XCJoZWFkZXJfY29udGVudHNcIjogXCJDdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSlwiLFxuXHRcImhlYWRlcl9pbWFnZVwiOiBcIkN1bHR1cmUtaGVhZGVyX2ltYWdlLTNadlA5XCIsXG5cdFwicmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyXCI6IFwiQ3VsdHVyZS1yaWdodFNpZGVJbWFnZUNvbmF0YWluZXItMnF0LWxcIixcblx0XCJjb250ZW50XCI6IFwiQ3VsdHVyZS1jb250ZW50LVFkeFJRXCIsXG5cdFwiaW1hZ2VcIjogXCJDdWx0dXJlLWltYWdlLTMyNWJ6XCIsXG5cdFwibGVmdFNpZGVJbWFnZUNvbmF0YWluZXJcIjogXCJDdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTXCIsXG5cdFwibGVmdFNpZGVJbWFnZUNvbnRlbnRcIjogXCJDdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb250ZW50LVp4TThlXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBzIGZyb20gJy4vQ3VsdHVyZS5zY3NzJztcblxuY2xhc3MgVGVzdHMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmhhbmRsZVNjcm9sbCgpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICB9XG5cbiAgaGFuZGxlU2Nyb2xsID0gKCkgPT4ge1xuICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+IDUwMCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IHRydWUsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZVNjcm9sbFRvcCA9ICgpID0+IHtcbiAgICB3aW5kb3cuc2Nyb2xsVG8oe1xuICAgICAgdG9wOiAwLFxuICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnLFxuICAgIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgZGlzcGxheVNjcm9sbFRvVG9wID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgc2hvd1Njcm9sbCB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgc2hvd1Njcm9sbCAmJiAoXG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3Muc2Nyb2xsVG9wfVxuICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlU2Nyb2xsVG9wKCk7XG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3Njcm9sbFRvcC5zdmdcIiBhbHQ9XCJzY3JvbGxUb3BcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIClcbiAgICApO1xuICB9O1xuXG4gIGRpc3BsYXlIZWFkZXIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJfY29udGVudHN9PlxuICAgICAgICA8aDI+XG4gICAgICAgICAgQ3VsdHVyZSBpcyBhIGZ1bm55IHRoaW5nXG4gICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgdG8gZGVzY3JpYmUgaW4gd29yZHNcbiAgICAgICAgPC9oMj5cbiAgICAgICAgPHA+XG4gICAgICAgICAgV2UgYmVsaWV2ZSBjdWx0dXJlIGlzIHRoZSBtb3N0IGltcG9ydGFudCBkcml2ZXIgb2Ygb3JnYW5pemF0aW9uYWxcbiAgICAgICAgICBzdWNjZXNzIGFuZCBvZiB0aGUgc2F0aXNmYWN0aW9uIHdlIGZlZWwgYXQgdGhlIGVuZCBvZiBlYWNoIGRheS4gQnV0XG4gICAgICAgICAgd2hlbiBwdXQgaW50byB3b3JkcywgaXQgY2FuIHNvdW5kIGNvbnRyaXZlZC4gU28gd2XigJlsbCBqdXN0IHRlbGwgeW91XG4gICAgICAgICAgd2hhdCB5b3XigJlkIG9ic2VydmUgYXMgeW91IHdhbmRlciB0aGUgaGFsbHMuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyX2ltYWdlfT5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2N1bHR1cmUvY3VsdHVyZS1iYW5uZXIuc3ZnXCIgYWx0PVwiXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlUaGlua0JpZyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgIDxoMj5UaGluayBCaWcgLSAxMFggbm90IDEwJTwvaDI+XG4gICAgICAgIDxwPlxuICAgICAgICAgIFdlIGJlbGlldmUgY3VsdHVyZSBpcyB0aGUgbW9zdCBpbXBvcnRhbnQgZHJpdmVyIG9mIG9yZ2FuaXphdGlvbmFsXG4gICAgICAgICAgc3VjY2VzcyBhbmQgb2YgdGhlIHNhdGlzZmFjdGlvbiB3ZSBmZWVsIGF0IHRoZSBlbmQgb2YgZWFjaCBkYXkuIEJ1dFxuICAgICAgICAgIHdoZW4gcHV0IGludG8gd29yZHMsaXQgY2FuIHNvdW5kIGNvbnRyaXZlZC4gU28gd2XigJlsbCBqdXN0IHRlbGwgeW91XG4gICAgICAgICAgd2hhdCB5b3XigJlkIG9ic2VydmUgYXMgeW91IHdhbmRlciB0aGUgaGFsbHMuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2V9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS90aGluay1iaWcuc3ZnXCIgYWx0PVwidGhpbmstYmlnXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlQcm9ibGVtcyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29udGVudH0+XG4gICAgICAgIDxoMj5cbiAgICAgICAgICBTb2x2ZSB3aG9sZSBhbmQgaGFyZFxuICAgICAgICAgIDxiciAvPiBwcm9ibGVtcyB0aGF0IG1hdHRlclxuICAgICAgICA8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBVc2VycyBoYXZlIG51bWVyb3VzIHNtYWxsIHByb2JsZW1zIGJ1dCB3ZSBoYXZlIGxlYXJuZWQgdG8gbG9vayBhdCB0aGVcbiAgICAgICAgICBiaWdnZXIgcGljdHVyZS4gV2UgZG9u4oCZdCBqdXN0IGJlbGlldmUgaW4gdGVsbGluZyB0aGUgdXNlciB3aGVyZSB0aGV5XG4gICAgICAgICAgYXJlIHdlYWsgYXQsIHdlIHdvdWxkIG9mZmVyIHlvdSBhIGxlYXJuaW5nIHBsYW4gdG8gaW1wcm92ZSBvbiBpdC5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9jdWx0dXJlL3Byb2JsZW1zLnN2Z1wiIGFsdD1cInByb2JsZW1zXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlFeHBlcmllbmNlID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgPGgyPkRlc2lnbiB0aGUgZXhwZXJpZW5jZSBub3QgdGhlIHByb2R1Y3Q8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBBIHByb2R1Y3QgdGhhdCBzb2x2ZXMgdGhlIHVzZXIgcHJvYmxlbSBpbiBhIHNpbXBsZSwgZWFzeSBhbmQgZWZmaWNpZW50XG4gICAgICAgICAgbWFubmVyLiBBbiBhbWF6aW5nIGV4cGVyaWVuY2Ugb24gdGhlIG90aGVyIGhhbmQgY3JlYXRlcyBjdXN0b21lclxuICAgICAgICAgIGRlbGlnaHQgYW5kIHB1dHMgYSBzbWlsZSBvbiB0aGVpciBmYWNlIGFsb25nIHdpdGggc29sdmluZyB0aGUgcHJvYmxlbS5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9jdWx0dXJlL2V4cGVyaWVuY2Uuc3ZnXCIgYWx0PVwiZXhwZXJpZW5jZVwiIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5QWNjb3VudGFiaWxpdHkgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MubGVmdFNpZGVJbWFnZUNvbmF0YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGVmdFNpZGVJbWFnZUNvbnRlbnR9PlxuICAgICAgICA8aDI+XG4gICAgICAgICAgQWNjb3VudGFiaWxpdHkgYW5kXG4gICAgICAgICAgPGJyIC8+IG93bmVyc2hpcFxuICAgICAgICA8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBXZSBkb27igJl0IG1pY3JvbWFuYWdlOyBwZXJpb2QuIFdlIGJlbGlldmUgdGhhdCB3aGVuIGVtcGxveWVlcyB0YWtlXG4gICAgICAgICAgb3duZXJzaGlwIGFuZCBoYXZlIGF1dG9ub215IHRoZXkgYWNoaWV2ZSBtb3JlLiBUaGUgYWNjb3VudGFiaWxpdHlcbiAgICAgICAgICBpbi1idWlsdCBpbiB0aGVpciBzdWNjZXNzLCBhbmQgdGhhdOKAmXMgdGhlIHdheSB3ZSB3YW50IGl0Li5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWdcbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2N1bHR1cmUvYWNjb3VudGFiaWxpdHktYW5kLW93bmVyc2hpcC5zdmdcIlxuICAgICAgICAgIGFsdD1cImFjY291bnRhYmlsaXR5IGFuZCBvd25lcnNoaXBcIlxuICAgICAgICAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVNtYXJ0ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgPGgyPkRldmVsb3AgYW5kIHJldGFpbiBzbWFydCBhbmQgaW50ZXJlc3RpbmcgcGVvcGxlPC9oMj5cbiAgICAgICAgPHA+XG4gICAgICAgICAgUGVvcGxlIGFyZSB0aGUga2V5IHRvIGFueSBvcmdhbmlzYXRpb24gYW5kIGhlcmUgYXQgRWduaWZ5IHdlIGhpcmUgdGhlXG4gICAgICAgICAgYmVzdCwgc21hcnQgYW5kIGludGVyZXN0aW5nIHBlb3BsZS4gSXTigJlzIG5vdCBqdXN0IHlvdXIgcmVzdW1lIHRoYXRcbiAgICAgICAgICB3ZeKAmXJlIGludGVyZXN0ZWQgaW4sIGJ1dCB3ZSB3YW50IHRvIGtub3cgdGhlIGRpc3RhbmNlIHRyYXZlbGxlZCBhbmRcbiAgICAgICAgICB0aGUgd291bmRzIHN1ZmZlcmVkIGFsb25nIHRoZSB3YXkuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2V9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS9zbWFydC5zdmdcIiBhbHQ9XCJzbWFydFwiIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5RmFpbHVyZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29udGVudH0+XG4gICAgICAgIDxoMj5GYWlsdXJlIHRvbGVyYW5jZTwvaDI+XG4gICAgICAgIDxwPlxuICAgICAgICAgIEZhaWx1cmVzIGFyZSB0aGUgcGlsbGFycyBvZiBzdWNjZXNzIC0gd2UgZGVlcGx5IGJlbGlldmUgaW4gdGhpcy4gV2VcbiAgICAgICAgICB3YW50IHRvIG1vdmUgZmFzdCBhbmQgc28gdGhpbmdzIHdpbGwgYnJlYWsgYW5kIG1pc3Rha2VzIHdpbGwgYmUgbWFkZSxcbiAgICAgICAgICB0aGUgaWRlYSBpcyB0byBsZWFybiBmcm9tIHRoZW0gYW5kIG1vdmUgZmFzdGVyLlxuICAgICAgICA8L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltYWdlfT5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2N1bHR1cmUvZmFpbHVyZS5zdmdcIiBhbHQ9XCJmYWlsdXJlXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlGdW4gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MucmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICA8aDI+SGF2ZSBmdW4hPC9oMj5cbiAgICAgICAgPHA+XG4gICAgICAgICAgV2UgZW5qb3kgd2hhdCB3ZSBkbywgYW5kIHdlIGFsc28gZW5qb3kgYSBjb3VwbGUgb2YgYmVlcnMgYWZ0ZXIgd29yayBpblxuICAgICAgICAgIHRoZSBtaWRkbGUgb2YgdGhlIHdlZWsuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2V9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS9oYXZlLWZ1bi5zdmdcIiBhbHQ9XCJoYXZlLWZ1blwiIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUhlYWRlcigpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlUaGlua0JpZygpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlQcm9ibGVtcygpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlFeHBlcmllbmNlKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUFjY291bnRhYmlsaXR5KCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheVNtYXJ0KCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUZhaWx1cmUoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5RnVuKCl9PC9kaXY+XG4gICAgICAgIHt0aGlzLmRpc3BsYXlTY3JvbGxUb1RvcCgpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFRlc3RzKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ3VsdHVyZS5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0N1bHR1cmUuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0N1bHR1cmUuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dC9MYXlvdXQnO1xuaW1wb3J0IEN1bHR1cmUgZnJvbSAnLi9DdWx0dXJlJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnQ3VsdHVyZSAtIE9yZ2FuaXphdGlvbmFsIHN1Y2Nlc3MnLFxuICAgIGNodW5rczogWydDdWx0dXJlJ10sXG4gICAgY29udGVudDpcbiAgICAgICdXZSBhcmUgd2l0aCBhIHN0cm9uZyBjdWx0dXJlIGRlZmluZWQgd2l0aCBvcmdhbml6YXRpb25hbCBzdWNlc3MgYW5kIHN0ZWFkeSBwcm9ncmVzcy4nLFxuICAgIGtleXdvcmRzOiAnZGlnaXRhbCBldmFsdWF0aW9uLCBhc3Nlc3NtZW50IEFwcCcsXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0IGZvb3RlckFzaD5cbiAgICAgICAgPEN1bHR1cmUgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQWVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUF6QkE7QUEyQkE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBbkNBO0FBcUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBcERBO0FBc0RBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXRFQTtBQTJFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF2RkE7QUE0RkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBMUdBO0FBK0dBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQTFIQTtBQStIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE3SUE7QUFxSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBaktBO0FBc0tBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWpMQTtBQXNMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE5TEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdUxBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBcE5BO0FBQ0E7QUFxTkE7Ozs7Ozs7QUMxTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFSQTtBQVlBO0FBQ0E7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9