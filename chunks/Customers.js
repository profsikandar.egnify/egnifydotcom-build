require("source-map-support").install();
exports.ids = ["Customers"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/customers/Customers.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Customers-headerContainer-3EA99 {\n  height: 80vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 64px;\n  background-color: #f7f7f7;\n}\n\n.Customers-scrollTop-2r3qa {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Customers-scrollTop-2r3qa img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Customers-header_text-1Nc_1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  width: 41%;\n}\n\n.Customers-trustedByTitle-3pPTH {\n  font-size: 64px;\n  font-size: 4rem;\n  font-weight: 600;\n  color: #25282b;\n}\n\n/* .trustedBy_text {\n  color: #000;\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 1.5;\n} */\n\n.Customers-trustedBy-3FuEk {\n  color: #f36;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-header_box_container-3ATaO {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  gap: 32px;\n}\n\n.Customers-box-2g2DN {\n  width: 320px;\n  height: 320px;\n  -webkit-box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n          box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n  padding: 54px 40px;\n  border-radius: 8px;\n  text-align: center;\n}\n\n.Customers-box_contents_tag-1-7OE {\n  font-size: 20px;\n  color: #25282b;\n  opacity: 0.6;\n  font-weight: normal;\n  margin-bottom: 24px;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-box_contents-1_6Sy {\n  font-size: 48px;\n  font-size: 3rem;\n  font-weight: 600;\n  color: #25282b;\n  margin: 16px 0;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-profilebox-YjxD8 {\n  width: 72px;\n  height: 72px;\n  border-radius: 50%;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Customers-profilebox-YjxD8 img {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Customers-newtworkContainer-WUsSr {\n  height: auto;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 32px 64px;\n  padding: 2rem 4rem;\n  background-color: #fff;\n}\n\n.Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm img {\n  width: 400px;\n  height: 450px;\n}\n\n.Customers-network-3OQoG {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: start;\n      align-items: flex-start;\n}\n\n.Customers-networkTitle-e7bL- {\n  font-size: 40px;\n  font-weight: 600;\n  color: #25282b;\n  text-align: left;\n}\n\n/* .network_content p {\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 2;\n} */\n\n.Customers-customers_container-22ceD {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #f7f7f7;\n}\n\n.Customers-customers_container2-X7fw- {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #fff;\n}\n\n.Customers-customer_review-c1UsK {\n  width: 50%;\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 64px;\n  padding: 4rem;\n}\n\n/* .customerLogo {\n  border-radius: 8%;\n} */\n\n.Customers-sriChaitanyaText-2FAda {\n  font-size: 24px;\n  font-weight: normal;\n  line-height: 1.5;\n  text-align: left;\n}\n\n.Customers-author-1T0Ew {\n  font-size: 14px;\n  line-height: 1.43;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-top: 16px;\n}\n\n.Customers-author_title-aiead {\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.2;\n  margin: 12px 0 8px 0;\n}\n\n.Customers-customers-1ngOt {\n  width: 50%;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  grid-row-gap: 0;\n  grid-column-gap: 0;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m {\n  -ms-flex: 1 1;\n      flex: 1 1;\n  height: 106px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m:nth-of-type(even) {\n  background-color: #fff;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m img {\n  -o-object-fit: contain;\n     object-fit: contain;\n  max-width: 200px;\n}\n\n@media only screen and (max-width: 990px) {\n  .Customers-scrollTop-2r3qa {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Customers-headerContainer-3EA99 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    text-align: center;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding: 24px;\n    background-color: #fff;\n  }\n\n  .Customers-header_text-1Nc_1 {\n    width: 100%;\n  }\n\n  /* .network_content > p {\n    font-size: 14px;\n    text-align: center;\n    line-height: 1.71;\n  } */\n\n  /* .trustedBy_text {\n    text-align: center;\n    max-width: 600px;\n    font-size: 14px;\n    line-height: 24px;\n    letter-spacing: -0.42px;\n    margin: 8px auto 0 auto;\n\n    p {\n      margin: 0;\n    }\n\n    p:nth-child(1) {\n      margin-bottom: 8px;\n    }\n  } */\n\n  .Customers-trustedByTitle-3pPTH {\n    font-size: 32px;\n  }\n\n  .Customers-header_box_container-3ATaO {\n    grid-template-columns: 1fr;\n    gap: 24px;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin-top: 24px;\n  }\n\n  .Customers-box-2g2DN {\n    width: 240px;\n    height: 240px;\n    padding: 36px 32px;\n  }\n\n    .Customers-box-2g2DN img {\n      width: 55px;\n      height: 55px;\n    }\n\n    .Customers-box-2g2DN .Customers-box_contents-1_6Sy {\n      font-size: 32px;\n      margin: 18px 0 12px 0;\n    }\n\n    .Customers-box-2g2DN .Customers-box_contents_tag-1-7OE {\n      font-size: 15px;\n      margin-bottom: 0;\n    }\n\n  .Customers-newtworkContainer-WUsSr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 100%;\n    padding: 1.5rem;\n  }\n\n  .Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm {\n    width: 100%;\n  }\n\n  .Customers-network-3OQoG {\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .Customers-networkTitle-e7bL- {\n    font-size: 32px;\n    text-align: center;\n  }\n\n  .Customers-customers_container-22ceD {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Customers-customer_review-c1UsK {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    padding: 32px 16px 24px;\n  }\n\n  .Customers-author-1T0Ew {\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  /* .customerLogo {\n    border-radius: 8px;\n  } */\n\n  .Customers-sriChaitanyaText-2FAda {\n    width: auto;\n    text-align: center;\n    font-size: 14px;\n    line-height: 1.7;\n  }\n\n  .Customers-customers-1ngOt {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: 0 auto;\n    grid-template-columns: repeat(3, 1fr);\n  }\n\n  .Customers-customers-1ngOt img {\n    width: 126px;\n    height: 68px;\n  }\n\n  .Customers-customers_container2-X7fw- {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/Customers.scss"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,uBAAuB;MACnB,oBAAoB;EACxB,cAAc;EACd,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,WAAW;CACZ;;AAED;EACE,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;;;;;;IAMI;;AAEJ;EACE,YAAY;EACZ,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,+BAA+B;EAC/B,UAAU;CACX;;AAED;EACE,aAAa;EACb,cAAc;EACd,0DAA0D;UAClD,kDAAkD;EAC1D,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,eAAe;EACf,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,mBAAmB;EACnB,mBAAmB;EACnB,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,YAAY;EACZ,0BAA0B;CAC3B;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,YAAY;EACZ,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,uBAAuB;EACvB,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,+BAA+B;EACnC,cAAc;EACd,cAAc;CACf;;AAED;;IAEI;;AAEJ;EACE,gBAAgB;EAChB,oBAAoB;EACpB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,qBAAqB;CACtB;;AAED;EACE,WAAW;EACX,cAAc;EACd,sCAAsC;EACtC,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,cAAc;MACV,UAAU;EACd,cAAc;EACd,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,0BAA0B;CAC3B;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,uBAAuB;KACpB,oBAAoB;EACvB,iBAAiB;CAClB;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,4BAA4B;IAC5B,yBAAyB;IACzB,oBAAoB;IACpB,mBAAmB;IACnB,qBAAqB;QACjB,4BAA4B;IAChC,cAAc;IACd,uBAAuB;GACxB;;EAED;IACE,YAAY;GACb;;EAED;;;;MAII;;EAEJ;;;;;;;;;;;;;;;MAeI;;EAEJ;IACE,gBAAgB;GACjB;;EAED;IACE,2BAA2B;IAC3B,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,aAAa;IACb,cAAc;IACd,mBAAmB;GACpB;;IAEC;MACE,YAAY;MACZ,aAAa;KACd;;IAED;MACE,gBAAgB;MAChB,sBAAsB;KACvB;;IAED;MACE,gBAAgB;MAChB,iBAAiB;KAClB;;EAEH;IACE,qBAAqB;IACrB,cAAc;IACd,mCAAmC;QAC/B,+BAA+B;IACnC,uBAAuB;QACnB,oBAAoB;IACxB,YAAY;IACZ,aAAa;IACb,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;;EAED;IACE,YAAY;IACZ,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;IACE,gBAAgB;IAChB,mBAAmB;GACpB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,YAAY;IACZ,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;IACnB,wBAAwB;GACzB;;EAED;IACE,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;;MAEI;;EAEJ;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;GACvC;;EAED;IACE,aAAa;IACb,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,mCAAmC;QAC/B,+BAA+B;GACpC;CACF","file":"Customers.scss","sourcesContent":[".headerContainer {\n  height: 80vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 64px;\n  background-color: #f7f7f7;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.header_text {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  width: 41%;\n}\n\n.trustedByTitle {\n  font-size: 64px;\n  font-size: 4rem;\n  font-weight: 600;\n  color: #25282b;\n}\n\n/* .trustedBy_text {\n  color: #000;\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 1.5;\n} */\n\n.trustedBy {\n  color: #f36;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.header_box_container {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  gap: 32px;\n}\n\n.box {\n  width: 320px;\n  height: 320px;\n  -webkit-box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n          box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n  padding: 54px 40px;\n  border-radius: 8px;\n  text-align: center;\n}\n\n.box_contents_tag {\n  font-size: 20px;\n  color: #25282b;\n  opacity: 0.6;\n  font-weight: normal;\n  margin-bottom: 24px;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.box_contents {\n  font-size: 48px;\n  font-size: 3rem;\n  font-weight: 600;\n  color: #25282b;\n  margin: 16px 0;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.profilebox {\n  width: 72px;\n  height: 72px;\n  border-radius: 50%;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.profilebox img {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.newtworkContainer {\n  height: auto;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 32px 64px;\n  padding: 2rem 4rem;\n  background-color: #fff;\n}\n\n.newtworkContainer .countryimg {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.newtworkContainer .countryimg img {\n  width: 400px;\n  height: 450px;\n}\n\n.network {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: start;\n      align-items: flex-start;\n}\n\n.networkTitle {\n  font-size: 40px;\n  font-weight: 600;\n  color: #25282b;\n  text-align: left;\n}\n\n/* .network_content p {\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 2;\n} */\n\n.customers_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #f7f7f7;\n}\n\n.customers_container2 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #fff;\n}\n\n.customer_review {\n  width: 50%;\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 64px;\n  padding: 4rem;\n}\n\n/* .customerLogo {\n  border-radius: 8%;\n} */\n\n.sriChaitanyaText {\n  font-size: 24px;\n  font-weight: normal;\n  line-height: 1.5;\n  text-align: left;\n}\n\n.author {\n  font-size: 14px;\n  line-height: 1.43;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-top: 16px;\n}\n\n.author_title {\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.2;\n  margin: 12px 0 8px 0;\n}\n\n.customers {\n  width: 50%;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  grid-row-gap: 0;\n  grid-column-gap: 0;\n}\n\n.customers .client {\n  -ms-flex: 1 1;\n      flex: 1 1;\n  height: 106px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.customers .client:nth-of-type(even) {\n  background-color: #fff;\n}\n\n.customers .client img {\n  -o-object-fit: contain;\n     object-fit: contain;\n  max-width: 200px;\n}\n\n@media only screen and (max-width: 990px) {\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .headerContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    text-align: center;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding: 24px;\n    background-color: #fff;\n  }\n\n  .header_text {\n    width: 100%;\n  }\n\n  /* .network_content > p {\n    font-size: 14px;\n    text-align: center;\n    line-height: 1.71;\n  } */\n\n  /* .trustedBy_text {\n    text-align: center;\n    max-width: 600px;\n    font-size: 14px;\n    line-height: 24px;\n    letter-spacing: -0.42px;\n    margin: 8px auto 0 auto;\n\n    p {\n      margin: 0;\n    }\n\n    p:nth-child(1) {\n      margin-bottom: 8px;\n    }\n  } */\n\n  .trustedByTitle {\n    font-size: 32px;\n  }\n\n  .header_box_container {\n    grid-template-columns: 1fr;\n    gap: 24px;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin-top: 24px;\n  }\n\n  .box {\n    width: 240px;\n    height: 240px;\n    padding: 36px 32px;\n  }\n\n    .box img {\n      width: 55px;\n      height: 55px;\n    }\n\n    .box .box_contents {\n      font-size: 32px;\n      margin: 18px 0 12px 0;\n    }\n\n    .box .box_contents_tag {\n      font-size: 15px;\n      margin-bottom: 0;\n    }\n\n  .newtworkContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 100%;\n    padding: 1.5rem;\n  }\n\n  .newtworkContainer .countryimg {\n    width: 100%;\n  }\n\n  .network {\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .networkTitle {\n    font-size: 32px;\n    text-align: center;\n  }\n\n  .customers_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .customer_review {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    padding: 32px 16px 24px;\n  }\n\n  .author {\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  /* .customerLogo {\n    border-radius: 8px;\n  } */\n\n  .sriChaitanyaText {\n    width: auto;\n    text-align: center;\n    font-size: 14px;\n    line-height: 1.7;\n  }\n\n  .customers {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: 0 auto;\n    grid-template-columns: repeat(3, 1fr);\n  }\n\n  .customers img {\n    width: 126px;\n    height: 68px;\n  }\n\n  .customers_container2 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"headerContainer": "Customers-headerContainer-3EA99",
	"scrollTop": "Customers-scrollTop-2r3qa",
	"header_text": "Customers-header_text-1Nc_1",
	"trustedByTitle": "Customers-trustedByTitle-3pPTH",
	"trustedBy": "Customers-trustedBy-3FuEk",
	"header_box_container": "Customers-header_box_container-3ATaO",
	"box": "Customers-box-2g2DN",
	"box_contents_tag": "Customers-box_contents_tag-1-7OE",
	"box_contents": "Customers-box_contents-1_6Sy",
	"profilebox": "Customers-profilebox-YjxD8",
	"newtworkContainer": "Customers-newtworkContainer-WUsSr",
	"countryimg": "Customers-countryimg-18rEm",
	"network": "Customers-network-3OQoG",
	"networkTitle": "Customers-networkTitle-e7bL-",
	"customers_container": "Customers-customers_container-22ceD",
	"customers_container2": "Customers-customers_container2-X7fw-",
	"customer_review": "Customers-customer_review-c1UsK",
	"sriChaitanyaText": "Customers-sriChaitanyaText-2FAda",
	"author": "Customers-author-1T0Ew",
	"author_title": "Customers-author_title-aiead",
	"customers": "Customers-customers-1ngOt",
	"client": "Customers-client-dHi1m"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/Customers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Customers_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/customers/Customers.scss");
/* harmony import */ var _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Customers_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/Customers.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class Customers extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }));
    });

    _defineProperty(this, "displayTrustedBy", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headerContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.trustedByTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, "Trusted by", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.trustedBy,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, " 100+ Institutions"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header_box_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMERS_STATS"].map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.icon,
      alt: item.label,
      height: "64px",
      width: "64px",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, item.number), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box_contents_tag,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, item.text))))));

    _defineProperty(this, "displayNetwork", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.newtworkContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.countryimg,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "images/customers/Group 1128.jpg",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.network,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.networkTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, "A large network servicing ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }), "the whole country"))));

    _defineProperty(this, "displayCustomersReview", data => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customer_review,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.sriChaitanyaText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, data.text)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.author,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profilebox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/person_blue.svg",
      alt: "profile",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.author_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }, data.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 132
      },
      __self: this
    }, data.designation)))));

    _defineProperty(this, "displayCustomersGrid", data => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 139
      },
      __self: this
    }, data.map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.client,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 141
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.icon,
      alt: item.label,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 142
      },
      __self: this
    })))));

    _defineProperty(this, "displayCustomers1", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149
      },
      __self: this
    }, this.displayCustomersReview(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMER_REVIEW"][0]), this.displayCustomersGrid(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CLIENTS_SET1"])));

    _defineProperty(this, "displayCustomers2", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers_container2,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    }, this.displayCustomersGrid(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CLIENTS_SET2"]), this.displayCustomersReview(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMER_REVIEW"][1])));

    this.state = {};
  }

  componentDidMount() {
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165
      },
      __self: this
    }, this.displayTrustedBy()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166
      },
      __self: this
    }, this.displayNetwork()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 167
      },
      __self: this
    }, this.displayCustomers1()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 168
      },
      __self: this
    }, this.displayCustomers2()), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Customers));

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/Customers.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/customers/Customers.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Customers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/customers/Customers.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/index.js";




async function action() {
  return {
    title: "Customers - Egnify's trusted online institutes and students",
    content: 'Our online education platform is enabling tutors and coaching institutes to go online instantly.',
    chunks: ['Customers'],
    keywords: 'students, teachers',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Customers__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0N1c3RvbWVycy5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycy9DdXN0b21lcnMuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdXN0b21lcnMvQ3VzdG9tZXJzLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycy9DdXN0b21lcnMuc2Nzcz9iZmVkIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLkN1c3RvbWVycy1oZWFkZXJDb250YWluZXItM0VBOTkge1xcbiAgaGVpZ2h0OiA4MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogNjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5DdXN0b21lcnMtc2Nyb2xsVG9wLTJyM3FhIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLXNjcm9sbFRvcC0ycjNxYSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5DdXN0b21lcnMtaGVhZGVyX3RleHQtMU5jXzEge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHdpZHRoOiA0MSU7XFxufVxcblxcbi5DdXN0b21lcnMtdHJ1c3RlZEJ5VGl0bGUtM3BQVEgge1xcbiAgZm9udC1zaXplOiA2NHB4O1xcbiAgZm9udC1zaXplOiA0cmVtO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4vKiAudHJ1c3RlZEJ5X3RleHQge1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG59ICovXFxuXFxuLkN1c3RvbWVycy10cnVzdGVkQnktM0Z1RWsge1xcbiAgY29sb3I6ICNmMzY7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG59XFxuXFxuLkN1c3RvbWVycy1oZWFkZXJfYm94X2NvbnRhaW5lci0zQVRhTyB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gIGdhcDogMzJweDtcXG59XFxuXFxuLkN1c3RvbWVycy1ib3gtMmcyRE4ge1xcbiAgd2lkdGg6IDMyMHB4O1xcbiAgaGVpZ2h0OiAzMjBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggNDBweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggNDBweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjEyKTtcXG4gIHBhZGRpbmc6IDU0cHggNDBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLkN1c3RvbWVycy1ib3hfY29udGVudHNfdGFnLTEtN09FIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG59XFxuXFxuLkN1c3RvbWVycy1ib3hfY29udGVudHMtMV82U3kge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgZm9udC1zaXplOiAzcmVtO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luOiAxNnB4IDA7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG59XFxuXFxuLkN1c3RvbWVycy1wcm9maWxlYm94LVlqeEQ4IHtcXG4gIHdpZHRoOiA3MnB4O1xcbiAgaGVpZ2h0OiA3MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLkN1c3RvbWVycy1wcm9maWxlYm94LVlqeEQ4IGltZyB7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLkN1c3RvbWVycy1uZXd0d29ya0NvbnRhaW5lci1XVXNTciB7XFxuICBoZWlnaHQ6IGF1dG87XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBwYWRkaW5nOiAzMnB4IDY0cHg7XFxuICBwYWRkaW5nOiAycmVtIDRyZW07XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLW5ld3R3b3JrQ29udGFpbmVyLVdVc1NyIC5DdXN0b21lcnMtY291bnRyeWltZy0xOHJFbSB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLW5ld3R3b3JrQ29udGFpbmVyLVdVc1NyIC5DdXN0b21lcnMtY291bnRyeWltZy0xOHJFbSBpbWcge1xcbiAgd2lkdGg6IDQwMHB4O1xcbiAgaGVpZ2h0OiA0NTBweDtcXG59XFxuXFxuLkN1c3RvbWVycy1uZXR3b3JrLTNPUW9HIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLW5ldHdvcmtUaXRsZS1lN2JMLSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4vKiAubmV0d29ya19jb250ZW50IHAge1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGxpbmUtaGVpZ2h0OiAyO1xcbn0gKi9cXG5cXG4uQ3VzdG9tZXJzLWN1c3RvbWVyc19jb250YWluZXItMjJjZUQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWN1c3RvbWVyc19jb250YWluZXIyLVg3ZnctIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLkN1c3RvbWVycy1jdXN0b21lcl9yZXZpZXctYzFVc0sge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHBhZGRpbmc6IDY0cHg7XFxuICBwYWRkaW5nOiA0cmVtO1xcbn1cXG5cXG4vKiAuY3VzdG9tZXJMb2dvIHtcXG4gIGJvcmRlci1yYWRpdXM6IDglO1xcbn0gKi9cXG5cXG4uQ3VzdG9tZXJzLXNyaUNoYWl0YW55YVRleHQtMkZBZGEge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWF1dGhvci0xVDBFdyB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMS40MztcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICBtYXJnaW4tdG9wOiAxNnB4O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWF1dGhvcl90aXRsZS1haWVhZCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gIG1hcmdpbjogMTJweCAwIDhweCAwO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWN1c3RvbWVycy0xbmdPdCB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICBncmlkLXJvdy1nYXA6IDA7XFxuICBncmlkLWNvbHVtbi1nYXA6IDA7XFxufVxcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090IC5DdXN0b21lcnMtY2xpZW50LWRIaTFtIHtcXG4gIC1tcy1mbGV4OiAxIDE7XFxuICAgICAgZmxleDogMSAxO1xcbiAgaGVpZ2h0OiAxMDZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090IC5DdXN0b21lcnMtY2xpZW50LWRIaTFtOm50aC1vZi10eXBlKGV2ZW4pIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090IC5DdXN0b21lcnMtY2xpZW50LWRIaTFtIGltZyB7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIG1heC13aWR0aDogMjAwcHg7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5DdXN0b21lcnMtc2Nyb2xsVG9wLTJyM3FhIHtcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgcmlnaHQ6IDE2cHg7XFxuICAgIGJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtaGVhZGVyQ29udGFpbmVyLTNFQTk5IHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgaGVpZ2h0OiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDI0cHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLWhlYWRlcl90ZXh0LTFOY18xIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAvKiAubmV0d29ya19jb250ZW50ID4gcCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsaW5lLWhlaWdodDogMS43MTtcXG4gIH0gKi9cXG5cXG4gIC8qIC50cnVzdGVkQnlfdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjQycHg7XFxuICAgIG1hcmdpbjogOHB4IGF1dG8gMCBhdXRvO1xcblxcbiAgICBwIHtcXG4gICAgICBtYXJnaW46IDA7XFxuICAgIH1cXG5cXG4gICAgcDpudGgtY2hpbGQoMSkge1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLkN1c3RvbWVycy10cnVzdGVkQnlUaXRsZS0zcFBUSCB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtaGVhZGVyX2JveF9jb250YWluZXItM0FUYU8ge1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgZ2FwOiAyNHB4O1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgbWFyZ2luLXRvcDogMjRweDtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtYm94LTJnMkROIHtcXG4gICAgd2lkdGg6IDI0MHB4O1xcbiAgICBoZWlnaHQ6IDI0MHB4O1xcbiAgICBwYWRkaW5nOiAzNnB4IDMycHg7XFxuICB9XFxuXFxuICAgIC5DdXN0b21lcnMtYm94LTJnMkROIGltZyB7XFxuICAgICAgd2lkdGg6IDU1cHg7XFxuICAgICAgaGVpZ2h0OiA1NXB4O1xcbiAgICB9XFxuXFxuICAgIC5DdXN0b21lcnMtYm94LTJnMkROIC5DdXN0b21lcnMtYm94X2NvbnRlbnRzLTFfNlN5IHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbWFyZ2luOiAxOHB4IDAgMTJweCAwO1xcbiAgICB9XFxuXFxuICAgIC5DdXN0b21lcnMtYm94LTJnMkROIC5DdXN0b21lcnMtYm94X2NvbnRlbnRzX3RhZy0xLTdPRSB7XFxuICAgICAgZm9udC1zaXplOiAxNXB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG5cXG4gIC5DdXN0b21lcnMtbmV3dHdvcmtDb250YWluZXItV1VzU3Ige1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtbmV3dHdvcmtDb250YWluZXItV1VzU3IgLkN1c3RvbWVycy1jb3VudHJ5aW1nLTE4ckVtIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLW5ldHdvcmstM09Rb0cge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLW5ldHdvcmtUaXRsZS1lN2JMLSB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1jdXN0b21lcnNfY29udGFpbmVyLTIyY2VEIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLWN1c3RvbWVyX3Jldmlldy1jMVVzSyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHggMjRweDtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtYXV0aG9yLTFUMEV3IHtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuICAvKiAuY3VzdG9tZXJMb2dvIHtcXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgfSAqL1xcblxcbiAgLkN1c3RvbWVycy1zcmlDaGFpdGFueWFUZXh0LTJGQWRhIHtcXG4gICAgd2lkdGg6IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMS43O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1jdXN0b21lcnMtMW5nT3Qge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLWN1c3RvbWVycy0xbmdPdCBpbWcge1xcbiAgICB3aWR0aDogMTI2cHg7XFxuICAgIGhlaWdodDogNjhweDtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtY3VzdG9tZXJzX2NvbnRhaW5lcjItWDdmdy0ge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdXN0b21lcnMvQ3VzdG9tZXJzLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEOzs7Ozs7SUFNSTs7QUFFSjtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsMkJBQTJCO0tBQ3hCLHdCQUF3QjtDQUM1Qjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCwrQkFBK0I7RUFDL0IsVUFBVTtDQUNYOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCwwREFBMEQ7VUFDbEQsa0RBQWtEO0VBQzFELG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsMkJBQTJCO0tBQ3hCLHdCQUF3QjtDQUM1Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLDJCQUEyQjtLQUN4Qix3QkFBd0I7Q0FDNUI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSx1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4Qix1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEOzs7OztJQUtJOztBQUVKO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsWUFBWTtFQUNaLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4Qix1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLFlBQVk7RUFDWix1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyxjQUFjO0VBQ2QsY0FBYztDQUNmOztBQUVEOztJQUVJOztBQUVKO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLHNDQUFzQztFQUN0QyxnQkFBZ0I7RUFDaEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsY0FBYztNQUNWLFVBQVU7RUFDZCxjQUFjO0VBQ2QscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsdUJBQXVCO0tBQ3BCLG9CQUFvQjtFQUN2QixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQiw0QkFBNEI7SUFDNUIseUJBQXlCO0lBQ3pCLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIscUJBQXFCO1FBQ2pCLDRCQUE0QjtJQUNoQyxjQUFjO0lBQ2QsdUJBQXVCO0dBQ3hCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEOzs7O01BSUk7O0VBRUo7Ozs7Ozs7Ozs7Ozs7OztNQWVJOztFQUVKO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsMkJBQTJCO0lBQzNCLFVBQVU7SUFDViwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsY0FBYztJQUNkLG1CQUFtQjtHQUNwQjs7SUFFQztNQUNFLFlBQVk7TUFDWixhQUFhO0tBQ2Q7O0lBRUQ7TUFDRSxnQkFBZ0I7TUFDaEIsc0JBQXNCO0tBQ3ZCOztJQUVEO01BQ0UsZ0JBQWdCO01BQ2hCLGlCQUFpQjtLQUNsQjs7RUFFSDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsbUNBQW1DO1FBQy9CLCtCQUErQjtJQUNuQyx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLFlBQVk7SUFDWixhQUFhO0lBQ2IsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtJQUNaLHVCQUF1QjtRQUNuQixvQkFBb0I7R0FDekI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7O0VBRUQ7SUFDRSxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsbUJBQW1CO0lBQ25CLHdCQUF3QjtHQUN6Qjs7RUFFRDtJQUNFLHVCQUF1QjtRQUNuQixvQkFBb0I7R0FDekI7O0VBRUQ7O01BRUk7O0VBRUo7SUFDRSxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0NBQXNDO0dBQ3ZDOztFQUVEO0lBQ0UsYUFBYTtJQUNiLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsbUNBQW1DO1FBQy9CLCtCQUErQjtHQUNwQztDQUNGXCIsXCJmaWxlXCI6XCJDdXN0b21lcnMuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIuaGVhZGVyQ29udGFpbmVyIHtcXG4gIGhlaWdodDogODB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uc2Nyb2xsVG9wIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uc2Nyb2xsVG9wIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLmhlYWRlcl90ZXh0IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICB3aWR0aDogNDElO1xcbn1cXG5cXG4udHJ1c3RlZEJ5VGl0bGUge1xcbiAgZm9udC1zaXplOiA2NHB4O1xcbiAgZm9udC1zaXplOiA0cmVtO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4vKiAudHJ1c3RlZEJ5X3RleHQge1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG59ICovXFxuXFxuLnRydXN0ZWRCeSB7XFxuICBjb2xvcjogI2YzNjtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbn1cXG5cXG4uaGVhZGVyX2JveF9jb250YWluZXIge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAxZnI7XFxuICBnYXA6IDMycHg7XFxufVxcblxcbi5ib3gge1xcbiAgd2lkdGg6IDMyMHB4O1xcbiAgaGVpZ2h0OiAzMjBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggNDBweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggNDBweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjEyKTtcXG4gIHBhZGRpbmc6IDU0cHggNDBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLmJveF9jb250ZW50c190YWcge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbn1cXG5cXG4uYm94X2NvbnRlbnRzIHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGZvbnQtc2l6ZTogM3JlbTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbjogMTZweCAwO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIC1vLXRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxufVxcblxcbi5wcm9maWxlYm94IHtcXG4gIHdpZHRoOiA3MnB4O1xcbiAgaGVpZ2h0OiA3MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnByb2ZpbGVib3ggaW1nIHtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4ubmV3dHdvcmtDb250YWluZXIge1xcbiAgaGVpZ2h0OiBhdXRvO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgcGFkZGluZzogMzJweCA2NHB4O1xcbiAgcGFkZGluZzogMnJlbSA0cmVtO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLm5ld3R3b3JrQ29udGFpbmVyIC5jb3VudHJ5aW1nIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5uZXd0d29ya0NvbnRhaW5lciAuY291bnRyeWltZyBpbWcge1xcbiAgd2lkdGg6IDQwMHB4O1xcbiAgaGVpZ2h0OiA0NTBweDtcXG59XFxuXFxuLm5ldHdvcmsge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxufVxcblxcbi5uZXR3b3JrVGl0bGUge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuXFxuLyogLm5ldHdvcmtfY29udGVudCBwIHtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBsaW5lLWhlaWdodDogMjtcXG59ICovXFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lcjIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uY3VzdG9tZXJfcmV2aWV3IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBwYWRkaW5nOiA2NHB4O1xcbiAgcGFkZGluZzogNHJlbTtcXG59XFxuXFxuLyogLmN1c3RvbWVyTG9nbyB7XFxuICBib3JkZXItcmFkaXVzOiA4JTtcXG59ICovXFxuXFxuLnNyaUNoYWl0YW55YVRleHQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4uYXV0aG9yIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjQzO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi10b3A6IDE2cHg7XFxufVxcblxcbi5hdXRob3JfdGl0bGUge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBtYXJnaW46IDEycHggMCA4cHggMDtcXG59XFxuXFxuLmN1c3RvbWVycyB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICBncmlkLXJvdy1nYXA6IDA7XFxuICBncmlkLWNvbHVtbi1nYXA6IDA7XFxufVxcblxcbi5jdXN0b21lcnMgLmNsaWVudCB7XFxuICAtbXMtZmxleDogMSAxO1xcbiAgICAgIGZsZXg6IDEgMTtcXG4gIGhlaWdodDogMTA2cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uY3VzdG9tZXJzIC5jbGllbnQ6bnRoLW9mLXR5cGUoZXZlbikge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLmN1c3RvbWVycyAuY2xpZW50IGltZyB7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIG1heC13aWR0aDogMjAwcHg7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgaGVpZ2h0OiAtbW96LWZpdC1jb250ZW50O1xcbiAgICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICBwYWRkaW5nOiAyNHB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgfVxcblxcbiAgLmhlYWRlcl90ZXh0IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAvKiAubmV0d29ya19jb250ZW50ID4gcCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsaW5lLWhlaWdodDogMS43MTtcXG4gIH0gKi9cXG5cXG4gIC8qIC50cnVzdGVkQnlfdGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjQycHg7XFxuICAgIG1hcmdpbjogOHB4IGF1dG8gMCBhdXRvO1xcblxcbiAgICBwIHtcXG4gICAgICBtYXJnaW46IDA7XFxuICAgIH1cXG5cXG4gICAgcDpudGgtY2hpbGQoMSkge1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLnRydXN0ZWRCeVRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLmhlYWRlcl9ib3hfY29udGFpbmVyIHtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIGdhcDogMjRweDtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICB9XFxuXFxuICAuYm94IHtcXG4gICAgd2lkdGg6IDI0MHB4O1xcbiAgICBoZWlnaHQ6IDI0MHB4O1xcbiAgICBwYWRkaW5nOiAzNnB4IDMycHg7XFxuICB9XFxuXFxuICAgIC5ib3ggaW1nIHtcXG4gICAgICB3aWR0aDogNTVweDtcXG4gICAgICBoZWlnaHQ6IDU1cHg7XFxuICAgIH1cXG5cXG4gICAgLmJveCAuYm94X2NvbnRlbnRzIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbWFyZ2luOiAxOHB4IDAgMTJweCAwO1xcbiAgICB9XFxuXFxuICAgIC5ib3ggLmJveF9jb250ZW50c190YWcge1xcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICB9XFxuXFxuICAubmV3dHdvcmtDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gIH1cXG5cXG4gIC5uZXd0d29ya0NvbnRhaW5lciAuY291bnRyeWltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLm5ldHdvcmsge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuICAubmV0d29ya1RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLmN1c3RvbWVyX3JldmlldyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHggMjRweDtcXG4gIH1cXG5cXG4gIC5hdXRob3Ige1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC8qIC5jdXN0b21lckxvZ28ge1xcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICB9ICovXFxuXFxuICAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgIHdpZHRoOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNztcXG4gIH1cXG5cXG4gIC5jdXN0b21lcnMge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICB9XFxuXFxuICAuY3VzdG9tZXJzIGltZyB7XFxuICAgIHdpZHRoOiAxMjZweDtcXG4gICAgaGVpZ2h0OiA2OHB4O1xcbiAgfVxcblxcbiAgLmN1c3RvbWVyc19jb250YWluZXIyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uLXJldmVyc2U7XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uLXJldmVyc2U7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJoZWFkZXJDb250YWluZXJcIjogXCJDdXN0b21lcnMtaGVhZGVyQ29udGFpbmVyLTNFQTk5XCIsXG5cdFwic2Nyb2xsVG9wXCI6IFwiQ3VzdG9tZXJzLXNjcm9sbFRvcC0ycjNxYVwiLFxuXHRcImhlYWRlcl90ZXh0XCI6IFwiQ3VzdG9tZXJzLWhlYWRlcl90ZXh0LTFOY18xXCIsXG5cdFwidHJ1c3RlZEJ5VGl0bGVcIjogXCJDdXN0b21lcnMtdHJ1c3RlZEJ5VGl0bGUtM3BQVEhcIixcblx0XCJ0cnVzdGVkQnlcIjogXCJDdXN0b21lcnMtdHJ1c3RlZEJ5LTNGdUVrXCIsXG5cdFwiaGVhZGVyX2JveF9jb250YWluZXJcIjogXCJDdXN0b21lcnMtaGVhZGVyX2JveF9jb250YWluZXItM0FUYU9cIixcblx0XCJib3hcIjogXCJDdXN0b21lcnMtYm94LTJnMkROXCIsXG5cdFwiYm94X2NvbnRlbnRzX3RhZ1wiOiBcIkN1c3RvbWVycy1ib3hfY29udGVudHNfdGFnLTEtN09FXCIsXG5cdFwiYm94X2NvbnRlbnRzXCI6IFwiQ3VzdG9tZXJzLWJveF9jb250ZW50cy0xXzZTeVwiLFxuXHRcInByb2ZpbGVib3hcIjogXCJDdXN0b21lcnMtcHJvZmlsZWJveC1ZanhEOFwiLFxuXHRcIm5ld3R3b3JrQ29udGFpbmVyXCI6IFwiQ3VzdG9tZXJzLW5ld3R3b3JrQ29udGFpbmVyLVdVc1NyXCIsXG5cdFwiY291bnRyeWltZ1wiOiBcIkN1c3RvbWVycy1jb3VudHJ5aW1nLTE4ckVtXCIsXG5cdFwibmV0d29ya1wiOiBcIkN1c3RvbWVycy1uZXR3b3JrLTNPUW9HXCIsXG5cdFwibmV0d29ya1RpdGxlXCI6IFwiQ3VzdG9tZXJzLW5ldHdvcmtUaXRsZS1lN2JMLVwiLFxuXHRcImN1c3RvbWVyc19jb250YWluZXJcIjogXCJDdXN0b21lcnMtY3VzdG9tZXJzX2NvbnRhaW5lci0yMmNlRFwiLFxuXHRcImN1c3RvbWVyc19jb250YWluZXIyXCI6IFwiQ3VzdG9tZXJzLWN1c3RvbWVyc19jb250YWluZXIyLVg3ZnctXCIsXG5cdFwiY3VzdG9tZXJfcmV2aWV3XCI6IFwiQ3VzdG9tZXJzLWN1c3RvbWVyX3Jldmlldy1jMVVzS1wiLFxuXHRcInNyaUNoYWl0YW55YVRleHRcIjogXCJDdXN0b21lcnMtc3JpQ2hhaXRhbnlhVGV4dC0yRkFkYVwiLFxuXHRcImF1dGhvclwiOiBcIkN1c3RvbWVycy1hdXRob3ItMVQwRXdcIixcblx0XCJhdXRob3JfdGl0bGVcIjogXCJDdXN0b21lcnMtYXV0aG9yX3RpdGxlLWFpZWFkXCIsXG5cdFwiY3VzdG9tZXJzXCI6IFwiQ3VzdG9tZXJzLWN1c3RvbWVycy0xbmdPdFwiLFxuXHRcImNsaWVudFwiOiBcIkN1c3RvbWVycy1jbGllbnQtZEhpMW1cIlxufTsiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHtcbiAgQ0xJRU5UU19TRVQxLFxuICBDTElFTlRTX1NFVDIsXG4gIENVU1RPTUVSU19TVEFUUyxcbiAgQ1VTVE9NRVJfUkVWSUVXLFxufSBmcm9tICcuLi9HZXRSYW5rc0NvbnN0YW50cyc7XG5pbXBvcnQgcyBmcm9tICcuL0N1c3RvbWVycy5zY3NzJztcblxuY2xhc3MgQ3VzdG9tZXJzIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5oYW5kbGVTY3JvbGwoKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgfVxuXG4gIGhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LnNjcm9sbFkgPiA1MDApIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiB0cnVlLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVTY3JvbGxUb3AgPSAoKSA9PiB7XG4gICAgd2luZG93LnNjcm9sbFRvKHtcbiAgICAgIHRvcDogMCxcbiAgICAgIGJlaGF2aW9yOiAnc21vb3RoJyxcbiAgICB9KTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgIH0pO1xuICB9O1xuXG4gIGRpc3BsYXlTY3JvbGxUb1RvcCA9ICgpID0+IHtcbiAgICBjb25zdCB7IHNob3dTY3JvbGwgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIHNob3dTY3JvbGwgJiYgKFxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLnNjcm9sbFRvcH1cbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbFRvcCgpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS9zY3JvbGxUb3Auc3ZnXCIgYWx0PVwic2Nyb2xsVG9wXCIgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICBkaXNwbGF5VHJ1c3RlZEJ5ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlckNvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJfdGV4dH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRydXN0ZWRCeVRpdGxlfT5cbiAgICAgICAgICBUcnVzdGVkIGJ5XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudHJ1c3RlZEJ5fT4gMTAwKyBJbnN0aXR1dGlvbnM8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT17cy50cnVzdGVkQnlfdGV4dH0+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsXG4gICAgICAgICAgICBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlXG4gICAgICAgICAgICBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW1cbiAgICAgICAgICA8L3A+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW1cbiAgICAgICAgICAgIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkXG4gICAgICAgICAgICBlZ2V0IHBlbGxlbnRlc3F1ZS5cbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PiAqL31cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyX2JveF9jb250YWluZXJ9PlxuICAgICAgICB7Q1VTVE9NRVJTX1NUQVRTLm1hcChpdGVtID0+IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3h9PlxuICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PXtpdGVtLmxhYmVsfSBoZWlnaHQ9XCI2NHB4XCIgd2lkdGg9XCI2NHB4XCIgLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJveF9jb250ZW50c30+e2l0ZW0ubnVtYmVyfTwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm94X2NvbnRlbnRzX3RhZ30+e2l0ZW0udGV4dH08L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5TmV0d29yayA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5uZXd0d29ya0NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb3VudHJ5aW1nfT5cbiAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvY3VzdG9tZXJzL0dyb3VwIDExMjguanBnXCIgYWx0PVwiXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubmV0d29ya30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm5ldHdvcmtUaXRsZX0+XG4gICAgICAgICAgQSBsYXJnZSBuZXR3b3JrIHNlcnZpY2luZyA8YnIgLz5cbiAgICAgICAgICB0aGUgd2hvbGUgY291bnRyeVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgey8qIDxkaXYgY2xhc3NOYW1lPXtzLm5ldHdvcmtfY29udGVudH0+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsXG4gICAgICAgICAgICBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlXG4gICAgICAgICAgICBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsXG4gICAgICAgICAgICBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmhcbiAgICAgICAgICAgIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuXG4gICAgICAgICAgPC9wPlxuICAgICAgICA8L2Rpdj4gKi99XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Q3VzdG9tZXJzUmV2aWV3ID0gZGF0YSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJfcmV2aWV3fT5cbiAgICAgIDxkaXY+XG4gICAgICAgIHsvKiA8aW1nIHNyYz17ZGF0YS5sb2dvfSBhbHQ9e2RhdGEubGFiZWx9IGNsYXNzTmFtZT17cy5jdXN0b21lckxvZ299IC8+ICovfVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zcmlDaGFpdGFueWFUZXh0fT57ZGF0YS50ZXh0fTwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdXRob3J9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcm9maWxlYm94fT5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaWNvbnMvcGVyc29uX2JsdWUuc3ZnXCIgYWx0PVwicHJvZmlsZVwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF1dGhvcl90aXRsZX0+e2RhdGEubmFtZX08L2Rpdj5cbiAgICAgICAgICA8ZGl2PntkYXRhLmRlc2lnbmF0aW9ufTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDdXN0b21lcnNHcmlkID0gZGF0YSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJzfT5cbiAgICAgIHtkYXRhLm1hcChpdGVtID0+IChcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY2xpZW50fT5cbiAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29ufSBhbHQ9e2l0ZW0ubGFiZWx9IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUN1c3RvbWVyczEgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJzX2NvbnRhaW5lcn0+XG4gICAgICB7dGhpcy5kaXNwbGF5Q3VzdG9tZXJzUmV2aWV3KENVU1RPTUVSX1JFVklFV1swXSl9XG4gICAgICB7dGhpcy5kaXNwbGF5Q3VzdG9tZXJzR3JpZChDTElFTlRTX1NFVDEpfVxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDdXN0b21lcnMyID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmN1c3RvbWVyc19jb250YWluZXIyfT5cbiAgICAgIHt0aGlzLmRpc3BsYXlDdXN0b21lcnNHcmlkKENMSUVOVFNfU0VUMil9XG4gICAgICB7dGhpcy5kaXNwbGF5Q3VzdG9tZXJzUmV2aWV3KENVU1RPTUVSX1JFVklFV1sxXSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlUcnVzdGVkQnkoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5TmV0d29yaygpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlDdXN0b21lcnMxKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUN1c3RvbWVyczIoKX08L2Rpdj5cbiAgICAgICAge3RoaXMuZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoQ3VzdG9tZXJzKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ3VzdG9tZXJzLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ3VzdG9tZXJzLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DdXN0b21lcnMuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dC9MYXlvdXQnO1xuaW1wb3J0IEN1c3RvbWVycyBmcm9tICcuL0N1c3RvbWVycyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogXCJDdXN0b21lcnMgLSBFZ25pZnkncyB0cnVzdGVkIG9ubGluZSBpbnN0aXR1dGVzIGFuZCBzdHVkZW50c1wiLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnT3VyIG9ubGluZSBlZHVjYXRpb24gcGxhdGZvcm0gaXMgZW5hYmxpbmcgdHV0b3JzIGFuZCBjb2FjaGluZyBpbnN0aXR1dGVzIHRvIGdvIG9ubGluZSBpbnN0YW50bHkuJyxcbiAgICBjaHVua3M6IFsnQ3VzdG9tZXJzJ10sXG4gICAga2V5d29yZHM6ICdzdHVkZW50cywgdGVhY2hlcnMnLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dCBmb290ZXJBc2g+XG4gICAgICAgIDxDdXN0b21lcnMgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBZUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQXpCQTtBQTJCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFuQ0E7QUFxQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFwREE7QUFzREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEvRUE7QUFzRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE3RkE7QUE2R0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXpIQTtBQStIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbklBO0FBeUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUExSUE7QUFnSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQS9JQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF5SUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFsS0E7QUFDQTtBQW1LQTs7Ozs7OztBQzlLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJBO0FBWUE7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=