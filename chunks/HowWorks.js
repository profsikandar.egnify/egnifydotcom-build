require("source-map-support").install();
exports.ids = ["HowWorks"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/HowWorks/HowWorks.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".HowWorks-root-2OdTt {\n  color: #25282b;\n}\n\n.HowWorks-maxContainer-3apfi {\n  max-width: 1700px;\n}\n\n.HowWorks-headerContainer-2anVa {\n  padding: 96px 64px;\n  padding: 6rem 4rem;\n  position: relative;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-maxContainer-3apfi {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: auto;\n}\n\n.HowWorks-triangle-1wWJT {\n  position: absolute;\n  top: 14.7%;\n  left: 7.6%;\n  width: 60px;\n  height: 70px;\n}\n\n.HowWorks-chemistry-Wj8pU {\n  position: absolute;\n  bottom: 23.5%;\n  left: 7.6%;\n  width: 74px;\n  height: 94px;\n}\n\n.HowWorks-maths-Smakv {\n  position: absolute;\n  bottom: 16.8%;\n  right: 15.7%;\n  width: 90px;\n  height: 62px;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-header_title-1pe4e {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: center;\n  margin-bottom: 24px;\n  font-weight: bold;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-header_content-6V6mN {\n  font-size: 20px;\n  line-height: 32px;\n  margin-bottom: 40px;\n  text-align: center;\n  max-width: 670px;\n}\n\n.HowWorks-scrollTop-219ko {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.HowWorks-scrollTop-219ko img {\n    width: 100%;\n    height: 100%;\n  }\n\n.HowWorks-buttonWrapper-1y5jj {\n  margin: 0 auto;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  max-width: 600px;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-requestDemo-2m23S {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  min-width: 240px;\n  min-width: 15rem;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  margin: 0 0.5rem 0 2rem;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU img {\n  width: 32px;\n  width: 2rem;\n  height: 32px;\n  height: 2rem;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-numberWrapper-R4yQ1 {\n  width: 24px;\n  height: 24px;\n  border-radius: 50%;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-right: 20px;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- span {\n  margin: 16px 0;\n  margin: 1rem 0;\n  font-size: 16px;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-numberWrapper-R4yQ1 span {\n  font-size: 14px;\n  color: #fff;\n}\n\n.HowWorks-navigator-1fc00 {\n  width: 100%;\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  position: relative;\n  margin: 0 auto;\n  border-top: 1px solid #d9d9d9;\n  padding: 80px 0;\n  padding: 5rem 0;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- {\n  width: 20%;\n  height: 50vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  position: -webkit-sticky;\n  position: sticky;\n  top: 164px;\n  border-right: 1px solid #d9d9d9;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 32px;\n  color: #25282b;\n  -webkit-transition: all 50ms ease-in-out;\n  -o-transition: all 50ms ease-in-out;\n  transition: all 50ms ease-in-out;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl.HowWorks-active-2Kxvn {\n  font-weight: bold;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1 {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background-color: #e5e5e5;\n  margin-right: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1.HowWorks-activeNumber-2c4oL {\n  background-color: #f36;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1 span {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1.HowWorks-activeNumber-2c4oL span {\n  color: #fff;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-scrollContainer-1JJ86 {\n  width: 80%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n/* .box {\n  width: 400px;\n  height: 400px;\n  margin: 32px auto;\n  border: 1px solid #f36;\n} */\n\n.HowWorks-section-3falQ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  width: 100%;\n  max-width: 800px;\n  margin: 0 auto;\n  padding: 80px 0;\n  border-bottom: 1px solid #d9d9d9;\n}\n\n.HowWorks-section-3falQ:first-child {\n  padding-top: 0;\n}\n\n.HowWorks-section-3falQ:last-child {\n  border-bottom: none;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O {\n  display: none;\n  width: 100%;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-title-2A7hc {\n  font-size: 14px;\n  line-height: 24px;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-arrowWrapper-2aqeX {\n  width: 24px;\n  height: 24px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 56px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-profile-2s6Ie {\n  width: 10%;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-grayBox-3y0if {\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n  background-color: #f0f0f0;\n  margin-right: 24px;\n  margin-top: 6px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-title-2A7hc {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-bottom: 12px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-text-18Q3A {\n  max-width: 500px;\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.HowWorks-section-3falQ .HowWorks-playerSection-1vIFi {\n  width: 100%;\n  height: 350px;\n  -webkit-box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n}\n\n@media only screen and (max-width: 990px) {\n  .HowWorks-headerContainer-2anVa {\n    padding: 40px 16px 80px;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-maxContainer-3apfi {\n    max-width: 550px;\n    margin: auto;\n  }\n\n  .HowWorks-triangle-1wWJT {\n    width: 34px;\n    height: 28px;\n    left: 6.1%;\n    top: 22.6%;\n  }\n\n  .HowWorks-chemistry-Wj8pU {\n    width: 36px;\n    height: 44px;\n    left: 2.5%;\n    top: 47.5%;\n  }\n\n  .HowWorks-maths-Smakv {\n    width: 44px;\n    height: 20px;\n    bottom: 17.6%;\n    right: 9%;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-header_title-1pe4e {\n    font-size: 32px;\n    line-height: 48px;\n    margin-bottom: 12px;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-header_content-6V6mN {\n    font-size: 16px;\n    line-height: 24px;\n    margin-bottom: 48px;\n    max-width: 300px;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj .HowWorks-requestDemo-2m23S {\n    margin-bottom: 40px;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU {\n    margin-left: 0;\n  }\n\n  .HowWorks-navigator-1fc00 {\n    padding: 0 16px;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi {\n    border-top: none;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-scrollContainer-1JJ86 {\n    width: 100%;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- {\n    display: none;\n  }\n\n  .HowWorks-section-3falQ {\n    -ms-flex-align: start;\n        align-items: flex-start;\n    max-width: 600px;\n    border-bottom: none;\n    padding: 0 0 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n    -ms-flex-align: center;\n        align-items: center;\n    padding: 16px 0;\n    margin-bottom: 0;\n    border-bottom: 1px solid #d9d9d9;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    margin-bottom: 32px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd.HowWorks-hide-JGQHT {\n    display: none;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-profile-2s6Ie .HowWorks-grayBox-3y0if {\n    width: 40px;\n    height: 40px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-title-2A7hc {\n    font-size: 24px;\n    line-height: 32px;\n    margin: 12px 0 8px 0;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-text-18Q3A {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-playerSection-1vIFi {\n    margin-bottom: 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-playerSection-1vIFi.HowWorks-hide-JGQHT {\n    display: none;\n  }\n\n  .HowWorks-scrollTop-219ko {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/HowWorks.scss"],"names":[],"mappings":"AAAA;EACE,eAAe;CAChB;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,WAAW;EACX,YAAY;EACZ,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,WAAW;EACX,YAAY;EACZ,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,aAAa;EACb,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;EACnB,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,oBAAoB;EACpB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,iBAAiB;EACjB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,gBAAgB;EAChB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,0BAA0B;EAC1B,qBAAqB;EACrB,wBAAwB;EACxB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;CACpB;;AAED;EACE,eAAe;EACf,eAAe;EACf,gBAAgB;CACjB;;AAED;EACE,gBAAgB;EAChB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,gBAAgB;CACjB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,eAAe;EACf,8BAA8B;EAC9B,gBAAgB;EAChB,gBAAgB;CACjB;;AAED;EACE,WAAW;EACX,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,yBAAyB;EACzB,iBAAiB;EACjB,WAAW;EACX,gCAAgC;CACjC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;EACpB,eAAe;EACf,yCAAyC;EACzC,oCAAoC;EACpC,iCAAiC;CAClC;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;CACb;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,gBAAgB;EAChB,iCAAiC;CAClC;;AAED;EACE,eAAe;CAChB;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,cAAc;EACd,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;CACrB;;AAED;EACE,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,gBAAgB;CACjB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,cAAc;EACd,qDAAqD;UAC7C,6CAA6C;CACtD;;AAED;EACE;IACE,wBAAwB;GACzB;;EAED;IACE,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;IACb,WAAW;IACX,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,WAAW;IACX,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,cAAc;IACd,UAAU;GACX;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;IACpB,iBAAiB;GAClB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,eAAe;GAChB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,YAAY;GACb;;EAED;IACE,cAAc;GACf;;EAED;IACE,sBAAsB;QAClB,wBAAwB;IAC5B,iBAAiB;IACjB,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,+BAA+B;IACnC,uBAAuB;QACnB,oBAAoB;IACxB,gBAAgB;IAChB,iBAAiB;IACjB,iCAAiC;GAClC;;EAED;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,oBAAoB;GACrB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;CACF","file":"HowWorks.scss","sourcesContent":[".root {\n  color: #25282b;\n}\n\n.maxContainer {\n  max-width: 1700px;\n}\n\n.headerContainer {\n  padding: 96px 64px;\n  padding: 6rem 4rem;\n  position: relative;\n}\n\n.headerContainer .maxContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: auto;\n}\n\n.triangle {\n  position: absolute;\n  top: 14.7%;\n  left: 7.6%;\n  width: 60px;\n  height: 70px;\n}\n\n.chemistry {\n  position: absolute;\n  bottom: 23.5%;\n  left: 7.6%;\n  width: 74px;\n  height: 94px;\n}\n\n.maths {\n  position: absolute;\n  bottom: 16.8%;\n  right: 15.7%;\n  width: 90px;\n  height: 62px;\n}\n\n.headerContainer .header_title {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: center;\n  margin-bottom: 24px;\n  font-weight: bold;\n}\n\n.headerContainer .header_content {\n  font-size: 20px;\n  line-height: 32px;\n  margin-bottom: 40px;\n  text-align: center;\n  max-width: 670px;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.buttonWrapper {\n  margin: 0 auto;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  max-width: 600px;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.buttonWrapper .requestDemo {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  min-width: 240px;\n  min-width: 15rem;\n}\n\n.buttonWrapper .whatsappwrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.buttonWrapper .whatsappwrapper .whatsapp {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  margin: 0 0.5rem 0 2rem;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.buttonWrapper .whatsappwrapper .whatsapp img {\n  width: 32px;\n  width: 2rem;\n  height: 32px;\n  height: 2rem;\n}\n\n.section .mobile_section .left .numberWrapper {\n  width: 24px;\n  height: 24px;\n  border-radius: 50%;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-right: 20px;\n}\n\n.navigator .maxContainer .sidenav span {\n  margin: 16px 0;\n  margin: 1rem 0;\n  font-size: 16px;\n}\n\n.section .mobile_section .left .numberWrapper span {\n  font-size: 14px;\n  color: #fff;\n}\n\n.navigator {\n  width: 100%;\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.navigator .maxContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  position: relative;\n  margin: 0 auto;\n  border-top: 1px solid #d9d9d9;\n  padding: 80px 0;\n  padding: 5rem 0;\n}\n\n.navigator .maxContainer .sidenav {\n  width: 20%;\n  height: 50vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  position: -webkit-sticky;\n  position: sticky;\n  top: 164px;\n  border-right: 1px solid #d9d9d9;\n}\n\n.navigator .maxContainer .sidenav .navItem {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 32px;\n  color: #25282b;\n  -webkit-transition: all 50ms ease-in-out;\n  -o-transition: all 50ms ease-in-out;\n  transition: all 50ms ease-in-out;\n}\n\n.navigator .maxContainer .sidenav .navItem.active {\n  font-weight: bold;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background-color: #e5e5e5;\n  margin-right: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper.activeNumber {\n  background-color: #f36;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper span {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper.activeNumber span {\n  color: #fff;\n}\n\n.navigator .maxContainer .scrollContainer {\n  width: 80%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n/* .box {\n  width: 400px;\n  height: 400px;\n  margin: 32px auto;\n  border: 1px solid #f36;\n} */\n\n.section {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  width: 100%;\n  max-width: 800px;\n  margin: 0 auto;\n  padding: 80px 0;\n  border-bottom: 1px solid #d9d9d9;\n}\n\n.section:first-child {\n  padding-top: 0;\n}\n\n.section:last-child {\n  border-bottom: none;\n}\n\n.section .mobile_section {\n  display: none;\n  width: 100%;\n}\n\n.section .mobile_section .left {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.section .mobile_section .left .title {\n  font-size: 14px;\n  line-height: 24px;\n}\n\n.section .mobile_section .arrowWrapper {\n  width: 24px;\n  height: 24px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.section .section_content {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 56px;\n}\n\n.section .section_content .profile {\n  width: 10%;\n}\n\n.section .section_content .grayBox {\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n  background-color: #f0f0f0;\n  margin-right: 24px;\n  margin-top: 6px;\n}\n\n.section .section_content .content_box {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.section .section_content .content_box .title {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-bottom: 12px;\n}\n\n.section .section_content .content_box .text {\n  max-width: 500px;\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.section .playerSection {\n  width: 100%;\n  height: 350px;\n  -webkit-box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n}\n\n@media only screen and (max-width: 990px) {\n  .headerContainer {\n    padding: 40px 16px 80px;\n  }\n\n  .headerContainer .maxContainer {\n    max-width: 550px;\n    margin: auto;\n  }\n\n  .triangle {\n    width: 34px;\n    height: 28px;\n    left: 6.1%;\n    top: 22.6%;\n  }\n\n  .chemistry {\n    width: 36px;\n    height: 44px;\n    left: 2.5%;\n    top: 47.5%;\n  }\n\n  .maths {\n    width: 44px;\n    height: 20px;\n    bottom: 17.6%;\n    right: 9%;\n  }\n\n  .headerContainer .header_title {\n    font-size: 32px;\n    line-height: 48px;\n    margin-bottom: 12px;\n  }\n\n  .headerContainer .header_content {\n    font-size: 16px;\n    line-height: 24px;\n    margin-bottom: 48px;\n    max-width: 300px;\n  }\n\n  .buttonWrapper {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .buttonWrapper .requestDemo {\n    margin-bottom: 40px;\n  }\n\n  .buttonWrapper .whatsappwrapper .whatsapp {\n    margin-left: 0;\n  }\n\n  .navigator {\n    padding: 0 16px;\n  }\n\n  .navigator .maxContainer {\n    border-top: none;\n  }\n\n  .navigator .maxContainer .scrollContainer {\n    width: 100%;\n  }\n\n  .navigator .maxContainer .sidenav {\n    display: none;\n  }\n\n  .section {\n    -ms-flex-align: start;\n        align-items: flex-start;\n    max-width: 600px;\n    border-bottom: none;\n    padding: 0 0 24px;\n  }\n\n  .section .mobile_section {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n    -ms-flex-align: center;\n        align-items: center;\n    padding: 16px 0;\n    margin-bottom: 0;\n    border-bottom: 1px solid #d9d9d9;\n  }\n\n  .section .section_content {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    margin-bottom: 32px;\n  }\n\n  .section .section_content.hide {\n    display: none;\n  }\n\n  .section .section_content .profile .grayBox {\n    width: 40px;\n    height: 40px;\n  }\n\n  .section .section_content .content_box .title {\n    font-size: 24px;\n    line-height: 32px;\n    margin: 12px 0 8px 0;\n  }\n\n  .section .section_content .content_box .text {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .section .playerSection {\n    margin-bottom: 24px;\n  }\n\n  .section .playerSection.hide {\n    display: none;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "HowWorks-root-2OdTt",
	"maxContainer": "HowWorks-maxContainer-3apfi",
	"headerContainer": "HowWorks-headerContainer-2anVa",
	"triangle": "HowWorks-triangle-1wWJT",
	"chemistry": "HowWorks-chemistry-Wj8pU",
	"maths": "HowWorks-maths-Smakv",
	"header_title": "HowWorks-header_title-1pe4e",
	"header_content": "HowWorks-header_content-6V6mN",
	"scrollTop": "HowWorks-scrollTop-219ko",
	"buttonWrapper": "HowWorks-buttonWrapper-1y5jj",
	"requestDemo": "HowWorks-requestDemo-2m23S",
	"whatsappwrapper": "HowWorks-whatsappwrapper-2B9Bl",
	"whatsapp": "HowWorks-whatsapp-2hvxU",
	"section": "HowWorks-section-3falQ",
	"mobile_section": "HowWorks-mobile_section-2Du5O",
	"left": "HowWorks-left-1AuFu",
	"numberWrapper": "HowWorks-numberWrapper-R4yQ1",
	"navigator": "HowWorks-navigator-1fc00",
	"sidenav": "HowWorks-sidenav-1EI1-",
	"navItem": "HowWorks-navItem-zChdl",
	"active": "HowWorks-active-2Kxvn",
	"activeNumber": "HowWorks-activeNumber-2c4oL",
	"scrollContainer": "HowWorks-scrollContainer-1JJ86",
	"title": "HowWorks-title-2A7hc",
	"arrowWrapper": "HowWorks-arrowWrapper-2aqeX",
	"section_content": "HowWorks-section_content-TbMpd",
	"profile": "HowWorks-profile-2s6Ie",
	"grayBox": "HowWorks-grayBox-3y0if",
	"content_box": "HowWorks-content_box-1vQ-h",
	"text": "HowWorks-text-18Q3A",
	"playerSection": "HowWorks-playerSection-1vIFi",
	"hide": "HowWorks-hide-JGQHT"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/HowWorks.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/HowWorks/HowWorks.scss");
/* harmony import */ var _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/HowWorks.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class HowWorks extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "handleResize", () => {
      let {
        isMobile
      } = this.state;

      if (window.innerWidth > 990) {
        isMobile = false;
      } else {
        isMobile = true;
      }

      this.setState({
        isMobile
      });
    });

    _defineProperty(this, "handleStepToggle", (title, status) => {
      const {
        openList
      } = this.state;

      if (status) {
        const ind = openList.findIndex(ele => ele === title);

        if (ind > -1) {
          openList.splice(ind, 1);
        }

        this.setState({
          openList
        });
      } else {
        openList.push(title);
        this.setState({
          openList
        });
      }
    });

    _defineProperty(this, "observeElelements", () => {
      const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          const nodeId = entry.target.getAttribute('id');
          const link = document.querySelector(`div.${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sidenav} a[href="#${nodeId}"]`);

          if (entry.isIntersecting) {
            link.classList.add(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active);
            link.childNodes[0].classList.add(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.activeNumber);
          } else {
            link.classList.remove(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active);
            link.childNodes[0].classList.remove(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.activeNumber);
          }
        });
      }, {
        threshold: 0.55
      });
      const ele = document.getElementById(`list`).querySelectorAll(`div[id]`);
      ele.forEach(element => {
        observer.observe(element);
      });
    });

    _defineProperty(this, "displayHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maxContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.header_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }, "How egnify works"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.header_content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 112
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.buttonWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/request-demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.requestDemo,
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, "START FREE TRIAL")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsappwrapper,
      href: "https://api.whatsapp.com/send?phone=919949523849&text=Hi GetRanks, I would like to know more about your Online Platform.",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsapp,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/whatsapp_logo.svg",
      alt: "whatsapp",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.triangle,
      src: "/images/icons/subject_icons/triangle.svg",
      alt: "triangle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.chemistry,
      src: "/images/icons/subject_icons/chemistry.svg",
      alt: "triangle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maths,
      src: "/images/icons/subject_icons/scale.svg",
      alt: "maths",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143
      },
      __self: this
    })));

    _defineProperty(this, "displaySteps", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.navigator,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maxContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 153
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sidenav,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 154
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS"].map((item, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: `#${item.title}`,
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.navItem,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.numberWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }, index + 1)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, item.title)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      id: "list",
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.scrollContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS"].map((box, index) => {
      const {
        openList,
        isMobile
      } = this.state;
      const isOpen = openList.includes(box.title);
      const contentClass = isOpen ? `${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_content}` : `${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_content} ${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.hide}`;
      const playerClass = isOpen ? `${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playerSection}` : `${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playerSection} ${_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.hide}`;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: box.title,
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 175
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobile_section,
        role: "presentation",
        onClick: () => {
          if (isMobile) {
            this.handleStepToggle(box.title, isOpen);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 176
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.left,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 185
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.numberWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 186
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      }, index + 1)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, box.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.arrowWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: isOpen ? `/images/icons/chevron-up.svg` : `/images/icons/chevron-down.svg`,
        alt: "arrow",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: contentClass,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 202
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.profile,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.grayBox,
        src: box.img,
        alt: box.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content_box,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, box.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      }, box.content))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: playerClass,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }));
    })))));

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }));
    });

    this.state = {
      openList: [..._GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS_TITLES"]],
      isMobile: false,
      showScroll: false
    };
  }

  componentDidMount() {
    this.handleResize();
    this.observeElelements();
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
    document.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    document.removeEventListener('resize', this.handleResize);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      id: "root",
      className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 239
      },
      __self: this
    }, this.displayHeader(), this.displaySteps(), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(HowWorks));

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/HowWorks.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/HowWorks/HowWorks.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _HowWorks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/HowWorks/HowWorks.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/index.js";




async function action() {
  return {
    title: 'Egnify makes teaching easy',
    chunks: ['HowWorks'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HowWorks__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0hvd1dvcmtzLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvSG93V29ya3MvSG93V29ya3Muc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Ib3dXb3Jrcy9Ib3dXb3Jrcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Ib3dXb3Jrcy9Ib3dXb3Jrcy5zY3NzPzg1ZTkiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvSG93V29ya3MvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5Ib3dXb3Jrcy1yb290LTJPZFR0IHtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIHtcXG4gIG1heC13aWR0aDogMTcwMHB4O1xcbn1cXG5cXG4uSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIHtcXG4gIHBhZGRpbmc6IDk2cHggNjRweDtcXG4gIHBhZGRpbmc6IDZyZW0gNHJlbTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLkhvd1dvcmtzLWhlYWRlckNvbnRhaW5lci0yYW5WYSAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5Ib3dXb3Jrcy10cmlhbmdsZS0xd1dKVCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDE0LjclO1xcbiAgbGVmdDogNy42JTtcXG4gIHdpZHRoOiA2MHB4O1xcbiAgaGVpZ2h0OiA3MHB4O1xcbn1cXG5cXG4uSG93V29ya3MtY2hlbWlzdHJ5LVdqOHBVIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMjMuNSU7XFxuICBsZWZ0OiA3LjYlO1xcbiAgd2lkdGg6IDc0cHg7XFxuICBoZWlnaHQ6IDk0cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1tYXRocy1TbWFrdiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IDE2LjglO1xcbiAgcmlnaHQ6IDE1LjclO1xcbiAgd2lkdGg6IDkwcHg7XFxuICBoZWlnaHQ6IDYycHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmEgLkhvd1dvcmtzLWhlYWRlcl90aXRsZS0xcGU0ZSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNjBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLkhvd1dvcmtzLWhlYWRlckNvbnRhaW5lci0yYW5WYSAuSG93V29ya3MtaGVhZGVyX2NvbnRlbnQtNlY2bU4ge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWF4LXdpZHRoOiA2NzBweDtcXG59XFxuXFxuLkhvd1dvcmtzLXNjcm9sbFRvcC0yMTlrbyB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLkhvd1dvcmtzLXNjcm9sbFRvcC0yMTlrbyBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5Ib3dXb3Jrcy1idXR0b25XcmFwcGVyLTF5NWpqIHtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiAuSG93V29ya3MtcmVxdWVzdERlbW8tMm0yM1Mge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbiAgbWluLXdpZHRoOiAyNDBweDtcXG4gIG1pbi13aWR0aDogMTVyZW07XFxufVxcblxcbi5Ib3dXb3Jrcy1idXR0b25XcmFwcGVyLTF5NWpqIC5Ib3dXb3Jrcy13aGF0c2FwcHdyYXBwZXItMkI5Qmwge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amogLkhvd1dvcmtzLXdoYXRzYXBwd3JhcHBlci0yQjlCbCAuSG93V29ya3Mtd2hhdHNhcHAtMmh2eFUge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgbWFyZ2luOiAwIDAuNXJlbSAwIDJyZW07XFxuICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amogLkhvd1dvcmtzLXdoYXRzYXBwd3JhcHBlci0yQjlCbCAuSG93V29ya3Mtd2hhdHNhcHAtMmh2eFUgaW1nIHtcXG4gIHdpZHRoOiAzMnB4O1xcbiAgd2lkdGg6IDJyZW07XFxuICBoZWlnaHQ6IDMycHg7XFxuICBoZWlnaHQ6IDJyZW07XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyAuSG93V29ya3MtbGVmdC0xQXVGdSAuSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMSB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSBzcGFuIHtcXG4gIG1hcmdpbjogMTZweCAwO1xcbiAgbWFyZ2luOiAxcmVtIDA7XFxuICBmb250LXNpemU6IDE2cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyAuSG93V29ya3MtbGVmdC0xQXVGdSAuSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMSBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogMCA2NHB4O1xcbiAgcGFkZGluZzogMCA0cmVtO1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q5ZDlkOTtcXG4gIHBhZGRpbmc6IDgwcHggMDtcXG4gIHBhZGRpbmc6IDVyZW0gMDtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIHtcXG4gIHdpZHRoOiAyMCU7XFxuICBoZWlnaHQ6IDUwdmg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgcG9zaXRpb246IC13ZWJraXQtc3RpY2t5O1xcbiAgcG9zaXRpb246IHN0aWNreTtcXG4gIHRvcDogMTY0cHg7XFxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkgLkhvd1dvcmtzLXNpZGVuYXYtMUVJMS0gLkhvd1dvcmtzLW5hdkl0ZW0tekNoZGwge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgNTBtcyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IGFsbCA1MG1zIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogYWxsIDUwbXMgZWFzZS1pbi1vdXQ7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSAuSG93V29ya3MtbmF2SXRlbS16Q2hkbC5Ib3dXb3Jrcy1hY3RpdmUtMkt4dm4ge1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSAuSG93V29ya3MtbmF2SXRlbS16Q2hkbCAuSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMSB7XFxuICB3aWR0aDogMzBweDtcXG4gIGhlaWdodDogMzBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlNWU1ZTU7XFxuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSAuSG93V29ya3MtbmF2SXRlbS16Q2hkbCAuSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMS5Ib3dXb3Jrcy1hY3RpdmVOdW1iZXItMmM0b0wge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIC5Ib3dXb3Jrcy1uYXZJdGVtLXpDaGRsIC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSAuSG93V29ya3MtbmF2SXRlbS16Q2hkbCAuSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMS5Ib3dXb3Jrcy1hY3RpdmVOdW1iZXItMmM0b0wgc3BhbiB7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zY3JvbGxDb250YWluZXItMUpKODYge1xcbiAgd2lkdGg6IDgwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcblxcbi8qIC5ib3gge1xcbiAgd2lkdGg6IDQwMHB4O1xcbiAgaGVpZ2h0OiA0MDBweDtcXG4gIG1hcmdpbjogMzJweCBhdXRvO1xcbiAgYm9yZGVyOiAxcHggc29saWQgI2YzNjtcXG59ICovXFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiA4MDBweDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcGFkZGluZzogODBweCAwO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOWQ5ZDk7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxROmZpcnN0LWNoaWxkIHtcXG4gIHBhZGRpbmctdG9wOiAwO1xcbn1cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUTpsYXN0LWNoaWxkIHtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyAuSG93V29ya3MtbGVmdC0xQXVGdSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyAuSG93V29ya3MtbGVmdC0xQXVGdSAuSG93V29ya3MtdGl0bGUtMkE3aGMge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyAuSG93V29ya3MtYXJyb3dXcmFwcGVyLTJhcWVYIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICBtYXJnaW4tYm90dG9tOiA1NnB4O1xcbn1cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIC5Ib3dXb3Jrcy1wcm9maWxlLTJzNkllIHtcXG4gIHdpZHRoOiAxMCU7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWdyYXlCb3gtM3kwaWYge1xcbiAgd2lkdGg6IDQ4cHg7XFxuICBoZWlnaHQ6IDQ4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgbWFyZ2luLXRvcDogNnB4O1xcbn1cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIC5Ib3dXb3Jrcy1jb250ZW50X2JveC0xdlEtaCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oIC5Ib3dXb3Jrcy10aXRsZS0yQTdoYyB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbn1cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIC5Ib3dXb3Jrcy1jb250ZW50X2JveC0xdlEtaCAuSG93V29ya3MtdGV4dC0xOFEzQSB7XFxuICBtYXgtd2lkdGg6IDUwMHB4O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1wbGF5ZXJTZWN0aW9uLTF2SUZpIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAzNTBweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDEycHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIHtcXG4gICAgcGFkZGluZzogNDBweCAxNnB4IDgwcHg7XFxuICB9XFxuXFxuICAuSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkge1xcbiAgICBtYXgtd2lkdGg6IDU1MHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuSG93V29ya3MtdHJpYW5nbGUtMXdXSlQge1xcbiAgICB3aWR0aDogMzRweDtcXG4gICAgaGVpZ2h0OiAyOHB4O1xcbiAgICBsZWZ0OiA2LjElO1xcbiAgICB0b3A6IDIyLjYlO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLWNoZW1pc3RyeS1XajhwVSB7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDQ0cHg7XFxuICAgIGxlZnQ6IDIuNSU7XFxuICAgIHRvcDogNDcuNSU7XFxuICB9XFxuXFxuICAuSG93V29ya3MtbWF0aHMtU21ha3Yge1xcbiAgICB3aWR0aDogNDRweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgICBib3R0b206IDE3LjYlO1xcbiAgICByaWdodDogOSU7XFxuICB9XFxuXFxuICAuSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIC5Ib3dXb3Jrcy1oZWFkZXJfdGl0bGUtMXBlNGUge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLWhlYWRlckNvbnRhaW5lci0yYW5WYSAuSG93V29ya3MtaGVhZGVyX2NvbnRlbnQtNlY2bU4ge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA0OHB4O1xcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amoge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICB9XFxuXFxuICAuSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiAuSG93V29ya3MtcmVxdWVzdERlbW8tMm0yM1Mge1xcbiAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amogLkhvd1dvcmtzLXdoYXRzYXBwd3JhcHBlci0yQjlCbCAuSG93V29ya3Mtd2hhdHNhcHAtMmh2eFUge1xcbiAgICBtYXJnaW4tbGVmdDogMDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAge1xcbiAgICBwYWRkaW5nOiAwIDE2cHg7XFxuICB9XFxuXFxuICAuSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkge1xcbiAgICBib3JkZXItdG9wOiBub25lO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zY3JvbGxDb250YWluZXItMUpKODYge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuSG93V29ya3Mtc2VjdGlvbi0zZmFsUSB7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICBwYWRkaW5nOiAwIDAgMjRweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1TyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgcGFkZGluZzogMTZweCAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q5ZDlkOTtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICB9XFxuXFxuICAuSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkLkhvd1dvcmtzLWhpZGUtSkdRSFQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZCAuSG93V29ya3MtcHJvZmlsZS0yczZJZSAuSG93V29ya3MtZ3JheUJveC0zeTBpZiB7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuICAuSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIC5Ib3dXb3Jrcy1jb250ZW50X2JveC0xdlEtaCAuSG93V29ya3MtdGl0bGUtMkE3aGMge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW46IDEycHggMCA4cHggMDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oIC5Ib3dXb3Jrcy10ZXh0LTE4UTNBIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1wbGF5ZXJTZWN0aW9uLTF2SUZpIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1wbGF5ZXJTZWN0aW9uLTF2SUZpLkhvd1dvcmtzLWhpZGUtSkdRSFQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNjcm9sbFRvcC0yMTlrbyB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvSG93V29ya3MvSG93V29ya3Muc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQix3QkFBd0I7RUFDeEIsa0JBQWtCO0VBQ2xCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLDhCQUE4QjtFQUM5QixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IseUJBQXlCO0VBQ3pCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZ0NBQWdDO0NBQ2pDOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YseUNBQXlDO0VBQ3pDLG9DQUFvQztFQUNwQyxpQ0FBaUM7Q0FDbEM7O0FBRUQ7RUFDRSxrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7Q0FDNUI7O0FBRUQ7Ozs7O0lBS0k7O0FBRUo7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUNBQWlDO0NBQ2xDOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGNBQWM7RUFDZCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztDQUNmOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsV0FBVztDQUNaOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQixnQkFBZ0I7Q0FDakI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0NBQzVCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osY0FBYztFQUNkLHFEQUFxRDtVQUM3Qyw2Q0FBNkM7Q0FDdEQ7O0FBRUQ7RUFDRTtJQUNFLHdCQUF3QjtHQUN6Qjs7RUFFRDtJQUNFLGlCQUFpQjtJQUNqQixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLGNBQWM7SUFDZCxVQUFVO0dBQ1g7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLDJCQUEyQjtRQUN2Qix1QkFBdUI7R0FDNUI7O0VBRUQ7SUFDRSxvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxlQUFlO0dBQ2hCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0Usc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QixpQkFBaUI7SUFDakIsb0JBQW9CO0lBQ3BCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsdUJBQXVCO1FBQ25CLCtCQUErQjtJQUNuQyx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsaUNBQWlDO0dBQ2xDOztFQUVEO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0Usb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkO0NBQ0ZcIixcImZpbGVcIjpcIkhvd1dvcmtzLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnJvb3Qge1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5tYXhDb250YWluZXIge1xcbiAgbWF4LXdpZHRoOiAxNzAwcHg7XFxufVxcblxcbi5oZWFkZXJDb250YWluZXIge1xcbiAgcGFkZGluZzogOTZweCA2NHB4O1xcbiAgcGFkZGluZzogNnJlbSA0cmVtO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uaGVhZGVyQ29udGFpbmVyIC5tYXhDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLnRyaWFuZ2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogMTQuNyU7XFxuICBsZWZ0OiA3LjYlO1xcbiAgd2lkdGg6IDYwcHg7XFxuICBoZWlnaHQ6IDcwcHg7XFxufVxcblxcbi5jaGVtaXN0cnkge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAyMy41JTtcXG4gIGxlZnQ6IDcuNiU7XFxuICB3aWR0aDogNzRweDtcXG4gIGhlaWdodDogOTRweDtcXG59XFxuXFxuLm1hdGhzIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMTYuOCU7XFxuICByaWdodDogMTUuNyU7XFxuICB3aWR0aDogOTBweDtcXG4gIGhlaWdodDogNjJweDtcXG59XFxuXFxuLmhlYWRlckNvbnRhaW5lciAuaGVhZGVyX3RpdGxlIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA2MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4uaGVhZGVyQ29udGFpbmVyIC5oZWFkZXJfY29udGVudCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXgtd2lkdGg6IDY3MHB4O1xcbn1cXG5cXG4uc2Nyb2xsVG9wIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uc2Nyb2xsVG9wIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLmJ1dHRvbldyYXBwZXIge1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogNjAwcHg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5idXR0b25XcmFwcGVyIC5yZXF1ZXN0RGVtbyB7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBjb2xvcjogIzAwMDtcXG4gIHBhZGRpbmc6IDE2cHggMjRweDtcXG4gIHdpZHRoOiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XFxuICBtaW4td2lkdGg6IDI0MHB4O1xcbiAgbWluLXdpZHRoOiAxNXJlbTtcXG59XFxuXFxuLmJ1dHRvbldyYXBwZXIgLndoYXRzYXBwd3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uYnV0dG9uV3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogIzI1MjgyYiAhaW1wb3J0YW50O1xcbiAgbWFyZ2luOiAwIDhweCAwIDMycHg7XFxuICBtYXJnaW46IDAgMC41cmVtIDAgMnJlbTtcXG4gIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uYnV0dG9uV3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICB3aWR0aDogMnJlbTtcXG4gIGhlaWdodDogMzJweDtcXG4gIGhlaWdodDogMnJlbTtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIC5sZWZ0IC5udW1iZXJXcmFwcGVyIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMjBweDtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IHNwYW4ge1xcbiAgbWFyZ2luOiAxNnB4IDA7XFxuICBtYXJnaW46IDFyZW0gMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIC5sZWZ0IC5udW1iZXJXcmFwcGVyIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5uYXZpZ2F0b3Ige1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiAwIDY0cHg7XFxuICBwYWRkaW5nOiAwIDRyZW07XFxufVxcblxcbi5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbiAgcGFkZGluZzogODBweCAwO1xcbiAgcGFkZGluZzogNXJlbSAwO1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYge1xcbiAgd2lkdGg6IDIwJTtcXG4gIGhlaWdodDogNTB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBwb3NpdGlvbjogLXdlYmtpdC1zdGlja3k7XFxuICBwb3NpdGlvbjogc3RpY2t5O1xcbiAgdG9wOiAxNjRweDtcXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNkOWQ5ZDk7XFxufVxcblxcbi5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciAuc2lkZW5hdiAubmF2SXRlbSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCA1MG1zIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDUwbXMgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBhbGwgNTBtcyBlYXNlLWluLW91dDtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IC5uYXZJdGVtLmFjdGl2ZSB7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IC5uYXZJdGVtIC5udW1iZXJXcmFwcGVyIHtcXG4gIHdpZHRoOiAzMHB4O1xcbiAgaGVpZ2h0OiAzMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U1ZTVlNTtcXG4gIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IC5uYXZJdGVtIC5udW1iZXJXcmFwcGVyLmFjdGl2ZU51bWJlciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgLm5hdkl0ZW0gLm51bWJlcldyYXBwZXIgc3BhbiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IC5uYXZJdGVtIC5udW1iZXJXcmFwcGVyLmFjdGl2ZU51bWJlciBzcGFuIHtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNjcm9sbENvbnRhaW5lciB7XFxuICB3aWR0aDogODAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLyogLmJveCB7XFxuICB3aWR0aDogNDAwcHg7XFxuICBoZWlnaHQ6IDQwMHB4O1xcbiAgbWFyZ2luOiAzMnB4IGF1dG87XFxuICBib3JkZXI6IDFweCBzb2xpZCAjZjM2O1xcbn0gKi9cXG5cXG4uc2VjdGlvbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDgwMHB4O1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwYWRkaW5nOiA4MHB4IDA7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLnNlY3Rpb246Zmlyc3QtY2hpbGQge1xcbiAgcGFkZGluZy10b3A6IDA7XFxufVxcblxcbi5zZWN0aW9uOmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIC5sZWZ0IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIC5sZWZ0IC50aXRsZSB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG59XFxuXFxuLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIC5hcnJvd1dyYXBwZXIge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDU2cHg7XFxufVxcblxcbi5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQgLnByb2ZpbGUge1xcbiAgd2lkdGg6IDEwJTtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuZ3JheUJveCB7XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogNDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICBtYXJnaW4tdG9wOiA2cHg7XFxufVxcblxcbi5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQgLmNvbnRlbnRfYm94IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuY29udGVudF9ib3ggLnRpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxufVxcblxcbi5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQgLmNvbnRlbnRfYm94IC50ZXh0IHtcXG4gIG1heC13aWR0aDogNTAwcHg7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG59XFxuXFxuLnNlY3Rpb24gLnBsYXllclNlY3Rpb24ge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5oZWFkZXJDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiA0MHB4IDE2cHggODBweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJDb250YWluZXIgLm1heENvbnRhaW5lciB7XFxuICAgIG1heC13aWR0aDogNTUwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC50cmlhbmdsZSB7XFxuICAgIHdpZHRoOiAzNHB4O1xcbiAgICBoZWlnaHQ6IDI4cHg7XFxuICAgIGxlZnQ6IDYuMSU7XFxuICAgIHRvcDogMjIuNiU7XFxuICB9XFxuXFxuICAuY2hlbWlzdHJ5IHtcXG4gICAgd2lkdGg6IDM2cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgbGVmdDogMi41JTtcXG4gICAgdG9wOiA0Ny41JTtcXG4gIH1cXG5cXG4gIC5tYXRocyB7XFxuICAgIHdpZHRoOiA0NHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICAgIGJvdHRvbTogMTcuNiU7XFxuICAgIHJpZ2h0OiA5JTtcXG4gIH1cXG5cXG4gIC5oZWFkZXJDb250YWluZXIgLmhlYWRlcl90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICB9XFxuXFxuICAuaGVhZGVyQ29udGFpbmVyIC5oZWFkZXJfY29udGVudCB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDQ4cHg7XFxuICAgIG1heC13aWR0aDogMzAwcHg7XFxuICB9XFxuXFxuICAuYnV0dG9uV3JhcHBlciB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5idXR0b25XcmFwcGVyIC5yZXF1ZXN0RGVtbyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuICAuYnV0dG9uV3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCB7XFxuICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgfVxcblxcbiAgLm5hdmlnYXRvciB7XFxuICAgIHBhZGRpbmc6IDAgMTZweDtcXG4gIH1cXG5cXG4gIC5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciB7XFxuICAgIGJvcmRlci10b3A6IG5vbmU7XFxuICB9XFxuXFxuICAubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNjcm9sbENvbnRhaW5lciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uIHtcXG4gICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIHBhZGRpbmc6IDAgMCAyNHB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLm1vYmlsZV9zZWN0aW9uIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBwYWRkaW5nOiAxNnB4IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDlkOWQ5O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQuaGlkZSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAuc2VjdGlvbl9jb250ZW50IC5wcm9maWxlIC5ncmF5Qm94IHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDBweDtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQgLmNvbnRlbnRfYm94IC50aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgIG1hcmdpbjogMTJweCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuY29udGVudF9ib3ggLnRleHQge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnBsYXllclNlY3Rpb24ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnBsYXllclNlY3Rpb24uaGlkZSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuc2Nyb2xsVG9wIHtcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgcmlnaHQ6IDE2cHg7XFxuICAgIGJvdHRvbTogMTZweDtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInJvb3RcIjogXCJIb3dXb3Jrcy1yb290LTJPZFR0XCIsXG5cdFwibWF4Q29udGFpbmVyXCI6IFwiSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpXCIsXG5cdFwiaGVhZGVyQ29udGFpbmVyXCI6IFwiSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhXCIsXG5cdFwidHJpYW5nbGVcIjogXCJIb3dXb3Jrcy10cmlhbmdsZS0xd1dKVFwiLFxuXHRcImNoZW1pc3RyeVwiOiBcIkhvd1dvcmtzLWNoZW1pc3RyeS1XajhwVVwiLFxuXHRcIm1hdGhzXCI6IFwiSG93V29ya3MtbWF0aHMtU21ha3ZcIixcblx0XCJoZWFkZXJfdGl0bGVcIjogXCJIb3dXb3Jrcy1oZWFkZXJfdGl0bGUtMXBlNGVcIixcblx0XCJoZWFkZXJfY29udGVudFwiOiBcIkhvd1dvcmtzLWhlYWRlcl9jb250ZW50LTZWNm1OXCIsXG5cdFwic2Nyb2xsVG9wXCI6IFwiSG93V29ya3Mtc2Nyb2xsVG9wLTIxOWtvXCIsXG5cdFwiYnV0dG9uV3JhcHBlclwiOiBcIkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1ampcIixcblx0XCJyZXF1ZXN0RGVtb1wiOiBcIkhvd1dvcmtzLXJlcXVlc3REZW1vLTJtMjNTXCIsXG5cdFwid2hhdHNhcHB3cmFwcGVyXCI6IFwiSG93V29ya3Mtd2hhdHNhcHB3cmFwcGVyLTJCOUJsXCIsXG5cdFwid2hhdHNhcHBcIjogXCJIb3dXb3Jrcy13aGF0c2FwcC0yaHZ4VVwiLFxuXHRcInNlY3Rpb25cIjogXCJIb3dXb3Jrcy1zZWN0aW9uLTNmYWxRXCIsXG5cdFwibW9iaWxlX3NlY3Rpb25cIjogXCJIb3dXb3Jrcy1tb2JpbGVfc2VjdGlvbi0yRHU1T1wiLFxuXHRcImxlZnRcIjogXCJIb3dXb3Jrcy1sZWZ0LTFBdUZ1XCIsXG5cdFwibnVtYmVyV3JhcHBlclwiOiBcIkhvd1dvcmtzLW51bWJlcldyYXBwZXItUjR5UTFcIixcblx0XCJuYXZpZ2F0b3JcIjogXCJIb3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDBcIixcblx0XCJzaWRlbmF2XCI6IFwiSG93V29ya3Mtc2lkZW5hdi0xRUkxLVwiLFxuXHRcIm5hdkl0ZW1cIjogXCJIb3dXb3Jrcy1uYXZJdGVtLXpDaGRsXCIsXG5cdFwiYWN0aXZlXCI6IFwiSG93V29ya3MtYWN0aXZlLTJLeHZuXCIsXG5cdFwiYWN0aXZlTnVtYmVyXCI6IFwiSG93V29ya3MtYWN0aXZlTnVtYmVyLTJjNG9MXCIsXG5cdFwic2Nyb2xsQ29udGFpbmVyXCI6IFwiSG93V29ya3Mtc2Nyb2xsQ29udGFpbmVyLTFKSjg2XCIsXG5cdFwidGl0bGVcIjogXCJIb3dXb3Jrcy10aXRsZS0yQTdoY1wiLFxuXHRcImFycm93V3JhcHBlclwiOiBcIkhvd1dvcmtzLWFycm93V3JhcHBlci0yYXFlWFwiLFxuXHRcInNlY3Rpb25fY29udGVudFwiOiBcIkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZFwiLFxuXHRcInByb2ZpbGVcIjogXCJIb3dXb3Jrcy1wcm9maWxlLTJzNkllXCIsXG5cdFwiZ3JheUJveFwiOiBcIkhvd1dvcmtzLWdyYXlCb3gtM3kwaWZcIixcblx0XCJjb250ZW50X2JveFwiOiBcIkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oXCIsXG5cdFwidGV4dFwiOiBcIkhvd1dvcmtzLXRleHQtMThRM0FcIixcblx0XCJwbGF5ZXJTZWN0aW9uXCI6IFwiSG93V29ya3MtcGxheWVyU2VjdGlvbi0xdklGaVwiLFxuXHRcImhpZGVcIjogXCJIb3dXb3Jrcy1oaWRlLUpHUUhUXCJcbn07IiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluay9MaW5rJztcbmltcG9ydCB7IEhPV19XT1JLUywgSE9XX1dPUktTX1RJVExFUyB9IGZyb20gJy4uL0dldFJhbmtzQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vSG93V29ya3Muc2Nzcyc7XG5cbmNsYXNzIEhvd1dvcmtzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG9wZW5MaXN0OiBbLi4uSE9XX1dPUktTX1RJVExFU10sXG4gICAgICBpc01vYmlsZTogZmFsc2UsXG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICB9O1xuICB9XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlUmVzaXplKCk7XG4gICAgdGhpcy5vYnNlcnZlRWxlbGVtZW50cygpO1xuICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZVJlc2l6ZSk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcbiAgfVxuXG4gIGhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LnNjcm9sbFkgPiA1MDApIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiB0cnVlLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVTY3JvbGxUb3AgPSAoKSA9PiB7XG4gICAgd2luZG93LnNjcm9sbFRvKHtcbiAgICAgIHRvcDogMCxcbiAgICAgIGJlaGF2aW9yOiAnc21vb3RoJyxcbiAgICB9KTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZVJlc2l6ZSA9ICgpID0+IHtcbiAgICBsZXQgeyBpc01vYmlsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiA5OTApIHtcbiAgICAgIGlzTW9iaWxlID0gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlzTW9iaWxlID0gdHJ1ZTtcbiAgICB9XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBpc01vYmlsZSxcbiAgICB9KTtcbiAgfTtcblxuICBoYW5kbGVTdGVwVG9nZ2xlID0gKHRpdGxlLCBzdGF0dXMpID0+IHtcbiAgICBjb25zdCB7IG9wZW5MaXN0IH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmIChzdGF0dXMpIHtcbiAgICAgIGNvbnN0IGluZCA9IG9wZW5MaXN0LmZpbmRJbmRleChlbGUgPT4gZWxlID09PSB0aXRsZSk7XG4gICAgICBpZiAoaW5kID4gLTEpIHtcbiAgICAgICAgb3Blbkxpc3Quc3BsaWNlKGluZCwgMSk7XG4gICAgICB9XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgb3Blbkxpc3QsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgb3Blbkxpc3QucHVzaCh0aXRsZSk7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgb3Blbkxpc3QsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgb2JzZXJ2ZUVsZWxlbWVudHMgPSAoKSA9PiB7XG4gICAgY29uc3Qgb2JzZXJ2ZXIgPSBuZXcgSW50ZXJzZWN0aW9uT2JzZXJ2ZXIoXG4gICAgICBlbnRyaWVzID0+IHtcbiAgICAgICAgZW50cmllcy5mb3JFYWNoKGVudHJ5ID0+IHtcbiAgICAgICAgICBjb25zdCBub2RlSWQgPSBlbnRyeS50YXJnZXQuZ2V0QXR0cmlidXRlKCdpZCcpO1xuICAgICAgICAgIGNvbnN0IGxpbmsgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFxuICAgICAgICAgICAgYGRpdi4ke3Muc2lkZW5hdn0gYVtocmVmPVwiIyR7bm9kZUlkfVwiXWAsXG4gICAgICAgICAgKTtcbiAgICAgICAgICBpZiAoZW50cnkuaXNJbnRlcnNlY3RpbmcpIHtcbiAgICAgICAgICAgIGxpbmsuY2xhc3NMaXN0LmFkZChzLmFjdGl2ZSk7XG4gICAgICAgICAgICBsaW5rLmNoaWxkTm9kZXNbMF0uY2xhc3NMaXN0LmFkZChzLmFjdGl2ZU51bWJlcik7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxpbmsuY2xhc3NMaXN0LnJlbW92ZShzLmFjdGl2ZSk7XG4gICAgICAgICAgICBsaW5rLmNoaWxkTm9kZXNbMF0uY2xhc3NMaXN0LnJlbW92ZShzLmFjdGl2ZU51bWJlcik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHRocmVzaG9sZDogMC41NSxcbiAgICAgIH0sXG4gICAgKTtcbiAgICBjb25zdCBlbGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgbGlzdGApLnF1ZXJ5U2VsZWN0b3JBbGwoYGRpdltpZF1gKTtcbiAgICBlbGUuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgIG9ic2VydmVyLm9ic2VydmUoZWxlbWVudCk7XG4gICAgfSk7XG4gIH07XG5cbiAgZGlzcGxheUhlYWRlciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWF4Q29udGFpbmVyfT5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmhlYWRlcl90aXRsZX0+SG93IGVnbmlmeSB3b3Jrczwvc3Bhbj5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmhlYWRlcl9jb250ZW50fT5cbiAgICAgICAgICBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LkxvcmVtIGlwc3VtXG4gICAgICAgICAgZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC5cbiAgICAgICAgPC9zcGFuPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5idXR0b25XcmFwcGVyfT5cbiAgICAgICAgICA8TGluayB0bz1cIi9yZXF1ZXN0LWRlbW9cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlcXVlc3REZW1vfSByb2xlPVwicHJlc2VudGF0aW9uXCI+XG4gICAgICAgICAgICAgIFNUQVJUIEZSRUUgVFJJQUxcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8YVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLndoYXRzYXBwd3JhcHBlcn1cbiAgICAgICAgICAgIGhyZWY9XCJodHRwczovL2FwaS53aGF0c2FwcC5jb20vc2VuZD9waG9uZT05MTk5NDk1MjM4NDkmdGV4dD1IaSBHZXRSYW5rcywgSSB3b3VsZCBsaWtlIHRvIGtub3cgbW9yZSBhYm91dCB5b3VyIE9ubGluZSBQbGF0Zm9ybS5cIlxuICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLndoYXRzYXBwfT5DaGF0IG9uPC9kaXY+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0c2FwcF9sb2dvLnN2Z1wiIGFsdD1cIndoYXRzYXBwXCIgLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8aW1nXG4gICAgICAgIGNsYXNzTmFtZT17cy50cmlhbmdsZX1cbiAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9zdWJqZWN0X2ljb25zL3RyaWFuZ2xlLnN2Z1wiXG4gICAgICAgIGFsdD1cInRyaWFuZ2xlXCJcbiAgICAgIC8+XG4gICAgICA8aW1nXG4gICAgICAgIGNsYXNzTmFtZT17cy5jaGVtaXN0cnl9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy9jaGVtaXN0cnkuc3ZnXCJcbiAgICAgICAgYWx0PVwidHJpYW5nbGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLm1hdGhzfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvc2NhbGUuc3ZnXCJcbiAgICAgICAgYWx0PVwibWF0aHNcIlxuICAgICAgLz5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5U3RlcHMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MubmF2aWdhdG9yfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1heENvbnRhaW5lcn0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNpZGVuYXZ9PlxuICAgICAgICAgIHtIT1dfV09SS1MubWFwKChpdGVtLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgPGEgaHJlZj17YCMke2l0ZW0udGl0bGV9YH0gY2xhc3NOYW1lPXtzLm5hdkl0ZW19PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5udW1iZXJXcmFwcGVyfT5cbiAgICAgICAgICAgICAgICA8c3Bhbj57aW5kZXggKyAxfTwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxzcGFuPntpdGVtLnRpdGxlfTwvc3Bhbj5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgaWQ9XCJsaXN0XCIgY2xhc3NOYW1lPXtzLnNjcm9sbENvbnRhaW5lcn0+XG4gICAgICAgICAge0hPV19XT1JLUy5tYXAoKGJveCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb3Blbkxpc3QsIGlzTW9iaWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgaXNPcGVuID0gb3Blbkxpc3QuaW5jbHVkZXMoYm94LnRpdGxlKTtcbiAgICAgICAgICAgIGNvbnN0IGNvbnRlbnRDbGFzcyA9IGlzT3BlblxuICAgICAgICAgICAgICA/IGAke3Muc2VjdGlvbl9jb250ZW50fWBcbiAgICAgICAgICAgICAgOiBgJHtzLnNlY3Rpb25fY29udGVudH0gJHtzLmhpZGV9YDtcbiAgICAgICAgICAgIGNvbnN0IHBsYXllckNsYXNzID0gaXNPcGVuXG4gICAgICAgICAgICAgID8gYCR7cy5wbGF5ZXJTZWN0aW9ufWBcbiAgICAgICAgICAgICAgOiBgJHtzLnBsYXllclNlY3Rpb259ICR7cy5oaWRlfWA7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICA8ZGl2IGlkPXtib3gudGl0bGV9IGNsYXNzTmFtZT17cy5zZWN0aW9ufT5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MubW9iaWxlX3NlY3Rpb259XG4gICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzTW9iaWxlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kbGVTdGVwVG9nZ2xlKGJveC50aXRsZSwgaXNPcGVuKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0fT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubnVtYmVyV3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2luZGV4ICsgMX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGl0bGV9Pntib3gudGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hcnJvd1dyYXBwZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzT3BlblxuICAgICAgICAgICAgICAgICAgICAgICAgICA/IGAvaW1hZ2VzL2ljb25zL2NoZXZyb24tdXAuc3ZnYFxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IGAvaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmdgXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cImFycm93XCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjb250ZW50Q2xhc3N9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJvZmlsZX0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPXtzLmdyYXlCb3h9IHNyYz17Ym94LmltZ30gYWx0PXtib3gudGl0bGV9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfYm94fT5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRpdGxlfT57Ym94LnRpdGxlfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHR9Pntib3guY29udGVudH08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cGxheWVyQ2xhc3N9IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9KX1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBzaG93U2Nyb2xsIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5zY3JvbGxUb3B9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGlkPVwicm9vdFwiIGNsYXNzTmFtZT17cy5yb290fT5cbiAgICAgICAge3RoaXMuZGlzcGxheUhlYWRlcigpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U3RlcHMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEhvd1dvcmtzKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSG93V29ya3Muc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Ib3dXb3Jrcy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSG93V29ya3Muc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dC9MYXlvdXQnO1xuaW1wb3J0IEhvd1dvcmtzIGZyb20gJy4vSG93V29ya3MnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnkgbWFrZXMgdGVhY2hpbmcgZWFzeScsXG4gICAgY2h1bmtzOiBbJ0hvd1dvcmtzJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8SG93V29ya3MgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFzQkE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQWhDQTtBQWtDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUExQ0E7QUE0Q0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBdERBO0FBd0RBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBeEVBO0FBMEVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkdBO0FBcUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBeElBO0FBZ0pBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUtBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQS9NQTtBQXFOQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQWxPQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaU5BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQS9PQTtBQUNBO0FBK09BOzs7Ozs7O0FDdFBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFTQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==