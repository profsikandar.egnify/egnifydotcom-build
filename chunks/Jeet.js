require("source-map-support").install();
exports.ids = ["Jeet"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/jeet/Jeet.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Jeet-root-2mgrQ {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Jeet-intro-3npEu {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Jeet-introLeftPane-35bDx {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Jeet-jeet-39Jtx {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Jeet-coachingInstitutes-1JOdO {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Jeet-introContent-Rjwmh {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Jeet-introRightPane-2PY7z {\n  padding-top: 5% !important;\n}\n\n.Jeet-introRightPane-2PY7z img {\n    width: 100%;\n  }\n\n.Jeet-content-3Aeuf {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Jeet-contentImageDiv-1Ghrs {\n  height: 136px;\n  width: 136px;\n}\n\n.Jeet-screenImage-1lKDc {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Jeet-headingContent-2nvVl {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Jeet-mainContent-1aDM3 {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Jeet-testimonialsTitle-3TLf_ {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Jeet-testimonialsTitle-3TLf_::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Jeet-contentSubdiv-14PcU {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Jeet-jeeDetailsContainer-2bXgG {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.Jeet-jeeDetailsContainer-2bXgG .Jeet-subSection-3d8aX {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.Jeet-jeeTitle-12EqG {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.Jeet-jeeDescription-CN83U {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.Jeet-title2-1XLYU {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.Jeet-underline-2e2oc {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .Jeet-root-2mgrQ {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Jeet-contentForSmall-OkyDd {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Jeet-contentImageDivSmall-59O9V {\n    width: 100%;\n  }\n\n    .Jeet-contentImageDivSmall-59O9V img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Jeet-screenImageSmall-3SS8g {\n    text-align: center;\n  }\n\n  .Jeet-headingContentSmall-3oslf {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Jeet-mainContentSmall-F5bvm {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .Jeet-testimonialsTitle-3TLf_ {\n    margin-bottom: 18%;\n  }\n\n  .Jeet-testimonialsTitle-3TLf_::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Jeet-introLeftPane-35bDx {\n    width: 100%;\n  }\n\n  .Jeet-jeet-39Jtx {\n    text-align: center;\n  }\n\n  .Jeet-coachingInstitutes-1JOdO {\n    text-align: center;\n  }\n\n  .Jeet-introContent-Rjwmh {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Jeet-link-CvyVA {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .Jeet-introContent-Rjwmh {\n    width: 300px;\n  }\n\n  .Jeet-jeeDetailsContainer-2bXgG {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .Jeet-jeeDetailsContainer-2bXgG .Jeet-subSection-3d8aX {\n      width: 100%;\n    }\n\n  .Jeet-jeeTitle-12EqG {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .Jeet-jeeDescription-CN83U {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Jeet-title2-1XLYU {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Jeet-underline-2e2oc {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Jeet-introContent-Rjwmh {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .Jeet-jeeTitle-12EqG {\n    font-size: 30px;\n  }\n\n  .Jeet-jeeDescription-CN83U {\n    font-size: 18px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/Jeet.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,6BAA6B;EAC7B,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,0BAA0B;CAC3B;;AAED;IACI,WAAW;IACX,4BAA4B;IAC5B,aAAa;IACb,qBAAqB;GACtB;;AAEH;EACE,gBAAgB;EAChB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,UAAU;EACV,0BAA0B;CAC3B;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;CACF;;AAED;EACE;IACE,aAAa;GACd;;EAED;IACE,aAAa;IACb,cAAc;GACf;;IAEC;MACE,YAAY;KACb;;EAEH;IACE,8BAA8B;IAC9B,gBAAgB;GACjB;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"Jeet.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.jeeDetailsContainer {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.jeeDetailsContainer .subSection {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.jeeTitle {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.jeeDescription {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.title2 {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.underline {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .introContent {\n    width: 300px;\n  }\n\n  .jeeDetailsContainer {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .jeeDetailsContainer .subSection {\n      width: 100%;\n    }\n\n  .jeeTitle {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .jeeTitle {\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    font-size: 18px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Jeet-root-2mgrQ",
	"intro": "Jeet-intro-3npEu",
	"introLeftPane": "Jeet-introLeftPane-35bDx",
	"jeet": "Jeet-jeet-39Jtx",
	"coachingInstitutes": "Jeet-coachingInstitutes-1JOdO",
	"introContent": "Jeet-introContent-Rjwmh",
	"introRightPane": "Jeet-introRightPane-2PY7z",
	"content": "Jeet-content-3Aeuf",
	"contentImageDiv": "Jeet-contentImageDiv-1Ghrs",
	"screenImage": "Jeet-screenImage-1lKDc",
	"headingContent": "Jeet-headingContent-2nvVl",
	"mainContent": "Jeet-mainContent-1aDM3",
	"testimonialsTitle": "Jeet-testimonialsTitle-3TLf_",
	"contentSubdiv": "Jeet-contentSubdiv-14PcU",
	"jeeDetailsContainer": "Jeet-jeeDetailsContainer-2bXgG",
	"subSection": "Jeet-subSection-3d8aX",
	"jeeTitle": "Jeet-jeeTitle-12EqG",
	"jeeDescription": "Jeet-jeeDescription-CN83U",
	"title2": "Jeet-title2-1XLYU",
	"underline": "Jeet-underline-2e2oc",
	"contentForSmall": "Jeet-contentForSmall-OkyDd",
	"contentImageDivSmall": "Jeet-contentImageDivSmall-59O9V",
	"screenImageSmall": "Jeet-screenImageSmall-3SS8g",
	"headingContentSmall": "Jeet-headingContentSmall-3oslf",
	"mainContentSmall": "Jeet-mainContentSmall-F5bvm",
	"link": "Jeet-link-CvyVA"
};

/***/ }),

/***/ "./src/routes/products/jeet/Jeet.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Jeet_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/jeet/Jeet.scss");
/* harmony import */ var _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/Jeet.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







const STATS = [{
  title: '80k+',
  description: 'Students attempted jee mains in 2018'
}, {
  title: '17.5%',
  description: 'cut-off clear rate'
}, {
  title: '13k',
  description: 'Students qualified jee mains in 2018'
}, {
  title: '40%',
  description: 'Students of ap and telangana'
}];
const CONTENT = [{
  headerContent: 'Understand where do your students stand',
  mainContent: 'Marks Analysis helps you understand how your students are performing in a snapshot and identify who needs help to clear the cut-off.',
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/jeet/screen1.png'
}, {
  headerContent: 'Identify student’s strengths & areas of improvement',
  mainContent: 'Concept-based Profile helps your student identify his/her strong & weak topics and forms a basis for his/her areas of focus to improve his performance.',
  icon: '/images/products/jeet/blub.svg',
  screenImage: '/images/products/jeet/screen2.png'
}, {
  headerContent: 'Compare performance data across your institute hierarchy',
  mainContent: 'Comparison Analysis helps you compare students’ performance between states, campuses, batches, sections, tests & take apt actions on red areas.',
  icon: '/images/products/jeet/weights.svg',
  screenImage: '/images/products/jeet/screen3.png'
}, {
  headerContent: 'Channelize performance data using relevant filters',
  mainContent: 'Contextualize your students’ performance data using pre-set filters based on your institute’s hierarchy and look at analysis filtered to the selection.',
  icon: '/images/products/jeet/group.svg',
  screenImage: '/images/products/jeet/screen4.png'
}, {
  headerContent: 'Downloadable reports to understand students’ performance',
  mainContent: 'Extensive excel reports of students’ performance data which can be sorted or filtered in the way you want to look at them.',
  icon: '/images/products/jeet/download.svg',
  screenImage: '/images/products/jeet/screen5.png'
}, {
  headerContent: 'Identify the questions most of your students have gone wrong',
  mainContent: 'Error Analysis helps you determine the questions in a test which majority of your students have not attempted or attempted wrongly to enable error practice.',
  icon: '/images/products/jeet/idle.svg',
  screenImage: '/images/products/jeet/screen6.png'
}];
const SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

class Jeet extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "makeAlignment", (contentIndex, key) => {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });
  }

  // static propTypes = {
  //
  // };
  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_3___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_3___default.a.pageview(window.location.href);
  }
  /**
   * @description Returns what alignment should the content take eg. flex-start flex end center
   * @param contentIndex,key
   * @author Sushruth
   * */


  render() {
    const view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane} col m4 s12`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, "GetRanks"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, "For Coaching Institues"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }, "GetRanks is a learning Analytics ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }), "platform which empowers", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }), "coaching institutes to", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }), "increase number of their", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }), "students clearing cut-off in", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }), "competitive exams."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/request-demo",
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 141
      },
      __self: this
    }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane} col m8 s12`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/products/jeet/JEET.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDetailsContainer} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.title2}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149
      },
      __self: this
    }, "Our Stats", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.underline}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    })), STATS.map((details, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subSection}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 154
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeTitle}`,
      style: {
        borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, details.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDescription}`,
      style: {
        borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166
      },
      __self: this
    }, details.description.toUpperCase())))), CONTENT.map((content, contentIndex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 181
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content} row hide-on-xs`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 182
      },
      __self: this
    }, [0, 1].map(key => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: contentIndex % 2 === key ? `col m5 l5 ${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}` : `col m7 l7 ${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}`,
      style: {
        justifyContent: this.makeAlignment(contentIndex, key)
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 184
      },
      __self: this
    }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 195
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 196
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 197
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 199
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 202
      },
      __self: this
    }, content.mainContent)) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      width: "87.6%",
      height: "54.45%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 206
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall} row hide-on-m-and-up`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 217
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 218
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 219
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 224
      },
      __self: this
    }, content.mainContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 225
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      width: "87.6%",
      height: "54.45%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 226
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        paddingTop: '64px',
        marginBottom: '5%'
      },
      className: "row hide",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 236
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    }, "Why our clients love Egnify"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: SLIDESHOW,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 241
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 243
      },
      __self: this
    }));
    return view;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Jeet));

/***/ }),

/***/ "./src/routes/products/jeet/Jeet.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/jeet/Jeet.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/jeet/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Jeet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/jeet/Jeet.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/index.js";




async function action() {
  return {
    title: 'Egnify',
    chunks: ['Jeet'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Jeet__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0plZXQuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2plZXQvSmVldC5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvamVldC9KZWV0LmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvamVldC9KZWV0LnNjc3M/NzJhNCIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2plZXQvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5KZWV0LXJvb3QtMm1nclEge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYzRjNmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbn1cXG5cXG4uSmVldC1pbnRyby0zbnBFdSB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogMyU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTtcXG59XFxuXFxuLkplZXQtaW50cm9MZWZ0UGFuZS0zNWJEeCB7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uSmVldC1qZWV0LTM5SnR4IHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5KZWV0LWNvYWNoaW5nSW5zdGl0dXRlcy0xSk9kTyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uSmVldC1pbnRyb0NvbnRlbnQtUmp3bWgge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgd2lkdGg6IDQyMHB4O1xcbn1cXG5cXG4uSmVldC1pbnRyb1JpZ2h0UGFuZS0yUFk3eiB7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG59XFxuXFxuLkplZXQtaW50cm9SaWdodFBhbmUtMlBZN3ogaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuLkplZXQtY29udGVudC0zQWV1ZiB7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG59XFxuXFxuLkplZXQtY29udGVudEltYWdlRGl2LTFHaHJzIHtcXG4gIGhlaWdodDogMTM2cHg7XFxuICB3aWR0aDogMTM2cHg7XFxufVxcblxcbi5KZWV0LXNjcmVlbkltYWdlLTFsS0RjIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctdG9wOiA2OHB4O1xcbn1cXG5cXG4uSmVldC1oZWFkaW5nQ29udGVudC0ybnZWbCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLkplZXQtbWFpbkNvbnRlbnQtMWFETTMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS42O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5KZWV0LXRlc3RpbW9uaWFsc1RpdGxlLTNUTGZfIHtcXG4gIGhlaWdodDogMzhweDtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbiAgcGFkZGluZzogMiU7XFxufVxcblxcbi5KZWV0LXRlc3RpbW9uaWFsc1RpdGxlLTNUTGZfOjphZnRlciB7XFxuICBjb250ZW50OiAnJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogMnB4O1xcbiAgYm9yZGVyLXRvcDogc29saWQgM3B4ICNlYzRjNmY7XFxuICBwYWRkaW5nLXRvcDogMTJweDtcXG59XFxuXFxuLkplZXQtY29udGVudFN1YmRpdi0xNFBjVSB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5KZWV0LWplZURldGFpbHNDb250YWluZXItMmJYZ0cge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWluLWhlaWdodDogMzAwcHggIWltcG9ydGFudDtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA1JTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZTNlNWY7XFxufVxcblxcbi5KZWV0LWplZURldGFpbHNDb250YWluZXItMmJYZ0cgLkplZXQtc3ViU2VjdGlvbi0zZDhhWCB7XFxuICAgIHdpZHRoOiAyNSU7XFxuICAgIG1pbi1oZWlnaHQ6IDgwcHggIWltcG9ydGFudDtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcXG4gIH1cXG5cXG4uSmVldC1qZWVUaXRsZS0xMkVxRyB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmYWIwMDtcXG59XFxuXFxuLkplZXQtamVlRGVzY3JpcHRpb24tQ044M1Uge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDUlO1xcbiAgY29sb3I6ICNmZmY7XFxuICBoZWlnaHQ6IDcwcHg7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLkplZXQtdGl0bGUyLTFYTFlVIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG5cXG4uSmVldC11bmRlcmxpbmUtMmUyb2Mge1xcbiAgbWFyZ2luLXRvcDogOHB4O1xcbiAgbWFyZ2luLWxlZnQ6IDQ4JTtcXG4gIHdpZHRoOiA0JTtcXG4gIGJvcmRlcjogc29saWQgMXB4ICNlYzRjNmY7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5KZWV0LXJvb3QtMm1nclEge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuSmVldC1jb250ZW50Rm9yU21hbGwtT2t5RGQge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuSmVldC1jb250ZW50SW1hZ2VEaXZTbWFsbC01OU85ViB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgICAuSmVldC1jb250ZW50SW1hZ2VEaXZTbWFsbC01OU85ViBpbWcge1xcbiAgICAgIGhlaWdodDogMTAwcHg7XFxuICAgICAgd2lkdGg6IDEwMHB4O1xcbiAgICB9XFxuXFxuICAuSmVldC1zY3JlZW5JbWFnZVNtYWxsLTNTUzhnIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkplZXQtaGVhZGluZ0NvbnRlbnRTbWFsbC0zb3NsZiB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLkplZXQtbWFpbkNvbnRlbnRTbWFsbC1GNWJ2bSB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLkplZXQtdGVzdGltb25pYWxzVGl0bGUtM1RMZl8ge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxOCU7XFxuICB9XFxuXFxuICAuSmVldC10ZXN0aW1vbmlhbHNUaXRsZS0zVExmXzo6YWZ0ZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0cHg7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLkplZXQtaW50cm9MZWZ0UGFuZS0zNWJEeCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLkplZXQtamVldC0zOUp0eCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5KZWV0LWNvYWNoaW5nSW5zdGl0dXRlcy0xSk9kTyB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5KZWV0LWludHJvQ29udGVudC1SandtaCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gICAgcGFkZGluZy1yaWdodDogMTUlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICB9XFxuXFxuICAuSmVldC1saW5rLUN2eVZBIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xcbiAgLkplZXQtaW50cm9Db250ZW50LVJqd21oIHtcXG4gICAgd2lkdGg6IDMwMHB4O1xcbiAgfVxcblxcbiAgLkplZXQtamVlRGV0YWlsc0NvbnRhaW5lci0yYlhnRyB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogMDtcXG4gIH1cXG5cXG4gICAgLkplZXQtamVlRGV0YWlsc0NvbnRhaW5lci0yYlhnRyAuSmVldC1zdWJTZWN0aW9uLTNkOGFYIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbiAgLkplZXQtamVlVGl0bGUtMTJFcUcge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgZm9udC1zaXplOiAzMHB4O1xcbiAgfVxcblxcbiAgLkplZXQtamVlRGVzY3JpcHRpb24tQ044M1Uge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDtcXG4gICAgcGFkZGluZy1sZWZ0OiAyMCU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcXG4gIH1cXG5cXG4gIC5KZWV0LXRpdGxlMi0xWExZVSB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgcGFkZGluZy10b3A6IDQwcHg7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLkplZXQtdW5kZXJsaW5lLTJlMm9jIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDQzJTtcXG4gICAgd2lkdGg6IDE1JTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xcbiAgLkplZXQtaW50cm9Db250ZW50LVJqd21oIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcblxcbiAgLkplZXQtamVlVGl0bGUtMTJFcUcge1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuSmVldC1qZWVEZXNjcmlwdGlvbi1DTjgzVSB7XFxuICAgIGZvbnQtc2l6ZTogMThweDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2plZXQvSmVldC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osNERBQTREO1VBQ3BELG9EQUFvRDtDQUM3RDs7QUFFRDtFQUNFLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGVBQWU7RUFDZiwyQkFBMkI7RUFDM0IsYUFBYTtDQUNkOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsMkJBQTJCO0NBQzVCOztBQUVEO0lBQ0ksWUFBWTtHQUNiOztBQUVIO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsY0FBYztFQUNkLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osOEJBQThCO0VBQzlCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLDBCQUEwQjtDQUMzQjs7QUFFRDtJQUNJLFdBQVc7SUFDWCw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLHFCQUFxQjtHQUN0Qjs7QUFFSDtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFVBQVU7RUFDViwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRTtJQUNFLFdBQVc7SUFDWCxVQUFVO0dBQ1g7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsWUFBWTtJQUNaLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjs7SUFFQztNQUNFLGNBQWM7TUFDZCxhQUFhO0tBQ2Q7O0VBRUg7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsYUFBYTtHQUNkOztFQUVEO0lBQ0UsYUFBYTtJQUNiLGNBQWM7R0FDZjs7SUFFQztNQUNFLFlBQVk7S0FDYjs7RUFFSDtJQUNFLDhCQUE4QjtJQUM5QixnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxpQkFBaUI7SUFDakIsV0FBVztHQUNaO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjtDQUNGXCIsXCJmaWxlXCI6XCJKZWV0LnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnJvb3Qge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYzRjNmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbn1cXG5cXG4uaW50cm8ge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xcbiAgbWFyZ2luLWJvdHRvbTogNSU7XFxufVxcblxcbi5pbnRyb0xlZnRQYW5lIHtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgcGFkZGluZy10b3A6IDUlICFpbXBvcnRhbnQ7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5qZWV0IHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5jb2FjaGluZ0luc3RpdHV0ZXMge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmludHJvQ29udGVudCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICB3aWR0aDogNDIwcHg7XFxufVxcblxcbi5pbnRyb1JpZ2h0UGFuZSB7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG59XFxuXFxuLmludHJvUmlnaHRQYW5lIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbi5jb250ZW50IHtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbn1cXG5cXG4uY29udGVudEltYWdlRGl2IHtcXG4gIGhlaWdodDogMTM2cHg7XFxuICB3aWR0aDogMTM2cHg7XFxufVxcblxcbi5zY3JlZW5JbWFnZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNjhweDtcXG59XFxuXFxuLmhlYWRpbmdDb250ZW50IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4ubWFpbkNvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS42O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICBoZWlnaHQ6IDM4cHg7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG4gIHBhZGRpbmc6IDIlO1xcbn1cXG5cXG4udGVzdGltb25pYWxzVGl0bGU6OmFmdGVyIHtcXG4gIGNvbnRlbnQ6ICcnO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiAycHg7XFxuICBib3JkZXItdG9wOiBzb2xpZCAzcHggI2VjNGM2ZjtcXG4gIHBhZGRpbmctdG9wOiAxMnB4O1xcbn1cXG5cXG4uY29udGVudFN1YmRpdiB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5qZWVEZXRhaWxzQ29udGFpbmVyIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDMwMHB4ICFpbXBvcnRhbnQ7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLWJvdHRvbTogNSU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2UzZTVmO1xcbn1cXG5cXG4uamVlRGV0YWlsc0NvbnRhaW5lciAuc3ViU2VjdGlvbiB7XFxuICAgIHdpZHRoOiAyNSU7XFxuICAgIG1pbi1oZWlnaHQ6IDgwcHggIWltcG9ydGFudDtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcXG4gIH1cXG5cXG4uamVlVGl0bGUge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICNmZmFiMDA7XFxufVxcblxcbi5qZWVEZXNjcmlwdGlvbiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZzogNSU7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGhlaWdodDogNzBweDtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4udGl0bGUyIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG5cXG4udW5kZXJsaW5lIHtcXG4gIG1hcmdpbi10b3A6IDhweDtcXG4gIG1hcmdpbi1sZWZ0OiA0OCU7XFxuICB3aWR0aDogNCU7XFxuICBib3JkZXI6IHNvbGlkIDFweCAjZWM0YzZmO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk2MHB4KSB7XFxuICAucm9vdCB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5jb250ZW50Rm9yU21hbGwge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuY29udGVudEltYWdlRGl2U21hbGwge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gICAgLmNvbnRlbnRJbWFnZURpdlNtYWxsIGltZyB7XFxuICAgICAgaGVpZ2h0OiAxMDBweDtcXG4gICAgICB3aWR0aDogMTAwcHg7XFxuICAgIH1cXG5cXG4gIC5zY3JlZW5JbWFnZVNtYWxsIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmhlYWRpbmdDb250ZW50U21hbGwge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC5tYWluQ29udGVudFNtYWxsIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS42O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAudGVzdGltb25pYWxzVGl0bGUge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxOCU7XFxuICB9XFxuXFxuICAudGVzdGltb25pYWxzVGl0bGU6OmFmdGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNHB4O1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5pbnRyb0xlZnRQYW5lIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuamVldCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jb2FjaGluZ0luc3RpdHV0ZXMge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmctbGVmdDogMTUlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gIH1cXG5cXG4gIC5saW5rIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xcbiAgLmludHJvQ29udGVudCB7XFxuICAgIHdpZHRoOiAzMDBweDtcXG4gIH1cXG5cXG4gIC5qZWVEZXRhaWxzQ29udGFpbmVyIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfVxcblxcbiAgICAuamVlRGV0YWlsc0NvbnRhaW5lciAuc3ViU2VjdGlvbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgIH1cXG5cXG4gIC5qZWVUaXRsZSB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuamVlRGVzY3JpcHRpb24ge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDtcXG4gICAgcGFkZGluZy1sZWZ0OiAyMCU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcXG4gIH1cXG5cXG4gIC50aXRsZTIge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC51bmRlcmxpbmUge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcblxcbiAgLmplZVRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMHB4O1xcbiAgfVxcblxcbiAgLmplZURlc2NyaXB0aW9uIHtcXG4gICAgZm9udC1zaXplOiAxOHB4O1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicm9vdFwiOiBcIkplZXQtcm9vdC0ybWdyUVwiLFxuXHRcImludHJvXCI6IFwiSmVldC1pbnRyby0zbnBFdVwiLFxuXHRcImludHJvTGVmdFBhbmVcIjogXCJKZWV0LWludHJvTGVmdFBhbmUtMzViRHhcIixcblx0XCJqZWV0XCI6IFwiSmVldC1qZWV0LTM5SnR4XCIsXG5cdFwiY29hY2hpbmdJbnN0aXR1dGVzXCI6IFwiSmVldC1jb2FjaGluZ0luc3RpdHV0ZXMtMUpPZE9cIixcblx0XCJpbnRyb0NvbnRlbnRcIjogXCJKZWV0LWludHJvQ29udGVudC1SandtaFwiLFxuXHRcImludHJvUmlnaHRQYW5lXCI6IFwiSmVldC1pbnRyb1JpZ2h0UGFuZS0yUFk3elwiLFxuXHRcImNvbnRlbnRcIjogXCJKZWV0LWNvbnRlbnQtM0FldWZcIixcblx0XCJjb250ZW50SW1hZ2VEaXZcIjogXCJKZWV0LWNvbnRlbnRJbWFnZURpdi0xR2hyc1wiLFxuXHRcInNjcmVlbkltYWdlXCI6IFwiSmVldC1zY3JlZW5JbWFnZS0xbEtEY1wiLFxuXHRcImhlYWRpbmdDb250ZW50XCI6IFwiSmVldC1oZWFkaW5nQ29udGVudC0ybnZWbFwiLFxuXHRcIm1haW5Db250ZW50XCI6IFwiSmVldC1tYWluQ29udGVudC0xYURNM1wiLFxuXHRcInRlc3RpbW9uaWFsc1RpdGxlXCI6IFwiSmVldC10ZXN0aW1vbmlhbHNUaXRsZS0zVExmX1wiLFxuXHRcImNvbnRlbnRTdWJkaXZcIjogXCJKZWV0LWNvbnRlbnRTdWJkaXYtMTRQY1VcIixcblx0XCJqZWVEZXRhaWxzQ29udGFpbmVyXCI6IFwiSmVldC1qZWVEZXRhaWxzQ29udGFpbmVyLTJiWGdHXCIsXG5cdFwic3ViU2VjdGlvblwiOiBcIkplZXQtc3ViU2VjdGlvbi0zZDhhWFwiLFxuXHRcImplZVRpdGxlXCI6IFwiSmVldC1qZWVUaXRsZS0xMkVxR1wiLFxuXHRcImplZURlc2NyaXB0aW9uXCI6IFwiSmVldC1qZWVEZXNjcmlwdGlvbi1DTjgzVVwiLFxuXHRcInRpdGxlMlwiOiBcIkplZXQtdGl0bGUyLTFYTFlVXCIsXG5cdFwidW5kZXJsaW5lXCI6IFwiSmVldC11bmRlcmxpbmUtMmUyb2NcIixcblx0XCJjb250ZW50Rm9yU21hbGxcIjogXCJKZWV0LWNvbnRlbnRGb3JTbWFsbC1Pa3lEZFwiLFxuXHRcImNvbnRlbnRJbWFnZURpdlNtYWxsXCI6IFwiSmVldC1jb250ZW50SW1hZ2VEaXZTbWFsbC01OU85VlwiLFxuXHRcInNjcmVlbkltYWdlU21hbGxcIjogXCJKZWV0LXNjcmVlbkltYWdlU21hbGwtM1NTOGdcIixcblx0XCJoZWFkaW5nQ29udGVudFNtYWxsXCI6IFwiSmVldC1oZWFkaW5nQ29udGVudFNtYWxsLTNvc2xmXCIsXG5cdFwibWFpbkNvbnRlbnRTbWFsbFwiOiBcIkplZXQtbWFpbkNvbnRlbnRTbWFsbC1GNWJ2bVwiLFxuXHRcImxpbmtcIjogXCJKZWV0LWxpbmstQ3Z5VkFcIlxufTsiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuLy8gaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XG5pbXBvcnQgU2xpZGVzaG93IGZyb20gJ2NvbXBvbmVudHMvU2xpZGVzaG93JztcbmltcG9ydCBSZXF1ZXN0RGVtbyBmcm9tICdjb21wb25lbnRzL1JlcXVlc3REZW1vJztcbmltcG9ydCBzIGZyb20gJy4vSmVldC5zY3NzJztcblxuY29uc3QgU1RBVFMgPSBbXG4gIHsgdGl0bGU6ICc4MGsrJywgZGVzY3JpcHRpb246ICdTdHVkZW50cyBhdHRlbXB0ZWQgamVlIG1haW5zIGluIDIwMTgnIH0sXG4gIHsgdGl0bGU6ICcxNy41JScsIGRlc2NyaXB0aW9uOiAnY3V0LW9mZiBjbGVhciByYXRlJyB9LFxuICB7IHRpdGxlOiAnMTNrJywgZGVzY3JpcHRpb246ICdTdHVkZW50cyBxdWFsaWZpZWQgamVlIG1haW5zIGluIDIwMTgnIH0sXG4gIHsgdGl0bGU6ICc0MCUnLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIG9mIGFwIGFuZCB0ZWxhbmdhbmEnIH0sXG5dO1xuXG5jb25zdCBDT05URU5UID0gW1xuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ1VuZGVyc3RhbmQgd2hlcmUgZG8geW91ciBzdHVkZW50cyBzdGFuZCcsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnTWFya3MgQW5hbHlzaXMgaGVscHMgeW91IHVuZGVyc3RhbmQgaG93IHlvdXIgc3R1ZGVudHMgYXJlIHBlcmZvcm1pbmcgaW4gYSBzbmFwc2hvdCBhbmQgaWRlbnRpZnkgd2hvIG5lZWRzIGhlbHAgdG8gY2xlYXIgdGhlIGN1dC1vZmYuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3BlcmNlbnQuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW4xLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnSWRlbnRpZnkgc3R1ZGVudOKAmXMgc3RyZW5ndGhzICYgYXJlYXMgb2YgaW1wcm92ZW1lbnQnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0NvbmNlcHQtYmFzZWQgUHJvZmlsZSBoZWxwcyB5b3VyIHN0dWRlbnQgaWRlbnRpZnkgaGlzL2hlciBzdHJvbmcgJiB3ZWFrIHRvcGljcyBhbmQgZm9ybXMgYSBiYXNpcyBmb3IgaGlzL2hlciBhcmVhcyBvZiBmb2N1cyB0byBpbXByb3ZlIGhpcyBwZXJmb3JtYW5jZS4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvYmx1Yi5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3NjcmVlbjIucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdDb21wYXJlIHBlcmZvcm1hbmNlIGRhdGEgYWNyb3NzIHlvdXIgaW5zdGl0dXRlIGhpZXJhcmNoeScsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnQ29tcGFyaXNvbiBBbmFseXNpcyBoZWxwcyB5b3UgY29tcGFyZSBzdHVkZW50c+KAmSBwZXJmb3JtYW5jZSBiZXR3ZWVuIHN0YXRlcywgY2FtcHVzZXMsIGJhdGNoZXMsIHNlY3Rpb25zLCB0ZXN0cyAmIHRha2UgYXB0IGFjdGlvbnMgb24gcmVkIGFyZWFzLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC93ZWlnaHRzLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuMy5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0NoYW5uZWxpemUgcGVyZm9ybWFuY2UgZGF0YSB1c2luZyByZWxldmFudCBmaWx0ZXJzJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdDb250ZXh0dWFsaXplIHlvdXIgc3R1ZGVudHPigJkgcGVyZm9ybWFuY2UgZGF0YSB1c2luZyBwcmUtc2V0IGZpbHRlcnMgYmFzZWQgb24geW91ciBpbnN0aXR1dGXigJlzIGhpZXJhcmNoeSBhbmQgbG9vayBhdCBhbmFseXNpcyBmaWx0ZXJlZCB0byB0aGUgc2VsZWN0aW9uLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9ncm91cC5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3NjcmVlbjQucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdEb3dubG9hZGFibGUgcmVwb3J0cyB0byB1bmRlcnN0YW5kIHN0dWRlbnRz4oCZIHBlcmZvcm1hbmNlJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdFeHRlbnNpdmUgZXhjZWwgcmVwb3J0cyBvZiBzdHVkZW50c+KAmSBwZXJmb3JtYW5jZSBkYXRhIHdoaWNoIGNhbiBiZSBzb3J0ZWQgb3IgZmlsdGVyZWQgaW4gdGhlIHdheSB5b3Ugd2FudCB0byBsb29rIGF0IHRoZW0uJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2Rvd25sb2FkLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuNS5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDpcbiAgICAgICdJZGVudGlmeSB0aGUgcXVlc3Rpb25zIG1vc3Qgb2YgeW91ciBzdHVkZW50cyBoYXZlIGdvbmUgd3JvbmcnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0Vycm9yIEFuYWx5c2lzIGhlbHBzIHlvdSBkZXRlcm1pbmUgdGhlIHF1ZXN0aW9ucyBpbiBhIHRlc3Qgd2hpY2ggbWFqb3JpdHkgb2YgeW91ciBzdHVkZW50cyBoYXZlIG5vdCBhdHRlbXB0ZWQgb3IgYXR0ZW1wdGVkIHdyb25nbHkgdG8gZW5hYmxlIGVycm9yIHByYWN0aWNlLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9pZGxlLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuNi5wbmcnLFxuICB9LFxuXTtcblxuY29uc3QgU0xJREVTSE9XID0gW1xuICB7XG4gICAgaW1hZ2U6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy90ZWxhbmdhbmEucG5nJyxcbiAgICBuYW1lOiAnTXIuIFNyaW5pdmFzIEthbm5hbicsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBTcmluaXZhcyBNdXJ0aHknLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MnLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICcnLFxuICAgIG5hbWU6ICdNci4gQW5kcmV3IFRhZycsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBNdXNzb2xpbmknLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MnLFxuICB9LFxuXTtcblxuY2xhc3MgSmVldCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIC8vIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gIC8vXG4gIC8vIH07XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgUmVhY3RHQS5pbml0aWFsaXplKHdpbmRvdy5BcHAuZ29vZ2xlVHJhY2tpbmdJZCwge1xuICAgICAgZGVidWc6IGZhbHNlLFxuICAgIH0pO1xuICAgIFJlYWN0R0EucGFnZXZpZXcod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBkZXNjcmlwdGlvbiBSZXR1cm5zIHdoYXQgYWxpZ25tZW50IHNob3VsZCB0aGUgY29udGVudCB0YWtlIGVnLiBmbGV4LXN0YXJ0IGZsZXggZW5kIGNlbnRlclxuICAgKiBAcGFyYW0gY29udGVudEluZGV4LGtleVxuICAgKiBAYXV0aG9yIFN1c2hydXRoXG4gICAqICovXG4gIG1ha2VBbGlnbm1lbnQgPSAoY29udGVudEluZGV4LCBrZXkpID0+IHtcbiAgICBpZiAoY29udGVudEluZGV4ICUgMiA9PT0ga2V5KSB7XG4gICAgICBpZiAoa2V5ID09PSAwKSB7XG4gICAgICAgIHJldHVybiAnZmxleC1zdGFydCc7XG4gICAgICB9XG4gICAgICByZXR1cm4gJ2ZsZXgtZW5kJztcbiAgICB9XG4gICAgcmV0dXJuICdjZW50ZXInO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB2aWV3ID0gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm9vdH0gcm93YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvfSByb3dgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb0xlZnRQYW5lfSBjb2wgbTQgczEyYH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5qZWV0fT5HZXRSYW5rczwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29hY2hpbmdJbnN0aXR1dGVzfT5Gb3IgQ29hY2hpbmcgSW5zdGl0dWVzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbnRyb0NvbnRlbnR9PlxuICAgICAgICAgICAgICBHZXRSYW5rcyBpcyBhIGxlYXJuaW5nIEFuYWx5dGljcyA8YnIgLz5cbiAgICAgICAgICAgICAgcGxhdGZvcm0gd2hpY2ggZW1wb3dlcnNcbiAgICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICAgIGNvYWNoaW5nIGluc3RpdHV0ZXMgdG9cbiAgICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICAgIGluY3JlYXNlIG51bWJlciBvZiB0aGVpclxuICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgc3R1ZGVudHMgY2xlYXJpbmcgY3V0LW9mZiBpblxuICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgY29tcGV0aXRpdmUgZXhhbXMuXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL3JlcXVlc3QtZGVtb1wiIGNsYXNzTmFtZT17cy5saW5rfT5cbiAgICAgICAgICAgICAgPGJ1dHRvbj5SRVFVRVNUIEEgREVNTzwvYnV0dG9uPlxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvUmlnaHRQYW5lfSBjb2wgbTggczEyYH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvcHJvZHVjdHMvamVldC9KRUVULnBuZ1wiIGFsdD1cIlwiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5qZWVEZXRhaWxzQ29udGFpbmVyfSByb3dgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy50aXRsZTJ9YH0+XG4gICAgICAgICAgICBPdXIgU3RhdHNcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnVuZGVybGluZX1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIHtTVEFUUy5tYXAoKGRldGFpbHMsIGluZGV4KSA9PiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zdWJTZWN0aW9ufWB9PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmplZVRpdGxlfWB9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlclJpZ2h0OlxuICAgICAgICAgICAgICAgICAgICBpbmRleCA8IFNUQVRTLmxlbmd0aCAtIDFcbiAgICAgICAgICAgICAgICAgICAgICA/ICdzb2xpZCAxcHggcmdiYSgxMzksIDEzOSwgMjIzLCAwLjMpJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJycsXG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtkZXRhaWxzLnRpdGxlfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5qZWVEZXNjcmlwdGlvbn1gfVxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBib3JkZXJSaWdodDpcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPCBTVEFUUy5sZW5ndGggLSAxXG4gICAgICAgICAgICAgICAgICAgICAgPyAnc29saWQgMXB4IHJnYmEoMTM5LCAxMzksIDIyMywgMC4zKSdcbiAgICAgICAgICAgICAgICAgICAgICA6ICcnLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7ZGV0YWlscy5kZXNjcmlwdGlvbi50b1VwcGVyQ2FzZSgpfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAge0NPTlRFTlQubWFwKChjb250ZW50LCBjb250ZW50SW5kZXgpID0+IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuY29udGVudH0gcm93IGhpZGUtb24teHNgfT5cbiAgICAgICAgICAgICAge1swLCAxXS5tYXAoa2V5ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgICBjb250ZW50SW5kZXggJSAyID09PSBrZXlcbiAgICAgICAgICAgICAgICAgICAgICA/IGBjb2wgbTUgbDUgJHtzLmNvbnRlbnRTdWJkaXZ9YFxuICAgICAgICAgICAgICAgICAgICAgIDogYGNvbCBtNyBsNyAke3MuY29udGVudFN1YmRpdn1gXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogdGhpcy5tYWtlQWxpZ25tZW50KGNvbnRlbnRJbmRleCwga2V5KSxcbiAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge2NvbnRlbnRJbmRleCAlIDIgPT09IGtleSA/IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50SW1hZ2VEaXZ9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBhbHQ9XCJcIiBzcmM9e2NvbnRlbnQuaWNvbn0gLz5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nQ29udGVudH0+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Y29udGVudC5oZWFkZXJDb250ZW50fVxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1haW5Db250ZW50fT57Y29udGVudC5tYWluQ29udGVudH08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zY3JlZW5JbWFnZX0+XG4gICAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNyYz17Y29udGVudC5zY3JlZW5JbWFnZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiODcuNiVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PVwiNTQuNDUlXCJcbiAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50Rm9yU21hbGx9IHJvdyBoaWRlLW9uLW0tYW5kLXVwYH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRJbWFnZURpdlNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17Y29udGVudC5pY29ufSAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ0NvbnRlbnRTbWFsbH0+XG4gICAgICAgICAgICAgICAge2NvbnRlbnQuaGVhZGVyQ29udGVudH1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1haW5Db250ZW50U21hbGx9Pntjb250ZW50Lm1haW5Db250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zY3JlZW5JbWFnZVNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfVxuICAgICAgICAgICAgICAgICAgd2lkdGg9XCI4Ny42JVwiXG4gICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI1NC40NSVcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkpfVxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3sgcGFkZGluZ1RvcDogJzY0cHgnLCBtYXJnaW5Cb3R0b206ICc1JScgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJyb3cgaGlkZVwiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0aW1vbmlhbHNUaXRsZX0+V2h5IG91ciBjbGllbnRzIGxvdmUgRWduaWZ5PC9kaXY+XG4gICAgICAgICAgPFNsaWRlc2hvdyBkYXRhPXtTTElERVNIT1d9IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8UmVxdWVzdERlbW8gLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gICAgcmV0dXJuIHZpZXc7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShKZWV0KTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSmVldC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0plZXQuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0plZXQuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IEplZXQgZnJvbSAnLi9KZWV0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnknLFxuICAgIGNodW5rczogWydKZWV0J10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8SmVldCAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBRUE7QUFFQTtBQUNBO0FBTkE7QUFVQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQVFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUF6QkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBOzs7Ozs7O0FBZUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBREE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFLQTtBQUNBO0FBREE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQTFKQTtBQUNBO0FBMkpBOzs7Ozs7O0FDelBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFTQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==