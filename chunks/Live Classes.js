require("source-map-support").install();
exports.ids = ["Live Classes"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Teach/Teach.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Teach-breadcrum-11xX5 {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.Teach-breadcrum-11xX5 span {\n  font-weight: 600;\n}\n\n.Teach-toggle_outer-VIsVE {\n  width: 32px;\n  height: 17.8px;\n  border-radius: 24px;\n  background-color: #f36;\n  padding: 2px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: -10px;\n}\n\n.Teach-toggle_on-2JbJd {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-toggle_inner-3gUru {\n  width: 14px;\n  height: 14px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n/*   position: absolute;\n  width: 24px;\n  height: 36px;\n  bottom: 87px;\n  right: 560px;\n\n  img {\n    width: 100%;\n    height: 100%;\n    object-fit: contain;\n  }\n} */\n\n.Teach-headerbackgroundmobile-2Z0QY {\n  position: absolute;\n  bottom: 0;\n  right: 64px;\n  width: 445px;\n  height: 584px;\n}\n\n.Teach-headerbackgroundmobile-2Z0QY img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.Teach-authorimgbox-28-2r {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-authorimgbox-28-2r img {\n    width: 100%;\n    height: 100%;\n    max-width: 640px;\n    max-height: 560px;\n    justify-self: flex-end;\n  }\n\n/* .viewmore.mobile {\n  font-size: 14px;\n  line-height: 24px;\n  width: 100%;\n  color: #0076ff;\n  display: none;\n  justify-content: center;\n  align-items: center;\n  margin-top: 30px;\n\n  img {\n    width: 20px;\n    height: 20px;\n    margin-left: 5px;\n    margin-top: 1px;\n  }\n} */\n\n.Teach-contentPart-1lSgK {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 60%;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-contentPart-1lSgK .Teach-topicImage-1CM7j {\n    width: 72px;\n    height: 72px;\n    padding: 16px;\n    margin-right: 24px;\n    background: #fff;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n  }\n\n.Teach-contentPart-1lSgK .Teach-topicImage-1CM7j img {\n      width: 42px;\n      height: 42px;\n    }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n\n    /* .viewmore {\n      font-size: 20px;\n      line-height: 32px;\n      width: 100%;\n      color: #0076ff;\n      display: flex;\n      align-items: center;\n      margin-top: 24px;\n\n      img {\n        width: 24px;\n        height: 24px;\n        margin-left: 5px;\n      }\n    } */\n  }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-section_title-2nyBd {\n      display: block;\n      margin: 12px 0;\n      font-size: 40px;\n      line-height: 48px;\n      letter-spacing: -1.2px;\n      color: #25282b;\n      font-weight: 600;\n    }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-textcontent-3s5Dz {\n      font-size: 20px;\n      line-height: 32px;\n      color: #25282b;\n      max-width: 370px;\n    }\n\n.Teach-customers_container-3dTy8 {\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  height: 560px;\n}\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR {\n    -ms-flex-order: 0;\n        order: 0;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: start;\n        align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n  }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-sriChaitanyaText-1HRt2 {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-author_title-3HNCy {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-about_author-3sn5h {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-allcustomers-2b8NO {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: row;\n          flex-direction: row;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-allcustomers-2b8NO img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.Teach-availableContainer-2e4mP {\n  background-color: #fff;\n  padding: 80px 113px 0 164px;\n  padding: 5rem 113px 0 164px;\n  // max-width: 1300px;\n}\n\n.Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-desktopImage-n9mcs {\n      width: 648px;\n      height: 328px;\n      margin-top: 35px;\n    }\n\n.Teach-availableTitle-28MiM {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Teach-available-k7Ani {\n  color: #f36;\n}\n\n.Teach-availableContentSection-JZlaF {\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.Teach-platformContainer-1IANc {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.Teach-platform-1GcJZ {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.Teach-platformOs-2DF6E {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.Teach-row-1VSyr {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.Teach-store-IZcDQ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.Teach-store-IZcDQ .Teach-playstore-PXfhG {\n    width: 140px;\n    height: 42px;\n    margin-right: 16px;\n  }\n\n.Teach-store-IZcDQ .Teach-appstore-2HMPf {\n    width: 140px;\n    height: 42px;\n  }\n\n.Teach-displayClients-1bg_Q span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ .Teach-topCard-2ZuD4 img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Teach-teachContainer-2Q-YB {\n  height: 680px;\n  padding: 24px 64px;\n  background-color: #ffece0;\n  position: relative;\n  cursor: alias;\n}\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as {\n    font-size: 48px;\n    line-height: 66px;\n    color: #25282b;\n    max-width: 542px;\n    margin: 12px 0 0 0;\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N {\n      width: 185px;\n      display: inline;\n      position: relative;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N img {\n        position: absolute;\n        bottom: -8px;\n        left: -8px;\n        width: 100%;\n        height: 8px;\n        max-width: 225px;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi {\n    margin-top: 64px;\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-requestDemo-AboHC {\n      border-radius: 4px;\n      background-color: #3fc;\n      font-size: 20px;\n      font-weight: 600;\n      line-height: 1.5;\n      cursor: pointer;\n      color: #000;\n      padding: 16px 24px;\n      width: -webkit-max-content;\n      width: -moz-max-content;\n      width: max-content;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 .Teach-whatsapp-34spS {\n        width: -webkit-fit-content;\n        width: -moz-fit-content;\n        width: fit-content;\n        font-size: 20px;\n        cursor: pointer;\n        font-weight: 600;\n        line-height: 1.5;\n        color: #25282b !important;\n        margin: 0 8px 0 32px;\n        padding-bottom: 0;\n        opacity: 0.6;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 img {\n        width: 32px;\n        height: 32px;\n      }\n\n/* .actionsWrapper {\n    position: absolute;\n    bottom: 26px;\n    width: 100%;\n    display: flex;\n\n    .action {\n      width: fit-content;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin-right: 24px;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        opacity: 0.7;\n        text-align: left;\n        margin-left: 8px;\n      }\n\n      .actionimgbox {\n        width: 24px;\n        height: 24px;\n        background-color: #fff;\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n\n        img {\n          width: 10px;\n          height: 9px;\n          background-color: #fff;\n        }\n      }\n    }\n  } */\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n\n    /* .featuresSection {\n      display: flex;\n      align-items: center;\n      margin-left: 30%;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        margin-right: 32px;\n        font-weight: 500;\n      }\n    } */\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 {\n        width: 40px;\n        height: 40px;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachSectionName-1AdS4 {\n        margin-left: 8px;\n        font-size: 20px;\n        line-height: 30px;\n        color: #ff6400;\n        font-weight: bold;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-contentText-3eX56 {\n    font-size: 20px;\n    line-height: 40px;\n    color: #25282b;\n    font-weight: normal;\n    margin: 16px 0 0 0;\n    max-width: 687px;\n  }\n\n.Teach-downloadApp-3tWrv {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.Teach-downloadApp-3tWrv .Teach-downloadText-2YOcN {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.Teach-downloadApp-3tWrv .Teach-playStoreIcon-3YdD8 {\n  width: 132px;\n  height: 40px;\n}\n\n.Teach-displayClients-1bg_Q {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.Teach-displayClients-1bg_Q h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq .Teach-client-Ii9JF {\n  width: 172px;\n  height: 112px;\n}\n\n.Teach-displayClients-1bg_Q span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Teach-displayClients-1bg_Q span a:hover {\n  text-decoration: underline;\n}\n\n.Teach-achievedContainer-Aa5ks {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedHeading-2yxR4 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-clients-1hUFi {\n  background-color: #fff3eb;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-students-PAFZG {\n  background-color: #f7effe;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-tests-27DEs {\n  background-color: #ffebf0;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-questions-WBnlD {\n  background-color: #ebffef;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-questionsHighlight-37La9 {\n  color: #00ac26;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-testsHighlight-2Hb9O {\n  color: #f36;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-studentsHighlight-2H5F_ {\n  color: #8c00fe;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-clientsHighlight-1ankD {\n  color: #f60;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-subText-3IRHK {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.Teach-section_container_reverse-1yEDY {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    padding: 0 64px;\n    margin: auto;\n    max-width: 1700px;\n  }\n\n.Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr .Teach-contentPart-1lSgK {\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n    }\n\n.Teach-section_container-cyyTN {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.Teach-section_container-cyyTN .Teach-section_contents-1NRDr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    padding: 0 64px;\n    max-width: 1700px;\n    margin: auto;\n  }\n\n.Teach-teachContent-19QTk {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  max-width: 337px;\n  vertical-align: top;\n  margin-bottom: 24px;\n}\n\n.Teach-teachContent-19QTk .Teach-getranks-3JcXa {\n    width: 89px;\n    height: 26px;\n    margin-bottom: -5px;\n  }\n\n.Teach-teachContent-19QTk .Teach-zoom-3RXu- {\n    width: 64px;\n    height: 18px;\n  }\n\n.Teach-imagePart-3NAi0 {\n  width: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n  position: relative;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ .Teach-topCard-2ZuD4 {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.Teach-imagePart-3NAi0 .Teach-bottomCircle-1QU8r {\n  position: absolute;\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n}\n\n.Teach-imagePart-3NAi0 .Teach-topCircle-2waHP {\n  position: absolute;\n  width: 64px;\n  height: 64px;\n  border-radius: 50%;\n}\n\n.Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      background-color: #ffe0e8;\n      width: 88px;\n      height: 88px;\n      bottom: -44px;\n      left: 79px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      background-color: #f2e5fe;\n      width: 88px;\n      height: 88px;\n      top: -44px;\n      right: 50px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      width: 88px;\n      height: 88px;\n      background-color: #ffe0e8;\n      bottom: -44px;\n      left: 48px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      width: 88px;\n      height: 88px;\n      background-color: #3fc;\n      top: -44px;\n      right: 67px;\n      opacity: 0.3;\n    }\n\n.Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      width: 88px;\n      height: 88px;\n      background-color: #feb546;\n      bottom: -44px;\n      left: 68px;\n      opacity: 0.2;\n    }\n\n.Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      width: 88px;\n      height: 88px;\n      background-color: #0076ff;\n      top: -44px;\n      right: 36px;\n      opacity: 0.2;\n    }\n\n.Teach-allcustomers-2b8NO p {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-content-s_zCd p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.Teach-tableContainer-32ppr {\n  padding: 56px 0;\n  background-color: #f7f7f7;\n}\n\n.Teach-tableContainer-32ppr h1 {\n    text-align: center;\n    margin: 0 0 24px 0;\n  }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU {\n    width: 956px;\n    display: grid;\n    grid-template-columns: 1fr 1fr;\n    margin: auto;\n  }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk {\n      padding: 24px 32px;\n      font-size: 18px;\n      display: -ms-flexbox;\n      display: flex;\n      border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz {\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-align: center;\n            align-items: center;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-comparisionText-3FmhP {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD {\n          width: 20px;\n          height: 20px;\n          margin-left: 12px;\n          border-radius: 4px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-positive-2nQ7l {\n          background-color: #cfc;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-negative-3D6w2 {\n          background-color: #ffd6d6;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 {\n        width: 5%;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct {\n          width: 8px;\n          height: 8px;\n          border-radius: 50%;\n          background-color: #25282b;\n          margin-top: 12px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct.Teach-red-zeFql {\n          background-color: #0c0;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ {\n        width: 95%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-direction: column;\n            flex-direction: column;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n          margin-bottom: 16px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(odd) {\n      background-color: #ffebeb;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(even) {\n      background-color: #ebffeb;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(1),\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(2) {\n      background-color: #f7f7f7;\n      padding: 8px 16px;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(3),\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(4) {\n      background-color: rgba(37, 40, 43, 0.1);\n      font-size: 24px;\n      line-height: 32px;\n      color: #25282b;\n      text-align: left;\n      padding: 8px 16px;\n      font-weight: 600;\n    }\n\n@media only screen and (max-width: 1280px) {\n  .Teach-section_contents-1NRDr {\n    padding: 0 40px;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .Teach-availableContainer-2e4mP {\n    padding: 5rem 64px 0 64px;\n  }\n    .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n      grid-template-columns: 1fr 1fr;\n    }\n}\n\n@media only screen and (max-width: 990px) {\n  .Teach-breadcrum-11xX5 {\n    display: none;\n  }\n\n  /* .toggle_outer {\n    width: 32px;\n    height: 18px;\n    margin-top: -8px;\n  }\n\n  .toggle_inner {\n    width: 14px;\n    height: 14px;\n  }\n\n  .toggleAtRight {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n  }\n\n  .toggleAtLeft {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n  } */\n\n  /* .mouseicon {\n    display: none;\n  } */\n\n  .Teach-section_container-cyyTN {\n    padding: 32px 0;\n  }\n\n    .Teach-section_container-cyyTN .Teach-section_contents-1NRDr {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .Teach-section_container-cyyTN .Teach-section_contents-1NRDr .Teach-section_title-2nyBd {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .Teach-section_container_reverse-1yEDY {\n    padding: 32px 0;\n  }\n\n    .Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr .Teach-section_title-2nyBd {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .Teach-contentPart-1lSgK {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n    .Teach-contentPart-1lSgK .Teach-topicImage-1CM7j {\n      width: 48px;\n      height: 48px;\n      margin: auto;\n      padding: 10px;\n    }\n\n      .Teach-contentPart-1lSgK .Teach-topicImage-1CM7j img {\n        width: 28px;\n        height: 28px;\n      }\n\n    .Teach-contentPart-1lSgK .Teach-content-s_zCd {\n      max-width: none;\n\n      /* .viewmore {\n        display: none;\n        justify-content: center;\n      } */\n    }\n\n      .Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-section_title-2nyBd {\n        text-align: center;\n        font-size: 24px;\n      }\n\n      .Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-textcontent-3s5Dz {\n        display: block;\n        width: 100%;\n        max-width: 600px;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n        margin: auto;\n      }\n\n  .Teach-teachContent-19QTk {\n    font-size: 14px;\n    line-height: 24px;\n    vertical-align: top;\n    margin-bottom: 12px;\n    max-width: 320px;\n    text-align: center;\n  }\n\n    .Teach-teachContent-19QTk .Teach-getranks-3JcXa {\n      width: 60px;\n      height: 18px;\n      margin-bottom: 0;\n    }\n\n    .Teach-teachContent-19QTk .Teach-zoom-3RXu- {\n      width: 43px;\n      height: 11px;\n      margin-bottom: 2px;\n    }\n\n  .Teach-imagePart-3NAi0 {\n    width: 100%;\n    margin-top: 40px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n\n    /* .viewmore.mobile {\n      margin-bottom: 32px;\n      display: flex;\n    } */\n  }\n\n    .Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ {\n      width: 320px;\n      height: 180px;\n    }\n      .Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -24px;\n        left: 254px;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        opacity: 0.2;\n        bottom: -16px;\n        left: 61px;\n      }\n      .Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        top: -16px;\n        left: 28px;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #3fc;\n        opacity: 0.2;\n        bottom: -24px;\n        left: 236px;\n      }\n      .Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #f2e5fe;\n        left: 243px;\n        top: -16px;\n        opacity: 1;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        right: 227px;\n        bottom: -24px;\n      }\n\n  .Teach-headerbackgroundmobile-2Z0QY {\n    width: 237px;\n    height: 340px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .Teach-customers_container-3dTy8 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n    .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR {\n      -ms-flex-order: 2;\n          order: 2;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n    }\n\n      .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-sriChaitanyaText-1HRt2 {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n        .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-about_author-3sn5h {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n\n    .Teach-customers_container-3dTy8 .Teach-authorimgbox-28-2r {\n      height: 50%;\n      width: 100%;\n      -ms-flex-order: 1;\n          order: 1;\n    }\n\n      .Teach-customers_container-3dTy8 .Teach-authorimgbox-28-2r img {\n        max-width: none;\n        max-height: none;\n      }\n\n  .Teach-teachContainer-2Q-YB .Teach-downloadApp-3tWrv {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .Teach-displayClients-1bg_Q span {\n    max-width: 307px;\n  }\n\n  .Teach-teachContainer-2Q-YB {\n    height: 1012px;\n    padding: 32px 16px;\n  }\n\n    .Teach-teachContainer-2Q-YB .Teach-heading-1K5as {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 24px auto 0 auto;\n    }\n\n      .Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N {\n        width: 120px;\n      }\n\n        .Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .Teach-teachContainer-2Q-YB .Teach-contentText-3eX56 {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-requestDemo-AboHC {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 .Teach-whatsapp-34spS {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 img {\n          width: 24px;\n          height: 24px;\n        }\n\n    /* .actionsWrapper {\n      right: 0;\n      width: fit-content;\n      margin-right: 16px;\n\n      .action {\n        margin-left: 12px;\n        margin-right: 0;\n\n        span {\n          display: none;\n        }\n\n        .actionimgbox {\n          margin: 0;\n        }\n      }\n    } */\n\n    .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 {\n          width: 32px;\n          height: 32px;\n        }\n\n        .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachSectionName-1AdS4 {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .Teach-displayClients-1bg_Q {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .Teach-displayClients-1bg_Q h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq .Teach-client-Ii9JF {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .Teach-achievedContainer-Aa5ks {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .Teach-achievedContainer-Aa5ks .Teach-achievedHeading-2yxR4 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .Teach-availableContainer-2e4mP {\n    padding: 32px 32px 0 32px;\n  }\n\n    .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ {\n      grid-template-columns: 1fr;\n    }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-availableTitle-28MiM {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc .Teach-platform-1GcJZ {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc .Teach-platformOs-2DF6E {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc:last-child {\n          margin-right: 0;\n        }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-desktopImage-n9mcs {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ .Teach-playstore-PXfhG {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ .Teach-appstore-2HMPf {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .Teach-tableContainer-32ppr {\n    padding: 40px 0;\n    background-color: #f7f7f7;\n  }\n\n    .Teach-tableContainer-32ppr h1 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 16px 32px 16px;\n    }\n\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU {\n      width: 328px;\n      display: grid;\n      grid-template-columns: 1fr;\n      margin: auto;\n    }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk {\n        padding: 16px 20px 16px 16px;\n        font-size: 16px;\n        display: -ms-flexbox;\n        display: flex;\n        border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n      }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz {\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-align: center;\n              align-items: center;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-comparisionText-3FmhP {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD {\n            width: 16px;\n            height: 16px;\n            margin-left: 8px;\n            border-radius: 4px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-positive-2nQ7l {\n            background-color: #cfc;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-negative-3D6w2 {\n            background-color: #ffd6d6;\n          }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ {\n          width: 92%;\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-direction: column;\n              flex-direction: column;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n            margin-bottom: 12px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span:last-child {\n            margin-bottom: 0;\n          }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 {\n          width: 8%;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct {\n            width: 8px;\n            height: 8px;\n            border-radius: 50%;\n            background-color: #25282b;\n            margin-top: 8px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 span:last-child {\n            margin-bottom: 0;\n          }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(1) {\n        background: none;\n        margin-top: 16px;\n        padding: 8px 0;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(2) {\n        grid-row: 1/2;\n        background: none;\n        padding: 8px 0;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(3) {\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(4) {\n        grid-row: 2/3;\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(6) {\n        grid-row: 3/4;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(8) {\n        grid-row: 4/5;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(10) {\n        grid-row: 5/6;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(12) {\n        grid-row: 6/7;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(13) {\n        border-bottom: none;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(14) {\n        grid-row: 7/8;\n        border-bottom: none;\n      }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/Teach.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,YAAY;EACZ,eAAe;EACf,oBAAoB;EACpB,uBAAuB;EACvB,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,uBAAuB;CACxB;;AAED;;;;;;;;;;;IAWI;;AAEJ;EACE,mBAAmB;EACnB,UAAU;EACV,YAAY;EACZ,aAAa;EACb,cAAc;CACf;;AAED;IACI,YAAY;IACZ,aAAa;IACb,uBAAuB;OACpB,oBAAoB;GACxB;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;IACI,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,kBAAkB;IAClB,uBAAuB;GACxB;;AAEH;;;;;;;;;;;;;;;;IAgBI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,WAAW;EACX,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;IACI,YAAY;IACZ,aAAa;IACb,cAAc;IACd,mBAAmB;IACnB,iBAAiB;IACjB,mBAAmB;IACnB,mDAAmD;YAC3C,2CAA2C;GACpD;;AAEH;MACM,YAAY;MACZ,aAAa;KACd;;AAEL;IACI,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;;IAEjB;;;;;;;;;;;;;;QAcI;GACL;;AAEH;MACM,eAAe;MACf,eAAe;MACf,gBAAgB;MAChB,kBAAkB;MAClB,uBAAuB;MACvB,eAAe;MACf,iBAAiB;KAClB;;AAEL;MACM,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;MACf,iBAAiB;KAClB;;AAEL;EACE,uBAAuB;EACvB,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,cAAc;CACf;;AAED;IACI,kBAAkB;QACd,SAAS;IACb,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,mBAAmB;IACnB,WAAW;GACZ;;AAEH;MACM,aAAa;MACb,aAAa;MACb,mBAAmB;MACnB,oBAAoB;KACrB;;AAEL;QACQ,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;AAEP;MACM,gBAAgB;MAChB,kBAAkB;MAClB,iBAAiB;MACjB,oBAAoB;MACpB,iBAAiB;KAClB;;AAEL;MACM,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;KAC5B;;AAEL;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,iBAAiB;QACjB,YAAY;QACZ,mBAAmB;OACpB;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;AAEP;MACM,gBAAgB;MAChB,kBAAkB;MAClB,iBAAiB;MACjB,YAAY;MACZ,qBAAqB;MACrB,cAAc;MACd,wBAAwB;UACpB,oBAAoB;MACxB,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,gBAAgB;QAChB,iBAAiB;QACjB,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;AAEP;EACE,uBAAuB;EACvB,4BAA4B;EAC5B,4BAA4B;EAC5B,qBAAqB;CACtB;;AAED;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;MACd,iBAAiB;KAClB;;AAEL;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,YAAY;CACb;;AAED;EACE,qBAAqB;MACjB,4BAA4B;CACjC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,sBAAsB;EACtB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,qBAAqB;MACjB,4BAA4B;EAChC,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,aAAa;IACb,aAAa;IACb,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,cAAc;CACf;;AAED;IACI,gBAAgB;IAChB,kBAAkB;IAClB,eAAe;IACf,iBAAiB;IACjB,mBAAmB;GACpB;;AAEH;MACM,aAAa;MACb,gBAAgB;MAChB,mBAAmB;KACpB;;AAEL;QACQ,mBAAmB;QACnB,aAAa;QACb,WAAW;QACX,YAAY;QACZ,YAAY;QACZ,iBAAiB;OAClB;;AAEP;IACI,iBAAiB;IACjB,qBAAqB;IACrB,cAAc;IACd,YAAY;IACZ,qBAAqB;QACjB,4BAA4B;IAChC,uBAAuB;QACnB,oBAAoB;GACzB;;AAEH;MACM,mBAAmB;MACnB,uBAAuB;MACvB,gBAAgB;MAChB,iBAAiB;MACjB,iBAAiB;MACjB,gBAAgB;MAChB,YAAY;MACZ,mBAAmB;MACnB,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;KACpB;;AAEL;MACM,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,qBAAqB;MACrB,cAAc;MACd,qBAAqB;UACjB,4BAA4B;MAChC,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,2BAA2B;QAC3B,wBAAwB;QACxB,mBAAmB;QACnB,gBAAgB;QAChB,gBAAgB;QAChB,iBAAiB;QACjB,iBAAiB;QACjB,0BAA0B;QAC1B,qBAAqB;QACrB,kBAAkB;QAClB,aAAa;OACd;;AAEP;QACQ,YAAY;QACZ,aAAa;OACd;;AAEP;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAsCM;;AAEN;IACI,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;;IAEjB;;;;;;;;;;;;QAYI;GACL;;AAEH;MACM,qBAAqB;MACrB,cAAc;MACd,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,YAAY;QACZ,aAAa;OACd;;AAEP;UACU,YAAY;UACZ,aAAa;UACb,uBAAuB;aACpB,oBAAoB;SACxB;;AAET;QACQ,iBAAiB;QACjB,gBAAgB;QAChB,kBAAkB;QAClB,eAAe;QACf,kBAAkB;OACnB;;AAEP;IACI,gBAAgB;IAChB,kBAAkB;IAClB,eAAe;IACf,oBAAoB;IACpB,mBAAmB;IACnB,iBAAiB;GAClB;;AAEH;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,aAAa;EACb,aAAa;CACd;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,sCAAsC;EACtC,UAAU;EACV,UAAU;EACV,aAAa;CACd;;AAED;EACE,aAAa;EACb,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,6BAA6B;EAC7B,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,sBAAsB;EACtB,+DAA+D;UACvD,uDAAuD;EAC/D,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;;;;;;;;;;;;;;IAcI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;EAC1B,iBAAiB;EACjB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,wBAAwB;QACpB,oBAAoB;IACxB,YAAY;IACZ,gBAAgB;IAChB,aAAa;IACb,kBAAkB;GACnB;;AAEH;MACM,qBAAqB;UACjB,4BAA4B;KACjC;;AAEL;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;EACvB,iBAAiB;EACjB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,gCAAgC;QAC5B,4BAA4B;IAChC,YAAY;IACZ,gBAAgB;IAChB,kBAAkB;IAClB,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,YAAY;IACZ,aAAa;IACb,oBAAoB;GACrB;;AAEH;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;EACd,mBAAmB;EACnB,mDAAmD;UAC3C,2CAA2C;EACnD,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;MACM,0BAA0B;MAC1B,YAAY;MACZ,aAAa;MACb,cAAc;MACd,WAAW;KACZ;;AAEL;MACM,0BAA0B;MAC1B,YAAY;MACZ,aAAa;MACb,WAAW;MACX,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,cAAc;MACd,WAAW;KACZ;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,uBAAuB;MACvB,WAAW;MACX,YAAY;MACZ,aAAa;KACd;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,cAAc;MACd,WAAW;MACX,aAAa;KACd;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,WAAW;MACX,YAAY;MACZ,aAAa;KACd;;AAEL;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,uBAAuB;EACvB,eAAe;EACf,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,0BAA0B;CAC3B;;AAED;IACI,mBAAmB;IACnB,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,cAAc;IACd,+BAA+B;IAC/B,aAAa;GACd;;AAEH;MACM,mBAAmB;MACnB,gBAAgB;MAChB,qBAAqB;MACrB,cAAc;MACd,2CAA2C;KAC5C;;AAEL;QACQ,qBAAqB;QACrB,cAAc;QACd,uBAAuB;YACnB,oBAAoB;OACzB;;AAEP;UACU,gBAAgB;UAChB,kBAAkB;UAClB,eAAe;SAChB;;AAET;UACU,YAAY;UACZ,aAAa;UACb,kBAAkB;UAClB,mBAAmB;SACpB;;AAET;UACU,uBAAuB;SACxB;;AAET;UACU,0BAA0B;SAC3B;;AAET;QACQ,UAAU;OACX;;AAEP;UACU,WAAW;UACX,YAAY;UACZ,mBAAmB;UACnB,0BAA0B;UAC1B,iBAAiB;SAClB;;AAET;UACU,uBAAuB;SACxB;;AAET;QACQ,WAAW;QACX,qBAAqB;QACrB,cAAc;QACd,2BAA2B;YACvB,uBAAuB;OAC5B;;AAEP;UACU,gBAAgB;UAChB,kBAAkB;UAClB,eAAe;UACf,oBAAoB;SACrB;;AAET;MACM,0BAA0B;KAC3B;;AAEL;MACM,0BAA0B;KAC3B;;AAEL;;MAEM,0BAA0B;MAC1B,kBAAkB;KACnB;;AAEL;;MAEM,wCAAwC;MACxC,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;MACf,iBAAiB;MACjB,kBAAkB;MAClB,iBAAiB;KAClB;;AAEL;EACE;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,0BAA0B;GAC3B;IACC;MACE,+BAA+B;KAChC;CACJ;;AAED;EACE;IACE,cAAc;GACf;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;MAyBI;;EAEJ;;MAEI;;EAEJ;IACE,gBAAgB;GACjB;;IAEC;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,gBAAgB;KACjB;;MAEC;QACE,oBAAoB;QACpB,eAAe;OAChB;;EAEL;IACE,gBAAgB;GACjB;;IAEC;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,gBAAgB;KACjB;;MAEC;QACE,oBAAoB;QACpB,eAAe;OAChB;;EAEL;IACE,YAAY;IACZ,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,aAAa;MACb,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;OACd;;IAEH;MACE,gBAAgB;;MAEhB;;;UAGI;KACL;;MAEC;QACE,mBAAmB;QACnB,gBAAgB;OACjB;;MAED;QACE,eAAe;QACf,YAAY;QACZ,iBAAiB;QACjB,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;QAClB,aAAa;OACd;;EAEL;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;IACpB,oBAAoB;IACpB,iBAAiB;IACjB,mBAAmB;GACpB;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,iBAAiB;KAClB;;IAED;MACE,YAAY;MACZ,aAAa;MACb,mBAAmB;KACpB;;EAEH;IACE,YAAY;IACZ,iBAAiB;IACjB,sBAAsB;QAClB,wBAAwB;IAC5B,2BAA2B;QACvB,uBAAuB;;IAE3B;;;QAGI;GACL;;IAEC;MACE,aAAa;MACb,cAAc;KACf;MACC;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,aAAa;QACb,WAAW;QACX,YAAY;OACb;;MAED;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,aAAa;QACb,cAAc;QACd,WAAW;OACZ;MACD;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,WAAW;QACX,WAAW;OACZ;;MAED;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,cAAc;QACd,YAAY;OACb;MACD;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,YAAY;QACZ,WAAW;QACX,WAAW;OACZ;;MAED;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,aAAa;QACb,cAAc;OACf;;EAEL;IACE,aAAa;IACb,cAAc;IACd,SAAS;IACT,SAAS;IACT,cAAc;IACd,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,aAAa;IACb,YAAY;IACZ,uBAAuB;QACnB,oBAAoB;GACzB;;IAEC;MACE,kBAAkB;UACd,SAAS;MACb,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,uBAAuB;UACnB,oBAAoB;MACxB,mBAAmB;MACnB,YAAY;MACZ,YAAY;MACZ,mBAAmB;KACpB;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,mBAAmB;QACnB,iBAAiB;QACjB,oBAAoB;OACrB;;MAED;QACE,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;QACnB,gBAAgB;OACjB;QACC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,oBAAoB;SACrB;;IAEL;MACE,YAAY;MACZ,YAAY;MACZ,kBAAkB;UACd,SAAS;KACd;;MAEC;QACE,gBAAgB;QAChB,iBAAiB;OAClB;;EAEL;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,aAAa;GACd;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,eAAe;IACf,mBAAmB;GACpB;;IAEC;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,gBAAgB;MAChB,yBAAyB;KAC1B;;MAEC;QACE,aAAa;OACd;;QAEC;UACE,QAAQ;UACR,iBAAiB;SAClB;;IAEL;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,iBAAiB;MACjB,yBAAyB;KAC1B;;IAED;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,iBAAiB;KAClB;;MAEC;QACE,kBAAkB;QAClB,gBAAgB;QAChB,kBAAkB;QAClB,gBAAgB;OACjB;;MAED;QACE,iBAAiB;QACjB,oBAAoB;OACrB;;QAEC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,WAAW;UACX,eAAe;SAChB;;QAED;UACE,YAAY;UACZ,aAAa;SACd;;IAEL;;;;;;;;;;;;;;;;;QAiBI;;IAEJ;MACE,sBAAsB;UAClB,wBAAwB;MAC5B,cAAc;;MAEd;;UAEI;KACL;QACG;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,gBAAgB;UAChB,kBAAkB;SACnB;;EAEP;IACE,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;GACd;;EAED;;MAEI;;EAEJ;IACE,mBAAmB;IACnB,2DAA2D;GAC5D;;IAEC;MACE,gBAAgB;MAChB,oBAAoB;MACpB,yBAAyB;MACzB,iBAAiB;KAClB;;IAED;MACE,2BAA2B;MAC3B,UAAU;KACX;;MAEC;QACE,YAAY;QACZ,iBAAiB;QACjB,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;QACnB,kBAAkB;QAClB,cAAc;;QAEd;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;;QAEb;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;OACd;;EAEL;IACE,0BAA0B;GAC3B;;IAEC;MACE,2BAA2B;KAC5B;;MAEC;QACE,gBAAgB;QAChB,mBAAmB;QACnB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,oBAAoB;QACpB,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,YAAY;UACZ,qBAAqB;cACjB,4BAA4B;UAChC,mBAAmB;UACnB,iBAAiB;SAClB;;UAEC;YACE,kBAAkB;YAClB,kBAAkB;WACnB;;UAED;YACE,gBAAgB;YAChB,kBAAkB;WACnB;;QAEH;UACE,gBAAgB;SACjB;;MAEH;QACE,aAAa;QACb,cAAc;QACd,aAAa;OACd;;MAED;QACE,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,aAAa;UACb,aAAa;UACb,mBAAmB;SACpB;;QAED;UACE,aAAa;UACb,aAAa;UACb,UAAU;SACX;;EAEP;IACE,gBAAgB;IAChB,0BAA0B;GAC3B;;IAEC;MACE,gBAAgB;MAChB,oBAAoB;MACpB,yBAAyB;KAC1B;;IAED;MACE,aAAa;MACb,cAAc;MACd,2BAA2B;MAC3B,aAAa;KACd;;MAEC;QACE,6BAA6B;QAC7B,gBAAgB;QAChB,qBAAqB;QACrB,cAAc;QACd,2CAA2C;OAC5C;;QAEC;UACE,qBAAqB;UACrB,cAAc;UACd,uBAAuB;cACnB,oBAAoB;SACzB;;UAEC;YACE,gBAAgB;YAChB,kBAAkB;YAClB,eAAe;WAChB;;UAED;YACE,YAAY;YACZ,aAAa;YACb,iBAAiB;YACjB,mBAAmB;WACpB;;UAED;YACE,uBAAuB;WACxB;;UAED;YACE,0BAA0B;WAC3B;;QAEH;UACE,WAAW;UACX,qBAAqB;UACrB,cAAc;UACd,2BAA2B;cACvB,uBAAuB;SAC5B;;UAEC;YACE,gBAAgB;YAChB,kBAAkB;YAClB,eAAe;YACf,oBAAoB;WACrB;;UAED;YACE,iBAAiB;WAClB;;QAEH;UACE,UAAU;SACX;;UAEC;YACE,WAAW;YACX,YAAY;YACZ,mBAAmB;YACnB,0BAA0B;YAC1B,gBAAgB;WACjB;;UAED;YACE,iBAAiB;WAClB;;MAEL;QACE,iBAAiB;QACjB,iBAAiB;QACjB,eAAe;OAChB;;MAED;QACE,cAAc;QACd,iBAAiB;QACjB,eAAe;OAChB;;MAED;QACE,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;MAED;QACE,cAAc;QACd,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,oBAAoB;OACrB;;MAED;QACE,cAAc;QACd,oBAAoB;OACrB;CACN","file":"Teach.scss","sourcesContent":[".breadcrum {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.breadcrum span {\n  font-weight: 600;\n}\n\n.toggle_outer {\n  width: 32px;\n  height: 17.8px;\n  border-radius: 24px;\n  background-color: #f36;\n  padding: 2px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: -10px;\n}\n\n.toggle_on {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.toggle_inner {\n  width: 14px;\n  height: 14px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n/*   position: absolute;\n  width: 24px;\n  height: 36px;\n  bottom: 87px;\n  right: 560px;\n\n  img {\n    width: 100%;\n    height: 100%;\n    object-fit: contain;\n  }\n} */\n\n.headerbackgroundmobile {\n  position: absolute;\n  bottom: 0;\n  right: 64px;\n  width: 445px;\n  height: 584px;\n}\n\n.headerbackgroundmobile img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.authorimgbox {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.authorimgbox img {\n    width: 100%;\n    height: 100%;\n    max-width: 640px;\n    max-height: 560px;\n    justify-self: flex-end;\n  }\n\n/* .viewmore.mobile {\n  font-size: 14px;\n  line-height: 24px;\n  width: 100%;\n  color: #0076ff;\n  display: none;\n  justify-content: center;\n  align-items: center;\n  margin-top: 30px;\n\n  img {\n    width: 20px;\n    height: 20px;\n    margin-left: 5px;\n    margin-top: 1px;\n  }\n} */\n\n.contentPart {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 60%;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.contentPart .topicImage {\n    width: 72px;\n    height: 72px;\n    padding: 16px;\n    margin-right: 24px;\n    background: #fff;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n  }\n\n.contentPart .topicImage img {\n      width: 42px;\n      height: 42px;\n    }\n\n.contentPart .content {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n\n    /* .viewmore {\n      font-size: 20px;\n      line-height: 32px;\n      width: 100%;\n      color: #0076ff;\n      display: flex;\n      align-items: center;\n      margin-top: 24px;\n\n      img {\n        width: 24px;\n        height: 24px;\n        margin-left: 5px;\n      }\n    } */\n  }\n\n.contentPart .content .section_title {\n      display: block;\n      margin: 12px 0;\n      font-size: 40px;\n      line-height: 48px;\n      letter-spacing: -1.2px;\n      color: #25282b;\n      font-weight: 600;\n    }\n\n.contentPart .content .textcontent {\n      font-size: 20px;\n      line-height: 32px;\n      color: #25282b;\n      max-width: 370px;\n    }\n\n.customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  height: 560px;\n}\n\n.customers_container .customer_review {\n    -ms-flex-order: 0;\n        order: 0;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: start;\n        align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n  }\n\n.customers_container .customer_review .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n    }\n\n.customers_container .customer_review .customerLogo img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.customers_container .customer_review .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n.customers_container .customer_review .authorWrapper {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.customers_container .customer_review .authorWrapper .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n.customers_container .customer_review .authorWrapper .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n\n.customers_container .customer_review .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: row;\n          flex-direction: row;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.customers_container .customer_review .allcustomers img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.availableContainer {\n  background-color: #fff;\n  padding: 80px 113px 0 164px;\n  padding: 5rem 113px 0 164px;\n  // max-width: 1300px;\n}\n\n.availableContainer .availableRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.availableContainer .availableRow .desktopImage {\n      width: 648px;\n      height: 328px;\n      margin-top: 35px;\n    }\n\n.availableTitle {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.available {\n  color: #f36;\n}\n\n.availableContentSection {\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.platformContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.platform {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.platformOs {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.row {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.store {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.store .playstore {\n    width: 140px;\n    height: 42px;\n    margin-right: 16px;\n  }\n\n.store .appstore {\n    width: 140px;\n    height: 42px;\n  }\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.imagePart .emptyCard .topCard img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer {\n  height: 680px;\n  padding: 24px 64px;\n  background-color: #ffece0;\n  position: relative;\n  cursor: alias;\n}\n\n.teachContainer .heading {\n    font-size: 48px;\n    line-height: 66px;\n    color: #25282b;\n    max-width: 542px;\n    margin: 12px 0 0 0;\n  }\n\n.teachContainer .heading .learningText {\n      width: 185px;\n      display: inline;\n      position: relative;\n    }\n\n.teachContainer .heading .learningText img {\n        position: absolute;\n        bottom: -8px;\n        left: -8px;\n        width: 100%;\n        height: 8px;\n        max-width: 225px;\n      }\n\n.teachContainer .buttonwrapper {\n    margin-top: 64px;\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n.teachContainer .buttonwrapper .requestDemo {\n      border-radius: 4px;\n      background-color: #3fc;\n      font-size: 20px;\n      font-weight: 600;\n      line-height: 1.5;\n      cursor: pointer;\n      color: #000;\n      padding: 16px 24px;\n      width: -webkit-max-content;\n      width: -moz-max-content;\n      width: max-content;\n    }\n\n.teachContainer .buttonwrapper .whatsappwrapper {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n        width: -webkit-fit-content;\n        width: -moz-fit-content;\n        width: fit-content;\n        font-size: 20px;\n        cursor: pointer;\n        font-weight: 600;\n        line-height: 1.5;\n        color: #25282b !important;\n        margin: 0 8px 0 32px;\n        padding-bottom: 0;\n        opacity: 0.6;\n      }\n\n.teachContainer .buttonwrapper .whatsappwrapper img {\n        width: 32px;\n        height: 32px;\n      }\n\n/* .actionsWrapper {\n    position: absolute;\n    bottom: 26px;\n    width: 100%;\n    display: flex;\n\n    .action {\n      width: fit-content;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin-right: 24px;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        opacity: 0.7;\n        text-align: left;\n        margin-left: 8px;\n      }\n\n      .actionimgbox {\n        width: 24px;\n        height: 24px;\n        background-color: #fff;\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n\n        img {\n          width: 10px;\n          height: 9px;\n          background-color: #fff;\n        }\n      }\n    }\n  } */\n\n.teachContainer .topSection {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n\n    /* .featuresSection {\n      display: flex;\n      align-items: center;\n      margin-left: 30%;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        margin-right: 32px;\n        font-weight: 500;\n      }\n    } */\n  }\n\n.teachContainer .topSection .teachSection {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.teachContainer .topSection .teachSection .teachImgBox {\n        width: 40px;\n        height: 40px;\n      }\n\n.teachContainer .topSection .teachSection .teachImgBox img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.teachContainer .topSection .teachSection .teachSectionName {\n        margin-left: 8px;\n        font-size: 20px;\n        line-height: 30px;\n        color: #ff6400;\n        font-weight: bold;\n      }\n\n.teachContainer .contentText {\n    font-size: 20px;\n    line-height: 40px;\n    color: #25282b;\n    font-weight: normal;\n    margin: 16px 0 0 0;\n    max-width: 687px;\n  }\n\n.downloadApp {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.downloadApp .downloadText {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.downloadApp .playStoreIcon {\n  width: 132px;\n  height: 40px;\n}\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients .clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.displayClients .clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.achievedContainer .achievedRow {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.achievedContainer .achievedRow .card {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n  background-color: #fff3eb;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n  background-color: #f7effe;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n  background-color: #ffebf0;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n  background-color: #ebffef;\n}\n\n.achievedContainer .achievedRow .card .highlight {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n  color: #00ac26;\n}\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n  color: #f36;\n}\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n  color: #8c00fe;\n}\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n  color: #f60;\n}\n\n.achievedContainer .achievedRow .card .subText {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.section_container_reverse {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.section_container_reverse .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    padding: 0 64px;\n    margin: auto;\n    max-width: 1700px;\n  }\n\n.section_container_reverse .section_contents .contentPart {\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n    }\n\n.section_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.section_container .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    padding: 0 64px;\n    max-width: 1700px;\n    margin: auto;\n  }\n\n.teachContent {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  max-width: 337px;\n  vertical-align: top;\n  margin-bottom: 24px;\n}\n\n.teachContent .getranks {\n    width: 89px;\n    height: 26px;\n    margin-bottom: -5px;\n  }\n\n.teachContent .zoom {\n    width: 64px;\n    height: 18px;\n  }\n\n.imagePart {\n  width: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.imagePart .emptyCard {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n  position: relative;\n}\n\n.imagePart .emptyCard .topCard {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.imagePart .bottomCircle {\n  position: absolute;\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n}\n\n.imagePart .topCircle {\n  position: absolute;\n  width: 64px;\n  height: 64px;\n  border-radius: 50%;\n}\n\n.imagePart.livepart .emptyCard .bottomCircle {\n      background-color: #ffe0e8;\n      width: 88px;\n      height: 88px;\n      bottom: -44px;\n      left: 79px;\n    }\n\n.imagePart.livepart .emptyCard .topCircle {\n      background-color: #f2e5fe;\n      width: 88px;\n      height: 88px;\n      top: -44px;\n      right: 50px;\n    }\n\n.imagePart.assignmentpart .emptyCard .bottomCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #ffe0e8;\n      bottom: -44px;\n      left: 48px;\n    }\n\n.imagePart.assignmentpart .emptyCard .topCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #3fc;\n      top: -44px;\n      right: 67px;\n      opacity: 0.3;\n    }\n\n.imagePart.doubtpart .emptyCard .bottomCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #feb546;\n      bottom: -44px;\n      left: 68px;\n      opacity: 0.2;\n    }\n\n.imagePart.doubtpart .emptyCard .topCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #0076ff;\n      top: -44px;\n      right: 36px;\n      opacity: 0.2;\n    }\n\n.allcustomers p {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.content p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.tableContainer {\n  padding: 56px 0;\n  background-color: #f7f7f7;\n}\n\n.tableContainer h1 {\n    text-align: center;\n    margin: 0 0 24px 0;\n  }\n\n.tableContainer .table {\n    width: 956px;\n    display: grid;\n    grid-template-columns: 1fr 1fr;\n    margin: auto;\n  }\n\n.tableContainer .table .comparision {\n      padding: 24px 32px;\n      font-size: 18px;\n      display: -ms-flexbox;\n      display: flex;\n      border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    }\n\n.tableContainer .table .comparision .comparisionWrapper {\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-align: center;\n            align-items: center;\n      }\n\n.tableContainer .table .comparision .comparisionWrapper .comparisionText {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box {\n          width: 20px;\n          height: 20px;\n          margin-left: 12px;\n          border-radius: 4px;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box.positive {\n          background-color: #cfc;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box.negative {\n          background-color: #ffd6d6;\n        }\n\n.tableContainer .table .comparision .dotWrapper {\n        width: 5%;\n      }\n\n.tableContainer .table .comparision .dotWrapper .dot {\n          width: 8px;\n          height: 8px;\n          border-radius: 50%;\n          background-color: #25282b;\n          margin-top: 12px;\n        }\n\n.tableContainer .table .comparision .dotWrapper .dot.red {\n          background-color: #0c0;\n        }\n\n.tableContainer .table .comparision .tableData {\n        width: 95%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-direction: column;\n            flex-direction: column;\n      }\n\n.tableContainer .table .comparision .tableData span {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n          margin-bottom: 16px;\n        }\n\n.tableContainer .table .comparision:nth-child(odd) {\n      background-color: #ffebeb;\n    }\n\n.tableContainer .table .comparision:nth-child(even) {\n      background-color: #ebffeb;\n    }\n\n.tableContainer .table .comparision:nth-child(1),\n    .tableContainer .table .comparision:nth-child(2) {\n      background-color: #f7f7f7;\n      padding: 8px 16px;\n    }\n\n.tableContainer .table .comparision:nth-child(3),\n    .tableContainer .table .comparision:nth-child(4) {\n      background-color: rgba(37, 40, 43, 0.1);\n      font-size: 24px;\n      line-height: 32px;\n      color: #25282b;\n      text-align: left;\n      padding: 8px 16px;\n      font-weight: 600;\n    }\n\n@media only screen and (max-width: 1280px) {\n  .section_contents {\n    padding: 0 40px;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .availableContainer {\n    padding: 5rem 64px 0 64px;\n  }\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr 1fr;\n    }\n}\n\n@media only screen and (max-width: 990px) {\n  .breadcrum {\n    display: none;\n  }\n\n  /* .toggle_outer {\n    width: 32px;\n    height: 18px;\n    margin-top: -8px;\n  }\n\n  .toggle_inner {\n    width: 14px;\n    height: 14px;\n  }\n\n  .toggleAtRight {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n  }\n\n  .toggleAtLeft {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n  } */\n\n  /* .mouseicon {\n    display: none;\n  } */\n\n  .section_container {\n    padding: 32px 0;\n  }\n\n    .section_container .section_contents {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .section_container .section_contents .section_title {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .section_container_reverse {\n    padding: 32px 0;\n  }\n\n    .section_container_reverse .section_contents {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .section_container_reverse .section_contents .section_title {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .contentPart {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n    .contentPart .topicImage {\n      width: 48px;\n      height: 48px;\n      margin: auto;\n      padding: 10px;\n    }\n\n      .contentPart .topicImage img {\n        width: 28px;\n        height: 28px;\n      }\n\n    .contentPart .content {\n      max-width: none;\n\n      /* .viewmore {\n        display: none;\n        justify-content: center;\n      } */\n    }\n\n      .contentPart .content .section_title {\n        text-align: center;\n        font-size: 24px;\n      }\n\n      .contentPart .content .textcontent {\n        display: block;\n        width: 100%;\n        max-width: 600px;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n        margin: auto;\n      }\n\n  .teachContent {\n    font-size: 14px;\n    line-height: 24px;\n    vertical-align: top;\n    margin-bottom: 12px;\n    max-width: 320px;\n    text-align: center;\n  }\n\n    .teachContent .getranks {\n      width: 60px;\n      height: 18px;\n      margin-bottom: 0;\n    }\n\n    .teachContent .zoom {\n      width: 43px;\n      height: 11px;\n      margin-bottom: 2px;\n    }\n\n  .imagePart {\n    width: 100%;\n    margin-top: 40px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n\n    /* .viewmore.mobile {\n      margin-bottom: 32px;\n      display: flex;\n    } */\n  }\n\n    .imagePart .emptyCard {\n      width: 320px;\n      height: 180px;\n    }\n      .imagePart.livepart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -24px;\n        left: 254px;\n      }\n\n      .imagePart.livepart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        opacity: 0.2;\n        bottom: -16px;\n        left: 61px;\n      }\n      .imagePart.assignmentpart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        top: -16px;\n        left: 28px;\n      }\n\n      .imagePart.assignmentpart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #3fc;\n        opacity: 0.2;\n        bottom: -24px;\n        left: 236px;\n      }\n      .imagePart.doubtpart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f2e5fe;\n        left: 243px;\n        top: -16px;\n        opacity: 1;\n      }\n\n      .imagePart.doubtpart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        right: 227px;\n        bottom: -24px;\n      }\n\n  .headerbackgroundmobile {\n    width: 237px;\n    height: 340px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .customers_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n    .customers_container .customer_review {\n      -ms-flex-order: 2;\n          order: 2;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n    }\n\n      .customers_container .customer_review .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .customers_container .customer_review .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n        .customers_container .customer_review .authorWrapper .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n\n    .customers_container .authorimgbox {\n      height: 50%;\n      width: 100%;\n      -ms-flex-order: 1;\n          order: 1;\n    }\n\n      .customers_container .authorimgbox img {\n        max-width: none;\n        max-height: none;\n      }\n\n  .teachContainer .downloadApp {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .displayClients span {\n    max-width: 307px;\n  }\n\n  .teachContainer {\n    height: 1012px;\n    padding: 32px 16px;\n  }\n\n    .teachContainer .heading {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 24px auto 0 auto;\n    }\n\n      .teachContainer .heading .learningText {\n        width: 120px;\n      }\n\n        .teachContainer .heading .learningText img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .teachContainer .contentText {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .teachContainer .buttonwrapper {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .teachContainer .buttonwrapper .requestDemo {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .teachContainer .buttonwrapper .whatsappwrapper {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .teachContainer .buttonwrapper .whatsappwrapper img {\n          width: 24px;\n          height: 24px;\n        }\n\n    /* .actionsWrapper {\n      right: 0;\n      width: fit-content;\n      margin-right: 16px;\n\n      .action {\n        margin-left: 12px;\n        margin-right: 0;\n\n        span {\n          display: none;\n        }\n\n        .actionimgbox {\n          margin: 0;\n        }\n      }\n    } */\n\n    .teachContainer .topSection {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .teachContainer .topSection .teachSection .teachImgBox {\n          width: 32px;\n          height: 32px;\n        }\n\n        .teachContainer .topSection .teachSection .teachSectionName {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .achievedContainer {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .achievedContainer .achievedHeading {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .achievedContainer .achievedRow .card {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .availableContainer {\n    padding: 32px 32px 0 32px;\n  }\n\n    .availableContainer .availableRow {\n      grid-template-columns: 1fr;\n    }\n\n      .availableContainer .availableRow .availableTitle {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .availableContainer .availableRow .row {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .row .platformContainer {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .availableContainer .availableRow .row .platformContainer .platform {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .availableContainer .availableRow .row .platformContainer .platformOs {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .availableContainer .availableRow .row .platformContainer:last-child {\n          margin-right: 0;\n        }\n\n      .availableContainer .availableRow .desktopImage {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .availableContainer .availableRow .store {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .store .playstore {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .availableContainer .availableRow .store .appstore {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .tableContainer {\n    padding: 40px 0;\n    background-color: #f7f7f7;\n  }\n\n    .tableContainer h1 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 16px 32px 16px;\n    }\n\n    .tableContainer .table {\n      width: 328px;\n      display: grid;\n      grid-template-columns: 1fr;\n      margin: auto;\n    }\n\n      .tableContainer .table .comparision {\n        padding: 16px 20px 16px 16px;\n        font-size: 16px;\n        display: -ms-flexbox;\n        display: flex;\n        border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n      }\n\n        .tableContainer .table .comparision .comparisionWrapper {\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-align: center;\n              align-items: center;\n        }\n\n          .tableContainer .table .comparision .comparisionWrapper .comparisionText {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box {\n            width: 16px;\n            height: 16px;\n            margin-left: 8px;\n            border-radius: 4px;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box.positive {\n            background-color: #cfc;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box.negative {\n            background-color: #ffd6d6;\n          }\n\n        .tableContainer .table .comparision .tableData {\n          width: 92%;\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-direction: column;\n              flex-direction: column;\n        }\n\n          .tableContainer .table .comparision .tableData span {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n            margin-bottom: 12px;\n          }\n\n          .tableContainer .table .comparision .tableData span:last-child {\n            margin-bottom: 0;\n          }\n\n        .tableContainer .table .comparision .dotWrapper {\n          width: 8%;\n        }\n\n          .tableContainer .table .comparision .dotWrapper .dot {\n            width: 8px;\n            height: 8px;\n            border-radius: 50%;\n            background-color: #25282b;\n            margin-top: 8px;\n          }\n\n          .tableContainer .table .comparision .dotWrapper span:last-child {\n            margin-bottom: 0;\n          }\n\n      .tableContainer .table .comparision:nth-child(1) {\n        background: none;\n        margin-top: 16px;\n        padding: 8px 0;\n      }\n\n      .tableContainer .table .comparision:nth-child(2) {\n        grid-row: 1/2;\n        background: none;\n        padding: 8px 0;\n      }\n\n      .tableContainer .table .comparision:nth-child(3) {\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .tableContainer .table .comparision:nth-child(4) {\n        grid-row: 2/3;\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .tableContainer .table .comparision:nth-child(6) {\n        grid-row: 3/4;\n      }\n\n      .tableContainer .table .comparision:nth-child(8) {\n        grid-row: 4/5;\n      }\n\n      .tableContainer .table .comparision:nth-child(10) {\n        grid-row: 5/6;\n      }\n\n      .tableContainer .table .comparision:nth-child(12) {\n        grid-row: 6/7;\n      }\n\n      .tableContainer .table .comparision:nth-child(13) {\n        border-bottom: none;\n      }\n\n      .tableContainer .table .comparision:nth-child(14) {\n        grid-row: 7/8;\n        border-bottom: none;\n      }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"breadcrum": "Teach-breadcrum-11xX5",
	"toggle_outer": "Teach-toggle_outer-VIsVE",
	"toggle_on": "Teach-toggle_on-2JbJd",
	"toggle_inner": "Teach-toggle_inner-3gUru",
	"headerbackgroundmobile": "Teach-headerbackgroundmobile-2Z0QY",
	"authorimgbox": "Teach-authorimgbox-28-2r",
	"contentPart": "Teach-contentPart-1lSgK",
	"topicImage": "Teach-topicImage-1CM7j",
	"content": "Teach-content-s_zCd",
	"section_title": "Teach-section_title-2nyBd",
	"textcontent": "Teach-textcontent-3s5Dz",
	"customers_container": "Teach-customers_container-3dTy8",
	"customer_review": "Teach-customer_review-3Q1HR",
	"customerLogo": "Teach-customerLogo-xFAq9",
	"sriChaitanyaText": "Teach-sriChaitanyaText-1HRt2",
	"authorWrapper": "Teach-authorWrapper-2FTu7",
	"author_title": "Teach-author_title-3HNCy",
	"about_author": "Teach-about_author-3sn5h",
	"allcustomers": "Teach-allcustomers-2b8NO",
	"availableContainer": "Teach-availableContainer-2e4mP",
	"availableRow": "Teach-availableRow-3SeEQ",
	"desktopImage": "Teach-desktopImage-n9mcs",
	"availableTitle": "Teach-availableTitle-28MiM",
	"available": "Teach-available-k7Ani",
	"availableContentSection": "Teach-availableContentSection-JZlaF",
	"platformContainer": "Teach-platformContainer-1IANc",
	"platform": "Teach-platform-1GcJZ",
	"platformOs": "Teach-platformOs-2DF6E",
	"row": "Teach-row-1VSyr",
	"store": "Teach-store-IZcDQ",
	"playstore": "Teach-playstore-PXfhG",
	"appstore": "Teach-appstore-2HMPf",
	"displayClients": "Teach-displayClients-1bg_Q",
	"imagePart": "Teach-imagePart-3NAi0",
	"emptyCard": "Teach-emptyCard-v-GO_",
	"topCard": "Teach-topCard-2ZuD4",
	"teachContainer": "Teach-teachContainer-2Q-YB",
	"heading": "Teach-heading-1K5as",
	"learningText": "Teach-learningText-3aV7N",
	"buttonwrapper": "Teach-buttonwrapper-i_jdi",
	"requestDemo": "Teach-requestDemo-AboHC",
	"whatsappwrapper": "Teach-whatsappwrapper-3jpz4",
	"whatsapp": "Teach-whatsapp-34spS",
	"topSection": "Teach-topSection-1ZZr-",
	"teachSection": "Teach-teachSection-33snH",
	"teachImgBox": "Teach-teachImgBox-mkz67",
	"teachSectionName": "Teach-teachSectionName-1AdS4",
	"contentText": "Teach-contentText-3eX56",
	"downloadApp": "Teach-downloadApp-3tWrv",
	"downloadText": "Teach-downloadText-2YOcN",
	"playStoreIcon": "Teach-playStoreIcon-3YdD8",
	"clientsWrapper": "Teach-clientsWrapper-3fJYq",
	"client": "Teach-client-Ii9JF",
	"achievedContainer": "Teach-achievedContainer-Aa5ks",
	"achievedHeading": "Teach-achievedHeading-2yxR4",
	"achievedRow": "Teach-achievedRow-AXwBV",
	"card": "Teach-card-6zL17",
	"achievedProfile": "Teach-achievedProfile-3w5Nj",
	"clients": "Teach-clients-1hUFi",
	"students": "Teach-students-PAFZG",
	"tests": "Teach-tests-27DEs",
	"questions": "Teach-questions-WBnlD",
	"highlight": "Teach-highlight-3llRS",
	"questionsHighlight": "Teach-questionsHighlight-37La9",
	"testsHighlight": "Teach-testsHighlight-2Hb9O",
	"studentsHighlight": "Teach-studentsHighlight-2H5F_",
	"clientsHighlight": "Teach-clientsHighlight-1ankD",
	"subText": "Teach-subText-3IRHK",
	"section_container_reverse": "Teach-section_container_reverse-1yEDY",
	"section_contents": "Teach-section_contents-1NRDr",
	"section_container": "Teach-section_container-cyyTN",
	"teachContent": "Teach-teachContent-19QTk",
	"getranks": "Teach-getranks-3JcXa",
	"zoom": "Teach-zoom-3RXu-",
	"bottomCircle": "Teach-bottomCircle-1QU8r",
	"topCircle": "Teach-topCircle-2waHP",
	"livepart": "Teach-livepart-3TKC8",
	"assignmentpart": "Teach-assignmentpart-20DkI",
	"doubtpart": "Teach-doubtpart-2NkyQ",
	"tableContainer": "Teach-tableContainer-32ppr",
	"table": "Teach-table-y9qtU",
	"comparision": "Teach-comparision-WqBQk",
	"comparisionWrapper": "Teach-comparisionWrapper-MJ1Qz",
	"comparisionText": "Teach-comparisionText-3FmhP",
	"box": "Teach-box-287sD",
	"positive": "Teach-positive-2nQ7l",
	"negative": "Teach-negative-3D6w2",
	"dotWrapper": "Teach-dotWrapper-ANQj6",
	"dot": "Teach-dot-fu9Ct",
	"red": "Teach-red-zeFql",
	"tableData": "Teach-tableData-3nntZ"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/Teach.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Teach_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/Teach/Teach.scss");
/* harmony import */ var _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Teach_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/Teach.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class Teach extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "displayToggle", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      role: "presentation",
      className: this.state.liveToggle ? `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_outer} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_on}` : _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_outer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_inner,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    })));

    _defineProperty(this, "displayTeachSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.breadcrum,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, "Home / ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Live Classes")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachImgBox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/live_colored.svg",
      alt: "live-classes",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSectionName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Live Classes"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, "Maximize the\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.learningText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, "learning\xA0"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/live classes/underscore_for_title.png",
      alt: "underscore",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    })), "of your students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, "A powerful teaching module that facilitates distance learning, promote positive student behaviour, engage parents in the learning process and reduce teacher workload all from one easy-to-use platform."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.buttonwrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.requestDemo,
      onClick: this.handleShowTrial,
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, "GET SUBSCRIPTION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsappwrapper,
      href: "https://api.whatsapp.com/send?phone=918800764909&text=Hi GetRanks, I would like to know more about your Online Platform.",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsapp,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/whatsapp_logo.svg",
      alt: "whatsapp",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadApp,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }, "Download the app"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playStoreIcon,
      src: "/images/home/platforms/playStore.png",
      alt: "play_store",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerbackgroundmobile,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/teach_header_human.webp",
      alt: "mobile_background",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }))));

    _defineProperty(this, "displayClients", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.displayClients,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 112
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOME_CLIENTS_UPDATED"].map(client => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.client,
      key: client.id,
      src: client.icon,
      alt: "clinet",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/customers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, "click here for more..."))));

    _defineProperty(this, "displayAchievedSoFar", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130
      },
      __self: this
    }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedRow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clients}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 139
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/clients.svg",
      alt: "profile",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 142
      },
      __self: this
    }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143
      },
      __self: this
    }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.students}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/students.svg",
      alt: "students",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.studentsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/tests.svg",
      alt: "tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.testsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 167
      },
      __self: this
    }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questions}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 173
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/questions.svg",
      alt: "questions",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 174
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questionsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179
      },
      __self: this
    }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 182
      },
      __self: this
    }, "Questions Solved")))));

    _defineProperty(this, "liveToggleHandler", () => {
      this.setState({
        liveToggle: !this.state.liveToggle
      });
    });

    _defineProperty(this, "lectureToggleHandler", () => {
      this.setState({
        lectureToggle: !this.state.lectureToggle
      });
    });

    _defineProperty(this, "assignmentToggleHandler", () => {
      this.setState({
        assignmentToggle: !this.state.assignmentToggle
      });
    });

    _defineProperty(this, "doubtToggleHandler", () => {
      this.setState({
        doubtToggle: !this.state.doubtToggle
      });
    });

    _defineProperty(this, "displayLiveSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 206
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topicImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 207
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Teach/old_Live.svg",
      alt: "live",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 208
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214
      },
      __self: this
    }, "Live"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.getranks,
      src: "/images/icons/getranks copy.svg",
      alt: "getranks",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 216
      },
      __self: this
    }), "\xA0+\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.zoom,
      src: "/images/icons/Zoom-Logo.png",
      alt: "zoom-logo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 222
      },
      __self: this
    }), "\xA0Integration Zoom is integrated with GetRanks. All Benefits-In, All Limitations-out"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 230
      },
      __self: this
    }, "No matter where education takes place, \"Egnify Live with Zoom \" can help engage students,faculty and staff for learning, collaboration and administration."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.livepart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 237
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 238
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 239
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/live.webp",
      alt: "live",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 242
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 243
      },
      __self: this
    }))))));

    _defineProperty(this, "displayIndependence", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 252
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 253
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 254
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 255
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 256
      },
      __self: this
    }, "Independence:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 257
      },
      __self: this
    }, "Your Zoom account will be used. All the features and provisions of your Zoom account shall be available with an added advantage of login restrictions only for selected students."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 264
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 265
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 266
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/independence.svg",
      alt: "independence",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 267
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 269
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 270
      },
      __self: this
    }))))));

    _defineProperty(this, "displayFlexibility", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 278
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 279
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 280
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 281
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 282
      },
      __self: this
    }, "Flexibility:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 283
      },
      __self: this
    }, "Any number of Zoom accounts can be used. Different Zoom accounts can be mapped with different Class Sessions thus allowing multiple teachers to use their personal accounts."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 290
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 291
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 292
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/flexibility.webp",
      alt: "flexibility",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 293
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 295
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 296
      },
      __self: this
    }))))));

    _defineProperty(this, "displaySecureAccess", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 304
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 305
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 306
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 307
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 308
      },
      __self: this
    }, "Secure Access:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 309
      },
      __self: this
    }, "Only selected students who have access to the App can now access Zoom class, no need to share Meeting ID everytime, No random access."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 316
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 317
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 318
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/live.webp",
      alt: "secureAccess",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 319
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 321
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 322
      },
      __self: this
    }))))));

    _defineProperty(this, "displayMultipleUses", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 330
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 331
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 332
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 333
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 334
      },
      __self: this
    }, "Multiple uses of Zoom:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 335
      },
      __self: this
    }, "Zoom facility available for live Class and Doubts modules. Soon Zoom is being integrated to the online Exam module for online Exam proctoring."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 342
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 343
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 344
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/multiple_uses.webp",
      alt: "multi-uses",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 345
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 347
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 348
      },
      __self: this
    }))))));

    _defineProperty(this, "displayOrganisedContent", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 356
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 357
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 358
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 359
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 360
      },
      __self: this
    }, "Organised Content:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 361
      },
      __self: this
    }, "Now you dont have to invite students by sending separate meeting notifications and reminders to parents. This will allow all the Zoom classes to be listed and seen on the students dashboard scheduled as a part of curriculum."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 369
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 370
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 371
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/organised_content.webp",
      alt: "secureAccess",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 372
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 377
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 378
      },
      __self: this
    }))))));

    _defineProperty(this, "displayAllowReplay", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 386
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 387
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 388
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 389
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 390
      },
      __self: this
    }, "Allow Replay:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 391
      },
      __self: this
    }, "Zoom meeting recordings can be uploaded for later replay by students."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 397
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 398
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 399
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/allow_replay.webp",
      alt: "multi-uses",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 400
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 402
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 403
      },
      __self: this
    }))))));

    _defineProperty(this, "displayAttendanceRecord", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 411
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 412
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 413
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 414
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 415
      },
      __self: this
    }, "Attendance Record:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 416
      },
      __self: this
    }, "Now its possible to maintain organized record of attendance in Zoom Class along with other attendance and digital learning activity record of the student."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 423
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 424
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 425
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Teach/flexibility.webp",
      alt: "secureAccess",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 426
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 428
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 429
      },
      __self: this
    }))))));

    _defineProperty(this, "displayDifference", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 437
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 438
      },
      __self: this
    }, "GetRanks + Zoom is", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 440
      },
      __self: this
    }), " more powerful than Zoom alone"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.table,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 442
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 443
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 444
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 445
      },
      __self: this
    }, "Negative Points"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.box} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.negative}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 446
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 449
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 450
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 451
      },
      __self: this
    }, "Positive Points"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.box} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.positive}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 452
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 455
      },
      __self: this
    }, "Zoom only"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 456
      },
      __self: this
    }, "GeRanks+Zoom"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 457
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 458
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 459
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 461
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 462
      },
      __self: this
    }, "Nothing Personalized. You become a part of a portal and popularize it. Your Brand identity is lost."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 466
      },
      __self: this
    }, "Student data & Content is shared on the portal"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 469
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 470
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 471
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 473
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 474
      },
      __self: this
    }, "Personalized APP with Institute Name and Logo strengthens your Brand"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 478
      },
      __self: this
    }, "Personalized System that you can also buy and install on your server"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 482
      },
      __self: this
    }, "Secure Content can be viewed by Users only inside the APP"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 487
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 488
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 489
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 491
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 492
      },
      __self: this
    }, "Does not allow replay. This saves bandwidth and reduce cost by compromising the essential need of Self Learning"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 496
      },
      __self: this
    }, "If internet is lost during session, that much session is missed. Student is never able to view it back. No provision to go back during live session"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 501
      },
      __self: this
    }, "Nothing is stored online. Live session video is Downloadable and freely distributable as link or file. Manage your files locally after downloading"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 508
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 509
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 510
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 512
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 513
      },
      __self: this
    }, "Lectures are auto recorded during live session and available for replay multiple times"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 517
      },
      __self: this
    }, "Student will not miss anything if internet is lost, he can start again and watch where he left. Possible to go back in live lecture to review a topic and then return to live moment"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 522
      },
      __self: this
    }, "Videos are stored in Safe Google Servers in your full access, no hassle of maintaining locally"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 528
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 529
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 530
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 532
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 533
      },
      __self: this
    }, "No attendance and participation Record"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 536
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 537
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 538
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 540
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 541
      },
      __self: this
    }, "Attendance record of every activity is maintained."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 544
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 545
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 546
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 548
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 549
      },
      __self: this
    }, "There is cost per broadcaster"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 552
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 553
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 554
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 556
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 557
      },
      __self: this
    }, "Multiple teachers can cast using single license"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 560
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 561
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 562
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 564
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 565
      },
      __self: this
    }, "No control over user identity. Users can enter with random names like \u2018rocky123\u2019 and create nuisance"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 571
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 572
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot} ${_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 573
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 575
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 576
      },
      __self: this
    }, "Restricted Login and Access Control: User cannot send any indecent message because registered name and number of sender is visible"))))));

    _defineProperty(this, "displayCustomers", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customers_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 587
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customer_review,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 588
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customerLogo,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 589
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "images/customers/Frame 1135.jpg",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 590
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sriChaitanyaText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 592
      },
      __self: this
    }, "\u201CLorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis\u201D"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.authorWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 598
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.author_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 599
      },
      __self: this
    }, "Srujan Sagar"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.about_author,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 600
      },
      __self: this
    }, "Acadamic head,", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 601
      },
      __self: this
    }, "Sri Chaitanya"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.allcustomers,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 604
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 605
      },
      __self: this
    }, "see all customers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "images/icons/arrowRight.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 606
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.authorimgbox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 609
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "images/icons/srujanSagar.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 610
      },
      __self: this
    }))));

    _defineProperty(this, "displayAvailableOn", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 615
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableRow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 616
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContentSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 617
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 618
      },
      __self: this
    }, "We\u2019re ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 619
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.available,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 620
      },
      __self: this
    }, "available"), " on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.row,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 622
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["PLATFORMS"].map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 624
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.icon,
      alt: "",
      height: "40px",
      width: "40px",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 625
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platform,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 626
      },
      __self: this
    }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformOs,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 627
      },
      __self: this
    }, item.available)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.store,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 631
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/platforms/playStore.webp",
      alt: "Play Store",
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playstore,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 632
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/platforms/appStore.webp",
      alt: "App Store",
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.appstore,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 637
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.desktopImage,
      src: "/images/home/platforms/platforms.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 645
      },
      __self: this
    }))));

    this.state = {
      liveToggle: false,
      lectureToggle: false,
      assignmentToggle: false,
      doubtToggle: false
    };
  } // Toggle Button Code


  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 655
      },
      __self: this
    }, this.displayTeachSection(), this.displayClients(), this.displayAchievedSoFar(), this.displayLiveSection(), this.displayIndependence(), this.displayFlexibility(), this.displaySecureAccess(), this.displayMultipleUses(), this.displayOrganisedContent(), this.displayAllowReplay(), this.displayAttendanceRecord(), this.displayDifference(), this.displayAvailableOn());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Teach));

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/Teach.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Teach/Teach.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Teach__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/Teach/Teach.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/index.js";




async function action() {
  return {
    title: 'GetRanks by Egnify: Assessment & Analytics Platform',
    chunk: ['Live Classes'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Teach__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL0xpdmUgQ2xhc3Nlcy5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1RlYWNoL1RlYWNoLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVhY2gvVGVhY2guanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVhY2gvVGVhY2guc2Nzcz82NTA5IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1RlYWNoL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuVGVhY2gtYnJlYWRjcnVtLTExeFg1IHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4uVGVhY2gtYnJlYWRjcnVtLTExeFg1IHNwYW4ge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlRlYWNoLXRvZ2dsZV9vdXRlci1WSXNWRSB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMTcuOHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBwYWRkaW5nOiAycHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLXRvcDogLTEwcHg7XFxufVxcblxcbi5UZWFjaC10b2dnbGVfb24tMkpiSmQge1xcbiAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi5UZWFjaC10b2dnbGVfaW5uZXItM2dVcnUge1xcbiAgd2lkdGg6IDE0cHg7XFxuICBoZWlnaHQ6IDE0cHg7XFxuICBib3JkZXItcmFkaXVzOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLyogICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMzZweDtcXG4gIGJvdHRvbTogODdweDtcXG4gIHJpZ2h0OiA1NjBweDtcXG5cXG4gIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICB9XFxufSAqL1xcblxcbi5UZWFjaC1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTJaMFFZIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgd2lkdGg6IDQ0NXB4O1xcbiAgaGVpZ2h0OiA1ODRweDtcXG59XFxuXFxuLlRlYWNoLWhlYWRlcmJhY2tncm91bmRtb2JpbGUtMlowUVkgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIH1cXG5cXG4uVGVhY2gtYXV0aG9yaW1nYm94LTI4LTJyIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLlRlYWNoLWF1dGhvcmltZ2JveC0yOC0yciBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDY0MHB4O1xcbiAgICBtYXgtaGVpZ2h0OiA1NjBweDtcXG4gICAganVzdGlmeS1zZWxmOiBmbGV4LWVuZDtcXG4gIH1cXG5cXG4vKiAudmlld21vcmUubW9iaWxlIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiAzMHB4O1xcblxcbiAgaW1nIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgbWFyZ2luLXRvcDogMXB4O1xcbiAgfVxcbn0gKi9cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHdpZHRoOiA2MCU7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLlRlYWNoLWNvbnRlbnRQYXJ0LTFsU2dLIC5UZWFjaC10b3BpY0ltYWdlLTFDTTdqIHtcXG4gICAgd2lkdGg6IDcycHg7XFxuICAgIGhlaWdodDogNzJweDtcXG4gICAgcGFkZGluZzogMTZweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIH1cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLXRvcGljSW1hZ2UtMUNNN2ogaW1nIHtcXG4gICAgICB3aWR0aDogNDJweDtcXG4gICAgICBoZWlnaHQ6IDQycHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2Qge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWluLXdpZHRoOiAyOTBweDtcXG4gICAgbWF4LXdpZHRoOiA1NDBweDtcXG5cXG4gICAgLyogLnZpZXdtb3JlIHtcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgY29sb3I6ICMwMDc2ZmY7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuXFxuICAgICAgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgICB9XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2QgLlRlYWNoLXNlY3Rpb25fdGl0bGUtMm55QmQge1xcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgIG1hcmdpbjogMTJweCAwO1xcbiAgICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgICBsaW5lLWhlaWdodDogNDhweDtcXG4gICAgICBsZXR0ZXItc3BhY2luZzogLTEuMnB4O1xcbiAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2QgLlRlYWNoLXRleHRjb250ZW50LTNzNUR6IHtcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgbWF4LXdpZHRoOiAzNzBweDtcXG4gICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBoZWlnaHQ6IDU2MHB4O1xcbn1cXG5cXG4uVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIHtcXG4gICAgLW1zLWZsZXgtb3JkZXI6IDA7XFxuICAgICAgICBvcmRlcjogMDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDQ4cHggNjRweDtcXG4gICAgd2lkdGg6IDUwJTtcXG4gIH1cXG5cXG4uVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1jdXN0b21lckxvZ28teEZBcTkge1xcbiAgICAgIHdpZHRoOiAxMjBweDtcXG4gICAgICBoZWlnaHQ6IDgwcHg7XFxuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1jdXN0b21lckxvZ28teEZBcTkgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLXNyaUNoYWl0YW55YVRleHQtMUhSdDIge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWF1dGhvcldyYXBwZXItMkZUdTcge1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIH1cXG5cXG4uVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1hdXRob3JXcmFwcGVyLTJGVHU3IC5UZWFjaC1hdXRob3JfdGl0bGUtM0hOQ3kge1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICAgICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWF1dGhvcldyYXBwZXItMkZUdTcgLlRlYWNoLWFib3V0X2F1dGhvci0zc241aCB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICAgICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWFsbGN1c3RvbWVycy0yYjhOTyB7XFxuICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtYWxsY3VzdG9tZXJzLTJiOE5PIGltZyB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgICAgd2lkdGg6IDIwcHg7XFxuICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogODBweCAxMTNweCAwIDE2NHB4O1xcbiAgcGFkZGluZzogNXJlbSAxMTNweCAwIDE2NHB4O1xcbiAgLy8gbWF4LXdpZHRoOiAxMzAwcHg7XFxufVxcblxcbi5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1kZXNrdG9wSW1hZ2UtbjltY3Mge1xcbiAgICAgIHdpZHRoOiA2NDhweDtcXG4gICAgICBoZWlnaHQ6IDMyOHB4O1xcbiAgICAgIG1hcmdpbi10b3A6IDM1cHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtYXZhaWxhYmxlVGl0bGUtMjhNaU0ge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlRlYWNoLWF2YWlsYWJsZS1rN0FuaSB7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLlRlYWNoLWF2YWlsYWJsZUNvbnRlbnRTZWN0aW9uLUpabGFGIHtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59XFxuXFxuLlRlYWNoLXBsYXRmb3JtQ29udGFpbmVyLTFJQU5jIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBtYXJnaW4tcmlnaHQ6IDJyZW07XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XFxufVxcblxcbi5UZWFjaC1wbGF0Zm9ybS0xR2NKWiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luOiA4cHggMCA0cHggMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uVGVhY2gtcGxhdGZvcm1Pcy0yREY2RSB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5UZWFjaC1yb3ctMVZTeXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICBtYXJnaW46IDQ4cHggMCAzMnB4IDA7XFxuICBtYXJnaW46IDNyZW0gMCAycmVtIDA7XFxufVxcblxcbi5UZWFjaC1zdG9yZS1JWmNEUSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0cmVtO1xcbn1cXG5cXG4uVGVhY2gtc3RvcmUtSVpjRFEgLlRlYWNoLXBsYXlzdG9yZS1QWGZoRyB7XFxuICAgIHdpZHRoOiAxNDBweDtcXG4gICAgaGVpZ2h0OiA0MnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICB9XFxuXFxuLlRlYWNoLXN0b3JlLUlaY0RRIC5UZWFjaC1hcHBzdG9yZS0ySE1QZiB7XFxuICAgIHdpZHRoOiAxNDBweDtcXG4gICAgaGVpZ2h0OiA0MnB4O1xcbiAgfVxcblxcbi5UZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUSBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENhcmQtMlp1RDQgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiB7XFxuICBoZWlnaHQ6IDY4MHB4O1xcbiAgcGFkZGluZzogMjRweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWNlMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGN1cnNvcjogYWxpYXM7XFxufVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtaGVhZGluZy0xSzVhcyB7XFxuICAgIGZvbnQtc2l6ZTogNDhweDtcXG4gICAgbGluZS1oZWlnaHQ6IDY2cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBtYXgtd2lkdGg6IDU0MnB4O1xcbiAgICBtYXJnaW46IDEycHggMCAwIDA7XFxuICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIC5UZWFjaC1sZWFybmluZ1RleHQtM2FWN04ge1xcbiAgICAgIHdpZHRoOiAxODVweDtcXG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XFxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIC5UZWFjaC1sZWFybmluZ1RleHQtM2FWN04gaW1nIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIGJvdHRvbTogLThweDtcXG4gICAgICAgIGxlZnQ6IC04cHg7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgbWF4LXdpZHRoOiAyMjVweDtcXG4gICAgICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIHtcXG4gICAgbWFyZ2luLXRvcDogNjRweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIC5UZWFjaC1yZXF1ZXN0RGVtby1BYm9IQyB7XFxuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgICAgY29sb3I6ICMwMDA7XFxuICAgICAgcGFkZGluZzogMTZweCAyNHB4O1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBtYXgtY29udGVudDtcXG4gICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkgLlRlYWNoLXdoYXRzYXBwd3JhcHBlci0zanB6NCAuVGVhY2gtd2hhdHNhcHAtMzRzcFMge1xcbiAgICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gICAgICAgIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XFxuICAgICAgICBvcGFjaXR5OiAwLjY7XFxuICAgICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IGltZyB7XFxuICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICB9XFxuXFxuLyogLmFjdGlvbnNXcmFwcGVyIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBib3R0b206IDI2cHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcblxcbiAgICAuYWN0aW9uIHtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG5cXG4gICAgICBzcGFuIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICBvcGFjaXR5OiAwLjc7XFxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmFjdGlvbmltZ2JveCB7XFxuICAgICAgICB3aWR0aDogMjRweDtcXG4gICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcblxcbiAgICAgICAgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDEwcHg7XFxuICAgICAgICAgIGhlaWdodDogOXB4O1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfVxcbiAgfSAqL1xcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNTZweDtcXG5cXG4gICAgLyogLmZlYXR1cmVzU2VjdGlvbiB7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi1sZWZ0OiAzMCU7XFxuXFxuICAgICAgc3BhbiB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAzMnB4O1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gICAgICB9XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCB7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC10b3BTZWN0aW9uLTFaWnItIC5UZWFjaC10ZWFjaFNlY3Rpb24tMzNzbkggLlRlYWNoLXRlYWNoSW1nQm94LW1rejY3IHtcXG4gICAgICAgIHdpZHRoOiA0MHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCAuVGVhY2gtdGVhY2hJbWdCb3gtbWt6NjcgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCAuVGVhY2gtdGVhY2hTZWN0aW9uTmFtZS0xQWRTNCB7XFxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICAgICAgICBjb2xvcjogI2ZmNjQwMDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWNvbnRlbnRUZXh0LTNlWDU2IHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgIG1hcmdpbjogMTZweCAwIDAgMDtcXG4gICAgbWF4LXdpZHRoOiA2ODdweDtcXG4gIH1cXG5cXG4uVGVhY2gtZG93bmxvYWRBcHAtM3RXcnYge1xcbiAgbWFyZ2luLXRvcDogNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4uVGVhY2gtZG93bmxvYWRBcHAtM3RXcnYgLlRlYWNoLWRvd25sb2FkVGV4dC0yWU9jTiB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLlRlYWNoLWRvd25sb2FkQXBwLTN0V3J2IC5UZWFjaC1wbGF5U3RvcmVJY29uLTNZZEQ4IHtcXG4gIHdpZHRoOiAxMzJweDtcXG4gIGhlaWdodDogNDBweDtcXG59XFxuXFxuLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWluLWhlaWdodDogNDY0cHg7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1EgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIC5UZWFjaC1jbGllbnRzV3JhcHBlci0zZkpZcSB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1EgLlRlYWNoLWNsaWVudHNXcmFwcGVyLTNmSllxIC5UZWFjaC1jbGllbnQtSWk5SkYge1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMTJweDtcXG59XFxuXFxuLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHNwYW4gYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG59XFxuXFxuLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHNwYW4gYTpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkSGVhZGluZy0yeXhSNCB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAvLyBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgZ2FwOiAxNnB4O1xcbiAgZ2FwOiAxcmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogMjAycHg7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXNpemU6IDEuNXJlbTtcXG4gIGNvbG9yOiByZ2JhKDM3LCA0MCwgNDMsIDAuNik7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICBwYWRkaW5nOiAxLjVyZW07XFxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWFjaGlldmVkUHJvZmlsZS0zdzVOaiB7XFxuICB3aWR0aDogNTJweDtcXG4gIGhlaWdodDogNTJweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWFjaGlldmVkUHJvZmlsZS0zdzVOai5UZWFjaC1jbGllbnRzLTFoVUZpIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYzZWI7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWFjaGlldmVkUHJvZmlsZS0zdzVOai5UZWFjaC1zdHVkZW50cy1QQUZaRyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1hY2hpZXZlZFByb2ZpbGUtM3c1TmouVGVhY2gtdGVzdHMtMjdERXMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtYWNoaWV2ZWRQcm9maWxlLTN3NU5qLlRlYWNoLXF1ZXN0aW9ucy1XQm5sRCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmVmO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1oaWdobGlnaHQtM2xsUlMge1xcbiAgZm9udC1zaXplOiAzNnB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtaGlnaGxpZ2h0LTNsbFJTLlRlYWNoLXF1ZXN0aW9uc0hpZ2hsaWdodC0zN0xhOSB7XFxuICBjb2xvcjogIzAwYWMyNjtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtaGlnaGxpZ2h0LTNsbFJTLlRlYWNoLXRlc3RzSGlnaGxpZ2h0LTJIYjlPIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1oaWdobGlnaHQtM2xsUlMuVGVhY2gtc3R1ZGVudHNIaWdobGlnaHQtMkg1Rl8ge1xcbiAgY29sb3I6ICM4YzAwZmU7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWhpZ2hsaWdodC0zbGxSUy5UZWFjaC1jbGllbnRzSGlnaGxpZ2h0LTFhbmtEIHtcXG4gIGNvbG9yOiAjZjYwO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1zdWJUZXh0LTNJUkhLIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4vKiAudG9nZ2xlQXRSaWdodCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi50b2dnbGVBdExlZnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggMCA2NHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxufSAqL1xcblxcbi5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTF5RURZIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgcGFkZGluZzogMTg0cHggMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uVGVhY2gtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xeUVEWSAuVGVhY2gtc2VjdGlvbl9jb250ZW50cy0xTlJEciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwIDY0cHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICB9XFxuXFxuLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMXlFRFkgLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIgLlRlYWNoLWNvbnRlbnRQYXJ0LTFsU2dLIHtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICB9XFxuXFxuLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyLWN5eVROIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMTg0cHggMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uVGVhY2gtc2VjdGlvbl9jb250YWluZXItY3l5VE4gLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDAgNjRweDtcXG4gICAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250ZW50LTE5UVRrIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXgtd2lkdGg6IDMzN3B4O1xcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxufVxcblxcbi5UZWFjaC10ZWFjaENvbnRlbnQtMTlRVGsgLlRlYWNoLWdldHJhbmtzLTNKY1hhIHtcXG4gICAgd2lkdGg6IDg5cHg7XFxuICAgIGhlaWdodDogMjZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogLTVweDtcXG4gIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250ZW50LTE5UVRrIC5UZWFjaC16b29tLTNSWHUtIHtcXG4gICAgd2lkdGg6IDY0cHg7XFxuICAgIGhlaWdodDogMThweDtcXG4gIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwIHtcXG4gIHdpZHRoOiA0MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyB7XFxuICB3aWR0aDogNjA2cHg7XFxuICBoZWlnaHQ6IDM0MXB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMzJweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMCAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC10b3BDYXJkLTJadUQ0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHotaW5kZXg6IDE7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxufVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAgLlRlYWNoLWJvdHRvbUNpcmNsZS0xUVU4ciB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogNDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG59XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMCAuVGVhY2gtdG9wQ2lyY2xlLTJ3YUhQIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA2NHB4O1xcbiAgaGVpZ2h0OiA2NHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWxpdmVwYXJ0LTNUS0M4IC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLWJvdHRvbUNpcmNsZS0xUVU4ciB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYm90dG9tOiAtNDRweDtcXG4gICAgICBsZWZ0OiA3OXB4O1xcbiAgICB9XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1saXZlcGFydC0zVEtDOCAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC10b3BDaXJjbGUtMndhSFAge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDUwcHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWFzc2lnbm1lbnRwYXJ0LTIwRGtJIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLWJvdHRvbUNpcmNsZS0xUVU4ciB7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxuICAgICAgYm90dG9tOiAtNDRweDtcXG4gICAgICBsZWZ0OiA0OHB4O1xcbiAgICB9XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1hc3NpZ25tZW50cGFydC0yMERrSSAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC10b3BDaXJjbGUtMndhSFAge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDY3cHg7XFxuICAgICAgb3BhY2l0eTogMC4zO1xcbiAgICB9XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1kb3VidHBhcnQtMk5reVEgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDY4cHg7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICB9XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1kb3VidHBhcnQtMk5reVEgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtdG9wQ2lyY2xlLTJ3YUhQIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICB0b3A6IC00NHB4O1xcbiAgICAgIHJpZ2h0OiAzNnB4O1xcbiAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgfVxcblxcbi5UZWFjaC1hbGxjdXN0b21lcnMtMmI4Tk8gcCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uVGVhY2gtY29udGVudC1zX3pDZCBwIHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBsZXR0ZXItc3BhY2luZzogMC40OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciB7XFxuICBwYWRkaW5nOiA1NnB4IDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgaDEge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIHtcXG4gICAgd2lkdGg6IDk1NnB4O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAxZnI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayB7XFxuICAgICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMCwgMCwgMCwgMC4xKTtcXG4gICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXoge1xcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtY29tcGFyaXNpb25XcmFwcGVyLU1KMVF6IC5UZWFjaC1jb21wYXJpc2lvblRleHQtM0ZtaFAge1xcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtY29tcGFyaXNpb25XcmFwcGVyLU1KMVF6IC5UZWFjaC1ib3gtMjg3c0Qge1xcbiAgICAgICAgICB3aWR0aDogMjBweDtcXG4gICAgICAgICAgaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMTJweDtcXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWJveC0yODdzRC5UZWFjaC1wb3NpdGl2ZS0yblE3bCB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNjZmM7XFxuICAgICAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiAuVGVhY2gtYm94LTI4N3NELlRlYWNoLW5lZ2F0aXZlLTNENncyIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDZkNjtcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtZG90V3JhcHBlci1BTlFqNiB7XFxuICAgICAgICB3aWR0aDogNSU7XFxuICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1kb3RXcmFwcGVyLUFOUWo2IC5UZWFjaC1kb3QtZnU5Q3Qge1xcbiAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDhweDtcXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1kb3RXcmFwcGVyLUFOUWo2IC5UZWFjaC1kb3QtZnU5Q3QuVGVhY2gtcmVkLXplRnFsIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzBjMDtcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtdGFibGVEYXRhLTNubnRaIHtcXG4gICAgICAgIHdpZHRoOiA5NSU7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtdGFibGVEYXRhLTNubnRaIHNwYW4ge1xcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQob2RkKSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJlYjtcXG4gICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZChldmVuKSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ViZmZlYjtcXG4gICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgxKSxcXG4gICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDIpIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDMpLFxcbiAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoNCkge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC4xKTtcXG4gICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcXG4gIC5UZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyIHtcXG4gICAgcGFkZGluZzogMCA0MHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xcbiAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCB7XFxuICAgIHBhZGRpbmc6IDVyZW0gNjRweCAwIDY0cHg7XFxuICB9XFxuICAgIC5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuVGVhY2gtYnJlYWRjcnVtLTExeFg1IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC8qIC50b2dnbGVfb3V0ZXIge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAxOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAtOHB4O1xcbiAgfVxcblxcbiAgLnRvZ2dsZV9pbm5lciB7XFxuICAgIHdpZHRoOiAxNHB4O1xcbiAgICBoZWlnaHQ6IDE0cHg7XFxuICB9XFxuXFxuICAudG9nZ2xlQXRSaWdodCB7XFxuICAgIHBhZGRpbmc6IDMlIDUlIDAgNSU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLnRvZ2dsZUF0TGVmdCB7XFxuICAgIHBhZGRpbmc6IDMlIDUlIDAgNSU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfSAqL1xcblxcbiAgLyogLm1vdXNlaWNvbiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAuVGVhY2gtc2VjdGlvbl9jb250YWluZXItY3l5VE4ge1xcbiAgICBwYWRkaW5nOiAzMnB4IDA7XFxuICB9XFxuXFxuICAgIC5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lci1jeXlUTiAuVGVhY2gtc2VjdGlvbl9jb250ZW50cy0xTlJEciB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgcGFkZGluZzogMCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyLWN5eVROIC5UZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyIC5UZWFjaC1zZWN0aW9uX3RpdGxlLTJueUJkIHtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgICBtYXJnaW46IDE2cHggMDtcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xeUVEWSB7XFxuICAgIHBhZGRpbmc6IDMycHggMDtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMXlFRFkgLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIHBhZGRpbmc6IDAgMTZweDtcXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTF5RURZIC5UZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyIC5UZWFjaC1zZWN0aW9uX3RpdGxlLTJueUJkIHtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgICBtYXJnaW46IDE2cHggMDtcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLWNvbnRlbnRQYXJ0LTFsU2dLIC5UZWFjaC10b3BpY0ltYWdlLTFDTTdqIHtcXG4gICAgICB3aWR0aDogNDhweDtcXG4gICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIHBhZGRpbmc6IDEwcHg7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLXRvcGljSW1hZ2UtMUNNN2ogaW1nIHtcXG4gICAgICAgIHdpZHRoOiAyOHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLlRlYWNoLWNvbnRlbnRQYXJ0LTFsU2dLIC5UZWFjaC1jb250ZW50LXNfekNkIHtcXG4gICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuXFxuICAgICAgLyogLnZpZXdtb3JlIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9ICovXFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2QgLlRlYWNoLXNlY3Rpb25fdGl0bGUtMm55QmQge1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2QgLlRlYWNoLXRleHRjb250ZW50LTNzNUR6IHtcXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgfVxcblxcbiAgLlRlYWNoLXRlYWNoQ29udGVudC0xOVFUayB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XFxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAgIG1heC13aWR0aDogMzIwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLXRlYWNoQ29udGVudC0xOVFUayAuVGVhY2gtZ2V0cmFua3MtM0pjWGEge1xcbiAgICAgIHdpZHRoOiA2MHB4O1xcbiAgICAgIGhlaWdodDogMThweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICB9XFxuXFxuICAgIC5UZWFjaC10ZWFjaENvbnRlbnQtMTlRVGsgLlRlYWNoLXpvb20tM1JYdS0ge1xcbiAgICAgIHdpZHRoOiA0M3B4O1xcbiAgICAgIGhlaWdodDogMTFweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAycHg7XFxuICAgIH1cXG5cXG4gIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogNDBweDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG5cXG4gICAgLyogLnZpZXdtb3JlLm1vYmlsZSB7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICB9ICovXFxuICB9XFxuXFxuICAgIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyB7XFxuICAgICAgd2lkdGg6IDMyMHB4O1xcbiAgICAgIGhlaWdodDogMTgwcHg7XFxuICAgIH1cXG4gICAgICAuVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWxpdmVwYXJ0LTNUS0M4IC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENpcmNsZS0yd2FIUCB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICB0b3A6IC0yNHB4O1xcbiAgICAgICAgbGVmdDogMjU0cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtbGl2ZXBhcnQtM1RLQzggLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIGJvdHRvbTogLTE2cHg7XFxuICAgICAgICBsZWZ0OiA2MXB4O1xcbiAgICAgIH1cXG4gICAgICAuVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWFzc2lnbm1lbnRwYXJ0LTIwRGtJIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENpcmNsZS0yd2FIUCB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgICBvcGFjaXR5OiAwLjE7XFxuICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgbGVmdDogMjhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1hc3NpZ25tZW50cGFydC0yMERrSSAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC1ib3R0b21DaXJjbGUtMVFVOHIge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICAgIGxlZnQ6IDIzNnB4O1xcbiAgICAgIH1cXG4gICAgICAuVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWRvdWJ0cGFydC0yTmt5USAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC10b3BDaXJjbGUtMndhSFAge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgICAgICAgbGVmdDogMjQzcHg7XFxuICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1kb3VidHBhcnQtMk5reVEgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgICAgIHJpZ2h0OiAyMjdweDtcXG4gICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgfVxcblxcbiAgLlRlYWNoLWhlYWRlcmJhY2tncm91bmRtb2JpbGUtMlowUVkge1xcbiAgICB3aWR0aDogMjM3cHg7XFxuICAgIGhlaWdodDogMzQwcHg7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICBsZWZ0OiAwJTtcXG4gICAgYm90dG9tOiAtMXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTgge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiB7XFxuICAgICAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgICAgIG9yZGVyOiAyO1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgaGVpZ2h0OiA1MCU7XFxuICAgICAgcGFkZGluZzogMzJweCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtY3VzdG9tZXJMb2dvLXhGQXE5IHtcXG4gICAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA1NnB4O1xcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLXNyaUNoYWl0YW55YVRleHQtMUhSdDIge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgfVxcbiAgICAgICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtYXV0aG9yV3JhcHBlci0yRlR1NyAuVGVhY2gtYWJvdXRfYXV0aG9yLTNzbjVoIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMzRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWF1dGhvcmltZ2JveC0yOC0yciB7XFxuICAgICAgaGVpZ2h0OiA1MCU7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgICAgIG9yZGVyOiAxO1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWF1dGhvcmltZ2JveC0yOC0yciBpbWcge1xcbiAgICAgICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICAgICAgbWF4LWhlaWdodDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWRvd25sb2FkQXBwLTN0V3J2IHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUSBzcGFuIHtcXG4gICAgbWF4LXdpZHRoOiAzMDdweDtcXG4gIH1cXG5cXG4gIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiB7XFxuICAgIGhlaWdodDogMTAxMnB4O1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICB9XFxuXFxuICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtaGVhZGluZy0xSzVhcyB7XFxuICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgbWFyZ2luOiAyNHB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIC5UZWFjaC1sZWFybmluZ1RleHQtM2FWN04ge1xcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtaGVhZGluZy0xSzVhcyAuVGVhY2gtbGVhcm5pbmdUZXh0LTNhVjdOIGltZyB7XFxuICAgICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICAgIG1pbi13aWR0aDogMTQwcHg7XFxuICAgICAgICB9XFxuXFxuICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtY29udGVudFRleHQtM2VYNTYge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgbWF4LXdpZHRoOiA2NTBweDtcXG4gICAgICBtYXJnaW46IDE2cHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIHtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBtYXJnaW4tdG9wOiA0OHB4O1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIC5UZWFjaC1yZXF1ZXN0RGVtby1BYm9IQyB7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWluLXdpZHRoOiBub25lO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkgLlRlYWNoLXdoYXRzYXBwd3JhcHBlci0zanB6NCB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkgLlRlYWNoLXdoYXRzYXBwd3JhcHBlci0zanB6NCAuVGVhY2gtd2hhdHNhcHAtMzRzcFMge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBvcGFjaXR5OiAxO1xcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IGltZyB7XFxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAgIC8qIC5hY3Rpb25zV3JhcHBlciB7XFxuICAgICAgcmlnaHQ6IDA7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG5cXG4gICAgICAuYWN0aW9uIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcblxcbiAgICAgICAgc3BhbiB7XFxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuYWN0aW9uaW1nYm94IHtcXG4gICAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfSAqL1xcblxcbiAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0ge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCAuVGVhY2gtdGVhY2hJbWdCb3gtbWt6Njcge1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC10b3BTZWN0aW9uLTFaWnItIC5UZWFjaC10ZWFjaFNlY3Rpb24tMzNzbkggLlRlYWNoLXRlYWNoU2VjdGlvbk5hbWUtMUFkUzQge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIG1pbi1oZWlnaHQ6IDI3cmVtO1xcbiAgfVxcblxcbiAgLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIGgzIHtcXG4gICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICBtYXgtd2lkdGg6IDE0Ljg3NXJlbTtcXG4gIH1cXG5cXG4gIC5UZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUSAuVGVhY2gtY2xpZW50c1dyYXBwZXItM2ZKWXEge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMCAwO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbjogMCBhdXRvO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBnYXA6IDFyZW07XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgfVxcblxcbiAgLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIC5UZWFjaC1jbGllbnRzV3JhcHBlci0zZkpZcSAuVGVhY2gtY2xpZW50LUlpOUpGIHtcXG4gICAgd2lkdGg6IDkuNzVyZW07XFxuICAgIGhlaWdodDogN3JlbTtcXG4gIH1cXG5cXG4gIC8qIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudC5hY3RpdmUge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gIH0gKi9cXG5cXG4gIC5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyB7XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweDtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL1RlYWNoL0NvbmZldHRpX21vYmlsZS5zdmcnKTtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZEhlYWRpbmctMnl4UjQge1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIG1hcmdpbjogMCBhdXRvIDI0cHggYXV0bztcXG4gICAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbiAgICB9XFxuXFxuICAgIC5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIGdhcDogMTJweDtcXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDMyOHB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogMTJweDtcXG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XFxuXFxuICAgICAgICAvKiAuaGlnaHRMaWdodCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIH0gKi9cXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNzpudGgtY2hpbGQoMikge1xcbiAgICAgICAgLW1zLWZsZXgtb3JkZXI6IDM7XFxuICAgICAgICAgICAgb3JkZXI6IDM7XFxuXFxuICAgICAgICAvKiAuaGlnaHRMaWdodDo6YWZ0ZXIge1xcbiAgICAgICAgICBjb250ZW50OiAnJztcXG4gICAgICAgIH0gKi9cXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNzpudGgtY2hpbGQoMykge1xcbiAgICAgICAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgICAgICAgb3JkZXI6IDI7XFxuICAgICAgfVxcblxcbiAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCB7XFxuICAgIHBhZGRpbmc6IDMycHggMzJweCAwIDMycHg7XFxuICB9XFxuXFxuICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLWF2YWlsYWJsZVRpdGxlLTI4TWlNIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtcm93LTFWU3lyIHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDM2cHg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1yb3ctMVZTeXIgLlRlYWNoLXBsYXRmb3JtQ29udGFpbmVyLTFJQU5jIHtcXG4gICAgICAgICAgd2lkdGg6IDY4cHg7XFxuICAgICAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtcm93LTFWU3lyIC5UZWFjaC1wbGF0Zm9ybUNvbnRhaW5lci0xSUFOYyAuVGVhY2gtcGxhdGZvcm0tMUdjSloge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuM3B4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtcm93LTFWU3lyIC5UZWFjaC1wbGF0Zm9ybUNvbnRhaW5lci0xSUFOYyAuVGVhY2gtcGxhdGZvcm1Pcy0yREY2RSB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLXJvdy0xVlN5ciAuVGVhY2gtcGxhdGZvcm1Db250YWluZXItMUlBTmM6bGFzdC1jaGlsZCB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLWRlc2t0b3BJbWFnZS1uOW1jcyB7XFxuICAgICAgICB3aWR0aDogMjk2cHg7XFxuICAgICAgICBoZWlnaHQ6IDE0MHB4O1xcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLXN0b3JlLUlaY0RRIHtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLXN0b3JlLUlaY0RRIC5UZWFjaC1wbGF5c3RvcmUtUFhmaEcge1xcbiAgICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICAgIGhlaWdodDogNDJweDtcXG4gICAgICAgICAgbWFyZ2luOiAwIDE2cHggMCAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1zdG9yZS1JWmNEUSAuVGVhY2gtYXBwc3RvcmUtMkhNUGYge1xcbiAgICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICAgIGhlaWdodDogNDJweDtcXG4gICAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgfVxcblxcbiAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIHtcXG4gICAgcGFkZGluZzogNDBweCAwO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgaDEge1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIG1hcmdpbjogMCAxNnB4IDMycHggMTZweDtcXG4gICAgfVxcblxcbiAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIHtcXG4gICAgICB3aWR0aDogMzI4cHg7XFxuICAgICAgZGlzcGxheTogZ3JpZDtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayB7XFxuICAgICAgICBwYWRkaW5nOiAxNnB4IDIwcHggMTZweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigwLCAwLCAwLCAwLjEpO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXoge1xcbiAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWNvbXBhcmlzaW9uVGV4dC0zRm1oUCB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWJveC0yODdzRCB7XFxuICAgICAgICAgICAgd2lkdGg6IDE2cHg7XFxuICAgICAgICAgICAgaGVpZ2h0OiAxNnB4O1xcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWJveC0yODdzRC5UZWFjaC1wb3NpdGl2ZS0yblE3bCB7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NmYztcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtY29tcGFyaXNpb25XcmFwcGVyLU1KMVF6IC5UZWFjaC1ib3gtMjg3c0QuVGVhY2gtbmVnYXRpdmUtM0Q2dzIge1xcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmQ2ZDY7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC10YWJsZURhdGEtM25udFoge1xcbiAgICAgICAgICB3aWR0aDogOTIlO1xcbiAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtdGFibGVEYXRhLTNubnRaIHNwYW4ge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC10YWJsZURhdGEtM25udFogc3BhbjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtZG90V3JhcHBlci1BTlFqNiB7XFxuICAgICAgICAgIHdpZHRoOiA4JTtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWRvdFdyYXBwZXItQU5RajYgLlRlYWNoLWRvdC1mdTlDdCB7XFxuICAgICAgICAgICAgd2lkdGg6IDhweDtcXG4gICAgICAgICAgICBoZWlnaHQ6IDhweDtcXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWRvdFdyYXBwZXItQU5RajYgc3BhbjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDEpIHtcXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XFxuICAgICAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgyKSB7XFxuICAgICAgICBncmlkLXJvdzogMS8yO1xcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAwO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMykge1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDEycHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoNCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDIvMztcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxMnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDYpIHtcXG4gICAgICAgIGdyaWQtcm93OiAzLzQ7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCg4KSB7XFxuICAgICAgICBncmlkLXJvdzogNC81O1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMTApIHtcXG4gICAgICAgIGdyaWQtcm93OiA1LzY7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgxMikge1xcbiAgICAgICAgZ3JpZC1yb3c6IDYvNztcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDEzKSB7XFxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMTQpIHtcXG4gICAgICAgIGdyaWQtcm93OiA3Lzg7XFxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICAgIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZWFjaC9UZWFjaC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsbUJBQW1CO01BQ2YsMEJBQTBCO0NBQy9COztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsdUJBQXVCO0NBQ3hCOztBQUVEOzs7Ozs7Ozs7OztJQVdJOztBQUVKO0VBQ0UsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsdUJBQXVCO09BQ3BCLG9CQUFvQjtHQUN4Qjs7QUFFSDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLG1CQUFtQjtNQUNmLDBCQUEwQjtDQUMvQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQix1QkFBdUI7R0FDeEI7O0FBRUg7Ozs7Ozs7Ozs7Ozs7Ozs7SUFnQkk7O0FBRUo7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsV0FBVztFQUNYLG1CQUFtQjtNQUNmLDBCQUEwQjtDQUMvQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLG1EQUFtRDtZQUMzQywyQ0FBMkM7R0FDcEQ7O0FBRUg7TUFDTSxZQUFZO01BQ1osYUFBYTtLQUNkOztBQUVMO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7O0lBRWpCOzs7Ozs7Ozs7Ozs7OztRQWNJO0dBQ0w7O0FBRUg7TUFDTSxlQUFlO01BQ2YsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsdUJBQXVCO01BQ3ZCLGVBQWU7TUFDZixpQkFBaUI7S0FDbEI7O0FBRUw7TUFDTSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLGVBQWU7TUFDZixpQkFBaUI7S0FDbEI7O0FBRUw7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGNBQWM7Q0FDZjs7QUFFRDtJQUNJLGtCQUFrQjtRQUNkLFNBQVM7SUFDYixxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QixtQkFBbUI7SUFDbkIsV0FBVztHQUNaOztBQUVIO01BQ00sYUFBYTtNQUNiLGFBQWE7TUFDYixtQkFBbUI7TUFDbkIsb0JBQW9CO0tBQ3JCOztBQUVMO1FBQ1EsWUFBWTtRQUNaLGFBQWE7UUFDYix1QkFBdUI7V0FDcEIsb0JBQW9CO09BQ3hCOztBQUVQO01BQ00sZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixpQkFBaUI7TUFDakIsb0JBQW9CO01BQ3BCLGlCQUFpQjtLQUNsQjs7QUFFTDtNQUNNLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtLQUM1Qjs7QUFFTDtRQUNRLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsaUJBQWlCO1FBQ2pCLFlBQVk7UUFDWixtQkFBbUI7T0FDcEI7O0FBRVA7UUFDUSxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLG9CQUFvQjtPQUNyQjs7QUFFUDtNQUNNLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsaUJBQWlCO01BQ2pCLFlBQVk7TUFDWixxQkFBcUI7TUFDckIsY0FBYztNQUNkLHdCQUF3QjtVQUNwQixvQkFBb0I7TUFDeEIsdUJBQXVCO1VBQ25CLG9CQUFvQjtLQUN6Qjs7QUFFTDtRQUNRLGdCQUFnQjtRQUNoQixpQkFBaUI7UUFDakIsWUFBWTtRQUNaLGFBQWE7UUFDYix1QkFBdUI7V0FDcEIsb0JBQW9CO09BQ3hCOztBQUVQO0VBQ0UsdUJBQXVCO0VBQ3ZCLDRCQUE0QjtFQUM1Qiw0QkFBNEI7RUFDNUIscUJBQXFCO0NBQ3RCOztBQUVEO0lBQ0ksMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLHNDQUFzQztJQUN0QyxhQUFhO0dBQ2Q7O0FBRUg7TUFDTSxhQUFhO01BQ2IsY0FBYztNQUNkLGlCQUFpQjtLQUNsQjs7QUFFTDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLHFCQUFxQjtNQUNqQiw0QkFBNEI7Q0FDakM7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxvQkFBb0I7TUFDaEIsZ0JBQWdCO0VBQ3BCLHNCQUFzQjtFQUN0QixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLG9CQUFvQjtNQUNoQixnQkFBZ0I7RUFDcEIscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQyxvQkFBb0I7RUFDcEIsb0JBQW9CO0NBQ3JCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLGFBQWE7SUFDYixtQkFBbUI7R0FDcEI7O0FBRUg7SUFDSSxhQUFhO0lBQ2IsYUFBYTtHQUNkOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYix1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGNBQWM7Q0FDZjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixtQkFBbUI7R0FDcEI7O0FBRUg7TUFDTSxhQUFhO01BQ2IsZ0JBQWdCO01BQ2hCLG1CQUFtQjtLQUNwQjs7QUFFTDtRQUNRLG1CQUFtQjtRQUNuQixhQUFhO1FBQ2IsV0FBVztRQUNYLFlBQVk7UUFDWixZQUFZO1FBQ1osaUJBQWlCO09BQ2xCOztBQUVQO0lBQ0ksaUJBQWlCO0lBQ2pCLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsWUFBWTtJQUNaLHFCQUFxQjtRQUNqQiw0QkFBNEI7SUFDaEMsdUJBQXVCO1FBQ25CLG9CQUFvQjtHQUN6Qjs7QUFFSDtNQUNNLG1CQUFtQjtNQUNuQix1QkFBdUI7TUFDdkIsZ0JBQWdCO01BQ2hCLGlCQUFpQjtNQUNqQixpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLFlBQVk7TUFDWixtQkFBbUI7TUFDbkIsMkJBQTJCO01BQzNCLHdCQUF3QjtNQUN4QixtQkFBbUI7S0FDcEI7O0FBRUw7TUFDTSwyQkFBMkI7TUFDM0Isd0JBQXdCO01BQ3hCLG1CQUFtQjtNQUNuQixxQkFBcUI7TUFDckIsY0FBYztNQUNkLHFCQUFxQjtVQUNqQiw0QkFBNEI7TUFDaEMsdUJBQXVCO1VBQ25CLG9CQUFvQjtLQUN6Qjs7QUFFTDtRQUNRLDJCQUEyQjtRQUMzQix3QkFBd0I7UUFDeEIsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixnQkFBZ0I7UUFDaEIsaUJBQWlCO1FBQ2pCLGlCQUFpQjtRQUNqQiwwQkFBMEI7UUFDMUIscUJBQXFCO1FBQ3JCLGtCQUFrQjtRQUNsQixhQUFhO09BQ2Q7O0FBRVA7UUFDUSxZQUFZO1FBQ1osYUFBYTtPQUNkOztBQUVQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQXNDTTs7QUFFTjtJQUNJLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixpQkFBaUI7O0lBRWpCOzs7Ozs7Ozs7Ozs7UUFZSTtHQUNMOztBQUVIO01BQ00scUJBQXFCO01BQ3JCLGNBQWM7TUFDZCx1QkFBdUI7VUFDbkIsb0JBQW9CO0tBQ3pCOztBQUVMO1FBQ1EsWUFBWTtRQUNaLGFBQWE7T0FDZDs7QUFFUDtVQUNVLFlBQVk7VUFDWixhQUFhO1VBQ2IsdUJBQXVCO2FBQ3BCLG9CQUFvQjtTQUN4Qjs7QUFFVDtRQUNRLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGVBQWU7UUFDZixrQkFBa0I7T0FDbkI7O0FBRVA7SUFDSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjs7QUFFSDtFQUNFLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QiwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGFBQWE7RUFDYixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQywwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7RUFDZCxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixZQUFZO0VBQ1osZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix1REFBdUQ7RUFDdkQseUJBQXlCO0VBQ3pCLDRCQUE0QjtFQUM1Qix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLDBCQUEwQjtNQUN0Qiw4QkFBOEI7Q0FDbkM7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixVQUFVO0VBQ1YsYUFBYTtDQUNkOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsK0RBQStEO1VBQ3ZELHVEQUF1RDtFQUMvRCx1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEOzs7Ozs7Ozs7Ozs7OztJQWNJOztBQUVKO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtJQUNJLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsd0JBQXdCO1FBQ3BCLG9CQUFvQjtJQUN4QixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixrQkFBa0I7R0FDbkI7O0FBRUg7TUFDTSxxQkFBcUI7VUFDakIsNEJBQTRCO0tBQ2pDOztBQUVMO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtFQUN2QixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtJQUNJLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsZ0NBQWdDO1FBQzVCLDRCQUE0QjtJQUNoQyxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixhQUFhO0dBQ2Q7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLG9CQUFvQjtDQUNyQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2Isb0JBQW9CO0dBQ3JCOztBQUVIO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixtREFBbUQ7VUFDM0MsMkNBQTJDO0VBQ25ELG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0NBQ3BCOztBQUVEO01BQ00sMEJBQTBCO01BQzFCLFlBQVk7TUFDWixhQUFhO01BQ2IsY0FBYztNQUNkLFdBQVc7S0FDWjs7QUFFTDtNQUNNLDBCQUEwQjtNQUMxQixZQUFZO01BQ1osYUFBYTtNQUNiLFdBQVc7TUFDWCxZQUFZO0tBQ2I7O0FBRUw7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLDBCQUEwQjtNQUMxQixjQUFjO01BQ2QsV0FBVztLQUNaOztBQUVMO01BQ00sWUFBWTtNQUNaLGFBQWE7TUFDYix1QkFBdUI7TUFDdkIsV0FBVztNQUNYLFlBQVk7TUFDWixhQUFhO0tBQ2Q7O0FBRUw7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLDBCQUEwQjtNQUMxQixjQUFjO01BQ2QsV0FBVztNQUNYLGFBQWE7S0FDZDs7QUFFTDtNQUNNLFlBQVk7TUFDWixhQUFhO01BQ2IsMEJBQTBCO01BQzFCLFdBQVc7TUFDWCxZQUFZO01BQ1osYUFBYTtLQUNkOztBQUVMO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLDBCQUEwQjtDQUMzQjs7QUFFRDtJQUNJLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0FBRUg7SUFDSSxhQUFhO0lBQ2IsY0FBYztJQUNkLCtCQUErQjtJQUMvQixhQUFhO0dBQ2Q7O0FBRUg7TUFDTSxtQkFBbUI7TUFDbkIsZ0JBQWdCO01BQ2hCLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkNBQTJDO0tBQzVDOztBQUVMO1FBQ1EscUJBQXFCO1FBQ3JCLGNBQWM7UUFDZCx1QkFBdUI7WUFDbkIsb0JBQW9CO09BQ3pCOztBQUVQO1VBQ1UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixlQUFlO1NBQ2hCOztBQUVUO1VBQ1UsWUFBWTtVQUNaLGFBQWE7VUFDYixrQkFBa0I7VUFDbEIsbUJBQW1CO1NBQ3BCOztBQUVUO1VBQ1UsdUJBQXVCO1NBQ3hCOztBQUVUO1VBQ1UsMEJBQTBCO1NBQzNCOztBQUVUO1FBQ1EsVUFBVTtPQUNYOztBQUVQO1VBQ1UsV0FBVztVQUNYLFlBQVk7VUFDWixtQkFBbUI7VUFDbkIsMEJBQTBCO1VBQzFCLGlCQUFpQjtTQUNsQjs7QUFFVDtVQUNVLHVCQUF1QjtTQUN4Qjs7QUFFVDtRQUNRLFdBQVc7UUFDWCxxQkFBcUI7UUFDckIsY0FBYztRQUNkLDJCQUEyQjtZQUN2Qix1QkFBdUI7T0FDNUI7O0FBRVA7VUFDVSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLGVBQWU7VUFDZixvQkFBb0I7U0FDckI7O0FBRVQ7TUFDTSwwQkFBMEI7S0FDM0I7O0FBRUw7TUFDTSwwQkFBMEI7S0FDM0I7O0FBRUw7O01BRU0sMEJBQTBCO01BQzFCLGtCQUFrQjtLQUNuQjs7QUFFTDs7TUFFTSx3Q0FBd0M7TUFDeEMsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixpQkFBaUI7S0FDbEI7O0FBRUw7RUFDRTtJQUNFLGdCQUFnQjtHQUNqQjtDQUNGOztBQUVEO0VBQ0U7SUFDRSwwQkFBMEI7R0FDM0I7SUFDQztNQUNFLCtCQUErQjtLQUNoQztDQUNKOztBQUVEO0VBQ0U7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7TUF5Qkk7O0VBRUo7O01BRUk7O0VBRUo7SUFDRSxnQkFBZ0I7R0FDakI7O0lBRUM7TUFDRSwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLGdCQUFnQjtLQUNqQjs7TUFFQztRQUNFLG9CQUFvQjtRQUNwQixlQUFlO09BQ2hCOztFQUVMO0lBQ0UsZ0JBQWdCO0dBQ2pCOztJQUVDO01BQ0UsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQixnQkFBZ0I7S0FDakI7O01BRUM7UUFDRSxvQkFBb0I7UUFDcEIsZUFBZTtPQUNoQjs7RUFFTDtJQUNFLFlBQVk7SUFDWiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7O0lBRUM7TUFDRSxZQUFZO01BQ1osYUFBYTtNQUNiLGFBQWE7TUFDYixjQUFjO0tBQ2Y7O01BRUM7UUFDRSxZQUFZO1FBQ1osYUFBYTtPQUNkOztJQUVIO01BQ0UsZ0JBQWdCOztNQUVoQjs7O1VBR0k7S0FDTDs7TUFFQztRQUNFLG1CQUFtQjtRQUNuQixnQkFBZ0I7T0FDakI7O01BRUQ7UUFDRSxlQUFlO1FBQ2YsWUFBWTtRQUNaLGlCQUFpQjtRQUNqQixtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixhQUFhO09BQ2Q7O0VBRUw7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtHQUNwQjs7SUFFQztNQUNFLFlBQVk7TUFDWixhQUFhO01BQ2IsaUJBQWlCO0tBQ2xCOztJQUVEO01BQ0UsWUFBWTtNQUNaLGFBQWE7TUFDYixtQkFBbUI7S0FDcEI7O0VBRUg7SUFDRSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsMkJBQTJCO1FBQ3ZCLHVCQUF1Qjs7SUFFM0I7OztRQUdJO0dBQ0w7O0lBRUM7TUFDRSxhQUFhO01BQ2IsY0FBYztLQUNmO01BQ0M7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLDBCQUEwQjtRQUMxQixhQUFhO1FBQ2IsV0FBVztRQUNYLFlBQVk7T0FDYjs7TUFFRDtRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsMEJBQTBCO1FBQzFCLGFBQWE7UUFDYixjQUFjO1FBQ2QsV0FBVztPQUNaO01BQ0Q7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLHVCQUF1QjtRQUN2QixhQUFhO1FBQ2IsV0FBVztRQUNYLFdBQVc7T0FDWjs7TUFFRDtRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCLGFBQWE7UUFDYixjQUFjO1FBQ2QsWUFBWTtPQUNiO01BQ0Q7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLDBCQUEwQjtRQUMxQixZQUFZO1FBQ1osV0FBVztRQUNYLFdBQVc7T0FDWjs7TUFFRDtRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCLGFBQWE7UUFDYixhQUFhO1FBQ2IsY0FBYztPQUNmOztFQUVMO0lBQ0UsYUFBYTtJQUNiLGNBQWM7SUFDZCxTQUFTO0lBQ1QsU0FBUztJQUNULGNBQWM7SUFDZCxhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsYUFBYTtJQUNiLFlBQVk7SUFDWix1QkFBdUI7UUFDbkIsb0JBQW9CO0dBQ3pCOztJQUVDO01BQ0Usa0JBQWtCO1VBQ2QsU0FBUztNQUNiLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQix1QkFBdUI7VUFDbkIsb0JBQW9CO01BQ3hCLG1CQUFtQjtNQUNuQixZQUFZO01BQ1osWUFBWTtNQUNaLG1CQUFtQjtLQUNwQjs7TUFFQztRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLGlCQUFpQjtRQUNqQixvQkFBb0I7T0FDckI7O01BRUQ7UUFDRSxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtRQUNuQixnQkFBZ0I7T0FDakI7UUFDQztVQUNFLGdCQUFnQjtVQUNoQixrQkFBa0I7VUFDbEIsb0JBQW9CO1NBQ3JCOztJQUVMO01BQ0UsWUFBWTtNQUNaLFlBQVk7TUFDWixrQkFBa0I7VUFDZCxTQUFTO0tBQ2Q7O01BRUM7UUFDRSxnQkFBZ0I7UUFDaEIsaUJBQWlCO09BQ2xCOztFQUVMO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsYUFBYTtHQUNkOztFQUVEO0lBQ0UsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsZUFBZTtJQUNmLG1CQUFtQjtHQUNwQjs7SUFFQztNQUNFLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLGdCQUFnQjtNQUNoQix5QkFBeUI7S0FDMUI7O01BRUM7UUFDRSxhQUFhO09BQ2Q7O1FBRUM7VUFDRSxRQUFRO1VBQ1IsaUJBQWlCO1NBQ2xCOztJQUVMO01BQ0UsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLHlCQUF5QjtLQUMxQjs7SUFFRDtNQUNFLDJCQUEyQjtVQUN2Qix1QkFBdUI7TUFDM0IsaUJBQWlCO0tBQ2xCOztNQUVDO1FBQ0Usa0JBQWtCO1FBQ2xCLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsZ0JBQWdCO09BQ2pCOztNQUVEO1FBQ0UsaUJBQWlCO1FBQ2pCLG9CQUFvQjtPQUNyQjs7UUFFQztVQUNFLGdCQUFnQjtVQUNoQixrQkFBa0I7VUFDbEIsV0FBVztVQUNYLGVBQWU7U0FDaEI7O1FBRUQ7VUFDRSxZQUFZO1VBQ1osYUFBYTtTQUNkOztJQUVMOzs7Ozs7Ozs7Ozs7Ozs7OztRQWlCSTs7SUFFSjtNQUNFLHNCQUFzQjtVQUNsQix3QkFBd0I7TUFDNUIsY0FBYzs7TUFFZDs7VUFFSTtLQUNMO1FBQ0c7VUFDRSxZQUFZO1VBQ1osYUFBYTtTQUNkOztRQUVEO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtTQUNuQjs7RUFFUDtJQUNFLHFCQUFxQjtJQUNyQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIscUJBQXFCO0dBQ3RCOztFQUVEO0lBQ0Usb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0NBQXNDO0lBQ3RDLG1DQUFtQztJQUNuQyxVQUFVO0lBQ1YsMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsYUFBYTtHQUNkOztFQUVEOztNQUVJOztFQUVKO0lBQ0UsbUJBQW1CO0lBQ25CLDJEQUEyRDtHQUM1RDs7SUFFQztNQUNFLGdCQUFnQjtNQUNoQixvQkFBb0I7TUFDcEIseUJBQXlCO01BQ3pCLGlCQUFpQjtLQUNsQjs7SUFFRDtNQUNFLDJCQUEyQjtNQUMzQixVQUFVO0tBQ1g7O01BRUM7UUFDRSxZQUFZO1FBQ1osaUJBQWlCO1FBQ2pCLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLGtCQUFrQjtRQUNsQixjQUFjOztRQUVkOztZQUVJO09BQ0w7O01BRUQ7UUFDRSxrQkFBa0I7WUFDZCxTQUFTOztRQUViOztZQUVJO09BQ0w7O01BRUQ7UUFDRSxrQkFBa0I7WUFDZCxTQUFTO09BQ2Q7O0VBRUw7SUFDRSwwQkFBMEI7R0FDM0I7O0lBRUM7TUFDRSwyQkFBMkI7S0FDNUI7O01BRUM7UUFDRSxnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLFVBQVU7UUFDVixvQkFBb0I7UUFDcEIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtPQUM3Qjs7UUFFQztVQUNFLFlBQVk7VUFDWixxQkFBcUI7Y0FDakIsNEJBQTRCO1VBQ2hDLG1CQUFtQjtVQUNuQixpQkFBaUI7U0FDbEI7O1VBRUM7WUFDRSxrQkFBa0I7WUFDbEIsa0JBQWtCO1dBQ25COztVQUVEO1lBQ0UsZ0JBQWdCO1lBQ2hCLGtCQUFrQjtXQUNuQjs7UUFFSDtVQUNFLGdCQUFnQjtTQUNqQjs7TUFFSDtRQUNFLGFBQWE7UUFDYixjQUFjO1FBQ2QsYUFBYTtPQUNkOztNQUVEO1FBQ0Usc0JBQXNCO1lBQ2xCLHdCQUF3QjtPQUM3Qjs7UUFFQztVQUNFLGFBQWE7VUFDYixhQUFhO1VBQ2IsbUJBQW1CO1NBQ3BCOztRQUVEO1VBQ0UsYUFBYTtVQUNiLGFBQWE7VUFDYixVQUFVO1NBQ1g7O0VBRVA7SUFDRSxnQkFBZ0I7SUFDaEIsMEJBQTBCO0dBQzNCOztJQUVDO01BQ0UsZ0JBQWdCO01BQ2hCLG9CQUFvQjtNQUNwQix5QkFBeUI7S0FDMUI7O0lBRUQ7TUFDRSxhQUFhO01BQ2IsY0FBYztNQUNkLDJCQUEyQjtNQUMzQixhQUFhO0tBQ2Q7O01BRUM7UUFDRSw2QkFBNkI7UUFDN0IsZ0JBQWdCO1FBQ2hCLHFCQUFxQjtRQUNyQixjQUFjO1FBQ2QsMkNBQTJDO09BQzVDOztRQUVDO1VBQ0UscUJBQXFCO1VBQ3JCLGNBQWM7VUFDZCx1QkFBdUI7Y0FDbkIsb0JBQW9CO1NBQ3pCOztVQUVDO1lBQ0UsZ0JBQWdCO1lBQ2hCLGtCQUFrQjtZQUNsQixlQUFlO1dBQ2hCOztVQUVEO1lBQ0UsWUFBWTtZQUNaLGFBQWE7WUFDYixpQkFBaUI7WUFDakIsbUJBQW1CO1dBQ3BCOztVQUVEO1lBQ0UsdUJBQXVCO1dBQ3hCOztVQUVEO1lBQ0UsMEJBQTBCO1dBQzNCOztRQUVIO1VBQ0UsV0FBVztVQUNYLHFCQUFxQjtVQUNyQixjQUFjO1VBQ2QsMkJBQTJCO2NBQ3ZCLHVCQUF1QjtTQUM1Qjs7VUFFQztZQUNFLGdCQUFnQjtZQUNoQixrQkFBa0I7WUFDbEIsZUFBZTtZQUNmLG9CQUFvQjtXQUNyQjs7VUFFRDtZQUNFLGlCQUFpQjtXQUNsQjs7UUFFSDtVQUNFLFVBQVU7U0FDWDs7VUFFQztZQUNFLFdBQVc7WUFDWCxZQUFZO1lBQ1osbUJBQW1CO1lBQ25CLDBCQUEwQjtZQUMxQixnQkFBZ0I7V0FDakI7O1VBRUQ7WUFDRSxpQkFBaUI7V0FDbEI7O01BRUw7UUFDRSxpQkFBaUI7UUFDakIsaUJBQWlCO1FBQ2pCLGVBQWU7T0FDaEI7O01BRUQ7UUFDRSxjQUFjO1FBQ2QsaUJBQWlCO1FBQ2pCLGVBQWU7T0FDaEI7O01BRUQ7UUFDRSxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLGNBQWM7UUFDZCxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLGNBQWM7T0FDZjs7TUFFRDtRQUNFLGNBQWM7T0FDZjs7TUFFRDtRQUNFLGNBQWM7T0FDZjs7TUFFRDtRQUNFLGNBQWM7T0FDZjs7TUFFRDtRQUNFLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLGNBQWM7UUFDZCxvQkFBb0I7T0FDckI7Q0FDTlwiLFwiZmlsZVwiOlwiVGVhY2guc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIuYnJlYWRjcnVtIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4uYnJlYWRjcnVtIHNwYW4ge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLnRvZ2dsZV9vdXRlciB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMTcuOHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBwYWRkaW5nOiAycHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLXRvcDogLTEwcHg7XFxufVxcblxcbi50b2dnbGVfb24ge1xcbiAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi50b2dnbGVfaW5uZXIge1xcbiAgd2lkdGg6IDE0cHg7XFxuICBoZWlnaHQ6IDE0cHg7XFxuICBib3JkZXItcmFkaXVzOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLyogICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMzZweDtcXG4gIGJvdHRvbTogODdweDtcXG4gIHJpZ2h0OiA1NjBweDtcXG5cXG4gIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICB9XFxufSAqL1xcblxcbi5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgd2lkdGg6IDQ0NXB4O1xcbiAgaGVpZ2h0OiA1ODRweDtcXG59XFxuXFxuLmhlYWRlcmJhY2tncm91bmRtb2JpbGUgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIH1cXG5cXG4uYXV0aG9yaW1nYm94IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLmF1dGhvcmltZ2JveCBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDY0MHB4O1xcbiAgICBtYXgtaGVpZ2h0OiA1NjBweDtcXG4gICAganVzdGlmeS1zZWxmOiBmbGV4LWVuZDtcXG4gIH1cXG5cXG4vKiAudmlld21vcmUubW9iaWxlIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiAzMHB4O1xcblxcbiAgaW1nIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgbWFyZ2luLXRvcDogMXB4O1xcbiAgfVxcbn0gKi9cXG5cXG4uY29udGVudFBhcnQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHdpZHRoOiA2MCU7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLmNvbnRlbnRQYXJ0IC50b3BpY0ltYWdlIHtcXG4gICAgd2lkdGg6IDcycHg7XFxuICAgIGhlaWdodDogNzJweDtcXG4gICAgcGFkZGluZzogMTZweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIH1cXG5cXG4uY29udGVudFBhcnQgLnRvcGljSW1hZ2UgaW1nIHtcXG4gICAgICB3aWR0aDogNDJweDtcXG4gICAgICBoZWlnaHQ6IDQycHg7XFxuICAgIH1cXG5cXG4uY29udGVudFBhcnQgLmNvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWluLXdpZHRoOiAyOTBweDtcXG4gICAgbWF4LXdpZHRoOiA1NDBweDtcXG5cXG4gICAgLyogLnZpZXdtb3JlIHtcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgY29sb3I6ICMwMDc2ZmY7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuXFxuICAgICAgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgICB9XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4uY29udGVudFBhcnQgLmNvbnRlbnQgLnNlY3Rpb25fdGl0bGUge1xcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgIG1hcmdpbjogMTJweCAwO1xcbiAgICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgICBsaW5lLWhlaWdodDogNDhweDtcXG4gICAgICBsZXR0ZXItc3BhY2luZzogLTEuMnB4O1xcbiAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG4uY29udGVudFBhcnQgLmNvbnRlbnQgLnRleHRjb250ZW50IHtcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgbWF4LXdpZHRoOiAzNzBweDtcXG4gICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBoZWlnaHQ6IDU2MHB4O1xcbn1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IHtcXG4gICAgLW1zLWZsZXgtb3JkZXI6IDA7XFxuICAgICAgICBvcmRlcjogMDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDQ4cHggNjRweDtcXG4gICAgd2lkdGg6IDUwJTtcXG4gIH1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5jdXN0b21lckxvZ28ge1xcbiAgICAgIHdpZHRoOiAxMjBweDtcXG4gICAgICBoZWlnaHQ6IDgwcHg7XFxuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICAgIH1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5jdXN0b21lckxvZ28gaW1nIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLnNyaUNoYWl0YW55YVRleHQge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmF1dGhvcldyYXBwZXIge1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIH1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5hdXRob3JXcmFwcGVyIC5hdXRob3JfdGl0bGUge1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICAgICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmF1dGhvcldyYXBwZXIgLmFib3V0X2F1dGhvciB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICAgICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmFsbGN1c3RvbWVycyB7XFxuICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuYWxsY3VzdG9tZXJzIGltZyB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgICAgd2lkdGg6IDIwcHg7XFxuICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuLmF2YWlsYWJsZUNvbnRhaW5lciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogODBweCAxMTNweCAwIDE2NHB4O1xcbiAgcGFkZGluZzogNXJlbSAxMTNweCAwIDE2NHB4O1xcbiAgLy8gbWF4LXdpZHRoOiAxMzAwcHg7XFxufVxcblxcbi5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5kZXNrdG9wSW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA2NDhweDtcXG4gICAgICBoZWlnaHQ6IDMyOHB4O1xcbiAgICAgIG1hcmdpbi10b3A6IDM1cHg7XFxuICAgIH1cXG5cXG4uYXZhaWxhYmxlVGl0bGUge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLmF2YWlsYWJsZSB7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLmF2YWlsYWJsZUNvbnRlbnRTZWN0aW9uIHtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59XFxuXFxuLnBsYXRmb3JtQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBtYXJnaW4tcmlnaHQ6IDJyZW07XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XFxufVxcblxcbi5wbGF0Zm9ybSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luOiA4cHggMCA0cHggMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4ucGxhdGZvcm1PcyB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5yb3cge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICBtYXJnaW46IDQ4cHggMCAzMnB4IDA7XFxuICBtYXJnaW46IDNyZW0gMCAycmVtIDA7XFxufVxcblxcbi5zdG9yZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0cmVtO1xcbn1cXG5cXG4uc3RvcmUgLnBsYXlzdG9yZSB7XFxuICAgIHdpZHRoOiAxNDBweDtcXG4gICAgaGVpZ2h0OiA0MnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICB9XFxuXFxuLnN0b3JlIC5hcHBzdG9yZSB7XFxuICAgIHdpZHRoOiAxNDBweDtcXG4gICAgaGVpZ2h0OiA0MnB4O1xcbiAgfVxcblxcbi5kaXNwbGF5Q2xpZW50cyBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC5lbXB0eUNhcmQgLnRvcENhcmQgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciB7XFxuICBoZWlnaHQ6IDY4MHB4O1xcbiAgcGFkZGluZzogMjRweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWNlMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGN1cnNvcjogYWxpYXM7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogNDhweDtcXG4gICAgbGluZS1oZWlnaHQ6IDY2cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBtYXgtd2lkdGg6IDU0MnB4O1xcbiAgICBtYXJnaW46IDEycHggMCAwIDA7XFxuICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQge1xcbiAgICAgIHdpZHRoOiAxODVweDtcXG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XFxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQgaW1nIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIGJvdHRvbTogLThweDtcXG4gICAgICAgIGxlZnQ6IC04cHg7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgbWF4LXdpZHRoOiAyMjVweDtcXG4gICAgICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNjRweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC5yZXF1ZXN0RGVtbyB7XFxuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgICAgY29sb3I6ICMwMDA7XFxuICAgICAgcGFkZGluZzogMTZweCAyNHB4O1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBtYXgtY29udGVudDtcXG4gICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciAud2hhdHNhcHAge1xcbiAgICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gICAgICAgIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XFxuICAgICAgICBvcGFjaXR5OiAwLjY7XFxuICAgICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIGltZyB7XFxuICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICB9XFxuXFxuLyogLmFjdGlvbnNXcmFwcGVyIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBib3R0b206IDI2cHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcblxcbiAgICAuYWN0aW9uIHtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG5cXG4gICAgICBzcGFuIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICBvcGFjaXR5OiAwLjc7XFxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmFjdGlvbmltZ2JveCB7XFxuICAgICAgICB3aWR0aDogMjRweDtcXG4gICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcblxcbiAgICAgICAgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDEwcHg7XFxuICAgICAgICAgIGhlaWdodDogOXB4O1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfVxcbiAgfSAqL1xcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNTZweDtcXG5cXG4gICAgLyogLmZlYXR1cmVzU2VjdGlvbiB7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbi1sZWZ0OiAzMCU7XFxuXFxuICAgICAgc3BhbiB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAzMnB4O1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gICAgICB9XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiB7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoSW1nQm94IHtcXG4gICAgICAgIHdpZHRoOiA0MHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xcbiAgICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3ggaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hTZWN0aW9uTmFtZSB7XFxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICAgICAgICBjb2xvcjogI2ZmNjQwMDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmNvbnRlbnRUZXh0IHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgIG1hcmdpbjogMTZweCAwIDAgMDtcXG4gICAgbWF4LXdpZHRoOiA2ODdweDtcXG4gIH1cXG5cXG4uZG93bmxvYWRBcHAge1xcbiAgbWFyZ2luLXRvcDogNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4uZG93bmxvYWRBcHAgLmRvd25sb2FkVGV4dCB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLmRvd25sb2FkQXBwIC5wbGF5U3RvcmVJY29uIHtcXG4gIHdpZHRoOiAxMzJweDtcXG4gIGhlaWdodDogNDBweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWluLWhlaWdodDogNDY0cHg7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMTJweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4gYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4gYTpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAvLyBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgZ2FwOiAxNnB4O1xcbiAgZ2FwOiAxcmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogMjAycHg7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXNpemU6IDEuNXJlbTtcXG4gIGNvbG9yOiByZ2JhKDM3LCA0MCwgNDMsIDAuNik7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICBwYWRkaW5nOiAxLjVyZW07XFxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZSB7XFxuICB3aWR0aDogNTJweDtcXG4gIGhlaWdodDogNTJweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5jbGllbnRzIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYzZWI7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5zdHVkZW50cyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUudGVzdHMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnF1ZXN0aW9ucyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmVmO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQge1xcbiAgZm9udC1zaXplOiAzNnB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnF1ZXN0aW9uc0hpZ2hsaWdodCB7XFxuICBjb2xvcjogIzAwYWMyNjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnRlc3RzSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQuc3R1ZGVudHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICM4YzAwZmU7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5jbGllbnRzSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjZjYwO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5zdWJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4vKiAudG9nZ2xlQXRSaWdodCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi50b2dnbGVBdExlZnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggMCA2NHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxufSAqL1xcblxcbi5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgcGFkZGluZzogMTg0cHggMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwIDY0cHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICB9XFxuXFxuLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UgLnNlY3Rpb25fY29udGVudHMgLmNvbnRlbnRQYXJ0IHtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICB9XFxuXFxuLnNlY3Rpb25fY29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMTg0cHggMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uc2VjdGlvbl9jb250YWluZXIgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDAgNjRweDtcXG4gICAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4udGVhY2hDb250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXgtd2lkdGg6IDMzN3B4O1xcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxufVxcblxcbi50ZWFjaENvbnRlbnQgLmdldHJhbmtzIHtcXG4gICAgd2lkdGg6IDg5cHg7XFxuICAgIGhlaWdodDogMjZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogLTVweDtcXG4gIH1cXG5cXG4udGVhY2hDb250ZW50IC56b29tIHtcXG4gICAgd2lkdGg6IDY0cHg7XFxuICAgIGhlaWdodDogMThweDtcXG4gIH1cXG5cXG4uaW1hZ2VQYXJ0IHtcXG4gIHdpZHRoOiA0MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5pbWFnZVBhcnQgLmVtcHR5Q2FyZCB7XFxuICB3aWR0aDogNjA2cHg7XFxuICBoZWlnaHQ6IDM0MXB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMzJweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLmltYWdlUGFydCAuZW1wdHlDYXJkIC50b3BDYXJkIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHotaW5kZXg6IDE7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxufVxcblxcbi5pbWFnZVBhcnQgLmJvdHRvbUNpcmNsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogNDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG59XFxuXFxuLmltYWdlUGFydCAudG9wQ2lyY2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA2NHB4O1xcbiAgaGVpZ2h0OiA2NHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0LmxpdmVwYXJ0IC5lbXB0eUNhcmQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYm90dG9tOiAtNDRweDtcXG4gICAgICBsZWZ0OiA3OXB4O1xcbiAgICB9XFxuXFxuLmltYWdlUGFydC5saXZlcGFydCAuZW1wdHlDYXJkIC50b3BDaXJjbGUge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDUwcHg7XFxuICAgIH1cXG5cXG4uaW1hZ2VQYXJ0LmFzc2lnbm1lbnRwYXJ0IC5lbXB0eUNhcmQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxuICAgICAgYm90dG9tOiAtNDRweDtcXG4gICAgICBsZWZ0OiA0OHB4O1xcbiAgICB9XFxuXFxuLmltYWdlUGFydC5hc3NpZ25tZW50cGFydCAuZW1wdHlDYXJkIC50b3BDaXJjbGUge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDY3cHg7XFxuICAgICAgb3BhY2l0eTogMC4zO1xcbiAgICB9XFxuXFxuLmltYWdlUGFydC5kb3VidHBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDY4cHg7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICB9XFxuXFxuLmltYWdlUGFydC5kb3VidHBhcnQgLmVtcHR5Q2FyZCAudG9wQ2lyY2xlIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICB0b3A6IC00NHB4O1xcbiAgICAgIHJpZ2h0OiAzNnB4O1xcbiAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgfVxcblxcbi5hbGxjdXN0b21lcnMgcCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uY29udGVudCBwIHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBsZXR0ZXItc3BhY2luZzogMC40OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi50YWJsZUNvbnRhaW5lciB7XFxuICBwYWRkaW5nOiA1NnB4IDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4udGFibGVDb250YWluZXIgaDEge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIHtcXG4gICAgd2lkdGg6IDk1NnB4O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAxZnI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiB7XFxuICAgICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMCwgMCwgMCwgMC4xKTtcXG4gICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIge1xcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuY29tcGFyaXNpb25XcmFwcGVyIC5jb21wYXJpc2lvblRleHQge1xcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuY29tcGFyaXNpb25XcmFwcGVyIC5ib3gge1xcbiAgICAgICAgICB3aWR0aDogMjBweDtcXG4gICAgICAgICAgaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMTJweDtcXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmJveC5wb3NpdGl2ZSB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNjZmM7XFxuICAgICAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciAuYm94Lm5lZ2F0aXZlIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDZkNjtcXG4gICAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuZG90V3JhcHBlciB7XFxuICAgICAgICB3aWR0aDogNSU7XFxuICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5kb3RXcmFwcGVyIC5kb3Qge1xcbiAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDhweDtcXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5kb3RXcmFwcGVyIC5kb3QucmVkIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzBjMDtcXG4gICAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAudGFibGVEYXRhIHtcXG4gICAgICAgIHdpZHRoOiA5NSU7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAudGFibGVEYXRhIHNwYW4ge1xcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gICAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQob2RkKSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJlYjtcXG4gICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZChldmVuKSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ViZmZlYjtcXG4gICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgxKSxcXG4gICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDIpIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDMpLFxcbiAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoNCkge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC4xKTtcXG4gICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcXG4gIC5zZWN0aW9uX2NvbnRlbnRzIHtcXG4gICAgcGFkZGluZzogMCA0MHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xcbiAgLmF2YWlsYWJsZUNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDVyZW0gNjRweCAwIDY0cHg7XFxuICB9XFxuICAgIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuYnJlYWRjcnVtIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC8qIC50b2dnbGVfb3V0ZXIge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAxOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAtOHB4O1xcbiAgfVxcblxcbiAgLnRvZ2dsZV9pbm5lciB7XFxuICAgIHdpZHRoOiAxNHB4O1xcbiAgICBoZWlnaHQ6IDE0cHg7XFxuICB9XFxuXFxuICAudG9nZ2xlQXRSaWdodCB7XFxuICAgIHBhZGRpbmc6IDMlIDUlIDAgNSU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLnRvZ2dsZUF0TGVmdCB7XFxuICAgIHBhZGRpbmc6IDMlIDUlIDAgNSU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfSAqL1xcblxcbiAgLyogLm1vdXNlaWNvbiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAuc2VjdGlvbl9jb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAzMnB4IDA7XFxuICB9XFxuXFxuICAgIC5zZWN0aW9uX2NvbnRhaW5lciAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgcGFkZGluZzogMCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLnNlY3Rpb25fY29udGFpbmVyIC5zZWN0aW9uX2NvbnRlbnRzIC5zZWN0aW9uX3RpdGxlIHtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgICBtYXJnaW46IDE2cHggMDtcXG4gICAgICB9XFxuXFxuICAuc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSB7XFxuICAgIHBhZGRpbmc6IDMycHggMDtcXG4gIH1cXG5cXG4gICAgLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIHBhZGRpbmc6IDAgMTZweDtcXG4gICAgfVxcblxcbiAgICAgIC5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlIC5zZWN0aW9uX2NvbnRlbnRzIC5zZWN0aW9uX3RpdGxlIHtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgICBtYXJnaW46IDE2cHggMDtcXG4gICAgICB9XFxuXFxuICAuY29udGVudFBhcnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLmNvbnRlbnRQYXJ0IC50b3BpY0ltYWdlIHtcXG4gICAgICB3aWR0aDogNDhweDtcXG4gICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIHBhZGRpbmc6IDEwcHg7XFxuICAgIH1cXG5cXG4gICAgICAuY29udGVudFBhcnQgLnRvcGljSW1hZ2UgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAyOHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLmNvbnRlbnRQYXJ0IC5jb250ZW50IHtcXG4gICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuXFxuICAgICAgLyogLnZpZXdtb3JlIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9ICovXFxuICAgIH1cXG5cXG4gICAgICAuY29udGVudFBhcnQgLmNvbnRlbnQgLnNlY3Rpb25fdGl0bGUge1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuY29udGVudFBhcnQgLmNvbnRlbnQgLnRleHRjb250ZW50IHtcXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgfVxcblxcbiAgLnRlYWNoQ29udGVudCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XFxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAgIG1heC13aWR0aDogMzIwcHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLnRlYWNoQ29udGVudCAuZ2V0cmFua3Mge1xcbiAgICAgIHdpZHRoOiA2MHB4O1xcbiAgICAgIGhlaWdodDogMThweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICB9XFxuXFxuICAgIC50ZWFjaENvbnRlbnQgLnpvb20ge1xcbiAgICAgIHdpZHRoOiA0M3B4O1xcbiAgICAgIGhlaWdodDogMTFweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAycHg7XFxuICAgIH1cXG5cXG4gIC5pbWFnZVBhcnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogNDBweDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG5cXG4gICAgLyogLnZpZXdtb3JlLm1vYmlsZSB7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICB9ICovXFxuICB9XFxuXFxuICAgIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCB7XFxuICAgICAgd2lkdGg6IDMyMHB4O1xcbiAgICAgIGhlaWdodDogMTgwcHg7XFxuICAgIH1cXG4gICAgICAuaW1hZ2VQYXJ0LmxpdmVwYXJ0IC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICB0b3A6IC0yNHB4O1xcbiAgICAgICAgbGVmdDogMjU0cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5pbWFnZVBhcnQubGl2ZXBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIGJvdHRvbTogLTE2cHg7XFxuICAgICAgICBsZWZ0OiA2MXB4O1xcbiAgICAgIH1cXG4gICAgICAuaW1hZ2VQYXJ0LmFzc2lnbm1lbnRwYXJ0IC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgICBvcGFjaXR5OiAwLjE7XFxuICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgbGVmdDogMjhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmltYWdlUGFydC5hc3NpZ25tZW50cGFydCAuZW1wdHlDYXJkIC5ib3R0b21DaXJjbGUge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICAgIGxlZnQ6IDIzNnB4O1xcbiAgICAgIH1cXG4gICAgICAuaW1hZ2VQYXJ0LmRvdWJ0cGFydCAuZW1wdHlDYXJkIC50b3BDaXJjbGUge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgICAgICAgbGVmdDogMjQzcHg7XFxuICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICB9XFxuXFxuICAgICAgLmltYWdlUGFydC5kb3VidHBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgICAgIHJpZ2h0OiAyMjdweDtcXG4gICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgfVxcblxcbiAgLmhlYWRlcmJhY2tncm91bmRtb2JpbGUge1xcbiAgICB3aWR0aDogMjM3cHg7XFxuICAgIGhlaWdodDogMzQwcHg7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICBsZWZ0OiAwJTtcXG4gICAgYm90dG9tOiAtMXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLmN1c3RvbWVyc19jb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gICAgLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyB7XFxuICAgICAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgICAgIG9yZGVyOiAyO1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgaGVpZ2h0OiA1MCU7XFxuICAgICAgcGFkZGluZzogMzJweCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuY3VzdG9tZXJMb2dvIHtcXG4gICAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA1NnB4O1xcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLnNyaUNoYWl0YW55YVRleHQge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgfVxcbiAgICAgICAgLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuYXV0aG9yV3JhcHBlciAuYWJvdXRfYXV0aG9yIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMzRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLmN1c3RvbWVyc19jb250YWluZXIgLmF1dGhvcmltZ2JveCB7XFxuICAgICAgaGVpZ2h0OiA1MCU7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgICAgIG9yZGVyOiAxO1xcbiAgICB9XFxuXFxuICAgICAgLmN1c3RvbWVyc19jb250YWluZXIgLmF1dGhvcmltZ2JveCBpbWcge1xcbiAgICAgICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICAgICAgbWF4LWhlaWdodDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAudGVhY2hDb250YWluZXIgLmRvd25sb2FkQXBwIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyBzcGFuIHtcXG4gICAgbWF4LXdpZHRoOiAzMDdweDtcXG4gIH1cXG5cXG4gIC50ZWFjaENvbnRhaW5lciB7XFxuICAgIGhlaWdodDogMTAxMnB4O1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICB9XFxuXFxuICAgIC50ZWFjaENvbnRhaW5lciAuaGVhZGluZyB7XFxuICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgbWFyZ2luOiAyNHB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQge1xcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAuaGVhZGluZyAubGVhcm5pbmdUZXh0IGltZyB7XFxuICAgICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICAgIG1pbi13aWR0aDogMTQwcHg7XFxuICAgICAgICB9XFxuXFxuICAgIC50ZWFjaENvbnRhaW5lciAuY29udGVudFRleHQge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgbWF4LXdpZHRoOiA2NTBweDtcXG4gICAgICBtYXJnaW46IDE2cHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIHtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBtYXJnaW4tdG9wOiA0OHB4O1xcbiAgICB9XFxuXFxuICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC5yZXF1ZXN0RGVtbyB7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWluLXdpZHRoOiBub25lO1xcbiAgICAgIH1cXG5cXG4gICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gICAgICB9XFxuXFxuICAgICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciAud2hhdHNhcHAge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBvcGFjaXR5OiAxO1xcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIGltZyB7XFxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAgIC8qIC5hY3Rpb25zV3JhcHBlciB7XFxuICAgICAgcmlnaHQ6IDA7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG5cXG4gICAgICAuYWN0aW9uIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcblxcbiAgICAgICAgc3BhbiB7XFxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuYWN0aW9uaW1nYm94IHtcXG4gICAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfSAqL1xcblxcbiAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24ge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoU2VjdGlvbk5hbWUge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIG1pbi1oZWlnaHQ6IDI3cmVtO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIGgzIHtcXG4gICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICBtYXgtd2lkdGg6IDE0Ljg3NXJlbTtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMCAwO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbjogMCBhdXRvO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBnYXA6IDFyZW07XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50IHtcXG4gICAgd2lkdGg6IDkuNzVyZW07XFxuICAgIGhlaWdodDogN3JlbTtcXG4gIH1cXG5cXG4gIC8qIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudC5hY3RpdmUge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gIH0gKi9cXG5cXG4gIC5hY2hpZXZlZENvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweDtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL1RlYWNoL0NvbmZldHRpX21vYmlsZS5zdmcnKTtcXG4gIH1cXG5cXG4gICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZEhlYWRpbmcge1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIG1hcmdpbjogMCBhdXRvIDI0cHggYXV0bztcXG4gICAgICBtYXgtd2lkdGg6IDMwMHB4O1xcbiAgICB9XFxuXFxuICAgIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIGdhcDogMTJweDtcXG4gICAgfVxcblxcbiAgICAgIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDMyOHB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogMTJweDtcXG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XFxuXFxuICAgICAgICAvKiAuaGlnaHRMaWdodCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIH0gKi9cXG4gICAgICB9XFxuXFxuICAgICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZDpudGgtY2hpbGQoMikge1xcbiAgICAgICAgLW1zLWZsZXgtb3JkZXI6IDM7XFxuICAgICAgICAgICAgb3JkZXI6IDM7XFxuXFxuICAgICAgICAvKiAuaGlnaHRMaWdodDo6YWZ0ZXIge1xcbiAgICAgICAgICBjb250ZW50OiAnJztcXG4gICAgICAgIH0gKi9cXG4gICAgICB9XFxuXFxuICAgICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZDpudGgtY2hpbGQoMykge1xcbiAgICAgICAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgICAgICAgb3JkZXI6IDI7XFxuICAgICAgfVxcblxcbiAgLmF2YWlsYWJsZUNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDMycHggMzJweCAwIDMycHg7XFxuICB9XFxuXFxuICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLmF2YWlsYWJsZVRpdGxlIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDM2cHg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIHtcXG4gICAgICAgICAgd2lkdGg6IDY4cHg7XFxuICAgICAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lciAucGxhdGZvcm0ge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuM3B4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lciAucGxhdGZvcm1PcyB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXI6bGFzdC1jaGlsZCB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLmRlc2t0b3BJbWFnZSB7XFxuICAgICAgICB3aWR0aDogMjk2cHg7XFxuICAgICAgICBoZWlnaHQ6IDE0MHB4O1xcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnN0b3JlIHtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnN0b3JlIC5wbGF5c3RvcmUge1xcbiAgICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICAgIGhlaWdodDogNDJweDtcXG4gICAgICAgICAgbWFyZ2luOiAwIDE2cHggMCAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5zdG9yZSAuYXBwc3RvcmUge1xcbiAgICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICAgIGhlaWdodDogNDJweDtcXG4gICAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgfVxcblxcbiAgLnRhYmxlQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogNDBweCAwO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAudGFibGVDb250YWluZXIgaDEge1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIG1hcmdpbjogMCAxNnB4IDMycHggMTZweDtcXG4gICAgfVxcblxcbiAgICAudGFibGVDb250YWluZXIgLnRhYmxlIHtcXG4gICAgICB3aWR0aDogMzI4cHg7XFxuICAgICAgZGlzcGxheTogZ3JpZDtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiB7XFxuICAgICAgICBwYWRkaW5nOiAxNnB4IDIwcHggMTZweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigwLCAwLCAwLCAwLjEpO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIge1xcbiAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmNvbXBhcmlzaW9uVGV4dCB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmJveCB7XFxuICAgICAgICAgICAgd2lkdGg6IDE2cHg7XFxuICAgICAgICAgICAgaGVpZ2h0OiAxNnB4O1xcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmJveC5wb3NpdGl2ZSB7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NmYztcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuY29tcGFyaXNpb25XcmFwcGVyIC5ib3gubmVnYXRpdmUge1xcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmQ2ZDY7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC50YWJsZURhdGEge1xcbiAgICAgICAgICB3aWR0aDogOTIlO1xcbiAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAudGFibGVEYXRhIHNwYW4ge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC50YWJsZURhdGEgc3BhbjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuZG90V3JhcHBlciB7XFxuICAgICAgICAgIHdpZHRoOiA4JTtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmRvdFdyYXBwZXIgLmRvdCB7XFxuICAgICAgICAgICAgd2lkdGg6IDhweDtcXG4gICAgICAgICAgICBoZWlnaHQ6IDhweDtcXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI1MjgyYjtcXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmRvdFdyYXBwZXIgc3BhbjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDEpIHtcXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XFxuICAgICAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgyKSB7XFxuICAgICAgICBncmlkLXJvdzogMS8yO1xcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAwO1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMykge1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDEycHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoNCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDIvMztcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxMnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDYpIHtcXG4gICAgICAgIGdyaWQtcm93OiAzLzQ7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCg4KSB7XFxuICAgICAgICBncmlkLXJvdzogNC81O1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMTApIHtcXG4gICAgICAgIGdyaWQtcm93OiA1LzY7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgxMikge1xcbiAgICAgICAgZ3JpZC1yb3c6IDYvNztcXG4gICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDEzKSB7XFxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMTQpIHtcXG4gICAgICAgIGdyaWQtcm93OiA3Lzg7XFxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICAgIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImJyZWFkY3J1bVwiOiBcIlRlYWNoLWJyZWFkY3J1bS0xMXhYNVwiLFxuXHRcInRvZ2dsZV9vdXRlclwiOiBcIlRlYWNoLXRvZ2dsZV9vdXRlci1WSXNWRVwiLFxuXHRcInRvZ2dsZV9vblwiOiBcIlRlYWNoLXRvZ2dsZV9vbi0ySmJKZFwiLFxuXHRcInRvZ2dsZV9pbm5lclwiOiBcIlRlYWNoLXRvZ2dsZV9pbm5lci0zZ1VydVwiLFxuXHRcImhlYWRlcmJhY2tncm91bmRtb2JpbGVcIjogXCJUZWFjaC1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTJaMFFZXCIsXG5cdFwiYXV0aG9yaW1nYm94XCI6IFwiVGVhY2gtYXV0aG9yaW1nYm94LTI4LTJyXCIsXG5cdFwiY29udGVudFBhcnRcIjogXCJUZWFjaC1jb250ZW50UGFydC0xbFNnS1wiLFxuXHRcInRvcGljSW1hZ2VcIjogXCJUZWFjaC10b3BpY0ltYWdlLTFDTTdqXCIsXG5cdFwiY29udGVudFwiOiBcIlRlYWNoLWNvbnRlbnQtc196Q2RcIixcblx0XCJzZWN0aW9uX3RpdGxlXCI6IFwiVGVhY2gtc2VjdGlvbl90aXRsZS0ybnlCZFwiLFxuXHRcInRleHRjb250ZW50XCI6IFwiVGVhY2gtdGV4dGNvbnRlbnQtM3M1RHpcIixcblx0XCJjdXN0b21lcnNfY29udGFpbmVyXCI6IFwiVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OFwiLFxuXHRcImN1c3RvbWVyX3Jldmlld1wiOiBcIlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUlwiLFxuXHRcImN1c3RvbWVyTG9nb1wiOiBcIlRlYWNoLWN1c3RvbWVyTG9nby14RkFxOVwiLFxuXHRcInNyaUNoYWl0YW55YVRleHRcIjogXCJUZWFjaC1zcmlDaGFpdGFueWFUZXh0LTFIUnQyXCIsXG5cdFwiYXV0aG9yV3JhcHBlclwiOiBcIlRlYWNoLWF1dGhvcldyYXBwZXItMkZUdTdcIixcblx0XCJhdXRob3JfdGl0bGVcIjogXCJUZWFjaC1hdXRob3JfdGl0bGUtM0hOQ3lcIixcblx0XCJhYm91dF9hdXRob3JcIjogXCJUZWFjaC1hYm91dF9hdXRob3ItM3NuNWhcIixcblx0XCJhbGxjdXN0b21lcnNcIjogXCJUZWFjaC1hbGxjdXN0b21lcnMtMmI4Tk9cIixcblx0XCJhdmFpbGFibGVDb250YWluZXJcIjogXCJUZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVBcIixcblx0XCJhdmFpbGFibGVSb3dcIjogXCJUZWFjaC1hdmFpbGFibGVSb3ctM1NlRVFcIixcblx0XCJkZXNrdG9wSW1hZ2VcIjogXCJUZWFjaC1kZXNrdG9wSW1hZ2UtbjltY3NcIixcblx0XCJhdmFpbGFibGVUaXRsZVwiOiBcIlRlYWNoLWF2YWlsYWJsZVRpdGxlLTI4TWlNXCIsXG5cdFwiYXZhaWxhYmxlXCI6IFwiVGVhY2gtYXZhaWxhYmxlLWs3QW5pXCIsXG5cdFwiYXZhaWxhYmxlQ29udGVudFNlY3Rpb25cIjogXCJUZWFjaC1hdmFpbGFibGVDb250ZW50U2VjdGlvbi1KWmxhRlwiLFxuXHRcInBsYXRmb3JtQ29udGFpbmVyXCI6IFwiVGVhY2gtcGxhdGZvcm1Db250YWluZXItMUlBTmNcIixcblx0XCJwbGF0Zm9ybVwiOiBcIlRlYWNoLXBsYXRmb3JtLTFHY0paXCIsXG5cdFwicGxhdGZvcm1Pc1wiOiBcIlRlYWNoLXBsYXRmb3JtT3MtMkRGNkVcIixcblx0XCJyb3dcIjogXCJUZWFjaC1yb3ctMVZTeXJcIixcblx0XCJzdG9yZVwiOiBcIlRlYWNoLXN0b3JlLUlaY0RRXCIsXG5cdFwicGxheXN0b3JlXCI6IFwiVGVhY2gtcGxheXN0b3JlLVBYZmhHXCIsXG5cdFwiYXBwc3RvcmVcIjogXCJUZWFjaC1hcHBzdG9yZS0ySE1QZlwiLFxuXHRcImRpc3BsYXlDbGllbnRzXCI6IFwiVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1FcIixcblx0XCJpbWFnZVBhcnRcIjogXCJUZWFjaC1pbWFnZVBhcnQtM05BaTBcIixcblx0XCJlbXB0eUNhcmRcIjogXCJUZWFjaC1lbXB0eUNhcmQtdi1HT19cIixcblx0XCJ0b3BDYXJkXCI6IFwiVGVhY2gtdG9wQ2FyZC0yWnVENFwiLFxuXHRcInRlYWNoQ29udGFpbmVyXCI6IFwiVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUJcIixcblx0XCJoZWFkaW5nXCI6IFwiVGVhY2gtaGVhZGluZy0xSzVhc1wiLFxuXHRcImxlYXJuaW5nVGV4dFwiOiBcIlRlYWNoLWxlYXJuaW5nVGV4dC0zYVY3TlwiLFxuXHRcImJ1dHRvbndyYXBwZXJcIjogXCJUZWFjaC1idXR0b253cmFwcGVyLWlfamRpXCIsXG5cdFwicmVxdWVzdERlbW9cIjogXCJUZWFjaC1yZXF1ZXN0RGVtby1BYm9IQ1wiLFxuXHRcIndoYXRzYXBwd3JhcHBlclwiOiBcIlRlYWNoLXdoYXRzYXBwd3JhcHBlci0zanB6NFwiLFxuXHRcIndoYXRzYXBwXCI6IFwiVGVhY2gtd2hhdHNhcHAtMzRzcFNcIixcblx0XCJ0b3BTZWN0aW9uXCI6IFwiVGVhY2gtdG9wU2VjdGlvbi0xWlpyLVwiLFxuXHRcInRlYWNoU2VjdGlvblwiOiBcIlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSFwiLFxuXHRcInRlYWNoSW1nQm94XCI6IFwiVGVhY2gtdGVhY2hJbWdCb3gtbWt6NjdcIixcblx0XCJ0ZWFjaFNlY3Rpb25OYW1lXCI6IFwiVGVhY2gtdGVhY2hTZWN0aW9uTmFtZS0xQWRTNFwiLFxuXHRcImNvbnRlbnRUZXh0XCI6IFwiVGVhY2gtY29udGVudFRleHQtM2VYNTZcIixcblx0XCJkb3dubG9hZEFwcFwiOiBcIlRlYWNoLWRvd25sb2FkQXBwLTN0V3J2XCIsXG5cdFwiZG93bmxvYWRUZXh0XCI6IFwiVGVhY2gtZG93bmxvYWRUZXh0LTJZT2NOXCIsXG5cdFwicGxheVN0b3JlSWNvblwiOiBcIlRlYWNoLXBsYXlTdG9yZUljb24tM1lkRDhcIixcblx0XCJjbGllbnRzV3JhcHBlclwiOiBcIlRlYWNoLWNsaWVudHNXcmFwcGVyLTNmSllxXCIsXG5cdFwiY2xpZW50XCI6IFwiVGVhY2gtY2xpZW50LUlpOUpGXCIsXG5cdFwiYWNoaWV2ZWRDb250YWluZXJcIjogXCJUZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrc1wiLFxuXHRcImFjaGlldmVkSGVhZGluZ1wiOiBcIlRlYWNoLWFjaGlldmVkSGVhZGluZy0yeXhSNFwiLFxuXHRcImFjaGlldmVkUm93XCI6IFwiVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlZcIixcblx0XCJjYXJkXCI6IFwiVGVhY2gtY2FyZC02ekwxN1wiLFxuXHRcImFjaGlldmVkUHJvZmlsZVwiOiBcIlRlYWNoLWFjaGlldmVkUHJvZmlsZS0zdzVOalwiLFxuXHRcImNsaWVudHNcIjogXCJUZWFjaC1jbGllbnRzLTFoVUZpXCIsXG5cdFwic3R1ZGVudHNcIjogXCJUZWFjaC1zdHVkZW50cy1QQUZaR1wiLFxuXHRcInRlc3RzXCI6IFwiVGVhY2gtdGVzdHMtMjdERXNcIixcblx0XCJxdWVzdGlvbnNcIjogXCJUZWFjaC1xdWVzdGlvbnMtV0JubERcIixcblx0XCJoaWdobGlnaHRcIjogXCJUZWFjaC1oaWdobGlnaHQtM2xsUlNcIixcblx0XCJxdWVzdGlvbnNIaWdobGlnaHRcIjogXCJUZWFjaC1xdWVzdGlvbnNIaWdobGlnaHQtMzdMYTlcIixcblx0XCJ0ZXN0c0hpZ2hsaWdodFwiOiBcIlRlYWNoLXRlc3RzSGlnaGxpZ2h0LTJIYjlPXCIsXG5cdFwic3R1ZGVudHNIaWdobGlnaHRcIjogXCJUZWFjaC1zdHVkZW50c0hpZ2hsaWdodC0ySDVGX1wiLFxuXHRcImNsaWVudHNIaWdobGlnaHRcIjogXCJUZWFjaC1jbGllbnRzSGlnaGxpZ2h0LTFhbmtEXCIsXG5cdFwic3ViVGV4dFwiOiBcIlRlYWNoLXN1YlRleHQtM0lSSEtcIixcblx0XCJzZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlXCI6IFwiVGVhY2gtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xeUVEWVwiLFxuXHRcInNlY3Rpb25fY29udGVudHNcIjogXCJUZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyXCIsXG5cdFwic2VjdGlvbl9jb250YWluZXJcIjogXCJUZWFjaC1zZWN0aW9uX2NvbnRhaW5lci1jeXlUTlwiLFxuXHRcInRlYWNoQ29udGVudFwiOiBcIlRlYWNoLXRlYWNoQ29udGVudC0xOVFUa1wiLFxuXHRcImdldHJhbmtzXCI6IFwiVGVhY2gtZ2V0cmFua3MtM0pjWGFcIixcblx0XCJ6b29tXCI6IFwiVGVhY2gtem9vbS0zUlh1LVwiLFxuXHRcImJvdHRvbUNpcmNsZVwiOiBcIlRlYWNoLWJvdHRvbUNpcmNsZS0xUVU4clwiLFxuXHRcInRvcENpcmNsZVwiOiBcIlRlYWNoLXRvcENpcmNsZS0yd2FIUFwiLFxuXHRcImxpdmVwYXJ0XCI6IFwiVGVhY2gtbGl2ZXBhcnQtM1RLQzhcIixcblx0XCJhc3NpZ25tZW50cGFydFwiOiBcIlRlYWNoLWFzc2lnbm1lbnRwYXJ0LTIwRGtJXCIsXG5cdFwiZG91YnRwYXJ0XCI6IFwiVGVhY2gtZG91YnRwYXJ0LTJOa3lRXCIsXG5cdFwidGFibGVDb250YWluZXJcIjogXCJUZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwclwiLFxuXHRcInRhYmxlXCI6IFwiVGVhY2gtdGFibGUteTlxdFVcIixcblx0XCJjb21wYXJpc2lvblwiOiBcIlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrXCIsXG5cdFwiY29tcGFyaXNpb25XcmFwcGVyXCI6IFwiVGVhY2gtY29tcGFyaXNpb25XcmFwcGVyLU1KMVF6XCIsXG5cdFwiY29tcGFyaXNpb25UZXh0XCI6IFwiVGVhY2gtY29tcGFyaXNpb25UZXh0LTNGbWhQXCIsXG5cdFwiYm94XCI6IFwiVGVhY2gtYm94LTI4N3NEXCIsXG5cdFwicG9zaXRpdmVcIjogXCJUZWFjaC1wb3NpdGl2ZS0yblE3bFwiLFxuXHRcIm5lZ2F0aXZlXCI6IFwiVGVhY2gtbmVnYXRpdmUtM0Q2dzJcIixcblx0XCJkb3RXcmFwcGVyXCI6IFwiVGVhY2gtZG90V3JhcHBlci1BTlFqNlwiLFxuXHRcImRvdFwiOiBcIlRlYWNoLWRvdC1mdTlDdFwiLFxuXHRcInJlZFwiOiBcIlRlYWNoLXJlZC16ZUZxbFwiLFxuXHRcInRhYmxlRGF0YVwiOiBcIlRlYWNoLXRhYmxlRGF0YS0zbm50WlwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IHsgUExBVEZPUk1TLCBIT01FX0NMSUVOVFNfVVBEQVRFRCB9IGZyb20gJy4uL0dldFJhbmtzQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vVGVhY2guc2Nzcyc7XG5cbmNsYXNzIFRlYWNoIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGxpdmVUb2dnbGU6IGZhbHNlLFxuICAgICAgbGVjdHVyZVRvZ2dsZTogZmFsc2UsXG4gICAgICBhc3NpZ25tZW50VG9nZ2xlOiBmYWxzZSxcbiAgICAgIGRvdWJ0VG9nZ2xlOiBmYWxzZSxcbiAgICB9O1xuICB9XG5cbiAgLy8gVG9nZ2xlIEJ1dHRvbiBDb2RlXG4gIGRpc3BsYXlUb2dnbGUgPSAoKSA9PiAoXG4gICAgPGRpdlxuICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICBjbGFzc05hbWU9e1xuICAgICAgICB0aGlzLnN0YXRlLmxpdmVUb2dnbGVcbiAgICAgICAgICA/IGAke3MudG9nZ2xlX291dGVyfSAke3MudG9nZ2xlX29ufWBcbiAgICAgICAgICA6IHMudG9nZ2xlX291dGVyXG4gICAgICB9XG4gICAgPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9nZ2xlX2lubmVyfSAvPlxuICAgIDwvZGl2PlxuICApO1xuICAvLyBUZWFjaCBTZWN0aW9uXG4gIGRpc3BsYXlUZWFjaFNlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hDb250YWluZXJ9PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmJyZWFkY3J1bX0+XG4gICAgICAgIEhvbWUgLyA8c3Bhbj5MaXZlIENsYXNzZXM8L3NwYW4+XG4gICAgICA8L3NwYW4+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BTZWN0aW9ufT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZWFjaEltZ0JveH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvbGl2ZV9jb2xvcmVkLnN2Z1wiIGFsdD1cImxpdmUtY2xhc3Nlc1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRlYWNoU2VjdGlvbk5hbWV9PkxpdmUgQ2xhc3Nlczwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxoMiBjbGFzc05hbWU9e3MuaGVhZGluZ30+XG4gICAgICAgIE1heGltaXplIHRoZSZuYnNwO1xuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWFybmluZ1RleHR9PlxuICAgICAgICAgIDxzcGFuPmxlYXJuaW5nJm5ic3A7PC9zcGFuPlxuICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9saXZlIGNsYXNzZXMvdW5kZXJzY29yZV9mb3JfdGl0bGUucG5nXCJcbiAgICAgICAgICAgIGFsdD1cInVuZGVyc2NvcmVcIlxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICBvZiB5b3VyIHN0dWRlbnRzXG4gICAgICA8L2gyPlxuICAgICAgPHAgY2xhc3NOYW1lPXtzLmNvbnRlbnRUZXh0fT5cbiAgICAgICAgQSBwb3dlcmZ1bCB0ZWFjaGluZyBtb2R1bGUgdGhhdCBmYWNpbGl0YXRlcyBkaXN0YW5jZSBsZWFybmluZywgcHJvbW90ZVxuICAgICAgICBwb3NpdGl2ZSBzdHVkZW50IGJlaGF2aW91ciwgZW5nYWdlIHBhcmVudHMgaW4gdGhlIGxlYXJuaW5nIHByb2Nlc3MgYW5kXG4gICAgICAgIHJlZHVjZSB0ZWFjaGVyIHdvcmtsb2FkIGFsbCBmcm9tIG9uZSBlYXN5LXRvLXVzZSBwbGF0Zm9ybS5cbiAgICAgIDwvcD5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJ1dHRvbndyYXBwZXJ9PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLnJlcXVlc3REZW1vfVxuICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuaGFuZGxlU2hvd1RyaWFsfVxuICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICA+XG4gICAgICAgICAgR0VUIFNVQlNDUklQVElPTlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGFcbiAgICAgICAgICBjbGFzc05hbWU9e3Mud2hhdHNhcHB3cmFwcGVyfVxuICAgICAgICAgIGhyZWY9XCJodHRwczovL2FwaS53aGF0c2FwcC5jb20vc2VuZD9waG9uZT05MTg4MDA3NjQ5MDkmdGV4dD1IaSBHZXRSYW5rcywgSSB3b3VsZCBsaWtlIHRvIGtub3cgbW9yZSBhYm91dCB5b3VyIE9ubGluZSBQbGF0Zm9ybS5cIlxuICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy53aGF0c2FwcH0+Q2hhdCBvbjwvZGl2PlxuICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXRzYXBwX2xvZ28uc3ZnXCIgYWx0PVwid2hhdHNhcHBcIiAvPlxuICAgICAgICA8L2E+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvd25sb2FkQXBwfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG93bmxvYWRUZXh0fT5Eb3dubG9hZCB0aGUgYXBwPC9kaXY+XG4gICAgICAgIDxhXG4gICAgICAgICAgaHJlZj1cImh0dHBzOi8vcGxheS5nb29nbGUuY29tL3N0b3JlL2FwcHMvZGV0YWlscz9pZD1jb20uZWduaWZ5LmdldHJhbmtzJmhsPWVuX0lOJmdsPVVTXCJcbiAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIlxuICAgICAgICA+XG4gICAgICAgICAgPGltZ1xuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnBsYXlTdG9yZUljb259XG4gICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXlTdG9yZS5wbmdcIlxuICAgICAgICAgICAgYWx0PVwicGxheV9zdG9yZVwiXG4gICAgICAgICAgLz5cbiAgICAgICAgPC9hPlxuICAgICAgPC9kaXY+XG4gICAgICB7LypcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vdXNlaWNvbn0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9tb3VzZV9pY29uLnN2Z1wiIGFsdD1cIm1vdXNlX2ljb25cIiAvPlxuICAgICAgPC9kaXY+ICovfVxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyYmFja2dyb3VuZG1vYmlsZX0+XG4gICAgICAgIDxpbWdcbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1RlYWNoL3RlYWNoX2hlYWRlcl9odW1hbi53ZWJwXCJcbiAgICAgICAgICBhbHQ9XCJtb2JpbGVfYmFja2dyb3VuZFwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Q2xpZW50cyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5kaXNwbGF5Q2xpZW50c30+XG4gICAgICA8aDM+XG4gICAgICAgIFRydXN0ZWQgYnkgPHNwYW4gLz5cbiAgICAgICAgbGVhZGluZyBFZHVjYXRpb25hbCBJbnN0aXR1dGlvbnNcbiAgICAgIDwvaDM+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jbGllbnRzV3JhcHBlcn0+XG4gICAgICAgIHtIT01FX0NMSUVOVFNfVVBEQVRFRC5tYXAoY2xpZW50ID0+IChcbiAgICAgICAgICA8aW1nXG4gICAgICAgICAgICBjbGFzc05hbWU9e3MuY2xpZW50fVxuICAgICAgICAgICAga2V5PXtjbGllbnQuaWR9XG4gICAgICAgICAgICBzcmM9e2NsaWVudC5pY29ufVxuICAgICAgICAgICAgYWx0PVwiY2xpbmV0XCJcbiAgICAgICAgICAvPlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPHNwYW4+XG4gICAgICAgIDxMaW5rIHRvPVwiL2N1c3RvbWVyc1wiPmNsaWNrIGhlcmUgZm9yIG1vcmUuLi48L0xpbms+XG4gICAgICA8L3NwYW4+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUFjaGlldmVkU29GYXIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRIZWFkaW5nfT5cbiAgICAgICAgV2hhdCB3ZSBoYXZlIGFjaGlldmVkIDxzcGFuIC8+XG4gICAgICAgIHNvIGZhclxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZFJvd30+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgyNTUsIDEwMiwgMCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLmNsaWVudHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL2NsaWVudHMuc3ZnXCIgYWx0PVwicHJvZmlsZVwiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLmNsaWVudHNIaWdobGlnaHR9YH0+MTUwKzwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5DbGllbnRzPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3Muc3R1ZGVudHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3N0dWRlbnRzLnN2Z1wiIGFsdD1cInN0dWRlbnRzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3Muc3R1ZGVudHNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAzIExha2grXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPlN0dWRlbnRzPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDAsIDExNSwgMjU1LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MudGVzdHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3Rlc3RzLnN2Z1wiIGFsdD1cInRlc3RzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MudGVzdHNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAzIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPlRlc3RzIEF0dGVtcHRlZDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgwLCAxNzIsIDM4LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MucXVlc3Rpb25zfWB9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9xdWVzdGlvbnMuc3ZnXCJcbiAgICAgICAgICAgICAgYWx0PVwicXVlc3Rpb25zXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnF1ZXN0aW9uc0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDIwIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zdWJUZXh0fT5RdWVzdGlvbnMgU29sdmVkPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIC8vIExpdmUgT25DbGljayBIYW5kbGVyXG4gIGxpdmVUb2dnbGVIYW5kbGVyID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBsaXZlVG9nZ2xlOiAhdGhpcy5zdGF0ZS5saXZlVG9nZ2xlIH0pO1xuICB9O1xuICBsZWN0dXJlVG9nZ2xlSGFuZGxlciA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgbGVjdHVyZVRvZ2dsZTogIXRoaXMuc3RhdGUubGVjdHVyZVRvZ2dsZSB9KTtcbiAgfTtcbiAgYXNzaWdubWVudFRvZ2dsZUhhbmRsZXIgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGFzc2lnbm1lbnRUb2dnbGU6ICF0aGlzLnN0YXRlLmFzc2lnbm1lbnRUb2dnbGUgfSk7XG4gIH07XG4gIGRvdWJ0VG9nZ2xlSGFuZGxlciA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZG91YnRUb2dnbGU6ICF0aGlzLnN0YXRlLmRvdWJ0VG9nZ2xlIH0pO1xuICB9O1xuXG4gIC8vIExpdmUgU2VjdGlvblxuICBkaXNwbGF5TGl2ZVNlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcGljSW1hZ2V9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvb2xkX0xpdmUuc3ZnXCJcbiAgICAgICAgICAgICAgYWx0PVwibGl2ZVwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PkxpdmU8L3NwYW4+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZWFjaENvbnRlbnR9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmdldHJhbmtzfVxuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvZ2V0cmFua3MgY29weS5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cImdldHJhbmtzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgJm5ic3A7KyZuYnNwO1xuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnpvb219XG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9ab29tLUxvZ28ucG5nXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJ6b29tLWxvZ29cIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAmbmJzcDtJbnRlZ3JhdGlvbiBab29tIGlzIGludGVncmF0ZWQgd2l0aCBHZXRSYW5rcy4gQWxsXG4gICAgICAgICAgICAgIEJlbmVmaXRzLUluLCBBbGwgTGltaXRhdGlvbnMtb3V0XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgTm8gbWF0dGVyIHdoZXJlIGVkdWNhdGlvbiB0YWtlcyBwbGFjZSwgJnF1b3Q7RWduaWZ5IExpdmUgd2l0aCBab29tXG4gICAgICAgICAgICAgICZxdW90OyBjYW4gaGVscCBlbmdhZ2Ugc3R1ZGVudHMsZmFjdWx0eSBhbmQgc3RhZmYgZm9yIGxlYXJuaW5nLFxuICAgICAgICAgICAgICBjb2xsYWJvcmF0aW9uIGFuZCBhZG1pbmlzdHJhdGlvbi5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MubGl2ZXBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvbGl2ZS53ZWJwXCIgYWx0PVwibGl2ZVwiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICAvLyBMZWN0dXJlIHNlY3Rpb25cbiAgZGlzcGxheUluZGVwZW5kZW5jZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PkluZGVwZW5kZW5jZTo8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBZb3VyIFpvb20gYWNjb3VudCB3aWxsIGJlIHVzZWQuIEFsbCB0aGUgZmVhdHVyZXMgYW5kIHByb3Zpc2lvbnMgb2ZcbiAgICAgICAgICAgICAgeW91ciBab29tIGFjY291bnQgc2hhbGwgYmUgYXZhaWxhYmxlIHdpdGggYW4gYWRkZWQgYWR2YW50YWdlIG9mXG4gICAgICAgICAgICAgIGxvZ2luIHJlc3RyaWN0aW9ucyBvbmx5IGZvciBzZWxlY3RlZCBzdHVkZW50cy5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLmFzc2lnbm1lbnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL2luZGVwZW5kZW5jZS5zdmdcIiBhbHQ9XCJpbmRlcGVuZGVuY2VcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG4gIC8vIERvdWJ0cyBTZWN0aW9uXG4gIGRpc3BsYXlGbGV4aWJpbGl0eSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+RmxleGliaWxpdHk6PC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgQW55IG51bWJlciBvZiBab29tIGFjY291bnRzIGNhbiBiZSB1c2VkLiBEaWZmZXJlbnQgWm9vbSBhY2NvdW50c1xuICAgICAgICAgICAgICBjYW4gYmUgbWFwcGVkIHdpdGggZGlmZmVyZW50IENsYXNzIFNlc3Npb25zIHRodXMgYWxsb3dpbmcgbXVsdGlwbGVcbiAgICAgICAgICAgICAgdGVhY2hlcnMgdG8gdXNlIHRoZWlyIHBlcnNvbmFsIGFjY291bnRzLlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuZG91YnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL2ZsZXhpYmlsaXR5LndlYnBcIiBhbHQ9XCJmbGV4aWJpbGl0eVwiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5U2VjdXJlQWNjZXNzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+U2VjdXJlIEFjY2Vzczo8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBPbmx5IHNlbGVjdGVkIHN0dWRlbnRzIHdobyBoYXZlIGFjY2VzcyB0byB0aGUgQXBwIGNhbiBub3cgYWNjZXNzXG4gICAgICAgICAgICAgIFpvb20gY2xhc3MsIG5vIG5lZWQgdG8gc2hhcmUgTWVldGluZyBJRCBldmVyeXRpbWUsIE5vIHJhbmRvbVxuICAgICAgICAgICAgICBhY2Nlc3MuXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5hc3NpZ25tZW50cGFydH1gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZWFjaC9saXZlLndlYnBcIiBhbHQ9XCJzZWN1cmVBY2Nlc3NcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheU11bHRpcGxlVXNlcyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+TXVsdGlwbGUgdXNlcyBvZiBab29tOjwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIFpvb20gZmFjaWxpdHkgYXZhaWxhYmxlIGZvciBsaXZlIENsYXNzIGFuZCBEb3VidHMgbW9kdWxlcy4gU29vblxuICAgICAgICAgICAgICBab29tIGlzIGJlaW5nIGludGVncmF0ZWQgdG8gdGhlIG9ubGluZSBFeGFtIG1vZHVsZSBmb3Igb25saW5lIEV4YW1cbiAgICAgICAgICAgICAgcHJvY3RvcmluZy5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLmRvdWJ0cGFydH1gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZWFjaC9tdWx0aXBsZV91c2VzLndlYnBcIiBhbHQ9XCJtdWx0aS11c2VzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlPcmdhbmlzZWRDb250ZW50ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+T3JnYW5pc2VkIENvbnRlbnQ6PC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgTm93IHlvdSBkb250IGhhdmUgdG8gaW52aXRlIHN0dWRlbnRzIGJ5IHNlbmRpbmcgc2VwYXJhdGUgbWVldGluZ1xuICAgICAgICAgICAgICBub3RpZmljYXRpb25zIGFuZCByZW1pbmRlcnMgdG8gcGFyZW50cy4gVGhpcyB3aWxsIGFsbG93IGFsbCB0aGVcbiAgICAgICAgICAgICAgWm9vbSBjbGFzc2VzIHRvIGJlIGxpc3RlZCBhbmQgc2VlbiBvbiB0aGUgc3R1ZGVudHMgZGFzaGJvYXJkXG4gICAgICAgICAgICAgIHNjaGVkdWxlZCBhcyBhIHBhcnQgb2YgY3VycmljdWx1bS5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLmFzc2lnbm1lbnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVhY2gvb3JnYW5pc2VkX2NvbnRlbnQud2VicFwiXG4gICAgICAgICAgICAgICAgYWx0PVwic2VjdXJlQWNjZXNzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlBbGxvd1JlcGxheSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+QWxsb3cgUmVwbGF5Ojwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIFpvb20gbWVldGluZyByZWNvcmRpbmdzIGNhbiBiZSB1cGxvYWRlZCBmb3IgbGF0ZXIgcmVwbGF5IGJ5XG4gICAgICAgICAgICAgIHN0dWRlbnRzLlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuZG91YnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL2FsbG93X3JlcGxheS53ZWJwXCIgYWx0PVwibXVsdGktdXNlc1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5QXR0ZW5kYW5jZVJlY29yZCA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PkF0dGVuZGFuY2UgUmVjb3JkOjwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIE5vdyBpdHMgcG9zc2libGUgdG8gbWFpbnRhaW4gb3JnYW5pemVkIHJlY29yZCBvZiBhdHRlbmRhbmNlIGluXG4gICAgICAgICAgICAgIFpvb20gQ2xhc3MgYWxvbmcgd2l0aCBvdGhlciBhdHRlbmRhbmNlIGFuZCBkaWdpdGFsIGxlYXJuaW5nXG4gICAgICAgICAgICAgIGFjdGl2aXR5IHJlY29yZCBvZiB0aGUgc3R1ZGVudC5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLmFzc2lnbm1lbnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL2ZsZXhpYmlsaXR5LndlYnBcIiBhbHQ9XCJzZWN1cmVBY2Nlc3NcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheURpZmZlcmVuY2UgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MudGFibGVDb250YWluZXJ9PlxuICAgICAgPGgxPlxuICAgICAgICBHZXRSYW5rcyArIFpvb20gaXNcbiAgICAgICAgPGJyIC8+IG1vcmUgcG93ZXJmdWwgdGhhbiBab29tIGFsb25lXG4gICAgICA8L2gxPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFibGV9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb25XcmFwcGVyfT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb21wYXJpc2lvblRleHR9Pk5lZ2F0aXZlIFBvaW50czwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmJveH0gJHtzLm5lZ2F0aXZlfWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb25XcmFwcGVyfT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb21wYXJpc2lvblRleHR9PlBvc2l0aXZlIFBvaW50czwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmJveH0gJHtzLnBvc2l0aXZlfWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+Wm9vbSBvbmx5PC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5HZVJhbmtzK1pvb208L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdFdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZG90fWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFibGVEYXRhfT5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBOb3RoaW5nIFBlcnNvbmFsaXplZC4gWW91IGJlY29tZSBhIHBhcnQgb2YgYSBwb3J0YWwgYW5kIHBvcHVsYXJpemVcbiAgICAgICAgICAgICAgaXQuIFlvdXIgQnJhbmQgaWRlbnRpdHkgaXMgbG9zdC5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlN0dWRlbnQgZGF0YSAmIENvbnRlbnQgaXMgc2hhcmVkIG9uIHRoZSBwb3J0YWw8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9ICR7cy5yZWR9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIFBlcnNvbmFsaXplZCBBUFAgd2l0aCBJbnN0aXR1dGUgTmFtZSBhbmQgTG9nbyBzdHJlbmd0aGVucyB5b3VyXG4gICAgICAgICAgICAgIEJyYW5kXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgUGVyc29uYWxpemVkIFN5c3RlbSB0aGF0IHlvdSBjYW4gYWxzbyBidXkgYW5kIGluc3RhbGwgb24geW91clxuICAgICAgICAgICAgICBzZXJ2ZXJcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBTZWN1cmUgQ29udGVudCBjYW4gYmUgdmlld2VkIGJ5IFVzZXJzIG9ubHkgaW5zaWRlIHRoZSBBUFBcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgRG9lcyBub3QgYWxsb3cgcmVwbGF5LiBUaGlzIHNhdmVzIGJhbmR3aWR0aCBhbmQgcmVkdWNlIGNvc3QgYnlcbiAgICAgICAgICAgICAgY29tcHJvbWlzaW5nIHRoZSBlc3NlbnRpYWwgbmVlZCBvZiBTZWxmIExlYXJuaW5nXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgSWYgaW50ZXJuZXQgaXMgbG9zdCBkdXJpbmcgc2Vzc2lvbiwgdGhhdCBtdWNoIHNlc3Npb24gaXMgbWlzc2VkLlxuICAgICAgICAgICAgICBTdHVkZW50IGlzIG5ldmVyIGFibGUgdG8gdmlldyBpdCBiYWNrLiBObyBwcm92aXNpb24gdG8gZ28gYmFja1xuICAgICAgICAgICAgICBkdXJpbmcgbGl2ZSBzZXNzaW9uXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgTm90aGluZyBpcyBzdG9yZWQgb25saW5lLiBMaXZlIHNlc3Npb24gdmlkZW8gaXMgRG93bmxvYWRhYmxlIGFuZFxuICAgICAgICAgICAgICBmcmVlbHkgZGlzdHJpYnV0YWJsZSBhcyBsaW5rIG9yIGZpbGUuIE1hbmFnZSB5b3VyIGZpbGVzIGxvY2FsbHlcbiAgICAgICAgICAgICAgYWZ0ZXIgZG93bmxvYWRpbmdcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH0gJHtzLnJlZH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgTGVjdHVyZXMgYXJlIGF1dG8gcmVjb3JkZWQgZHVyaW5nIGxpdmUgc2Vzc2lvbiBhbmQgYXZhaWxhYmxlIGZvclxuICAgICAgICAgICAgICByZXBsYXkgbXVsdGlwbGUgdGltZXNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBTdHVkZW50IHdpbGwgbm90IG1pc3MgYW55dGhpbmcgaWYgaW50ZXJuZXQgaXMgbG9zdCwgaGUgY2FuIHN0YXJ0XG4gICAgICAgICAgICAgIGFnYWluIGFuZCB3YXRjaCB3aGVyZSBoZSBsZWZ0LiBQb3NzaWJsZSB0byBnbyBiYWNrIGluIGxpdmUgbGVjdHVyZVxuICAgICAgICAgICAgICB0byByZXZpZXcgYSB0b3BpYyBhbmQgdGhlbiByZXR1cm4gdG8gbGl2ZSBtb21lbnRcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBWaWRlb3MgYXJlIHN0b3JlZCBpbiBTYWZlIEdvb2dsZSBTZXJ2ZXJzIGluIHlvdXIgZnVsbCBhY2Nlc3MsIG5vXG4gICAgICAgICAgICAgIGhhc3NsZSBvZiBtYWludGFpbmluZyBsb2NhbGx5XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+Tm8gYXR0ZW5kYW5jZSBhbmQgcGFydGljaXBhdGlvbiBSZWNvcmQ8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9ICR7cy5yZWR9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+QXR0ZW5kYW5jZSByZWNvcmQgb2YgZXZlcnkgYWN0aXZpdHkgaXMgbWFpbnRhaW5lZC48L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+VGhlcmUgaXMgY29zdCBwZXIgYnJvYWRjYXN0ZXI8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9ICR7cy5yZWR9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+TXVsdGlwbGUgdGVhY2hlcnMgY2FuIGNhc3QgdXNpbmcgc2luZ2xlIGxpY2Vuc2U8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIE5vIGNvbnRyb2wgb3ZlciB1c2VyIGlkZW50aXR5LiBVc2VycyBjYW4gZW50ZXIgd2l0aCByYW5kb20gbmFtZXNcbiAgICAgICAgICAgICAgbGlrZSDigJhyb2NreTEyM+KAmSBhbmQgY3JlYXRlIG51aXNhbmNlXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9ICR7cy5yZWR9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIFJlc3RyaWN0ZWQgTG9naW4gYW5kIEFjY2VzcyBDb250cm9sOiBVc2VyIGNhbm5vdCBzZW5kIGFueSBpbmRlY2VudFxuICAgICAgICAgICAgICBtZXNzYWdlIGJlY2F1c2UgcmVnaXN0ZXJlZCBuYW1lIGFuZCBudW1iZXIgb2Ygc2VuZGVyIGlzIHZpc2libGVcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Q3VzdG9tZXJzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmN1c3RvbWVyc19jb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJfcmV2aWV3fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJMb2dvfT5cbiAgICAgICAgICA8aW1nIHNyYz1cImltYWdlcy9jdXN0b21lcnMvRnJhbWUgMTEzNS5qcGdcIiBhbHQ9XCJcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3JpQ2hhaXRhbnlhVGV4dH0+XG4gICAgICAgICAg4oCcTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbFxuICAgICAgICAgIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXNcbiAgICAgICAgICBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyXG4gICAgICAgICAgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpc+KAnVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXV0aG9yV3JhcHBlcn0+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmF1dGhvcl90aXRsZX0+U3J1amFuIFNhZ2FyPC9zcGFuPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5hYm91dF9hdXRob3J9PlxuICAgICAgICAgICAgQWNhZGFtaWMgaGVhZCw8c3Bhbj5TcmkgQ2hhaXRhbnlhPC9zcGFuPlxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFsbGN1c3RvbWVyc30+XG4gICAgICAgICAgPHNwYW4+c2VlIGFsbCBjdXN0b21lcnM8L3NwYW4+XG4gICAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvaWNvbnMvYXJyb3dSaWdodC5wbmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXV0aG9yaW1nYm94fT5cbiAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvaWNvbnMvc3J1amFuU2FnYXIucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5QXZhaWxhYmxlT24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZVJvd30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZUNvbnRlbnRTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdmFpbGFibGVUaXRsZX0+XG4gICAgICAgICAgICBXZeKAmXJlIDxiciAvPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmF2YWlsYWJsZX0+YXZhaWxhYmxlPC9zcGFuPiBvblxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJvd30+XG4gICAgICAgICAgICB7UExBVEZPUk1TLm1hcChpdGVtID0+IChcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhdGZvcm1Db250YWluZXJ9PlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtpdGVtLmljb259IGFsdD1cIlwiIGhlaWdodD1cIjQwcHhcIiB3aWR0aD1cIjQwcHhcIiAvPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYXRmb3JtfT57aXRlbS5sYWJlbH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGF0Zm9ybU9zfT57aXRlbS5hdmFpbGFibGV9PC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3RvcmV9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXlTdG9yZS53ZWJwXCJcbiAgICAgICAgICAgICAgYWx0PVwiUGxheSBTdG9yZVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5wbGF5c3RvcmV9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL2FwcFN0b3JlLndlYnBcIlxuICAgICAgICAgICAgICBhbHQ9XCJBcHAgU3RvcmVcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuYXBwc3RvcmV9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8aW1nXG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmRlc2t0b3BJbWFnZX1cbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXRmb3Jtcy5wbmdcIlxuICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICB7dGhpcy5kaXNwbGF5VGVhY2hTZWN0aW9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlDbGllbnRzKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlBY2hpZXZlZFNvRmFyKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlMaXZlU2VjdGlvbigpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5SW5kZXBlbmRlbmNlKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlGbGV4aWJpbGl0eSgpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U2VjdXJlQWNjZXNzKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlNdWx0aXBsZVVzZXMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheU9yZ2FuaXNlZENvbnRlbnQoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUFsbG93UmVwbGF5KCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlBdHRlbmRhbmNlUmVjb3JkKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlEaWZmZXJlbmNlKCl9XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlDdXN0b21lcnMoKSAqL31cbiAgICAgICAge3RoaXMuZGlzcGxheUF2YWlsYWJsZU9uKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoVGVhY2gpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZWFjaC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1RlYWNoLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZWFjaC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0JztcbmltcG9ydCBUZWFjaCBmcm9tICcuL1RlYWNoJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnR2V0UmFua3MgYnkgRWduaWZ5OiBBc3Nlc3NtZW50ICYgQW5hbHl0aWNzIFBsYXRmb3JtJyxcbiAgICBjaHVuazogWydMaXZlIENsYXNzZXMnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQgZm9vdGVyQXNoPlxuICAgICAgICA8VGVhY2ggLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBYUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFyQkE7QUF5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBM0ZBO0FBbUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFwSEE7QUF5SEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEvS0E7QUFzTEE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQXhMQTtBQXlMQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBM0xBO0FBNExBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUE5TEE7QUErTEE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQWpNQTtBQW9NQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE1T0E7QUFvUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXZRQTtBQThRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBalNBO0FBd1NBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEzVEE7QUFrVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXJWQTtBQTRWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBblhBO0FBMFhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE1WUE7QUFtWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXRhQTtBQTZhQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBempCQTtBQW1rQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEzbEJBO0FBK2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE1bkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQTJuQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTtBQUNBO0FBMXBCQTtBQUNBO0FBMnBCQTs7Ozs7OztBQ2xxQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBU0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=