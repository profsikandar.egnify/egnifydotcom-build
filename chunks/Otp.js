require("source-map-support").install();
exports.ids = ["Otp"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/otp/Otp.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Otp-root-3_HHi {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Otp-intro-29Jt_ {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Otp-introLeftPane-3f1ds {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Otp-jeet-3ssDG {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Otp-coachingInstitutes-2hftY {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Otp-introContent-nSCzx {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Otp-introRightPane-3w8NH {\n  padding-top: 5% !important;\n}\n\n.Otp-introRightPane-3w8NH img {\n    width: 100%;\n  }\n\n.Otp-content-_6A1r {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Otp-contentImageDiv-eFW5W {\n  height: 136px;\n  width: 136px;\n}\n\n.Otp-screenImage-27Z1f {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Otp-headingContent-3qFSG {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Otp-mainContent-2ViNK {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Otp-testimonialsTitle-1OuV3 {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Otp-testimonialsTitle-1OuV3::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Otp-contentSubdiv-yvn5L {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Otp-jeeDetailsContainer-1kBqS {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.Otp-jeeDetailsContainer-1kBqS .Otp-subSection-1insS {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.Otp-jeeTitle-y-j2t {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.Otp-jeeDescription-27sHd {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.Otp-title2-164qD {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.Otp-underline-ffzz8 {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .Otp-root-3_HHi {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Otp-contentForSmall-3bhwV {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Otp-contentImageDivSmall-1OrJN {\n    width: 100%;\n  }\n\n    .Otp-contentImageDivSmall-1OrJN img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Otp-screenImageSmall-2S7YZ {\n    text-align: center;\n  }\n\n  .Otp-headingContentSmall-1TYj4 {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Otp-mainContentSmall-1pfgV {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .Otp-testimonialsTitle-1OuV3 {\n    margin-bottom: 18%;\n  }\n\n  .Otp-testimonialsTitle-1OuV3::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Otp-introLeftPane-3f1ds {\n    width: 100%;\n  }\n\n  .Otp-jeet-3ssDG {\n    text-align: center;\n  }\n\n  .Otp-coachingInstitutes-2hftY {\n    text-align: center;\n  }\n\n  .Otp-introContent-nSCzx {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Otp-link-2uVcd {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Otp-title2-164qD {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Otp-underline-ffzz8 {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .Otp-jeeDetailsContainer-1kBqS {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .Otp-jeeDetailsContainer-1kBqS .Otp-subSection-1insS {\n      width: 100%;\n    }\n\n  .Otp-jeeTitle-y-j2t {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Otp-title2-164qD {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Otp-underline-ffzz8 {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .Otp-introContent-nSCzx {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Otp-introContent-nSCzx {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .Otp-jeeTitle-y-j2t {\n    font-size: 30px;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    font-size: 18px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/Otp.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,6BAA6B;EAC7B,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,0BAA0B;CAC3B;;AAED;IACI,WAAW;IACX,4BAA4B;IAC5B,aAAa;IACb,qBAAqB;GACtB;;AAEH;EACE,gBAAgB;EAChB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,UAAU;EACV,0BAA0B;CAC3B;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,aAAa;IACb,cAAc;GACf;;IAEC;MACE,YAAY;KACb;;EAEH;IACE,8BAA8B;IAC9B,gBAAgB;GACjB;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"Otp.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.jeeDetailsContainer {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.jeeDetailsContainer .subSection {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.jeeTitle {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.jeeDescription {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.title2 {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.underline {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .jeeDetailsContainer {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .jeeDetailsContainer .subSection {\n      width: 100%;\n    }\n\n  .jeeTitle {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .introContent {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .jeeTitle {\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    font-size: 18px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Otp-root-3_HHi",
	"intro": "Otp-intro-29Jt_",
	"introLeftPane": "Otp-introLeftPane-3f1ds",
	"jeet": "Otp-jeet-3ssDG",
	"coachingInstitutes": "Otp-coachingInstitutes-2hftY",
	"introContent": "Otp-introContent-nSCzx",
	"introRightPane": "Otp-introRightPane-3w8NH",
	"content": "Otp-content-_6A1r",
	"contentImageDiv": "Otp-contentImageDiv-eFW5W",
	"screenImage": "Otp-screenImage-27Z1f",
	"headingContent": "Otp-headingContent-3qFSG",
	"mainContent": "Otp-mainContent-2ViNK",
	"testimonialsTitle": "Otp-testimonialsTitle-1OuV3",
	"contentSubdiv": "Otp-contentSubdiv-yvn5L",
	"jeeDetailsContainer": "Otp-jeeDetailsContainer-1kBqS",
	"subSection": "Otp-subSection-1insS",
	"jeeTitle": "Otp-jeeTitle-y-j2t",
	"jeeDescription": "Otp-jeeDescription-27sHd",
	"title2": "Otp-title2-164qD",
	"underline": "Otp-underline-ffzz8",
	"contentForSmall": "Otp-contentForSmall-3bhwV",
	"contentImageDivSmall": "Otp-contentImageDivSmall-1OrJN",
	"screenImageSmall": "Otp-screenImageSmall-2S7YZ",
	"headingContentSmall": "Otp-headingContentSmall-1TYj4",
	"mainContentSmall": "Otp-mainContentSmall-1pfgV",
	"link": "Otp-link-2uVcd"
};

/***/ }),

/***/ "./src/routes/products/otp/Otp.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Otp_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/otp/Otp.scss");
/* harmony import */ var _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Otp_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/Otp.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







const STATS = [{
  title: '80k+',
  description: 'Students attempted jee mains in 2018'
}, {
  title: '17.5%',
  description: 'cut-off clear rate'
}, {
  title: '13k',
  description: 'Students qualified jee mains in 2018'
}, {
  title: '40%',
  description: 'Students of ap and telangana'
}];
const CONTENT = [{
  headerContent: 'Flexible Assessments',
  mainContent: 'Simulating the JEE experience, one can conduct an assessment in online and offline modes simultaneously. Provide students with the best in online assessment experience and do away with the hassle of maintaining expensive hardware.',
  icon: '/images/products/otp/icon-1.svg',
  screenImage: '/images/products/otp/screen1.png'
}, {
  headerContent: 'Own Question Papers',
  mainContent: 'The quality of assessments itself is the most important criteria to understand the true potential of Students. As such we provide you with the capability to upload your own question paper to maintain optimum quality standards throughout your assessments.',
  icon: '/images/products/otp/icon-2.svg',
  screenImage: '/images/products/otp/screen2.png'
}, {
  headerContent: 'Student profile',
  mainContent: 'Students get access to their test analysis, across marks, errors, concepts and behavior, and customized tests for practice and learning material. A one-stop-shop to help your students prepare and perform better.',
  icon: '/images/products/otp/icon-3.svg',
  screenImage: '/images/products/otp/screen3.png'
}, {
  headerContent: 'Mark Analysis',
  mainContent: 'Marks Analysis helps you understand how your students are performing in a snapshot and identify who needs help to clear the cut-off.',
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/jeet/screen1.png'
}, {
  headerContent: 'Error Analysis',
  mainContent: 'Error Analysis helps you determine the questions in a test which majority of your students have not attempted or attempted wrongly to enable error practice.',
  icon: '/images/products/jeet/idle.svg',
  screenImage: '/images/products/jeet/screen6.png'
}, {
  headerContent: 'Personalised Learning/Teaching Plans',
  mainContent: 'No two students are same and each of them has a different path to success. Our personalized learning plans target individual students’ weak areas and help them score better, every time. Our Teaching Plans enables you to teach the topics that matter more to your students and help create a better learning experience. ',
  icon: '/images/products/otp/icon-6.svg',
  screenImage: '/images/products/otp/screen6.png'
}];
const SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class.'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class.'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

class Otp extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "makeAlignment", (contentIndex, key) => {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });
  }

  // static propTypes = {
  //
  // };
  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_4___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_4___default.a.pageview(window.location.href);
  }
  /**
   * @description Returns what alignment should the content take eg. flex-start flex end center
   * @param contentIndex,key
   * @author Sushruth
   * */


  render() {
    const view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane} col m4 s12`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }, "PREP"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, "For Online Assessments"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, "Empowers Institutes to create, schedule and conduct online assessments and access in-depth competence improve student exam readiness."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/request-demo",
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 132
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane} col m8 s12`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 136
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/products/otp/otp-hero-2x.png",
      alt: "",
      style: {
        width: '90%'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDetailsContainer} row`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.title2}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }, "Our Stats", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.underline}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    })), STATS.map((details, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subSection}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeTitle}`,
      style: {
        borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    }, details.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDescription}`,
      style: {
        borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162
      },
      __self: this
    }, details.description.toUpperCase())))), CONTENT.map((content, contentIndex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 177
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content} row hide-on-xs`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 178
      },
      __self: this
    }, [0, 1].map(key => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: contentIndex % 2 === key ? `col m5 l5 ${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}` : `col m7 l7 ${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv}`,
      style: {
        justifyContent: this.makeAlignment(contentIndex, key)
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 180
      },
      __self: this
    }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 191
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 192
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 193
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 195
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 198
      },
      __self: this
    }, content.mainContent)) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 201
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      width: "87.6%",
      height: "54.45%",
      style: {
        width: contentIndex === 5 ? '70%' : ''
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 202
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall} row hide-on-m-and-up`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 216
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 218
      },
      __self: this
    }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221
      },
      __self: this
    }, content.mainContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 222
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      alt: "",
      src: content.screenImage,
      width: "87.6%",
      height: "54.45%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 223
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        paddingTop: '64px',
        marginBottom: '5%'
      },
      className: "row hide",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 233
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 237
      },
      __self: this
    }, "Why our clients love Egnify"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: SLIDESHOW,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 238
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    }));
    return view;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Otp));

/***/ }),

/***/ "./src/routes/products/otp/Otp.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/otp/Otp.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/otp/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Otp__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/otp/Otp.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/index.js";




async function action() {
  return {
    title: 'Egnify',
    chunks: ['Otp'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Otp__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL090cC5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvb3RwL090cC5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvb3RwL090cC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL290cC9PdHAuc2Nzcz80ZTQwIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvb3RwL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuT3RwLXJvb3QtM19ISGkge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYzRjNmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbn1cXG5cXG4uT3RwLWludHJvLTI5SnRfIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAzJTtcXG4gIG1hcmdpbi1ib3R0b206IDUlO1xcbn1cXG5cXG4uT3RwLWludHJvTGVmdFBhbmUtM2YxZHMge1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLk90cC1qZWV0LTNzc0RHIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5PdHAtY29hY2hpbmdJbnN0aXR1dGVzLTJoZnRZIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjhweDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5PdHAtaW50cm9Db250ZW50LW5TQ3p4IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcXG4gIHdpZHRoOiA0MjBweDtcXG59XFxuXFxuLk90cC1pbnRyb1JpZ2h0UGFuZS0zdzhOSCB7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG59XFxuXFxuLk90cC1pbnRyb1JpZ2h0UGFuZS0zdzhOSCBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4uT3RwLWNvbnRlbnQtXzZBMXIge1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxufVxcblxcbi5PdHAtY29udGVudEltYWdlRGl2LWVGVzVXIHtcXG4gIGhlaWdodDogMTM2cHg7XFxuICB3aWR0aDogMTM2cHg7XFxufVxcblxcbi5PdHAtc2NyZWVuSW1hZ2UtMjdaMWYge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDY4cHg7XFxufVxcblxcbi5PdHAtaGVhZGluZ0NvbnRlbnQtM3FGU0cge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5PdHAtbWFpbkNvbnRlbnQtMlZpTksge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS42O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5PdHAtdGVzdGltb25pYWxzVGl0bGUtMU91VjMge1xcbiAgaGVpZ2h0OiAzOHB4O1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxuICBwYWRkaW5nOiAyJTtcXG59XFxuXFxuLk90cC10ZXN0aW1vbmlhbHNUaXRsZS0xT3VWMzo6YWZ0ZXIge1xcbiAgY29udGVudDogJyc7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgd2lkdGg6IDQ4cHg7XFxuICBoZWlnaHQ6IDJweDtcXG4gIGJvcmRlci10b3A6IHNvbGlkIDNweCAjZWM0YzZmO1xcbiAgcGFkZGluZy10b3A6IDEycHg7XFxufVxcblxcbi5PdHAtY29udGVudFN1YmRpdi15dm41TCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5PdHAtamVlRGV0YWlsc0NvbnRhaW5lci0xa0JxUyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiAzMDBweCAhaW1wb3J0YW50O1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1ib3R0b206IDUlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNlM2U1ZjtcXG59XFxuXFxuLk90cC1qZWVEZXRhaWxzQ29udGFpbmVyLTFrQnFTIC5PdHAtc3ViU2VjdGlvbi0xaW5zUyB7XFxuICAgIHdpZHRoOiAyNSU7XFxuICAgIG1pbi1oZWlnaHQ6IDgwcHggIWltcG9ydGFudDtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcXG4gIH1cXG5cXG4uT3RwLWplZVRpdGxlLXktajJ0IHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZhYjAwO1xcbn1cXG5cXG4uT3RwLWplZURlc2NyaXB0aW9uLTI3c0hkIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA1JTtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgaGVpZ2h0OiA3MHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi5PdHAtdGl0bGUyLTE2NHFEIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG5cXG4uT3RwLXVuZGVybGluZS1mZnp6OCB7XFxuICBtYXJnaW4tdG9wOiA4cHg7XFxuICBtYXJnaW4tbGVmdDogNDglO1xcbiAgd2lkdGg6IDQlO1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2VjNGM2ZjtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NjBweCkge1xcbiAgLk90cC1yb290LTNfSEhpIHtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLk90cC1jb250ZW50Rm9yU21hbGwtM2Jod1Yge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuT3RwLWNvbnRlbnRJbWFnZURpdlNtYWxsLTFPckpOIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAgIC5PdHAtY29udGVudEltYWdlRGl2U21hbGwtMU9ySk4gaW1nIHtcXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xcbiAgICAgIHdpZHRoOiAxMDBweDtcXG4gICAgfVxcblxcbiAgLk90cC1zY3JlZW5JbWFnZVNtYWxsLTJTN1laIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLk90cC1oZWFkaW5nQ29udGVudFNtYWxsLTFUWWo0IHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAuT3RwLW1haW5Db250ZW50U21hbGwtMXBmZ1Yge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC5PdHAtdGVzdGltb25pYWxzVGl0bGUtMU91VjMge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxOCU7XFxuICB9XFxuXFxuICAuT3RwLXRlc3RpbW9uaWFsc1RpdGxlLTFPdVYzOjphZnRlciB7XFxuICAgIG1hcmdpbi10b3A6IDRweDtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XFxuICAuT3RwLWludHJvTGVmdFBhbmUtM2YxZHMge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5PdHAtamVldC0zc3NERyB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5PdHAtY29hY2hpbmdJbnN0aXR1dGVzLTJoZnRZIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLk90cC1pbnRyb0NvbnRlbnQtblNDengge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgfVxcblxcbiAgLk90cC1saW5rLTJ1VmNkIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLk90cC1qZWVEZXNjcmlwdGlvbi0yN3NIZCB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xcbiAgICBwYWRkaW5nLWxlZnQ6IDIwJTtcXG4gICAgcGFkZGluZy1yaWdodDogMjAlO1xcbiAgfVxcblxcbiAgLk90cC10aXRsZTItMTY0cUQge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC5PdHAtdW5kZXJsaW5lLWZmeno4IHtcXG4gICAgbWFyZ2luLWxlZnQ6IDQzJTtcXG4gICAgd2lkdGg6IDE1JTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcXG4gIC5PdHAtamVlRGV0YWlsc0NvbnRhaW5lci0xa0JxUyB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogMDtcXG4gIH1cXG5cXG4gICAgLk90cC1qZWVEZXRhaWxzQ29udGFpbmVyLTFrQnFTIC5PdHAtc3ViU2VjdGlvbi0xaW5zUyB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgIH1cXG5cXG4gIC5PdHAtamVlVGl0bGUteS1qMnQge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgZm9udC1zaXplOiAzMHB4O1xcbiAgfVxcblxcbiAgLk90cC1qZWVEZXNjcmlwdGlvbi0yN3NIZCB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xcbiAgICBwYWRkaW5nLWxlZnQ6IDIwJTtcXG4gICAgcGFkZGluZy1yaWdodDogMjAlO1xcbiAgfVxcblxcbiAgLk90cC10aXRsZTItMTY0cUQge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC5PdHAtdW5kZXJsaW5lLWZmeno4IHtcXG4gICAgbWFyZ2luLWxlZnQ6IDQzJTtcXG4gICAgd2lkdGg6IDE1JTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAoZGV2aWNlLXdpZHRoOiAxMDI0cHgpIHtcXG4gIC5PdHAtaW50cm9Db250ZW50LW5TQ3p4IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcXG4gIC5PdHAtaW50cm9Db250ZW50LW5TQ3p4IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcblxcbiAgLk90cC1qZWVUaXRsZS15LWoydCB7XFxuICAgIGZvbnQtc2l6ZTogMzBweDtcXG4gIH1cXG5cXG4gIC5PdHAtamVlRGVzY3JpcHRpb24tMjdzSGQge1xcbiAgICBmb250LXNpemU6IDE4cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9vdHAvT3RwLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWiw0REFBNEQ7VUFDcEQsb0RBQW9EO0NBQzdEOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZUFBZTtFQUNmLDJCQUEyQjtFQUMzQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7SUFDSSxZQUFZO0dBQ2I7O0FBRUg7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixlQUFlO0VBQ2YsWUFBWTtFQUNaLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYiw2QkFBNkI7RUFDN0Isa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsMEJBQTBCO0NBQzNCOztBQUVEO0lBQ0ksV0FBVztJQUNYLDRCQUE0QjtJQUM1QixhQUFhO0lBQ2IscUJBQXFCO0dBQ3RCOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztJQUNYLFVBQVU7R0FDWDs7RUFFRDtJQUNFLGFBQWE7SUFDYixZQUFZO0lBQ1osbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztJQUVDO01BQ0UsY0FBYztNQUNkLGFBQWE7S0FDZDs7RUFFSDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3Qjs7RUFFRDtJQUNFLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLGlCQUFpQjtJQUNqQixXQUFXO0dBQ1o7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsYUFBYTtJQUNiLGNBQWM7R0FDZjs7SUFFQztNQUNFLFlBQVk7S0FDYjs7RUFFSDtJQUNFLDhCQUE4QjtJQUM5QixnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxpQkFBaUI7SUFDakIsV0FBVztHQUNaO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7R0FDYjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRlwiLFwiZmlsZVwiOlwiT3RwLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnJvb3Qge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYzRjNmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbn1cXG5cXG4uaW50cm8ge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xcbiAgbWFyZ2luLWJvdHRvbTogNSU7XFxufVxcblxcbi5pbnRyb0xlZnRQYW5lIHtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgcGFkZGluZy10b3A6IDUlICFpbXBvcnRhbnQ7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5qZWV0IHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5jb2FjaGluZ0luc3RpdHV0ZXMge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmludHJvQ29udGVudCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICB3aWR0aDogNDIwcHg7XFxufVxcblxcbi5pbnRyb1JpZ2h0UGFuZSB7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG59XFxuXFxuLmludHJvUmlnaHRQYW5lIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbi5jb250ZW50IHtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbn1cXG5cXG4uY29udGVudEltYWdlRGl2IHtcXG4gIGhlaWdodDogMTM2cHg7XFxuICB3aWR0aDogMTM2cHg7XFxufVxcblxcbi5zY3JlZW5JbWFnZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNjhweDtcXG59XFxuXFxuLmhlYWRpbmdDb250ZW50IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4ubWFpbkNvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS42O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICBoZWlnaHQ6IDM4cHg7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG4gIHBhZGRpbmc6IDIlO1xcbn1cXG5cXG4udGVzdGltb25pYWxzVGl0bGU6OmFmdGVyIHtcXG4gIGNvbnRlbnQ6ICcnO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiAycHg7XFxuICBib3JkZXItdG9wOiBzb2xpZCAzcHggI2VjNGM2ZjtcXG4gIHBhZGRpbmctdG9wOiAxMnB4O1xcbn1cXG5cXG4uY29udGVudFN1YmRpdiB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5qZWVEZXRhaWxzQ29udGFpbmVyIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDMwMHB4ICFpbXBvcnRhbnQ7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLWJvdHRvbTogNSU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2UzZTVmO1xcbn1cXG5cXG4uamVlRGV0YWlsc0NvbnRhaW5lciAuc3ViU2VjdGlvbiB7XFxuICAgIHdpZHRoOiAyNSU7XFxuICAgIG1pbi1oZWlnaHQ6IDgwcHggIWltcG9ydGFudDtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcXG4gIH1cXG5cXG4uamVlVGl0bGUge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICNmZmFiMDA7XFxufVxcblxcbi5qZWVEZXNjcmlwdGlvbiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZzogNSU7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGhlaWdodDogNzBweDtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4udGl0bGUyIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDMlO1xcbn1cXG5cXG4udW5kZXJsaW5lIHtcXG4gIG1hcmdpbi10b3A6IDhweDtcXG4gIG1hcmdpbi1sZWZ0OiA0OCU7XFxuICB3aWR0aDogNCU7XFxuICBib3JkZXI6IHNvbGlkIDFweCAjZWM0YzZmO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk2MHB4KSB7XFxuICAucm9vdCB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5jb250ZW50Rm9yU21hbGwge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDYlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuY29udGVudEltYWdlRGl2U21hbGwge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gICAgLmNvbnRlbnRJbWFnZURpdlNtYWxsIGltZyB7XFxuICAgICAgaGVpZ2h0OiAxMDBweDtcXG4gICAgICB3aWR0aDogMTAwcHg7XFxuICAgIH1cXG5cXG4gIC5zY3JlZW5JbWFnZVNtYWxsIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmhlYWRpbmdDb250ZW50U21hbGwge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC5tYWluQ29udGVudFNtYWxsIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS42O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAudGVzdGltb25pYWxzVGl0bGUge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxOCU7XFxuICB9XFxuXFxuICAudGVzdGltb25pYWxzVGl0bGU6OmFmdGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNHB4O1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5pbnRyb0xlZnRQYW5lIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuamVldCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jb2FjaGluZ0luc3RpdHV0ZXMge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmctbGVmdDogMTUlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gIH1cXG5cXG4gIC5saW5rIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLmplZURlc2NyaXB0aW9uIHtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcXG4gICAgZm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XFxuICAgIHBhZGRpbmctbGVmdDogMjAlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XFxuICB9XFxuXFxuICAudGl0bGUyIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XFxuICB9XFxuXFxuICAudW5kZXJsaW5lIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDQzJTtcXG4gICAgd2lkdGg6IDE1JTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcXG4gIC5qZWVEZXRhaWxzQ29udGFpbmVyIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfVxcblxcbiAgICAuamVlRGV0YWlsc0NvbnRhaW5lciAuc3ViU2VjdGlvbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgIH1cXG5cXG4gIC5qZWVUaXRsZSB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuamVlRGVzY3JpcHRpb24ge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDtcXG4gICAgcGFkZGluZy1sZWZ0OiAyMCU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcXG4gIH1cXG5cXG4gIC50aXRsZTIge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC51bmRlcmxpbmUge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDEwMjRweCkge1xcbiAgLmludHJvQ29udGVudCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcblxcbiAgLmplZVRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMHB4O1xcbiAgfVxcblxcbiAgLmplZURlc2NyaXB0aW9uIHtcXG4gICAgZm9udC1zaXplOiAxOHB4O1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicm9vdFwiOiBcIk90cC1yb290LTNfSEhpXCIsXG5cdFwiaW50cm9cIjogXCJPdHAtaW50cm8tMjlKdF9cIixcblx0XCJpbnRyb0xlZnRQYW5lXCI6IFwiT3RwLWludHJvTGVmdFBhbmUtM2YxZHNcIixcblx0XCJqZWV0XCI6IFwiT3RwLWplZXQtM3NzREdcIixcblx0XCJjb2FjaGluZ0luc3RpdHV0ZXNcIjogXCJPdHAtY29hY2hpbmdJbnN0aXR1dGVzLTJoZnRZXCIsXG5cdFwiaW50cm9Db250ZW50XCI6IFwiT3RwLWludHJvQ29udGVudC1uU0N6eFwiLFxuXHRcImludHJvUmlnaHRQYW5lXCI6IFwiT3RwLWludHJvUmlnaHRQYW5lLTN3OE5IXCIsXG5cdFwiY29udGVudFwiOiBcIk90cC1jb250ZW50LV82QTFyXCIsXG5cdFwiY29udGVudEltYWdlRGl2XCI6IFwiT3RwLWNvbnRlbnRJbWFnZURpdi1lRlc1V1wiLFxuXHRcInNjcmVlbkltYWdlXCI6IFwiT3RwLXNjcmVlbkltYWdlLTI3WjFmXCIsXG5cdFwiaGVhZGluZ0NvbnRlbnRcIjogXCJPdHAtaGVhZGluZ0NvbnRlbnQtM3FGU0dcIixcblx0XCJtYWluQ29udGVudFwiOiBcIk90cC1tYWluQ29udGVudC0yVmlOS1wiLFxuXHRcInRlc3RpbW9uaWFsc1RpdGxlXCI6IFwiT3RwLXRlc3RpbW9uaWFsc1RpdGxlLTFPdVYzXCIsXG5cdFwiY29udGVudFN1YmRpdlwiOiBcIk90cC1jb250ZW50U3ViZGl2LXl2bjVMXCIsXG5cdFwiamVlRGV0YWlsc0NvbnRhaW5lclwiOiBcIk90cC1qZWVEZXRhaWxzQ29udGFpbmVyLTFrQnFTXCIsXG5cdFwic3ViU2VjdGlvblwiOiBcIk90cC1zdWJTZWN0aW9uLTFpbnNTXCIsXG5cdFwiamVlVGl0bGVcIjogXCJPdHAtamVlVGl0bGUteS1qMnRcIixcblx0XCJqZWVEZXNjcmlwdGlvblwiOiBcIk90cC1qZWVEZXNjcmlwdGlvbi0yN3NIZFwiLFxuXHRcInRpdGxlMlwiOiBcIk90cC10aXRsZTItMTY0cURcIixcblx0XCJ1bmRlcmxpbmVcIjogXCJPdHAtdW5kZXJsaW5lLWZmeno4XCIsXG5cdFwiY29udGVudEZvclNtYWxsXCI6IFwiT3RwLWNvbnRlbnRGb3JTbWFsbC0zYmh3VlwiLFxuXHRcImNvbnRlbnRJbWFnZURpdlNtYWxsXCI6IFwiT3RwLWNvbnRlbnRJbWFnZURpdlNtYWxsLTFPckpOXCIsXG5cdFwic2NyZWVuSW1hZ2VTbWFsbFwiOiBcIk90cC1zY3JlZW5JbWFnZVNtYWxsLTJTN1laXCIsXG5cdFwiaGVhZGluZ0NvbnRlbnRTbWFsbFwiOiBcIk90cC1oZWFkaW5nQ29udGVudFNtYWxsLTFUWWo0XCIsXG5cdFwibWFpbkNvbnRlbnRTbWFsbFwiOiBcIk90cC1tYWluQ29udGVudFNtYWxsLTFwZmdWXCIsXG5cdFwibGlua1wiOiBcIk90cC1saW5rLTJ1VmNkXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0Jztcbi8vIGltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IFNsaWRlc2hvdyBmcm9tICdjb21wb25lbnRzL1NsaWRlc2hvdyc7XG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XG5pbXBvcnQgUmVxdWVzdERlbW8gZnJvbSAnY29tcG9uZW50cy9SZXF1ZXN0RGVtbyc7XG5pbXBvcnQgcyBmcm9tICcuL090cC5zY3NzJztcblxuY29uc3QgU1RBVFMgPSBbXG4gIHsgdGl0bGU6ICc4MGsrJywgZGVzY3JpcHRpb246ICdTdHVkZW50cyBhdHRlbXB0ZWQgamVlIG1haW5zIGluIDIwMTgnIH0sXG4gIHsgdGl0bGU6ICcxNy41JScsIGRlc2NyaXB0aW9uOiAnY3V0LW9mZiBjbGVhciByYXRlJyB9LFxuICB7IHRpdGxlOiAnMTNrJywgZGVzY3JpcHRpb246ICdTdHVkZW50cyBxdWFsaWZpZWQgamVlIG1haW5zIGluIDIwMTgnIH0sXG4gIHsgdGl0bGU6ICc0MCUnLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIG9mIGFwIGFuZCB0ZWxhbmdhbmEnIH0sXG5dO1xuXG5jb25zdCBDT05URU5UID0gW1xuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0ZsZXhpYmxlIEFzc2Vzc21lbnRzJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdTaW11bGF0aW5nIHRoZSBKRUUgZXhwZXJpZW5jZSwgb25lIGNhbiBjb25kdWN0IGFuIGFzc2Vzc21lbnQgaW4gb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIHNpbXVsdGFuZW91c2x5LiBQcm92aWRlIHN0dWRlbnRzIHdpdGggdGhlIGJlc3QgaW4gb25saW5lIGFzc2Vzc21lbnQgZXhwZXJpZW5jZSBhbmQgZG8gYXdheSB3aXRoIHRoZSBoYXNzbGUgb2YgbWFpbnRhaW5pbmcgZXhwZW5zaXZlIGhhcmR3YXJlLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvb3RwL2ljb24tMS5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvc2NyZWVuMS5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ093biBRdWVzdGlvbiBQYXBlcnMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1RoZSBxdWFsaXR5IG9mIGFzc2Vzc21lbnRzIGl0c2VsZiBpcyB0aGUgbW9zdCBpbXBvcnRhbnQgY3JpdGVyaWEgdG8gdW5kZXJzdGFuZCB0aGUgdHJ1ZSBwb3RlbnRpYWwgb2YgU3R1ZGVudHMuIEFzIHN1Y2ggd2UgcHJvdmlkZSB5b3Ugd2l0aCB0aGUgY2FwYWJpbGl0eSB0byB1cGxvYWQgeW91ciBvd24gcXVlc3Rpb24gcGFwZXIgdG8gbWFpbnRhaW4gb3B0aW11bSBxdWFsaXR5IHN0YW5kYXJkcyB0aHJvdWdob3V0IHlvdXIgYXNzZXNzbWVudHMuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvaWNvbi0yLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9zY3JlZW4yLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnU3R1ZGVudCBwcm9maWxlJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdTdHVkZW50cyBnZXQgYWNjZXNzIHRvIHRoZWlyIHRlc3QgYW5hbHlzaXMsIGFjcm9zcyBtYXJrcywgZXJyb3JzLCBjb25jZXB0cyBhbmQgYmVoYXZpb3IsIGFuZCBjdXN0b21pemVkIHRlc3RzIGZvciBwcmFjdGljZSBhbmQgbGVhcm5pbmcgbWF0ZXJpYWwuIEEgb25lLXN0b3Atc2hvcCB0byBoZWxwIHlvdXIgc3R1ZGVudHMgcHJlcGFyZSBhbmQgcGVyZm9ybSBiZXR0ZXIuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvaWNvbi0zLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9zY3JlZW4zLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnTWFyayBBbmFseXNpcycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnTWFya3MgQW5hbHlzaXMgaGVscHMgeW91IHVuZGVyc3RhbmQgaG93IHlvdXIgc3R1ZGVudHMgYXJlIHBlcmZvcm1pbmcgaW4gYSBzbmFwc2hvdCBhbmQgaWRlbnRpZnkgd2hvIG5lZWRzIGhlbHAgdG8gY2xlYXIgdGhlIGN1dC1vZmYuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3BlcmNlbnQuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW4xLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnRXJyb3IgQW5hbHlzaXMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0Vycm9yIEFuYWx5c2lzIGhlbHBzIHlvdSBkZXRlcm1pbmUgdGhlIHF1ZXN0aW9ucyBpbiBhIHRlc3Qgd2hpY2ggbWFqb3JpdHkgb2YgeW91ciBzdHVkZW50cyBoYXZlIG5vdCBhdHRlbXB0ZWQgb3IgYXR0ZW1wdGVkIHdyb25nbHkgdG8gZW5hYmxlIGVycm9yIHByYWN0aWNlLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9pZGxlLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuNi5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ1BlcnNvbmFsaXNlZCBMZWFybmluZy9UZWFjaGluZyBQbGFucycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnTm8gdHdvIHN0dWRlbnRzIGFyZSBzYW1lIGFuZCBlYWNoIG9mIHRoZW0gaGFzIGEgZGlmZmVyZW50IHBhdGggdG8gc3VjY2Vzcy4gT3VyIHBlcnNvbmFsaXplZCBsZWFybmluZyBwbGFucyB0YXJnZXQgaW5kaXZpZHVhbCBzdHVkZW50c+KAmSB3ZWFrIGFyZWFzIGFuZCBoZWxwIHRoZW0gc2NvcmUgYmV0dGVyLCBldmVyeSB0aW1lLiBPdXIgVGVhY2hpbmcgUGxhbnMgZW5hYmxlcyB5b3UgdG8gdGVhY2ggdGhlIHRvcGljcyB0aGF0IG1hdHRlciBtb3JlIHRvIHlvdXIgc3R1ZGVudHMgYW5kIGhlbHAgY3JlYXRlIGEgYmV0dGVyIGxlYXJuaW5nIGV4cGVyaWVuY2UuICcsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvb3RwL2ljb24tNi5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvc2NyZWVuNi5wbmcnLFxuICB9LFxuXTtcblxuY29uc3QgU0xJREVTSE9XID0gW1xuICB7XG4gICAgaW1hZ2U6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy90ZWxhbmdhbmEucG5nJyxcbiAgICBuYW1lOiAnTXIuIFNyaW5pdmFzIEthbm5hbicsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcy4nLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICcnLFxuICAgIG5hbWU6ICdNci4gU3Jpbml2YXMgTXVydGh5JyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzLicsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBBbmRyZXcgVGFnJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIE11c3NvbGluaScsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG5dO1xuXG5jbGFzcyBPdHAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAvLyBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAvL1xuICAvLyB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIFJlYWN0R0EuaW5pdGlhbGl6ZSh3aW5kb3cuQXBwLmdvb2dsZVRyYWNraW5nSWQsIHtcbiAgICAgIGRlYnVnOiBmYWxzZSxcbiAgICB9KTtcbiAgICBSZWFjdEdBLnBhZ2V2aWV3KHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAZGVzY3JpcHRpb24gUmV0dXJucyB3aGF0IGFsaWdubWVudCBzaG91bGQgdGhlIGNvbnRlbnQgdGFrZSBlZy4gZmxleC1zdGFydCBmbGV4IGVuZCBjZW50ZXJcbiAgICogQHBhcmFtIGNvbnRlbnRJbmRleCxrZXlcbiAgICogQGF1dGhvciBTdXNocnV0aFxuICAgKiAqL1xuICBtYWtlQWxpZ25tZW50ID0gKGNvbnRlbnRJbmRleCwga2V5KSA9PiB7XG4gICAgaWYgKGNvbnRlbnRJbmRleCAlIDIgPT09IGtleSkge1xuICAgICAgaWYgKGtleSA9PT0gMCkge1xuICAgICAgICByZXR1cm4gJ2ZsZXgtc3RhcnQnO1xuICAgICAgfVxuICAgICAgcmV0dXJuICdmbGV4LWVuZCc7XG4gICAgfVxuICAgIHJldHVybiAnY2VudGVyJztcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgdmlldyA9IChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvb3R9IHJvd2B9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb30gcm93YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW50cm9MZWZ0UGFuZX0gY29sIG00IHMxMmB9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuamVldH0+UFJFUDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29hY2hpbmdJbnN0aXR1dGVzfT5Gb3IgT25saW5lIEFzc2Vzc21lbnRzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbnRyb0NvbnRlbnR9PlxuICAgICAgICAgICAgICBFbXBvd2VycyBJbnN0aXR1dGVzIHRvIGNyZWF0ZSwgc2NoZWR1bGUgYW5kIGNvbmR1Y3Qgb25saW5lXG4gICAgICAgICAgICAgIGFzc2Vzc21lbnRzIGFuZCBhY2Nlc3MgaW4tZGVwdGggY29tcGV0ZW5jZSBpbXByb3ZlIHN0dWRlbnQgZXhhbVxuICAgICAgICAgICAgICByZWFkaW5lc3MuXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL3JlcXVlc3QtZGVtb1wiIGNsYXNzTmFtZT17cy5saW5rfT5cbiAgICAgICAgICAgICAgPGJ1dHRvbj5SRVFVRVNUIEEgREVNTzwvYnV0dG9uPlxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvUmlnaHRQYW5lfSBjb2wgbTggczEyYH0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvcHJvZHVjdHMvb3RwL290cC1oZXJvLTJ4LnBuZ1wiXG4gICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiAnOTAlJyB9fVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmplZURldGFpbHNDb250YWluZXJ9IHJvd2B9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnRpdGxlMn1gfT5cbiAgICAgICAgICAgIE91ciBTdGF0c1xuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MudW5kZXJsaW5lfWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAge1NUQVRTLm1hcCgoZGV0YWlscywgaW5kZXgpID0+IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnN1YlNlY3Rpb259YH0+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuamVlVGl0bGV9YH1cbiAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6XG4gICAgICAgICAgICAgICAgICAgIGluZGV4IDwgU1RBVFMubGVuZ3RoIC0gMVxuICAgICAgICAgICAgICAgICAgICAgID8gJ3NvbGlkIDFweCByZ2JhKDEzOSwgMTM5LCAyMjMsIDAuMyknXG4gICAgICAgICAgICAgICAgICAgICAgOiAnJyxcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2RldGFpbHMudGl0bGV9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmplZURlc2NyaXB0aW9ufWB9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlclJpZ2h0OlxuICAgICAgICAgICAgICAgICAgICBpbmRleCA8IFNUQVRTLmxlbmd0aCAtIDFcbiAgICAgICAgICAgICAgICAgICAgICA/ICdzb2xpZCAxcHggcmdiYSgxMzksIDEzOSwgMjIzLCAwLjMpJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJycsXG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtkZXRhaWxzLmRlc2NyaXB0aW9uLnRvVXBwZXJDYXNlKCl9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7Q09OVEVOVC5tYXAoKGNvbnRlbnQsIGNvbnRlbnRJbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50fSByb3cgaGlkZS1vbi14c2B9PlxuICAgICAgICAgICAgICB7WzAsIDFdLm1hcChrZXkgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRJbmRleCAlIDIgPT09IGtleVxuICAgICAgICAgICAgICAgICAgICAgID8gYGNvbCBtNSBsNSAke3MuY29udGVudFN1YmRpdn1gXG4gICAgICAgICAgICAgICAgICAgICAgOiBgY29sIG03IGw3ICR7cy5jb250ZW50U3ViZGl2fWBcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiB0aGlzLm1ha2VBbGlnbm1lbnQoY29udGVudEluZGV4LCBrZXkpLFxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICB7Y29udGVudEluZGV4ICUgMiA9PT0ga2V5ID8gKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRJbWFnZURpdn0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17Y29udGVudC5pY29ufSAvPlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmdDb250ZW50fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtjb250ZW50LmhlYWRlckNvbnRlbnR9XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbkNvbnRlbnR9Pntjb250ZW50Lm1haW5Db250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNjcmVlbkltYWdlfT5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCI4Ny42JVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI1NC40NSVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IGNvbnRlbnRJbmRleCA9PT0gNSA/ICc3MCUnIDogJycgfX1cbiAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50Rm9yU21hbGx9IHJvdyBoaWRlLW9uLW0tYW5kLXVwYH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRJbWFnZURpdlNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17Y29udGVudC5pY29ufSAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ0NvbnRlbnRTbWFsbH0+XG4gICAgICAgICAgICAgICAge2NvbnRlbnQuaGVhZGVyQ29udGVudH1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1haW5Db250ZW50U21hbGx9Pntjb250ZW50Lm1haW5Db250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zY3JlZW5JbWFnZVNtYWxsfT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfVxuICAgICAgICAgICAgICAgICAgd2lkdGg9XCI4Ny42JVwiXG4gICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI1NC40NSVcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkpfVxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3sgcGFkZGluZ1RvcDogJzY0cHgnLCBtYXJnaW5Cb3R0b206ICc1JScgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJyb3cgaGlkZVwiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0aW1vbmlhbHNUaXRsZX0+V2h5IG91ciBjbGllbnRzIGxvdmUgRWduaWZ5PC9kaXY+XG4gICAgICAgICAgPFNsaWRlc2hvdyBkYXRhPXtTTElERVNIT1d9IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8UmVxdWVzdERlbW8gLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gICAgcmV0dXJuIHZpZXc7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShPdHApO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9PdHAuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9PdHAuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL090cC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgT3RwIGZyb20gJy4vT3RwJztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnknLFxuICAgIGNodW5rczogWydPdHAnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxPdHAgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBU0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFRQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBekJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTs7Ozs7OztBQWVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBS0E7QUFDQTtBQURBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUF4SkE7QUFDQTtBQXlKQTs7Ozs7OztBQ3RQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBU0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=