require("source-map-support").install();
exports.ids = ["Press"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Modal-bodyOverlay-3fSN4 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.Modal-modal-9QpxU {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,+BAA+B;EAC/B,iBAAiB;CAClB;;AAED;EACE,eAAe;EACf,qCAAqC;EACrC,cAAc;EACd,mBAAmB;CACpB;;AAED;;;;;;;EAOE","file":"Modal.scss","sourcesContent":[".bodyOverlay {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.modal {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bodyOverlay": "Modal-bodyOverlay-3fSN4",
	"modal": "Modal-modal-9QpxU"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/press/press.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".press-root-mEOgr {\n  //margin-top: 85px;\n}\n\n.press-mediaimgbox-1_4cl {\n  width: 200px;\n  height: 96px;\n  margin-right: 24px;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.16);\n}\n\n.press-mediaimgbox-1_4cl img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    margin-left: 0;\n  }\n\n.press-videoimgbox-3Zudz {\n  width: 172px;\n  height: 126px;\n  margin-right: 24px;\n}\n\n.press-videoimgbox-3Zudz img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n  }\n\n.press-togglegroup-mKgsa {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  padding: 56px 64px 32px;\n  display: -ms-flexbox;\n  display: flex;\n}\n\nbutton {\n  border-radius: 0;\n}\n\n.press-mediabutton-1nqxd {\n  padding: 8px 16px;\n  padding: 0.5rem 1rem;\n  border: 1px solid #f36;\n  color: #f36;\n  border-radius: none;\n  background-color: #fff;\n  line-height: normal;\n}\n\n.press-mediabutton-1nqxd.press-media-38g3L {\n  border-radius: 4px 0 0 4px;\n}\n\n.press-mediabutton-1nqxd.press-video-wIJpb {\n  border-radius: 0 4px 4px 0;\n}\n\n.press-mediabutton-1nqxd.press-active-1N70- {\n  background-color: #f36;\n  color: #fff;\n}\n\n.press-mediawrapper-1v5DP {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-line-pack: start;\n      align-content: flex-start;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.press-mediacontainer-3Bwvu {\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.press-mediacardwrapper-3sFOs {\n  width: 860px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.press-mediacard-5v4_- {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  cursor: pointer;\n}\n\n.press-mediacontentbox-q_feo {\n  width: 635px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.press-mediatitle-1cnMU {\n  margin: 0;\n  font-size: 20px;\n  line-height: 30px;\n  margin-bottom: 8px;\n}\n\n.press-mediapostdate-273R1 {\n  font-size: 14px;\n  opacity: 0.6;\n  margin: 0 0 8px 0;\n  font-weight: normal;\n}\n\n.press-mediacontent-220NA {\n  font-size: 14px;\n  line-height: 21px;\n  color: #000;\n  margin-bottom: 8px;\n}\n\n.press-readmore-eVfYA {\n  font-size: 14px;\n  line-height: 21px;\n  text-decoration: underline;\n}\n\n.press-horizontalline-2yBQI {\n  width: 100%;\n  height: 1px;\n  background-color: #000;\n  opacity: 0.2;\n  margin: 24px 0;\n}\n\n.press-aligncenter-92RTZ {\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.press-querywrapper-1Hz5w {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n  margin-right: 64px;\n  margin-right: 4rem;\n}\n\n.press-querybox-1S9aT {\n  width: 215px;\n  height: 126px;\n  padding: 9.6px 16px;\n  padding: 0.6rem 1rem;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.08);\n}\n\n.press-querybox-1S9aT h2 {\n  font-size: 16px;\n  margin: 5px 0 8px 0;\n  line-height: 1.25;\n  font-weight: 600;\n}\n\n.press-querybox-1S9aT .press-horizontalline-2yBQI {\n  height: 1px;\n  width: 100%;\n  margin: 15px auto;\n}\n\n.press-mailwrapper-neZ_i {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 10px auto;\n}\n\n.press-mailwrapper-neZ_i img {\n    margin-right: 5px;\n  }\n\n.press-mailwrapper-neZ_i span {\n    color: #0076ff;\n  }\n\n.press-player-TNRQi {\n  width: 100%;\n  height: 302px;\n}\n\n.press-popupoverlay-3nEuM {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.press-presspopup-2L4gU {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.press-titlebox-1HyUE {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.press-titlebox-1HyUE h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.press-titlebox-1HyUE img {\n    cursor: pointer;\n  }\n\n.press-scrollTop-sIW2V {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.press-scrollTop-sIW2V img {\n    width: 100%;\n    height: 100%;\n  }\n\n.press-header-3mx0Z {\n  width: 100%;\n  height: 240px;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.press-header-3mx0Z .press-headerTitle-28Wi0 {\n  font-size: 48px;\n  line-height: 64px;\n  color: #fff;\n  font-weight: 600;\n}\n\n@media only screen and (min-width: 1400px) {\n  .press-mediacontainer-3Bwvu {\n    width: 60%;\n  }\n\n  .press-querywrapper-1Hz5w {\n    width: 30%;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .press-mediacontainer-3Bwvu {\n    padding-right: 2rem;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .press-togglegroup-mKgsa {\n    padding-left: 2rem;\n  }\n\n  .press-scrollTop-sIW2V {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .press-mediatitle-1cnMU {\n    margin: 16px 0 0;\n    line-height: 24px;\n  }\n\n  .press-horizontalline-2yBQI {\n    margin: 16px 0;\n  }\n\n  .press-readmore-eVfYA {\n    line-height: 20px;\n  }\n\n  .press-mediacontainer-3Bwvu {\n    padding: 0 1rem;\n  }\n\n  .press-mediawrapper-1v5DP {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .press-mediaimgbox-1_4cl {\n    margin: 0 0 0.75rem 0;\n  }\n\n  .press-querywrapper-1Hz5w {\n    width: 100%;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .press-querybox-1S9aT {\n    width: 328px;\n    height: 90px;\n    padding: 16px;\n    margin-top: 24px;\n  }\n\n    .press-querybox-1S9aT h2 {\n      margin: 0;\n    }\n\n    .press-querybox-1S9aT .press-horizontalline-2yBQI {\n      margin: 8px 0;\n    }\n\n    .press-querybox-1S9aT .press-mailwrapper-neZ_i {\n      margin: 0;\n    }\n}\n\n@media only screen and (max-width: 870px) {\n  .press-root-mEOgr {\n    //margin-top: 90px;\n  }\n\n  .press-mediacardwrapper-3sFOs {\n    width: 100%;\n  }\n\n  .press-mediacard-5v4_- {\n    width: 90%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n  }\n\n  .press-mediacontentbox-q_feo {\n    width: 100%;\n  }\n\n  .press-header-3mx0Z {\n    width: 100%;\n    height: 220px;\n  }\n\n  .press-header-3mx0Z .press-headerTitle-28Wi0 {\n    font-size: 40px;\n    line-height: 56px;\n  }\n\n  .press-togglegroup-mKgsa {\n    padding: 1rem 2rem;\n  }\n\n  .press-aligncenter-92RTZ {\n    -ms-flex-align: start;\n        align-items: flex-start;\n  }\n\n  .press-presspopup-2L4gU {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .press-player-TNRQi {\n    padding-right: 12px;\n    min-width: none;\n  }\n\n  .press-titlebox-1HyUE {\n    margin-bottom: 16px;\n  }\n\n  .press-titlebox-1HyUE h2 {\n    font-size: 11.5px;\n  }\n}\n\n@media only screen and (max-width: 560px) {\n  /* .header {\n    background-size: contain;\n  } */\n\n  .press-togglegroup-mKgsa {\n    padding-left: 1rem;\n  }\n\n  .press-mediapostdate-273R1 {\n    font-size: 12px;\n  }\n\n  .press-mediacontent-220NA {\n    font-size: 12px;\n    line-height: 20px;\n  }\n\n  .press-readmore-eVfYA {\n    font-size: 12px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/press/press.scss"],"names":[],"mappings":"AAAA;EACE,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,aAAa;EACb,mBAAmB;EACnB,qDAAqD;UAC7C,6CAA6C;CACtD;;AAED;IACI,YAAY;IACZ,aAAa;IACb,qBAAqB;OAClB,kBAAkB;IACrB,eAAe;GAChB;;AAEH;EACE,aAAa;EACb,cAAc;EACd,mBAAmB;CACpB;;AAED;IACI,YAAY;IACZ,aAAa;IACb,qBAAqB;OAClB,kBAAkB;GACtB;;AAEH;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,kBAAkB;EAClB,qBAAqB;EACrB,uBAAuB;EACvB,YAAY;EACZ,oBAAoB;EACpB,uBAAuB;EACvB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,uBAAuB;EACvB,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,0BAA0B;EAC9B,uBAAuB;MACnB,+BAA+B;CACpC;;AAED;EACE,gBAAgB;EAChB,gBAAgB;CACjB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,gBAAgB;CACjB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,UAAU;EACV,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,aAAa;EACb,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,YAAY;EACZ,uBAAuB;EACvB,aAAa;EACb,eAAe;CAChB;;AAED;EACE,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,0BAA0B;EAC9B,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,cAAc;EACd,oBAAoB;EACpB,qBAAqB;EACrB,qDAAqD;UAC7C,6CAA6C;CACtD;;AAED;EACE,gBAAgB;EAChB,oBAAoB;EACpB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,YAAY;EACZ,YAAY;EACZ,kBAAkB;CACnB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;IACI,kBAAkB;GACnB;;AAEH;IACI,eAAe;GAChB;;AAEH;EACE,YAAY;EACZ,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,OAAO;EACP,QAAQ;EACR,YAAY;EACZ,cAAc;EACd,qCAAqC;EACrC,WAAW;CACZ;;AAED;EACE,cAAc;EACd,YAAY;EACZ,iBAAiB;EACjB,cAAc;EACd,uBAAuB;EACvB,mBAAmB;EACnB,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,oBAAoB;CACrB;;AAED;IACI,gBAAgB;IAChB,UAAU;IACV,WAAW;IACX,oBAAoB;IACpB,2BAA2B;OACxB,wBAAwB;IAC3B,iBAAiB;GAClB;;AAEH;IACI,gBAAgB;GACjB;;AAEH;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,YAAY;EACZ,cAAc;EACd,4FAA4F;EAC5F,0EAA0E;EAC1E,qEAAqE;EACrE,sEAAsE;EACtE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE;IACE,WAAW;GACZ;;EAED;IACE,WAAW;GACZ;CACF;;AAED;EACE;IACE,oBAAoB;GACrB;CACF;;AAED;EACE;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,iBAAiB;IACjB,kBAAkB;GACnB;;EAED;IACE,eAAe;GAChB;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,sBAAsB;GACvB;;EAED;IACE,YAAY;IACZ,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,aAAa;IACb,aAAa;IACb,cAAc;IACd,iBAAiB;GAClB;;IAEC;MACE,UAAU;KACX;;IAED;MACE,cAAc;KACf;;IAED;MACE,UAAU;KACX;CACJ;;AAED;EACE;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;EAED;IACE,WAAW;IACX,2BAA2B;QACvB,uBAAuB;IAC3B,qBAAqB;QACjB,4BAA4B;GACjC;;EAED;IACE,YAAY;GACb;;EAED;IACE,YAAY;IACZ,cAAc;GACf;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,cAAc;IACd,aAAa;IACb,cAAc;GACf;;EAED;IACE,oBAAoB;IACpB,gBAAgB;GACjB;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,kBAAkB;GACnB;CACF;;AAED;EACE;;MAEI;;EAEJ;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"press.scss","sourcesContent":[".root {\n  //margin-top: 85px;\n}\n\n.mediaimgbox {\n  width: 200px;\n  height: 96px;\n  margin-right: 24px;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.16);\n}\n\n.mediaimgbox img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    margin-left: 0;\n  }\n\n.videoimgbox {\n  width: 172px;\n  height: 126px;\n  margin-right: 24px;\n}\n\n.videoimgbox img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n  }\n\n.togglegroup {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  padding: 56px 64px 32px;\n  display: -ms-flexbox;\n  display: flex;\n}\n\nbutton {\n  border-radius: 0;\n}\n\n.mediabutton {\n  padding: 8px 16px;\n  padding: 0.5rem 1rem;\n  border: 1px solid #f36;\n  color: #f36;\n  border-radius: none;\n  background-color: #fff;\n  line-height: normal;\n}\n\n.mediabutton.media {\n  border-radius: 4px 0 0 4px;\n}\n\n.mediabutton.video {\n  border-radius: 0 4px 4px 0;\n}\n\n.mediabutton.active {\n  background-color: #f36;\n  color: #fff;\n}\n\n.mediawrapper {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-line-pack: start;\n      align-content: flex-start;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.mediacontainer {\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.mediacardwrapper {\n  width: 860px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.mediacard {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  cursor: pointer;\n}\n\n.mediacontentbox {\n  width: 635px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.mediatitle {\n  margin: 0;\n  font-size: 20px;\n  line-height: 30px;\n  margin-bottom: 8px;\n}\n\n.mediapostdate {\n  font-size: 14px;\n  opacity: 0.6;\n  margin: 0 0 8px 0;\n  font-weight: normal;\n}\n\n.mediacontent {\n  font-size: 14px;\n  line-height: 21px;\n  color: #000;\n  margin-bottom: 8px;\n}\n\n.readmore {\n  font-size: 14px;\n  line-height: 21px;\n  text-decoration: underline;\n}\n\n.horizontalline {\n  width: 100%;\n  height: 1px;\n  background-color: #000;\n  opacity: 0.2;\n  margin: 24px 0;\n}\n\n.aligncenter {\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.querywrapper {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n  margin-right: 64px;\n  margin-right: 4rem;\n}\n\n.querybox {\n  width: 215px;\n  height: 126px;\n  padding: 9.6px 16px;\n  padding: 0.6rem 1rem;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.08);\n}\n\n.querybox h2 {\n  font-size: 16px;\n  margin: 5px 0 8px 0;\n  line-height: 1.25;\n  font-weight: 600;\n}\n\n.querybox .horizontalline {\n  height: 1px;\n  width: 100%;\n  margin: 15px auto;\n}\n\n.mailwrapper {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 10px auto;\n}\n\n.mailwrapper img {\n    margin-right: 5px;\n  }\n\n.mailwrapper span {\n    color: #0076ff;\n  }\n\n.player {\n  width: 100%;\n  height: 302px;\n}\n\n.popupoverlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.presspopup {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.titlebox {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.titlebox h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.titlebox img {\n    cursor: pointer;\n  }\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.header {\n  width: 100%;\n  height: 240px;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.header .headerTitle {\n  font-size: 48px;\n  line-height: 64px;\n  color: #fff;\n  font-weight: 600;\n}\n\n@media only screen and (min-width: 1400px) {\n  .mediacontainer {\n    width: 60%;\n  }\n\n  .querywrapper {\n    width: 30%;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .mediacontainer {\n    padding-right: 2rem;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .togglegroup {\n    padding-left: 2rem;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .mediatitle {\n    margin: 16px 0 0;\n    line-height: 24px;\n  }\n\n  .horizontalline {\n    margin: 16px 0;\n  }\n\n  .readmore {\n    line-height: 20px;\n  }\n\n  .mediacontainer {\n    padding: 0 1rem;\n  }\n\n  .mediawrapper {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .mediaimgbox {\n    margin: 0 0 0.75rem 0;\n  }\n\n  .querywrapper {\n    width: 100%;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .querybox {\n    width: 328px;\n    height: 90px;\n    padding: 16px;\n    margin-top: 24px;\n  }\n\n    .querybox h2 {\n      margin: 0;\n    }\n\n    .querybox .horizontalline {\n      margin: 8px 0;\n    }\n\n    .querybox .mailwrapper {\n      margin: 0;\n    }\n}\n\n@media only screen and (max-width: 870px) {\n  .root {\n    //margin-top: 90px;\n  }\n\n  .mediacardwrapper {\n    width: 100%;\n  }\n\n  .mediacard {\n    width: 90%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n  }\n\n  .mediacontentbox {\n    width: 100%;\n  }\n\n  .header {\n    width: 100%;\n    height: 220px;\n  }\n\n  .header .headerTitle {\n    font-size: 40px;\n    line-height: 56px;\n  }\n\n  .togglegroup {\n    padding: 1rem 2rem;\n  }\n\n  .aligncenter {\n    -ms-flex-align: start;\n        align-items: flex-start;\n  }\n\n  .presspopup {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .player {\n    padding-right: 12px;\n    min-width: none;\n  }\n\n  .titlebox {\n    margin-bottom: 16px;\n  }\n\n  .titlebox h2 {\n    font-size: 11.5px;\n  }\n}\n\n@media only screen and (max-width: 560px) {\n  /* .header {\n    background-size: contain;\n  } */\n\n  .togglegroup {\n    padding-left: 1rem;\n  }\n\n  .mediapostdate {\n    font-size: 12px;\n  }\n\n  .mediacontent {\n    font-size: 12px;\n    line-height: 20px;\n  }\n\n  .readmore {\n    font-size: 12px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "press-root-mEOgr",
	"mediaimgbox": "press-mediaimgbox-1_4cl",
	"videoimgbox": "press-videoimgbox-3Zudz",
	"togglegroup": "press-togglegroup-mKgsa",
	"mediabutton": "press-mediabutton-1nqxd",
	"media": "press-media-38g3L",
	"video": "press-video-wIJpb",
	"active": "press-active-1N70-",
	"mediawrapper": "press-mediawrapper-1v5DP",
	"mediacontainer": "press-mediacontainer-3Bwvu",
	"mediacardwrapper": "press-mediacardwrapper-3sFOs",
	"mediacard": "press-mediacard-5v4_-",
	"mediacontentbox": "press-mediacontentbox-q_feo",
	"mediatitle": "press-mediatitle-1cnMU",
	"mediapostdate": "press-mediapostdate-273R1",
	"mediacontent": "press-mediacontent-220NA",
	"readmore": "press-readmore-eVfYA",
	"horizontalline": "press-horizontalline-2yBQI",
	"aligncenter": "press-aligncenter-92RTZ",
	"querywrapper": "press-querywrapper-1Hz5w",
	"querybox": "press-querybox-1S9aT",
	"mailwrapper": "press-mailwrapper-neZ_i",
	"player": "press-player-TNRQi",
	"popupoverlay": "press-popupoverlay-3nEuM",
	"presspopup": "press-presspopup-2L4gU",
	"titlebox": "press-titlebox-1HyUE",
	"scrollTop": "press-scrollTop-sIW2V",
	"header": "press-header-3mx0Z",
	"headerTitle": "press-headerTitle-28Wi0"
};

/***/ }),

/***/ "./src/components/Modal/Modal.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Modal/Modal.scss");
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Modal_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class Modal extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "handleKeyDownEvent", e => {
      if (e.which === 27) {
        this.props.toggleModal();
      }
    });

    _defineProperty(this, "handleOutsideClick", e => {
      // ignore clicks on the component itself
      if (this.modalContainer && this.modalContainer.contains(e.target)) {
        return;
      }

      if (this.modalContainer) {
        document.removeEventListener('click', this.handleOutsideClick, false);
        document.removeEventListener('touchstart', this.handleOutsideClick, false);
        this.props.toggleModal();
      }
    });
  }

  componentDidMount() {
    if (this.modalContainer) {
      this.modalContainer.focus();
    }

    document.addEventListener('click', this.handleOutsideClick, false);
    document.addEventListener('touchstart', this.handleOutsideClick, false);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `body-overlay ${_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bodyOverlay} ${this.props.overlayClassName}`,
      role: "presentation",
      onKeyDown: this.handleKeyDownEvent,
      ref: ref => {
        this.modalOverlay = ref;
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.modal} ${this.props.modalClassName}`,
      ref: ref => {
        this.modalContainer = ref;
      },
      tabIndex: "-1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, this.props.children));
  }

}

_defineProperty(Modal, "propTypes", {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  modalClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  overlayClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  toggleModal: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Modal));

/***/ }),

/***/ "./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/press/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _press__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/press/press.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/press/index.js";




async function action() {
  return {
    title: 'Press - Egnify highlitened in major press companies.',
    chunks: ['Press'],
    content: 'Egnify has been highlighted as the best online software for coaching by major press companies across India like Deccan Chronicle, The Indian Express and The Telangana Today.',
    keywords: 'Egnify, Analytics, Indian Education, Education, Schools, Tests, JEE, NEET, JEET, PREP, QMS, Online Test, grading, class, ranks, students, demo, request, institutes, kiran babu, yerranagu',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_press__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/routes/products/get-ranks/press/press.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Modal/Modal.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("react-player");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _press_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/press/press.scss");
/* harmony import */ var _press_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_press_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/press/press.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








class Press extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }));
    });

    _defineProperty(this, "displayHeaderSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.header,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, "egnify in News")));

    _defineProperty(this, "changeToVideo", () => {
      this.setState({
        showMedia: false,
        showVideo: true
      });
    });

    _defineProperty(this, "changeToMedia", () => {
      this.setState({
        showMedia: true,
        showVideo: false
      });
    });

    _defineProperty(this, "displayToggleSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.togglegroup,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: this.state.showMedia ? `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediabutton} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.media} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active}` : `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediabutton} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.media}`,
      onClick: this.changeToMedia,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Media Coverage"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: this.state.showVideo ? `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediabutton} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.video} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active}` : `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediabutton} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.video}`,
      onClick: this.changeToVideo,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, "Video")));

    _defineProperty(this, "displayquerybox", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.querybox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, "For media queries, Please contact"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.horizontalline,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mailwrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/press/mail_outline.svg",
      alt: "mail",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, "kiran@getranks.in"))));

    _defineProperty(this, "displayMediaCoverage", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediawrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacontainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["MEDIA_COVERAGE"].map((post, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacardwrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediaimgbox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: post.icon,
      alt: post.title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacontentbox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediatitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, post.title), post.author ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediapostdate,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, post.author) : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }, post.content), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.readmore,
      href: post.url,
      target: "_blank",
      without: true,
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }, "Read more ..."))), index !== _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["MEDIA_COVERAGE"].length - 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.horizontalline,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 141
      },
      __self: this
    }) : null))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.querywrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146
      },
      __self: this
    }, this.displayquerybox())));

    _defineProperty(this, "displayVideoCoverage", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediawrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacontainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"].map((post, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacardwrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 154
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacard} ${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.aligncenter}`,
      onClick: () => this.openModal(index),
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoimgbox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: post.icon1,
      alt: post.title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediacontentbox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 163
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mediatitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, post.title))), index !== _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"].length - 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.horizontalline,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 168
      },
      __self: this
    }) : null))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.querywrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 173
      },
      __self: this
    }, this.displayquerybox())));

    _defineProperty(this, "openModal", index => {
      this.setState({
        showModal: true,
        clickedpost: _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"][index]
      });
    });

    _defineProperty(this, "toggleModal", () => {
      this.setState({
        showModal: false
      });
    });

    _defineProperty(this, "displayModal", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Modal__WEBPACK_IMPORTED_MODULE_2__["default"], {
      modalClassName: `${_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.presspopup}`,
      overlayClassName: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.popupoverlay,
      toggleModal: this.toggleModal,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 186
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.titlebox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 191
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 192
      },
      __self: this
    }, this.state.clickedpost.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/close.svg",
      onClick: this.toggleModal,
      alt: "close",
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 193
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_3___default.a, {
      playing: false,
      url: this.state.clickedpost.url,
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.player,
      width: "100%",
      height: "80%",
      controls: true,
      loop: true,
      playsinline: true,
      pip: false,
      config: {
        youtube: {
          playerVars: {
            showinfo: 1
          }
        }
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 201
      },
      __self: this
    })));

    this.state = {
      showMedia: true,
      showVideo: false,
      clickedpost: null,
      showModal: false
    };
  }

  componentDidMount() {
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _press_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221
      },
      __self: this
    }, this.state.showModal ? this.displayModal() : null, this.displayHeaderSection(), this.displayToggleSection(), this.state.showMedia ? this.displayMediaCoverage() : this.displayVideoCoverage(), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_press_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Press));

/***/ }),

/***/ "./src/routes/products/get-ranks/press/press.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/press/press.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1ByZXNzLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvTW9kYWwvTW9kYWwuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmVzcy9wcmVzcy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLnNjc3M/ZDgwZSIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmVzcy9pbmRleC5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmVzcy9wcmVzcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmVzcy9wcmVzcy5zY3NzPzE0MzkiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5Nb2RhbC1ib2R5T3ZlcmxheS0zZlNONCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4uTW9kYWwtbW9kYWwtOVFweFUge1xcbiAgb3ZlcmZsb3c6IGF1dG87XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4vKlxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTM2MXB4KSB7XFxuICAubWFya3NNb2RhbCB7XFxuICAgIHdpZHRoOiA2MDBweDtcXG4gICAgbWFyZ2luOiAtMTMzcHggMCAwIC0zMDBweDtcXG4gIH1cXG59XFxuKi9cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QiwrQkFBK0I7RUFDL0IsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLHFDQUFxQztFQUNyQyxjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCOztBQUVEOzs7Ozs7O0VBT0VcIixcImZpbGVcIjpcIk1vZGFsLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLmJvZHlPdmVybGF5IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi5tb2RhbCB7XFxuICBvdmVyZmxvdzogYXV0bztcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi8qXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMzYxcHgpIHtcXG4gIC5tYXJrc01vZGFsIHtcXG4gICAgd2lkdGg6IDYwMHB4O1xcbiAgICBtYXJnaW46IC0xMzNweCAwIDAgLTMwMHB4O1xcbiAgfVxcbn1cXG4qL1xcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJib2R5T3ZlcmxheVwiOiBcIk1vZGFsLWJvZHlPdmVybGF5LTNmU040XCIsXG5cdFwibW9kYWxcIjogXCJNb2RhbC1tb2RhbC05UXB4VVwiXG59OyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIucHJlc3Mtcm9vdC1tRU9nciB7XFxuICAvL21hcmdpbi10b3A6IDg1cHg7XFxufVxcblxcbi5wcmVzcy1tZWRpYWltZ2JveC0xXzRjbCB7XFxuICB3aWR0aDogMjAwcHg7XFxuICBoZWlnaHQ6IDk2cHg7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG59XFxuXFxuLnByZXNzLW1lZGlhaW1nYm94LTFfNGNsIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7XFxuICB9XFxuXFxuLnByZXNzLXZpZGVvaW1nYm94LTNadWR6IHtcXG4gIHdpZHRoOiAxNzJweDtcXG4gIGhlaWdodDogMTI2cHg7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxufVxcblxcbi5wcmVzcy12aWRlb2ltZ2JveC0zWnVkeiBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICB9XFxuXFxuLnByZXNzLXRvZ2dsZWdyb3VwLW1LZ3NhIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggMzJweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuYnV0dG9uIHtcXG4gIGJvcmRlci1yYWRpdXM6IDA7XFxufVxcblxcbi5wcmVzcy1tZWRpYWJ1dHRvbi0xbnF4ZCB7XFxuICBwYWRkaW5nOiA4cHggMTZweDtcXG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtO1xcbiAgYm9yZGVyOiAxcHggc29saWQgI2YzNjtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgYm9yZGVyLXJhZGl1czogbm9uZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbn1cXG5cXG4ucHJlc3MtbWVkaWFidXR0b24tMW5xeGQucHJlc3MtbWVkaWEtMzhnM0wge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4IDAgMCA0cHg7XFxufVxcblxcbi5wcmVzcy1tZWRpYWJ1dHRvbi0xbnF4ZC5wcmVzcy12aWRlby13SUpwYiB7XFxuICBib3JkZXItcmFkaXVzOiAwIDRweCA0cHggMDtcXG59XFxuXFxuLnByZXNzLW1lZGlhYnV0dG9uLTFucXhkLnByZXNzLWFjdGl2ZS0xTjcwLSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5wcmVzcy1tZWRpYXdyYXBwZXItMXY1RFAge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtbGluZS1wYWNrOiBzdGFydDtcXG4gICAgICBhbGlnbi1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5wcmVzcy1tZWRpYWNvbnRhaW5lci0zQnd2dSB7XFxuICBwYWRkaW5nOiAwIDY0cHg7XFxuICBwYWRkaW5nOiAwIDRyZW07XFxufVxcblxcbi5wcmVzcy1tZWRpYWNhcmR3cmFwcGVyLTNzRk9zIHtcXG4gIHdpZHRoOiA4NjBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcblxcbi5wcmVzcy1tZWRpYWNhcmQtNXY0Xy0ge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5wcmVzcy1tZWRpYWNvbnRlbnRib3gtcV9mZW8ge1xcbiAgd2lkdGg6IDYzNXB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLnByZXNzLW1lZGlhdGl0bGUtMWNuTVUge1xcbiAgbWFyZ2luOiAwO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5wcmVzcy1tZWRpYXBvc3RkYXRlLTI3M1IxIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG59XFxuXFxuLnByZXNzLW1lZGlhY29udGVudC0yMjBOQSB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjFweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4ucHJlc3MtcmVhZG1vcmUtZVZmWUEge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLnByZXNzLWhvcml6b250YWxsaW5lLTJ5QlFJIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC4yO1xcbiAgbWFyZ2luOiAyNHB4IDA7XFxufVxcblxcbi5wcmVzcy1hbGlnbmNlbnRlci05MlJUWiB7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5wcmVzcy1xdWVyeXdyYXBwZXItMUh6NXcge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxuICBtYXJnaW4tcmlnaHQ6IDY0cHg7XFxuICBtYXJnaW4tcmlnaHQ6IDRyZW07XFxufVxcblxcbi5wcmVzcy1xdWVyeWJveC0xUzlhVCB7XFxuICB3aWR0aDogMjE1cHg7XFxuICBoZWlnaHQ6IDEyNnB4O1xcbiAgcGFkZGluZzogOS42cHggMTZweDtcXG4gIHBhZGRpbmc6IDAuNnJlbSAxcmVtO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxufVxcblxcbi5wcmVzcy1xdWVyeWJveC0xUzlhVCBoMiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBtYXJnaW46IDVweCAwIDhweCAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4ucHJlc3MtcXVlcnlib3gtMVM5YVQgLnByZXNzLWhvcml6b250YWxsaW5lLTJ5QlFJIHtcXG4gIGhlaWdodDogMXB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IDE1cHggYXV0bztcXG59XFxuXFxuLnByZXNzLW1haWx3cmFwcGVyLW5lWl9pIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbjogMTBweCBhdXRvO1xcbn1cXG5cXG4ucHJlc3MtbWFpbHdyYXBwZXItbmVaX2kgaW1nIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XFxuICB9XFxuXFxuLnByZXNzLW1haWx3cmFwcGVyLW5lWl9pIHNwYW4ge1xcbiAgICBjb2xvcjogIzAwNzZmZjtcXG4gIH1cXG5cXG4ucHJlc3MtcGxheWVyLVROUlFpIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAzMDJweDtcXG59XFxuXFxuLnByZXNzLXBvcHVwb3ZlcmxheS0zbkV1TSB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMHZoO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xcbiAgei1pbmRleDogMztcXG59XFxuXFxuLnByZXNzLXByZXNzcG9wdXAtMkw0Z1Uge1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiA1ODVweDtcXG4gIGhlaWdodDogNDA3cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xcbiAgei1pbmRleDogNDtcXG59XFxuXFxuLnByZXNzLXRpdGxlYm94LTFIeVVFIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBtYXJnaW4tYm90dG9tOiAyOHB4O1xcbn1cXG5cXG4ucHJlc3MtdGl0bGVib3gtMUh5VUUgaDIge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcXG4gICAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgfVxcblxcbi5wcmVzcy10aXRsZWJveC0xSHlVRSBpbWcge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICB9XFxuXFxuLnByZXNzLXNjcm9sbFRvcC1zSVcyViB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLnByZXNzLXNjcm9sbFRvcC1zSVcyViBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5wcmVzcy1oZWFkZXItM214MFoge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDI0MHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgdG9wLCByaWdodCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGxlZnQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQobGVmdCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnByZXNzLWhlYWRlci0zbXgwWiAucHJlc3MtaGVhZGVyVGl0bGUtMjhXaTAge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgbGluZS1oZWlnaHQ6IDY0cHg7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTQwMHB4KSB7XFxuICAucHJlc3MtbWVkaWFjb250YWluZXItM0J3dnUge1xcbiAgICB3aWR0aDogNjAlO1xcbiAgfVxcblxcbiAgLnByZXNzLXF1ZXJ5d3JhcHBlci0xSHo1dyB7XFxuICAgIHdpZHRoOiAzMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XFxuICAucHJlc3MtbWVkaWFjb250YWluZXItM0J3dnUge1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAycmVtO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAucHJlc3MtdG9nZ2xlZ3JvdXAtbUtnc2Ege1xcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW07XFxuICB9XFxuXFxuICAucHJlc3Mtc2Nyb2xsVG9wLXNJVzJWIHtcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgcmlnaHQ6IDE2cHg7XFxuICAgIGJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5wcmVzcy1tZWRpYXRpdGxlLTFjbk1VIHtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAucHJlc3MtaG9yaXpvbnRhbGxpbmUtMnlCUUkge1xcbiAgICBtYXJnaW46IDE2cHggMDtcXG4gIH1cXG5cXG4gIC5wcmVzcy1yZWFkbW9yZS1lVmZZQSB7XFxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbiAgLnByZXNzLW1lZGlhY29udGFpbmVyLTNCd3Z1IHtcXG4gICAgcGFkZGluZzogMCAxcmVtO1xcbiAgfVxcblxcbiAgLnByZXNzLW1lZGlhd3JhcHBlci0xdjVEUCB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5wcmVzcy1tZWRpYWltZ2JveC0xXzRjbCB7XFxuICAgIG1hcmdpbjogMCAwIDAuNzVyZW0gMDtcXG4gIH1cXG5cXG4gIC5wcmVzcy1xdWVyeXdyYXBwZXItMUh6NXcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAucHJlc3MtcXVlcnlib3gtMVM5YVQge1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogOTBweDtcXG4gICAgcGFkZGluZzogMTZweDtcXG4gICAgbWFyZ2luLXRvcDogMjRweDtcXG4gIH1cXG5cXG4gICAgLnByZXNzLXF1ZXJ5Ym94LTFTOWFUIGgyIHtcXG4gICAgICBtYXJnaW46IDA7XFxuICAgIH1cXG5cXG4gICAgLnByZXNzLXF1ZXJ5Ym94LTFTOWFUIC5wcmVzcy1ob3Jpem9udGFsbGluZS0yeUJRSSB7XFxuICAgICAgbWFyZ2luOiA4cHggMDtcXG4gICAgfVxcblxcbiAgICAucHJlc3MtcXVlcnlib3gtMVM5YVQgLnByZXNzLW1haWx3cmFwcGVyLW5lWl9pIHtcXG4gICAgICBtYXJnaW46IDA7XFxuICAgIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4NzBweCkge1xcbiAgLnByZXNzLXJvb3QtbUVPZ3Ige1xcbiAgICAvL21hcmdpbi10b3A6IDkwcHg7XFxuICB9XFxuXFxuICAucHJlc3MtbWVkaWFjYXJkd3JhcHBlci0zc0ZPcyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLnByZXNzLW1lZGlhY2FyZC01djRfLSB7XFxuICAgIHdpZHRoOiA5MCU7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICB9XFxuXFxuICAucHJlc3MtbWVkaWFjb250ZW50Ym94LXFfZmVvIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAucHJlc3MtaGVhZGVyLTNteDBaIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMjIwcHg7XFxuICB9XFxuXFxuICAucHJlc3MtaGVhZGVyLTNteDBaIC5wcmVzcy1oZWFkZXJUaXRsZS0yOFdpMCB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDU2cHg7XFxuICB9XFxuXFxuICAucHJlc3MtdG9nZ2xlZ3JvdXAtbUtnc2Ege1xcbiAgICBwYWRkaW5nOiAxcmVtIDJyZW07XFxuICB9XFxuXFxuICAucHJlc3MtYWxpZ25jZW50ZXItOTJSVFoge1xcbiAgICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIH1cXG5cXG4gIC5wcmVzcy1wcmVzc3BvcHVwLTJMNGdVIHtcXG4gICAgcGFkZGluZzogMTJweDtcXG4gICAgd2lkdGg6IDMyOHB4O1xcbiAgICBoZWlnaHQ6IDIxN3B4O1xcbiAgfVxcblxcbiAgLnByZXNzLXBsYXllci1UTlJRaSB7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XFxuICAgIG1pbi13aWR0aDogbm9uZTtcXG4gIH1cXG5cXG4gIC5wcmVzcy10aXRsZWJveC0xSHlVRSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAucHJlc3MtdGl0bGVib3gtMUh5VUUgaDIge1xcbiAgICBmb250LXNpemU6IDExLjVweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1NjBweCkge1xcbiAgLyogLmhlYWRlciB7XFxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcXG4gIH0gKi9cXG5cXG4gIC5wcmVzcy10b2dnbGVncm91cC1tS2dzYSB7XFxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcXG4gIH1cXG5cXG4gIC5wcmVzcy1tZWRpYXBvc3RkYXRlLTI3M1IxIHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgfVxcblxcbiAgLnByZXNzLW1lZGlhY29udGVudC0yMjBOQSB7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAucHJlc3MtcmVhZG1vcmUtZVZmWUEge1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJlc3MvcHJlc3Muc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHFEQUFxRDtVQUM3Qyw2Q0FBNkM7Q0FDdEQ7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtPQUNsQixrQkFBa0I7SUFDckIsZUFBZTtHQUNoQjs7QUFFSDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixxQkFBcUI7T0FDbEIsa0JBQWtCO0dBQ3RCOztBQUVIO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWTtDQUNiOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsMEJBQTBCO0VBQzlCLHVCQUF1QjtNQUNuQiwrQkFBK0I7Q0FDcEM7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtDQUM1Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtDQUM1Qjs7QUFFRDtFQUNFLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxtQkFBbUI7TUFDZiwwQkFBMEI7RUFDOUIsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHFCQUFxQjtFQUNyQixxREFBcUQ7VUFDN0MsNkNBQTZDO0NBQ3REOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsa0JBQWtCO0NBQ25COztBQUVEO0lBQ0ksa0JBQWtCO0dBQ25COztBQUVIO0lBQ0ksZUFBZTtHQUNoQjs7QUFFSDtFQUNFLFlBQVk7RUFDWixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLFFBQVE7RUFDUixZQUFZO0VBQ1osY0FBYztFQUNkLHFDQUFxQztFQUNyQyxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsb0JBQW9CO0NBQ3JCOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixXQUFXO0lBQ1gsb0JBQW9CO0lBQ3BCLDJCQUEyQjtPQUN4Qix3QkFBd0I7SUFDM0IsaUJBQWlCO0dBQ2xCOztBQUVIO0lBQ0ksZ0JBQWdCO0dBQ2pCOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QsNEZBQTRGO0VBQzVGLDBFQUEwRTtFQUMxRSxxRUFBcUU7RUFDckUsc0VBQXNFO0VBQ3RFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0U7SUFDRSxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxXQUFXO0dBQ1o7Q0FDRjs7QUFFRDtFQUNFO0lBQ0Usb0JBQW9CO0dBQ3JCO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGlCQUFpQjtJQUNqQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxlQUFlO0dBQ2hCOztFQUVEO0lBQ0Usa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtHQUM1Qjs7RUFFRDtJQUNFLHNCQUFzQjtHQUN2Qjs7RUFFRDtJQUNFLFlBQVk7SUFDWixzQkFBc0I7UUFDbEIsd0JBQXdCO0dBQzdCOztFQUVEO0lBQ0UsYUFBYTtJQUNiLGFBQWE7SUFDYixjQUFjO0lBQ2QsaUJBQWlCO0dBQ2xCOztJQUVDO01BQ0UsVUFBVTtLQUNYOztJQUVEO01BQ0UsY0FBYztLQUNmOztJQUVEO01BQ0UsVUFBVTtLQUNYO0NBQ0o7O0FBRUQ7RUFDRTtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLFdBQVc7SUFDWCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHFCQUFxQjtRQUNqQiw0QkFBNEI7R0FDakM7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osY0FBYztHQUNmOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsYUFBYTtJQUNiLGNBQWM7R0FDZjs7RUFFRDtJQUNFLG9CQUFvQjtJQUNwQixnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxrQkFBa0I7R0FDbkI7Q0FDRjs7QUFFRDtFQUNFOztNQUVJOztFQUVKO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjtDQUNGXCIsXCJmaWxlXCI6XCJwcmVzcy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5yb290IHtcXG4gIC8vbWFyZ2luLXRvcDogODVweDtcXG59XFxuXFxuLm1lZGlhaW1nYm94IHtcXG4gIHdpZHRoOiAyMDBweDtcXG4gIGhlaWdodDogOTZweDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbn1cXG5cXG4ubWVkaWFpbWdib3ggaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICBtYXJnaW4tbGVmdDogMDtcXG4gIH1cXG5cXG4udmlkZW9pbWdib3gge1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMjZweDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuXFxuLnZpZGVvaW1nYm94IGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gIH1cXG5cXG4udG9nZ2xlZ3JvdXAge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAzMnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG5idXR0b24ge1xcbiAgYm9yZGVyLXJhZGl1czogMDtcXG59XFxuXFxuLm1lZGlhYnV0dG9uIHtcXG4gIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgcGFkZGluZzogMC41cmVtIDFyZW07XFxuICBib3JkZXI6IDFweCBzb2xpZCAjZjM2O1xcbiAgY29sb3I6ICNmMzY7XFxuICBib3JkZXItcmFkaXVzOiBub25lO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxufVxcblxcbi5tZWRpYWJ1dHRvbi5tZWRpYSB7XFxuICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcXG59XFxuXFxuLm1lZGlhYnV0dG9uLnZpZGVvIHtcXG4gIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xcbn1cXG5cXG4ubWVkaWFidXR0b24uYWN0aXZlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLm1lZGlhd3JhcHBlciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1saW5lLXBhY2s6IHN0YXJ0O1xcbiAgICAgIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG59XFxuXFxuLm1lZGlhY29udGFpbmVyIHtcXG4gIHBhZGRpbmc6IDAgNjRweDtcXG4gIHBhZGRpbmc6IDAgNHJlbTtcXG59XFxuXFxuLm1lZGlhY2FyZHdyYXBwZXIge1xcbiAgd2lkdGg6IDg2MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLm1lZGlhY2FyZCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLm1lZGlhY29udGVudGJveCB7XFxuICB3aWR0aDogNjM1cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG4ubWVkaWF0aXRsZSB7XFxuICBtYXJnaW46IDA7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLm1lZGlhcG9zdGRhdGUge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbn1cXG5cXG4ubWVkaWFjb250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMXB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5yZWFkbW9yZSB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjFweDtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uaG9yaXpvbnRhbGxpbmUge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjI7XFxuICBtYXJnaW46IDI0cHggMDtcXG59XFxuXFxuLmFsaWduY2VudGVyIHtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnF1ZXJ5d3JhcHBlciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG4gIG1hcmdpbi1yaWdodDogNjRweDtcXG4gIG1hcmdpbi1yaWdodDogNHJlbTtcXG59XFxuXFxuLnF1ZXJ5Ym94IHtcXG4gIHdpZHRoOiAyMTVweDtcXG4gIGhlaWdodDogMTI2cHg7XFxuICBwYWRkaW5nOiA5LjZweCAxNnB4O1xcbiAgcGFkZGluZzogMC42cmVtIDFyZW07XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG59XFxuXFxuLnF1ZXJ5Ym94IGgyIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIG1hcmdpbjogNXB4IDAgOHB4IDA7XFxuICBsaW5lLWhlaWdodDogMS4yNTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5xdWVyeWJveCAuaG9yaXpvbnRhbGxpbmUge1xcbiAgaGVpZ2h0OiAxcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1hcmdpbjogMTVweCBhdXRvO1xcbn1cXG5cXG4ubWFpbHdyYXBwZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luOiAxMHB4IGF1dG87XFxufVxcblxcbi5tYWlsd3JhcHBlciBpbWcge1xcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcXG4gIH1cXG5cXG4ubWFpbHdyYXBwZXIgc3BhbiB7XFxuICAgIGNvbG9yOiAjMDA3NmZmO1xcbiAgfVxcblxcbi5wbGF5ZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDMwMnB4O1xcbn1cXG5cXG4ucG9wdXBvdmVybGF5IHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICB6LWluZGV4OiAzO1xcbn1cXG5cXG4ucHJlc3Nwb3B1cCB7XFxuICBwYWRkaW5nOiAyNHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDU4NXB4O1xcbiAgaGVpZ2h0OiA0MDdweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICB6LWluZGV4OiA0O1xcbn1cXG5cXG4udGl0bGVib3gge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIG1hcmdpbi1ib3R0b206IDI4cHg7XFxufVxcblxcbi50aXRsZWJveCBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xcbiAgICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuLnRpdGxlYm94IGltZyB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gIH1cXG5cXG4uc2Nyb2xsVG9wIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uc2Nyb2xsVG9wIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLmhlYWRlciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMjQwcHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCB0b3AsIHJpZ2h0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgI2VhNGM3MCAwJSwgI2IyNDU3YyAxMDAlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uaGVhZGVyIC5oZWFkZXJUaXRsZSB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxNDAwcHgpIHtcXG4gIC5tZWRpYWNvbnRhaW5lciB7XFxuICAgIHdpZHRoOiA2MCU7XFxuICB9XFxuXFxuICAucXVlcnl3cmFwcGVyIHtcXG4gICAgd2lkdGg6IDMwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcXG4gIC5tZWRpYWNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDJyZW07XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC50b2dnbGVncm91cCB7XFxuICAgIHBhZGRpbmctbGVmdDogMnJlbTtcXG4gIH1cXG5cXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLm1lZGlhdGl0bGUge1xcbiAgICBtYXJnaW46IDE2cHggMCAwO1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5ob3Jpem9udGFsbGluZSB7XFxuICAgIG1hcmdpbjogMTZweCAwO1xcbiAgfVxcblxcbiAgLnJlYWRtb3JlIHtcXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAubWVkaWFjb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAwIDFyZW07XFxuICB9XFxuXFxuICAubWVkaWF3cmFwcGVyIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLm1lZGlhaW1nYm94IHtcXG4gICAgbWFyZ2luOiAwIDAgMC43NXJlbSAwO1xcbiAgfVxcblxcbiAgLnF1ZXJ5d3JhcHBlciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5xdWVyeWJveCB7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA5MHB4O1xcbiAgICBwYWRkaW5nOiAxNnB4O1xcbiAgICBtYXJnaW4tdG9wOiAyNHB4O1xcbiAgfVxcblxcbiAgICAucXVlcnlib3ggaDIge1xcbiAgICAgIG1hcmdpbjogMDtcXG4gICAgfVxcblxcbiAgICAucXVlcnlib3ggLmhvcml6b250YWxsaW5lIHtcXG4gICAgICBtYXJnaW46IDhweCAwO1xcbiAgICB9XFxuXFxuICAgIC5xdWVyeWJveCAubWFpbHdyYXBwZXIge1xcbiAgICAgIG1hcmdpbjogMDtcXG4gICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDg3MHB4KSB7XFxuICAucm9vdCB7XFxuICAgIC8vbWFyZ2luLXRvcDogOTBweDtcXG4gIH1cXG5cXG4gIC5tZWRpYWNhcmR3cmFwcGVyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAubWVkaWFjYXJkIHtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIH1cXG5cXG4gIC5tZWRpYWNvbnRlbnRib3gge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5oZWFkZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAyMjBweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXIgLmhlYWRlclRpdGxlIHtcXG4gICAgZm9udC1zaXplOiA0MHB4O1xcbiAgICBsaW5lLWhlaWdodDogNTZweDtcXG4gIH1cXG5cXG4gIC50b2dnbGVncm91cCB7XFxuICAgIHBhZGRpbmc6IDFyZW0gMnJlbTtcXG4gIH1cXG5cXG4gIC5hbGlnbmNlbnRlciB7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgfVxcblxcbiAgLnByZXNzcG9wdXAge1xcbiAgICBwYWRkaW5nOiAxMnB4O1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogMjE3cHg7XFxuICB9XFxuXFxuICAucGxheWVyIHtcXG4gICAgcGFkZGluZy1yaWdodDogMTJweDtcXG4gICAgbWluLXdpZHRoOiBub25lO1xcbiAgfVxcblxcbiAgLnRpdGxlYm94IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC50aXRsZWJveCBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMTEuNXB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU2MHB4KSB7XFxuICAvKiAuaGVhZGVyIHtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgfSAqL1xcblxcbiAgLnRvZ2dsZWdyb3VwIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xcbiAgfVxcblxcbiAgLm1lZGlhcG9zdGRhdGUge1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICB9XFxuXFxuICAubWVkaWFjb250ZW50IHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gIH1cXG5cXG4gIC5yZWFkbW9yZSB7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInJvb3RcIjogXCJwcmVzcy1yb290LW1FT2dyXCIsXG5cdFwibWVkaWFpbWdib3hcIjogXCJwcmVzcy1tZWRpYWltZ2JveC0xXzRjbFwiLFxuXHRcInZpZGVvaW1nYm94XCI6IFwicHJlc3MtdmlkZW9pbWdib3gtM1p1ZHpcIixcblx0XCJ0b2dnbGVncm91cFwiOiBcInByZXNzLXRvZ2dsZWdyb3VwLW1LZ3NhXCIsXG5cdFwibWVkaWFidXR0b25cIjogXCJwcmVzcy1tZWRpYWJ1dHRvbi0xbnF4ZFwiLFxuXHRcIm1lZGlhXCI6IFwicHJlc3MtbWVkaWEtMzhnM0xcIixcblx0XCJ2aWRlb1wiOiBcInByZXNzLXZpZGVvLXdJSnBiXCIsXG5cdFwiYWN0aXZlXCI6IFwicHJlc3MtYWN0aXZlLTFONzAtXCIsXG5cdFwibWVkaWF3cmFwcGVyXCI6IFwicHJlc3MtbWVkaWF3cmFwcGVyLTF2NURQXCIsXG5cdFwibWVkaWFjb250YWluZXJcIjogXCJwcmVzcy1tZWRpYWNvbnRhaW5lci0zQnd2dVwiLFxuXHRcIm1lZGlhY2FyZHdyYXBwZXJcIjogXCJwcmVzcy1tZWRpYWNhcmR3cmFwcGVyLTNzRk9zXCIsXG5cdFwibWVkaWFjYXJkXCI6IFwicHJlc3MtbWVkaWFjYXJkLTV2NF8tXCIsXG5cdFwibWVkaWFjb250ZW50Ym94XCI6IFwicHJlc3MtbWVkaWFjb250ZW50Ym94LXFfZmVvXCIsXG5cdFwibWVkaWF0aXRsZVwiOiBcInByZXNzLW1lZGlhdGl0bGUtMWNuTVVcIixcblx0XCJtZWRpYXBvc3RkYXRlXCI6IFwicHJlc3MtbWVkaWFwb3N0ZGF0ZS0yNzNSMVwiLFxuXHRcIm1lZGlhY29udGVudFwiOiBcInByZXNzLW1lZGlhY29udGVudC0yMjBOQVwiLFxuXHRcInJlYWRtb3JlXCI6IFwicHJlc3MtcmVhZG1vcmUtZVZmWUFcIixcblx0XCJob3Jpem9udGFsbGluZVwiOiBcInByZXNzLWhvcml6b250YWxsaW5lLTJ5QlFJXCIsXG5cdFwiYWxpZ25jZW50ZXJcIjogXCJwcmVzcy1hbGlnbmNlbnRlci05MlJUWlwiLFxuXHRcInF1ZXJ5d3JhcHBlclwiOiBcInByZXNzLXF1ZXJ5d3JhcHBlci0xSHo1d1wiLFxuXHRcInF1ZXJ5Ym94XCI6IFwicHJlc3MtcXVlcnlib3gtMVM5YVRcIixcblx0XCJtYWlsd3JhcHBlclwiOiBcInByZXNzLW1haWx3cmFwcGVyLW5lWl9pXCIsXG5cdFwicGxheWVyXCI6IFwicHJlc3MtcGxheWVyLVROUlFpXCIsXG5cdFwicG9wdXBvdmVybGF5XCI6IFwicHJlc3MtcG9wdXBvdmVybGF5LTNuRXVNXCIsXG5cdFwicHJlc3Nwb3B1cFwiOiBcInByZXNzLXByZXNzcG9wdXAtMkw0Z1VcIixcblx0XCJ0aXRsZWJveFwiOiBcInByZXNzLXRpdGxlYm94LTFIeVVFXCIsXG5cdFwic2Nyb2xsVG9wXCI6IFwicHJlc3Mtc2Nyb2xsVG9wLXNJVzJWXCIsXG5cdFwiaGVhZGVyXCI6IFwicHJlc3MtaGVhZGVyLTNteDBaXCIsXG5cdFwiaGVhZGVyVGl0bGVcIjogXCJwcmVzcy1oZWFkZXJUaXRsZS0yOFdpMFwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9Nb2RhbC5zY3NzJztcblxuY2xhc3MgTW9kYWwgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAvKiogTGlicmFyeSBWYXJpYWJsZSBmb3IgdmVyaWZ5aW5nIHByb3BzIGZyb20gdGhlIHBhcmVudCBjb21wb25lbnQgKi9cbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBtb2RhbENsYXNzTmFtZTogUHJvcFR5cGVzLm9iamVjdCwgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgIG92ZXJsYXlDbGFzc05hbWU6IFByb3BUeXBlcy5vYmplY3QsIC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICB0b2dnbGVNb2RhbDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBpZiAodGhpcy5tb2RhbENvbnRhaW5lcikge1xuICAgICAgdGhpcy5tb2RhbENvbnRhaW5lci5mb2N1cygpO1xuICAgIH1cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gIH1cblxuICBoYW5kbGVLZXlEb3duRXZlbnQgPSBlID0+IHtcbiAgICBpZiAoZS53aGljaCA9PT0gMjcpIHtcbiAgICAgIHRoaXMucHJvcHMudG9nZ2xlTW9kYWwoKTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlT3V0c2lkZUNsaWNrID0gZSA9PiB7XG4gICAgLy8gaWdub3JlIGNsaWNrcyBvbiB0aGUgY29tcG9uZW50IGl0c2VsZlxuICAgIGlmICh0aGlzLm1vZGFsQ29udGFpbmVyICYmIHRoaXMubW9kYWxDb250YWluZXIuY29udGFpbnMoZS50YXJnZXQpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICh0aGlzLm1vZGFsQ29udGFpbmVyKSB7XG4gICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFxuICAgICAgICAndG91Y2hzdGFydCcsXG4gICAgICAgIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLFxuICAgICAgICBmYWxzZSxcbiAgICAgICk7XG4gICAgICB0aGlzLnByb3BzLnRvZ2dsZU1vZGFsKCk7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2Bib2R5LW92ZXJsYXkgJHtzLmJvZHlPdmVybGF5fSAke3RoaXMucHJvcHMub3ZlcmxheUNsYXNzTmFtZX1gfVxuICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgb25LZXlEb3duPXt0aGlzLmhhbmRsZUtleURvd25FdmVudH1cbiAgICAgICAgcmVmPXtyZWYgPT4ge1xuICAgICAgICAgIHRoaXMubW9kYWxPdmVybGF5ID0gcmVmO1xuICAgICAgICB9fVxuICAgICAgPlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLm1vZGFsfSAke3RoaXMucHJvcHMubW9kYWxDbGFzc05hbWV9YH1cbiAgICAgICAgICByZWY9e3JlZiA9PiB7XG4gICAgICAgICAgICB0aGlzLm1vZGFsQ29udGFpbmVyID0gcmVmO1xuICAgICAgICAgIH19XG4gICAgICAgICAgdGFiSW5kZXg9XCItMVwiXG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZHJlbn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoTW9kYWwpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Nb2RhbC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL01vZGFsLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Nb2RhbC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgUHJlc3MgZnJvbSAnLi9wcmVzcyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ1ByZXNzIC0gRWduaWZ5IGhpZ2hsaXRlbmVkIGluIG1ham9yIHByZXNzIGNvbXBhbmllcy4nLFxuICAgIGNodW5rczogWydQcmVzcyddLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnRWduaWZ5IGhhcyBiZWVuIGhpZ2hsaWdodGVkIGFzIHRoZSBiZXN0IG9ubGluZSBzb2Z0d2FyZSBmb3IgY29hY2hpbmcgYnkgbWFqb3IgcHJlc3MgY29tcGFuaWVzIGFjcm9zcyBJbmRpYSBsaWtlIERlY2NhbiBDaHJvbmljbGUsIFRoZSBJbmRpYW4gRXhwcmVzcyBhbmQgVGhlIFRlbGFuZ2FuYSBUb2RheS4nLFxuICAgIGtleXdvcmRzOlxuICAgICAgJ0VnbmlmeSwgQW5hbHl0aWNzLCBJbmRpYW4gRWR1Y2F0aW9uLCBFZHVjYXRpb24sIFNjaG9vbHMsIFRlc3RzLCBKRUUsIE5FRVQsIEpFRVQsIFBSRVAsIFFNUywgT25saW5lIFRlc3QsIGdyYWRpbmcsIGNsYXNzLCByYW5rcywgc3R1ZGVudHMsIGRlbW8sIHJlcXVlc3QsIGluc3RpdHV0ZXMsIGtpcmFuIGJhYnUsIHllcnJhbmFndScsXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8UHJlc3MgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iLCJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IE1vZGFsIGZyb20gJ2NvbXBvbmVudHMvTW9kYWwnO1xuaW1wb3J0IFJlYWN0UGxheWVyIGZyb20gJ3JlYWN0LXBsYXllcic7XG5cbmltcG9ydCBzIGZyb20gJy4vcHJlc3Muc2Nzcyc7XG5pbXBvcnQgeyBNRURJQV9DT1ZFUkFHRSwgUFJFU1NfVklERU9fQ09WRVJBR0UgfSBmcm9tICcuLi9HZXRSYW5rc0NvbnN0YW50cyc7XG5cbmNsYXNzIFByZXNzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHNob3dNZWRpYTogdHJ1ZSxcbiAgICAgIHNob3dWaWRlbzogZmFsc2UsXG4gICAgICBjbGlja2VkcG9zdDogbnVsbCxcbiAgICAgIHNob3dNb2RhbDogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cblxuICBoYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5zY3JvbGxZID4gNTAwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlU2Nyb2xsVG9wID0gKCkgPT4ge1xuICAgIHdpbmRvdy5zY3JvbGxUbyh7XG4gICAgICB0b3A6IDAsXG4gICAgICBiZWhhdmlvcjogJ3Ntb290aCcsXG4gICAgfSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICB9KTtcbiAgfTtcblxuICBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBzaG93U2Nyb2xsIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5zY3JvbGxUb3B9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheUhlYWRlclNlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyfT5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5oZWFkZXJUaXRsZX0+ZWduaWZ5IGluIE5ld3M8L3NwYW4+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY2hhbmdlVG9WaWRlbyA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgc2hvd01lZGlhOiBmYWxzZSwgc2hvd1ZpZGVvOiB0cnVlIH0pO1xuICB9O1xuICBjaGFuZ2VUb01lZGlhID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBzaG93TWVkaWE6IHRydWUsIHNob3dWaWRlbzogZmFsc2UgfSk7XG4gIH07XG4gIGRpc3BsYXlUb2dnbGVTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvZ2dsZWdyb3VwfT5cbiAgICAgIDxidXR0b25cbiAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICB0aGlzLnN0YXRlLnNob3dNZWRpYVxuICAgICAgICAgICAgPyBgJHtzLm1lZGlhYnV0dG9ufSAke3MubWVkaWF9ICR7cy5hY3RpdmV9YFxuICAgICAgICAgICAgOiBgJHtzLm1lZGlhYnV0dG9ufSAke3MubWVkaWF9YFxuICAgICAgICB9XG4gICAgICAgIG9uQ2xpY2s9e3RoaXMuY2hhbmdlVG9NZWRpYX1cbiAgICAgID5cbiAgICAgICAgTWVkaWEgQ292ZXJhZ2VcbiAgICAgIDwvYnV0dG9uPlxuICAgICAgPGJ1dHRvblxuICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgIHRoaXMuc3RhdGUuc2hvd1ZpZGVvXG4gICAgICAgICAgICA/IGAke3MubWVkaWFidXR0b259ICR7cy52aWRlb30gJHtzLmFjdGl2ZX1gXG4gICAgICAgICAgICA6IGAke3MubWVkaWFidXR0b259ICR7cy52aWRlb31gXG4gICAgICAgIH1cbiAgICAgICAgb25DbGljaz17dGhpcy5jaGFuZ2VUb1ZpZGVvfVxuICAgICAgPlxuICAgICAgICBWaWRlb1xuICAgICAgPC9idXR0b24+XG4gICAgPC9kaXY+XG4gICk7XG4gIGRpc3BsYXlxdWVyeWJveCA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5xdWVyeWJveH0+XG4gICAgICA8aDI+Rm9yIG1lZGlhIHF1ZXJpZXMsIFBsZWFzZSBjb250YWN0PC9oMj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhvcml6b250YWxsaW5lfSAvPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbHdyYXBwZXJ9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvcHJlc3MvbWFpbF9vdXRsaW5lLnN2Z1wiIGFsdD1cIm1haWxcIiAvPlxuICAgICAgICA8c3Bhbj5raXJhbkBnZXRyYW5rcy5pbjwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5TWVkaWFDb3ZlcmFnZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5tZWRpYXdyYXBwZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWVkaWFjb250YWluZXJ9PlxuICAgICAgICB7TUVESUFfQ09WRVJBR0UubWFwKChwb3N0LCBpbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1lZGlhY2FyZHdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWVkaWFjYXJkfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWVkaWFpbWdib3h9PlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtwb3N0Lmljb259IGFsdD17cG9zdC50aXRsZX0gLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1lZGlhY29udGVudGJveH0+XG4gICAgICAgICAgICAgICAgPGgyIGNsYXNzTmFtZT17cy5tZWRpYXRpdGxlfT57cG9zdC50aXRsZX08L2gyPlxuICAgICAgICAgICAgICAgIHtwb3N0LmF1dGhvciA/IChcbiAgICAgICAgICAgICAgICAgIDxoNiBjbGFzc05hbWU9e3MubWVkaWFwb3N0ZGF0ZX0+e3Bvc3QuYXV0aG9yfTwvaDY+XG4gICAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWVkaWFjb250ZW50fT57cG9zdC5jb250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucmVhZG1vcmV9XG4gICAgICAgICAgICAgICAgICBocmVmPXtwb3N0LnVybH1cbiAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICB3aXRob3V0XG4gICAgICAgICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICBSZWFkIG1vcmUgLi4uXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAge2luZGV4ICE9PSBNRURJQV9DT1ZFUkFHRS5sZW5ndGggLSAxID8gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ob3Jpem9udGFsbGluZX0gLz5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MucXVlcnl3cmFwcGVyfT57dGhpcy5kaXNwbGF5cXVlcnlib3goKX08L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5VmlkZW9Db3ZlcmFnZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5tZWRpYXdyYXBwZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWVkaWFjb250YWluZXJ9PlxuICAgICAgICB7UFJFU1NfVklERU9fQ09WRVJBR0UubWFwKChwb3N0LCBpbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1lZGlhY2FyZHdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MubWVkaWFjYXJkfSAke3MuYWxpZ25jZW50ZXJ9YH1cbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5vcGVuTW9kYWwoaW5kZXgpfVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9pbWdib3h9PlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtwb3N0Lmljb24xfSBhbHQ9e3Bvc3QudGl0bGV9IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tZWRpYWNvbnRlbnRib3h9PlxuICAgICAgICAgICAgICAgIDxoMiBjbGFzc05hbWU9e3MubWVkaWF0aXRsZX0+e3Bvc3QudGl0bGV9PC9oMj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIHtpbmRleCAhPT0gUFJFU1NfVklERU9fQ09WRVJBR0UubGVuZ3RoIC0gMSA/IChcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaG9yaXpvbnRhbGxpbmV9IC8+XG4gICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnF1ZXJ5d3JhcHBlcn0+e3RoaXMuZGlzcGxheXF1ZXJ5Ym94KCl9PC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG4gIG9wZW5Nb2RhbCA9IGluZGV4ID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dNb2RhbDogdHJ1ZSxcbiAgICAgIGNsaWNrZWRwb3N0OiBQUkVTU19WSURFT19DT1ZFUkFHRVtpbmRleF0sXG4gICAgfSk7XG4gIH07XG4gIHRvZ2dsZU1vZGFsID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBzaG93TW9kYWw6IGZhbHNlIH0pO1xuICB9O1xuICBkaXNwbGF5TW9kYWwgPSAoKSA9PiAoXG4gICAgPE1vZGFsXG4gICAgICBtb2RhbENsYXNzTmFtZT17YCR7cy5wcmVzc3BvcHVwfWB9XG4gICAgICBvdmVybGF5Q2xhc3NOYW1lPXtzLnBvcHVwb3ZlcmxheX1cbiAgICAgIHRvZ2dsZU1vZGFsPXt0aGlzLnRvZ2dsZU1vZGFsfVxuICAgID5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRpdGxlYm94fT5cbiAgICAgICAgPGgyPnt0aGlzLnN0YXRlLmNsaWNrZWRwb3N0LnRpdGxlfTwvaDI+XG4gICAgICAgIDxpbWdcbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL2Nsb3NlLnN2Z1wiXG4gICAgICAgICAgb25DbGljaz17dGhpcy50b2dnbGVNb2RhbH1cbiAgICAgICAgICBhbHQ9XCJjbG9zZVwiXG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cblxuICAgICAgPFJlYWN0UGxheWVyXG4gICAgICAgIHBsYXlpbmc9e2ZhbHNlfVxuICAgICAgICB1cmw9e3RoaXMuc3RhdGUuY2xpY2tlZHBvc3QudXJsfVxuICAgICAgICBjbGFzc05hbWU9e3MucGxheWVyfVxuICAgICAgICB3aWR0aD1cIjEwMCVcIlxuICAgICAgICBoZWlnaHQ9XCI4MCVcIlxuICAgICAgICBjb250cm9sc1xuICAgICAgICBsb29wXG4gICAgICAgIHBsYXlzaW5saW5lXG4gICAgICAgIHBpcD17ZmFsc2V9XG4gICAgICAgIGNvbmZpZz17e1xuICAgICAgICAgIHlvdXR1YmU6IHtcbiAgICAgICAgICAgIHBsYXllclZhcnM6IHsgc2hvd2luZm86IDEgfSxcbiAgICAgICAgICB9LFxuICAgICAgICB9fVxuICAgICAgLz5cbiAgICA8L01vZGFsPlxuICApO1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJvb3R9PlxuICAgICAgICB7dGhpcy5zdGF0ZS5zaG93TW9kYWwgPyB0aGlzLmRpc3BsYXlNb2RhbCgpIDogbnVsbH1cbiAgICAgICAge3RoaXMuZGlzcGxheUhlYWRlclNlY3Rpb24oKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVRvZ2dsZVNlY3Rpb24oKX1cblxuICAgICAgICB7dGhpcy5zdGF0ZS5zaG93TWVkaWFcbiAgICAgICAgICA/IHRoaXMuZGlzcGxheU1lZGlhQ292ZXJhZ2UoKVxuICAgICAgICAgIDogdGhpcy5kaXNwbGF5VmlkZW9Db3ZlcmFnZSgpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U2Nyb2xsVG9Ub3AoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShQcmVzcyk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL3ByZXNzLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vcHJlc3Muc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL3ByZXNzLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQXdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBckNBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF1QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBN0RBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUpBO0FBQ0E7QUE0REE7Ozs7Ozs7QUNwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVEE7QUFhQTtBQUNBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQW9CQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBOUJBO0FBZ0NBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQXhDQTtBQTBDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQXpEQTtBQTJEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE3REE7QUFpRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBbkVBO0FBb0VBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQXRFQTtBQXVFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBS0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUtBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFuRkE7QUErRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBckdBO0FBeUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBeklBO0FBNklBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFwS0E7QUF1S0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBNUtBO0FBNktBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUEvS0E7QUFpTEE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQURBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE5TEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQStMQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBaE9BO0FBQ0E7QUFpT0E7Ozs7Ozs7QUMxT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=