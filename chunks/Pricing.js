require("source-map-support").install();
exports.ids = ["Pricing"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Modal-bodyOverlay-3fSN4 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.Modal-modal-9QpxU {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,+BAA+B;EAC/B,iBAAiB;CAClB;;AAED;EACE,eAAe;EACf,qCAAqC;EACrC,cAAc;EACd,mBAAmB;CACpB;;AAED;;;;;;;EAOE","file":"Modal.scss","sourcesContent":[".bodyOverlay {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.modal {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bodyOverlay": "Modal-bodyOverlay-3fSN4",
	"modal": "Modal-modal-9QpxU"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/pricing/Pricing.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Pricing-pricingHeader-2MLoH {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.Pricing-darkBackground-17ag8 {\n  background-color: #25282b !important;\n}\n\n.Pricing-price_title-3Yqn8 {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.Pricing-price_content-5qlsh {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.Pricing-modulesContainer-9h2Yz {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 72px;\n}\n\n.Pricing-modulesHeader-zfiUY {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.Pricing-modulesList-3_DuR {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(5, 1fr);\n  gap: 44px;\n  margin-top: 24px;\n}\n\n.Pricing-module-dy7g3 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Pricing-imageWrapper-2cu95 {\n  width: 52px;\n  height: 52px;\n  border-radius: 26px;\n  -webkit-box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-bottom: 12px;\n}\n\n.Pricing-imageWrapper-2cu95 img {\n  width: 32px;\n  height: 32px;\n}\n\n.Pricing-scrollTop-Aag07 {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Pricing-scrollTop-Aag07 img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Pricing-price_content-5qlsh p {\n  font-size: 16px;\n  opacity: 0.7;\n  line-height: 2;\n  line-height: 32px;\n}\n\n.Pricing-annual-30ie_ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  margin-right: 50px;\n  height: auto;\n}\n\n.Pricing-toggle_btn-3t9M8 {\n  margin-right: 50px;\n}\n\n.Pricing-annual_text-c6h-G {\n  font-size: 14px;\n  padding-bottom: 4px;\n  line-height: normal;\n  border-bottom: 2px solid #fff;\n}\n\n.Pricing-annual_offer-2VBwG {\n  opacity: 0.8;\n  color: #f36;\n  margin-bottom: 4px;\n  display: inline-block;\n}\n\n.Pricing-underline-VNfR5 {\n  border-bottom: 2px solid black;\n}\n\n.Pricing-pricing_container-2DZ9K {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.Pricing-plans-3oyRG {\n  padding: 0 48px 32px 48px;\n  padding: 0 3rem 2rem 3rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Pricing-plan-cbyB7 {\n  width: 270px;\n  height: 477px;\n  margin: 0 6px;\n  border-radius: 8px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  position: relative;\n}\n\n.Pricing-plan-cbyB7 .Pricing-innerCard-XD-XR {\n    background-color: #fff;\n    position: absolute;\n    padding: 32px 0;\n    padding: 2rem 0;\n    width: 100%;\n    height: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    z-index: 1;\n  }\n\n.Pricing-symbol-1PZl7 {\n  border-radius: 100%;\n}\n\n.Pricing-pricing_title-18Vxm {\n  font-size: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  width: 100%;\n  color: #25282b;\n  margin-bottom: 50px;\n}\n\n.Pricing-toggle_outer-TDyU0 {\n  width: 56px;\n  height: 31px;\n  border-radius: 24px;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 3px;\n}\n\n.Pricing-toggle_on-KyTo6 {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Pricing-toggle_inner-dxNua {\n  width: 24.9px;\n  height: 24.9px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n.Pricing-plan_title_students-ixHtF {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.Pricing-plan_title-2z_k0 p {\n    margin: 0;\n  }\n\n.Pricing-plan_title_price-2Zx49 {\n  font-weight: 600;\n  color: #25282b;\n  font-size: 24px;\n  opacity: 1;\n  display: inline-block;\n  margin-bottom: 8px;\n}\n\n.Pricing-price-rctgP {\n  color: #25282b;\n  font-size: 32px;\n  font-weight: 600;\n}\n\n.Pricing-price-rctgP p {\n    margin: 32px 0;\n  }\n\n.Pricing-price_desc-4xVbA {\n  color: #000;\n  opacity: 0.6;\n  line-height: 1.5;\n}\n\n.Pricing-price_desc-4xVbA p {\n    margin: 0 0 24px 0;\n  }\n\n.Pricing-plan-cbyB7 button {\n  margin-top: 40px;\n  padding: 0% 15%;\n  text-transform: capitalize;\n  font-weight: normal;\n  border-radius: 24px;\n}\n\n.Pricing-price_contact-7uNig a {\n  text-decoration: underline;\n  // margin-bottom: 500px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.Pricing-pricePerStudentHighlight-qAWcu {\n  font-size: 24px;\n  font-weight: 600;\n  background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n  -webkit-text-fill-color: transparent;\n  -webkit-background-clip: text;\n  color: #25282b;\n}\n\n.Pricing-buyButton-3g0NI {\n  background-color: #f36;\n}\n\n.Pricing-enterprise-1O-mF {\n  width: 91.5%;\n  margin: auto;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 8px;\n  padding: 42px 200px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 56px;\n}\n\n.Pricing-enterpriseHeading-2Xm68 {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.Pricing-enterprisecontext-3AD9J {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.Pricing-contactus-XfL9G {\n  padding: 8px 20px;\n  border-radius: 24px;\n  background-color: #f36;\n  color: #fff;\n  font-size: 14px;\n  line-height: 20px;\n}\n\n/* .faq_container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  color: #25282b;\n  background-color: #f7f7f7;\n}\n\n.faq_title p {\n  font-size: 40px;\n  font-weight: 600;\n}\n\n.expand_all {\n  margin: 0% 10%;\n  align-self: flex-end;\n  font-size: 16px;\n  color: #25282b;\n  cursor: pointer;\n  border-bottom: 1px solid black;\n  width: max-content;\n}\n\n.accordian {\n  width: 80%;\n} */\n\n/* .border {\n  cursor: pointer;\n  border-bottom: 1px solid #c7c7c7;\n  padding: 44px 0 40px 0;\n}\n\n.border:last-child {\n  border-bottom: none;\n} */\n\n/* .question {\n  width: 100%;\n  font-size: 16px;\n  color: #25282b;\n  font-weight: bold;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.answer {\n  display: none;\n  font-size: 14px;\n  line-height: 25.4px;\n  width: 85%;\n} */\n\n/* .show .answer {\n  display: block;\n} */\n\n/* .up {\n  display: none;\n} */\n\n/* .show .up {\n  display: block;\n  transform: rotate(180deg);\n}\n\n.show .down {\n  display: none;\n} */\n\n.Pricing-leftArrow-3BcpG,\n.Pricing-rightArrow-1CGiq {\n  display: none;\n}\n\n.Pricing-circle_bg-vbvtk {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.Pricing-square_bg-1fWOk {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n  -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n.Pricing-line_vector_bg-2Zkss {\n  -webkit-transform: rotate(180deg);\n      -ms-transform: rotate(180deg);\n          transform: rotate(180deg);\n  opacity: 0.5;\n  position: absolute;\n  left: 10%;\n}\n\n.Pricing-triangle_bg-1X7I4 {\n  position: absolute;\n  right: 7%;\n}\n\n.Pricing-displayClients-SxgNR {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.Pricing-displayClients-SxgNR h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Pricing-displayClients-SxgNR span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Pricing-displayClients-SxgNR span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q a {\n  width: 259px;\n  height: 127px;\n}\n\n.Pricing-displayClients-SxgNR span a:hover {\n  text-decoration: underline;\n}\n\n.Pricing-clientsWrapper-65G_i {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Pricing-clientsWrapper-65G_i .Pricing-client-14kGt {\n  width: 172px;\n  height: 112px;\n}\n\n.Pricing-achievedContainer-3NBMf {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu {\n    text-align: center;\n    font-size: 40px;\n    line-height: 1.2;\n    color: #25282b;\n    font-weight: 600;\n    margin-bottom: 40px;\n  }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(4, 1fr);\n    // grid-template-rows: repeat(2, 1fr);\n    gap: 16px;\n    gap: 1rem;\n    margin: auto;\n  }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd {\n      width: 270px;\n      height: 202px;\n      font-size: 24px;\n      font-size: 1.5rem;\n      color: rgba(37, 40, 43, 0.6);\n      line-height: 40px;\n      padding: 24px;\n      padding: 1.5rem;\n      border-radius: 0.5rem;\n      -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n              box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n      background-color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI {\n        width: 52px;\n        height: 52px;\n        border-radius: 50%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-pack: center;\n            justify-content: center;\n        -ms-flex-align: center;\n            align-items: center;\n        margin-bottom: 20px;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-clients-2mnCX {\n        background-color: #fff3eb;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-students-1YHpU {\n        background-color: #f7effe;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-tests-3nDUz {\n        background-color: #ffebf0;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-questions-3qJ8D {\n        background-color: #ebffef;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm {\n        font-size: 36px;\n        font-weight: bold;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-questionsHighlight-1HJlR {\n        color: #00ac26;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-testsHighlight-vkapq {\n        color: #f36;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-studentsHighlight-2UuO9 {\n        color: #8c00fe;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-clientsHighlight-2hUQE {\n        color: #f60;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-subText-3PFWz {\n        font-size: 24px;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.Pricing-featured-3vf8L {\n  background-color: #f7f7f7;\n  padding: 56px 64px 40px;\n  padding: 3.5rem 4rem 2.5rem;\n}\n\n.Pricing-featured-3vf8L h2 {\n  text-align: center;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n  margin-top: 0;\n  font-size: 40px;\n  font-size: 2.5rem;\n  line-height: 48px;\n  line-height: 3rem;\n}\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 40px;\n  gap: 2.5rem;\n  margin: auto;\n}\n\n.Pricing-titlebox-1mBcy {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.Pricing-titlebox-1mBcy h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.Pricing-titlebox-1mBcy img {\n    cursor: pointer;\n  }\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q a img {\n  background-color: #fff;\n  width: 100%;\n  height: 100%;\n}\n\n.Pricing-on_news-2bgDh {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-max-content;\n  height: -moz-max-content;\n  height: max-content;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  gap: 12px;\n  margin: 0 auto 56px auto;\n}\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9 {\n    width: 368px;\n    height: 250px;\n    cursor: pointer;\n  }\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9:nth-child(1) {\n    margin-left: 0;\n  }\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9:nth-child(3) {\n    margin-right: 0;\n  }\n\n.Pricing-awardsContainer-2PyIg {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 56px 192px;\n  padding: 3.5rem 12rem;\n  background-color: #fff;\n}\n\n.Pricing-awardsContainer-2PyIg .Pricing-awardsTitle-2F_p8 {\n    font-size: 40px;\n    font-size: 2.5rem;\n    font-weight: 600;\n    color: #25282b;\n    margin-bottom: 48px;\n    margin-bottom: 3rem;\n  }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL {\n    display: grid;\n    grid-template-columns: repeat(3, 1fr);\n    gap: 88px;\n    gap: 5.5rem;\n  }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP {\n      width: 240px;\n      width: 15rem;\n      height: 208px;\n      height: 13rem;\n    }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP:nth-child(3n + 2) {\n      margin-right: 0;\n    }\n\n.Pricing-awardsContainer-2PyIg .Pricing-dots-2Jt23 {\n    display: none;\n  }\n\n.Pricing-popupoverlay-32yez {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.Pricing-presspopup-1UCPJ {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.Pricing-player-1FCsc {\n  width: 100%;\n  height: 302px;\n}\n\n@media only screen and (max-width: 990px) {\n  .Pricing-mobilePricingContainer-3w6Qk {\n    width: 100%;\n  }\n\n  .Pricing-modulesContainer-9h2Yz {\n    margin: 64px 0;\n  }\n\n  .Pricing-tabsContainer-1-5am {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    background-color: #f7f7f7;\n  }\n\n  .Pricing-annualtab-dyIQZ {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-right-radius: 8px;\n  }\n\n  .Pricing-monthtab-3Jy_s {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-left-radius: 8px;\n  }\n\n  .Pricing-activeTab-3vi1R {\n    // cursor: pointer;\n    background-color: #f7f7f7;\n    border: 0;\n  }\n\n  .Pricing-mobilePlans-vbHxh {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n    padding-bottom: 12px;\n  }\n\n  .Pricing-mobilePriceCard-32uBZ {\n    padding: 24px 52px 24px 53px;\n    margin: 12px 0;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    border-radius: 8px;\n    max-width: 328px;\n    background-color: #fff;\n  }\n\n  .Pricing-presspopup-1UCPJ {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .Pricing-titlebox-1mBcy {\n    margin-bottom: 16px;\n  }\n\n  .Pricing-titlebox-1mBcy h2 {\n    font-size: 11.5px;\n  }\n\n  .Pricing-modulesList-3_DuR {\n    grid-template-columns: repeat(3, 1fr);\n    grid-column-gap: 40px;\n    grid-row-gap: 20px;\n  }\n\n  .Pricing-imageWrapper-2cu95 {\n    width: 40px;\n    height: 40px;\n  }\n\n  .Pricing-imageWrapper-2cu95 img {\n    width: 24px;\n    height: 24px;\n  }\n\n  .Pricing-pricePerStudentHighlightMobile-36iXF {\n    font-size: 20px;\n    font-weight: 600;\n    background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n    -webkit-text-fill-color: transparent;\n    -webkit-background-clip: text;\n    color: #25282b;\n    opacity: 1 !important;\n  }\n\n  .Pricing-displayClients-SxgNR {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n    background-color: #fff;\n  }\n\n  .Pricing-displayClients-SxgNR h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-client-14kGt {\n    width: 9.75rem;\n    height: 7rem;\n    border: 1px solid rgba(37, 40, 43, 0.16);\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-client-14kGt.Pricing-active-1fA3n {\n    display: block;\n  }\n\n  .Pricing-leftArrow-3BcpG {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    left: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .Pricing-leftArrow-3BcpG span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .Pricing-rightArrow-1CGiq {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    right: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .Pricing-rightArrow-1CGiq span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i span {\n    max-width: 400px;\n  }\n\n  .Pricing-dots-2Jt23 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n    margin-bottom: 24px;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-dots-2Jt23 {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .Pricing-achievedContainer-3NBMf {\n    padding: 1.5rem 1rem;\n    background-image: url('/images/home/Confetti.svg');\n    background-color: #f7f7f7;\n  }\n\n    .Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu {\n      font-size: 1.5rem;\n      padding: 0;\n      margin-bottom: 1.5rem;\n    }\n\n      .Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu span::after {\n        content: '\\A';\n        white-space: pre;\n      }\n\n    .Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL {\n      grid-template-columns: 1fr;\n      gap: 0.75rem;\n      max-width: 20.5rem;\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: auto;\n    }\n\n  .Pricing-awardsContainer-2PyIg {\n    padding: 1.5rem;\n  }\n\n    .Pricing-awardsContainer-2PyIg .Pricing-awardsTitle-2F_p8 {\n      font-size: 1.5rem;\n      padding-bottom: 0;\n      margin-bottom: 2rem;\n    }\n\n    .Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      grid-template-columns: 1fr 1fr;\n      gap: 1.5rem 1rem;\n      margin: auto;\n      position: relative;\n    }\n\n      .Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP {\n        width: 100%;\n        height: 100%;\n        max-width: 15rem;\n        max-height: 13rem;\n      }\n    .Pricing-price_contact-7uNig a {\n      font-size: 14px;\n      line-height: 21px;\n    }\n\n  .Pricing-featured-3vf8L {\n    padding: 1.5rem 1rem 1.25rem;\n  }\n\n    .Pricing-featured-3vf8L h2 {\n      font-size: 1.5rem;\n      line-height: normal;\n      margin: 0 0 1.5rem 0;\n    }\n\n    .Pricing-featured-3vf8L .Pricing-showMedia-6U76q {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: 0 auto 0 auto;\n      grid-template-columns: 1fr 1fr;\n      gap: 1rem;\n      position: relative;\n    }\n\n      .Pricing-featured-3vf8L .Pricing-showMedia-6U76q a {\n        width: 140px;\n        height: 72px;\n      }\n\n  .Pricing-on_news-2bgDh {\n    margin-bottom: 1.5rem;\n    grid-template-columns: repeat(1, 1fr);\n  }\n\n    .Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9 {\n      width: 100%;\n      max-width: 340px;\n    }\n\n  .Pricing-pricingHeader-2MLoH {\n    padding: 24px 0 0;\n  }\n\n  .Pricing-scrollTop-Aag07 {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Pricing-price_title-3Yqn8 {\n    font-size: 32px;\n  }\n\n  .Pricing-price_content-5qlsh {\n    width: 100%;\n    margin: 16px 0 0;\n    max-width: 90%;\n  }\n\n  .Pricing-toggle_btn-3t9M8 {\n    margin-left: -20px;\n    margin-right: 20px;\n  }\n\n  .Pricing-toggle_outer-TDyU0 {\n    width: 40px;\n    height: 22px;\n  }\n\n  .Pricing-toggle_inner-dxNua {\n    width: 17.8px;\n    height: 17.6px;\n  }\n\n  .Pricing-plan-cbyB7 {\n    width: 270px;\n    display: none;\n    margin: 0;\n  }\n\n  .Pricing-active-1fA3n {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .Pricing-dot-2Wd1n {\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background-color: #e5e5e5;\n    margin: 0 5px;\n  }\n\n  .Pricing-dotactive-31__- {\n    background-color: #f36;\n  }\n\n  .Pricing-plan_title_students-ixHtF {\n    font-size: 16px;\n    line-height: 32px;\n  }\n\n  .Pricing-plan_title-2z_k0 p {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  /* .question {\n    font-size: 14px;\n    font-weight: 600;\n  } */\n    .Pricing-price-rctgP p {\n      font-size: 32px;\n      letter-spacing: -0.96px;\n    }\n\n  .Pricing-price_desc-4xVbA {\n    width: 101%;\n    font-size: 14px;\n    line-height: 21px;\n    margin-top: 24px;\n    opacity: 0.8;\n  }\n\n  .Pricing-buyButton-3g0NI {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    width: 174px;\n    border-radius: 24px;\n    color: #fff;\n    padding: 10px 16px;\n    font-size: 16px;\n    font-weight: 600;\n    margin-top: 28px;\n  }\n\n  /* .faq_container {\n    padding: 24px;\n\n    .faq_title p {\n      font-size: 32px;\n      margin-top: 0;\n      margin-bottom: 24px;\n    }\n\n    .expand_all {\n      font-size: 14px;\n      margin: 0 auto 4px auto;\n    }\n\n    .accordian {\n      width: 100%;\n\n      .border {\n        padding: 16px 0;\n\n        .answer {\n          width: 100%;\n\n          p {\n            margin-top: 8px;\n            margin-bottom: 0;\n            line-height: 18px;\n            opacity: 0.7;\n            font-size: 12px;\n          }\n        }\n      }\n    }\n  } */\n\n  .Pricing-circle_bg-vbvtk {\n    display: none;\n    position: absolute;\n    width: 12%;\n    top: 25%;\n    left: 95%;\n  }\n\n  .Pricing-square_bg-1fWOk {\n    display: none;\n    position: absolute;\n    top: 120%;\n    left: -3%;\n    -webkit-transform: rotate(45deg);\n        -ms-transform: rotate(45deg);\n            transform: rotate(45deg);\n    width: 10%;\n  }\n\n  .Pricing-line_vector_bg-2Zkss {\n    display: none;\n  }\n\n  .Pricing-triangle_bg-1X7I4 {\n    display: none;\n    position: absolute;\n    left: -10%;\n    width: 20%;\n    top: 554px;\n  }\n}\n\n.Pricing-moduleLabel-28Van {\n  color: #25282b;\n}\n\n.Pricing-viewLink-1-bHw {\n  width: 195px;\n  height: 32px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 64.6px 0 112px 0;\n  padding: 0 !important;\n  font-size: 24px;\n  line-height: 1.33;\n  color: #f36;\n  text-align: center;\n  text-decoration: underline;\n}\n\n@media only screen and (max-width: 990px) {\n  .Pricing-viewLink-1-bHw {\n    font-size: 14px;\n    margin: 0 0 48px 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/Pricing.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,qCAAqC;CACtC;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oDAAoD;UAC5C,4CAA4C;EACpD,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,aAAa;EACb,eAAe;EACf,kBAAkB;CACnB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,mBAAmB;EACnB,aAAa;CACd;;AAED;EACE,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,oBAAoB;EACpB,oBAAoB;EACpB,8BAA8B;CAC/B;;AAED;EACE,aAAa;EACb,YAAY;EACZ,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,+BAA+B;CAChC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,0BAA0B;EAC1B,0BAA0B;EAC1B,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,aAAa;EACb,cAAc;EACd,cAAc;EACd,mBAAmB;EACnB,uBAAuB;EACvB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;CACpB;;AAED;IACI,uBAAuB;IACvB,mBAAmB;IACnB,gBAAgB;IAChB,gBAAgB;IAChB,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;IACnB,WAAW;GACZ;;AAEH;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,YAAY;EACZ,eAAe;EACf,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;CACd;;AAED;EACE,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;EACE,cAAc;EACd,eAAe;EACf,oBAAoB;EACpB,uBAAuB;CACxB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,UAAU;GACX;;AAEH;EACE,iBAAiB;EACjB,eAAe;EACf,gBAAgB;EAChB,WAAW;EACX,sBAAsB;EACtB,mBAAmB;CACpB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,iBAAiB;CAClB;;AAED;IACI,eAAe;GAChB;;AAEH;EACE,YAAY;EACZ,aAAa;EACb,iBAAiB;CAClB;;AAED;IACI,mBAAmB;GACpB;;AAEH;EACE,iBAAiB;EACjB,gBAAgB;EAChB,2BAA2B;EAC3B,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,wEAAwE;EACxE,mEAAmE;EACnE,gEAAgE;EAChE,qCAAqC;EACrC,8BAA8B;EAC9B,eAAe;CAChB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,aAAa;EACb,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,oBAAoB;EACpB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,8BAA8B;EAClC,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,kBAAkB;EAClB,oBAAoB;EACpB,uBAAuB;EACvB,YAAY;EACZ,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;;;;;;;;;;;;;;;;;;;;;;;;;IAyBI;;AAEJ;;;;;;;;IAQI;;AAEJ;;;;;;;;;;;;;;;IAeI;;AAEJ;;IAEI;;AAEJ;;IAEI;;AAEJ;;;;;;;IAOI;;AAEJ;;EAEE,cAAc;CACf;;AAED;EACE,mBAAmB;EACnB,UAAU;EACV,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,QAAQ;EACR,iCAAiC;MAC7B,6BAA6B;UACzB,yBAAyB;CAClC;;AAED;EACE,kCAAkC;MAC9B,8BAA8B;UAC1B,0BAA0B;EAClC,aAAa;EACb,mBAAmB;EACnB,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,UAAU;CACX;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;IACI,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,iBAAiB;IACjB,oBAAoB;GACrB;;AAEH;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,sCAAsC;IACtC,UAAU;IACV,UAAU;IACV,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;MACd,gBAAgB;MAChB,kBAAkB;MAClB,6BAA6B;MAC7B,kBAAkB;MAClB,cAAc;MACd,gBAAgB;MAChB,sBAAsB;MACtB,+DAA+D;cACvD,uDAAuD;MAC/D,uBAAuB;MACvB,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,YAAY;QACZ,aAAa;QACb,mBAAmB;QACnB,qBAAqB;QACrB,cAAc;QACd,sBAAsB;YAClB,wBAAwB;QAC5B,uBAAuB;YACnB,oBAAoB;QACxB,oBAAoB;OACrB;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,kBAAkB;QAClB,mBAAmB;OACpB;;AAEP;QACQ,eAAe;OAChB;;AAEP;QACQ,YAAY;OACb;;AAEP;QACQ,eAAe;OAChB;;AAEP;QACQ,YAAY;OACb;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;OACpB;;AAEP;EACE,0BAA0B;EAC1B,wBAAwB;EACxB,4BAA4B;CAC7B;;AAED;EACE,mBAAmB;EACnB,oBAAoB;EACpB,sBAAsB;EACtB,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,aAAa;CACd;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,oBAAoB;CACrB;;AAED;IACI,gBAAgB;IAChB,UAAU;IACV,WAAW;IACX,oBAAoB;IACpB,2BAA2B;OACxB,wBAAwB;IAC3B,iBAAiB;GAClB;;AAEH;IACI,gBAAgB;GACjB;;AAEH;EACE,uBAAuB;EACvB,YAAY;EACZ,aAAa;CACd;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,yBAAyB;CAC1B;;AAED;IACI,aAAa;IACb,cAAc;IACd,gBAAgB;GACjB;;AAEH;IACI,eAAe;GAChB;;AAEH;IACI,gBAAgB;GACjB;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;EACpB,sBAAsB;EACtB,uBAAuB;CACxB;;AAED;IACI,gBAAgB;IAChB,kBAAkB;IAClB,iBAAiB;IACjB,eAAe;IACf,oBAAoB;IACpB,oBAAoB;GACrB;;AAEH;IACI,cAAc;IACd,sCAAsC;IACtC,UAAU;IACV,YAAY;GACb;;AAEH;MACM,aAAa;MACb,aAAa;MACb,cAAc;MACd,cAAc;KACf;;AAEL;MACM,gBAAgB;KACjB;;AAEL;IACI,cAAc;GACf;;AAEH;EACE,gBAAgB;EAChB,OAAO;EACP,QAAQ;EACR,YAAY;EACZ,cAAc;EACd,qCAAqC;EACrC,WAAW;CACZ;;AAED;EACE,cAAc;EACd,YAAY;EACZ,iBAAiB;EACjB,cAAc;EACd,uBAAuB;EACvB,mBAAmB;EACnB,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,cAAc;CACf;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,qBAAqB;IACrB,cAAc;IACd,0BAA0B;GAC3B;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,0BAA0B;IAC1B,mBAAmB;IACnB,gCAAgC;GACjC;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,0BAA0B;IAC1B,mBAAmB;IACnB,+BAA+B;GAChC;;EAED;IACE,mBAAmB;IACnB,0BAA0B;IAC1B,UAAU;GACX;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,0BAA0B;IAC1B,qBAAqB;GACtB;;EAED;IACE,6BAA6B;IAC7B,eAAe;IACf,qDAAqD;YAC7C,6CAA6C;IACrD,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,iBAAiB;IACjB,uBAAuB;GACxB;;EAED;IACE,cAAc;IACd,aAAa;IACb,cAAc;GACf;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,sCAAsC;IACtC,sBAAsB;IACtB,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,wEAAwE;IACxE,mEAAmE;IACnE,gEAAgE;IAChE,qCAAqC;IACrC,8BAA8B;IAC9B,eAAe;IACf,sBAAsB;GACvB;;EAED;IACE,qBAAqB;IACrB,kBAAkB;IAClB,uBAAuB;GACxB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;IACb,yCAAyC;GAC1C;;EAED;IACE,eAAe;GAChB;;EAED;IACE,eAAe;IACf,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,mBAAmB;IACnB,YAAY;IACZ,SAAS;IACT,mBAAmB;GACpB;;IAEC;MACE,kBAAkB;MAClB,4BAA4B;MAC5B,mBAAmB;MACnB,UAAU;MACV,UAAU;MACV,gBAAgB;KACjB;;EAEH;IACE,eAAe;IACf,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,mBAAmB;IACnB,aAAa;IACb,SAAS;IACT,mBAAmB;GACpB;;IAEC;MACE,kBAAkB;MAClB,4BAA4B;MAC5B,mBAAmB;MACnB,UAAU;MACV,UAAU;MACV,gBAAgB;KACjB;;EAEH;IACE,iBAAiB;GAClB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,qBAAqB;IACrB,cAAc;GACf;;EAED;IACE,qBAAqB;IACrB,mDAAmD;IACnD,0BAA0B;GAC3B;;IAEC;MACE,kBAAkB;MAClB,WAAW;MACX,sBAAsB;KACvB;;MAEC;QACE,cAAc;QACd,iBAAiB;OAClB;;IAEH;MACE,2BAA2B;MAC3B,aAAa;MACb,mBAAmB;MACnB,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,aAAa;KACd;;EAEH;IACE,gBAAgB;GACjB;;IAEC;MACE,kBAAkB;MAClB,kBAAkB;MAClB,oBAAoB;KACrB;;IAED;MACE,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,+BAA+B;MAC/B,iBAAiB;MACjB,aAAa;MACb,mBAAmB;KACpB;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,iBAAiB;QACjB,kBAAkB;OACnB;IACH;MACE,gBAAgB;MAChB,kBAAkB;KACnB;;EAEH;IACE,6BAA6B;GAC9B;;IAEC;MACE,kBAAkB;MAClB,oBAAoB;MACpB,qBAAqB;KACtB;;IAED;MACE,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,sBAAsB;MACtB,+BAA+B;MAC/B,UAAU;MACV,mBAAmB;KACpB;;MAEC;QACE,aAAa;QACb,aAAa;OACd;;EAEL;IACE,sBAAsB;IACtB,sCAAsC;GACvC;;IAEC;MACE,YAAY;MACZ,iBAAiB;KAClB;;EAEH;IACE,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,eAAe;GAChB;;EAED;IACE,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;IACd,eAAe;GAChB;;EAED;IACE,aAAa;IACb,cAAc;IACd,UAAU;GACX;;EAED;IACE,qBAAqB;IACrB,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;IACb,mBAAmB;IACnB,0BAA0B;IAC1B,cAAc;GACf;;EAED;IACE,uBAAuB;GACxB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;;;MAGI;IACF;MACE,gBAAgB;MAChB,wBAAwB;KACzB;;EAEH;IACE,YAAY;IACZ,gBAAgB;IAChB,kBAAkB;IAClB,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,aAAa;IACb,oBAAoB;IACpB,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;IACjB,iBAAiB;GAClB;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAiCI;;EAEJ;IACE,cAAc;IACd,mBAAmB;IACnB,WAAW;IACX,SAAS;IACT,UAAU;GACX;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,UAAU;IACV,UAAU;IACV,iCAAiC;QAC7B,6BAA6B;YACzB,yBAAyB;IACjC,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,WAAW;GACZ;CACF;;AAED;EACE,eAAe;CAChB;;AAED;EACE,aAAa;EACb,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,yBAAyB;EACzB,sBAAsB;EACtB,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;CAC5B;;AAED;EACE;IACE,gBAAgB;IAChB,mBAAmB;GACpB;CACF","file":"Pricing.scss","sourcesContent":[".pricingHeader {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.darkBackground {\n  background-color: #25282b !important;\n}\n\n.price_title {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.price_content {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.modulesContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 72px;\n}\n\n.modulesHeader {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.modulesList {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(5, 1fr);\n  gap: 44px;\n  margin-top: 24px;\n}\n\n.module {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.imageWrapper {\n  width: 52px;\n  height: 52px;\n  border-radius: 26px;\n  -webkit-box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-bottom: 12px;\n}\n\n.imageWrapper img {\n  width: 32px;\n  height: 32px;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.price_content p {\n  font-size: 16px;\n  opacity: 0.7;\n  line-height: 2;\n  line-height: 32px;\n}\n\n.annual {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  margin-right: 50px;\n  height: auto;\n}\n\n.toggle_btn {\n  margin-right: 50px;\n}\n\n.annual_text {\n  font-size: 14px;\n  padding-bottom: 4px;\n  line-height: normal;\n  border-bottom: 2px solid #fff;\n}\n\n.annual_offer {\n  opacity: 0.8;\n  color: #f36;\n  margin-bottom: 4px;\n  display: inline-block;\n}\n\n.underline {\n  border-bottom: 2px solid black;\n}\n\n.pricing_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.plans {\n  padding: 0 48px 32px 48px;\n  padding: 0 3rem 2rem 3rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.plan {\n  width: 270px;\n  height: 477px;\n  margin: 0 6px;\n  border-radius: 8px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  position: relative;\n}\n\n.plan .innerCard {\n    background-color: #fff;\n    position: absolute;\n    padding: 32px 0;\n    padding: 2rem 0;\n    width: 100%;\n    height: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    z-index: 1;\n  }\n\n.symbol {\n  border-radius: 100%;\n}\n\n.pricing_title {\n  font-size: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  width: 100%;\n  color: #25282b;\n  margin-bottom: 50px;\n}\n\n.toggle_outer {\n  width: 56px;\n  height: 31px;\n  border-radius: 24px;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 3px;\n}\n\n.toggle_on {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.toggle_inner {\n  width: 24.9px;\n  height: 24.9px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n.plan_title_students {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.plan_title p {\n    margin: 0;\n  }\n\n.plan_title_price {\n  font-weight: 600;\n  color: #25282b;\n  font-size: 24px;\n  opacity: 1;\n  display: inline-block;\n  margin-bottom: 8px;\n}\n\n.price {\n  color: #25282b;\n  font-size: 32px;\n  font-weight: 600;\n}\n\n.price p {\n    margin: 32px 0;\n  }\n\n.price_desc {\n  color: #000;\n  opacity: 0.6;\n  line-height: 1.5;\n}\n\n.price_desc p {\n    margin: 0 0 24px 0;\n  }\n\n.plan button {\n  margin-top: 40px;\n  padding: 0% 15%;\n  text-transform: capitalize;\n  font-weight: normal;\n  border-radius: 24px;\n}\n\n.price_contact a {\n  text-decoration: underline;\n  // margin-bottom: 500px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.pricePerStudentHighlight {\n  font-size: 24px;\n  font-weight: 600;\n  background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n  -webkit-text-fill-color: transparent;\n  -webkit-background-clip: text;\n  color: #25282b;\n}\n\n.buyButton {\n  background-color: #f36;\n}\n\n.enterprise {\n  width: 91.5%;\n  margin: auto;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 8px;\n  padding: 42px 200px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 56px;\n}\n\n.enterpriseHeading {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.enterprisecontext {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.contactus {\n  padding: 8px 20px;\n  border-radius: 24px;\n  background-color: #f36;\n  color: #fff;\n  font-size: 14px;\n  line-height: 20px;\n}\n\n/* .faq_container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  color: #25282b;\n  background-color: #f7f7f7;\n}\n\n.faq_title p {\n  font-size: 40px;\n  font-weight: 600;\n}\n\n.expand_all {\n  margin: 0% 10%;\n  align-self: flex-end;\n  font-size: 16px;\n  color: #25282b;\n  cursor: pointer;\n  border-bottom: 1px solid black;\n  width: max-content;\n}\n\n.accordian {\n  width: 80%;\n} */\n\n/* .border {\n  cursor: pointer;\n  border-bottom: 1px solid #c7c7c7;\n  padding: 44px 0 40px 0;\n}\n\n.border:last-child {\n  border-bottom: none;\n} */\n\n/* .question {\n  width: 100%;\n  font-size: 16px;\n  color: #25282b;\n  font-weight: bold;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.answer {\n  display: none;\n  font-size: 14px;\n  line-height: 25.4px;\n  width: 85%;\n} */\n\n/* .show .answer {\n  display: block;\n} */\n\n/* .up {\n  display: none;\n} */\n\n/* .show .up {\n  display: block;\n  transform: rotate(180deg);\n}\n\n.show .down {\n  display: none;\n} */\n\n.leftArrow,\n.rightArrow {\n  display: none;\n}\n\n.circle_bg {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.square_bg {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n  -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n.line_vector_bg {\n  -webkit-transform: rotate(180deg);\n      -ms-transform: rotate(180deg);\n          transform: rotate(180deg);\n  opacity: 0.5;\n  position: absolute;\n  left: 10%;\n}\n\n.triangle_bg {\n  position: absolute;\n  right: 7%;\n}\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.featured .showMedia a {\n  width: 259px;\n  height: 127px;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n    text-align: center;\n    font-size: 40px;\n    line-height: 1.2;\n    color: #25282b;\n    font-weight: 600;\n    margin-bottom: 40px;\n  }\n\n.achievedContainer .achievedRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(4, 1fr);\n    // grid-template-rows: repeat(2, 1fr);\n    gap: 16px;\n    gap: 1rem;\n    margin: auto;\n  }\n\n.achievedContainer .achievedRow .card {\n      width: 270px;\n      height: 202px;\n      font-size: 24px;\n      font-size: 1.5rem;\n      color: rgba(37, 40, 43, 0.6);\n      line-height: 40px;\n      padding: 24px;\n      padding: 1.5rem;\n      border-radius: 0.5rem;\n      -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n              box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n      background-color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.achievedContainer .achievedRow .card .achievedProfile {\n        width: 52px;\n        height: 52px;\n        border-radius: 50%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-pack: center;\n            justify-content: center;\n        -ms-flex-align: center;\n            align-items: center;\n        margin-bottom: 20px;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n        background-color: #fff3eb;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n        background-color: #f7effe;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n        background-color: #ffebf0;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n        background-color: #ebffef;\n      }\n\n.achievedContainer .achievedRow .card .highlight {\n        font-size: 36px;\n        font-weight: bold;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n        color: #00ac26;\n      }\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n        color: #f36;\n      }\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n        color: #8c00fe;\n      }\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n        color: #f60;\n      }\n\n.achievedContainer .achievedRow .card .subText {\n        font-size: 24px;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.featured {\n  background-color: #f7f7f7;\n  padding: 56px 64px 40px;\n  padding: 3.5rem 4rem 2.5rem;\n}\n\n.featured h2 {\n  text-align: center;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n  margin-top: 0;\n  font-size: 40px;\n  font-size: 2.5rem;\n  line-height: 48px;\n  line-height: 3rem;\n}\n\n.featured .showMedia {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 40px;\n  gap: 2.5rem;\n  margin: auto;\n}\n\n.titlebox {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.titlebox h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.titlebox img {\n    cursor: pointer;\n  }\n\n.featured .showMedia a img {\n  background-color: #fff;\n  width: 100%;\n  height: 100%;\n}\n\n.on_news {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-max-content;\n  height: -moz-max-content;\n  height: max-content;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  gap: 12px;\n  margin: 0 auto 56px auto;\n}\n\n.on_news .featuredImage {\n    width: 368px;\n    height: 250px;\n    cursor: pointer;\n  }\n\n.on_news .featuredImage:nth-child(1) {\n    margin-left: 0;\n  }\n\n.on_news .featuredImage:nth-child(3) {\n    margin-right: 0;\n  }\n\n.awardsContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 56px 192px;\n  padding: 3.5rem 12rem;\n  background-color: #fff;\n}\n\n.awardsContainer .awardsTitle {\n    font-size: 40px;\n    font-size: 2.5rem;\n    font-weight: 600;\n    color: #25282b;\n    margin-bottom: 48px;\n    margin-bottom: 3rem;\n  }\n\n.awardsContainer .achievedRow {\n    display: grid;\n    grid-template-columns: repeat(3, 1fr);\n    gap: 88px;\n    gap: 5.5rem;\n  }\n\n.awardsContainer .achievedRow .awardLogo {\n      width: 240px;\n      width: 15rem;\n      height: 208px;\n      height: 13rem;\n    }\n\n.awardsContainer .achievedRow .awardLogo:nth-child(3n + 2) {\n      margin-right: 0;\n    }\n\n.awardsContainer .dots {\n    display: none;\n  }\n\n.popupoverlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.presspopup {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.player {\n  width: 100%;\n  height: 302px;\n}\n\n@media only screen and (max-width: 990px) {\n  .mobilePricingContainer {\n    width: 100%;\n  }\n\n  .modulesContainer {\n    margin: 64px 0;\n  }\n\n  .tabsContainer {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    background-color: #f7f7f7;\n  }\n\n  .annualtab {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-right-radius: 8px;\n  }\n\n  .monthtab {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-left-radius: 8px;\n  }\n\n  .activeTab {\n    // cursor: pointer;\n    background-color: #f7f7f7;\n    border: 0;\n  }\n\n  .mobilePlans {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n    padding-bottom: 12px;\n  }\n\n  .mobilePriceCard {\n    padding: 24px 52px 24px 53px;\n    margin: 12px 0;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    border-radius: 8px;\n    max-width: 328px;\n    background-color: #fff;\n  }\n\n  .presspopup {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .titlebox {\n    margin-bottom: 16px;\n  }\n\n  .titlebox h2 {\n    font-size: 11.5px;\n  }\n\n  .modulesList {\n    grid-template-columns: repeat(3, 1fr);\n    grid-column-gap: 40px;\n    grid-row-gap: 20px;\n  }\n\n  .imageWrapper {\n    width: 40px;\n    height: 40px;\n  }\n\n  .imageWrapper img {\n    width: 24px;\n    height: 24px;\n  }\n\n  .pricePerStudentHighlightMobile {\n    font-size: 20px;\n    font-weight: 600;\n    background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n    -webkit-text-fill-color: transparent;\n    -webkit-background-clip: text;\n    color: #25282b;\n    opacity: 1 !important;\n  }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n    background-color: #fff;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n    border: 1px solid rgba(37, 40, 43, 0.16);\n  }\n\n  .displayClients .clientsWrapper .client.active {\n    display: block;\n  }\n\n  .leftArrow {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    left: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .leftArrow span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .rightArrow {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    right: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .rightArrow span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .displayClients .clientsWrapper span {\n    max-width: 400px;\n  }\n\n  .dots {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n    margin-bottom: 24px;\n  }\n\n  .displayClients .clientsWrapper .dots {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .achievedContainer {\n    padding: 1.5rem 1rem;\n    background-image: url('/images/home/Confetti.svg');\n    background-color: #f7f7f7;\n  }\n\n    .achievedContainer .achievedHeading {\n      font-size: 1.5rem;\n      padding: 0;\n      margin-bottom: 1.5rem;\n    }\n\n      .achievedContainer .achievedHeading span::after {\n        content: '\\a';\n        white-space: pre;\n      }\n\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr;\n      gap: 0.75rem;\n      max-width: 20.5rem;\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: auto;\n    }\n\n  .awardsContainer {\n    padding: 1.5rem;\n  }\n\n    .awardsContainer .awardsTitle {\n      font-size: 1.5rem;\n      padding-bottom: 0;\n      margin-bottom: 2rem;\n    }\n\n    .awardsContainer .achievedRow {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      grid-template-columns: 1fr 1fr;\n      gap: 1.5rem 1rem;\n      margin: auto;\n      position: relative;\n    }\n\n      .awardsContainer .achievedRow .awardLogo {\n        width: 100%;\n        height: 100%;\n        max-width: 15rem;\n        max-height: 13rem;\n      }\n    .price_contact a {\n      font-size: 14px;\n      line-height: 21px;\n    }\n\n  .featured {\n    padding: 1.5rem 1rem 1.25rem;\n  }\n\n    .featured h2 {\n      font-size: 1.5rem;\n      line-height: normal;\n      margin: 0 0 1.5rem 0;\n    }\n\n    .featured .showMedia {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: 0 auto 0 auto;\n      grid-template-columns: 1fr 1fr;\n      gap: 1rem;\n      position: relative;\n    }\n\n      .featured .showMedia a {\n        width: 140px;\n        height: 72px;\n      }\n\n  .on_news {\n    margin-bottom: 1.5rem;\n    grid-template-columns: repeat(1, 1fr);\n  }\n\n    .on_news .featuredImage {\n      width: 100%;\n      max-width: 340px;\n    }\n\n  .pricingHeader {\n    padding: 24px 0 0;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .price_title {\n    font-size: 32px;\n  }\n\n  .price_content {\n    width: 100%;\n    margin: 16px 0 0;\n    max-width: 90%;\n  }\n\n  .toggle_btn {\n    margin-left: -20px;\n    margin-right: 20px;\n  }\n\n  .toggle_outer {\n    width: 40px;\n    height: 22px;\n  }\n\n  .toggle_inner {\n    width: 17.8px;\n    height: 17.6px;\n  }\n\n  .plan {\n    width: 270px;\n    display: none;\n    margin: 0;\n  }\n\n  .active {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .dot {\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background-color: #e5e5e5;\n    margin: 0 5px;\n  }\n\n  .dotactive {\n    background-color: #f36;\n  }\n\n  .plan_title_students {\n    font-size: 16px;\n    line-height: 32px;\n  }\n\n  .plan_title p {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  /* .question {\n    font-size: 14px;\n    font-weight: 600;\n  } */\n    .price p {\n      font-size: 32px;\n      letter-spacing: -0.96px;\n    }\n\n  .price_desc {\n    width: 101%;\n    font-size: 14px;\n    line-height: 21px;\n    margin-top: 24px;\n    opacity: 0.8;\n  }\n\n  .buyButton {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    width: 174px;\n    border-radius: 24px;\n    color: #fff;\n    padding: 10px 16px;\n    font-size: 16px;\n    font-weight: 600;\n    margin-top: 28px;\n  }\n\n  /* .faq_container {\n    padding: 24px;\n\n    .faq_title p {\n      font-size: 32px;\n      margin-top: 0;\n      margin-bottom: 24px;\n    }\n\n    .expand_all {\n      font-size: 14px;\n      margin: 0 auto 4px auto;\n    }\n\n    .accordian {\n      width: 100%;\n\n      .border {\n        padding: 16px 0;\n\n        .answer {\n          width: 100%;\n\n          p {\n            margin-top: 8px;\n            margin-bottom: 0;\n            line-height: 18px;\n            opacity: 0.7;\n            font-size: 12px;\n          }\n        }\n      }\n    }\n  } */\n\n  .circle_bg {\n    display: none;\n    position: absolute;\n    width: 12%;\n    top: 25%;\n    left: 95%;\n  }\n\n  .square_bg {\n    display: none;\n    position: absolute;\n    top: 120%;\n    left: -3%;\n    -webkit-transform: rotate(45deg);\n        -ms-transform: rotate(45deg);\n            transform: rotate(45deg);\n    width: 10%;\n  }\n\n  .line_vector_bg {\n    display: none;\n  }\n\n  .triangle_bg {\n    display: none;\n    position: absolute;\n    left: -10%;\n    width: 20%;\n    top: 554px;\n  }\n}\n\n.moduleLabel {\n  color: #25282b;\n}\n\n.viewLink {\n  width: 195px;\n  height: 32px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 64.6px 0 112px 0;\n  padding: 0 !important;\n  font-size: 24px;\n  line-height: 1.33;\n  color: #f36;\n  text-align: center;\n  text-decoration: underline;\n}\n\n@media only screen and (max-width: 990px) {\n  .viewLink {\n    font-size: 14px;\n    margin: 0 0 48px 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"pricingHeader": "Pricing-pricingHeader-2MLoH",
	"darkBackground": "Pricing-darkBackground-17ag8",
	"price_title": "Pricing-price_title-3Yqn8",
	"price_content": "Pricing-price_content-5qlsh",
	"modulesContainer": "Pricing-modulesContainer-9h2Yz",
	"modulesHeader": "Pricing-modulesHeader-zfiUY",
	"modulesList": "Pricing-modulesList-3_DuR",
	"module": "Pricing-module-dy7g3",
	"imageWrapper": "Pricing-imageWrapper-2cu95",
	"scrollTop": "Pricing-scrollTop-Aag07",
	"annual": "Pricing-annual-30ie_",
	"toggle_btn": "Pricing-toggle_btn-3t9M8",
	"annual_text": "Pricing-annual_text-c6h-G",
	"annual_offer": "Pricing-annual_offer-2VBwG",
	"underline": "Pricing-underline-VNfR5",
	"pricing_container": "Pricing-pricing_container-2DZ9K",
	"plans": "Pricing-plans-3oyRG",
	"plan": "Pricing-plan-cbyB7",
	"innerCard": "Pricing-innerCard-XD-XR",
	"symbol": "Pricing-symbol-1PZl7",
	"pricing_title": "Pricing-pricing_title-18Vxm",
	"toggle_outer": "Pricing-toggle_outer-TDyU0",
	"toggle_on": "Pricing-toggle_on-KyTo6",
	"toggle_inner": "Pricing-toggle_inner-dxNua",
	"plan_title_students": "Pricing-plan_title_students-ixHtF",
	"plan_title": "Pricing-plan_title-2z_k0",
	"plan_title_price": "Pricing-plan_title_price-2Zx49",
	"price": "Pricing-price-rctgP",
	"price_desc": "Pricing-price_desc-4xVbA",
	"price_contact": "Pricing-price_contact-7uNig",
	"pricePerStudentHighlight": "Pricing-pricePerStudentHighlight-qAWcu",
	"buyButton": "Pricing-buyButton-3g0NI",
	"enterprise": "Pricing-enterprise-1O-mF",
	"enterpriseHeading": "Pricing-enterpriseHeading-2Xm68",
	"enterprisecontext": "Pricing-enterprisecontext-3AD9J",
	"contactus": "Pricing-contactus-XfL9G",
	"leftArrow": "Pricing-leftArrow-3BcpG",
	"rightArrow": "Pricing-rightArrow-1CGiq",
	"circle_bg": "Pricing-circle_bg-vbvtk",
	"square_bg": "Pricing-square_bg-1fWOk",
	"line_vector_bg": "Pricing-line_vector_bg-2Zkss",
	"triangle_bg": "Pricing-triangle_bg-1X7I4",
	"displayClients": "Pricing-displayClients-SxgNR",
	"featured": "Pricing-featured-3vf8L",
	"showMedia": "Pricing-showMedia-6U76q",
	"clientsWrapper": "Pricing-clientsWrapper-65G_i",
	"client": "Pricing-client-14kGt",
	"achievedContainer": "Pricing-achievedContainer-3NBMf",
	"achievedHeading": "Pricing-achievedHeading-hxnwu",
	"achievedRow": "Pricing-achievedRow-29TsL",
	"card": "Pricing-card-3-jMd",
	"achievedProfile": "Pricing-achievedProfile-1jgdI",
	"clients": "Pricing-clients-2mnCX",
	"students": "Pricing-students-1YHpU",
	"tests": "Pricing-tests-3nDUz",
	"questions": "Pricing-questions-3qJ8D",
	"highlight": "Pricing-highlight-3-Tsm",
	"questionsHighlight": "Pricing-questionsHighlight-1HJlR",
	"testsHighlight": "Pricing-testsHighlight-vkapq",
	"studentsHighlight": "Pricing-studentsHighlight-2UuO9",
	"clientsHighlight": "Pricing-clientsHighlight-2hUQE",
	"subText": "Pricing-subText-3PFWz",
	"titlebox": "Pricing-titlebox-1mBcy",
	"on_news": "Pricing-on_news-2bgDh",
	"featuredImage": "Pricing-featuredImage-Joc-9",
	"awardsContainer": "Pricing-awardsContainer-2PyIg",
	"awardsTitle": "Pricing-awardsTitle-2F_p8",
	"awardLogo": "Pricing-awardLogo-2pqvP",
	"dots": "Pricing-dots-2Jt23",
	"popupoverlay": "Pricing-popupoverlay-32yez",
	"presspopup": "Pricing-presspopup-1UCPJ",
	"player": "Pricing-player-1FCsc",
	"mobilePricingContainer": "Pricing-mobilePricingContainer-3w6Qk",
	"tabsContainer": "Pricing-tabsContainer-1-5am",
	"annualtab": "Pricing-annualtab-dyIQZ",
	"monthtab": "Pricing-monthtab-3Jy_s",
	"activeTab": "Pricing-activeTab-3vi1R",
	"mobilePlans": "Pricing-mobilePlans-vbHxh",
	"mobilePriceCard": "Pricing-mobilePriceCard-32uBZ",
	"pricePerStudentHighlightMobile": "Pricing-pricePerStudentHighlightMobile-36iXF",
	"active": "Pricing-active-1fA3n",
	"dot": "Pricing-dot-2Wd1n",
	"dotactive": "Pricing-dotactive-31__-",
	"moduleLabel": "Pricing-moduleLabel-28Van",
	"viewLink": "Pricing-viewLink-1-bHw"
};

/***/ }),

/***/ "./src/components/Modal/Modal.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Modal/Modal.scss");
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Modal_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class Modal extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "handleKeyDownEvent", e => {
      if (e.which === 27) {
        this.props.toggleModal();
      }
    });

    _defineProperty(this, "handleOutsideClick", e => {
      // ignore clicks on the component itself
      if (this.modalContainer && this.modalContainer.contains(e.target)) {
        return;
      }

      if (this.modalContainer) {
        document.removeEventListener('click', this.handleOutsideClick, false);
        document.removeEventListener('touchstart', this.handleOutsideClick, false);
        this.props.toggleModal();
      }
    });
  }

  componentDidMount() {
    if (this.modalContainer) {
      this.modalContainer.focus();
    }

    document.addEventListener('click', this.handleOutsideClick, false);
    document.addEventListener('touchstart', this.handleOutsideClick, false);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `body-overlay ${_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bodyOverlay} ${this.props.overlayClassName}`,
      role: "presentation",
      onKeyDown: this.handleKeyDownEvent,
      ref: ref => {
        this.modalOverlay = ref;
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.modal} ${this.props.modalClassName}`,
      ref: ref => {
        this.modalContainer = ref;
      },
      tabIndex: "-1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, this.props.children));
  }

}

_defineProperty(Modal, "propTypes", {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  modalClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  overlayClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  toggleModal: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Modal));

/***/ }),

/***/ "./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/Pricing.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("react-player");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Modal/Modal.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/get-ranks/pricing/constants.js");
/* harmony import */ var _Pricing_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./src/routes/products/get-ranks/pricing/Pricing.scss");
/* harmony import */ var _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/Pricing.js";









function Pricing() {
  const [activeIndex, setActiveIndex] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0); // const [expandAll, setExpandAll] = useState(false);

  const [toggle, setToggle] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [showScroll, setShowScroll] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [showModal, setShowModal] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [activePress, setActivePress] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const [isMobile, setIsMobile] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const handleScroll = () => {
    if (window.scrollY > 500) {
      setShowScroll(true);
    } else {
      setShowScroll(false);
    }
  };

  const handleResize = () => {
    if (window.innerWidth >= 990) {
      setIsMobile(false);
    } else {
      setIsMobile(true);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    handleScroll();
    handleResize();
    window.addEventListener('scroll', handleScroll);
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleResize);
    };
  });

  const handleScrollTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
    setShowScroll(false);
  };

  const displayScrollToTop = () => showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.scrollTop,
    role: "presentation",
    onClick: () => {
      handleScrollTop();
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/home/scrollTop.svg",
    alt: "scrollTop",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }));

  const toggleHandler = () => {
    setToggle(!toggle);
  };

  const displayModules = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesHeader,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  }, "Every plan includes"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesList,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: this
  }, _constants__WEBPACK_IMPORTED_MODULE_6__["ModulesList"].map(module => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: `/modules/${module.value}`,
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.module,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.imageWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: module.path,
    alt: module.label,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.moduleLabel,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, module.label)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.line_vector_bg,
    src: "images/icons/vector_line.png",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.triangle_bg,
    src: "images/icons/triangle-background.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: this
  }));

  const gotoPrevSlide = () => {
    if (activeIndex === 0) {
      setActiveIndex(3);
    } else setActiveIndex(activeIndex - 1);
  };

  const gotoNextSlide = () => {
    if (activeIndex === 3) setActiveIndex(0);else setActiveIndex(activeIndex + 1);
  };

  const displayMobilePricings = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePricingContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.tabsContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "presentation",
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annualtab} ${!toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.activeTab : null}`,
    onClick: () => {
      setToggle(false);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: this
  }, "Annual Plans"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "presentation",
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.monthtab} ${toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.activeTab : null}`,
    onClick: () => {
      setToggle(true);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 128
    },
    __self: this
  }, "Monthly Plans")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePlans,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 138
    },
    __self: this
  }, (toggle ? _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"] : _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerAnnumDetails"]).map(detail => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePriceCard,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 141
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 142
    },
    __self: this
  }, detail.pack), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_students,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 143
    },
    __self: this
  }, "upto ", detail.noOfStudents, " students"), toggle ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147
    },
    __self: this
  }, "switch to annual pack to save 15%") : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 151
    },
    __self: this
  }, "Per student\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricePerStudentHighlightMobile,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 153
    },
    __self: this
  }, "\u20B945/-"), "\xA0per month"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160
    },
    __self: this
  }, "\u20B9", detail.pricePerMonth))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      width: 'fit-content'
    },
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePriceCard,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 164
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 165
    },
    __self: this
  }, "Enterprise"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 166
    },
    __self: this
  }, "For institutes with more than 1000 students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: "/request-demo",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 169
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 170
    },
    __self: this
  }, "Contact us")))));

  const displayPricings = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricing_container,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 178
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricing_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 179
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 190
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_text} ${toggle ? '' : _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.underline}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 191
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_offer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 192
    },
    __self: this
  }, "(save 15%)"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 193
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 194
    },
    __self: this
  }, "Annual Plans"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_btn,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 197
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "presentation",
    onClick: () => toggleHandler(),
    className: toggle ? `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_outer} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_on} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.darkBackground}` : _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_outer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 198
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_inner,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 207
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_text} ${toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.underline : ''}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 210
    },
    __self: this
  }, "Monthly Plans")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plans,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 214
    },
    __self: this
  }, (toggle ? _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"] : _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerAnnumDetails"]).map((plan, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: plan.pack,
    className: index === activeIndex ? `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.active}` : `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 217
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.innerCard,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 223
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.leftArrow,
    onClick: gotoPrevSlide,
    role: "presentation",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 224
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 229
    },
    __self: this
  }, "\u2039")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.rightArrow,
    onClick: gotoNextSlide,
    role: "presentation",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 231
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 236
    },
    __self: this
  }, "\u203A")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.symbol,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 238
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: `images/icons/${plan.image}`,
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 239
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 241
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 242
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 243
    },
    __self: this
  }, plan.pack), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 244
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_students,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 245
    },
    __self: this
  }, "Up to ", plan.noOfStudents, " students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 250
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 251
    },
    __self: this
  }, "\u20B9", plan.pricePerMonth)), !toggle ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 254
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 255
    },
    __self: this
  }, "Per Student"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricePerStudentHighlight,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 256
    },
    __self: this
  }, "\u20B945/-"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 259
    },
    __self: this
  }, "per month")) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 263
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 264
    },
    __self: this
  }, "switch to annual pack to save 15%")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_contact}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 266
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/contact-us",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 267
    },
    __self: this
  }, "Contact Us for a Quote"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 271
    },
    __self: this
  }, "Buy"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dots,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 273
    },
    __self: this
  }, _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"].map((dot, clientindex) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: dot.pack,
    className: clientindex === activeIndex ? `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dot} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dotactive}` : `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dot}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 275
    },
    __self: this
  }))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterprise,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 290
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterpriseHeading,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 291
    },
    __self: this
  }, "Enterprise"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterprisecontext,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 292
    },
    __self: this
  }, "For institutes with more than 1000 students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: "/request-demo",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 295
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.contactus,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 296
    },
    __self: this
  }, "Contact Us"))));

  const displayPricingHeader = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricingHeader,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 303
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.circle_bg,
    src: "images/icons/circle-background.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 304
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.square_bg,
    src: "images/icons/square-background.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 309
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 314
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 315
    },
    __self: this
  }, "Choose your edition."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 316
    },
    __self: this
  }, "Try it free for 7 days.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_content,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 318
    },
    __self: this
  }, "Egnify plans start as low as Rs.45/- per student per month."), displayModules(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: "/pricing/viewDetails",
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.viewLink,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 322
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 323
    },
    __self: this
  }, "View Plan Details")), isMobile ? displayMobilePricings() : displayPricings()); // Pricing
  // const displayPricing = (
  //   <div className={s.pricing_container}>
  //     <div className={s.pricing_title}>
  //       <img
  //         className={s.line_vector_bg}
  //         src="images/icons/vector_line.png"
  //         alt=""
  //       />
  //       <img
  //         className={s.triangle_bg}
  //         src="images/icons/triangle-background.svg"
  //         alt=""
  //       />
  //       <div className={s.annual}>
  //         <div className={`${s.annual_text} ${toggle ? '' : s.underline}`}>
  //           <span className={s.annual_offer}>(save 15%)</span>
  //           <br />
  //           <span>Annual Plans</span>
  //         </div>
  //       </div>
  //       <div className={s.toggle_btn}>
  //         <div
  //           role="presentation"
  //           onClick={() => toggleHandler()}
  //           className={
  //             toggle ? `${s.toggle_outer} ${s.toggle_on}` : s.toggle_outer
  //           }
  //         >
  //           <div className={s.toggle_inner} />
  //         </div>
  //       </div>
  //       <div className={`${s.annual_text} ${toggle ? s.underline : ''}`}>
  //         Monthly Plans
  //       </div>
  //     </div>
  //     <div className={s.plans}>
  //       {(toggle ? pricingPerMonthDetails : pricingPerAnnumDetails).map(
  //         (plan, index) => (
  //           <div
  //             key={plan.pack}
  //             className={
  //               index === activeIndex ? `${s.plan} ${s.active}` : `${s.plan}`
  //             }
  //           >
  //             <div className={s.innerCard}>
  //               <div
  //                 className={s.leftArrow}
  //                 onClick={gotoPrevSlide}
  //                 role="presentation"
  //               >
  //                 <span>&#8249;</span>
  //               </div>
  //               <div
  //                 className={s.rightArrow}
  //                 onClick={gotoNextSlide}
  //                 role="presentation"
  //               >
  //                 <span>&#8250;</span>
  //               </div>
  //               <div className={s.symbol}>
  //                 <img src={`images/icons/${plan.image}`} alt="" />
  //               </div>
  //               <div className={s.plan_title}>
  //                 <p>
  //                   <span className={s.plan_title_price}>{plan.pack}</span>
  //                   <br />
  //                   <span className={s.plan_title_students}>
  //                     Up to {plan.noOfStudents} students
  //                   </span>
  //                 </p>
  //               </div>
  //               <div className={s.price}>
  //                 <p>{plan.pricePerMonth}</p>
  //               </div>
  //               <div className={s.price_desc}>
  //                 <p>
  //                   Per {toggle ? 'month' : 'year'}, billed
  //                   <br /> {toggle ? 'month' : 'year'} in Rupees
  //                 </p>
  //               </div>
  //               <div className={`${s.price_contact}`}>
  //                 <a href="/contact-us">Contact Us for a Quote</a>
  //               </div>
  //               <button>Buy</button>
  //
  //               <div className={s.dots}>
  //                 {pricingPerMonthDetails.map((dot, clientindex) => (
  //                   <div
  //                     key={dot.pack}
  //                     className={
  //                       clientindex === activeIndex
  //                         ? `${s.dot} ${s.dotactive}`
  //                         : `${s.dot}`
  //                     }
  //                   />
  //                 ))}
  //               </div>
  //             </div>
  //           </div>
  //         ),
  //       )}
  //     </div>
  //   </div>
  // );
  // const onClickHandler = e => {
  //   const parent = e.currentTarget.parentElement;
  //
  //   if (parent.classList.contains(`${s.show}`)) {
  //     parent.classList.remove(`${s.show}`);
  //   } else {
  //     parent.classList.add(`${s.show}`);
  //   }
  // };
  // const expandAppHandler = () => {
  //   setExpandAll(!expandAll);
  // };
  // const pricingFAQ = (
  //   <div className={s.faq_container}>
  //     <div className={s.faq_title}>
  //       <p>Pricing FAQs</p>
  //     </div>
  //     <div
  //       className={s.expand_all}
  //       role="presentation"
  //       onClick={() => {
  //         expandAppHandler();
  //       }}
  //     >
  //       {expandAll ? 'Minimize All' : 'Expand All'}
  //     </div>
  //     <div className={s.accordian}>
  //       {FAQ.map(item => (
  //         <div
  //           key={item.label}
  //           className={`${s.border} ${expandAll ? s.show : ''}`}
  //         >
  //           <div
  //             role="presentation"
  //             onClick={e => {
  //               onClickHandler(e);
  //             }}
  //             className={s.question}
  //           >
  //             <span>{item.label}</span>
  //             <img
  //               className={s.down}
  //               src="images/icons/chevron-down.svg"
  //               alt=""
  //             />
  //             <img
  //               className={s.up}
  //               src="images/icons/chevron-down.svg"
  //               alt=""
  //             />
  //           </div>
  //           <div className={s.answer}>
  //             <p>{item.content}</p>
  //           </div>
  //         </div>
  //       ))}
  //     </div>
  //   </div>
  // );
  // const displayViewLink = () => (
  //  <span className={s.view}>
  //  <Link to="/pricing/viewDetails" className={s.viewLink}>View Plan Details</Link>
  // </span>
  // )

  const displayTrustedBy = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.displayClients,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 504
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 505
    },
    __self: this
  }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 506
    },
    __self: this
  }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clientsWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 509
    },
    __self: this
  }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_CLIENTS_UPDATED"].map(client => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.client,
    key: client.id,
    src: client.icon,
    alt: "clinet",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 511
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 519
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: "/customers",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 520
    },
    __self: this
  }, "click here for more...")));

  const displayAchieved = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 526
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedHeading,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 527
    },
    __self: this
  }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 528
    },
    __self: this
  }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedRow,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 531
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
    style: {
      boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 532
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clients}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 536
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/home/what-achieved/clients.svg",
    alt: "profile",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 537
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clientsHighlight}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 539
    },
    __self: this
  }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 540
    },
    __self: this
  }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
    style: {
      boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 542
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.students}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 546
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/home/what-achieved/students.svg",
    alt: "students",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 547
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.studentsHighlight}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 549
    },
    __self: this
  }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 552
    },
    __self: this
  }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card}`,
    style: {
      boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 554
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.tests}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 558
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/home/what-achieved/tests.svg",
    alt: "tests",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 559
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.testsHighlight}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 561
    },
    __self: this
  }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 564
    },
    __self: this
  }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
    style: {
      boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 566
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.questions}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 570
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/home/what-achieved/questions.svg",
    alt: "questions",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 571
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight} ${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.questionsHighlight}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 576
    },
    __self: this
  }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.subText,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 579
    },
    __self: this
  }, "Questions Solved"))));

  const openModal = index => {
    setShowModal(true);
    setActivePress(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"][index]);
  };

  const toggleModal = () => {
    setShowModal(false);
  };

  const displayFeaturedIn = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.featured,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 595
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 596
    },
    __self: this
  }, "We've got featured in"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.on_news,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 597
    },
    __self: this
  }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"].map((item, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "presentation",
    onClick: () => openModal(index),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 599
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: item.icon,
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.featuredImage,
    alt: item.title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 600
    },
    __self: this
  })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.showMedia,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 604
    },
    __self: this
  }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_MEDIA"].map(media => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.active,
    href: media.url,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 606
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: media.icon,
    alt: media.name,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 607
    },
    __self: this
  })))));

  const displayAwards = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardsContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 615
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardsTitle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 616
    },
    __self: this
  }, "Awards"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedRow}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 617
    },
    __self: this
  }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_AWARDS"].map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: item.icon1,
    alt: item.content,
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardLogo,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 619
    },
    __self: this
  }))));

  const displayModal = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Modal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    modalClassName: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.presspopup,
    overlayClassName: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.popupoverlay,
    toggleModal: toggleModal,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 626
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.titlebox,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 631
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 632
    },
    __self: this
  }, activePress.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/images/icons/close.svg",
    onClick: toggleModal,
    alt: "close",
    role: "presentation",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 633
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_2___default.a, {
    playing: false,
    url: activePress.url,
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.player,
    width: "100%",
    height: "80%",
    controls: true,
    loop: true,
    playsinline: true,
    pip: false,
    config: {
      youtube: {
        playerVars: {
          showinfo: 1
        }
      }
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 641
    },
    __self: this
  }));

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 661
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 662
    },
    __self: this
  }, displayPricingHeader), displayTrustedBy(), displayAchieved(), displayFeaturedIn(), displayAwards(), displayScrollToTop(), showModal ? displayModal() : null);
}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a)(Pricing));

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/Pricing.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/pricing/Pricing.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAQ", function() { return FAQ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pricingPerAnnumDetails", function() { return pricingPerAnnumDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pricingPerMonthDetails", function() { return pricingPerMonthDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModulesList", function() { return ModulesList; });
const FAQ = [{
  label: 'What is GetRanks Learning Platform?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What do I get with a Rise subscription?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How much does a subscription cost?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Do you offer monthly subscriptions?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Is there a free trial period?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Do you offer discounts?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How can I get an even bigger discount?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I buy a subscription outside the India?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I buy a subscription on behalf of someone else?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How do I purchase a tax-exempt subscription?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I cancel my subscription at any time?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What is your refund policy?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What’s the difference between user roles?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What user roles count towards my plan’s user limit?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I upgrade my plan as needed?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}];
const pricingPerAnnumDetails = [{
  image: 'triangle.svg',
  pack: 'Starter',
  noOfStudents: 100,
  pricePerMonth: '50,000/-'
}, {
  image: 'rhombus.svg',
  pack: 'Growth',
  noOfStudents: 250,
  pricePerMonth: '2,00,000/-'
}, {
  image: 'pentagon.svg',
  pack: 'Midsize',
  noOfStudents: 500,
  pricePerMonth: '3,00,000/-'
}, {
  image: 'hexagon.svg',
  pack: 'Large',
  noOfStudents: 1000,
  pricePerMonth: '4,00,000/-'
}];
const pricingPerMonthDetails = [{
  image: 'triangle.svg',
  pack: 'Starter',
  noOfStudents: 100,
  pricePerMonth: '20,000/-'
}, {
  image: 'rhombus.svg',
  pack: 'Growth',
  noOfStudents: 250,
  pricePerMonth: '50,000/-'
}, {
  image: 'pentagon.svg',
  pack: 'Midsize',
  noOfStudents: 500,
  pricePerMonth: '1,00,000/-'
}, {
  image: 'hexagon.svg',
  pack: 'Large',
  noOfStudents: 1000,
  pricePerMonth: '2,00,000/-'
}];
const ModulesList = [{
  label: 'Live Classes',
  path: '/images/home/New SubMenu Items/Teach/old_Live.svg',
  value: 'liveclasses'
}, {
  label: 'Online Tests',
  path: '/images/home/New SubMenu Items/Test/module_test.svg',
  value: 'tests'
}, {
  label: 'Assignments',
  path: '/images/home/New SubMenu Items/Teach/old_Assignments.svg',
  value: 'assignments'
}, {
  label: 'Doubts',
  path: '/images/home/New SubMenu Items/Teach/old_Doubts.svg',
  value: 'doubts'
}, {
  label: 'Connect',
  path: '/images/home/New SubMenu Items/Connect/new_connect.svg',
  value: 'connect'
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Pricing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/pricing/Pricing.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/index.js";




async function action() {
  return {
    title: ['GetRanks by Egnify: Assessment & Analytics Platform'],
    chunks: ['Pricing'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Pricing__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1ByaWNpbmcuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9Nb2RhbC9Nb2RhbC5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaWNpbmcvUHJpY2luZy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLnNjc3M/ZDgwZSIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nL1ByaWNpbmcuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJpY2luZy9QcmljaW5nLnNjc3M/NTIzMyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nL2NvbnN0YW50cy5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuTW9kYWwtYm9keU92ZXJsYXktM2ZTTjQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjUpO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG59XFxuXFxuLk1vZGFsLW1vZGFsLTlRcHhVIHtcXG4gIG92ZXJmbG93OiBhdXRvO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xcbiAgb3V0bGluZTogbm9uZTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLypcXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEzNjFweCkge1xcbiAgLm1hcmtzTW9kYWwge1xcbiAgICB3aWR0aDogNjAwcHg7XFxuICAgIG1hcmdpbjogLTEzM3B4IDAgMCAtMzAwcHg7XFxuICB9XFxufVxcbiovXFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9Nb2RhbC9Nb2RhbC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsK0JBQStCO0VBQy9CLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGVBQWU7RUFDZixxQ0FBcUM7RUFDckMsY0FBYztFQUNkLG1CQUFtQjtDQUNwQjs7QUFFRDs7Ozs7OztFQU9FXCIsXCJmaWxlXCI6XCJNb2RhbC5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5ib2R5T3ZlcmxheSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4ubW9kYWwge1xcbiAgb3ZlcmZsb3c6IGF1dG87XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4vKlxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTM2MXB4KSB7XFxuICAubWFya3NNb2RhbCB7XFxuICAgIHdpZHRoOiA2MDBweDtcXG4gICAgbWFyZ2luOiAtMTMzcHggMCAwIC0zMDBweDtcXG4gIH1cXG59XFxuKi9cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwiYm9keU92ZXJsYXlcIjogXCJNb2RhbC1ib2R5T3ZlcmxheS0zZlNONFwiLFxuXHRcIm1vZGFsXCI6IFwiTW9kYWwtbW9kYWwtOVFweFVcIlxufTsiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlByaWNpbmctcHJpY2luZ0hlYWRlci0yTUxvSCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDUlO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uUHJpY2luZy1kYXJrQmFja2dyb3VuZC0xN2FnOCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiICFpbXBvcnRhbnQ7XFxufVxcblxcbi5QcmljaW5nLXByaWNlX3RpdGxlLTNZcW44IHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDU2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmctcHJpY2VfY29udGVudC01cWxzaCB7XFxuICB3aWR0aDogNTAlO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbn1cXG5cXG4uUHJpY2luZy1tb2R1bGVzQ29udGFpbmVyLTloMll6IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiA3MnB4O1xcbn1cXG5cXG4uUHJpY2luZy1tb2R1bGVzSGVhZGVyLXpmaVVZIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmctbW9kdWxlc0xpc3QtM19EdVIge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg1LCAxZnIpO1xcbiAgZ2FwOiA0NHB4O1xcbiAgbWFyZ2luLXRvcDogMjRweDtcXG59XFxuXFxuLlByaWNpbmctbW9kdWxlLWR5N2czIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5QcmljaW5nLWltYWdlV3JhcHBlci0yY3U5NSB7XFxuICB3aWR0aDogNTJweDtcXG4gIGhlaWdodDogNTJweDtcXG4gIGJvcmRlci1yYWRpdXM6IDI2cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbn1cXG5cXG4uUHJpY2luZy1pbWFnZVdyYXBwZXItMmN1OTUgaW1nIHtcXG4gIHdpZHRoOiAzMnB4O1xcbiAgaGVpZ2h0OiAzMnB4O1xcbn1cXG5cXG4uUHJpY2luZy1zY3JvbGxUb3AtQWFnMDcge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5QcmljaW5nLXNjcm9sbFRvcC1BYWcwNyBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5QcmljaW5nLXByaWNlX2NvbnRlbnQtNXFsc2ggcCB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBvcGFjaXR5OiAwLjc7XFxuICBsaW5lLWhlaWdodDogMjtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbn1cXG5cXG4uUHJpY2luZy1hbm51YWwtMzBpZV8ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIG1hcmdpbi1yaWdodDogNTBweDtcXG4gIGhlaWdodDogYXV0bztcXG59XFxuXFxuLlByaWNpbmctdG9nZ2xlX2J0bi0zdDlNOCB7XFxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7XFxufVxcblxcbi5QcmljaW5nLWFubnVhbF90ZXh0LWM2aC1HIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XFxuICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNmZmY7XFxufVxcblxcbi5QcmljaW5nLWFubnVhbF9vZmZlci0yVkJ3RyB7XFxuICBvcGFjaXR5OiAwLjg7XFxuICBjb2xvcjogI2YzNjtcXG4gIG1hcmdpbi1ib3R0b206IDRweDtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG59XFxuXFxuLlByaWNpbmctdW5kZXJsaW5lLVZOZlI1IHtcXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCBibGFjaztcXG59XFxuXFxuLlByaWNpbmctcHJpY2luZ19jb250YWluZXItMkRaOUsge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLlByaWNpbmctcGxhbnMtM295Ukcge1xcbiAgcGFkZGluZzogMCA0OHB4IDMycHggNDhweDtcXG4gIHBhZGRpbmc6IDAgM3JlbSAycmVtIDNyZW07XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4uUHJpY2luZy1wbGFuLWNieUI3IHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogNDc3cHg7XFxuICBtYXJnaW46IDAgNnB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uUHJpY2luZy1wbGFuLWNieUI3IC5QcmljaW5nLWlubmVyQ2FyZC1YRC1YUiB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcGFkZGluZzogMzJweCAwO1xcbiAgICBwYWRkaW5nOiAycmVtIDA7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHotaW5kZXg6IDE7XFxuICB9XFxuXFxuLlByaWNpbmctc3ltYm9sLTFQWmw3IHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XFxufVxcblxcbi5QcmljaW5nLXByaWNpbmdfdGl0bGUtMThWeG0ge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xcbn1cXG5cXG4uUHJpY2luZy10b2dnbGVfb3V0ZXItVER5VTAge1xcbiAgd2lkdGg6IDU2cHg7XFxuICBoZWlnaHQ6IDMxcHg7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiAzcHg7XFxufVxcblxcbi5QcmljaW5nLXRvZ2dsZV9vbi1LeVRvNiB7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLlByaWNpbmctdG9nZ2xlX2lubmVyLWR4TnVhIHtcXG4gIHdpZHRoOiAyNC45cHg7XFxuICBoZWlnaHQ6IDI0LjlweDtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uUHJpY2luZy1wbGFuX3RpdGxlX3N0dWRlbnRzLWl4SHRGIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uUHJpY2luZy1wbGFuX3RpdGxlLTJ6X2swIHAge1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuLlByaWNpbmctcGxhbl90aXRsZV9wcmljZS0yWng0OSB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBvcGFjaXR5OiAxO1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uUHJpY2luZy1wcmljZS1yY3RnUCB7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5QcmljaW5nLXByaWNlLXJjdGdQIHAge1xcbiAgICBtYXJnaW46IDMycHggMDtcXG4gIH1cXG5cXG4uUHJpY2luZy1wcmljZV9kZXNjLTR4VmJBIHtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG59XFxuXFxuLlByaWNpbmctcHJpY2VfZGVzYy00eFZiQSBwIHtcXG4gICAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgfVxcblxcbi5QcmljaW5nLXBsYW4tY2J5QjcgYnV0dG9uIHtcXG4gIG1hcmdpbi10b3A6IDQwcHg7XFxuICBwYWRkaW5nOiAwJSAxNSU7XFxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbn1cXG5cXG4uUHJpY2luZy1wcmljZV9jb250YWN0LTd1TmlnIGEge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAvLyBtYXJnaW4tYm90dG9tOiA1MDBweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uUHJpY2luZy1wcmljZVBlclN0dWRlbnRIaWdobGlnaHQtcUFXY3Uge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDU5ZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmctYnV5QnV0dG9uLTNnME5JIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5QcmljaW5nLWVudGVycHJpc2UtMU8tbUYge1xcbiAgd2lkdGg6IDkxLjUlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICBwYWRkaW5nOiA0MnB4IDIwMHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDU2cHg7XFxufVxcblxcbi5QcmljaW5nLWVudGVycHJpc2VIZWFkaW5nLTJYbTY4IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmctZW50ZXJwcmlzZWNvbnRleHQtM0FEOUoge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxufVxcblxcbi5QcmljaW5nLWNvbnRhY3R1cy1YZkw5RyB7XFxuICBwYWRkaW5nOiA4cHggMjBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgY29sb3I6ICNmZmY7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG59XFxuXFxuLyogLmZhcV9jb250YWluZXIge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uZmFxX3RpdGxlIHAge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLmV4cGFuZF9hbGwge1xcbiAgbWFyZ2luOiAwJSAxMCU7XFxuICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrO1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbn1cXG5cXG4uYWNjb3JkaWFuIHtcXG4gIHdpZHRoOiA4MCU7XFxufSAqL1xcblxcbi8qIC5ib3JkZXIge1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjN2M3Yzc7XFxuICBwYWRkaW5nOiA0NHB4IDAgNDBweCAwO1xcbn1cXG5cXG4uYm9yZGVyOmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59ICovXFxuXFxuLyogLnF1ZXN0aW9uIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbn1cXG5cXG4uYW5zd2VyIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjUuNHB4O1xcbiAgd2lkdGg6IDg1JTtcXG59ICovXFxuXFxuLyogLnNob3cgLmFuc3dlciB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59ICovXFxuXFxuLyogLnVwIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufSAqL1xcblxcbi8qIC5zaG93IC51cCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XFxufVxcblxcbi5zaG93IC5kb3duIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufSAqL1xcblxcbi5QcmljaW5nLWxlZnRBcnJvdy0zQmNwRyxcXG4uUHJpY2luZy1yaWdodEFycm93LTFDR2lxIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufVxcblxcbi5QcmljaW5nLWNpcmNsZV9iZy12YnZ0ayB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBsZWZ0OiAxMCU7XFxuICB0b3A6IDUlO1xcbn1cXG5cXG4uUHJpY2luZy1zcXVhcmVfYmctMWZXT2sge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDEwJTtcXG4gIHRvcDogNSU7XFxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxufVxcblxcbi5QcmljaW5nLWxpbmVfdmVjdG9yX2JnLTJaa3NzIHtcXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gIG9wYWNpdHk6IDAuNTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGxlZnQ6IDEwJTtcXG59XFxuXFxuLlByaWNpbmctdHJpYW5nbGVfYmctMVg3STQge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDclO1xcbn1cXG5cXG4uUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDQ2NHB4O1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDQwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTE1MnB4O1xcbiAgbWFyZ2luOiAxMnB4IGF1dG8gMDtcXG59XFxuXFxuLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uUHJpY2luZy1mZWF0dXJlZC0zdmY4TCAuUHJpY2luZy1zaG93TWVkaWEtNlU3NnEgYSB7XFxuICB3aWR0aDogMjU5cHg7XFxuICBoZWlnaHQ6IDEyN3B4O1xcbn1cXG5cXG4uUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiBzcGFuIGE6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxufVxcblxcbi5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIC5QcmljaW5nLWNsaWVudC0xNGtHdCB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2ltYWdlcy9ob21lL25ld19jb25mZXR0aS5zdmcnKTtcXG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG59XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRIZWFkaW5nLWh4bnd1IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDQwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIHtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAgIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMTZweDtcXG4gICAgZ2FwOiAxcmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCB7XFxuICAgICAgd2lkdGg6IDI3MHB4O1xcbiAgICAgIGhlaWdodDogMjAycHg7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIGNvbG9yOiByZ2JhKDM3LCA0MCwgNDMsIDAuNik7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICAgICAgcGFkZGluZzogMjRweDtcXG4gICAgICBwYWRkaW5nOiAxLjVyZW07XFxuICAgICAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xcbiAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1hY2hpZXZlZFByb2ZpbGUtMWpnZEkge1xcbiAgICAgICAgd2lkdGg6IDUycHg7XFxuICAgICAgICBoZWlnaHQ6IDUycHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWFjaGlldmVkUHJvZmlsZS0xamdkSS5QcmljaW5nLWNsaWVudHMtMm1uQ1gge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1hY2hpZXZlZFByb2ZpbGUtMWpnZEkuUHJpY2luZy1zdHVkZW50cy0xWUhwVSB7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWFjaGlldmVkUHJvZmlsZS0xamdkSS5QcmljaW5nLXRlc3RzLTNuRFV6IHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmViZjA7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctYWNoaWV2ZWRQcm9maWxlLTFqZ2RJLlByaWNpbmctcXVlc3Rpb25zLTNxSjhEIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctaGlnaGxpZ2h0LTMtVHNtIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctaGlnaGxpZ2h0LTMtVHNtLlByaWNpbmctcXVlc3Rpb25zSGlnaGxpZ2h0LTFISmxSIHtcXG4gICAgICAgIGNvbG9yOiAjMDBhYzI2O1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWhpZ2hsaWdodC0zLVRzbS5QcmljaW5nLXRlc3RzSGlnaGxpZ2h0LXZrYXBxIHtcXG4gICAgICAgIGNvbG9yOiAjZjM2O1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWhpZ2hsaWdodC0zLVRzbS5QcmljaW5nLXN0dWRlbnRzSGlnaGxpZ2h0LTJVdU85IHtcXG4gICAgICAgIGNvbG9yOiAjOGMwMGZlO1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWhpZ2hsaWdodC0zLVRzbS5QcmljaW5nLWNsaWVudHNIaWdobGlnaHQtMmhVUUUge1xcbiAgICAgICAgY29sb3I6ICNmNjA7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctc3ViVGV4dC0zUEZXeiB7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgcGFkZGluZzogMy41cmVtIDRyZW0gMi41cmVtO1xcbn1cXG5cXG4uUHJpY2luZy1mZWF0dXJlZC0zdmY4TCBoMiB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMi41cmVtO1xcbiAgbWFyZ2luLXRvcDogMDtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtc2l6ZTogMi41cmVtO1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogM3JlbTtcXG59XFxuXFxuLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwgLlByaWNpbmctc2hvd01lZGlhLTZVNzZxIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIGdhcDogNDBweDtcXG4gIGdhcDogMi41cmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uUHJpY2luZy10aXRsZWJveC0xbUJjeSB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgbWFyZ2luLWJvdHRvbTogMjhweDtcXG59XFxuXFxuLlByaWNpbmctdGl0bGVib3gtMW1CY3kgaDIge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcXG4gICAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgfVxcblxcbi5QcmljaW5nLXRpdGxlYm94LTFtQmN5IGltZyB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gIH1cXG5cXG4uUHJpY2luZy1mZWF0dXJlZC0zdmY4TCAuUHJpY2luZy1zaG93TWVkaWEtNlU3NnEgYSBpbWcge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uUHJpY2luZy1vbl9uZXdzLTJiZ0RoIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IG1heC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICBnYXA6IDEycHg7XFxuICBtYXJnaW46IDAgYXV0byA1NnB4IGF1dG87XFxufVxcblxcbi5QcmljaW5nLW9uX25ld3MtMmJnRGggLlByaWNpbmctZmVhdHVyZWRJbWFnZS1Kb2MtOSB7XFxuICAgIHdpZHRoOiAzNjhweDtcXG4gICAgaGVpZ2h0OiAyNTBweDtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgfVxcblxcbi5QcmljaW5nLW9uX25ld3MtMmJnRGggLlByaWNpbmctZmVhdHVyZWRJbWFnZS1Kb2MtOTpudGgtY2hpbGQoMSkge1xcbiAgICBtYXJnaW4tbGVmdDogMDtcXG4gIH1cXG5cXG4uUHJpY2luZy1vbl9uZXdzLTJiZ0RoIC5QcmljaW5nLWZlYXR1cmVkSW1hZ2UtSm9jLTk6bnRoLWNoaWxkKDMpIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgfVxcblxcbi5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogNTZweCAxOTJweDtcXG4gIHBhZGRpbmc6IDMuNXJlbSAxMnJlbTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1hd2FyZHNUaXRsZS0yRl9wOCB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgZm9udC1zaXplOiAyLjVyZW07XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBtYXJnaW4tYm90dG9tOiA0OHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xcbiAgfVxcblxcbi5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCB7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgIGdhcDogODhweDtcXG4gICAgZ2FwOiA1LjVyZW07XFxuICB9XFxuXFxuLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWF3YXJkTG9nby0ycHF2UCB7XFxuICAgICAgd2lkdGg6IDI0MHB4O1xcbiAgICAgIHdpZHRoOiAxNXJlbTtcXG4gICAgICBoZWlnaHQ6IDIwOHB4O1xcbiAgICAgIGhlaWdodDogMTNyZW07XFxuICAgIH1cXG5cXG4uUHJpY2luZy1hd2FyZHNDb250YWluZXItMlB5SWcgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctYXdhcmRMb2dvLTJwcXZQOm50aC1jaGlsZCgzbiArIDIpIHtcXG4gICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIH1cXG5cXG4uUHJpY2luZy1hd2FyZHNDb250YWluZXItMlB5SWcgLlByaWNpbmctZG90cy0ySnQyMyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuLlByaWNpbmctcG9wdXBvdmVybGF5LTMyeWV6IHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICB6LWluZGV4OiAzO1xcbn1cXG5cXG4uUHJpY2luZy1wcmVzc3BvcHVwLTFVQ1BKIHtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogNTg1cHg7XFxuICBoZWlnaHQ6IDQwN3B4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcXG4gIHotaW5kZXg6IDQ7XFxufVxcblxcbi5QcmljaW5nLXBsYXllci0xRkNzYyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMzAycHg7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5QcmljaW5nLW1vYmlsZVByaWNpbmdDb250YWluZXItM3c2UWsge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLW1vZHVsZXNDb250YWluZXItOWgyWXoge1xcbiAgICBtYXJnaW46IDY0cHggMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRhYnNDb250YWluZXItMS01YW0ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICB9XFxuXFxuICAuUHJpY2luZy1hbm51YWx0YWItZHlJUVoge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIHdpZHRoOiA1MCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDhweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLW1vbnRodGFiLTNKeV9zIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB3aWR0aDogNTAlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDhweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWFjdGl2ZVRhYi0zdmkxUiB7XFxuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gICAgYm9yZGVyOiAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmctbW9iaWxlUGxhbnMtdmJIeGgge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgIHBhZGRpbmctYm90dG9tOiAxMnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctbW9iaWxlUHJpY2VDYXJkLTMydUJaIHtcXG4gICAgcGFkZGluZzogMjRweCA1MnB4IDI0cHggNTNweDtcXG4gICAgbWFyZ2luOiAxMnB4IDA7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgbWF4LXdpZHRoOiAzMjhweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXByZXNzcG9wdXAtMVVDUEoge1xcbiAgICBwYWRkaW5nOiAxMnB4O1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogMjE3cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy10aXRsZWJveC0xbUJjeSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy10aXRsZWJveC0xbUJjeSBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMTEuNXB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctbW9kdWxlc0xpc3QtM19EdVIge1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xcbiAgICBncmlkLWNvbHVtbi1nYXA6IDQwcHg7XFxuICAgIGdyaWQtcm93LWdhcDogMjBweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWltYWdlV3JhcHBlci0yY3U5NSB7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1pbWFnZVdyYXBwZXItMmN1OTUgaW1nIHtcXG4gICAgd2lkdGg6IDI0cHg7XFxuICAgIGhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodE1vYmlsZS0zNmlYRiB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAgIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudCgzMWRlZywgI2YzNiAyOSUsICNmZjYwMWQgNzAlKTtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDU5ZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xcbiAgfVxcblxcbiAgLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiBoMyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWF4LXdpZHRoOiAxNC44NzVyZW07XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiAuUHJpY2luZy1jbGllbnRzV3JhcHBlci02NUdfaSB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiAuUHJpY2luZy1jbGllbnRzV3JhcHBlci02NUdfaSAuUHJpY2luZy1jbGllbnQtMTRrR3Qge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMTYpO1xcbiAgfVxcblxcbiAgLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIgLlByaWNpbmctY2xpZW50c1dyYXBwZXItNjVHX2kgLlByaWNpbmctY2xpZW50LTE0a0d0LlByaWNpbmctYWN0aXZlLTFmQTNuIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICB9XFxuXFxuICAuUHJpY2luZy1sZWZ0QXJyb3ctM0JjcEcge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDI1cHg7XFxuICAgIGhlaWdodDogMjVweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGxlZnQ6IC00MHB4O1xcbiAgICB0b3A6IDUwJTtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgfVxcblxcbiAgICAuUHJpY2luZy1sZWZ0QXJyb3ctM0JjcEcgc3BhbiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYigzNywgNDAsIDQzLCAwLjUpO1xcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICBsZWZ0OiA3cHg7XFxuICAgICAgdG9wOiAtMnB4O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgfVxcblxcbiAgLlByaWNpbmctcmlnaHRBcnJvdy0xQ0dpcSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB3aWR0aDogMjVweDtcXG4gICAgaGVpZ2h0OiAyNXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcmlnaHQ6IC00MHB4O1xcbiAgICB0b3A6IDUwJTtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgfVxcblxcbiAgICAuUHJpY2luZy1yaWdodEFycm93LTFDR2lxIHNwYW4ge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIGNvbG9yOiByZ2IoMzcsIDQwLCA0MywgMC41KTtcXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgICAgbGVmdDogN3B4O1xcbiAgICAgIHRvcDogLTJweDtcXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIH1cXG5cXG4gIC5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIC5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctZG90cy0ySnQyMyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDU2cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiAuUHJpY2luZy1jbGllbnRzV3JhcHBlci02NUdfaSAuUHJpY2luZy1kb3RzLTJKdDIzIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9Db25mZXR0aS5zdmcnKTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIH1cXG5cXG4gICAgLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRIZWFkaW5nLWh4bnd1IHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcXG4gICAgfVxcblxcbiAgICAgIC5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkSGVhZGluZy1oeG53dSBzcGFuOjphZnRlciB7XFxuICAgICAgICBjb250ZW50OiAnXFxcXEEnO1xcbiAgICAgICAgd2hpdGUtc3BhY2U6IHByZTtcXG4gICAgICB9XFxuXFxuICAgIC5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgICBnYXA6IDAuNzVyZW07XFxuICAgICAgbWF4LXdpZHRoOiAyMC41cmVtO1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgIH1cXG5cXG4gIC5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gIH1cXG5cXG4gICAgLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIC5QcmljaW5nLWF3YXJkc1RpdGxlLTJGX3A4IHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgICB9XFxuXFxuICAgIC5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCB7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgICBnYXA6IDEuNXJlbSAxcmVtO1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIH1cXG5cXG4gICAgICAuUHJpY2luZy1hd2FyZHNDb250YWluZXItMlB5SWcgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctYXdhcmRMb2dvLTJwcXZQIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgbWF4LXdpZHRoOiAxNXJlbTtcXG4gICAgICAgIG1heC1oZWlnaHQ6IDEzcmVtO1xcbiAgICAgIH1cXG4gICAgLlByaWNpbmctcHJpY2VfY29udGFjdC03dU5pZyBhIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICAgIH1cXG5cXG4gIC5QcmljaW5nLWZlYXR1cmVkLTN2ZjhMIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW0gMS4yNXJlbTtcXG4gIH1cXG5cXG4gICAgLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwgaDIge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIDAgMS41cmVtIDA7XFxuICAgIH1cXG5cXG4gICAgLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwgLlByaWNpbmctc2hvd01lZGlhLTZVNzZxIHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMCBhdXRvO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgICBnYXA6IDFyZW07XFxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB9XFxuXFxuICAgICAgLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwgLlByaWNpbmctc2hvd01lZGlhLTZVNzZxIGEge1xcbiAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgaGVpZ2h0OiA3MnB4O1xcbiAgICAgIH1cXG5cXG4gIC5QcmljaW5nLW9uX25ld3MtMmJnRGgge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDEsIDFmcik7XFxuICB9XFxuXFxuICAgIC5QcmljaW5nLW9uX25ld3MtMmJnRGggLlByaWNpbmctZmVhdHVyZWRJbWFnZS1Kb2MtOSB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWF4LXdpZHRoOiAzNDBweDtcXG4gICAgfVxcblxcbiAgLlByaWNpbmctcHJpY2luZ0hlYWRlci0yTUxvSCB7XFxuICAgIHBhZGRpbmc6IDI0cHggMCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmctc2Nyb2xsVG9wLUFhZzA3IHtcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgcmlnaHQ6IDE2cHg7XFxuICAgIGJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXByaWNlX3RpdGxlLTNZcW44IHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcHJpY2VfY29udGVudC01cWxzaCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW46IDE2cHggMCAwO1xcbiAgICBtYXgtd2lkdGg6IDkwJTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRvZ2dsZV9idG4tM3Q5TTgge1xcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XFxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRvZ2dsZV9vdXRlci1URHlVMCB7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDIycHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy10b2dnbGVfaW5uZXItZHhOdWEge1xcbiAgICB3aWR0aDogMTcuOHB4O1xcbiAgICBoZWlnaHQ6IDE3LjZweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXBsYW4tY2J5Qjcge1xcbiAgICB3aWR0aDogMjcwcHg7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWFjdGl2ZS0xZkEzbiB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctZG90LTJXZDFuIHtcXG4gICAgd2lkdGg6IDEwcHg7XFxuICAgIGhlaWdodDogMTBweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTVlNWU1O1xcbiAgICBtYXJnaW46IDAgNXB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctZG90YWN0aXZlLTMxX18tIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXBsYW5fdGl0bGVfc3R1ZGVudHMtaXhIdEYge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcGxhbl90aXRsZS0yel9rMCBwIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC8qIC5xdWVzdGlvbiB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIH0gKi9cXG4gICAgLlByaWNpbmctcHJpY2UtcmN0Z1AgcCB7XFxuICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgIGxldHRlci1zcGFjaW5nOiAtMC45NnB4O1xcbiAgICB9XFxuXFxuICAuUHJpY2luZy1wcmljZV9kZXNjLTR4VmJBIHtcXG4gICAgd2lkdGg6IDEwMSU7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgIG9wYWNpdHk6IDAuODtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWJ1eUJ1dHRvbi0zZzBOSSB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogMTc0cHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgbWFyZ2luLXRvcDogMjhweDtcXG4gIH1cXG5cXG4gIC8qIC5mYXFfY29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMjRweDtcXG5cXG4gICAgLmZhcV90aXRsZSBwIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgICB9XFxuXFxuICAgIC5leHBhbmRfYWxsIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gNHB4IGF1dG87XFxuICAgIH1cXG5cXG4gICAgLmFjY29yZGlhbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuXFxuICAgICAgLmJvcmRlciB7XFxuICAgICAgICBwYWRkaW5nOiAxNnB4IDA7XFxuXFxuICAgICAgICAuYW5zd2VyIHtcXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XFxuXFxuICAgICAgICAgIHAge1xcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNztcXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XFxuICAgICAgICAgIH1cXG4gICAgICAgIH1cXG4gICAgICB9XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC5QcmljaW5nLWNpcmNsZV9iZy12YnZ0ayB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgd2lkdGg6IDEyJTtcXG4gICAgdG9wOiAyNSU7XFxuICAgIGxlZnQ6IDk1JTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXNxdWFyZV9iZy0xZldPayB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiAxMjAlO1xcbiAgICBsZWZ0OiAtMyU7XFxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgIHdpZHRoOiAxMCU7XFxuICB9XFxuXFxuICAuUHJpY2luZy1saW5lX3ZlY3Rvcl9iZy0yWmtzcyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZy10cmlhbmdsZV9iZy0xWDdJNCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgbGVmdDogLTEwJTtcXG4gICAgd2lkdGg6IDIwJTtcXG4gICAgdG9wOiA1NTRweDtcXG4gIH1cXG59XFxuXFxuLlByaWNpbmctbW9kdWxlTGFiZWwtMjhWYW4ge1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5QcmljaW5nLXZpZXdMaW5rLTEtYkh3IHtcXG4gIHdpZHRoOiAxOTVweDtcXG4gIGhlaWdodDogMzJweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbjogNjQuNnB4IDAgMTEycHggMDtcXG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjMzO1xcbiAgY29sb3I6ICNmMzY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLlByaWNpbmctdmlld0xpbmstMS1iSHcge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIG1hcmdpbjogMCAwIDQ4cHggMDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nL1ByaWNpbmcuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UscUNBQXFDO0NBQ3RDOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsb0RBQW9EO1VBQzVDLDRDQUE0QztFQUNwRCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsZ0JBQWdCO0NBQ2pCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixtQkFBbUI7RUFDbkIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsOEJBQThCO0NBQy9COztBQUVEO0VBQ0UsYUFBYTtFQUNiLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsK0JBQStCO0NBQ2hDOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0NBQzVCOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLDBCQUEwQjtFQUMxQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtDQUM3Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2QsY0FBYztFQUNkLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7Q0FDcEI7O0FBRUQ7SUFDSSx1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixtQkFBbUI7SUFDbkIsV0FBVztHQUNaOztBQUVIO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixlQUFlO0VBQ2Ysb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLG1CQUFtQjtNQUNmLDBCQUEwQjtDQUMvQjs7QUFFRDtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0lBQ0ksVUFBVTtHQUNYOztBQUVIO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtDQUNsQjs7QUFFRDtJQUNJLGVBQWU7R0FDaEI7O0FBRUg7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGlCQUFpQjtDQUNsQjs7QUFFRDtJQUNJLG1CQUFtQjtHQUNwQjs7QUFFSDtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsMkJBQTJCO0VBQzNCLG9CQUFvQjtFQUNwQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHdFQUF3RTtFQUN4RSxtRUFBbUU7RUFDbkUsZ0VBQWdFO0VBQ2hFLHFDQUFxQztFQUNyQyw4QkFBOEI7RUFDOUIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixhQUFhO0VBQ2IscURBQXFEO1VBQzdDLDZDQUE2QztFQUNyRCxtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMEJBQTBCO01BQ3RCLDhCQUE4QjtFQUNsQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXlCSTs7QUFFSjs7Ozs7Ozs7SUFRSTs7QUFFSjs7Ozs7Ozs7Ozs7Ozs7O0lBZUk7O0FBRUo7O0lBRUk7O0FBRUo7O0lBRUk7O0FBRUo7Ozs7Ozs7SUFPSTs7QUFFSjs7RUFFRSxjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFFBQVE7Q0FDVDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsUUFBUTtFQUNSLGlDQUFpQztNQUM3Qiw2QkFBNkI7VUFDekIseUJBQXlCO0NBQ2xDOztBQUVEO0VBQ0Usa0NBQWtDO01BQzlCLDhCQUE4QjtVQUMxQiwwQkFBMEI7RUFDbEMsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtDQUNYOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0Usc0JBQXNCO0VBQ3RCLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixZQUFZO0VBQ1osZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHVEQUF1RDtFQUN2RCx5QkFBeUI7RUFDekIsNEJBQTRCO0VBQzVCLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsMEJBQTBCO01BQ3RCLDhCQUE4QjtDQUNuQzs7QUFFRDtJQUNJLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsb0JBQW9CO0dBQ3JCOztBQUVIO0lBQ0ksMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLHNDQUFzQztJQUN0QyxzQ0FBc0M7SUFDdEMsVUFBVTtJQUNWLFVBQVU7SUFDVixhQUFhO0dBQ2Q7O0FBRUg7TUFDTSxhQUFhO01BQ2IsY0FBYztNQUNkLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsNkJBQTZCO01BQzdCLGtCQUFrQjtNQUNsQixjQUFjO01BQ2QsZ0JBQWdCO01BQ2hCLHNCQUFzQjtNQUN0QiwrREFBK0Q7Y0FDdkQsdURBQXVEO01BQy9ELHVCQUF1QjtNQUN2QixxQkFBcUI7TUFDckIsY0FBYztNQUNkLDJCQUEyQjtVQUN2Qix1QkFBdUI7TUFDM0IsdUJBQXVCO1VBQ25CLG9CQUFvQjtLQUN6Qjs7QUFFTDtRQUNRLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLHFCQUFxQjtRQUNyQixjQUFjO1FBQ2Qsc0JBQXNCO1lBQ2xCLHdCQUF3QjtRQUM1Qix1QkFBdUI7WUFDbkIsb0JBQW9CO1FBQ3hCLG9CQUFvQjtPQUNyQjs7QUFFUDtRQUNRLDBCQUEwQjtPQUMzQjs7QUFFUDtRQUNRLDBCQUEwQjtPQUMzQjs7QUFFUDtRQUNRLDBCQUEwQjtPQUMzQjs7QUFFUDtRQUNRLDBCQUEwQjtPQUMzQjs7QUFFUDtRQUNRLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtPQUNwQjs7QUFFUDtRQUNRLGVBQWU7T0FDaEI7O0FBRVA7UUFDUSxZQUFZO09BQ2I7O0FBRVA7UUFDUSxlQUFlO09BQ2hCOztBQUVQO1FBQ1EsWUFBWTtPQUNiOztBQUVQO1FBQ1EsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixtQkFBbUI7T0FDcEI7O0FBRVA7RUFDRSwwQkFBMEI7RUFDMUIsd0JBQXdCO0VBQ3hCLDRCQUE0QjtDQUM3Qjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4Qix1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLG9CQUFvQjtDQUNyQjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsV0FBVztJQUNYLG9CQUFvQjtJQUNwQiwyQkFBMkI7T0FDeEIsd0JBQXdCO0lBQzNCLGlCQUFpQjtHQUNsQjs7QUFFSDtJQUNJLGdCQUFnQjtHQUNqQjs7QUFFSDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsNEJBQTRCO0VBQzVCLHlCQUF5QjtFQUN6QixvQkFBb0I7RUFDcEIsY0FBYztFQUNkLHNDQUFzQztFQUN0QyxVQUFVO0VBQ1YseUJBQXlCO0NBQzFCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLGNBQWM7SUFDZCxnQkFBZ0I7R0FDakI7O0FBRUg7SUFDSSxlQUFlO0dBQ2hCOztBQUVIO0lBQ0ksZ0JBQWdCO0dBQ2pCOztBQUVIO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixvQkFBb0I7RUFDcEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtDQUN4Qjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsb0JBQW9CO0dBQ3JCOztBQUVIO0lBQ0ksY0FBYztJQUNkLHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsWUFBWTtHQUNiOztBQUVIO01BQ00sYUFBYTtNQUNiLGFBQWE7TUFDYixjQUFjO01BQ2QsY0FBYztLQUNmOztBQUVMO01BQ00sZ0JBQWdCO0tBQ2pCOztBQUVMO0lBQ0ksY0FBYztHQUNmOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLE9BQU87RUFDUCxRQUFRO0VBQ1IsWUFBWTtFQUNaLGNBQWM7RUFDZCxxQ0FBcUM7RUFDckMsV0FBVztDQUNaOztBQUVEO0VBQ0UsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsY0FBYztFQUNkLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsV0FBVztDQUNaOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGNBQWM7Q0FDZjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsZUFBZTtHQUNoQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsY0FBYztJQUNkLDBCQUEwQjtHQUMzQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsMEJBQTBCO0lBQzFCLG1CQUFtQjtJQUNuQixnQ0FBZ0M7R0FDakM7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkIsK0JBQStCO0dBQ2hDOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixVQUFVO0dBQ1g7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1Qix1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLDBCQUEwQjtJQUMxQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSw2QkFBNkI7SUFDN0IsZUFBZTtJQUNmLHFEQUFxRDtZQUM3Qyw2Q0FBNkM7SUFDckQsMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLHVCQUF1QjtHQUN4Qjs7RUFFRDtJQUNFLGNBQWM7SUFDZCxhQUFhO0lBQ2IsY0FBYztHQUNmOztFQUVEO0lBQ0Usb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0Usa0JBQWtCO0dBQ25COztFQUVEO0lBQ0Usc0NBQXNDO0lBQ3RDLHNCQUFzQjtJQUN0QixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsd0VBQXdFO0lBQ3hFLG1FQUFtRTtJQUNuRSxnRUFBZ0U7SUFDaEUscUNBQXFDO0lBQ3JDLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2Ysc0JBQXNCO0dBQ3ZCOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQix1QkFBdUI7R0FDeEI7O0VBRUQ7SUFDRSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIscUJBQXFCO0dBQ3RCOztFQUVEO0lBQ0Usb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0NBQXNDO0lBQ3RDLG1DQUFtQztJQUNuQyxVQUFVO0lBQ1YsMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsYUFBYTtJQUNiLHlDQUF5QztHQUMxQzs7RUFFRDtJQUNFLGVBQWU7R0FDaEI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsWUFBWTtJQUNaLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixTQUFTO0lBQ1QsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0Usa0JBQWtCO01BQ2xCLDRCQUE0QjtNQUM1QixtQkFBbUI7TUFDbkIsVUFBVTtNQUNWLFVBQVU7TUFDVixnQkFBZ0I7S0FDakI7O0VBRUg7SUFDRSxlQUFlO0lBQ2YsWUFBWTtJQUNaLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixTQUFTO0lBQ1QsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0Usa0JBQWtCO01BQ2xCLDRCQUE0QjtNQUM1QixtQkFBbUI7TUFDbkIsVUFBVTtNQUNWLFVBQVU7TUFDVixnQkFBZ0I7S0FDakI7O0VBRUg7SUFDRSxpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixpQkFBaUI7SUFDakIsb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7R0FDZjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixtREFBbUQ7SUFDbkQsMEJBQTBCO0dBQzNCOztJQUVDO01BQ0Usa0JBQWtCO01BQ2xCLFdBQVc7TUFDWCxzQkFBc0I7S0FDdkI7O01BRUM7UUFDRSxjQUFjO1FBQ2QsaUJBQWlCO09BQ2xCOztJQUVIO01BQ0UsMkJBQTJCO01BQzNCLGFBQWE7TUFDYixtQkFBbUI7TUFDbkIsMkJBQTJCO01BQzNCLHdCQUF3QjtNQUN4QixtQkFBbUI7TUFDbkIsYUFBYTtLQUNkOztFQUVIO0lBQ0UsZ0JBQWdCO0dBQ2pCOztJQUVDO01BQ0Usa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixvQkFBb0I7S0FDckI7O0lBRUQ7TUFDRSwyQkFBMkI7TUFDM0Isd0JBQXdCO01BQ3hCLG1CQUFtQjtNQUNuQiwrQkFBK0I7TUFDL0IsaUJBQWlCO01BQ2pCLGFBQWE7TUFDYixtQkFBbUI7S0FDcEI7O01BRUM7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLGlCQUFpQjtRQUNqQixrQkFBa0I7T0FDbkI7SUFDSDtNQUNFLGdCQUFnQjtNQUNoQixrQkFBa0I7S0FDbkI7O0VBRUg7SUFDRSw2QkFBNkI7R0FDOUI7O0lBRUM7TUFDRSxrQkFBa0I7TUFDbEIsb0JBQW9CO01BQ3BCLHFCQUFxQjtLQUN0Qjs7SUFFRDtNQUNFLDJCQUEyQjtNQUMzQix3QkFBd0I7TUFDeEIsbUJBQW1CO01BQ25CLHNCQUFzQjtNQUN0QiwrQkFBK0I7TUFDL0IsVUFBVTtNQUNWLG1CQUFtQjtLQUNwQjs7TUFFQztRQUNFLGFBQWE7UUFDYixhQUFhO09BQ2Q7O0VBRUw7SUFDRSxzQkFBc0I7SUFDdEIsc0NBQXNDO0dBQ3ZDOztJQUVDO01BQ0UsWUFBWTtNQUNaLGlCQUFpQjtLQUNsQjs7RUFFSDtJQUNFLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsZUFBZTtHQUNoQjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsY0FBYztJQUNkLFVBQVU7R0FDWDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsY0FBYztHQUNmOztFQUVEO0lBQ0UsdUJBQXVCO0dBQ3hCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7OztNQUdJO0lBQ0Y7TUFDRSxnQkFBZ0I7TUFDaEIsd0JBQXdCO0tBQ3pCOztFQUVIO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGlCQUFpQjtHQUNsQjs7RUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BaUNJOztFQUVKO0lBQ0UsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsU0FBUztJQUNULFVBQVU7R0FDWDs7RUFFRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFVBQVU7SUFDVixpQ0FBaUM7UUFDN0IsNkJBQTZCO1lBQ3pCLHlCQUF5QjtJQUNqQyxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxXQUFXO0lBQ1gsV0FBVztHQUNaO0NBQ0Y7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4Qix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQiwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRTtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7R0FDcEI7Q0FDRlwiLFwiZmlsZVwiOlwiUHJpY2luZy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5wcmljaW5nSGVhZGVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNSU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5kYXJrQmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiICFpbXBvcnRhbnQ7XFxufVxcblxcbi5wcmljZV90aXRsZSB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgZm9udC1zaXplOiA1NnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5wcmljZV9jb250ZW50IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi5tb2R1bGVzQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiA3MnB4O1xcbn1cXG5cXG4ubW9kdWxlc0hlYWRlciB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5tb2R1bGVzTGlzdCB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDUsIDFmcik7XFxuICBnYXA6IDQ0cHg7XFxuICBtYXJnaW4tdG9wOiAyNHB4O1xcbn1cXG5cXG4ubW9kdWxlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5pbWFnZVdyYXBwZXIge1xcbiAgd2lkdGg6IDUycHg7XFxuICBoZWlnaHQ6IDUycHg7XFxuICBib3JkZXItcmFkaXVzOiAyNnB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLmltYWdlV3JhcHBlciBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICBoZWlnaHQ6IDMycHg7XFxufVxcblxcbi5zY3JvbGxUb3Age1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5zY3JvbGxUb3AgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4ucHJpY2VfY29udGVudCBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIG9wYWNpdHk6IDAuNztcXG4gIGxpbmUtaGVpZ2h0OiAyO1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxufVxcblxcbi5hbm51YWwge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIG1hcmdpbi1yaWdodDogNTBweDtcXG4gIGhlaWdodDogYXV0bztcXG59XFxuXFxuLnRvZ2dsZV9idG4ge1xcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xcbn1cXG5cXG4uYW5udWFsX3RleHQge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDRweDtcXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2ZmZjtcXG59XFxuXFxuLmFubnVhbF9vZmZlciB7XFxuICBvcGFjaXR5OiAwLjg7XFxuICBjb2xvcjogI2YzNjtcXG4gIG1hcmdpbi1ib3R0b206IDRweDtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG59XFxuXFxuLnVuZGVybGluZSB7XFxuICBib3JkZXItYm90dG9tOiAycHggc29saWQgYmxhY2s7XFxufVxcblxcbi5wcmljaW5nX2NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG4ucGxhbnMge1xcbiAgcGFkZGluZzogMCA0OHB4IDMycHggNDhweDtcXG4gIHBhZGRpbmc6IDAgM3JlbSAycmVtIDNyZW07XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4ucGxhbiB7XFxuICB3aWR0aDogMjcwcHg7XFxuICBoZWlnaHQ6IDQ3N3B4O1xcbiAgbWFyZ2luOiAwIDZweDtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLnBsYW4gLmlubmVyQ2FyZCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcGFkZGluZzogMzJweCAwO1xcbiAgICBwYWRkaW5nOiAycmVtIDA7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHotaW5kZXg6IDE7XFxuICB9XFxuXFxuLnN5bWJvbCB7XFxuICBib3JkZXItcmFkaXVzOiAxMDAlO1xcbn1cXG5cXG4ucHJpY2luZ190aXRsZSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XFxufVxcblxcbi50b2dnbGVfb3V0ZXIge1xcbiAgd2lkdGg6IDU2cHg7XFxuICBoZWlnaHQ6IDMxcHg7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiAzcHg7XFxufVxcblxcbi50b2dnbGVfb24ge1xcbiAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi50b2dnbGVfaW5uZXIge1xcbiAgd2lkdGg6IDI0LjlweDtcXG4gIGhlaWdodDogMjQuOXB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5wbGFuX3RpdGxlX3N0dWRlbnRzIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4ucGxhbl90aXRsZSBwIHtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbi5wbGFuX3RpdGxlX3ByaWNlIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIG9wYWNpdHk6IDE7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5wcmljZSB7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5wcmljZSBwIHtcXG4gICAgbWFyZ2luOiAzMnB4IDA7XFxuICB9XFxuXFxuLnByaWNlX2Rlc2Mge1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbn1cXG5cXG4ucHJpY2VfZGVzYyBwIHtcXG4gICAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgfVxcblxcbi5wbGFuIGJ1dHRvbiB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgcGFkZGluZzogMCUgMTUlO1xcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcXG59XFxuXFxuLnByaWNlX2NvbnRhY3QgYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIC8vIG1hcmdpbi1ib3R0b206IDUwMHB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5wcmljZVBlclN0dWRlbnRIaWdobGlnaHQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDU5ZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLmJ1eUJ1dHRvbiB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uZW50ZXJwcmlzZSB7XFxuICB3aWR0aDogOTEuNSU7XFxuICBtYXJnaW46IGF1dG87XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIHBhZGRpbmc6IDQycHggMjAwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNTZweDtcXG59XFxuXFxuLmVudGVycHJpc2VIZWFkaW5nIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLmVudGVycHJpc2Vjb250ZXh0IHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbn1cXG5cXG4uY29udGFjdHVzIHtcXG4gIHBhZGRpbmc6IDhweCAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4vKiAuZmFxX2NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5mYXFfdGl0bGUgcCB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uZXhwYW5kX2FsbCB7XFxuICBtYXJnaW46IDAlIDEwJTtcXG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XFxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XFxufVxcblxcbi5hY2NvcmRpYW4ge1xcbiAgd2lkdGg6IDgwJTtcXG59ICovXFxuXFxuLyogLmJvcmRlciB7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2M3YzdjNztcXG4gIHBhZGRpbmc6IDQ0cHggMCA0MHB4IDA7XFxufVxcblxcbi5ib3JkZXI6bGFzdC1jaGlsZCB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn0gKi9cXG5cXG4vKiAucXVlc3Rpb24ge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5hbnN3ZXIge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNS40cHg7XFxuICB3aWR0aDogODUlO1xcbn0gKi9cXG5cXG4vKiAuc2hvdyAuYW5zd2VyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn0gKi9cXG5cXG4vKiAudXAge1xcbiAgZGlzcGxheTogbm9uZTtcXG59ICovXFxuXFxuLyogLnNob3cgLnVwIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG59XFxuXFxuLnNob3cgLmRvd24ge1xcbiAgZGlzcGxheTogbm9uZTtcXG59ICovXFxuXFxuLmxlZnRBcnJvdyxcXG4ucmlnaHRBcnJvdyB7XFxuICBkaXNwbGF5OiBub25lO1xcbn1cXG5cXG4uY2lyY2xlX2JnIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGxlZnQ6IDEwJTtcXG4gIHRvcDogNSU7XFxufVxcblxcbi5zcXVhcmVfYmcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDEwJTtcXG4gIHRvcDogNSU7XFxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxufVxcblxcbi5saW5lX3ZlY3Rvcl9iZyB7XFxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XFxuICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XFxuICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XFxuICBvcGFjaXR5OiAwLjU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBsZWZ0OiAxMCU7XFxufVxcblxcbi50cmlhbmdsZV9iZyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICByaWdodDogNyU7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDQ2NHB4O1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDQwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIGgzIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luLXRvcDogMDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uZmVhdHVyZWQgLnNob3dNZWRpYSBhIHtcXG4gIHdpZHRoOiAyNTlweDtcXG4gIGhlaWdodDogMTI3cHg7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyBzcGFuIGE6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxufVxcblxcbi5jbGllbnRzV3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9uZXdfY29uZmV0dGkuc3ZnJyk7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDQwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gICAgLy8gZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxNnB4O1xcbiAgICBnYXA6IDFyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIHtcXG4gICAgICB3aWR0aDogMjcwcHg7XFxuICAgICAgaGVpZ2h0OiAyMDJweDtcXG4gICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICBwYWRkaW5nOiAyNHB4O1xcbiAgICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gICAgICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICAgICAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUge1xcbiAgICAgICAgd2lkdGg6IDUycHg7XFxuICAgICAgICBoZWlnaHQ6IDUycHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUuY2xpZW50cyB7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmM2ViO1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUuc3R1ZGVudHMge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZWZmZTtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnRlc3RzIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmViZjA7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5xdWVzdGlvbnMge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ViZmZlZjtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0IHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5xdWVzdGlvbnNIaWdobGlnaHQge1xcbiAgICAgICAgY29sb3I6ICMwMGFjMjY7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC50ZXN0c0hpZ2hsaWdodCB7XFxuICAgICAgICBjb2xvcjogI2YzNjtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnN0dWRlbnRzSGlnaGxpZ2h0IHtcXG4gICAgICAgIGNvbG9yOiAjOGMwMGZlO1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQuY2xpZW50c0hpZ2hsaWdodCB7XFxuICAgICAgICBjb2xvcjogI2Y2MDtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuc3ViVGV4dCB7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuLmZlYXR1cmVkIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIHBhZGRpbmc6IDMuNXJlbSA0cmVtIDIuNXJlbTtcXG59XFxuXFxuLmZlYXR1cmVkIGgyIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICBtYXJnaW4tYm90dG9tOiAyLjVyZW07XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC1zaXplOiAyLjVyZW07XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiAzcmVtO1xcbn1cXG5cXG4uZmVhdHVyZWQgLnNob3dNZWRpYSB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICBnYXA6IDQwcHg7XFxuICBnYXA6IDIuNXJlbTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLnRpdGxlYm94IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBtYXJnaW4tYm90dG9tOiAyOHB4O1xcbn1cXG5cXG4udGl0bGVib3ggaDIge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcXG4gICAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgfVxcblxcbi50aXRsZWJveCBpbWcge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICB9XFxuXFxuLmZlYXR1cmVkIC5zaG93TWVkaWEgYSBpbWcge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4ub25fbmV3cyB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgaGVpZ2h0OiAtbW96LW1heC1jb250ZW50O1xcbiAgaGVpZ2h0OiBtYXgtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xcbiAgZ2FwOiAxMnB4O1xcbiAgbWFyZ2luOiAwIGF1dG8gNTZweCBhdXRvO1xcbn1cXG5cXG4ub25fbmV3cyAuZmVhdHVyZWRJbWFnZSB7XFxuICAgIHdpZHRoOiAzNjhweDtcXG4gICAgaGVpZ2h0OiAyNTBweDtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgfVxcblxcbi5vbl9uZXdzIC5mZWF0dXJlZEltYWdlOm50aC1jaGlsZCgxKSB7XFxuICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgfVxcblxcbi5vbl9uZXdzIC5mZWF0dXJlZEltYWdlOm50aC1jaGlsZCgzKSB7XFxuICAgIG1hcmdpbi1yaWdodDogMDtcXG4gIH1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA1NnB4IDE5MnB4O1xcbiAgcGFkZGluZzogMy41cmVtIDEycmVtO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLmF3YXJkc0NvbnRhaW5lciAuYXdhcmRzVGl0bGUge1xcbiAgICBmb250LXNpemU6IDQwcHg7XFxuICAgIGZvbnQtc2l6ZTogMi41cmVtO1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgbWFyZ2luLWJvdHRvbTogNDhweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcXG4gIH1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgIGdhcDogODhweDtcXG4gICAgZ2FwOiA1LjVyZW07XFxuICB9XFxuXFxuLmF3YXJkc0NvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmF3YXJkTG9nbyB7XFxuICAgICAgd2lkdGg6IDI0MHB4O1xcbiAgICAgIHdpZHRoOiAxNXJlbTtcXG4gICAgICBoZWlnaHQ6IDIwOHB4O1xcbiAgICAgIGhlaWdodDogMTNyZW07XFxuICAgIH1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuYXdhcmRMb2dvOm50aC1jaGlsZCgzbiArIDIpIHtcXG4gICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIH1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIC5kb3RzIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4ucG9wdXBvdmVybGF5IHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwdmg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICB6LWluZGV4OiAzO1xcbn1cXG5cXG4ucHJlc3Nwb3B1cCB7XFxuICBwYWRkaW5nOiAyNHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDU4NXB4O1xcbiAgaGVpZ2h0OiA0MDdweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3JkZXItcmFkaXVzOiA1cHg7XFxuICB6LWluZGV4OiA0O1xcbn1cXG5cXG4ucGxheWVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAzMDJweDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLm1vYmlsZVByaWNpbmdDb250YWluZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5tb2R1bGVzQ29udGFpbmVyIHtcXG4gICAgbWFyZ2luOiA2NHB4IDA7XFxuICB9XFxuXFxuICAudGFic0NvbnRhaW5lciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIH1cXG5cXG4gIC5hbm51YWx0YWIge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIHdpZHRoOiA1MCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDhweDtcXG4gIH1cXG5cXG4gIC5tb250aHRhYiB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcXG4gICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA4cHg7XFxuICB9XFxuXFxuICAuYWN0aXZlVGFiIHtcXG4gICAgLy8gY3Vyc29yOiBwb2ludGVyO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgICBib3JkZXI6IDA7XFxuICB9XFxuXFxuICAubW9iaWxlUGxhbnMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgIHBhZGRpbmctYm90dG9tOiAxMnB4O1xcbiAgfVxcblxcbiAgLm1vYmlsZVByaWNlQ2FyZCB7XFxuICAgIHBhZGRpbmc6IDI0cHggNTJweCAyNHB4IDUzcHg7XFxuICAgIG1hcmdpbjogMTJweCAwO1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgIG1heC13aWR0aDogMzI4cHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB9XFxuXFxuICAucHJlc3Nwb3B1cCB7XFxuICAgIHBhZGRpbmc6IDEycHg7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiAyMTdweDtcXG4gIH1cXG5cXG4gIC50aXRsZWJveCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAudGl0bGVib3ggaDIge1xcbiAgICBmb250LXNpemU6IDExLjVweDtcXG4gIH1cXG5cXG4gIC5tb2R1bGVzTGlzdCB7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgIGdyaWQtY29sdW1uLWdhcDogNDBweDtcXG4gICAgZ3JpZC1yb3ctZ2FwOiAyMHB4O1xcbiAgfVxcblxcbiAgLmltYWdlV3JhcHBlciB7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuICAuaW1hZ2VXcmFwcGVyIGltZyB7XFxuICAgIHdpZHRoOiAyNHB4O1xcbiAgICBoZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAucHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0TW9iaWxlIHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgzMWRlZywgI2YzNiAyOSUsICNmZjYwMWQgNzAlKTtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNTlkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBvcGFjaXR5OiAxICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMTYpO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfVxcblxcbiAgLmxlZnRBcnJvdyB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB3aWR0aDogMjVweDtcXG4gICAgaGVpZ2h0OiAyNXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgbGVmdDogLTQwcHg7XFxuICAgIHRvcDogNTAlO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB9XFxuXFxuICAgIC5sZWZ0QXJyb3cgc3BhbiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYigzNywgNDAsIDQzLCAwLjUpO1xcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICBsZWZ0OiA3cHg7XFxuICAgICAgdG9wOiAtMnB4O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgfVxcblxcbiAgLnJpZ2h0QXJyb3cge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDI1cHg7XFxuICAgIGhlaWdodDogMjVweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHJpZ2h0OiAtNDBweDtcXG4gICAgdG9wOiA1MCU7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIH1cXG5cXG4gICAgLnJpZ2h0QXJyb3cgc3BhbiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYigzNywgNDAsIDQzLCAwLjUpO1xcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICBsZWZ0OiA3cHg7XFxuICAgICAgdG9wOiAtMnB4O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciBzcGFuIHtcXG4gICAgbWF4LXdpZHRoOiA0MDBweDtcXG4gIH1cXG5cXG4gIC5kb3RzIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNTZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmRvdHMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gIH1cXG5cXG4gIC5hY2hpZXZlZENvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9Db25mZXR0aS5zdmcnKTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIH1cXG5cXG4gICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZEhlYWRpbmcge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIHBhZGRpbmc6IDA7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xcbiAgICB9XFxuXFxuICAgICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZEhlYWRpbmcgc3Bhbjo6YWZ0ZXIge1xcbiAgICAgICAgY29udGVudDogJ1xcXFxhJztcXG4gICAgICAgIHdoaXRlLXNwYWNlOiBwcmU7XFxuICAgICAgfVxcblxcbiAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgICBnYXA6IDAuNzVyZW07XFxuICAgICAgbWF4LXdpZHRoOiAyMC41cmVtO1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgIH1cXG5cXG4gIC5hd2FyZHNDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAxLjVyZW07XFxuICB9XFxuXFxuICAgIC5hd2FyZHNDb250YWluZXIgLmF3YXJkc1RpdGxlIHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgICB9XFxuXFxuICAgIC5hd2FyZHNDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgICAgIGdhcDogMS41cmVtIDFyZW07XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgfVxcblxcbiAgICAgIC5hd2FyZHNDb250YWluZXIgLmFjaGlldmVkUm93IC5hd2FyZExvZ28ge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDE1cmVtO1xcbiAgICAgICAgbWF4LWhlaWdodDogMTNyZW07XFxuICAgICAgfVxcbiAgICAucHJpY2VfY29udGFjdCBhIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICAgIH1cXG5cXG4gIC5mZWF0dXJlZCB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtIDEuMjVyZW07XFxuICB9XFxuXFxuICAgIC5mZWF0dXJlZCBoMiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICBtYXJnaW46IDAgMCAxLjVyZW0gMDtcXG4gICAgfVxcblxcbiAgICAuZmVhdHVyZWQgLnNob3dNZWRpYSB7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIG1hcmdpbjogMCBhdXRvIDAgYXV0bztcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAxZnI7XFxuICAgICAgZ2FwOiAxcmVtO1xcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgfVxcblxcbiAgICAgIC5mZWF0dXJlZCAuc2hvd01lZGlhIGEge1xcbiAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgaGVpZ2h0OiA3MnB4O1xcbiAgICAgIH1cXG5cXG4gIC5vbl9uZXdzIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxLCAxZnIpO1xcbiAgfVxcblxcbiAgICAub25fbmV3cyAuZmVhdHVyZWRJbWFnZSB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWF4LXdpZHRoOiAzNDBweDtcXG4gICAgfVxcblxcbiAgLnByaWNpbmdIZWFkZXIge1xcbiAgICBwYWRkaW5nOiAyNHB4IDAgMDtcXG4gIH1cXG5cXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLnByaWNlX3RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLnByaWNlX2NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMDtcXG4gICAgbWF4LXdpZHRoOiA5MCU7XFxuICB9XFxuXFxuICAudG9nZ2xlX2J0biB7XFxuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbiAgLnRvZ2dsZV9vdXRlciB7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDIycHg7XFxuICB9XFxuXFxuICAudG9nZ2xlX2lubmVyIHtcXG4gICAgd2lkdGg6IDE3LjhweDtcXG4gICAgaGVpZ2h0OiAxNy42cHg7XFxuICB9XFxuXFxuICAucGxhbiB7XFxuICAgIHdpZHRoOiAyNzBweDtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgfVxcblxcbiAgLmRvdCB7XFxuICAgIHdpZHRoOiAxMHB4O1xcbiAgICBoZWlnaHQ6IDEwcHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U1ZTVlNTtcXG4gICAgbWFyZ2luOiAwIDVweDtcXG4gIH1cXG5cXG4gIC5kb3RhY3RpdmUge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgfVxcblxcbiAgLnBsYW5fdGl0bGVfc3R1ZGVudHMge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgfVxcblxcbiAgLnBsYW5fdGl0bGUgcCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAvKiAucXVlc3Rpb24ge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB9ICovXFxuICAgIC5wcmljZSBwIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGV0dGVyLXNwYWNpbmc6IC0wLjk2cHg7XFxuICAgIH1cXG5cXG4gIC5wcmljZV9kZXNjIHtcXG4gICAgd2lkdGg6IDEwMSU7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgIG9wYWNpdHk6IDAuODtcXG4gIH1cXG5cXG4gIC5idXlCdXR0b24ge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgd2lkdGg6IDE3NHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICBjb2xvcjogI2ZmZjtcXG4gICAgcGFkZGluZzogMTBweCAxNnB4O1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIG1hcmdpbi10b3A6IDI4cHg7XFxuICB9XFxuXFxuICAvKiAuZmFxX2NvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDI0cHg7XFxuXFxuICAgIC5mYXFfdGl0bGUgcCB7XFxuICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgIG1hcmdpbi10b3A6IDA7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gICAgfVxcblxcbiAgICAuZXhwYW5kX2FsbCB7XFxuICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgIG1hcmdpbjogMCBhdXRvIDRweCBhdXRvO1xcbiAgICB9XFxuXFxuICAgIC5hY2NvcmRpYW4ge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcblxcbiAgICAgIC5ib3JkZXIge1xcbiAgICAgICAgcGFkZGluZzogMTZweCAwO1xcblxcbiAgICAgICAgLmFuc3dlciB7XFxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xcblxcbiAgICAgICAgICBwIHtcXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMThweDtcXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjc7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICAgICAgICB9XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuICAuY2lyY2xlX2JnIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogMTIlO1xcbiAgICB0b3A6IDI1JTtcXG4gICAgbGVmdDogOTUlO1xcbiAgfVxcblxcbiAgLnNxdWFyZV9iZyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiAxMjAlO1xcbiAgICBsZWZ0OiAtMyU7XFxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgIHdpZHRoOiAxMCU7XFxuICB9XFxuXFxuICAubGluZV92ZWN0b3JfYmcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnRyaWFuZ2xlX2JnIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAtMTAlO1xcbiAgICB3aWR0aDogMjAlO1xcbiAgICB0b3A6IDU1NHB4O1xcbiAgfVxcbn1cXG5cXG4ubW9kdWxlTGFiZWwge1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi52aWV3TGluayB7XFxuICB3aWR0aDogMTk1cHg7XFxuICBoZWlnaHQ6IDMycHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW46IDY0LjZweCAwIDExMnB4IDA7XFxuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMS4zMztcXG4gIGNvbG9yOiAjZjM2O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC52aWV3TGluayB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbWFyZ2luOiAwIDAgNDhweCAwO1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicHJpY2luZ0hlYWRlclwiOiBcIlByaWNpbmctcHJpY2luZ0hlYWRlci0yTUxvSFwiLFxuXHRcImRhcmtCYWNrZ3JvdW5kXCI6IFwiUHJpY2luZy1kYXJrQmFja2dyb3VuZC0xN2FnOFwiLFxuXHRcInByaWNlX3RpdGxlXCI6IFwiUHJpY2luZy1wcmljZV90aXRsZS0zWXFuOFwiLFxuXHRcInByaWNlX2NvbnRlbnRcIjogXCJQcmljaW5nLXByaWNlX2NvbnRlbnQtNXFsc2hcIixcblx0XCJtb2R1bGVzQ29udGFpbmVyXCI6IFwiUHJpY2luZy1tb2R1bGVzQ29udGFpbmVyLTloMll6XCIsXG5cdFwibW9kdWxlc0hlYWRlclwiOiBcIlByaWNpbmctbW9kdWxlc0hlYWRlci16ZmlVWVwiLFxuXHRcIm1vZHVsZXNMaXN0XCI6IFwiUHJpY2luZy1tb2R1bGVzTGlzdC0zX0R1UlwiLFxuXHRcIm1vZHVsZVwiOiBcIlByaWNpbmctbW9kdWxlLWR5N2czXCIsXG5cdFwiaW1hZ2VXcmFwcGVyXCI6IFwiUHJpY2luZy1pbWFnZVdyYXBwZXItMmN1OTVcIixcblx0XCJzY3JvbGxUb3BcIjogXCJQcmljaW5nLXNjcm9sbFRvcC1BYWcwN1wiLFxuXHRcImFubnVhbFwiOiBcIlByaWNpbmctYW5udWFsLTMwaWVfXCIsXG5cdFwidG9nZ2xlX2J0blwiOiBcIlByaWNpbmctdG9nZ2xlX2J0bi0zdDlNOFwiLFxuXHRcImFubnVhbF90ZXh0XCI6IFwiUHJpY2luZy1hbm51YWxfdGV4dC1jNmgtR1wiLFxuXHRcImFubnVhbF9vZmZlclwiOiBcIlByaWNpbmctYW5udWFsX29mZmVyLTJWQndHXCIsXG5cdFwidW5kZXJsaW5lXCI6IFwiUHJpY2luZy11bmRlcmxpbmUtVk5mUjVcIixcblx0XCJwcmljaW5nX2NvbnRhaW5lclwiOiBcIlByaWNpbmctcHJpY2luZ19jb250YWluZXItMkRaOUtcIixcblx0XCJwbGFuc1wiOiBcIlByaWNpbmctcGxhbnMtM295UkdcIixcblx0XCJwbGFuXCI6IFwiUHJpY2luZy1wbGFuLWNieUI3XCIsXG5cdFwiaW5uZXJDYXJkXCI6IFwiUHJpY2luZy1pbm5lckNhcmQtWEQtWFJcIixcblx0XCJzeW1ib2xcIjogXCJQcmljaW5nLXN5bWJvbC0xUFpsN1wiLFxuXHRcInByaWNpbmdfdGl0bGVcIjogXCJQcmljaW5nLXByaWNpbmdfdGl0bGUtMThWeG1cIixcblx0XCJ0b2dnbGVfb3V0ZXJcIjogXCJQcmljaW5nLXRvZ2dsZV9vdXRlci1URHlVMFwiLFxuXHRcInRvZ2dsZV9vblwiOiBcIlByaWNpbmctdG9nZ2xlX29uLUt5VG82XCIsXG5cdFwidG9nZ2xlX2lubmVyXCI6IFwiUHJpY2luZy10b2dnbGVfaW5uZXItZHhOdWFcIixcblx0XCJwbGFuX3RpdGxlX3N0dWRlbnRzXCI6IFwiUHJpY2luZy1wbGFuX3RpdGxlX3N0dWRlbnRzLWl4SHRGXCIsXG5cdFwicGxhbl90aXRsZVwiOiBcIlByaWNpbmctcGxhbl90aXRsZS0yel9rMFwiLFxuXHRcInBsYW5fdGl0bGVfcHJpY2VcIjogXCJQcmljaW5nLXBsYW5fdGl0bGVfcHJpY2UtMlp4NDlcIixcblx0XCJwcmljZVwiOiBcIlByaWNpbmctcHJpY2UtcmN0Z1BcIixcblx0XCJwcmljZV9kZXNjXCI6IFwiUHJpY2luZy1wcmljZV9kZXNjLTR4VmJBXCIsXG5cdFwicHJpY2VfY29udGFjdFwiOiBcIlByaWNpbmctcHJpY2VfY29udGFjdC03dU5pZ1wiLFxuXHRcInByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodFwiOiBcIlByaWNpbmctcHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0LXFBV2N1XCIsXG5cdFwiYnV5QnV0dG9uXCI6IFwiUHJpY2luZy1idXlCdXR0b24tM2cwTklcIixcblx0XCJlbnRlcnByaXNlXCI6IFwiUHJpY2luZy1lbnRlcnByaXNlLTFPLW1GXCIsXG5cdFwiZW50ZXJwcmlzZUhlYWRpbmdcIjogXCJQcmljaW5nLWVudGVycHJpc2VIZWFkaW5nLTJYbTY4XCIsXG5cdFwiZW50ZXJwcmlzZWNvbnRleHRcIjogXCJQcmljaW5nLWVudGVycHJpc2Vjb250ZXh0LTNBRDlKXCIsXG5cdFwiY29udGFjdHVzXCI6IFwiUHJpY2luZy1jb250YWN0dXMtWGZMOUdcIixcblx0XCJsZWZ0QXJyb3dcIjogXCJQcmljaW5nLWxlZnRBcnJvdy0zQmNwR1wiLFxuXHRcInJpZ2h0QXJyb3dcIjogXCJQcmljaW5nLXJpZ2h0QXJyb3ctMUNHaXFcIixcblx0XCJjaXJjbGVfYmdcIjogXCJQcmljaW5nLWNpcmNsZV9iZy12YnZ0a1wiLFxuXHRcInNxdWFyZV9iZ1wiOiBcIlByaWNpbmctc3F1YXJlX2JnLTFmV09rXCIsXG5cdFwibGluZV92ZWN0b3JfYmdcIjogXCJQcmljaW5nLWxpbmVfdmVjdG9yX2JnLTJaa3NzXCIsXG5cdFwidHJpYW5nbGVfYmdcIjogXCJQcmljaW5nLXRyaWFuZ2xlX2JnLTFYN0k0XCIsXG5cdFwiZGlzcGxheUNsaWVudHNcIjogXCJQcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SXCIsXG5cdFwiZmVhdHVyZWRcIjogXCJQcmljaW5nLWZlYXR1cmVkLTN2ZjhMXCIsXG5cdFwic2hvd01lZGlhXCI6IFwiUHJpY2luZy1zaG93TWVkaWEtNlU3NnFcIixcblx0XCJjbGllbnRzV3JhcHBlclwiOiBcIlByaWNpbmctY2xpZW50c1dyYXBwZXItNjVHX2lcIixcblx0XCJjbGllbnRcIjogXCJQcmljaW5nLWNsaWVudC0xNGtHdFwiLFxuXHRcImFjaGlldmVkQ29udGFpbmVyXCI6IFwiUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZlwiLFxuXHRcImFjaGlldmVkSGVhZGluZ1wiOiBcIlByaWNpbmctYWNoaWV2ZWRIZWFkaW5nLWh4bnd1XCIsXG5cdFwiYWNoaWV2ZWRSb3dcIjogXCJQcmljaW5nLWFjaGlldmVkUm93LTI5VHNMXCIsXG5cdFwiY2FyZFwiOiBcIlByaWNpbmctY2FyZC0zLWpNZFwiLFxuXHRcImFjaGlldmVkUHJvZmlsZVwiOiBcIlByaWNpbmctYWNoaWV2ZWRQcm9maWxlLTFqZ2RJXCIsXG5cdFwiY2xpZW50c1wiOiBcIlByaWNpbmctY2xpZW50cy0ybW5DWFwiLFxuXHRcInN0dWRlbnRzXCI6IFwiUHJpY2luZy1zdHVkZW50cy0xWUhwVVwiLFxuXHRcInRlc3RzXCI6IFwiUHJpY2luZy10ZXN0cy0zbkRVelwiLFxuXHRcInF1ZXN0aW9uc1wiOiBcIlByaWNpbmctcXVlc3Rpb25zLTNxSjhEXCIsXG5cdFwiaGlnaGxpZ2h0XCI6IFwiUHJpY2luZy1oaWdobGlnaHQtMy1Uc21cIixcblx0XCJxdWVzdGlvbnNIaWdobGlnaHRcIjogXCJQcmljaW5nLXF1ZXN0aW9uc0hpZ2hsaWdodC0xSEpsUlwiLFxuXHRcInRlc3RzSGlnaGxpZ2h0XCI6IFwiUHJpY2luZy10ZXN0c0hpZ2hsaWdodC12a2FwcVwiLFxuXHRcInN0dWRlbnRzSGlnaGxpZ2h0XCI6IFwiUHJpY2luZy1zdHVkZW50c0hpZ2hsaWdodC0yVXVPOVwiLFxuXHRcImNsaWVudHNIaWdobGlnaHRcIjogXCJQcmljaW5nLWNsaWVudHNIaWdobGlnaHQtMmhVUUVcIixcblx0XCJzdWJUZXh0XCI6IFwiUHJpY2luZy1zdWJUZXh0LTNQRld6XCIsXG5cdFwidGl0bGVib3hcIjogXCJQcmljaW5nLXRpdGxlYm94LTFtQmN5XCIsXG5cdFwib25fbmV3c1wiOiBcIlByaWNpbmctb25fbmV3cy0yYmdEaFwiLFxuXHRcImZlYXR1cmVkSW1hZ2VcIjogXCJQcmljaW5nLWZlYXR1cmVkSW1hZ2UtSm9jLTlcIixcblx0XCJhd2FyZHNDb250YWluZXJcIjogXCJQcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZ1wiLFxuXHRcImF3YXJkc1RpdGxlXCI6IFwiUHJpY2luZy1hd2FyZHNUaXRsZS0yRl9wOFwiLFxuXHRcImF3YXJkTG9nb1wiOiBcIlByaWNpbmctYXdhcmRMb2dvLTJwcXZQXCIsXG5cdFwiZG90c1wiOiBcIlByaWNpbmctZG90cy0ySnQyM1wiLFxuXHRcInBvcHVwb3ZlcmxheVwiOiBcIlByaWNpbmctcG9wdXBvdmVybGF5LTMyeWV6XCIsXG5cdFwicHJlc3Nwb3B1cFwiOiBcIlByaWNpbmctcHJlc3Nwb3B1cC0xVUNQSlwiLFxuXHRcInBsYXllclwiOiBcIlByaWNpbmctcGxheWVyLTFGQ3NjXCIsXG5cdFwibW9iaWxlUHJpY2luZ0NvbnRhaW5lclwiOiBcIlByaWNpbmctbW9iaWxlUHJpY2luZ0NvbnRhaW5lci0zdzZRa1wiLFxuXHRcInRhYnNDb250YWluZXJcIjogXCJQcmljaW5nLXRhYnNDb250YWluZXItMS01YW1cIixcblx0XCJhbm51YWx0YWJcIjogXCJQcmljaW5nLWFubnVhbHRhYi1keUlRWlwiLFxuXHRcIm1vbnRodGFiXCI6IFwiUHJpY2luZy1tb250aHRhYi0zSnlfc1wiLFxuXHRcImFjdGl2ZVRhYlwiOiBcIlByaWNpbmctYWN0aXZlVGFiLTN2aTFSXCIsXG5cdFwibW9iaWxlUGxhbnNcIjogXCJQcmljaW5nLW1vYmlsZVBsYW5zLXZiSHhoXCIsXG5cdFwibW9iaWxlUHJpY2VDYXJkXCI6IFwiUHJpY2luZy1tb2JpbGVQcmljZUNhcmQtMzJ1QlpcIixcblx0XCJwcmljZVBlclN0dWRlbnRIaWdobGlnaHRNb2JpbGVcIjogXCJQcmljaW5nLXByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodE1vYmlsZS0zNmlYRlwiLFxuXHRcImFjdGl2ZVwiOiBcIlByaWNpbmctYWN0aXZlLTFmQTNuXCIsXG5cdFwiZG90XCI6IFwiUHJpY2luZy1kb3QtMldkMW5cIixcblx0XCJkb3RhY3RpdmVcIjogXCJQcmljaW5nLWRvdGFjdGl2ZS0zMV9fLVwiLFxuXHRcIm1vZHVsZUxhYmVsXCI6IFwiUHJpY2luZy1tb2R1bGVMYWJlbC0yOFZhblwiLFxuXHRcInZpZXdMaW5rXCI6IFwiUHJpY2luZy12aWV3TGluay0xLWJId1wiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9Nb2RhbC5zY3NzJztcblxuY2xhc3MgTW9kYWwgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAvKiogTGlicmFyeSBWYXJpYWJsZSBmb3IgdmVyaWZ5aW5nIHByb3BzIGZyb20gdGhlIHBhcmVudCBjb21wb25lbnQgKi9cbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBtb2RhbENsYXNzTmFtZTogUHJvcFR5cGVzLm9iamVjdCwgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgIG92ZXJsYXlDbGFzc05hbWU6IFByb3BUeXBlcy5vYmplY3QsIC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICB0b2dnbGVNb2RhbDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBpZiAodGhpcy5tb2RhbENvbnRhaW5lcikge1xuICAgICAgdGhpcy5tb2RhbENvbnRhaW5lci5mb2N1cygpO1xuICAgIH1cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gIH1cblxuICBoYW5kbGVLZXlEb3duRXZlbnQgPSBlID0+IHtcbiAgICBpZiAoZS53aGljaCA9PT0gMjcpIHtcbiAgICAgIHRoaXMucHJvcHMudG9nZ2xlTW9kYWwoKTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlT3V0c2lkZUNsaWNrID0gZSA9PiB7XG4gICAgLy8gaWdub3JlIGNsaWNrcyBvbiB0aGUgY29tcG9uZW50IGl0c2VsZlxuICAgIGlmICh0aGlzLm1vZGFsQ29udGFpbmVyICYmIHRoaXMubW9kYWxDb250YWluZXIuY29udGFpbnMoZS50YXJnZXQpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICh0aGlzLm1vZGFsQ29udGFpbmVyKSB7XG4gICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLCBmYWxzZSk7XG4gICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFxuICAgICAgICAndG91Y2hzdGFydCcsXG4gICAgICAgIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrLFxuICAgICAgICBmYWxzZSxcbiAgICAgICk7XG4gICAgICB0aGlzLnByb3BzLnRvZ2dsZU1vZGFsKCk7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2Bib2R5LW92ZXJsYXkgJHtzLmJvZHlPdmVybGF5fSAke3RoaXMucHJvcHMub3ZlcmxheUNsYXNzTmFtZX1gfVxuICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgb25LZXlEb3duPXt0aGlzLmhhbmRsZUtleURvd25FdmVudH1cbiAgICAgICAgcmVmPXtyZWYgPT4ge1xuICAgICAgICAgIHRoaXMubW9kYWxPdmVybGF5ID0gcmVmO1xuICAgICAgICB9fVxuICAgICAgPlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLm1vZGFsfSAke3RoaXMucHJvcHMubW9kYWxDbGFzc05hbWV9YH1cbiAgICAgICAgICByZWY9e3JlZiA9PiB7XG4gICAgICAgICAgICB0aGlzLm1vZGFsQ29udGFpbmVyID0gcmVmO1xuICAgICAgICAgIH19XG4gICAgICAgICAgdGFiSW5kZXg9XCItMVwiXG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZHJlbn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoTW9kYWwpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Nb2RhbC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL01vZGFsLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Nb2RhbC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IFJlYWN0UGxheWVyIGZyb20gJ3JlYWN0LXBsYXllcic7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IE1vZGFsIGZyb20gJ2NvbXBvbmVudHMvTW9kYWwnO1xuaW1wb3J0IHtcbiAgSE9NRV9DTElFTlRTX1VQREFURUQsXG4gIFBSRVNTX1ZJREVPX0NPVkVSQUdFLFxuICBIT01FX01FRElBLFxuICBIT01FX0FXQVJEUyxcbn0gZnJvbSAnLi4vR2V0UmFua3NDb25zdGFudHMnO1xuaW1wb3J0IHtcbiAgcHJpY2luZ1BlckFubnVtRGV0YWlscyxcbiAgcHJpY2luZ1Blck1vbnRoRGV0YWlscyxcbiAgTW9kdWxlc0xpc3QsXG59IGZyb20gJy4vY29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vUHJpY2luZy5zY3NzJztcblxuZnVuY3Rpb24gUHJpY2luZygpIHtcbiAgY29uc3QgW2FjdGl2ZUluZGV4LCBzZXRBY3RpdmVJbmRleF0gPSB1c2VTdGF0ZSgwKTtcbiAgLy8gY29uc3QgW2V4cGFuZEFsbCwgc2V0RXhwYW5kQWxsXSA9IHVzZVN0YXRlKGZhbHNlKTtcbiAgY29uc3QgW3RvZ2dsZSwgc2V0VG9nZ2xlXSA9IHVzZVN0YXRlKGZhbHNlKTtcbiAgY29uc3QgW3Nob3dTY3JvbGwsIHNldFNob3dTY3JvbGxdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBbc2hvd01vZGFsLCBzZXRTaG93TW9kYWxdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBbYWN0aXZlUHJlc3MsIHNldEFjdGl2ZVByZXNzXSA9IHVzZVN0YXRlKG51bGwpO1xuICBjb25zdCBbaXNNb2JpbGUsIHNldElzTW9iaWxlXSA9IHVzZVN0YXRlKGZhbHNlKTtcblxuICBjb25zdCBoYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5zY3JvbGxZID4gNTAwKSB7XG4gICAgICBzZXRTaG93U2Nyb2xsKHRydWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzZXRTaG93U2Nyb2xsKGZhbHNlKTtcbiAgICB9XG4gIH07XG5cbiAgY29uc3QgaGFuZGxlUmVzaXplID0gKCkgPT4ge1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSA5OTApIHtcbiAgICAgIHNldElzTW9iaWxlKGZhbHNlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc2V0SXNNb2JpbGUodHJ1ZSk7XG4gICAgfVxuICB9O1xuXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgaGFuZGxlU2Nyb2xsKCk7XG4gICAgaGFuZGxlUmVzaXplKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGhhbmRsZVNjcm9sbCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGhhbmRsZVJlc2l6ZSk7XG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBoYW5kbGVTY3JvbGwpO1xuICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGhhbmRsZVJlc2l6ZSk7XG4gICAgfTtcbiAgfSk7XG5cbiAgY29uc3QgaGFuZGxlU2Nyb2xsVG9wID0gKCkgPT4ge1xuICAgIHdpbmRvdy5zY3JvbGxUbyh7XG4gICAgICB0b3A6IDAsXG4gICAgICBiZWhhdmlvcjogJ3Ntb290aCcsXG4gICAgfSk7XG4gICAgc2V0U2hvd1Njcm9sbChmYWxzZSk7XG4gIH07XG5cbiAgY29uc3QgZGlzcGxheVNjcm9sbFRvVG9wID0gKCkgPT5cbiAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPXtzLnNjcm9sbFRvcH1cbiAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICBoYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgfX1cbiAgICAgID5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuXG4gIGNvbnN0IHRvZ2dsZUhhbmRsZXIgPSAoKSA9PiB7XG4gICAgc2V0VG9nZ2xlKCF0b2dnbGUpO1xuICB9O1xuXG4gIGNvbnN0IGRpc3BsYXlNb2R1bGVzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vZHVsZXNDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlc0hlYWRlcn0+RXZlcnkgcGxhbiBpbmNsdWRlczwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlc0xpc3R9PlxuICAgICAgICB7TW9kdWxlc0xpc3QubWFwKG1vZHVsZSA9PiAoXG4gICAgICAgICAgPExpbmsgdG89e2AvbW9kdWxlcy8ke21vZHVsZS52YWx1ZX1gfSBjbGFzc05hbWU9e3MubW9kdWxlfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltYWdlV3JhcHBlcn0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPXttb2R1bGUucGF0aH0gYWx0PXttb2R1bGUubGFiZWx9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vZHVsZUxhYmVsfT57bW9kdWxlLmxhYmVsfTwvZGl2PlxuICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLmxpbmVfdmVjdG9yX2JnfVxuICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvdmVjdG9yX2xpbmUucG5nXCJcbiAgICAgICAgYWx0PVwiXCJcbiAgICAgIC8+XG4gICAgICA8aW1nXG4gICAgICAgIGNsYXNzTmFtZT17cy50cmlhbmdsZV9iZ31cbiAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3RyaWFuZ2xlLWJhY2tncm91bmQuc3ZnXCJcbiAgICAgICAgYWx0PVwiXCJcbiAgICAgIC8+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZ290b1ByZXZTbGlkZSA9ICgpID0+IHtcbiAgICBpZiAoYWN0aXZlSW5kZXggPT09IDApIHtcbiAgICAgIHNldEFjdGl2ZUluZGV4KDMpO1xuICAgIH0gZWxzZSBzZXRBY3RpdmVJbmRleChhY3RpdmVJbmRleCAtIDEpO1xuICB9O1xuICBjb25zdCBnb3RvTmV4dFNsaWRlID0gKCkgPT4ge1xuICAgIGlmIChhY3RpdmVJbmRleCA9PT0gMykgc2V0QWN0aXZlSW5kZXgoMCk7XG4gICAgZWxzZSBzZXRBY3RpdmVJbmRleChhY3RpdmVJbmRleCArIDEpO1xuICB9O1xuXG4gIGNvbnN0IGRpc3BsYXlNb2JpbGVQcmljaW5ncyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2JpbGVQcmljaW5nQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYnNDb250YWluZXJ9PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmFubnVhbHRhYn0gJHshdG9nZ2xlID8gcy5hY3RpdmVUYWIgOiBudWxsfWB9XG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgc2V0VG9nZ2xlKGZhbHNlKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgQW5udWFsIFBsYW5zXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLm1vbnRodGFifSAke3RvZ2dsZSA/IHMuYWN0aXZlVGFiIDogbnVsbH1gfVxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHNldFRvZ2dsZSh0cnVlKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgTW9udGhseSBQbGFuc1xuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9iaWxlUGxhbnN9PlxuICAgICAgICB7KHRvZ2dsZSA/IHByaWNpbmdQZXJNb250aERldGFpbHMgOiBwcmljaW5nUGVyQW5udW1EZXRhaWxzKS5tYXAoXG4gICAgICAgICAgZGV0YWlsID0+IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vYmlsZVByaWNlQ2FyZH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYW5fdGl0bGVfcHJpY2V9PntkZXRhaWwucGFja308L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhbl90aXRsZV9zdHVkZW50c30+XG4gICAgICAgICAgICAgICAgdXB0byB7ZGV0YWlsLm5vT2ZTdHVkZW50c30gc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIHt0b2dnbGUgPyAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfZGVzY30+XG4gICAgICAgICAgICAgICAgICBzd2l0Y2ggdG8gYW5udWFsIHBhY2sgdG8gc2F2ZSAxNSVcbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV9kZXNjfT5cbiAgICAgICAgICAgICAgICAgIFBlciBzdHVkZW50Jm5ic3A7XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MucHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0TW9iaWxlfT5cbiAgICAgICAgICAgICAgICAgICAgJiM4Mzc3OzQ1Ly1cbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICZuYnNwO3BlciBtb250aFxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApfVxuXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJ1eUJ1dHRvbn0+JiM4Mzc3O3tkZXRhaWwucHJpY2VQZXJNb250aH08L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICksXG4gICAgICAgICl9XG4gICAgICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6ICdmaXQtY29udGVudCcgfX0gY2xhc3NOYW1lPXtzLm1vYmlsZVByaWNlQ2FyZH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhbl90aXRsZV9wcmljZX0+RW50ZXJwcmlzZTwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX2Rlc2N9PlxuICAgICAgICAgICAgRm9yIGluc3RpdHV0ZXMgd2l0aCBtb3JlIHRoYW4gMTAwMCBzdHVkZW50c1xuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxMaW5rIHRvPVwiL3JlcXVlc3QtZGVtb1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYnV5QnV0dG9ufT5Db250YWN0IHVzPC9kaXY+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGNvbnN0IGRpc3BsYXlQcmljaW5ncyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljaW5nX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljaW5nX3RpdGxlfT5cbiAgICAgICAgey8qICA8aW1nXG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmxpbmVfdmVjdG9yX2JnfVxuICAgICAgICAgIHNyYz1cImltYWdlcy9pY29ucy92ZWN0b3JfbGluZS5wbmdcIlxuICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgIC8+XG4gICAgICAgIDxpbWdcbiAgICAgICAgICBjbGFzc05hbWU9e3MudHJpYW5nbGVfYmd9XG4gICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3RyaWFuZ2xlLWJhY2tncm91bmQuc3ZnXCJcbiAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAvPiAgKi99XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFubnVhbH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYW5udWFsX3RleHR9ICR7dG9nZ2xlID8gJycgOiBzLnVuZGVybGluZX1gfT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5hbm51YWxfb2ZmZXJ9PihzYXZlIDE1JSk8L3NwYW4+XG4gICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgIDxzcGFuPkFubnVhbCBQbGFuczwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvZ2dsZV9idG59PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdG9nZ2xlSGFuZGxlcigpfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgdG9nZ2xlXG4gICAgICAgICAgICAgICAgPyBgJHtzLnRvZ2dsZV9vdXRlcn0gJHtzLnRvZ2dsZV9vbn0gJHtzLmRhcmtCYWNrZ3JvdW5kfWBcbiAgICAgICAgICAgICAgICA6IHMudG9nZ2xlX291dGVyXG4gICAgICAgICAgICB9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9nZ2xlX2lubmVyfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYW5udWFsX3RleHR9ICR7dG9nZ2xlID8gcy51bmRlcmxpbmUgOiAnJ31gfT5cbiAgICAgICAgICBNb250aGx5IFBsYW5zXG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGFuc30+XG4gICAgICAgIHsodG9nZ2xlID8gcHJpY2luZ1Blck1vbnRoRGV0YWlscyA6IHByaWNpbmdQZXJBbm51bURldGFpbHMpLm1hcChcbiAgICAgICAgICAocGxhbiwgaW5kZXgpID0+IChcbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAga2V5PXtwbGFuLnBhY2t9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgaW5kZXggPT09IGFjdGl2ZUluZGV4ID8gYCR7cy5wbGFufSAke3MuYWN0aXZlfWAgOiBgJHtzLnBsYW59YFxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmlubmVyQ2FyZH0+XG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmxlZnRBcnJvd31cbiAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2dvdG9QcmV2U2xpZGV9XG4gICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICA8c3Bhbj4mIzgyNDk7PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5yaWdodEFycm93fVxuICAgICAgICAgICAgICAgICAgb25DbGljaz17Z290b05leHRTbGlkZX1cbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPiYjODI1MDs8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ltYm9sfT5cbiAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaW1hZ2VzL2ljb25zLyR7cGxhbi5pbWFnZX1gfSBhbHQ9XCJcIiAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYW5fdGl0bGV9PlxuICAgICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3ByaWNlfT57cGxhbi5wYWNrfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3N0dWRlbnRzfT5cbiAgICAgICAgICAgICAgICAgICAgICBVcCB0byB7cGxhbi5ub09mU3R1ZGVudHN9IHN0dWRlbnRzXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZX0+XG4gICAgICAgICAgICAgICAgICA8cD4mIzgzNzc7e3BsYW4ucHJpY2VQZXJNb250aH08L3A+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgeyF0b2dnbGUgPyAoXG4gICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlBlciBTdHVkZW50PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodH0+XG4gICAgICAgICAgICAgICAgICAgICAgJiM4Mzc3OzQ1Ly1cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXY+cGVyIG1vbnRoPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfZGVzY30+XG4gICAgICAgICAgICAgICAgICAgICAgPHA+c3dpdGNoIHRvIGFubnVhbCBwYWNrIHRvIHNhdmUgMTUlPC9wPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MucHJpY2VfY29udGFjdH1gfT5cbiAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiL2NvbnRhY3QtdXNcIj5Db250YWN0IFVzIGZvciBhIFF1b3RlPC9hPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9e3MuYnV5QnV0dG9ufT5CdXk8L2J1dHRvbj5cblxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdHN9PlxuICAgICAgICAgICAgICAgICAge3ByaWNpbmdQZXJNb250aERldGFpbHMubWFwKChkb3QsIGNsaWVudGluZGV4KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBrZXk9e2RvdC5wYWNrfVxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGllbnRpbmRleCA9PT0gYWN0aXZlSW5kZXhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmRvdH0gJHtzLmRvdGFjdGl2ZX1gXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogYCR7cy5kb3R9YFxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICksXG4gICAgICAgICl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVudGVycHJpc2V9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbnRlcnByaXNlSGVhZGluZ30+RW50ZXJwcmlzZTwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbnRlcnByaXNlY29udGV4dH0+XG4gICAgICAgICAgRm9yIGluc3RpdHV0ZXMgd2l0aCBtb3JlIHRoYW4gMTAwMCBzdHVkZW50c1xuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPExpbmsgdG89XCIvcmVxdWVzdC1kZW1vXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGFjdHVzfT5Db250YWN0IFVzPC9kaXY+XG4gICAgICAgIDwvTGluaz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGNvbnN0IGRpc3BsYXlQcmljaW5nSGVhZGVyID0gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNpbmdIZWFkZXJ9PlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MuY2lyY2xlX2JnfVxuICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvY2lyY2xlLWJhY2tncm91bmQuc3ZnXCJcbiAgICAgICAgYWx0PVwiXCJcbiAgICAgIC8+XG4gICAgICA8aW1nXG4gICAgICAgIGNsYXNzTmFtZT17cy5zcXVhcmVfYmd9XG4gICAgICAgIHNyYz1cImltYWdlcy9pY29ucy9zcXVhcmUtYmFja2dyb3VuZC5zdmdcIlxuICAgICAgICBhbHQ9XCJcIlxuICAgICAgLz5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX3RpdGxlfT5DaG9vc2UgeW91ciBlZGl0aW9uLjwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV90aXRsZX0+VHJ5IGl0IGZyZWUgZm9yIDcgZGF5cy48L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfY29udGVudH0+XG4gICAgICAgIEVnbmlmeSBwbGFucyBzdGFydCBhcyBsb3cgYXMgUnMuNDUvLSBwZXIgc3R1ZGVudCBwZXIgbW9udGguXG4gICAgICA8L2Rpdj5cbiAgICAgIHtkaXNwbGF5TW9kdWxlcygpfVxuICAgICAgPExpbmsgdG89XCIvcHJpY2luZy92aWV3RGV0YWlsc1wiIGNsYXNzTmFtZT17cy52aWV3TGlua30+XG4gICAgICAgIDxwPlZpZXcgUGxhbiBEZXRhaWxzPC9wPlxuICAgICAgPC9MaW5rPlxuICAgICAge2lzTW9iaWxlID8gZGlzcGxheU1vYmlsZVByaWNpbmdzKCkgOiBkaXNwbGF5UHJpY2luZ3MoKX1cbiAgICA8L2Rpdj5cbiAgKTtcblxuICAvLyBQcmljaW5nXG4gIC8vIGNvbnN0IGRpc3BsYXlQcmljaW5nID0gKFxuICAvLyAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNpbmdfY29udGFpbmVyfT5cbiAgLy8gICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNpbmdfdGl0bGV9PlxuICAvLyAgICAgICA8aW1nXG4gIC8vICAgICAgICAgY2xhc3NOYW1lPXtzLmxpbmVfdmVjdG9yX2JnfVxuICAvLyAgICAgICAgIHNyYz1cImltYWdlcy9pY29ucy92ZWN0b3JfbGluZS5wbmdcIlxuICAvLyAgICAgICAgIGFsdD1cIlwiXG4gIC8vICAgICAgIC8+XG4gIC8vICAgICAgIDxpbWdcbiAgLy8gICAgICAgICBjbGFzc05hbWU9e3MudHJpYW5nbGVfYmd9XG4gIC8vICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3RyaWFuZ2xlLWJhY2tncm91bmQuc3ZnXCJcbiAgLy8gICAgICAgICBhbHQ9XCJcIlxuICAvLyAgICAgICAvPlxuICAvLyAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hbm51YWx9PlxuICAvLyAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFubnVhbF90ZXh0fSAke3RvZ2dsZSA/ICcnIDogcy51bmRlcmxpbmV9YH0+XG4gIC8vICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYW5udWFsX29mZmVyfT4oc2F2ZSAxNSUpPC9zcGFuPlxuICAvLyAgICAgICAgICAgPGJyIC8+XG4gIC8vICAgICAgICAgICA8c3Bhbj5Bbm51YWwgUGxhbnM8L3NwYW4+XG4gIC8vICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgIDwvZGl2PlxuICAvLyAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b2dnbGVfYnRufT5cbiAgLy8gICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgLy8gICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRvZ2dsZUhhbmRsZXIoKX1cbiAgLy8gICAgICAgICAgIGNsYXNzTmFtZT17XG4gIC8vICAgICAgICAgICAgIHRvZ2dsZSA/IGAke3MudG9nZ2xlX291dGVyfSAke3MudG9nZ2xlX29ufWAgOiBzLnRvZ2dsZV9vdXRlclxuICAvLyAgICAgICAgICAgfVxuICAvLyAgICAgICAgID5cbiAgLy8gICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvZ2dsZV9pbm5lcn0gLz5cbiAgLy8gICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgPC9kaXY+XG4gIC8vICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFubnVhbF90ZXh0fSAke3RvZ2dsZSA/IHMudW5kZXJsaW5lIDogJyd9YH0+XG4gIC8vICAgICAgICAgTW9udGhseSBQbGFuc1xuICAvLyAgICAgICA8L2Rpdj5cbiAgLy8gICAgIDwvZGl2PlxuICAvLyAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhbnN9PlxuICAvLyAgICAgICB7KHRvZ2dsZSA/IHByaWNpbmdQZXJNb250aERldGFpbHMgOiBwcmljaW5nUGVyQW5udW1EZXRhaWxzKS5tYXAoXG4gIC8vICAgICAgICAgKHBsYW4sIGluZGV4KSA9PiAoXG4gIC8vICAgICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICAgIGtleT17cGxhbi5wYWNrfVxuICAvLyAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAvLyAgICAgICAgICAgICAgIGluZGV4ID09PSBhY3RpdmVJbmRleCA/IGAke3MucGxhbn0gJHtzLmFjdGl2ZX1gIDogYCR7cy5wbGFufWBcbiAgLy8gICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICAgPlxuICAvLyAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbm5lckNhcmR9PlxuICAvLyAgICAgICAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5sZWZ0QXJyb3d9XG4gIC8vICAgICAgICAgICAgICAgICBvbkNsaWNrPXtnb3RvUHJldlNsaWRlfVxuICAvLyAgICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gIC8vICAgICAgICAgICAgICAgPlxuICAvLyAgICAgICAgICAgICAgICAgPHNwYW4+JiM4MjQ5Ozwvc3Bhbj5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucmlnaHRBcnJvd31cbiAgLy8gICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2dvdG9OZXh0U2xpZGV9XG4gIC8vICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgLy8gICAgICAgICAgICAgICA+XG4gIC8vICAgICAgICAgICAgICAgICA8c3Bhbj4mIzgyNTA7PC9zcGFuPlxuICAvLyAgICAgICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN5bWJvbH0+XG4gIC8vICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGltYWdlcy9pY29ucy8ke3BsYW4uaW1hZ2V9YH0gYWx0PVwiXCIgLz5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlfT5cbiAgLy8gICAgICAgICAgICAgICAgIDxwPlxuICAvLyAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MucGxhbl90aXRsZV9wcmljZX0+e3BsYW4ucGFja308L3NwYW4+XG4gIC8vICAgICAgICAgICAgICAgICAgIDxiciAvPlxuICAvLyAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MucGxhbl90aXRsZV9zdHVkZW50c30+XG4gIC8vICAgICAgICAgICAgICAgICAgICAgVXAgdG8ge3BsYW4ubm9PZlN0dWRlbnRzfSBzdHVkZW50c1xuICAvLyAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gIC8vICAgICAgICAgICAgICAgICA8L3A+XG4gIC8vICAgICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2V9PlxuICAvLyAgICAgICAgICAgICAgICAgPHA+e3BsYW4ucHJpY2VQZXJNb250aH08L3A+XG4gIC8vICAgICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfZGVzY30+XG4gIC8vICAgICAgICAgICAgICAgICA8cD5cbiAgLy8gICAgICAgICAgICAgICAgICAgUGVyIHt0b2dnbGUgPyAnbW9udGgnIDogJ3llYXInfSwgYmlsbGVkXG4gIC8vICAgICAgICAgICAgICAgICAgIDxiciAvPiB7dG9nZ2xlID8gJ21vbnRoJyA6ICd5ZWFyJ30gaW4gUnVwZWVzXG4gIC8vICAgICAgICAgICAgICAgICA8L3A+XG4gIC8vICAgICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MucHJpY2VfY29udGFjdH1gfT5cbiAgLy8gICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvY29udGFjdC11c1wiPkNvbnRhY3QgVXMgZm9yIGEgUXVvdGU8L2E+XG4gIC8vICAgICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICAgICAgPGJ1dHRvbj5CdXk8L2J1dHRvbj5cbiAgLy9cbiAgLy8gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RzfT5cbiAgLy8gICAgICAgICAgICAgICAgIHtwcmljaW5nUGVyTW9udGhEZXRhaWxzLm1hcCgoZG90LCBjbGllbnRpbmRleCkgPT4gKFxuICAvLyAgICAgICAgICAgICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICAgICAgICAgICAga2V5PXtkb3QucGFja31cbiAgLy8gICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAvLyAgICAgICAgICAgICAgICAgICAgICAgY2xpZW50aW5kZXggPT09IGFjdGl2ZUluZGV4XG4gIC8vICAgICAgICAgICAgICAgICAgICAgICAgID8gYCR7cy5kb3R9ICR7cy5kb3RhY3RpdmV9YFxuICAvLyAgICAgICAgICAgICAgICAgICAgICAgICA6IGAke3MuZG90fWBcbiAgLy8gICAgICAgICAgICAgICAgICAgICB9XG4gIC8vICAgICAgICAgICAgICAgICAgIC8+XG4gIC8vICAgICAgICAgICAgICAgICApKX1cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICApLFxuICAvLyAgICAgICApfVxuICAvLyAgICAgPC9kaXY+XG4gIC8vICAgPC9kaXY+XG4gIC8vICk7XG5cbiAgLy8gY29uc3Qgb25DbGlja0hhbmRsZXIgPSBlID0+IHtcbiAgLy8gICBjb25zdCBwYXJlbnQgPSBlLmN1cnJlbnRUYXJnZXQucGFyZW50RWxlbWVudDtcbiAgLy9cbiAgLy8gICBpZiAocGFyZW50LmNsYXNzTGlzdC5jb250YWlucyhgJHtzLnNob3d9YCkpIHtcbiAgLy8gICAgIHBhcmVudC5jbGFzc0xpc3QucmVtb3ZlKGAke3Muc2hvd31gKTtcbiAgLy8gICB9IGVsc2Uge1xuICAvLyAgICAgcGFyZW50LmNsYXNzTGlzdC5hZGQoYCR7cy5zaG93fWApO1xuICAvLyAgIH1cbiAgLy8gfTtcblxuICAvLyBjb25zdCBleHBhbmRBcHBIYW5kbGVyID0gKCkgPT4ge1xuICAvLyAgIHNldEV4cGFuZEFsbCghZXhwYW5kQWxsKTtcbiAgLy8gfTtcblxuICAvLyBjb25zdCBwcmljaW5nRkFRID0gKFxuICAvLyAgIDxkaXYgY2xhc3NOYW1lPXtzLmZhcV9jb250YWluZXJ9PlxuICAvLyAgICAgPGRpdiBjbGFzc05hbWU9e3MuZmFxX3RpdGxlfT5cbiAgLy8gICAgICAgPHA+UHJpY2luZyBGQVFzPC9wPlxuICAvLyAgICAgPC9kaXY+XG4gIC8vICAgICA8ZGl2XG4gIC8vICAgICAgIGNsYXNzTmFtZT17cy5leHBhbmRfYWxsfVxuICAvLyAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgLy8gICAgICAgb25DbGljaz17KCkgPT4ge1xuICAvLyAgICAgICAgIGV4cGFuZEFwcEhhbmRsZXIoKTtcbiAgLy8gICAgICAgfX1cbiAgLy8gICAgID5cbiAgLy8gICAgICAge2V4cGFuZEFsbCA/ICdNaW5pbWl6ZSBBbGwnIDogJ0V4cGFuZCBBbGwnfVxuICAvLyAgICAgPC9kaXY+XG4gIC8vICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2NvcmRpYW59PlxuICAvLyAgICAgICB7RkFRLm1hcChpdGVtID0+IChcbiAgLy8gICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICBrZXk9e2l0ZW0ubGFiZWx9XG4gIC8vICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuYm9yZGVyfSAke2V4cGFuZEFsbCA/IHMuc2hvdyA6ICcnfWB9XG4gIC8vICAgICAgICAgPlxuICAvLyAgICAgICAgICAgPGRpdlxuICAvLyAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgLy8gICAgICAgICAgICAgb25DbGljaz17ZSA9PiB7XG4gIC8vICAgICAgICAgICAgICAgb25DbGlja0hhbmRsZXIoZSk7XG4gIC8vICAgICAgICAgICAgIH19XG4gIC8vICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5xdWVzdGlvbn1cbiAgLy8gICAgICAgICAgID5cbiAgLy8gICAgICAgICAgICAgPHNwYW4+e2l0ZW0ubGFiZWx9PC9zcGFuPlxuICAvLyAgICAgICAgICAgICA8aW1nXG4gIC8vICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmRvd259XG4gIC8vICAgICAgICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmdcIlxuICAvLyAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gIC8vICAgICAgICAgICAgIC8+XG4gIC8vICAgICAgICAgICAgIDxpbWdcbiAgLy8gICAgICAgICAgICAgICBjbGFzc05hbWU9e3MudXB9XG4gIC8vICAgICAgICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmdcIlxuICAvLyAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gIC8vICAgICAgICAgICAgIC8+XG4gIC8vICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFuc3dlcn0+XG4gIC8vICAgICAgICAgICAgIDxwPntpdGVtLmNvbnRlbnR9PC9wPlxuICAvLyAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICkpfVxuICAvLyAgICAgPC9kaXY+XG4gIC8vICAgPC9kaXY+XG4gIC8vICk7XG5cbiAgLy8gY29uc3QgZGlzcGxheVZpZXdMaW5rID0gKCkgPT4gKFxuICAvLyAgPHNwYW4gY2xhc3NOYW1lPXtzLnZpZXd9PlxuICAvLyAgPExpbmsgdG89XCIvcHJpY2luZy92aWV3RGV0YWlsc1wiIGNsYXNzTmFtZT17cy52aWV3TGlua30+VmlldyBQbGFuIERldGFpbHM8L0xpbms+XG4gIC8vIDwvc3Bhbj5cbiAgLy8gKVxuXG4gIGNvbnN0IGRpc3BsYXlUcnVzdGVkQnkgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuZGlzcGxheUNsaWVudHN9PlxuICAgICAgPGgzPlxuICAgICAgICBUcnVzdGVkIGJ5IDxzcGFuIC8+XG4gICAgICAgIGxlYWRpbmcgRWR1Y2F0aW9uYWwgSW5zdGl0dXRpb25zXG4gICAgICA8L2gzPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY2xpZW50c1dyYXBwZXJ9PlxuICAgICAgICB7SE9NRV9DTElFTlRTX1VQREFURUQubWFwKGNsaWVudCA9PiAoXG4gICAgICAgICAgPGltZ1xuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmNsaWVudH1cbiAgICAgICAgICAgIGtleT17Y2xpZW50LmlkfVxuICAgICAgICAgICAgc3JjPXtjbGllbnQuaWNvbn1cbiAgICAgICAgICAgIGFsdD1cImNsaW5ldFwiXG4gICAgICAgICAgLz5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxzcGFuPlxuICAgICAgICA8TGluayB0bz1cIi9jdXN0b21lcnNcIj5jbGljayBoZXJlIGZvciBtb3JlLi4uPC9MaW5rPlxuICAgICAgPC9zcGFuPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGNvbnN0IGRpc3BsYXlBY2hpZXZlZCA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZENvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZEhlYWRpbmd9PlxuICAgICAgICBXaGF0IHdlIGhhdmUgYWNoaWV2ZWQgPHNwYW4gLz5cbiAgICAgICAgc28gZmFyXG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkUm93fT5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDI1NSwgMTAyLCAwLCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MuY2xpZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvY2xpZW50cy5zdmdcIiBhbHQ9XCJwcm9maWxlXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MuY2xpZW50c0hpZ2hsaWdodH1gfT4xNTArPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPkNsaWVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5zdHVkZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvc3R1ZGVudHMuc3ZnXCIgYWx0PVwic3R1ZGVudHNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5zdHVkZW50c0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDMgTGFraCtcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+U3R1ZGVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmNhcmR9YH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgwLCAxMTUsIDI1NSwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnRlc3RzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC90ZXN0cy5zdmdcIiBhbHQ9XCJ0ZXN0c1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnRlc3RzSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMyBNaWxsaW9uK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5UZXN0cyBBdHRlbXB0ZWQ8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMCwgMTcyLCAzOCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnF1ZXN0aW9uc31gfT5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvcXVlc3Rpb25zLnN2Z1wiXG4gICAgICAgICAgICAgIGFsdD1cInF1ZXN0aW9uc1wiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5xdWVzdGlvbnNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAyMCBNaWxsaW9uK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc3ViVGV4dH0+UXVlc3Rpb25zIFNvbHZlZDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBjb25zdCBvcGVuTW9kYWwgPSBpbmRleCA9PiB7XG4gICAgc2V0U2hvd01vZGFsKHRydWUpO1xuICAgIHNldEFjdGl2ZVByZXNzKFBSRVNTX1ZJREVPX0NPVkVSQUdFW2luZGV4XSk7XG4gIH07XG5cbiAgY29uc3QgdG9nZ2xlTW9kYWwgPSAoKSA9PiB7XG4gICAgc2V0U2hvd01vZGFsKGZhbHNlKTtcbiAgfTtcblxuICBjb25zdCBkaXNwbGF5RmVhdHVyZWRJbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5mZWF0dXJlZH0+XG4gICAgICA8aDI+V2UmYXBvczt2ZSBnb3QgZmVhdHVyZWQgaW48L2gyPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Mub25fbmV3c30+XG4gICAgICAgIHtQUkVTU19WSURFT19DT1ZFUkFHRS5tYXAoKGl0ZW0sIGluZGV4KSA9PiAoXG4gICAgICAgICAgPGRpdiByb2xlPVwicHJlc2VudGF0aW9uXCIgb25DbGljaz17KCkgPT4gb3Blbk1vZGFsKGluZGV4KX0+XG4gICAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29ufSBjbGFzc05hbWU9e3MuZmVhdHVyZWRJbWFnZX0gYWx0PXtpdGVtLnRpdGxlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2hvd01lZGlhfT5cbiAgICAgICAge0hPTUVfTUVESUEubWFwKG1lZGlhID0+IChcbiAgICAgICAgICA8YSBjbGFzc05hbWU9e3MuYWN0aXZlfSBocmVmPXttZWRpYS51cmx9PlxuICAgICAgICAgICAgPGltZyBzcmM9e21lZGlhLmljb259IGFsdD17bWVkaWEubmFtZX0gLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZGlzcGxheUF3YXJkcyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hd2FyZHNDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXdhcmRzVGl0bGV9PkF3YXJkczwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRSb3d9YH0+XG4gICAgICAgIHtIT01FX0FXQVJEUy5tYXAoaXRlbSA9PiAoXG4gICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbjF9IGFsdD17aXRlbS5jb250ZW50fSBjbGFzc05hbWU9e3MuYXdhcmRMb2dvfSAvPlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGNvbnN0IGRpc3BsYXlNb2RhbCA9ICgpID0+IChcbiAgICA8TW9kYWxcbiAgICAgIG1vZGFsQ2xhc3NOYW1lPXtzLnByZXNzcG9wdXB9XG4gICAgICBvdmVybGF5Q2xhc3NOYW1lPXtzLnBvcHVwb3ZlcmxheX1cbiAgICAgIHRvZ2dsZU1vZGFsPXt0b2dnbGVNb2RhbH1cbiAgICA+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy50aXRsZWJveH0+XG4gICAgICAgIDxoMj57YWN0aXZlUHJlc3MudGl0bGV9PC9oMj5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvY2xvc2Uuc3ZnXCJcbiAgICAgICAgICBvbkNsaWNrPXt0b2dnbGVNb2RhbH1cbiAgICAgICAgICBhbHQ9XCJjbG9zZVwiXG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cblxuICAgICAgPFJlYWN0UGxheWVyXG4gICAgICAgIHBsYXlpbmc9e2ZhbHNlfVxuICAgICAgICB1cmw9e2FjdGl2ZVByZXNzLnVybH1cbiAgICAgICAgY2xhc3NOYW1lPXtzLnBsYXllcn1cbiAgICAgICAgd2lkdGg9XCIxMDAlXCJcbiAgICAgICAgaGVpZ2h0PVwiODAlXCJcbiAgICAgICAgY29udHJvbHNcbiAgICAgICAgbG9vcFxuICAgICAgICBwbGF5c2lubGluZVxuICAgICAgICBwaXA9e2ZhbHNlfVxuICAgICAgICBjb25maWc9e3tcbiAgICAgICAgICB5b3V0dWJlOiB7XG4gICAgICAgICAgICBwbGF5ZXJWYXJzOiB7IHNob3dpbmZvOiAxIH0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfX1cbiAgICAgIC8+XG4gICAgPC9Nb2RhbD5cbiAgKTtcblxuICByZXR1cm4gKFxuICAgIDxkaXY+XG4gICAgICA8ZGl2PntkaXNwbGF5UHJpY2luZ0hlYWRlcn08L2Rpdj5cbiAgICAgIHtkaXNwbGF5VHJ1c3RlZEJ5KCl9XG4gICAgICB7ZGlzcGxheUFjaGlldmVkKCl9XG4gICAgICB7ZGlzcGxheUZlYXR1cmVkSW4oKX1cbiAgICAgIHtkaXNwbGF5QXdhcmRzKCl9XG4gICAgICB7ZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICB7c2hvd01vZGFsID8gZGlzcGxheU1vZGFsKCkgOiBudWxsfVxuICAgIDwvZGl2PlxuICApO1xufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFByaWNpbmcpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpY2luZy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpY2luZy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImV4cG9ydCBjb25zdCBGQVEgPSBbXG4gIHtcbiAgICBsYWJlbDogJ1doYXQgaXMgR2V0UmFua3MgTGVhcm5pbmcgUGxhdGZvcm0/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ1doYXQgZG8gSSBnZXQgd2l0aCBhIFJpc2Ugc3Vic2NyaXB0aW9uPycsXG4gICAgY29udGVudDpcbiAgICAgICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4nLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdIb3cgbXVjaCBkb2VzIGEgc3Vic2NyaXB0aW9uIGNvc3Q/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0RvIHlvdSBvZmZlciBtb250aGx5IHN1YnNjcmlwdGlvbnM/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0lzIHRoZXJlIGEgZnJlZSB0cmlhbCBwZXJpb2Q/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0RvIHlvdSBvZmZlciBkaXNjb3VudHM/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0hvdyBjYW4gSSBnZXQgYW4gZXZlbiBiaWdnZXIgZGlzY291bnQ/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0NhbiBJIGJ1eSBhIHN1YnNjcmlwdGlvbiBvdXRzaWRlIHRoZSBJbmRpYT8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnQ2FuIEkgYnV5IGEgc3Vic2NyaXB0aW9uIG9uIGJlaGFsZiBvZiBzb21lb25lIGVsc2U/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0hvdyBkbyBJIHB1cmNoYXNlIGEgdGF4LWV4ZW1wdCBzdWJzY3JpcHRpb24/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0NhbiBJIGNhbmNlbCBteSBzdWJzY3JpcHRpb24gYXQgYW55IHRpbWU/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ1doYXQgaXMgeW91ciByZWZ1bmQgcG9saWN5PycsXG4gICAgY29udGVudDpcbiAgICAgICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4nLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdXaGF04oCZcyB0aGUgZGlmZmVyZW5jZSBiZXR3ZWVuIHVzZXIgcm9sZXM/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ1doYXQgdXNlciByb2xlcyBjb3VudCB0b3dhcmRzIG15IHBsYW7igJlzIHVzZXIgbGltaXQ/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0NhbiBJIHVwZ3JhZGUgbXkgcGxhbiBhcyBuZWVkZWQ/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgcHJpY2luZ1BlckFubnVtRGV0YWlscyA9IFtcbiAge1xuICAgIGltYWdlOiAndHJpYW5nbGUuc3ZnJyxcbiAgICBwYWNrOiAnU3RhcnRlcicsXG4gICAgbm9PZlN0dWRlbnRzOiAxMDAsXG4gICAgcHJpY2VQZXJNb250aDogJzUwLDAwMC8tJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAncmhvbWJ1cy5zdmcnLFxuICAgIHBhY2s6ICdHcm93dGgnLFxuICAgIG5vT2ZTdHVkZW50czogMjUwLFxuICAgIHByaWNlUGVyTW9udGg6ICcyLDAwLDAwMC8tJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAncGVudGFnb24uc3ZnJyxcbiAgICBwYWNrOiAnTWlkc2l6ZScsXG4gICAgbm9PZlN0dWRlbnRzOiA1MDAsXG4gICAgcHJpY2VQZXJNb250aDogJzMsMDAsMDAwLy0nLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICdoZXhhZ29uLnN2ZycsXG4gICAgcGFjazogJ0xhcmdlJyxcbiAgICBub09mU3R1ZGVudHM6IDEwMDAsXG4gICAgcHJpY2VQZXJNb250aDogJzQsMDAsMDAwLy0nLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IHByaWNpbmdQZXJNb250aERldGFpbHMgPSBbXG4gIHtcbiAgICBpbWFnZTogJ3RyaWFuZ2xlLnN2ZycsXG4gICAgcGFjazogJ1N0YXJ0ZXInLFxuICAgIG5vT2ZTdHVkZW50czogMTAwLFxuICAgIHByaWNlUGVyTW9udGg6ICcyMCwwMDAvLScsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJ3Job21idXMuc3ZnJyxcbiAgICBwYWNrOiAnR3Jvd3RoJyxcbiAgICBub09mU3R1ZGVudHM6IDI1MCxcbiAgICBwcmljZVBlck1vbnRoOiAnNTAsMDAwLy0nLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICdwZW50YWdvbi5zdmcnLFxuICAgIHBhY2s6ICdNaWRzaXplJyxcbiAgICBub09mU3R1ZGVudHM6IDUwMCxcbiAgICBwcmljZVBlck1vbnRoOiAnMSwwMCwwMDAvLScsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJ2hleGFnb24uc3ZnJyxcbiAgICBwYWNrOiAnTGFyZ2UnLFxuICAgIG5vT2ZTdHVkZW50czogMTAwMCxcbiAgICBwcmljZVBlck1vbnRoOiAnMiwwMCwwMDAvLScsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgTW9kdWxlc0xpc3QgPSBbXG4gIHtcbiAgICBsYWJlbDogJ0xpdmUgQ2xhc3NlcycsXG4gICAgcGF0aDogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfTGl2ZS5zdmcnLFxuICAgIHZhbHVlOiAnbGl2ZWNsYXNzZXMnLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdPbmxpbmUgVGVzdHMnLFxuICAgIHBhdGg6ICcvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVzdC9tb2R1bGVfdGVzdC5zdmcnLFxuICAgIHZhbHVlOiAndGVzdHMnLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdBc3NpZ25tZW50cycsXG4gICAgcGF0aDogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfQXNzaWdubWVudHMuc3ZnJyxcbiAgICB2YWx1ZTogJ2Fzc2lnbm1lbnRzJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnRG91YnRzJyxcbiAgICBwYXRoOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9Eb3VidHMuc3ZnJyxcbiAgICB2YWx1ZTogJ2RvdWJ0cycsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0Nvbm5lY3QnLFxuICAgIHBhdGg6ICcvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29ubmVjdC9uZXdfY29ubmVjdC5zdmcnLFxuICAgIHZhbHVlOiAnY29ubmVjdCcsXG4gIH0sXG5dO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0JztcbmltcG9ydCBQcmljaW5nIGZyb20gJy4vUHJpY2luZyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogWydHZXRSYW5rcyBieSBFZ25pZnk6IEFzc2Vzc21lbnQgJiBBbmFseXRpY3MgUGxhdGZvcm0nXSxcbiAgICBjaHVua3M6IFsnUHJpY2luZyddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dCBmb290ZXJBc2g+XG4gICAgICAgIDxQcmljaW5nIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQXdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBckNBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF1QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBN0RBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUpBO0FBQ0E7QUE0REE7Ozs7Ozs7QUNwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQURBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFrQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNocUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7QUM3QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBOzs7Ozs7Ozs7Ozs7OztBQ3pKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBU0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=