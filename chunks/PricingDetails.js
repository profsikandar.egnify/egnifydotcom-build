require("source-map-support").install();
exports.ids = ["PricingDetails"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".PricingViewDetails-pricingHeader-2bYv- {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.PricingViewDetails-price_title-2nqFt {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.PricingViewDetails-price_content-1fVtK {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.PricingViewDetails-whoAreYou-3T0mg {\n  margin: 72px 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.PricingViewDetails-adminContainer-29ElQ {\n  cursor: pointer;\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 20px 20px;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.PricingViewDetails-tabsContainer-3_Zwu {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin: 24px 0 72px 0;\n}\n\n.PricingViewDetails-adminLabelContainer-2Mroj {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.PricingViewDetails-adminLabel-23wjb {\n  // width: 119px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 19px 38px 8px 8px;\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-imgContainer-1Jj_u {\n  width: 40px;\n  height: 40px;\n  margin: 10px 8px 0 0;\n  padding: 10px;\n  background-color: rgba(196, 196, 196, 0.3);\n  border-radius: 50px;\n}\n\n.PricingViewDetails-adminImg-2mAsS {\n  width: 20px;\n  height: 20px;\n  color: #25282b;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-plane-21eaT {\n  margin: 0 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.PricingViewDetails-moduleContainer-I6SSy {\n  width: 30%;\n  cursor: pointer;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 30px;\n  border-right: 1px solid rgba(0, 0, 0, 0.2);\n  background-color: #fff;\n}\n\n.PricingViewDetails-moduleContainer-I6SSy:last-child {\n  border-right-width: 0;\n}\n\n.PricingViewDetails-moduleContainerActive-2BKQt {\n  background-color: #f36;\n}\n\n.PricingViewDetails-iconActive-1MVuw {\n  background-color: #f36;\n}\n\n.PricingViewDetails-headerContainer-3Lmkq {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  // height: 48px;\n  width: 100%;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n}\n\n.PricingViewDetails-icon-UXaHR {\n  width: 24px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-moduleName-3StFu {\n  margin: 0 0 0 8px;\n  font-size: 14px;\n  letter-spacing: -0.28px;\n  color: #25282b;\n}\n\n.PricingViewDetails-moduleNameActive-3Lzqz {\n  color: #fff;\n  font-weight: 600;\n}\n\n.PricingViewDetails-adminContainerActive-1Z0JL {\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 0 20px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n          box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n  border: solid 1px #ff99b3;\n  text-align: center;\n}\n\n.PricingViewDetails-rightIcon-2i32Z {\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  width: 16px;\n  height: 16px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-active-2cVke {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.PricingViewDetails-subModules-1R5-x {\n  cursor: pointer;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 0 24px 0;\n  font-size: 16px;\n  line-height: 1.5;\n  color: #25282b;\n}\n\n.PricingViewDetails-subModuleActive-1hEpJ {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #f36;\n}\n\n.PricingViewDetails-iconDataContainer-i2G6T {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  // margin-left: 1px solid #d9d9d9;\n}\n\n.PricingViewDetails-iconDataContainer1-ur6rZ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  margin-left: 10px;\n  // margin-bottom: 8px;\n}\n\n.PricingViewDetails-dataContainer-3Q7OM {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 14px;\n  line-height: 1.57;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-icons-2XSLR {\n  width: 18px;\n  height: 18px;\n  margin: 6px 12px 0 0;\n}\n\n.PricingViewDetails-iconContainer-1QcNN {\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 12px;\n  // border-right: 1px solid #d9d9d9;\n}\n\n/* .dataIconContainer {\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  column-gap: 3rem;\n  row-gap: 3rem;\n}\n */\n\n/* .verticalLine {\n  height: 420px;\n  width: 1px;\n  margin: 63px 10px 0 0;\n  background-color: #d9d9d9;\n} */\n\n.PricingViewDetails-studentModule-1lFxy {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 16px;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-sectionName-1WSX5 {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin-bottom: 24px;\n  font-size: 24px;\n  font-weight: 600;\n  line-height: 1.33;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-subModulesFlex-3p7bL {\n  display: -ms-flexbox;\n  display: flex;\n  margin: 32px 0 32px 0;\n}\n\n/* .subsubmodules {\n  display: flex;\n}\n */\n\n.PricingViewDetails-moduleFlex-2NouI {\n  padding: 0 64px 0 64px;\n  width: 87%;\n  margin: auto;\n}\n\n/* .moduleFlex1 {\n  padding: 0 64px 0 64px;\n  width: fit-content;\n  margin: auto;\n}\n */\n\n.PricingViewDetails-modules-1dSYh {\n  padding: 0 64px 0 64px;\n  width: 100%;\n  margin: auto;\n  overflow: hidden;\n}\n\n.PricingViewDetails-arrowIcon-1I8M6 {\n  width: 10.9px;\n  height: 16px;\n}\n\n.PricingViewDetails-subModuleContainer-2F_iK {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n}\n\n.PricingViewDetails-submodulesSectionContainer-2D5Jx {\n  -ms-flex: 0.4 1;\n      flex: 0.4 1;\n  // width: 30%;\n}\n\n.PricingViewDetails-submodulesSectionContainer1-3Hz0T {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n  // width: 25%;\n  margin: 50px 0 0 0;\n  // border-left: 1px solid #d9d9d9;\n}\n\n.PricingViewDetails-imageContainer-3CA5q {\n  width: 9%;\n}\n\n/* @media screen and (min-width: 1280px) {\n  .moduleFlex{\n    width: 53.5%;\n  }\n} */\n\n@media only screen and (min-width: 991px) {\n  .PricingViewDetails-plus-25OpF {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleIcon-_NUTR {\n    display: none;\n  }\n\n  .PricingViewDetails-subModuleMobileContainer-3Xkg3 {\n    display: none;\n  }\n\n  .PricingViewDetails-plusContainer-39Pqg {\n    display: none;\n  }\n\n  /* .headerContainerMobileView {\n    display: none;\n  } */\n\n  .PricingViewDetails-headerContainers-3kGGo {\n    display: none;\n  }\n\n  .PricingViewDetails-goBackLink-mPLEA {\n    display: none;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .PricingViewDetails-modules-1dSYh {\n    padding: 0;\n  }\n\n  .PricingViewDetails-pricingHeader-2bYv- {\n    margin: 24px 0 0 0;\n    padding: 0 64px 0 64px;\n  }\n\n  .PricingViewDetails-price_title-2nqFt {\n    font-size: 32px;\n  }\n\n  .PricingViewDetails-price_content-1fVtK {\n    font-size: 14px;\n    width: 100%;\n    line-height: 1.71;\n    margin: 12px 0 48px 0;\n  }\n\n  .PricingViewDetails-whoAreYou-3T0mg {\n    font-size: 20px;\n    margin: 48px 0 24px 0;\n  }\n\n  .PricingViewDetails-goBackLink-mPLEA {\n    text-align: center;\n    font-size: 14px;\n    color: #f36;\n    text-decoration: underline;\n  }\n\n  .PricingViewDetails-adminContainerActive-1Z0JL {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    background-color: #e8e8e8;\n    border-style: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    padding: 0;\n    border-bottom-right-radius: 8px;\n    // padding: 0 49px 24px 50px;\n  }\n\n  /* .adminContainerActive:second-child {\n    border-bottom-left-radius: 8px;\n  } */\n\n  .PricingViewDetails-adminContainer-29ElQ {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    border-style: none;\n    background-color: transparent;\n    margin: 0;\n    padding: 0;\n  }\n\n  .PricingViewDetails-rightIcon-2i32Z {\n    display: none;\n  }\n\n  .PricingViewDetails-imgContainer-1Jj_u {\n    width: 10%;\n    height: 0;\n    background-color: transparent;\n    // padding: 24px 49px 24px 50px;\n    // margin-left: 40px;\n    // padding: 0;\n    // margin: 19px 0 0 0;\n  }\n\n  .PricingViewDetails-adminLabel-23wjb {\n    // width:0px;\n    font-size: 14px;\n    text-align: center;\n    margin: 19px 0 0 0;\n  }\n\n  .PricingViewDetails-moduleData-EDv-k {\n    background-color: #f7f7f7;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .PricingViewDetails-tabsContainer-3_Zwu {\n    // justify-content: flex-start;\n    margin: 0 0 20px 0;\n    width: 100%;\n    padding: 0;\n  }\n\n  .PricingViewDetails-plane-21eaT {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleFlex-2NouI {\n    padding: 0;\n  }\n\n  /* .headerContainer {\n    display: none;\n  } */\n\n  /*  .mobileMOduleContainer {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  }\n\n  .mobileMOduleContainerActive {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    border-radius: 24px;\n    width: 328px;\n    height: 44px;\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  } */\n\n  /*  .plus {\n    width: 20px;\n    height: 20px;\n  } */\n\n  /* .imageMobileUrl {\n    width: 10%;\n    height: 20px;\n  } */\n\n  /* .mobileModuleName {\n    width: 75%;\n    font-size: 14px;\n    color: #25282b;\n    text-align: left;\n  }\n\n  .mobileModuleNameActive {\n    width: 75%;\n    font-size: 14px;\n    color: #f36;\n    text-align: left;\n  } */\n\n  .PricingViewDetails-headerContainers-3kGGo {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    // justify-content: center;\n    border: none;\n    // margin: 100px 0 0 0;\n  }\n\n  .PricingViewDetails-active-2cVke {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .PricingViewDetails-adminLabelContainer-2Mroj {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .PricingViewDetails-moduleContainer-I6SSy {\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n  }\n\n  .PricingViewDetails-moduleContainerActive-2BKQt {\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    border-radius: 24px;\n  }\n\n  /* .icon {\n    width: 10%;\n    height: 20px;\n  } */\n\n  .PricingViewDetails-adminImg-2mAsS {\n    width: 16px;\n    height: 16px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .PricingViewDetails-moduleName-3StFu {\n    width: 90%;\n    font-size: 14px;\n    color: #25282b;\n  }\n\n  .PricingViewDetails-moduleNameActive-3Lzqz {\n    color: #f36;\n    width: 90%;\n  }\n\n  .PricingViewDetails-plus-25OpF {\n    width: 20px;\n    height: 20px;\n    display: -ms-flexbox;\n    display: flex;\n    justify-items: flex-end;\n    -ms-flex-align: end;\n        align-items: flex-end;\n  }\n\n  .PricingViewDetails-subModuleContainer-2F_iK {\n    display: none;\n  }\n\n  .PricingViewDetails-dataContainer-3Q7OM {\n    display: none;\n  }\n\n  .PricingViewDetails-icons-2XSLR {\n    display: none;\n  }\n\n  .PricingViewDetails-submodulesSectionContainer1-3Hz0T {\n    display: none;\n  }\n\n  .PricingViewDetails-sectionName-1WSX5 {\n    display: none;\n  }\n\n  /* .subModuleMobileContainer {\n    margin: 0 0 0 16px;\n  } */\n\n  .PricingViewDetails-subModuleName-3x1To {\n    font-size: 16px;\n    line-height: 1.5;\n    font-weight: 600;\n    color: #25282b;\n    margin: 16px 0 12px 0;\n  }\n\n  .PricingViewDetails-submoduleDataContainer-22IY- {\n    display: -ms-flexbox;\n    display: flex;\n    margin: 0 0 8px 0;\n  }\n\n  .PricingViewDetails-submoduleData-1NPL2 {\n    font-size: 12px;\n    color: #25282b;\n    text-align: left;\n    line-height: 1.67;\n    margin-left: 8px;\n  }\n\n  .PricingViewDetails-rightIcons-81roB {\n    width: 20px;\n    height: 20px;\n  }\n\n  .PricingViewDetails-iconActive-1MVuw {\n    display: none;\n  }\n\n  .PricingViewDetails-subModuleMobileContainer-3Xkg3 {\n    // height: 20%;\n    overflow-y: scroll;\n    margin: 0 0 0 16px;\n  }\n\n  .PricingViewDetails-icon-UXaHR {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleIcon-_NUTR {\n    width: 20px;\n    height: 20px;\n  }\n\n  .PricingViewDetails-headerContainer-3Lmkq {\n    display: none;\n  }\n\n  .PricingViewDetails-submodulesSectionContainer-2D5Jx {\n    display: none;\n  }\n\n  .PricingViewDetails-subModulesFlex-3p7bL {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,sBAAsB;EACtB,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,mBAAmB;EACnB,aAAa;EACb,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,6BAA6B;EAC7B,qCAAqC;EACrC,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2CAA2C;EAC3C,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,eAAe;EACf,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,2CAA2C;EAC3C,uBAAuB;CACxB;;AAED;EACE,sBAAsB;CACvB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,gBAAgB;EAChB,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,kBAAkB;EAClB,gBAAgB;EAChB,wBAAwB;EACxB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,wDAAwD;UAChD,gDAAgD;EACxD,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,WAAW;EACX,YAAY;EACZ,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,qBAAqB;EACzB,kCAAkC;CACnC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,qBAAqB;EACzB,kBAAkB;EAClB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;MACjB,aAAa;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;CACtB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;EACpB,mCAAmC;CACpC;;AAED;;;;;;GAMG;;AAEH;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,aAAa;EACjB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,qBAAqB;MACjB,aAAa;EACjB,oBAAoB;EACpB,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;CACvB;;AAED;;;GAGG;;AAEH;EACE,uBAAuB;EACvB,WAAW;EACX,aAAa;CACd;;AAED;;;;;GAKG;;AAEH;EACE,uBAAuB;EACvB,YAAY;EACZ,aAAa;EACb,iBAAiB;CAClB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,gBAAgB;MACZ,YAAY;CACjB;;AAED;EACE,gBAAgB;MACZ,YAAY;EAChB,cAAc;CACf;;AAED;EACE,gBAAgB;MACZ,YAAY;EAChB,cAAc;EACd,mBAAmB;EACnB,kCAAkC;CACnC;;AAED;EACE,UAAU;CACX;;AAED;;;;IAII;;AAEJ;EACE;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;;MAEI;;EAEJ;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;CACF;;AAED;EACE;IACE,WAAW;GACZ;;EAED;IACE,mBAAmB;IACnB,uBAAuB;GACxB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,YAAY;IACZ,kBAAkB;IAClB,sBAAsB;GACvB;;EAED;IACE,gBAAgB;IAChB,sBAAsB;GACvB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,YAAY;IACZ,2BAA2B;GAC5B;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,0BAA0B;IAC1B,mBAAmB;IACnB,yBAAyB;YACjB,iBAAiB;IACzB,UAAU;IACV,WAAW;IACX,gCAAgC;IAChC,6BAA6B;GAC9B;;EAED;;MAEI;;EAEJ;IACE,gBAAgB;IAChB,WAAW;IACX,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;IAClC,mBAAmB;IACnB,8BAA8B;IAC9B,UAAU;IACV,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,WAAW;IACX,UAAU;IACV,8BAA8B;IAC9B,gCAAgC;IAChC,qBAAqB;IACrB,cAAc;IACd,sBAAsB;GACvB;;EAED;IACE,aAAa;IACb,gBAAgB;IAChB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,0BAA0B;IAC1B,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,+BAA+B;IAC/B,mBAAmB;IACnB,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,WAAW;GACZ;;EAED;;MAEI;;EAEJ;;;;;;;;;;;;;;;;;;;;;;;;;;;MA2BI;;EAEJ;;;MAGI;;EAEJ;;;MAGI;;EAEJ;;;;;;;;;;;;MAYI;;EAEJ;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,2BAA2B;IAC3B,aAAa;IACb,uBAAuB;GACxB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;GACnC;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;GACnC;;EAED;IACE,qBAAqB;QACjB,4BAA4B;IAChC,aAAa;IACb,aAAa;IACb,oBAAoB;IACpB,qCAAqC;IACrC,uBAAuB;IACvB,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,0BAA0B;IAC1B,0BAA0B;IAC1B,oBAAoB;GACrB;;EAED;;;MAGI;;EAEJ;IACE,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;IACE,WAAW;IACX,gBAAgB;IAChB,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,wBAAwB;IACxB,oBAAoB;QAChB,sBAAsB;GAC3B;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;;MAEI;;EAEJ;IACE,gBAAgB;IAChB,iBAAiB;IACjB,iBAAiB;IACjB,eAAe;IACf,sBAAsB;GACvB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,iBAAiB;IACjB,kBAAkB;IAClB,iBAAiB;GAClB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;GACf;;EAED;IACE,eAAe;IACf,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;CACF","file":"PricingViewDetails.scss","sourcesContent":[".pricingHeader {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.price_title {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.price_content {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.whoAreYou {\n  margin: 72px 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.adminContainer {\n  cursor: pointer;\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 20px 20px;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.tabsContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin: 24px 0 72px 0;\n}\n\n.adminLabelContainer {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.adminLabel {\n  // width: 119px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 19px 38px 8px 8px;\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.imgContainer {\n  width: 40px;\n  height: 40px;\n  margin: 10px 8px 0 0;\n  padding: 10px;\n  background-color: rgba(196, 196, 196, 0.3);\n  border-radius: 50px;\n}\n\n.adminImg {\n  width: 20px;\n  height: 20px;\n  color: #25282b;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.plane {\n  margin: 0 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.moduleContainer {\n  width: 30%;\n  cursor: pointer;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 30px;\n  border-right: 1px solid rgba(0, 0, 0, 0.2);\n  background-color: #fff;\n}\n\n.moduleContainer:last-child {\n  border-right-width: 0;\n}\n\n.moduleContainerActive {\n  background-color: #f36;\n}\n\n.iconActive {\n  background-color: #f36;\n}\n\n.headerContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  // height: 48px;\n  width: 100%;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n}\n\n.icon {\n  width: 24px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.moduleName {\n  margin: 0 0 0 8px;\n  font-size: 14px;\n  letter-spacing: -0.28px;\n  color: #25282b;\n}\n\n.moduleNameActive {\n  color: #fff;\n  font-weight: 600;\n}\n\n.adminContainerActive {\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 0 20px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n          box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n  border: solid 1px #ff99b3;\n  text-align: center;\n}\n\n.rightIcon {\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  width: 16px;\n  height: 16px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.active {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.subModules {\n  cursor: pointer;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 0 24px 0;\n  font-size: 16px;\n  line-height: 1.5;\n  color: #25282b;\n}\n\n.subModuleActive {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #f36;\n}\n\n.iconDataContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  // margin-left: 1px solid #d9d9d9;\n}\n\n.iconDataContainer1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  margin-left: 10px;\n  // margin-bottom: 8px;\n}\n\n.dataContainer {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 14px;\n  line-height: 1.57;\n  text-align: left;\n  color: #25282b;\n}\n\n.icons {\n  width: 18px;\n  height: 18px;\n  margin: 6px 12px 0 0;\n}\n\n.iconContainer {\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 12px;\n  // border-right: 1px solid #d9d9d9;\n}\n\n/* .dataIconContainer {\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  column-gap: 3rem;\n  row-gap: 3rem;\n}\n */\n\n/* .verticalLine {\n  height: 420px;\n  width: 1px;\n  margin: 63px 10px 0 0;\n  background-color: #d9d9d9;\n} */\n\n.studentModule {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 16px;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.sectionName {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin-bottom: 24px;\n  font-size: 24px;\n  font-weight: 600;\n  line-height: 1.33;\n  text-align: left;\n  color: #25282b;\n}\n\n.subModulesFlex {\n  display: -ms-flexbox;\n  display: flex;\n  margin: 32px 0 32px 0;\n}\n\n/* .subsubmodules {\n  display: flex;\n}\n */\n\n.moduleFlex {\n  padding: 0 64px 0 64px;\n  width: 87%;\n  margin: auto;\n}\n\n/* .moduleFlex1 {\n  padding: 0 64px 0 64px;\n  width: fit-content;\n  margin: auto;\n}\n */\n\n.modules {\n  padding: 0 64px 0 64px;\n  width: 100%;\n  margin: auto;\n  overflow: hidden;\n}\n\n.arrowIcon {\n  width: 10.9px;\n  height: 16px;\n}\n\n.subModuleContainer {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n}\n\n.submodulesSectionContainer {\n  -ms-flex: 0.4 1;\n      flex: 0.4 1;\n  // width: 30%;\n}\n\n.submodulesSectionContainer1 {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n  // width: 25%;\n  margin: 50px 0 0 0;\n  // border-left: 1px solid #d9d9d9;\n}\n\n.imageContainer {\n  width: 9%;\n}\n\n/* @media screen and (min-width: 1280px) {\n  .moduleFlex{\n    width: 53.5%;\n  }\n} */\n\n@media only screen and (min-width: 991px) {\n  .plus {\n    display: none;\n  }\n\n  .moduleIcon {\n    display: none;\n  }\n\n  .subModuleMobileContainer {\n    display: none;\n  }\n\n  .plusContainer {\n    display: none;\n  }\n\n  /* .headerContainerMobileView {\n    display: none;\n  } */\n\n  .headerContainers {\n    display: none;\n  }\n\n  .goBackLink {\n    display: none;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .modules {\n    padding: 0;\n  }\n\n  .pricingHeader {\n    margin: 24px 0 0 0;\n    padding: 0 64px 0 64px;\n  }\n\n  .price_title {\n    font-size: 32px;\n  }\n\n  .price_content {\n    font-size: 14px;\n    width: 100%;\n    line-height: 1.71;\n    margin: 12px 0 48px 0;\n  }\n\n  .whoAreYou {\n    font-size: 20px;\n    margin: 48px 0 24px 0;\n  }\n\n  .goBackLink {\n    text-align: center;\n    font-size: 14px;\n    color: #f36;\n    text-decoration: underline;\n  }\n\n  .adminContainerActive {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    background-color: #e8e8e8;\n    border-style: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    padding: 0;\n    border-bottom-right-radius: 8px;\n    // padding: 0 49px 24px 50px;\n  }\n\n  /* .adminContainerActive:second-child {\n    border-bottom-left-radius: 8px;\n  } */\n\n  .adminContainer {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    border-style: none;\n    background-color: transparent;\n    margin: 0;\n    padding: 0;\n  }\n\n  .rightIcon {\n    display: none;\n  }\n\n  .imgContainer {\n    width: 10%;\n    height: 0;\n    background-color: transparent;\n    // padding: 24px 49px 24px 50px;\n    // margin-left: 40px;\n    // padding: 0;\n    // margin: 19px 0 0 0;\n  }\n\n  .adminLabel {\n    // width:0px;\n    font-size: 14px;\n    text-align: center;\n    margin: 19px 0 0 0;\n  }\n\n  .moduleData {\n    background-color: #f7f7f7;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .tabsContainer {\n    // justify-content: flex-start;\n    margin: 0 0 20px 0;\n    width: 100%;\n    padding: 0;\n  }\n\n  .plane {\n    display: none;\n  }\n\n  .moduleFlex {\n    padding: 0;\n  }\n\n  /* .headerContainer {\n    display: none;\n  } */\n\n  /*  .mobileMOduleContainer {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  }\n\n  .mobileMOduleContainerActive {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    border-radius: 24px;\n    width: 328px;\n    height: 44px;\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  } */\n\n  /*  .plus {\n    width: 20px;\n    height: 20px;\n  } */\n\n  /* .imageMobileUrl {\n    width: 10%;\n    height: 20px;\n  } */\n\n  /* .mobileModuleName {\n    width: 75%;\n    font-size: 14px;\n    color: #25282b;\n    text-align: left;\n  }\n\n  .mobileModuleNameActive {\n    width: 75%;\n    font-size: 14px;\n    color: #f36;\n    text-align: left;\n  } */\n\n  .headerContainers {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    // justify-content: center;\n    border: none;\n    // margin: 100px 0 0 0;\n  }\n\n  .active {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .adminLabelContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .moduleContainer {\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n  }\n\n  .moduleContainerActive {\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    border-radius: 24px;\n  }\n\n  /* .icon {\n    width: 10%;\n    height: 20px;\n  } */\n\n  .adminImg {\n    width: 16px;\n    height: 16px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .moduleName {\n    width: 90%;\n    font-size: 14px;\n    color: #25282b;\n  }\n\n  .moduleNameActive {\n    color: #f36;\n    width: 90%;\n  }\n\n  .plus {\n    width: 20px;\n    height: 20px;\n    display: -ms-flexbox;\n    display: flex;\n    justify-items: flex-end;\n    -ms-flex-align: end;\n        align-items: flex-end;\n  }\n\n  .subModuleContainer {\n    display: none;\n  }\n\n  .dataContainer {\n    display: none;\n  }\n\n  .icons {\n    display: none;\n  }\n\n  .submodulesSectionContainer1 {\n    display: none;\n  }\n\n  .sectionName {\n    display: none;\n  }\n\n  /* .subModuleMobileContainer {\n    margin: 0 0 0 16px;\n  } */\n\n  .subModuleName {\n    font-size: 16px;\n    line-height: 1.5;\n    font-weight: 600;\n    color: #25282b;\n    margin: 16px 0 12px 0;\n  }\n\n  .submoduleDataContainer {\n    display: -ms-flexbox;\n    display: flex;\n    margin: 0 0 8px 0;\n  }\n\n  .submoduleData {\n    font-size: 12px;\n    color: #25282b;\n    text-align: left;\n    line-height: 1.67;\n    margin-left: 8px;\n  }\n\n  .rightIcons {\n    width: 20px;\n    height: 20px;\n  }\n\n  .iconActive {\n    display: none;\n  }\n\n  .subModuleMobileContainer {\n    // height: 20%;\n    overflow-y: scroll;\n    margin: 0 0 0 16px;\n  }\n\n  .icon {\n    display: none;\n  }\n\n  .moduleIcon {\n    width: 20px;\n    height: 20px;\n  }\n\n  .headerContainer {\n    display: none;\n  }\n\n  .submodulesSectionContainer {\n    display: none;\n  }\n\n  .subModulesFlex {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"pricingHeader": "PricingViewDetails-pricingHeader-2bYv-",
	"price_title": "PricingViewDetails-price_title-2nqFt",
	"price_content": "PricingViewDetails-price_content-1fVtK",
	"whoAreYou": "PricingViewDetails-whoAreYou-3T0mg",
	"adminContainer": "PricingViewDetails-adminContainer-29ElQ",
	"tabsContainer": "PricingViewDetails-tabsContainer-3_Zwu",
	"adminLabelContainer": "PricingViewDetails-adminLabelContainer-2Mroj",
	"adminLabel": "PricingViewDetails-adminLabel-23wjb",
	"imgContainer": "PricingViewDetails-imgContainer-1Jj_u",
	"adminImg": "PricingViewDetails-adminImg-2mAsS",
	"plane": "PricingViewDetails-plane-21eaT",
	"moduleContainer": "PricingViewDetails-moduleContainer-I6SSy",
	"moduleContainerActive": "PricingViewDetails-moduleContainerActive-2BKQt",
	"iconActive": "PricingViewDetails-iconActive-1MVuw",
	"headerContainer": "PricingViewDetails-headerContainer-3Lmkq",
	"icon": "PricingViewDetails-icon-UXaHR",
	"moduleName": "PricingViewDetails-moduleName-3StFu",
	"moduleNameActive": "PricingViewDetails-moduleNameActive-3Lzqz",
	"adminContainerActive": "PricingViewDetails-adminContainerActive-1Z0JL",
	"rightIcon": "PricingViewDetails-rightIcon-2i32Z",
	"active": "PricingViewDetails-active-2cVke",
	"subModules": "PricingViewDetails-subModules-1R5-x",
	"subModuleActive": "PricingViewDetails-subModuleActive-1hEpJ",
	"iconDataContainer": "PricingViewDetails-iconDataContainer-i2G6T",
	"iconDataContainer1": "PricingViewDetails-iconDataContainer1-ur6rZ",
	"dataContainer": "PricingViewDetails-dataContainer-3Q7OM",
	"icons": "PricingViewDetails-icons-2XSLR",
	"iconContainer": "PricingViewDetails-iconContainer-1QcNN",
	"studentModule": "PricingViewDetails-studentModule-1lFxy",
	"sectionName": "PricingViewDetails-sectionName-1WSX5",
	"subModulesFlex": "PricingViewDetails-subModulesFlex-3p7bL",
	"moduleFlex": "PricingViewDetails-moduleFlex-2NouI",
	"modules": "PricingViewDetails-modules-1dSYh",
	"arrowIcon": "PricingViewDetails-arrowIcon-1I8M6",
	"subModuleContainer": "PricingViewDetails-subModuleContainer-2F_iK",
	"submodulesSectionContainer": "PricingViewDetails-submodulesSectionContainer-2D5Jx",
	"submodulesSectionContainer1": "PricingViewDetails-submodulesSectionContainer1-3Hz0T",
	"imageContainer": "PricingViewDetails-imageContainer-3CA5q",
	"plus": "PricingViewDetails-plus-25OpF",
	"moduleIcon": "PricingViewDetails-moduleIcon-_NUTR",
	"subModuleMobileContainer": "PricingViewDetails-subModuleMobileContainer-3Xkg3",
	"plusContainer": "PricingViewDetails-plusContainer-39Pqg",
	"headerContainers": "PricingViewDetails-headerContainers-3kGGo",
	"goBackLink": "PricingViewDetails-goBackLink-mPLEA",
	"moduleData": "PricingViewDetails-moduleData-EDv-k",
	"subModuleName": "PricingViewDetails-subModuleName-3x1To",
	"submoduleDataContainer": "PricingViewDetails-submoduleDataContainer-22IY-",
	"submoduleData": "PricingViewDetails-submoduleData-1NPL2",
	"rightIcons": "PricingViewDetails-rightIcons-81roB"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/Constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanDetails", function() { return PlanDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "demo", function() { return demo; });
const PlanDetails = {
  admin: {
    label: 'Admin/Teacher',
    value: 'admin',
    imgUrl: '/images/admin.svg',
    modules: {
      onlinetest: {
        label: 'Online Tests',
        value: 'onlinetest',
        imageUrl: '/images/Tests.svg',
        mobileImageUrl: '/images/PricingDetails/TestsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeTests.svg',
        subModules: {
          tests: {
            label: 'Online Testing Platform',
            value: 'tests',
            data: [{
              label: 'Test Creation',
              value: 'testcreation'
            }, {
              label: 'Edit Test',
              value: 'edittest'
            }, {
              label: 'Delete Test',
              value: 'deletetest'
            }, {
              label: 'Grace Period',
              value: 'graceperiod'
            }, {
              label: 'Assign Question Paper to Teachers',
              value: 'assignquestionpapertoteacher'
            }, {
              label: 'Upload your own Question Paper in .docx',
              value: 'uploadyourownquestionpaperindocx'
            }, {
              label: 'Upload your own question paper in 5 min',
              value: 'uploadyourownquestionpaperin5min'
            }, {
              label: 'Supports any type of questions (single answer, multiple answer, paragraph etc.,',
              value: 'supportsanytypeofqustions'
            }, {
              label: 'Partial Paper',
              value: 'partialpaper'
            }, {
              label: 'Preview like Test Platform',
              value: 'previewliketestplatform'
            }, {
              label: 'Proctoring',
              value: 'proctoring'
            }, {
              label: 'Live Attendance / Attendance',
              value: 'liveattendanceattendance'
            }, {
              label: 'Downloadable attendance sheets',
              value: 'downloadableattendancesheets'
            }, {
              label: 'Downloadable question paper, key and solutions',
              value: 'downloadquestionpaperkeysolutions'
            }, {
              label: 'Key Editing',
              value: 'keyediting'
            }, {
              label: 'Auto Reports & Analysis',
              value: 'autoreportsanalysis'
            }, {
              label: 'Results generation for Offline exam',
              value: 'resultsgenerationforofflineexams'
            }, {
              label: 'Supports Online / Offline tests',
              value: 'supportsonlineofflinetests'
            }, {
              label: 'Flexible test times',
              value: 'flexibletesttimes '
            }, {
              label: 'Best Hierarcy system',
              value: 'besthierarcysystem'
            }, {
              label: 'Parser can convert text / images / equations / graphs',
              value: 'parsercanconverttextimagesequationsgraphs'
            }]
          },
          qustionbank: {
            label: 'Question Bank',
            value: 'qustionbank',
            data: [{
              label: 'Question bank with 1,50,000+ Questions with Key and Solutions',
              value: 'questionbankwith1,50,000+questionswithkeyandsolutions'
            }, {
              label: 'Questions in 3 difficulty levels (Easy / Medium / High)',
              value: 'Questionsin3difficultylevels'
            }, {
              label: 'Question tagging',
              value: 'questiontagging'
            }]
          },
          papers: {
            label: 'Papers',
            value: 'papers',
            data: [{
              label: 'Create Question Paper',
              value: 'createquestionpaper'
            }, {
              label: 'Select questions',
              value: 'selectquestions'
            }, {
              label: 'Question paper resume feature',
              value: 'questionpaperresumefeature'
            }, {
              label: 'Partial Paper feature',
              value: 'partialpaperfeature'
            }, {
              label: 'Design personalized question paper/s with Institute Name, Logo & Watermark',
              value: 'designpersonalizedquestionpaper/swithinstitutenamelogo&watermark'
            }]
          },
          practice: {
            label: 'Practice',
            value: 'practice',
            data: [{
              label: 'Unlimited Practice tests',
              value: 'unlimitedpracticetests'
            }, {
              label: 'Questions based practice',
              value: 'questionsbasedpractice'
            }, {
              label: 'Time based Practice',
              value: 'timebasedpractice'
            }, {
              label: 'Detailed analysis of all practice tests',
              value: 'detailedanalysisofallpracticetests'
            }]
          },
          analysis: {
            label: 'Analysis',
            value: 'analysis',
            data: [{
              label: 'Marks Analyssis',
              value: 'markanalyssis'
            }, {
              label: 'Topic wise Analysis',
              value: 'topicwiseanalysis'
            }, {
              label: 'Error Analysis',
              value: 'erroranalysis'
            }]
          },
          reports: {
            label: 'Reports',
            value: 'reports',
            data: [{
              label: 'Test Results',
              value: 'testresults'
            }, {
              label: 'C_W_U Report',
              value: 'cwcreport'
            }, {
              label: 'Marks Distribution',
              value: 'marksdistribution'
            }, {
              label: 'Error Count',
              value: 'errorcount'
            }, {
              label: 'Weak Subjects',
              value: 'Weaksubject'
            }]
          }
        }
      },
      liveclasses: {
        label: 'Live classes',
        value: 'liveclasses',
        imageUrl: '/images/Live.svg',
        mobileImageUrl: '/images/PricingDetails/LiveMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeLive.svg',
        subModules: {
          live: {
            label: 'Live',
            value: 'live',
            data: [{
              label: 'Interractive Live Class Creation (integrated with zoom)',
              value: 'interractiveliveclasscreation'
            }, {
              label: 'Recording Class creation',
              value: 'recordingclasscreation'
            }, {
              label: 'Recurring classes',
              value: 'recurringclasses'
            }, {
              label: 'Attach file while class creating (image / pdf)',
              value: 'attachfilewhileclasscreating'
            }, {
              label: 'Edit Class',
              value: 'editclass'
            }, {
              label: 'Delete Class',
              value: 'deleteclass'
            }, {
              label: 'Live Attendance',
              value: 'liveattendance'
            }, {
              label: 'Watch later',
              value: 'watchlater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }, {
              label: 'Download classes list',
              value: 'downloadclasseslist'
            }, {
              label: 'Secure Notes, Downloadable links',
              value: 'securenotesdownloadablelinks'
            }, {
              label: 'Student Management System',
              value: 'studentmanagementsystem'
            }, {
              label: 'Best Hierarcy system',
              value: 'besthierarcysystem'
            }]
          },
          securecontent: {
            label: 'Restricted Login',
            value: 'securecontent',
            data: [{
              label: 'Content visible only inside App',
              value: 'contentvisibleonlyinsideapp'
            }, {
              label: 'Video Link sharing disabled',
              value: 'videolinksharingdisabled'
            }, {
              label: 'Screen Recording Disabled',
              value: 'screenrecordingdisabled'
            }, {
              label: 'Screenshot Disabled',
              value: 'screenshotdisabled'
            }, {
              label: 'eNotes Downloading Enabled/Disabled',
              value: 'enotesdownloadingenableddisabled'
            }, {
              label: 'Only Registered Students can enter with known identity, so no nuisance during class unlike Zoom',
              value: 'onlyregisteredstudentscanenterwithknownidentitysononuisanceduringclassunlikezoom'
            }]
          }
        }
      },
      assignments: {
        label: 'Assignments',
        value: 'assignments',
        imageUrl: '/images/Assignments.svg',
        mobileImageUrl: '/images/PricingDetails/AssignmentsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeAssignments.svg',
        subModules: {
          assignment: {
            label: 'Assignments',
            value: 'assignment',
            data: [{
              label: 'Easy Creation of assignment',
              value: 'easycreationofassignment'
            }, {
              label: 'Submission status',
              value: 'submissionstatus'
            }, {
              label: 'Awarding Marks',
              value: 'awardingmarks'
            }, {
              label: 'Easy to Evaluate',
              value: 'easytoevaluate'
            }, {
              label: 'Helpful Toolbox',
              value: 'helpfultoolbox'
            }, {
              label: 'Anytime, Anywhere Assignments',
              value: 'anytimeanywhereassignments'
            }, {
              label: 'Grade Faster with Easy-to-Use Tools',
              value: 'gradefasterwitheasytousetools'
            }]
          }
        }
      },
      doubts: {
        label: 'Doubts',
        value: 'doubts',
        imageUrl: '/images/Doubts.svg',
        mobileImageUrl: '/images/PricingDetails/DoubtsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeDoubts.svg',
        subModules: {
          doubt: {
            label: 'Doubts',
            value: 'doubt',
            data: [{
              label: 'Easy to clear doubts (Chat / upload image / insert link)',
              value: 'easytocleardoubts'
            }, {
              label: 'Bookmark doubts',
              value: 'bookmarkdoubts'
            }, {
              label: 'Reopen doubts',
              value: 'reopendoubts'
            }, {
              label: 'Check later',
              value: 'checklater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }]
          }
        }
      },
      connect: {
        label: 'Connect',
        value: 'connect',
        imageUrl: '/images/Connect.svg',
        mobileImageUrl: '/images/PricingDetails/ConnectMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeConnect.svg',
        subModules: {
          schedule: {
            label: 'Schedule',
            value: 'schedule'
          },
          notifications: {
            label: 'Notifications',
            value: 'notifications'
          },
          messanger: {
            label: 'Messanger',
            value: 'messanger'
          }
        }
      }
    }
  },
  student: {
    label: 'Student',
    value: 'student',
    imgUrl: '/images/student.svg',
    modules: {
      onlinetest: {
        label: 'Online Tests',
        value: 'onlinetest',
        imageUrl: '/images/Tests.svg',
        mobileImageUrl: '/images/PricingDetails/TestsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeTests.svg',
        subModules: {
          tests: {
            label: 'Tests',
            value: 'tests',
            data: [{
              label: 'Easy to check Upcoming Tests',
              value: 'easytocheckupcomingtests'
            }, {
              label: 'Easy to attempt',
              value: 'easytoattempt'
            }, {
              label: 'Answered / Not Answered / Not visited lists can be checked easily',
              value: 'Answerednotanswerednotvisitedlistscanbecheckedeasily'
            }, {
              label: 'Subject wise questions response can be checked easily by colours',
              value: 'subjectwisequestionsresponsecanbecheckedeasilybycolours'
            }, {
              label: 'Instant Results',
              value: 'instantresults'
            }]
          },
          analysis: {
            label: 'Analysis',
            value: 'analysis',
            data: [{
              label: 'Overall Test Performance',
              value: 'overalltestperformance'
            }, {
              label: 'Attempt & Accuracy rate in all tests',
              value: 'attemptaccuracyrateinalltests'
            }, {
              label: 'Tests Performance in all topics',
              value: 'testsperformanceinalltopics'
            }, {
              label: 'Overall / Subject wise Marks Trend',
              value: 'overallsubjectwisemarkstrend'
            }, {
              label: 'Overall / Subject wise Attempt and Accuracy trend',
              value: 'overallsubjectwiseattemptandaccuracytrend'
            }, {
              label: 'Easily find completed tests',
              value: 'easilyfindcompletedtests'
            }, {
              label: 'Individual and Overall Summary',
              value: 'individualandoverallsummary'
            }, {
              label: 'Subject wise marks of all / individual tests',
              value: 'subjectwisemarksofallindividualtests'
            }, {
              label: 'Attempt & Accuracy rate in all subjects',
              value: 'attemptaccuracyrateinallsubjects'
            }, {
              label: 'Questions distribution in overall & subject wise in individual / all tests',
              value: 'questionsdistributioninoverallsubjectwiseinindividualalltests'
            }, {
              label: 'Topic / sub topic / question level analysis',
              value: 'topicsubtopicquestionlevelanalysis'
            }, {
              label: 'Question wise behaviour analysis',
              value: 'questionwisebehaviouranalysis'
            }, {
              label: 'Question / subject / difficult level Time spent',
              value: 'questionsubjectdifficultleveltimespent'
            }, {
              label: 'Time spent on Correct / wrong / unattempted questions',
              value: 'timespentoncorrectwrongunattemptedquestions'
            }, {
              label: 'Can Compare wit Topper',
              value: 'cancomparewittopper'
            }]
          },
          practice: {
            label: 'Practice',
            value: 'practice',
            data: [{
              label: 'Practice available for 11th, 12th and both',
              value: 'practiceavailablefor11th12thandboth'
            }, {
              label: 'Supports - JEE Main / NEET / EAMCET',
              value: 'supportsjeemainneeteamcet'
            }, {
              label: 'Unlimited Practice tests',
              value: 'unlimitedpracticetests'
            }, {
              label: 'Full syllabus Tests',
              value: 'fullsyllabustests'
            }, {
              label: 'Subject wise practice',
              value: 'subjectwisepractice'
            }, {
              label: 'chapter wise practice',
              value: 'chapterwisepractice'
            }, {
              label: 'Questions based practice',
              value: 'questionsbasedpractice'
            }, {
              label: 'Time based Practice',
              value: 'timebasedpractice'
            }, {
              label: 'Instant Results',
              value: 'instantresults'
            }, {
              label: 'Detailed analysis of all practice tests',
              value: 'detailedanalysisofallpracticetests'
            }]
          }
        }
      },
      liveclasses: {
        label: 'Live classes',
        value: 'liveclasses',
        imageUrl: '/images/Live.svg',
        mobileImageUrl: '/images/PricingDetails/LiveMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeLive.svg',
        subModules: {
          live: {
            label: 'Live',
            value: 'live',
            data: [{
              label: 'Signle Click to Join class',
              value: 'signleclicktojoinclass'
            }, {
              label: 'No need to download any apps (zoom integrated)',
              value: 'noneedtodownloadanyapps'
            }, {
              label: 'Watch later',
              value: 'watchlater'
            }, {
              label: 'Smart Filters',
              value: 'Smartfilters'
            }]
          }
        }
      },
      assignments: {
        label: 'Assignments',
        value: 'assignments',
        imageUrl: '/images/Assignments.svg',
        mobileImageUrl: '/images/PricingDetails/AssignmentsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeAssignments.svg',
        subModules: {
          assignment: {
            label: 'Assignments',
            value: 'assignment',
            data: [{
              label: 'Easy submission',
              value: 'easysubmission'
            }, {
              label: 'Easy to check evaluation',
              value: 'easytocheckevaluation'
            }]
          }
        }
      },
      doubts: {
        label: 'Doubts',
        value: 'doubts',
        imageUrl: '/images/Doubts.svg',
        mobileImageUrl: '/images/PricingDetails/DoubtsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeDoubts.svg',
        subModules: {
          doubt: {
            label: 'Doubts',
            value: 'doubt',
            data: [{
              label: 'Easy to ask doubts (Chat / upload image / insert link)',
              value: 'easytoaskdoubts'
            }, {
              label: 'Bookmark doubts',
              value: 'bookmarkdoubts'
            }, {
              label: 'Reopen doubts',
              value: 'reopendoubts'
            }, {
              label: 'Check later',
              value: 'checklater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }]
          }
        }
      }
    }
  }
};
const demo = () => {};

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _Constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/Constants.js");
/* harmony import */ var _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss");
/* harmony import */ var _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class PricingViewDetails extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleResize", () => {
      const {
        mobile
      } = this.state; // console.log("function called")

      if (window.innerWidth < 990) {
        this.setState({
          mobile: !mobile
        });
      } else {
        this.setState({
          mobile
        });
      }
    });

    _defineProperty(this, "showRole", value => {
      this.setState({
        show: !true
      });
      const {
        role
      } = this.state;
      const plan = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][value].modules;
      const first = Object.values(plan || {});
      const firstMod = first[0].value;
      const subMod = plan[firstMod].subModules;
      const firstSub = Object.values(subMod || {});
      const firstOne = firstSub[0].value;

      if (value !== role) {
        this.setState({
          role: value,
          activeModule: firstMod,
          activeSubmodule: firstOne
        });
      } else {
        this.setState({
          role: value,
          activeModule: firstMod,
          activeSubmodule: firstOne
        });
      }
    });

    _defineProperty(this, "showModules", key => {
      const {
        role,
        activeModule
      } = this.state;
      const mod = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      const sub = mod[key].subModules;
      const submodule = Object.values(sub || {});

      if (key !== activeModule) {
        this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });
      } else {
        this.setState({
          activeModule: key
        });
      }
    });

    _defineProperty(this, "showSubmodule", value => {
      const {
        activeSubmodule
      } = this.state;

      if (value !== activeSubmodule) {
        this.setState({
          activeSubmodule: value
        });
      }

      this.setState({
        activeSubmodule: value
      });
    });

    _defineProperty(this, "showData", value => {
      const {
        activeSection
      } = this.state;

      if (value !== activeSection) {
        this.setState({
          activeSection: value
        });
      } else {
        this.setState({
          activeSection: value
        });
      }
    });

    _defineProperty(this, "displaySections", () => {
      const {
        role,
        activeModule,
        activeSubmodule
      } = this.state;
      const activeSubModuleName = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule].label;
      const sectionData = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submodulesSectionContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sectionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, activeSubModuleName), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, Object.values(sectionData.data || {}).map((d, index) => {
        const below = index < 10;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconDataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        }, below && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 108
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Icons.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icons,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 114
          },
          __self: this
        }, d.label)));
      })));
    });

    _defineProperty(this, "displaySection", () => {
      const {
        role,
        activeModule,
        activeSubmodule
      } = this.state;
      const sectionData = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submodulesSectionContainer1,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, Object.values(sectionData.data || {}).map((d, index) => {
        const below = index >= 10;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconDataContainer1,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 135
          },
          __self: this
        }, below && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 137
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Icons.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icons,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 138
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 143
          },
          __self: this
        }, d.label)));
      }));
    });

    _defineProperty(this, "displaySubModules", () => {
      const {
        role,
        activeModule,
        activeSubmodule
      } = this.state;
      const filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, Object.values(filter || {}).map(each => {
        const isActive = activeSubmodule === each.value;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.studentModule,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 161
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imageContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 162
          },
          __self: this
        }, isActive ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/PricingDetails/Arrow.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.arrowIcon,
          alt: "arrow",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 164
          },
          __self: this
        }) : null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModules} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModules,
          onClick: () => {
            this.showSubmodule(each.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 171
          },
          __self: this
        }, each.label));
      }));
    });

    _defineProperty(this, "showModule", key => {
      const {
        role,
        activeModule
      } = this.state;
      const mod = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      const sub = mod[key].subModules;
      const submodule = Object.values(sub || {});

      if (key !== activeModule) {
        this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });
        /* this.setState(prevState => ({
          show: !prevState.show,
        }));   */

        this.setState({
          show: !true
        });
      } else {
        this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });
        this.setState(prevState => ({
          show: !prevState.show
        }));
      }
    });

    _defineProperty(this, "displayModule", () => {
      const {
        role,
        activeModule,
        show
      } = this.state;
      const filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainers,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }, Object.values(filter).map(item => {
        const isActiveModule = activeModule === item.value;
        const showAll = activeModule === item.value;
        const imageUrl = isActiveModule ? item.activeImageUrl : item.imageUrl;
        const imageMobileUrl = isActiveModule && !show ? item.mobileImageUrl : item.imageUrl;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 224
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActiveModule && !show ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainerActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer,
          onClick: () => {
            this.showModule(item.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 225
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageUrl,
          className: isActiveModule ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon,
          alt: "img",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 236
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageMobileUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleIcon,
          alt: "mobileIcon",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 243
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: isActiveModule && !show ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleNameActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 248
          },
          __self: this
        }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plusContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 257
          },
          __self: this
        }, isActiveModule && !show ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/minus.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plus,
          alt: "minus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 259
          },
          __self: this
        }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/plus.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plus,
          alt: "plus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 265
          },
          __self: this
        }))), isActiveModule && !show && showAll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 270
          },
          __self: this
        }, Object.values(item.subModules || {}).map(each => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleMobileContainer} custom-scrollbar`,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 272
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 275
          },
          __self: this
        }, each.label), Object.values(each.data || {}).map(data => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submoduleDataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 277
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Icons.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rightIcons,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 278
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submoduleData,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 283
          },
          __self: this
        }, data.label)))))));
      }));
    });

    _defineProperty(this, "displayModules", () => {
      const {
        role,
        activeModule
      } = this.state;
      const filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 301
        },
        __self: this
      }, Object.values(filter).map(item => {
        const isActiveModule = activeModule === item.value;
        const imageUrl = isActiveModule ? item.activeImageUrl : item.imageUrl;
        const imageMobileUrl = isActiveModule ? item.mobileImageUrl : item.imageUrl;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActiveModule ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainerActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer,
          onClick: () => {
            this.showModules(item.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 309
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageUrl,
          className: isActiveModule ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon,
          alt: "img",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 320
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageMobileUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleIcon,
          alt: "mobile",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 327
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: isActiveModule ? `${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName} ${_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleNameActive}` : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 328
          },
          __self: this
        }, item.label));
      }));
    });

    _defineProperty(this, "displayTypeOfUser", () => {
      const {
        role
      } = this.state;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tabsContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        },
        __self: this
      }, Object.values(_Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"]).map(each => {
        const isActive = role === each.value;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminContainerActive : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminContainer,
          onClick: () => {
            this.showRole(each.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 351
          },
          __self: this
        }, isActive ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Shape.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rightIcon,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 359
          },
          __self: this
        }) : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminLabelContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 365
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imgContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 366
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: each.imgUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminImg,
          alt: "student",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 367
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminLabel,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 369
          },
          __self: this
        }, each.label)));
      }));
    });

    _defineProperty(this, "displayPlanes", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plane,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 378
      },
      __self: this
    }, "Every Plan includes"));

    _defineProperty(this, "displayPricingHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.pricingHeader,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 381
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 382
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 383
      },
      __self: this
    }, "Choose your edition."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 384
      },
      __self: this
    }, "Try it free for 7 days.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 386
      },
      __self: this
    }, "Egnify plans start as low as Rs.45/- per student per month.")));

    _defineProperty(this, "dispalyWhoAreYou", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 393
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whoAreYou,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 394
      },
      __self: this
    }, "Who are you?")));

    _defineProperty(this, "dispalyGoBackLink", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/pricing",
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.goBackLink,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 399
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 400
      },
      __self: this
    }, "back to pricing details")));

    this.state = {
      role: 'admin',
      activeModule: 'onlinetest',
      activeSubmodule: 'tests',
      mobile: false,
      show: false
    };
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.modules,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 406
      },
      __self: this
    }, this.displayPricingHeader(), this.dispalyGoBackLink(), this.dispalyWhoAreYou(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleData,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 410
      },
      __self: this
    }, this.displayTypeOfUser(), this.displayPlanes(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleFlex,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 413
      },
      __self: this
    }, this.displayModules(), this.displayModule(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModulesFlex,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 417
      },
      __self: this
    }, this.displaySubModules(), this.displaySections(), this.displaySection()))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(PricingViewDetails));

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _PricingViewDetails__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/index.js";




async function action() {
  return {
    title: ['GetRanks by Egnify: Assessment & Analytics Platform'],
    chunks: ['PricingDetails'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PricingViewDetails__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1ByaWNpbmdEZXRhaWxzLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvUHJpY2luZ1ZpZXdEZXRhaWxzL1ByaWNpbmdWaWV3RGV0YWlscy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1ByaWNpbmdWaWV3RGV0YWlscy9Db25zdGFudHMuanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvUHJpY2luZ1ZpZXdEZXRhaWxzL1ByaWNpbmdWaWV3RGV0YWlscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9QcmljaW5nVmlld0RldGFpbHMvUHJpY2luZ1ZpZXdEZXRhaWxzLnNjc3M/OTViOCIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9QcmljaW5nVmlld0RldGFpbHMvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5QcmljaW5nVmlld0RldGFpbHMtcHJpY2luZ0hlYWRlci0yYll2LSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDUlO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNlX3RpdGxlLTJucUZ0IHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDU2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1wcmljZV9jb250ZW50LTFmVnRLIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtd2hvQXJlWW91LTNUMG1nIHtcXG4gIG1hcmdpbjogNzJweCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkNvbnRhaW5lci0yOUVsUSB7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogMjI2cHg7XFxuICBoZWlnaHQ6IDgycHg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW46IDAgNDBweCAwIDA7XFxuICBwYWRkaW5nOiAxMHB4IDEwcHggMjBweCAyMHB4O1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy10YWJzQ29udGFpbmVyLTNfWnd1IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbjogMjRweCAwIDcycHggMDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkxhYmVsQ29udGFpbmVyLTJNcm9qIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkxhYmVsLTIzd2piIHtcXG4gIC8vIHdpZHRoOiAxMTlweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMTlweCAzOHB4IDhweCA4cHg7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1pbWdDb250YWluZXItMUpqX3Uge1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICBtYXJnaW46IDEwcHggOHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE5NiwgMTk2LCAxOTYsIDAuMyk7XFxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluSW1nLTJtQXNTIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1wbGFuZS0yMWVhVCB7XFxuICBtYXJnaW46IDAgMCAyNHB4IDA7XFxuICBmb250LXNpemU6IDI4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlQ29udGFpbmVyLUk2U1N5IHtcXG4gIHdpZHRoOiAzMCU7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiAxMnB4IDMwcHg7XFxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUNvbnRhaW5lci1JNlNTeTpsYXN0LWNoaWxkIHtcXG4gIGJvcmRlci1yaWdodC13aWR0aDogMDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVDb250YWluZXJBY3RpdmUtMkJLUXQge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1pY29uQWN0aXZlLTFNVnV3IHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtaGVhZGVyQ29udGFpbmVyLTNMbWtxIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC8vIGhlaWdodDogNDhweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb24tVVhhSFIge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lLTNTdEZ1IHtcXG4gIG1hcmdpbjogMCAwIDAgOHB4O1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjI4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lQWN0aXZlLTNMenF6IHtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkNvbnRhaW5lckFjdGl2ZS0xWjBKTCB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogMjI2cHg7XFxuICBoZWlnaHQ6IDgycHg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW46IDAgNDBweCAwIDA7XFxuICBwYWRkaW5nOiAxMHB4IDEwcHggMCAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMTZweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDI1NSwgNTEsIDEwMiwgMC4xNik7XFxuICBib3JkZXI6IHNvbGlkIDFweCAjZmY5OWIzO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXJpZ2h0SWNvbi0yaTMyWiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDhweDtcXG4gIHJpZ2h0OiA4cHg7XFxuICB3aWR0aDogMTZweDtcXG4gIGhlaWdodDogMTZweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFjdGl2ZS0yY1ZrZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlcy0xUjUteCB7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW46IDAgMCAyNHB4IDA7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlQWN0aXZlLTFoRXBKIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25EYXRhQ29udGFpbmVyLWkyRzZUIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHJvdztcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHJvdztcXG4gIC8vIG1hcmdpbi1sZWZ0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1pY29uRGF0YUNvbnRhaW5lcjEtdXI2cloge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogcm93O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogcm93O1xcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XFxuICAvLyBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtZGF0YUNvbnRhaW5lci0zUTdPTSB7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMS41NztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1pY29ucy0yWFNMUiB7XFxuICB3aWR0aDogMThweDtcXG4gIGhlaWdodDogMThweDtcXG4gIG1hcmdpbjogNnB4IDEycHggMCAwO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25Db250YWluZXItMVFjTk4ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gIC8vIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNkOWQ5ZDk7XFxufVxcblxcbi8qIC5kYXRhSWNvbkNvbnRhaW5lciB7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIGNvbHVtbi1nYXA6IDNyZW07XFxuICByb3ctZ2FwOiAzcmVtO1xcbn1cXG4gKi9cXG5cXG4vKiAudmVydGljYWxMaW5lIHtcXG4gIGhlaWdodDogNDIwcHg7XFxuICB3aWR0aDogMXB4O1xcbiAgbWFyZ2luOiA2M3B4IDEwcHggMCAwO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q5ZDlkOTtcXG59ICovXFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdHVkZW50TW9kdWxlLTFsRnh5IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtc2VjdGlvbk5hbWUtMVdTWDUge1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS4zMztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVzRmxleC0zcDdiTCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW46IDMycHggMCAzMnB4IDA7XFxufVxcblxcbi8qIC5zdWJzdWJtb2R1bGVzIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcbiAqL1xcblxcbi5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlRmxleC0yTm91SSB7XFxuICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgd2lkdGg6IDg3JTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLyogLm1vZHVsZUZsZXgxIHtcXG4gIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBtYXJnaW46IGF1dG87XFxufVxcbiAqL1xcblxcbi5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlcy0xZFNZaCB7XFxuICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFycm93SWNvbi0xSThNNiB7XFxuICB3aWR0aDogMTAuOXB4O1xcbiAgaGVpZ2h0OiAxNnB4O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZUNvbnRhaW5lci0yRl9pSyB7XFxuICAtbXMtZmxleDogMC4zIDE7XFxuICAgICAgZmxleDogMC4zIDE7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXItMkQ1Sngge1xcbiAgLW1zLWZsZXg6IDAuNCAxO1xcbiAgICAgIGZsZXg6IDAuNCAxO1xcbiAgLy8gd2lkdGg6IDMwJTtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lcjEtM0h6MFQge1xcbiAgLW1zLWZsZXg6IDAuMyAxO1xcbiAgICAgIGZsZXg6IDAuMyAxO1xcbiAgLy8gd2lkdGg6IDI1JTtcXG4gIG1hcmdpbjogNTBweCAwIDAgMDtcXG4gIC8vIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1pbWFnZUNvbnRhaW5lci0zQ0E1cSB7XFxuICB3aWR0aDogOSU7XFxufVxcblxcbi8qIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyODBweCkge1xcbiAgLm1vZHVsZUZsZXh7XFxuICAgIHdpZHRoOiA1My41JTtcXG4gIH1cXG59ICovXFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTFweCkge1xcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1wbHVzLTI1T3BGIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlSWNvbi1fTlVUUiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lci0zWGtnMyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXBsdXNDb250YWluZXItMzlQcWcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLyogLmhlYWRlckNvbnRhaW5lck1vYmlsZVZpZXcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfSAqL1xcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1oZWFkZXJDb250YWluZXJzLTNrR0dvIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtZ29CYWNrTGluay1tUExFQSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlcy0xZFNZaCB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNpbmdIZWFkZXItMmJZdi0ge1xcbiAgICBtYXJnaW46IDI0cHggMCAwIDA7XFxuICAgIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNlX3RpdGxlLTJucUZ0IHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1wcmljZV9jb250ZW50LTFmVnRLIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNzE7XFxuICAgIG1hcmdpbjogMTJweCAwIDQ4cHggMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtd2hvQXJlWW91LTNUMG1nIHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBtYXJnaW46IDQ4cHggMCAyNHB4IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWdvQmFja0xpbmstbVBMRUEge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkNvbnRhaW5lckFjdGl2ZS0xWjBKTCB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgaGVpZ2h0OiA2NXB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XFxuICAgIGJvcmRlci1zdHlsZTogbm9uZTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDhweDtcXG4gICAgLy8gcGFkZGluZzogMCA0OXB4IDI0cHggNTBweDtcXG4gIH1cXG5cXG4gIC8qIC5hZG1pbkNvbnRhaW5lckFjdGl2ZTpzZWNvbmQtY2hpbGQge1xcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA4cHg7XFxuICB9ICovXFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluQ29udGFpbmVyLTI5RWxRIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB3aWR0aDogNTAlO1xcbiAgICBoZWlnaHQ6IDY1cHg7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICAgIGJvcmRlci1zdHlsZTogbm9uZTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtcmlnaHRJY29uLTJpMzJaIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtaW1nQ29udGFpbmVyLTFKal91IHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAwO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgLy8gcGFkZGluZzogMjRweCA0OXB4IDI0cHggNTBweDtcXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwcHg7XFxuICAgIC8vIHBhZGRpbmc6IDA7XFxuICAgIC8vIG1hcmdpbjogMTlweCAwIDAgMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtYWRtaW5MYWJlbC0yM3dqYiB7XFxuICAgIC8vIHdpZHRoOjBweDtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogMTlweCAwIDAgMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlRGF0YS1FRHYtayB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy10YWJzQ29udGFpbmVyLTNfWnd1IHtcXG4gICAgLy8ganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICBtYXJnaW46IDAgMCAyMHB4IDA7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1wbGFuZS0yMWVhVCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUZsZXgtMk5vdUkge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLyogLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAvKiAgLm1vYmlsZU1PZHVsZUNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgICBwYWRkaW5nOiAwIDAgMCAxNnB4O1xcbiAgfVxcblxcbiAgLm1vYmlsZU1PZHVsZUNvbnRhaW5lckFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmOTliMztcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjVmNztcXG4gICAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICAgIHBhZGRpbmc6IDAgMCAwIDE2cHg7XFxuICB9ICovXFxuXFxuICAvKiAgLnBsdXMge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLyogLmltYWdlTW9iaWxlVXJsIHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLyogLm1vYmlsZU1vZHVsZU5hbWUge1xcbiAgICB3aWR0aDogNzUlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfVxcblxcbiAgLm1vYmlsZU1vZHVsZU5hbWVBY3RpdmUge1xcbiAgICB3aWR0aDogNzUlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfSAqL1xcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1oZWFkZXJDb250YWluZXJzLTNrR0dvIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLy8ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGJvcmRlcjogbm9uZTtcXG4gICAgLy8gbWFyZ2luOiAxMDBweCAwIDAgMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtYWN0aXZlLTJjVmtlIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtYWRtaW5MYWJlbENvbnRhaW5lci0yTXJvaiB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUNvbnRhaW5lci1JNlNTeSB7XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW46IDAgMCA4cHggMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlQ29udGFpbmVyQWN0aXZlLTJCS1F0IHtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIGJvcmRlcjogc29saWQgMXB4ICNmZjk5YjM7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY1Zjc7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICB9XFxuXFxuICAvKiAuaWNvbiB7XFxuICAgIHdpZHRoOiAxMCU7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH0gKi9cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtYWRtaW5JbWctMm1Bc1Mge1xcbiAgICB3aWR0aDogMTZweDtcXG4gICAgaGVpZ2h0OiAxNnB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lLTNTdEZ1IHtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlTmFtZUFjdGl2ZS0zTHpxeiB7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICB3aWR0aDogOTAlO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1wbHVzLTI1T3BGIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtZW5kO1xcbiAgICAtbXMtZmxleC1hbGlnbjogZW5kO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVDb250YWluZXItMkZfaUsge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1kYXRhQ29udGFpbmVyLTNRN09NIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtaWNvbnMtMlhTTFIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lcjEtM0h6MFQge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zZWN0aW9uTmFtZS0xV1NYNSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAvKiAuc3ViTW9kdWxlTW9iaWxlQ29udGFpbmVyIHtcXG4gICAgbWFyZ2luOiAwIDAgMCAxNnB4O1xcbiAgfSAqL1xcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVOYW1lLTN4MVRvIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMTJweCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVEYXRhQ29udGFpbmVyLTIySVktIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVEYXRhLTFOUEwyIHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNjc7XFxuICAgIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXJpZ2h0SWNvbnMtODFyb0Ige1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1pY29uQWN0aXZlLTFNVnV3IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlTW9iaWxlQ29udGFpbmVyLTNYa2czIHtcXG4gICAgLy8gaGVpZ2h0OiAyMCU7XFxuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcXG4gICAgbWFyZ2luOiAwIDAgMCAxNnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1pY29uLVVYYUhSIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlSWNvbi1fTlVUUiB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWhlYWRlckNvbnRhaW5lci0zTG1rcSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyLTJENUp4IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlc0ZsZXgtM3A3Ykwge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1ByaWNpbmdWaWV3RGV0YWlscy9QcmljaW5nVmlld0RldGFpbHMuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixhQUFhO0VBQ2IscUJBQXFCO01BQ2pCLGFBQWE7RUFDakIsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixxQ0FBcUM7RUFDckMsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IscUJBQXFCO01BQ2pCLGFBQWE7RUFDakIsMEJBQTBCO0VBQzFCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJDQUEyQztFQUMzQyxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWU7RUFDZix1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG1CQUFtQjtFQUNuQiwyQ0FBMkM7RUFDM0MsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0Usc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1oscUNBQXFDO0VBQ3JDLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IscUJBQXFCO01BQ2pCLGFBQWE7RUFDakIsdUJBQXVCO0tBQ3BCLG9CQUFvQjtDQUN4Qjs7QUFFRDtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsd0JBQXdCO0VBQ3hCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixhQUFhO0VBQ2IscUJBQXFCO01BQ2pCLGFBQWE7RUFDakIsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsd0RBQXdEO1VBQ2hELGdEQUFnRDtFQUN4RCwwQkFBMEI7RUFDMUIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQix1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsbUJBQW1CO01BQ2YscUJBQXFCO0VBQ3pCLGtDQUFrQztDQUNuQzs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsbUJBQW1CO01BQ2YscUJBQXFCO0VBQ3pCLGtCQUFrQjtFQUNsQixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLG9CQUFvQjtFQUNwQixtQ0FBbUM7Q0FDcEM7O0FBRUQ7Ozs7OztHQU1HOztBQUVIOzs7OztJQUtJOztBQUVKO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UscUJBQXFCO01BQ2pCLGFBQWE7RUFDakIsb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7Q0FDdkI7O0FBRUQ7OztHQUdHOztBQUVIO0VBQ0UsdUJBQXVCO0VBQ3ZCLFdBQVc7RUFDWCxhQUFhO0NBQ2Q7O0FBRUQ7Ozs7O0dBS0c7O0FBRUg7RUFDRSx1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGFBQWE7RUFDYixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsYUFBYTtDQUNkOztBQUVEO0VBQ0UsZ0JBQWdCO01BQ1osWUFBWTtDQUNqQjs7QUFFRDtFQUNFLGdCQUFnQjtNQUNaLFlBQVk7RUFDaEIsY0FBYztDQUNmOztBQUVEO0VBQ0UsZ0JBQWdCO01BQ1osWUFBWTtFQUNoQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGtDQUFrQztDQUNuQzs7QUFFRDtFQUNFLFVBQVU7Q0FDWDs7QUFFRDs7OztJQUlJOztBQUVKO0VBQ0U7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7O01BRUk7O0VBRUo7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLHVCQUF1QjtHQUN4Qjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHNCQUFzQjtHQUN2Qjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixzQkFBc0I7R0FDdkI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWiwyQkFBMkI7R0FDNUI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsMEJBQTBCO0lBQzFCLG1CQUFtQjtJQUNuQix5QkFBeUI7WUFDakIsaUJBQWlCO0lBQ3pCLFVBQVU7SUFDVixXQUFXO0lBQ1gsZ0NBQWdDO0lBQ2hDLDZCQUE2QjtHQUM5Qjs7RUFFRDs7TUFFSTs7RUFFSjtJQUNFLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMEJBQTBCO1FBQ3RCLDhCQUE4QjtJQUNsQyxtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFVBQVU7SUFDVixXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLDhCQUE4QjtJQUM5QixnQ0FBZ0M7SUFDaEMscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxzQkFBc0I7R0FDdkI7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSwwQkFBMEI7SUFDMUIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7O0VBRUQ7SUFDRSwrQkFBK0I7SUFDL0IsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxXQUFXO0dBQ1o7O0VBRUQ7O01BRUk7O0VBRUo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQTJCSTs7RUFFSjs7O01BR0k7O0VBRUo7OztNQUdJOztFQUVKOzs7Ozs7Ozs7Ozs7TUFZSTs7RUFFSjtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQiwyQkFBMkI7SUFDM0IsYUFBYTtJQUNiLHVCQUF1QjtHQUN4Qjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMEJBQTBCO1FBQ3RCLDhCQUE4QjtHQUNuQzs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMEJBQTBCO1FBQ3RCLDhCQUE4QjtHQUNuQzs7RUFFRDtJQUNFLHFCQUFxQjtRQUNqQiw0QkFBNEI7SUFDaEMsYUFBYTtJQUNiLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIscUNBQXFDO0lBQ3JDLHVCQUF1QjtJQUN2QixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osMEJBQTBCO0lBQzFCLDBCQUEwQjtJQUMxQixvQkFBb0I7R0FDckI7O0VBRUQ7OztNQUdJOztFQUVKO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsdUJBQXVCO1FBQ25CLG9CQUFvQjtHQUN6Qjs7RUFFRDtJQUNFLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsZUFBZTtHQUNoQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsd0JBQXdCO0lBQ3hCLG9CQUFvQjtRQUNoQixzQkFBc0I7R0FDM0I7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7O01BRUk7O0VBRUo7SUFDRSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Ysc0JBQXNCO0dBQ3ZCOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjtDQUNGXCIsXCJmaWxlXCI6XCJQcmljaW5nVmlld0RldGFpbHMuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucHJpY2luZ0hlYWRlciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDUlO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4ucHJpY2VfdGl0bGUge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc2l6ZTogNTZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4ucHJpY2VfY29udGVudCB7XFxuICB3aWR0aDogNTAlO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbn1cXG5cXG4ud2hvQXJlWW91IHtcXG4gIG1hcmdpbjogNzJweCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLmFkbWluQ29udGFpbmVyIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiAyMjZweDtcXG4gIGhlaWdodDogODJweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHggMTBweCAyMHB4IDIwcHg7XFxuICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4udGFic0NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW46IDI0cHggMCA3MnB4IDA7XFxufVxcblxcbi5hZG1pbkxhYmVsQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLmFkbWluTGFiZWwge1xcbiAgLy8gd2lkdGg6IDExOXB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luOiAxOXB4IDM4cHggOHB4IDhweDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uaW1nQ29udGFpbmVyIHtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgbWFyZ2luOiAxMHB4IDhweCAwIDA7XFxuICBwYWRkaW5nOiAxMHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgxOTYsIDE5NiwgMTk2LCAwLjMpO1xcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcXG59XFxuXFxuLmFkbWluSW1nIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLnBsYW5lIHtcXG4gIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLm1vZHVsZUNvbnRhaW5lciB7XFxuICB3aWR0aDogMzAlO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogMTJweCAzMHB4O1xcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLm1vZHVsZUNvbnRhaW5lcjpsYXN0LWNoaWxkIHtcXG4gIGJvcmRlci1yaWdodC13aWR0aDogMDtcXG59XFxuXFxuLm1vZHVsZUNvbnRhaW5lckFjdGl2ZSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uaWNvbkFjdGl2ZSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uaGVhZGVyQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC8vIGhlaWdodDogNDhweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbn1cXG5cXG4uaWNvbiB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4ubW9kdWxlTmFtZSB7XFxuICBtYXJnaW46IDAgMCAwIDhweDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxldHRlci1zcGFjaW5nOiAtMC4yOHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5tb2R1bGVOYW1lQWN0aXZlIHtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLmFkbWluQ29udGFpbmVyQWN0aXZlIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiAyMjZweDtcXG4gIGhlaWdodDogODJweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHggMTBweCAwIDIwcHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAxNnB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMTZweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjE2KTtcXG4gIGJvcmRlcjogc29saWQgMXB4ICNmZjk5YjM7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5yaWdodEljb24ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA4cHg7XFxuICByaWdodDogOHB4O1xcbiAgd2lkdGg6IDE2cHg7XFxuICBoZWlnaHQ6IDE2cHg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLmFjdGl2ZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5zdWJNb2R1bGVzIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLnN1Yk1vZHVsZUFjdGl2ZSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLmljb25EYXRhQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHJvdztcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHJvdztcXG4gIC8vIG1hcmdpbi1sZWZ0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLmljb25EYXRhQ29udGFpbmVyMSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiByb3c7XFxuICAgICAganVzdGlmeS1jb250ZW50OiByb3c7XFxuICBtYXJnaW4tbGVmdDogMTBweDtcXG4gIC8vIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLmRhdGFDb250YWluZXIge1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTc7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5pY29ucyB7XFxuICB3aWR0aDogMThweDtcXG4gIGhlaWdodDogMThweDtcXG4gIG1hcmdpbjogNnB4IDEycHggMCAwO1xcbn1cXG5cXG4uaWNvbkNvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgLy8gYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLyogLmRhdGFJY29uQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xcbiAgY29sdW1uLWdhcDogM3JlbTtcXG4gIHJvdy1nYXA6IDNyZW07XFxufVxcbiAqL1xcblxcbi8qIC52ZXJ0aWNhbExpbmUge1xcbiAgaGVpZ2h0OiA0MjBweDtcXG4gIHdpZHRoOiAxcHg7XFxuICBtYXJnaW46IDYzcHggMTBweCAwIDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDlkOWQ5O1xcbn0gKi9cXG5cXG4uc3R1ZGVudE1vZHVsZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uc2VjdGlvbk5hbWUge1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS4zMztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLnN1Yk1vZHVsZXNGbGV4IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIG1hcmdpbjogMzJweCAwIDMycHggMDtcXG59XFxuXFxuLyogLnN1YnN1Ym1vZHVsZXMge1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuICovXFxuXFxuLm1vZHVsZUZsZXgge1xcbiAgcGFkZGluZzogMCA2NHB4IDAgNjRweDtcXG4gIHdpZHRoOiA4NyU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi8qIC5tb2R1bGVGbGV4MSB7XFxuICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG4gKi9cXG5cXG4ubW9kdWxlcyB7XFxuICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4uYXJyb3dJY29uIHtcXG4gIHdpZHRoOiAxMC45cHg7XFxuICBoZWlnaHQ6IDE2cHg7XFxufVxcblxcbi5zdWJNb2R1bGVDb250YWluZXIge1xcbiAgLW1zLWZsZXg6IDAuMyAxO1xcbiAgICAgIGZsZXg6IDAuMyAxO1xcbn1cXG5cXG4uc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXIge1xcbiAgLW1zLWZsZXg6IDAuNCAxO1xcbiAgICAgIGZsZXg6IDAuNCAxO1xcbiAgLy8gd2lkdGg6IDMwJTtcXG59XFxuXFxuLnN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMSB7XFxuICAtbXMtZmxleDogMC4zIDE7XFxuICAgICAgZmxleDogMC4zIDE7XFxuICAvLyB3aWR0aDogMjUlO1xcbiAgbWFyZ2luOiA1MHB4IDAgMCAwO1xcbiAgLy8gYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uaW1hZ2VDb250YWluZXIge1xcbiAgd2lkdGg6IDklO1xcbn1cXG5cXG4vKiBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgwcHgpIHtcXG4gIC5tb2R1bGVGbGV4e1xcbiAgICB3aWR0aDogNTMuNSU7XFxuICB9XFxufSAqL1xcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkxcHgpIHtcXG4gIC5wbHVzIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5tb2R1bGVJY29uIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zdWJNb2R1bGVNb2JpbGVDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnBsdXNDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLyogLmhlYWRlckNvbnRhaW5lck1vYmlsZVZpZXcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfSAqL1xcblxcbiAgLmhlYWRlckNvbnRhaW5lcnMge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLmdvQmFja0xpbmsge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAubW9kdWxlcyB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAucHJpY2luZ0hlYWRlciB7XFxuICAgIG1hcmdpbjogMjRweCAwIDAgMDtcXG4gICAgcGFkZGluZzogMCA2NHB4IDAgNjRweDtcXG4gIH1cXG5cXG4gIC5wcmljZV90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gIH1cXG5cXG4gIC5wcmljZV9jb250ZW50IHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNzE7XFxuICAgIG1hcmdpbjogMTJweCAwIDQ4cHggMDtcXG4gIH1cXG5cXG4gIC53aG9BcmVZb3Uge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIG1hcmdpbjogNDhweCAwIDI0cHggMDtcXG4gIH1cXG5cXG4gIC5nb0JhY2tMaW5rIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIH1cXG5cXG4gIC5hZG1pbkNvbnRhaW5lckFjdGl2ZSB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgaGVpZ2h0OiA2NXB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XFxuICAgIGJvcmRlci1zdHlsZTogbm9uZTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDhweDtcXG4gICAgLy8gcGFkZGluZzogMCA0OXB4IDI0cHggNTBweDtcXG4gIH1cXG5cXG4gIC8qIC5hZG1pbkNvbnRhaW5lckFjdGl2ZTpzZWNvbmQtY2hpbGQge1xcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA4cHg7XFxuICB9ICovXFxuXFxuICAuYWRtaW5Db250YWluZXIge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIHdpZHRoOiA1MCU7XFxuICAgIGhlaWdodDogNjVweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gICAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLnJpZ2h0SWNvbiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuaW1nQ29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAwO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgLy8gcGFkZGluZzogMjRweCA0OXB4IDI0cHggNTBweDtcXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwcHg7XFxuICAgIC8vIHBhZGRpbmc6IDA7XFxuICAgIC8vIG1hcmdpbjogMTlweCAwIDAgMDtcXG4gIH1cXG5cXG4gIC5hZG1pbkxhYmVsIHtcXG4gICAgLy8gd2lkdGg6MHB4O1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAxOXB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLm1vZHVsZURhdGEge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC50YWJzQ29udGFpbmVyIHtcXG4gICAgLy8ganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICBtYXJnaW46IDAgMCAyMHB4IDA7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLnBsYW5lIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5tb2R1bGVGbGV4IHtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC8qIC5oZWFkZXJDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfSAqL1xcblxcbiAgLyogIC5tb2JpbGVNT2R1bGVDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICBqdXN0aWZ5LWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW46IDAgMCA4cHggMDtcXG4gICAgcGFkZGluZzogMCAwIDAgMTZweDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVNT2R1bGVDb250YWluZXJBY3RpdmUge1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICBqdXN0aWZ5LWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIGJvcmRlcjogc29saWQgMXB4ICNmZjk5YjM7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY1Zjc7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgICBwYWRkaW5nOiAwIDAgMCAxNnB4O1xcbiAgfSAqL1xcblxcbiAgLyogIC5wbHVzIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH0gKi9cXG5cXG4gIC8qIC5pbWFnZU1vYmlsZVVybCB7XFxuICAgIHdpZHRoOiAxMCU7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH0gKi9cXG5cXG4gIC8qIC5tb2JpbGVNb2R1bGVOYW1lIHtcXG4gICAgd2lkdGg6IDc1JTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVNb2R1bGVOYW1lQWN0aXZlIHtcXG4gICAgd2lkdGg6IDc1JTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIH0gKi9cXG5cXG4gIC5oZWFkZXJDb250YWluZXJzIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLy8ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGJvcmRlcjogbm9uZTtcXG4gICAgLy8gbWFyZ2luOiAxMDBweCAwIDAgMDtcXG4gIH1cXG5cXG4gIC5hY3RpdmUge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgfVxcblxcbiAgLmFkbWluTGFiZWxDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgfVxcblxcbiAgLm1vZHVsZUNvbnRhaW5lciB7XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICB3aWR0aDogMzI4cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW46IDAgMCA4cHggMDtcXG4gIH1cXG5cXG4gIC5tb2R1bGVDb250YWluZXJBY3RpdmUge1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmOTliMztcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjVmNztcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIH1cXG5cXG4gIC8qIC5pY29uIHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLmFkbWluSW1nIHtcXG4gICAgd2lkdGg6IDE2cHg7XFxuICAgIGhlaWdodDogMTZweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5tb2R1bGVOYW1lIHtcXG4gICAgd2lkdGg6IDkwJTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gIH1cXG5cXG4gIC5tb2R1bGVOYW1lQWN0aXZlIHtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIHdpZHRoOiA5MCU7XFxuICB9XFxuXFxuICAucGx1cyB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBqdXN0aWZ5LWl0ZW1zOiBmbGV4LWVuZDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGVuZDtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcXG4gIH1cXG5cXG4gIC5zdWJNb2R1bGVDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLmRhdGFDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLmljb25zIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lcjEge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnNlY3Rpb25OYW1lIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC8qIC5zdWJNb2R1bGVNb2JpbGVDb250YWluZXIge1xcbiAgICBtYXJnaW46IDAgMCAwIDE2cHg7XFxuICB9ICovXFxuXFxuICAuc3ViTW9kdWxlTmFtZSB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIG1hcmdpbjogMTZweCAwIDEycHggMDtcXG4gIH1cXG5cXG4gIC5zdWJtb2R1bGVEYXRhQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLnN1Ym1vZHVsZURhdGEge1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICBsaW5lLWhlaWdodDogMS42NztcXG4gICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gIH1cXG5cXG4gIC5yaWdodEljb25zIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH1cXG5cXG4gIC5pY29uQWN0aXZlIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zdWJNb2R1bGVNb2JpbGVDb250YWluZXIge1xcbiAgICAvLyBoZWlnaHQ6IDIwJTtcXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xcbiAgICBtYXJnaW46IDAgMCAwIDE2cHg7XFxuICB9XFxuXFxuICAuaWNvbiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAubW9kdWxlSWNvbiB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAuaGVhZGVyQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuc3ViTW9kdWxlc0ZsZXgge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicHJpY2luZ0hlYWRlclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1wcmljaW5nSGVhZGVyLTJiWXYtXCIsXG5cdFwicHJpY2VfdGl0bGVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtcHJpY2VfdGl0bGUtMm5xRnRcIixcblx0XCJwcmljZV9jb250ZW50XCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNlX2NvbnRlbnQtMWZWdEtcIixcblx0XCJ3aG9BcmVZb3VcIjogXCJQcmljaW5nVmlld0RldGFpbHMtd2hvQXJlWW91LTNUMG1nXCIsXG5cdFwiYWRtaW5Db250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtYWRtaW5Db250YWluZXItMjlFbFFcIixcblx0XCJ0YWJzQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXRhYnNDb250YWluZXItM19ad3VcIixcblx0XCJhZG1pbkxhYmVsQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluTGFiZWxDb250YWluZXItMk1yb2pcIixcblx0XCJhZG1pbkxhYmVsXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluTGFiZWwtMjN3amJcIixcblx0XCJpbWdDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaW1nQ29udGFpbmVyLTFKal91XCIsXG5cdFwiYWRtaW5JbWdcIjogXCJQcmljaW5nVmlld0RldGFpbHMtYWRtaW5JbWctMm1Bc1NcIixcblx0XCJwbGFuZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1wbGFuZS0yMWVhVFwiLFxuXHRcIm1vZHVsZUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVDb250YWluZXItSTZTU3lcIixcblx0XCJtb2R1bGVDb250YWluZXJBY3RpdmVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtbW9kdWxlQ29udGFpbmVyQWN0aXZlLTJCS1F0XCIsXG5cdFwiaWNvbkFjdGl2ZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pY29uQWN0aXZlLTFNVnV3XCIsXG5cdFwiaGVhZGVyQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWhlYWRlckNvbnRhaW5lci0zTG1rcVwiLFxuXHRcImljb25cIjogXCJQcmljaW5nVmlld0RldGFpbHMtaWNvbi1VWGFIUlwiLFxuXHRcIm1vZHVsZU5hbWVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtbW9kdWxlTmFtZS0zU3RGdVwiLFxuXHRcIm1vZHVsZU5hbWVBY3RpdmVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtbW9kdWxlTmFtZUFjdGl2ZS0zTHpxelwiLFxuXHRcImFkbWluQ29udGFpbmVyQWN0aXZlXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluQ29udGFpbmVyQWN0aXZlLTFaMEpMXCIsXG5cdFwicmlnaHRJY29uXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXJpZ2h0SWNvbi0yaTMyWlwiLFxuXHRcImFjdGl2ZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1hY3RpdmUtMmNWa2VcIixcblx0XCJzdWJNb2R1bGVzXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZXMtMVI1LXhcIixcblx0XCJzdWJNb2R1bGVBY3RpdmVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlQWN0aXZlLTFoRXBKXCIsXG5cdFwiaWNvbkRhdGFDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaWNvbkRhdGFDb250YWluZXItaTJHNlRcIixcblx0XCJpY29uRGF0YUNvbnRhaW5lcjFcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaWNvbkRhdGFDb250YWluZXIxLXVyNnJaXCIsXG5cdFwiZGF0YUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1kYXRhQ29udGFpbmVyLTNRN09NXCIsXG5cdFwiaWNvbnNcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaWNvbnMtMlhTTFJcIixcblx0XCJpY29uQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25Db250YWluZXItMVFjTk5cIixcblx0XCJzdHVkZW50TW9kdWxlXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN0dWRlbnRNb2R1bGUtMWxGeHlcIixcblx0XCJzZWN0aW9uTmFtZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zZWN0aW9uTmFtZS0xV1NYNVwiLFxuXHRcInN1Yk1vZHVsZXNGbGV4XCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZXNGbGV4LTNwN2JMXCIsXG5cdFwibW9kdWxlRmxleFwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVGbGV4LTJOb3VJXCIsXG5cdFwibW9kdWxlc1wiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVzLTFkU1loXCIsXG5cdFwiYXJyb3dJY29uXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWFycm93SWNvbi0xSThNNlwiLFxuXHRcInN1Yk1vZHVsZUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVDb250YWluZXItMkZfaUtcIixcblx0XCJzdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lci0yRDVKeFwiLFxuXHRcInN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lcjEtM0h6MFRcIixcblx0XCJpbWFnZUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pbWFnZUNvbnRhaW5lci0zQ0E1cVwiLFxuXHRcInBsdXNcIjogXCJQcmljaW5nVmlld0RldGFpbHMtcGx1cy0yNU9wRlwiLFxuXHRcIm1vZHVsZUljb25cIjogXCJQcmljaW5nVmlld0RldGFpbHMtbW9kdWxlSWNvbi1fTlVUUlwiLFxuXHRcInN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVNb2JpbGVDb250YWluZXItM1hrZzNcIixcblx0XCJwbHVzQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXBsdXNDb250YWluZXItMzlQcWdcIixcblx0XCJoZWFkZXJDb250YWluZXJzXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWhlYWRlckNvbnRhaW5lcnMtM2tHR29cIixcblx0XCJnb0JhY2tMaW5rXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWdvQmFja0xpbmstbVBMRUFcIixcblx0XCJtb2R1bGVEYXRhXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZURhdGEtRUR2LWtcIixcblx0XCJzdWJNb2R1bGVOYW1lXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZU5hbWUtM3gxVG9cIixcblx0XCJzdWJtb2R1bGVEYXRhQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZURhdGFDb250YWluZXItMjJJWS1cIixcblx0XCJzdWJtb2R1bGVEYXRhXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZURhdGEtMU5QTDJcIixcblx0XCJyaWdodEljb25zXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXJpZ2h0SWNvbnMtODFyb0JcIlxufTsiLCJleHBvcnQgY29uc3QgUGxhbkRldGFpbHMgPSB7XG4gIGFkbWluOiB7XG4gICAgbGFiZWw6ICdBZG1pbi9UZWFjaGVyJyxcbiAgICB2YWx1ZTogJ2FkbWluJyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2FkbWluLnN2ZycsXG4gICAgbW9kdWxlczoge1xuICAgICAgb25saW5ldGVzdDoge1xuICAgICAgICBsYWJlbDogJ09ubGluZSBUZXN0cycsXG4gICAgICAgIHZhbHVlOiAnb25saW5ldGVzdCcsXG4gICAgICAgIGltYWdlVXJsOiAnL2ltYWdlcy9UZXN0cy5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvVGVzdHNNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZVRlc3RzLnN2ZycsXG4gICAgICAgIHN1Yk1vZHVsZXM6IHtcbiAgICAgICAgICB0ZXN0czoge1xuICAgICAgICAgICAgbGFiZWw6ICdPbmxpbmUgVGVzdGluZyBQbGF0Zm9ybScsXG4gICAgICAgICAgICB2YWx1ZTogJ3Rlc3RzJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1Rlc3QgQ3JlYXRpb24nLCB2YWx1ZTogJ3Rlc3RjcmVhdGlvbicgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0VkaXQgVGVzdCcsIHZhbHVlOiAnZWRpdHRlc3QnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdEZWxldGUgVGVzdCcsIHZhbHVlOiAnZGVsZXRldGVzdCcgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0dyYWNlIFBlcmlvZCcsIHZhbHVlOiAnZ3JhY2VwZXJpb2QnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Fzc2lnbiBRdWVzdGlvbiBQYXBlciB0byBUZWFjaGVycycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhc3NpZ25xdWVzdGlvbnBhcGVydG90ZWFjaGVyJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVXBsb2FkIHlvdXIgb3duIFF1ZXN0aW9uIFBhcGVyIGluIC5kb2N4JyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3VwbG9hZHlvdXJvd25xdWVzdGlvbnBhcGVyaW5kb2N4JyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVXBsb2FkIHlvdXIgb3duIHF1ZXN0aW9uIHBhcGVyIGluIDUgbWluJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3VwbG9hZHlvdXJvd25xdWVzdGlvbnBhcGVyaW41bWluJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ1N1cHBvcnRzIGFueSB0eXBlIG9mIHF1ZXN0aW9ucyAoc2luZ2xlIGFuc3dlciwgbXVsdGlwbGUgYW5zd2VyLCBwYXJhZ3JhcGggZXRjLiwnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnc3VwcG9ydHNhbnl0eXBlb2ZxdXN0aW9ucycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdQYXJ0aWFsIFBhcGVyJywgdmFsdWU6ICdwYXJ0aWFscGFwZXInIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1ByZXZpZXcgbGlrZSBUZXN0IFBsYXRmb3JtJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3ByZXZpZXdsaWtldGVzdHBsYXRmb3JtJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1Byb2N0b3JpbmcnLCB2YWx1ZTogJ3Byb2N0b3JpbmcnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0xpdmUgQXR0ZW5kYW5jZSAvIEF0dGVuZGFuY2UnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnbGl2ZWF0dGVuZGFuY2VhdHRlbmRhbmNlJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRG93bmxvYWRhYmxlIGF0dGVuZGFuY2Ugc2hlZXRzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Rvd25sb2FkYWJsZWF0dGVuZGFuY2VzaGVldHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEb3dubG9hZGFibGUgcXVlc3Rpb24gcGFwZXIsIGtleSBhbmQgc29sdXRpb25zJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Rvd25sb2FkcXVlc3Rpb25wYXBlcmtleXNvbHV0aW9ucycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdLZXkgRWRpdGluZycsIHZhbHVlOiAna2V5ZWRpdGluZycgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQXV0byBSZXBvcnRzICYgQW5hbHlzaXMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnYXV0b3JlcG9ydHNhbmFseXNpcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1Jlc3VsdHMgZ2VuZXJhdGlvbiBmb3IgT2ZmbGluZSBleGFtJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3Jlc3VsdHNnZW5lcmF0aW9uZm9yb2ZmbGluZWV4YW1zJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VwcG9ydHMgT25saW5lIC8gT2ZmbGluZSB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzdXBwb3J0c29ubGluZW9mZmxpbmV0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdGbGV4aWJsZSB0ZXN0IHRpbWVzJywgdmFsdWU6ICdmbGV4aWJsZXRlc3R0aW1lcyAnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdCZXN0IEhpZXJhcmN5IHN5c3RlbScsIHZhbHVlOiAnYmVzdGhpZXJhcmN5c3lzdGVtJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdQYXJzZXIgY2FuIGNvbnZlcnQgdGV4dCAvIGltYWdlcyAvIGVxdWF0aW9ucyAvIGdyYXBocycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdwYXJzZXJjYW5jb252ZXJ0dGV4dGltYWdlc2VxdWF0aW9uc2dyYXBocycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgcXVzdGlvbmJhbms6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb24gQmFuaycsXG4gICAgICAgICAgICB2YWx1ZTogJ3F1c3Rpb25iYW5rJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ1F1ZXN0aW9uIGJhbmsgd2l0aCAxLDUwLDAwMCsgUXVlc3Rpb25zIHdpdGggS2V5IGFuZCBTb2x1dGlvbnMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncXVlc3Rpb25iYW5rd2l0aDEsNTAsMDAwK3F1ZXN0aW9uc3dpdGhrZXlhbmRzb2x1dGlvbnMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnUXVlc3Rpb25zIGluIDMgZGlmZmljdWx0eSBsZXZlbHMgKEVhc3kgLyBNZWRpdW0gLyBIaWdoKScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdRdWVzdGlvbnNpbjNkaWZmaWN1bHR5bGV2ZWxzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1F1ZXN0aW9uIHRhZ2dpbmcnLCB2YWx1ZTogJ3F1ZXN0aW9udGFnZ2luZycgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwYXBlcnM6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUGFwZXJzJyxcbiAgICAgICAgICAgIHZhbHVlOiAncGFwZXJzJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0NyZWF0ZSBRdWVzdGlvbiBQYXBlcicsIHZhbHVlOiAnY3JlYXRlcXVlc3Rpb25wYXBlcicgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1NlbGVjdCBxdWVzdGlvbnMnLCB2YWx1ZTogJ3NlbGVjdHF1ZXN0aW9ucycgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb24gcGFwZXIgcmVzdW1lIGZlYXR1cmUnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncXVlc3Rpb25wYXBlcnJlc3VtZWZlYXR1cmUnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnUGFydGlhbCBQYXBlciBmZWF0dXJlJywgdmFsdWU6ICdwYXJ0aWFscGFwZXJmZWF0dXJlJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnRGVzaWduIHBlcnNvbmFsaXplZCBxdWVzdGlvbiBwYXBlci9zIHdpdGggSW5zdGl0dXRlIE5hbWUsIExvZ28gJiBXYXRlcm1hcmsnLFxuICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgJ2Rlc2lnbnBlcnNvbmFsaXplZHF1ZXN0aW9ucGFwZXIvc3dpdGhpbnN0aXR1dGVuYW1lbG9nbyZ3YXRlcm1hcmsnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHByYWN0aWNlOiB7XG4gICAgICAgICAgICBsYWJlbDogJ1ByYWN0aWNlJyxcbiAgICAgICAgICAgIHZhbHVlOiAncHJhY3RpY2UnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdVbmxpbWl0ZWQgUHJhY3RpY2UgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndW5saW1pdGVkcHJhY3RpY2V0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1F1ZXN0aW9ucyBiYXNlZCBwcmFjdGljZScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdxdWVzdGlvbnNiYXNlZHByYWN0aWNlJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1RpbWUgYmFzZWQgUHJhY3RpY2UnLCB2YWx1ZTogJ3RpbWViYXNlZHByYWN0aWNlJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZXRhaWxlZCBhbmFseXNpcyBvZiBhbGwgcHJhY3RpY2UgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZGV0YWlsZWRhbmFseXNpc29mYWxscHJhY3RpY2V0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5hbHlzaXM6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnQW5hbHlzaXMnLFxuICAgICAgICAgICAgdmFsdWU6ICdhbmFseXNpcycsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ01hcmtzIEFuYWx5c3NpcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdtYXJrYW5hbHlzc2lzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVG9waWMgd2lzZSBBbmFseXNpcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd0b3BpY3dpc2VhbmFseXNpcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdFcnJvciBBbmFseXNpcycsIHZhbHVlOiAnZXJyb3JhbmFseXNpcycgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICByZXBvcnRzOiB7XG4gICAgICAgICAgICBsYWJlbDogJ1JlcG9ydHMnLFxuICAgICAgICAgICAgdmFsdWU6ICdyZXBvcnRzJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVGVzdCBSZXN1bHRzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3Rlc3RyZXN1bHRzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ19XX1UgUmVwb3J0JyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2N3Y3JlcG9ydCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ01hcmtzIERpc3RyaWJ1dGlvbicsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdtYXJrc2Rpc3RyaWJ1dGlvbicsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vycm9yIENvdW50JyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Vycm9yY291bnQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnV2VhayBTdWJqZWN0cycsIHZhbHVlOiAnV2Vha3N1YmplY3QnIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgbGl2ZWNsYXNzZXM6IHtcbiAgICAgICAgbGFiZWw6ICdMaXZlIGNsYXNzZXMnLFxuICAgICAgICB2YWx1ZTogJ2xpdmVjbGFzc2VzJyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0xpdmUuc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0xpdmVNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZUxpdmUuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIGxpdmU6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnTGl2ZScsXG4gICAgICAgICAgICB2YWx1ZTogJ2xpdmUnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnSW50ZXJyYWN0aXZlIExpdmUgQ2xhc3MgQ3JlYXRpb24gKGludGVncmF0ZWQgd2l0aCB6b29tKScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdpbnRlcnJhY3RpdmVsaXZlY2xhc3NjcmVhdGlvbicsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlY29yZGluZyBDbGFzcyBjcmVhdGlvbicsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdyZWNvcmRpbmdjbGFzc2NyZWF0aW9uJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1JlY3VycmluZyBjbGFzc2VzJywgdmFsdWU6ICdyZWN1cnJpbmdjbGFzc2VzJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBdHRhY2ggZmlsZSB3aGlsZSBjbGFzcyBjcmVhdGluZyAoaW1hZ2UgLyBwZGYpJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2F0dGFjaGZpbGV3aGlsZWNsYXNzY3JlYXRpbmcnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnRWRpdCBDbGFzcycsIHZhbHVlOiAnZWRpdGNsYXNzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnRGVsZXRlIENsYXNzJywgdmFsdWU6ICdkZWxldGVjbGFzcycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0xpdmUgQXR0ZW5kYW5jZScsIHZhbHVlOiAnbGl2ZWF0dGVuZGFuY2UnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdXYXRjaCBsYXRlcicsIHZhbHVlOiAnd2F0Y2hsYXRlcicgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1NtYXJ0IEZpbHRlcnMnLCB2YWx1ZTogJ3NtYXJ0ZmlsdGVycycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Rvd25sb2FkIGNsYXNzZXMgbGlzdCcsIHZhbHVlOiAnZG93bmxvYWRjbGFzc2VzbGlzdCcgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2VjdXJlIE5vdGVzLCBEb3dubG9hZGFibGUgbGlua3MnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnc2VjdXJlbm90ZXNkb3dubG9hZGFibGVsaW5rcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N0dWRlbnQgTWFuYWdlbWVudCBTeXN0ZW0nLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnc3R1ZGVudG1hbmFnZW1lbnRzeXN0ZW0nLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQmVzdCBIaWVyYXJjeSBzeXN0ZW0nLCB2YWx1ZTogJ2Jlc3RoaWVyYXJjeXN5c3RlbScgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBzZWN1cmVjb250ZW50OiB7XG4gICAgICAgICAgICBsYWJlbDogJ1Jlc3RyaWN0ZWQgTG9naW4nLFxuICAgICAgICAgICAgdmFsdWU6ICdzZWN1cmVjb250ZW50JyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ29udGVudCB2aXNpYmxlIG9ubHkgaW5zaWRlIEFwcCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdjb250ZW50dmlzaWJsZW9ubHlpbnNpZGVhcHAnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdWaWRlbyBMaW5rIHNoYXJpbmcgZGlzYWJsZWQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndmlkZW9saW5rc2hhcmluZ2Rpc2FibGVkJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2NyZWVuIFJlY29yZGluZyBEaXNhYmxlZCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzY3JlZW5yZWNvcmRpbmdkaXNhYmxlZCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdTY3JlZW5zaG90IERpc2FibGVkJywgdmFsdWU6ICdzY3JlZW5zaG90ZGlzYWJsZWQnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ2VOb3RlcyBEb3dubG9hZGluZyBFbmFibGVkL0Rpc2FibGVkJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Vub3Rlc2Rvd25sb2FkaW5nZW5hYmxlZGRpc2FibGVkJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ09ubHkgUmVnaXN0ZXJlZCBTdHVkZW50cyBjYW4gZW50ZXIgd2l0aCBrbm93biBpZGVudGl0eSwgc28gbm8gbnVpc2FuY2UgZHVyaW5nIGNsYXNzIHVubGlrZSBab29tJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICdvbmx5cmVnaXN0ZXJlZHN0dWRlbnRzY2FuZW50ZXJ3aXRoa25vd25pZGVudGl0eXNvbm9udWlzYW5jZWR1cmluZ2NsYXNzdW5saWtlem9vbScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgYXNzaWdubWVudHM6IHtcbiAgICAgICAgbGFiZWw6ICdBc3NpZ25tZW50cycsXG4gICAgICAgIHZhbHVlOiAnYXNzaWdubWVudHMnLFxuICAgICAgICBpbWFnZVVybDogJy9pbWFnZXMvQXNzaWdubWVudHMuc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0Fzc2lnbm1lbnRzTW9iaWxlLnN2ZycsXG4gICAgICAgIGFjdGl2ZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9hY3RpdmVBc3NpZ25tZW50cy5zdmcnLFxuICAgICAgICBzdWJNb2R1bGVzOiB7XG4gICAgICAgICAgYXNzaWdubWVudDoge1xuICAgICAgICAgICAgbGFiZWw6ICdBc3NpZ25tZW50cycsXG4gICAgICAgICAgICB2YWx1ZTogJ2Fzc2lnbm1lbnQnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdFYXN5IENyZWF0aW9uIG9mIGFzc2lnbm1lbnQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZWFzeWNyZWF0aW9ub2Zhc3NpZ25tZW50JyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1N1Ym1pc3Npb24gc3RhdHVzJywgdmFsdWU6ICdzdWJtaXNzaW9uc3RhdHVzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQXdhcmRpbmcgTWFya3MnLCB2YWx1ZTogJ2F3YXJkaW5nbWFya3MnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdFYXN5IHRvIEV2YWx1YXRlJywgdmFsdWU6ICdlYXN5dG9ldmFsdWF0ZScgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0hlbHBmdWwgVG9vbGJveCcsIHZhbHVlOiAnaGVscGZ1bHRvb2xib3gnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FueXRpbWUsIEFueXdoZXJlIEFzc2lnbm1lbnRzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2FueXRpbWVhbnl3aGVyZWFzc2lnbm1lbnRzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnR3JhZGUgRmFzdGVyIHdpdGggRWFzeS10by1Vc2UgVG9vbHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZ3JhZGVmYXN0ZXJ3aXRoZWFzeXRvdXNldG9vbHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIGRvdWJ0czoge1xuICAgICAgICBsYWJlbDogJ0RvdWJ0cycsXG4gICAgICAgIHZhbHVlOiAnZG91YnRzJyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0RvdWJ0cy5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvRG91YnRzTW9iaWxlLnN2ZycsXG4gICAgICAgIGFjdGl2ZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9hY3RpdmVEb3VidHMuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIGRvdWJ0OiB7XG4gICAgICAgICAgICBsYWJlbDogJ0RvdWJ0cycsXG4gICAgICAgICAgICB2YWx1ZTogJ2RvdWJ0JyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ0Vhc3kgdG8gY2xlYXIgZG91YnRzIChDaGF0IC8gdXBsb2FkIGltYWdlIC8gaW5zZXJ0IGxpbmspJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Vhc3l0b2NsZWFyZG91YnRzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Jvb2ttYXJrIGRvdWJ0cycsIHZhbHVlOiAnYm9va21hcmtkb3VidHMnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdSZW9wZW4gZG91YnRzJywgdmFsdWU6ICdyZW9wZW5kb3VidHMnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdDaGVjayBsYXRlcicsIHZhbHVlOiAnY2hlY2tsYXRlcicgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1NtYXJ0IEZpbHRlcnMnLCB2YWx1ZTogJ3NtYXJ0ZmlsdGVycycgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBjb25uZWN0OiB7XG4gICAgICAgIGxhYmVsOiAnQ29ubmVjdCcsXG4gICAgICAgIHZhbHVlOiAnY29ubmVjdCcsXG4gICAgICAgIGltYWdlVXJsOiAnL2ltYWdlcy9Db25uZWN0LnN2ZycsXG4gICAgICAgIG1vYmlsZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9Db25uZWN0TW9iaWxlLnN2ZycsXG4gICAgICAgIGFjdGl2ZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9hY3RpdmVDb25uZWN0LnN2ZycsXG4gICAgICAgIHN1Yk1vZHVsZXM6IHtcbiAgICAgICAgICBzY2hlZHVsZToge1xuICAgICAgICAgICAgbGFiZWw6ICdTY2hlZHVsZScsXG4gICAgICAgICAgICB2YWx1ZTogJ3NjaGVkdWxlJyxcbiAgICAgICAgICB9LFxuICAgICAgICAgIG5vdGlmaWNhdGlvbnM6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnTm90aWZpY2F0aW9ucycsXG4gICAgICAgICAgICB2YWx1ZTogJ25vdGlmaWNhdGlvbnMnLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgbWVzc2FuZ2VyOiB7XG4gICAgICAgICAgICBsYWJlbDogJ01lc3NhbmdlcicsXG4gICAgICAgICAgICB2YWx1ZTogJ21lc3NhbmdlcicsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgfSxcbiAgc3R1ZGVudDoge1xuICAgIGxhYmVsOiAnU3R1ZGVudCcsXG4gICAgdmFsdWU6ICdzdHVkZW50JyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL3N0dWRlbnQuc3ZnJyxcbiAgICBtb2R1bGVzOiB7XG4gICAgICBvbmxpbmV0ZXN0OiB7XG4gICAgICAgIGxhYmVsOiAnT25saW5lIFRlc3RzJyxcbiAgICAgICAgdmFsdWU6ICdvbmxpbmV0ZXN0JyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL1Rlc3RzLnN2ZycsXG4gICAgICAgIG1vYmlsZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9UZXN0c01vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlVGVzdHMuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIHRlc3RzOiB7XG4gICAgICAgICAgICBsYWJlbDogJ1Rlc3RzJyxcbiAgICAgICAgICAgIHZhbHVlOiAndGVzdHMnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdFYXN5IHRvIGNoZWNrIFVwY29taW5nIFRlc3RzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Vhc3l0b2NoZWNrdXBjb21pbmd0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdFYXN5IHRvIGF0dGVtcHQnLCB2YWx1ZTogJ2Vhc3l0b2F0dGVtcHQnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDpcbiAgICAgICAgICAgICAgICAgICdBbnN3ZXJlZCAvIE5vdCBBbnN3ZXJlZCAvIE5vdCB2aXNpdGVkIGxpc3RzIGNhbiBiZSBjaGVja2VkIGVhc2lseScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdBbnN3ZXJlZG5vdGFuc3dlcmVkbm90dmlzaXRlZGxpc3RzY2FuYmVjaGVja2VkZWFzaWx5JyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ1N1YmplY3Qgd2lzZSBxdWVzdGlvbnMgcmVzcG9uc2UgY2FuIGJlIGNoZWNrZWQgZWFzaWx5IGJ5IGNvbG91cnMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgJ3N1YmplY3R3aXNlcXVlc3Rpb25zcmVzcG9uc2VjYW5iZWNoZWNrZWRlYXNpbHlieWNvbG91cnMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnSW5zdGFudCBSZXN1bHRzJywgdmFsdWU6ICdpbnN0YW50cmVzdWx0cycgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmFseXNpczoge1xuICAgICAgICAgICAgbGFiZWw6ICdBbmFseXNpcycsXG4gICAgICAgICAgICB2YWx1ZTogJ2FuYWx5c2lzJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnT3ZlcmFsbCBUZXN0IFBlcmZvcm1hbmNlJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ292ZXJhbGx0ZXN0cGVyZm9ybWFuY2UnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBdHRlbXB0ICYgQWNjdXJhY3kgcmF0ZSBpbiBhbGwgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnYXR0ZW1wdGFjY3VyYWN5cmF0ZWluYWxsdGVzdHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUZXN0cyBQZXJmb3JtYW5jZSBpbiBhbGwgdG9waWNzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3Rlc3RzcGVyZm9ybWFuY2VpbmFsbHRvcGljcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ092ZXJhbGwgLyBTdWJqZWN0IHdpc2UgTWFya3MgVHJlbmQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnb3ZlcmFsbHN1YmplY3R3aXNlbWFya3N0cmVuZCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ092ZXJhbGwgLyBTdWJqZWN0IHdpc2UgQXR0ZW1wdCBhbmQgQWNjdXJhY3kgdHJlbmQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnb3ZlcmFsbHN1YmplY3R3aXNlYXR0ZW1wdGFuZGFjY3VyYWN5dHJlbmQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdFYXNpbHkgZmluZCBjb21wbGV0ZWQgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZWFzaWx5ZmluZGNvbXBsZXRlZHRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnSW5kaXZpZHVhbCBhbmQgT3ZlcmFsbCBTdW1tYXJ5JyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2luZGl2aWR1YWxhbmRvdmVyYWxsc3VtbWFyeScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1YmplY3Qgd2lzZSBtYXJrcyBvZiBhbGwgLyBpbmRpdmlkdWFsIHRlc3RzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3N1YmplY3R3aXNlbWFya3NvZmFsbGluZGl2aWR1YWx0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0F0dGVtcHQgJiBBY2N1cmFjeSByYXRlIGluIGFsbCBzdWJqZWN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhdHRlbXB0YWNjdXJhY3lyYXRlaW5hbGxzdWJqZWN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDpcbiAgICAgICAgICAgICAgICAgICdRdWVzdGlvbnMgZGlzdHJpYnV0aW9uIGluIG92ZXJhbGwgJiBzdWJqZWN0IHdpc2UgaW4gaW5kaXZpZHVhbCAvIGFsbCB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAncXVlc3Rpb25zZGlzdHJpYnV0aW9uaW5vdmVyYWxsc3ViamVjdHdpc2VpbmluZGl2aWR1YWxhbGx0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1RvcGljIC8gc3ViIHRvcGljIC8gcXVlc3Rpb24gbGV2ZWwgYW5hbHlzaXMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndG9waWNzdWJ0b3BpY3F1ZXN0aW9ubGV2ZWxhbmFseXNpcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1F1ZXN0aW9uIHdpc2UgYmVoYXZpb3VyIGFuYWx5c2lzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3F1ZXN0aW9ud2lzZWJlaGF2aW91cmFuYWx5c2lzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb24gLyBzdWJqZWN0IC8gZGlmZmljdWx0IGxldmVsIFRpbWUgc3BlbnQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncXVlc3Rpb25zdWJqZWN0ZGlmZmljdWx0bGV2ZWx0aW1lc3BlbnQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUaW1lIHNwZW50IG9uIENvcnJlY3QgLyB3cm9uZyAvIHVuYXR0ZW1wdGVkIHF1ZXN0aW9ucycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd0aW1lc3BlbnRvbmNvcnJlY3R3cm9uZ3VuYXR0ZW1wdGVkcXVlc3Rpb25zJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0NhbiBDb21wYXJlIHdpdCBUb3BwZXInLCB2YWx1ZTogJ2NhbmNvbXBhcmV3aXR0b3BwZXInIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgcHJhY3RpY2U6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUHJhY3RpY2UnLFxuICAgICAgICAgICAgdmFsdWU6ICdwcmFjdGljZScsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1ByYWN0aWNlIGF2YWlsYWJsZSBmb3IgMTF0aCwgMTJ0aCBhbmQgYm90aCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdwcmFjdGljZWF2YWlsYWJsZWZvcjExdGgxMnRoYW5kYm90aCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1cHBvcnRzIC0gSkVFIE1haW4gLyBORUVUIC8gRUFNQ0VUJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3N1cHBvcnRzamVlbWFpbm5lZXRlYW1jZXQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdVbmxpbWl0ZWQgUHJhY3RpY2UgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndW5saW1pdGVkcHJhY3RpY2V0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdGdWxsIHN5bGxhYnVzIFRlc3RzJywgdmFsdWU6ICdmdWxsc3lsbGFidXN0ZXN0cycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1N1YmplY3Qgd2lzZSBwcmFjdGljZScsIHZhbHVlOiAnc3ViamVjdHdpc2VwcmFjdGljZScgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ2NoYXB0ZXIgd2lzZSBwcmFjdGljZScsIHZhbHVlOiAnY2hhcHRlcndpc2VwcmFjdGljZScgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb25zIGJhc2VkIHByYWN0aWNlJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3F1ZXN0aW9uc2Jhc2VkcHJhY3RpY2UnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnVGltZSBiYXNlZCBQcmFjdGljZScsIHZhbHVlOiAndGltZWJhc2VkcHJhY3RpY2UnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdJbnN0YW50IFJlc3VsdHMnLCB2YWx1ZTogJ2luc3RhbnRyZXN1bHRzJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZXRhaWxlZCBhbmFseXNpcyBvZiBhbGwgcHJhY3RpY2UgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZGV0YWlsZWRhbmFseXNpc29mYWxscHJhY3RpY2V0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgbGl2ZWNsYXNzZXM6IHtcbiAgICAgICAgbGFiZWw6ICdMaXZlIGNsYXNzZXMnLFxuICAgICAgICB2YWx1ZTogJ2xpdmVjbGFzc2VzJyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0xpdmUuc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0xpdmVNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZUxpdmUuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIGxpdmU6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnTGl2ZScsXG4gICAgICAgICAgICB2YWx1ZTogJ2xpdmUnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaWdubGUgQ2xpY2sgdG8gSm9pbiBjbGFzcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzaWdubGVjbGlja3Rvam9pbmNsYXNzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnTm8gbmVlZCB0byBkb3dubG9hZCBhbnkgYXBwcyAoem9vbSBpbnRlZ3JhdGVkKScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdub25lZWR0b2Rvd25sb2FkYW55YXBwcycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdXYXRjaCBsYXRlcicsIHZhbHVlOiAnd2F0Y2hsYXRlcicgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1NtYXJ0IEZpbHRlcnMnLCB2YWx1ZTogJ1NtYXJ0ZmlsdGVycycgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBhc3NpZ25tZW50czoge1xuICAgICAgICBsYWJlbDogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgdmFsdWU6ICdhc3NpZ25tZW50cycsXG4gICAgICAgIGltYWdlVXJsOiAnL2ltYWdlcy9Bc3NpZ25tZW50cy5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvQXNzaWdubWVudHNNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZUFzc2lnbm1lbnRzLnN2ZycsXG4gICAgICAgIHN1Yk1vZHVsZXM6IHtcbiAgICAgICAgICBhc3NpZ25tZW50OiB7XG4gICAgICAgICAgICBsYWJlbDogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgICAgIHZhbHVlOiAnYXNzaWdubWVudCcsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdFYXN5IHN1Ym1pc3Npb24nLCB2YWx1ZTogJ2Vhc3lzdWJtaXNzaW9uJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdFYXN5IHRvIGNoZWNrIGV2YWx1YXRpb24nLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZWFzeXRvY2hlY2tldmFsdWF0aW9uJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBkb3VidHM6IHtcbiAgICAgICAgbGFiZWw6ICdEb3VidHMnLFxuICAgICAgICB2YWx1ZTogJ2RvdWJ0cycsXG4gICAgICAgIGltYWdlVXJsOiAnL2ltYWdlcy9Eb3VidHMuc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0RvdWJ0c01vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlRG91YnRzLnN2ZycsXG4gICAgICAgIHN1Yk1vZHVsZXM6IHtcbiAgICAgICAgICBkb3VidDoge1xuICAgICAgICAgICAgbGFiZWw6ICdEb3VidHMnLFxuICAgICAgICAgICAgdmFsdWU6ICdkb3VidCcsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vhc3kgdG8gYXNrIGRvdWJ0cyAoQ2hhdCAvIHVwbG9hZCBpbWFnZSAvIGluc2VydCBsaW5rKScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdlYXN5dG9hc2tkb3VidHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQm9va21hcmsgZG91YnRzJywgdmFsdWU6ICdib29rbWFya2RvdWJ0cycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1Jlb3BlbiBkb3VidHMnLCB2YWx1ZTogJ3Jlb3BlbmRvdWJ0cycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0NoZWNrIGxhdGVyJywgdmFsdWU6ICdjaGVja2xhdGVyJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU21hcnQgRmlsdGVycycsIHZhbHVlOiAnc21hcnRmaWx0ZXJzJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICB9LFxufTtcblxuZXhwb3J0IGNvbnN0IGRlbW8gPSAoKSA9PiB7fTtcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgeyBQbGFuRGV0YWlscyB9IGZyb20gJy4vQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vUHJpY2luZ1ZpZXdEZXRhaWxzLnNjc3MnO1xuXG5jbGFzcyBQcmljaW5nVmlld0RldGFpbHMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgcm9sZTogJ2FkbWluJyxcbiAgICAgIGFjdGl2ZU1vZHVsZTogJ29ubGluZXRlc3QnLFxuICAgICAgYWN0aXZlU3VibW9kdWxlOiAndGVzdHMnLFxuICAgICAgbW9iaWxlOiBmYWxzZSxcbiAgICAgIHNob3c6IGZhbHNlLFxuICAgIH07XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmhhbmRsZVJlc2l6ZSgpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZVJlc2l6ZSk7XG4gIH1cblxuICBoYW5kbGVSZXNpemUgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBtb2JpbGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgLy8gY29uc29sZS5sb2coXCJmdW5jdGlvbiBjYWxsZWRcIilcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTApIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb2JpbGU6ICFtb2JpbGUgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb2JpbGUgfSk7XG4gICAgfVxuICB9O1xuXG4gIHNob3dSb2xlID0gdmFsdWUgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvdzogIXRydWUsXG4gICAgfSk7XG4gICAgY29uc3QgeyByb2xlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHBsYW4gPSBQbGFuRGV0YWlsc1t2YWx1ZV0ubW9kdWxlcztcbiAgICBjb25zdCBmaXJzdCA9IE9iamVjdC52YWx1ZXMocGxhbiB8fCB7fSk7XG4gICAgY29uc3QgZmlyc3RNb2QgPSBmaXJzdFswXS52YWx1ZTtcbiAgICBjb25zdCBzdWJNb2QgPSBwbGFuW2ZpcnN0TW9kXS5zdWJNb2R1bGVzO1xuICAgIGNvbnN0IGZpcnN0U3ViID0gT2JqZWN0LnZhbHVlcyhzdWJNb2QgfHwge30pO1xuICAgIGNvbnN0IGZpcnN0T25lID0gZmlyc3RTdWJbMF0udmFsdWU7XG5cbiAgICBpZiAodmFsdWUgIT09IHJvbGUpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICByb2xlOiB2YWx1ZSxcbiAgICAgICAgYWN0aXZlTW9kdWxlOiBmaXJzdE1vZCxcbiAgICAgICAgYWN0aXZlU3VibW9kdWxlOiBmaXJzdE9uZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgcm9sZTogdmFsdWUsXG4gICAgICAgIGFjdGl2ZU1vZHVsZTogZmlyc3RNb2QsXG4gICAgICAgIGFjdGl2ZVN1Ym1vZHVsZTogZmlyc3RPbmUsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgc2hvd01vZHVsZXMgPSBrZXkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSwgYWN0aXZlTW9kdWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IG1vZCA9IFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXM7XG4gICAgY29uc3Qgc3ViID0gbW9kW2tleV0uc3ViTW9kdWxlcztcbiAgICBjb25zdCBzdWJtb2R1bGUgPSBPYmplY3QudmFsdWVzKHN1YiB8fCB7fSk7XG4gICAgaWYgKGtleSAhPT0gYWN0aXZlTW9kdWxlKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlTW9kdWxlOiBrZXksIGFjdGl2ZVN1Ym1vZHVsZTogc3VibW9kdWxlWzBdLnZhbHVlIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlTW9kdWxlOiBrZXkgfSk7XG4gICAgfVxuICB9O1xuXG4gIHNob3dTdWJtb2R1bGUgPSB2YWx1ZSA9PiB7XG4gICAgY29uc3QgeyBhY3RpdmVTdWJtb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKHZhbHVlICE9PSBhY3RpdmVTdWJtb2R1bGUpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVTdWJtb2R1bGU6IHZhbHVlIH0pO1xuICAgIH1cbiAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlU3VibW9kdWxlOiB2YWx1ZSB9KTtcbiAgfTtcblxuICBzaG93RGF0YSA9IHZhbHVlID0+IHtcbiAgICBjb25zdCB7IGFjdGl2ZVNlY3Rpb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKHZhbHVlICE9PSBhY3RpdmVTZWN0aW9uKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlU2VjdGlvbjogdmFsdWUgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVTZWN0aW9uOiB2YWx1ZSB9KTtcbiAgICB9XG4gIH07XG5cbiAgZGlzcGxheVNlY3Rpb25zID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSwgYWN0aXZlTW9kdWxlLCBhY3RpdmVTdWJtb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgYWN0aXZlU3ViTW9kdWxlTmFtZSA9XG4gICAgICBQbGFuRGV0YWlsc1tyb2xlXS5tb2R1bGVzW2FjdGl2ZU1vZHVsZV0uc3ViTW9kdWxlc1thY3RpdmVTdWJtb2R1bGVdLmxhYmVsO1xuICAgIGNvbnN0IHNlY3Rpb25EYXRhID1cbiAgICAgIFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXNbYWN0aXZlTW9kdWxlXS5zdWJNb2R1bGVzW2FjdGl2ZVN1Ym1vZHVsZV07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXJ9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uTmFtZX0+e2FjdGl2ZVN1Yk1vZHVsZU5hbWV9PC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAge09iamVjdC52YWx1ZXMoc2VjdGlvbkRhdGEuZGF0YSB8fCB7fSkubWFwKChkLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgYmVsb3cgPSBpbmRleCA8IDEwO1xuXG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pY29uRGF0YUNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAge2JlbG93ICYmIChcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmljb25Db250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9JY29ucy5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5pY29uc31cbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJyaWdodFwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRhdGFDb250YWluZXJ9PntkLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApO1xuICAgICAgICAgIH0pfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheVNlY3Rpb24gPSAoKSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUsIGFjdGl2ZVN1Ym1vZHVsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBzZWN0aW9uRGF0YSA9XG4gICAgICBQbGFuRGV0YWlsc1tyb2xlXS5tb2R1bGVzW2FjdGl2ZU1vZHVsZV0uc3ViTW9kdWxlc1thY3RpdmVTdWJtb2R1bGVdO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMX0+XG4gICAgICAgIHtPYmplY3QudmFsdWVzKHNlY3Rpb25EYXRhLmRhdGEgfHwge30pLm1hcCgoZCwgaW5kZXgpID0+IHtcbiAgICAgICAgICBjb25zdCBiZWxvdyA9IGluZGV4ID49IDEwO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pY29uRGF0YUNvbnRhaW5lcjF9PlxuICAgICAgICAgICAgICB7YmVsb3cgJiYgKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmljb25Db250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL0ljb25zLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5pY29uc31cbiAgICAgICAgICAgICAgICAgICAgYWx0PVwicmlnaHRcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRhdGFDb250YWluZXJ9PntkLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9O1xuXG4gIGRpc3BsYXlTdWJNb2R1bGVzID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSwgYWN0aXZlTW9kdWxlLCBhY3RpdmVTdWJtb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgZmlsdGVyID0gUGxhbkRldGFpbHNbcm9sZV0ubW9kdWxlc1thY3RpdmVNb2R1bGVdLnN1Yk1vZHVsZXM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Yk1vZHVsZUNvbnRhaW5lcn0+XG4gICAgICAgIHtPYmplY3QudmFsdWVzKGZpbHRlciB8fCB7fSkubWFwKGVhY2ggPT4ge1xuICAgICAgICAgIGNvbnN0IGlzQWN0aXZlID0gYWN0aXZlU3VibW9kdWxlID09PSBlYWNoLnZhbHVlO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdHVkZW50TW9kdWxlfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2VDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgIHtpc0FjdGl2ZSA/IChcbiAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9QcmljaW5nRGV0YWlscy9BcnJvdy5zdmdcIlxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuYXJyb3dJY29ufVxuICAgICAgICAgICAgICAgICAgICBhbHQ9XCJhcnJvd1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICBpc0FjdGl2ZVxuICAgICAgICAgICAgICAgICAgICA/IGAke3Muc3ViTW9kdWxlc30gJHtzLnN1Yk1vZHVsZUFjdGl2ZX1gXG4gICAgICAgICAgICAgICAgICAgIDogcy5zdWJNb2R1bGVzXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd1N1Ym1vZHVsZShlYWNoLnZhbHVlKTtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2VhY2gubGFiZWx9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9O1xuXG4gIHNob3dNb2R1bGUgPSBrZXkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSwgYWN0aXZlTW9kdWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IG1vZCA9IFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXM7XG4gICAgY29uc3Qgc3ViID0gbW9kW2tleV0uc3ViTW9kdWxlcztcbiAgICBjb25zdCBzdWJtb2R1bGUgPSBPYmplY3QudmFsdWVzKHN1YiB8fCB7fSk7XG4gICAgaWYgKGtleSAhPT0gYWN0aXZlTW9kdWxlKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlTW9kdWxlOiBrZXksIGFjdGl2ZVN1Ym1vZHVsZTogc3VibW9kdWxlWzBdLnZhbHVlIH0pO1xuICAgICAgLyogdGhpcy5zZXRTdGF0ZShwcmV2U3RhdGUgPT4gKHtcbiAgICAgICAgc2hvdzogIXByZXZTdGF0ZS5zaG93LFxuICAgICAgfSkpOyAgICovXG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvdzogIXRydWUsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGFjdGl2ZU1vZHVsZToga2V5LCBhY3RpdmVTdWJtb2R1bGU6IHN1Ym1vZHVsZVswXS52YWx1ZSB9KTtcbiAgICAgIHRoaXMuc2V0U3RhdGUocHJldlN0YXRlID0+ICh7XG4gICAgICAgIHNob3c6ICFwcmV2U3RhdGUuc2hvdyxcbiAgICAgIH0pKTtcbiAgICB9XG4gIH07XG5cbiAgZGlzcGxheU1vZHVsZSA9ICgpID0+IHtcbiAgICBjb25zdCB7IHJvbGUsIGFjdGl2ZU1vZHVsZSwgc2hvdyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBmaWx0ZXIgPSBQbGFuRGV0YWlsc1tyb2xlXS5tb2R1bGVzO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJDb250YWluZXJzfT5cbiAgICAgICAge09iamVjdC52YWx1ZXMoZmlsdGVyKS5tYXAoaXRlbSA9PiB7XG4gICAgICAgICAgY29uc3QgaXNBY3RpdmVNb2R1bGUgPSBhY3RpdmVNb2R1bGUgPT09IGl0ZW0udmFsdWU7XG4gICAgICAgICAgY29uc3Qgc2hvd0FsbCA9IGFjdGl2ZU1vZHVsZSA9PT0gaXRlbS52YWx1ZTtcbiAgICAgICAgICBjb25zdCBpbWFnZVVybCA9IGlzQWN0aXZlTW9kdWxlID8gaXRlbS5hY3RpdmVJbWFnZVVybCA6IGl0ZW0uaW1hZ2VVcmw7XG4gICAgICAgICAgY29uc3QgaW1hZ2VNb2JpbGVVcmwgPVxuICAgICAgICAgICAgaXNBY3RpdmVNb2R1bGUgJiYgIXNob3cgPyBpdGVtLm1vYmlsZUltYWdlVXJsIDogaXRlbS5pbWFnZVVybDtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICBpc0FjdGl2ZU1vZHVsZSAmJiAhc2hvd1xuICAgICAgICAgICAgICAgICAgICA/IGAke3MubW9kdWxlQ29udGFpbmVyfSAke3MubW9kdWxlQ29udGFpbmVyQWN0aXZlfWBcbiAgICAgICAgICAgICAgICAgICAgOiBzLm1vZHVsZUNvbnRhaW5lclxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICB0aGlzLnNob3dNb2R1bGUoaXRlbS52YWx1ZSk7XG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17aW1hZ2VVcmx9XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgICBpc0FjdGl2ZU1vZHVsZSA/IGAke3MuaWNvbn0gJHtzLmljb25BY3RpdmV9YCA6IHMuaWNvblxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYWx0PVwiaW1nXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17aW1hZ2VNb2JpbGVVcmx9XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MubW9kdWxlSWNvbn1cbiAgICAgICAgICAgICAgICAgIGFsdD1cIm1vYmlsZUljb25cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPHBcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgIGlzQWN0aXZlTW9kdWxlICYmICFzaG93XG4gICAgICAgICAgICAgICAgICAgICAgPyBgJHtzLm1vZHVsZU5hbWV9ICR7cy5tb2R1bGVOYW1lQWN0aXZlfWBcbiAgICAgICAgICAgICAgICAgICAgICA6IHMubW9kdWxlTmFtZVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIHtpdGVtLmxhYmVsfVxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbHVzQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgIHtpc0FjdGl2ZU1vZHVsZSAmJiAhc2hvdyA/IChcbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvbWludXMuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucGx1c31cbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJtaW51c1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvcGx1cy5zdmdcIiBjbGFzc05hbWU9e3MucGx1c30gYWx0PVwicGx1c1wiIC8+XG4gICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAge2lzQWN0aXZlTW9kdWxlICYmICFzaG93ICYmIHNob3dBbGwgJiYgKFxuICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICB7T2JqZWN0LnZhbHVlcyhpdGVtLnN1Yk1vZHVsZXMgfHwge30pLm1hcChlYWNoID0+IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5zdWJNb2R1bGVNb2JpbGVDb250YWluZXJ9IGN1c3RvbS1zY3JvbGxiYXJgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViTW9kdWxlTmFtZX0+e2VhY2gubGFiZWx9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAge09iamVjdC52YWx1ZXMoZWFjaC5kYXRhIHx8IHt9KS5tYXAoZGF0YSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJtb2R1bGVEYXRhQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvSWNvbnMuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucmlnaHRJY29uc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJyaWdodFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Ym1vZHVsZURhdGF9PntkYXRhLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheU1vZHVsZXMgPSAoKSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgZmlsdGVyID0gUGxhbkRldGFpbHNbcm9sZV0ubW9kdWxlcztcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyQ29udGFpbmVyfT5cbiAgICAgICAge09iamVjdC52YWx1ZXMoZmlsdGVyKS5tYXAoaXRlbSA9PiB7XG4gICAgICAgICAgY29uc3QgaXNBY3RpdmVNb2R1bGUgPSBhY3RpdmVNb2R1bGUgPT09IGl0ZW0udmFsdWU7XG4gICAgICAgICAgY29uc3QgaW1hZ2VVcmwgPSBpc0FjdGl2ZU1vZHVsZSA/IGl0ZW0uYWN0aXZlSW1hZ2VVcmwgOiBpdGVtLmltYWdlVXJsO1xuICAgICAgICAgIGNvbnN0IGltYWdlTW9iaWxlVXJsID0gaXNBY3RpdmVNb2R1bGVcbiAgICAgICAgICAgID8gaXRlbS5tb2JpbGVJbWFnZVVybFxuICAgICAgICAgICAgOiBpdGVtLmltYWdlVXJsO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgaXNBY3RpdmVNb2R1bGVcbiAgICAgICAgICAgICAgICAgID8gYCR7cy5tb2R1bGVDb250YWluZXJ9ICR7cy5tb2R1bGVDb250YWluZXJBY3RpdmV9YFxuICAgICAgICAgICAgICAgICAgOiBzLm1vZHVsZUNvbnRhaW5lclxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dNb2R1bGVzKGl0ZW0udmFsdWUpO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz17aW1hZ2VVcmx9XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgIGlzQWN0aXZlTW9kdWxlID8gYCR7cy5pY29ufSAke3MuaWNvbkFjdGl2ZX1gIDogcy5pY29uXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGFsdD1cImltZ1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPXtpbWFnZU1vYmlsZVVybH0gY2xhc3NOYW1lPXtzLm1vZHVsZUljb259IGFsdD1cIm1vYmlsZVwiIC8+XG4gICAgICAgICAgICAgIDxwXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgIGlzQWN0aXZlTW9kdWxlXG4gICAgICAgICAgICAgICAgICAgID8gYCR7cy5tb2R1bGVOYW1lfSAke3MubW9kdWxlTmFtZUFjdGl2ZX1gXG4gICAgICAgICAgICAgICAgICAgIDogcy5tb2R1bGVOYW1lXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2l0ZW0ubGFiZWx9XG4gICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICBkaXNwbGF5VHlwZU9mVXNlciA9ICgpID0+IHtcbiAgICBjb25zdCB7IHJvbGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYnNDb250YWluZXJ9PlxuICAgICAgICB7T2JqZWN0LnZhbHVlcyhQbGFuRGV0YWlscykubWFwKGVhY2ggPT4ge1xuICAgICAgICAgIGNvbnN0IGlzQWN0aXZlID0gcm9sZSA9PT0gZWFjaC52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2lzQWN0aXZlID8gcy5hZG1pbkNvbnRhaW5lckFjdGl2ZSA6IHMuYWRtaW5Db250YWluZXJ9XG4gICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dSb2xlKGVhY2gudmFsdWUpO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAge2lzQWN0aXZlID8gKFxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvU2hhcGUuc3ZnXCJcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5yaWdodEljb259XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJyaWdodFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtpc0FjdGl2ZSA/IHMuYWN0aXZlIDogcy5hZG1pbkxhYmVsQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWdDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2VhY2guaW1nVXJsfSBjbGFzc05hbWU9e3MuYWRtaW5JbWd9IGFsdD1cInN0dWRlbnRcIiAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5hZG1pbkxhYmVsfT57ZWFjaC5sYWJlbH08L3NwYW4+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9O1xuXG4gIGRpc3BsYXlQbGFuZXMgPSAoKSA9PiA8aDEgY2xhc3NOYW1lPXtzLnBsYW5lfT5FdmVyeSBQbGFuIGluY2x1ZGVzPC9oMT47XG5cbiAgZGlzcGxheVByaWNpbmdIZWFkZXIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2luZ0hlYWRlcn0+XG4gICAgICA8ZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV90aXRsZX0+Q2hvb3NlIHlvdXIgZWRpdGlvbi48L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfdGl0bGV9PlRyeSBpdCBmcmVlIGZvciA3IGRheXMuPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX2NvbnRlbnR9PlxuICAgICAgICBFZ25pZnkgcGxhbnMgc3RhcnQgYXMgbG93IGFzIFJzLjQ1Ly0gcGVyIHN0dWRlbnQgcGVyIG1vbnRoLlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGFseVdob0FyZVlvdSA9ICgpID0+IChcbiAgICA8ZGl2PlxuICAgICAgPGgxIGNsYXNzTmFtZT17cy53aG9BcmVZb3V9PldobyBhcmUgeW91PzwvaDE+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGFseUdvQmFja0xpbmsgPSAoKSA9PiAoXG4gICAgPExpbmsgdG89XCIvcHJpY2luZ1wiIGNsYXNzTmFtZT17cy5nb0JhY2tMaW5rfT5cbiAgICAgIDxwPmJhY2sgdG8gcHJpY2luZyBkZXRhaWxzPC9wPlxuICAgIDwvTGluaz5cbiAgKTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vZHVsZXN9PlxuICAgICAgICB7dGhpcy5kaXNwbGF5UHJpY2luZ0hlYWRlcigpfVxuICAgICAgICB7dGhpcy5kaXNwYWx5R29CYWNrTGluaygpfVxuICAgICAgICB7dGhpcy5kaXNwYWx5V2hvQXJlWW91KCl9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vZHVsZURhdGF9PlxuICAgICAgICAgIHt0aGlzLmRpc3BsYXlUeXBlT2ZVc2VyKCl9XG4gICAgICAgICAge3RoaXMuZGlzcGxheVBsYW5lcygpfVxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vZHVsZUZsZXh9PlxuICAgICAgICAgICAge3RoaXMuZGlzcGxheU1vZHVsZXMoKX1cbiAgICAgICAgICAgIHt0aGlzLmRpc3BsYXlNb2R1bGUoKX1cbiAgICAgICAgICAgIHsvKiAgIHt0aGlzLmRpc3BsYXlNb2JpbGVNb2R1bGVzKCl9ICAqL31cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Yk1vZHVsZXNGbGV4fT5cbiAgICAgICAgICAgICAge3RoaXMuZGlzcGxheVN1Yk1vZHVsZXMoKX1cbiAgICAgICAgICAgICAge3RoaXMuZGlzcGxheVNlY3Rpb25zKCl9XG4gICAgICAgICAgICAgIHt0aGlzLmRpc3BsYXlTZWN0aW9uKCl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoUHJpY2luZ1ZpZXdEZXRhaWxzKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpY2luZ1ZpZXdEZXRhaWxzLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpY2luZ1ZpZXdEZXRhaWxzLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nVmlld0RldGFpbHMuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dC9MYXlvdXQnO1xuaW1wb3J0IFByaWNpbmdWaWV3RGV0YWlscyBmcm9tICcuL1ByaWNpbmdWaWV3RGV0YWlscyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogWydHZXRSYW5rcyBieSBFZ25pZnk6IEFzc2Vzc21lbnQgJiBBbmFseXRpY3MgUGxhdGZvcm0nXSxcbiAgICBjaHVua3M6IFsnUHJpY2luZ0RldGFpbHMnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQgZm9vdGVyQXNoPlxuICAgICAgICA8UHJpY2luZ1ZpZXdEZXRhaWxzIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQzFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBMURBO0FBZ0VBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUhBO0FBTUE7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBZEE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBSEE7QUFYQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQWJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBWkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQXBCQTtBQXZJQTtBQU5BO0FBc0tBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBSEE7QUFNQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFoQ0E7QUFtQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBSEE7QUFyQkE7QUFwQ0E7QUFOQTtBQXlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBaEJBO0FBREE7QUFOQTtBQStCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBWkE7QUFEQTtBQU5BO0FBd0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBVEE7QUFOQTtBQXZTQTtBQUpBO0FBa1VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUhBO0FBTUE7QUFFQTtBQUhBO0FBTUE7QUFBQTtBQUFBO0FBcEJBO0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUhBO0FBT0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBOURBO0FBaUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBekJBO0FBekZBO0FBTkE7QUFnSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQWJBO0FBREE7QUFOQTtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBTEE7QUFEQTtBQU5BO0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFYQTtBQURBO0FBTkE7QUE5S0E7QUFKQTtBQW5VQTtBQWdoQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hoQkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQWlCQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF6QkE7QUEyQkE7QUFDQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFwREE7QUFzREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQWhFQTtBQWtFQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUF4RUE7QUEwRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBakZBO0FBbUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBSUE7QUFDQTtBQW5IQTtBQXFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBR0E7QUFDQTtBQS9JQTtBQWlKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFHQTtBQUNBO0FBckxBO0FBdUxBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBMU1BO0FBNE1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFHQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBR0E7QUFDQTtBQS9SQTtBQWlTQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFHQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFHQTtBQUNBO0FBOVVBO0FBZ1ZBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7QUFDQTtBQWhYQTtBQWlYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbFhBO0FBb1hBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUExWEE7QUFnWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWxZQTtBQXNZQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF0WUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTRYQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBbmFBO0FBQ0E7QUFvYUE7Ozs7Ozs7QUM1YUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBU0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=