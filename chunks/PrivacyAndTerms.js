require("source-map-support").install();
exports.ids = ["PrivacyAndTerms"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/privacy-and-terms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n.PrivacyAndTerms-bannerSection-1i3ow {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.PrivacyAndTerms-heading-13FWP {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.PrivacyAndTerms-introContent-31zzH {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.PrivacyAndTerms-introContent-31zzH div {\n    padding-bottom: 40px;\n  }\n\n.PrivacyAndTerms-commit-3LsC1 {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.PrivacyAndTerms-commitHeading-2iU1G {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1Hc0T {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1Hc0T li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .PrivacyAndTerms-heading-13FWP {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .PrivacyAndTerms-heading-13FWP {\n    font-size: 35px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/PrivacyAndTerms.scss"],"names":[],"mappings":";AACA;EACE,oBAAoB;EACpB,kBAAkB;EAClB,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,kBAAkB;EAClB,mBAAmB;EACnB,kBAAkB;CACnB;;AAED;IACI,qBAAqB;GACtB;;AAEH;EACE,kBAAkB;EAClB,2CAA2C;CAC5C;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,qBAAqB;CACtB;;AAED;EACE,kBAAkB;EAClB,mBAAmB;EACnB,qBAAqB;CACtB;;AAED;IACI,qBAAqB;IACrB,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,iCAAiC;GAClC;;AAEH;EACE;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,gBAAgB;GACjB;CACF","file":"PrivacyAndTerms.scss","sourcesContent":["\n.bannerSection {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.heading {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.introContent {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.introContent div {\n    padding-bottom: 40px;\n  }\n\n.commit {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.commitHeading {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.commitContent {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.commitContent li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .heading {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .heading {\n    font-size: 35px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bannerSection": "PrivacyAndTerms-bannerSection-1i3ow",
	"heading": "PrivacyAndTerms-heading-13FWP",
	"introContent": "PrivacyAndTerms-introContent-31zzH",
	"commit": "PrivacyAndTerms-commit-3LsC1",
	"commitHeading": "PrivacyAndTerms-commitHeading-2iU1G",
	"commitContent": "PrivacyAndTerms-commitContent-1Hc0T"
};

/***/ }),

/***/ "./src/routes/privacy-and-terms/PrivacyAndTerms.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/privacy-and-terms/PrivacyAndTerms.scss");
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/PrivacyAndTerms.js";
 // import cx from 'classnames';


 // import Link from 'components/Link';


const introContent = [{
  content: 'Egnify is honored to be entrusted by educators and families to support their educational needs and school operations. We take responsibility to both support the effective use of student information and safeguard student privacy and information security.'
}, {
  content: 'Egnify supports schools – including their teachers, students and parents – to manage student data, carry out school operations, support instruction and learning opportunities, and develop and improve products/services intended for educational/school use. In so doing, it is critical that schools and school service providers build trust by effectively protecting the privacy of student information and communicating with parents about how student information is used and safeguarded.'
}, {
  content: 'We pledge to carry out responsible stewardship and appropriate use of student personal information according to the commitments below and in adherence to all laws applicable to us as school service providers.'
}];
const commitContent = [{
  content: 'Not collect, maintain, use or share student personal information beyond that needed for authorized educational/school purposes, or as authorized by the parent/student.'
}, {
  content: 'Not sell student personal information.'
}, {
  content: 'Not use or disclose student information collected through an educational/school service (whether personal information or otherwise) for behavioral targeting of advertisements to students.'
}, {
  content: 'Not build a personal profile of a student other than for supporting authorized educational/school purposes or as authorized by the parent/student.'
}, {
  content: 'Not make material changes to school service provider consumer privacy policies without first providing prominent notice to the account holder(s) (i.e., the educational institution/agency, or the parent/student when the information is collected directly from the student with student/parent consent) and allowing them choices before data is used in any manner inconsistent with terms they were initially provided; and not make material changes to other policies or practices governing the use of student personal information that are inconsistent with contractual requirements.'
}, {
  content: 'Collect, use, share, and retain student personal information only for purposes for which we were authorized by the educational institution/agency, teacher or the parent/student.'
}, {
  content: 'Disclose clearly in contracts or privacy policies, including in a manner easy for parents to understand, what types of student personal information we collect, if any, and the purposes for which the information we maintain is used or shared with third parties.'
}, {
  content: 'Support access to and correction of student personally identifiable information by the student or their authorized parent, either by assisting the educational institution in meeting its requirements or directly when the information is collected directly from the student with student/parent consent.'
}, {
  content: 'Maintain a comprehensive security program that is reasonably designed to protect the security, privacy, confidentiality, and integrity of student personal information against risks – such as unauthorized access or use, or unintended or inappropriate disclosure – through the use of administrative, technological, and physical safeguards appropriate to the sensitivity of the information.'
}, {
  content: 'Require that our vendors with whom student personal information is shared in order to deliver the educational service, if any, are obligated to implement these same commitments for the given student personal information.'
}, {
  content: 'Allow a successor entity to maintain the student personal information, in the case of our merger or acquisition by another entity, provided the successor entity is subject to these same commitments for the previously collected student personal information.'
}];

class PrivacyAndTerms extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.pageview(window.location.href);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bannerSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, "Privacy Policy")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.introContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, introContent.map(content => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, content.content))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commit,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commitHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, "We Commit To:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commitContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }, commitContent.map(content => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: this
    }, content.content)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, "We use Google Analytics as described in", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://policies.google.com/technologies/partner-sites",
      target: "_blank",
      style: {
        textDecoration: 'underline'
      },
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, "\"How Google uses data when you use our partner's sites or apps.\""), "You can prvent your data from being used by Google Analytics on our websites by installing the Google Analytics opt-out browser add-on. For enhanced privacy purposes,we also employ IP address masking, a technique used to truncate IP addresses collected by Google Analytics and store them in an abbreviated form to prevent them from being traced back to individual users. Portions of our website may also use Google Analytics for Display Advertisers including DoubleClick or Dynamic Remarketing which provide interest-based ads based on your visit to this or other websites. You can use Ads Settings to manage the Google ads you see and opt-out of interest-based ads. We also use Adobe Marketing Cloud as described.You can similary exercise your rights with respect to use of this data as described in the \"Exercising Choice\" section below."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }, "Google and its Affiliates may retain and use, subject to the terms of its privacy policy (located at www.google.com/privacy.html), information collected in Your use of the Service. Google will not shareYour Customer Data or any Third Party's Customer Data with any third parties unless Google (i) has Your consent for any Customer Data or any Third Party's consent for the Third Party's Customer Data; (ii) concludes that it is required by law or has a google faith belief that access, preservation or disclosure of Customer Data is reasonably necessary to protect the rights, property or saftey of Google, its users or the public; or (iii) provides Customer Data in certain limited circumstances to third parties to carry out tasks on Google's behalf(e.g., billing or data storage) with strict restrictions that prevent the data from being used or shared except as directed by Google. When this is done, it is subject to agreements that oblige those parties to process Customer Data only on Google's instructions and in compliance with this Agreement and appropriate confidentiality and security measures.")))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(PrivacyAndTerms));

/***/ }),

/***/ "./src/routes/privacy-and-terms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/privacy-and-terms/PrivacyAndTerms.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/privacy-and-terms/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/privacy-and-terms/PrivacyAndTerms.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/index.js";




async function action() {
  return {
    title: 'Privacy Policy | Egnify',
    chunks: ['PrivacyAndTerms'],
    content: 'Get to know about our privacy policy.',
    keywords: 'privacy policy',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1ByaXZhY3lBbmRUZXJtcy5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJpdmFjeS1hbmQtdGVybXMvUHJpdmFjeUFuZFRlcm1zLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcml2YWN5LWFuZC10ZXJtcy9Qcml2YWN5QW5kVGVybXMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcml2YWN5LWFuZC10ZXJtcy9Qcml2YWN5QW5kVGVybXMuc2Nzcz9lNWM4IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJpdmFjeS1hbmQtdGVybXMvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbi5Qcml2YWN5QW5kVGVybXMtYmFubmVyU2VjdGlvbi0xaTNvdyB7XFxuICBiYWNrZ3JvdW5kOiAjM2UzZTVmO1xcbiAgbWluLWhlaWdodDogMzY2cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgxNjZkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDE2NmRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjg0ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1oZWFkaW5nLTEzRldQIHtcXG4gIGhlaWdodDogNThweDtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1pbnRyb0NvbnRlbnQtMzF6ekgge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICBwYWRkaW5nLXRvcDogNDBweDtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1pbnRyb0NvbnRlbnQtMzF6ekggZGl2IHtcXG4gICAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1jb21taXQtM0xzQzEge1xcbiAgcGFkZGluZy10b3A6IDUwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIxNiwgMjE2LCAyMTYsIDAuMik7XFxufVxcblxcbi5Qcml2YWN5QW5kVGVybXMtY29tbWl0SGVhZGluZy0yaVUxRyB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWNvbW1pdENvbnRlbnQtMUhjMFQge1xcbiAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICBwYWRkaW5nLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1jb21taXRDb250ZW50LTFIYzBUIGxpIHtcXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIGxpc3Qtc3R5bGUtdHlwZTogZGlzYyAhaW1wb3J0YW50O1xcbiAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5Qcml2YWN5QW5kVGVybXMtaGVhZGluZy0xM0ZXUCB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MDBweCkge1xcbiAgLlByaXZhY3lBbmRUZXJtcy1oZWFkaW5nLTEzRldQIHtcXG4gICAgZm9udC1zaXplOiAzNXB4O1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJpdmFjeS1hbmQtdGVybXMvUHJpdmFjeUFuZFRlcm1zLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQUNBO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixvRUFBb0U7RUFDcEUsK0RBQStEO0VBQy9ELDREQUE0RDtFQUM1RCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0NBQ25COztBQUVEO0lBQ0kscUJBQXFCO0dBQ3RCOztBQUVIO0VBQ0Usa0JBQWtCO0VBQ2xCLDJDQUEyQztDQUM1Qzs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixxQkFBcUI7Q0FDdEI7O0FBRUQ7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsaUNBQWlDO0dBQ2xDOztBQUVIO0VBQ0U7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsZ0JBQWdCO0dBQ2pCO0NBQ0ZcIixcImZpbGVcIjpcIlByaXZhY3lBbmRUZXJtcy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIlxcbi5iYW5uZXJTZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICMzZTNlNWY7XFxuICBtaW4taGVpZ2h0OiAzNjZweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDE2NmRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMTY2ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyODRkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uaGVhZGluZyB7XFxuICBoZWlnaHQ6IDU4cHg7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5pbnRyb0NvbnRlbnQge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICBwYWRkaW5nLXRvcDogNDBweDtcXG59XFxuXFxuLmludHJvQ29udGVudCBkaXYge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4uY29tbWl0IHtcXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMTYsIDIxNiwgMjE2LCAwLjIpO1xcbn1cXG5cXG4uY29tbWl0SGVhZGluZyB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uY29tbWl0Q29udGVudCB7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uY29tbWl0Q29udGVudCBsaSB7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBsaXN0LXN0eWxlLXR5cGU6IGRpc2MgIWltcG9ydGFudDtcXG4gIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XFxuICAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MDBweCkge1xcbiAgLmhlYWRpbmcge1xcbiAgICBmb250LXNpemU6IDM1cHg7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJiYW5uZXJTZWN0aW9uXCI6IFwiUHJpdmFjeUFuZFRlcm1zLWJhbm5lclNlY3Rpb24tMWkzb3dcIixcblx0XCJoZWFkaW5nXCI6IFwiUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctMTNGV1BcIixcblx0XCJpbnRyb0NvbnRlbnRcIjogXCJQcml2YWN5QW5kVGVybXMtaW50cm9Db250ZW50LTMxenpIXCIsXG5cdFwiY29tbWl0XCI6IFwiUHJpdmFjeUFuZFRlcm1zLWNvbW1pdC0zTHNDMVwiLFxuXHRcImNvbW1pdEhlYWRpbmdcIjogXCJQcml2YWN5QW5kVGVybXMtY29tbWl0SGVhZGluZy0yaVUxR1wiLFxuXHRcImNvbW1pdENvbnRlbnRcIjogXCJQcml2YWN5QW5kVGVybXMtY29tbWl0Q29udGVudC0xSGMwVFwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG4vLyBpbXBvcnQgY3ggZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG4vLyBpbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IHMgZnJvbSAnLi9Qcml2YWN5QW5kVGVybXMuc2Nzcyc7XG5cbmNvbnN0IGludHJvQ29udGVudCA9IFtcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnRWduaWZ5IGlzIGhvbm9yZWQgdG8gYmUgZW50cnVzdGVkIGJ5IGVkdWNhdG9ycyBhbmQgZmFtaWxpZXMgdG8gc3VwcG9ydCB0aGVpciBlZHVjYXRpb25hbCBuZWVkcyBhbmQgc2Nob29sIG9wZXJhdGlvbnMuIFdlIHRha2UgcmVzcG9uc2liaWxpdHkgdG8gYm90aCBzdXBwb3J0IHRoZSBlZmZlY3RpdmUgdXNlIG9mIHN0dWRlbnQgaW5mb3JtYXRpb24gYW5kIHNhZmVndWFyZCBzdHVkZW50IHByaXZhY3kgYW5kIGluZm9ybWF0aW9uIHNlY3VyaXR5LicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ0VnbmlmeSBzdXBwb3J0cyBzY2hvb2xzIOKAkyBpbmNsdWRpbmcgdGhlaXIgdGVhY2hlcnMsIHN0dWRlbnRzIGFuZCBwYXJlbnRzIOKAkyB0byBtYW5hZ2Ugc3R1ZGVudCBkYXRhLCBjYXJyeSBvdXQgc2Nob29sIG9wZXJhdGlvbnMsIHN1cHBvcnQgaW5zdHJ1Y3Rpb24gYW5kIGxlYXJuaW5nIG9wcG9ydHVuaXRpZXMsIGFuZCBkZXZlbG9wIGFuZCBpbXByb3ZlIHByb2R1Y3RzL3NlcnZpY2VzIGludGVuZGVkIGZvciBlZHVjYXRpb25hbC9zY2hvb2wgdXNlLiBJbiBzbyBkb2luZywgaXQgaXMgY3JpdGljYWwgdGhhdCBzY2hvb2xzIGFuZCBzY2hvb2wgc2VydmljZSBwcm92aWRlcnMgYnVpbGQgdHJ1c3QgYnkgZWZmZWN0aXZlbHkgcHJvdGVjdGluZyB0aGUgcHJpdmFjeSBvZiBzdHVkZW50IGluZm9ybWF0aW9uIGFuZCBjb21tdW5pY2F0aW5nIHdpdGggcGFyZW50cyBhYm91dCBob3cgc3R1ZGVudCBpbmZvcm1hdGlvbiBpcyB1c2VkIGFuZCBzYWZlZ3VhcmRlZC4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdXZSBwbGVkZ2UgdG8gY2Fycnkgb3V0IHJlc3BvbnNpYmxlIHN0ZXdhcmRzaGlwIGFuZCBhcHByb3ByaWF0ZSB1c2Ugb2Ygc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiBhY2NvcmRpbmcgdG8gdGhlIGNvbW1pdG1lbnRzIGJlbG93IGFuZCBpbiBhZGhlcmVuY2UgdG8gYWxsIGxhd3MgYXBwbGljYWJsZSB0byB1cyBhcyBzY2hvb2wgc2VydmljZSBwcm92aWRlcnMuJyxcbiAgfSxcbl07XG5cbmNvbnN0IGNvbW1pdENvbnRlbnQgPSBbXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ05vdCBjb2xsZWN0LCBtYWludGFpbiwgdXNlIG9yIHNoYXJlIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24gYmV5b25kIHRoYXQgbmVlZGVkIGZvciBhdXRob3JpemVkIGVkdWNhdGlvbmFsL3NjaG9vbCBwdXJwb3Nlcywgb3IgYXMgYXV0aG9yaXplZCBieSB0aGUgcGFyZW50L3N0dWRlbnQuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6ICdOb3Qgc2VsbCBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uLicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ05vdCB1c2Ugb3IgZGlzY2xvc2Ugc3R1ZGVudCBpbmZvcm1hdGlvbiBjb2xsZWN0ZWQgdGhyb3VnaCBhbiBlZHVjYXRpb25hbC9zY2hvb2wgc2VydmljZSAod2hldGhlciBwZXJzb25hbCBpbmZvcm1hdGlvbiBvciBvdGhlcndpc2UpIGZvciBiZWhhdmlvcmFsIHRhcmdldGluZyBvZiBhZHZlcnRpc2VtZW50cyB0byBzdHVkZW50cy4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdOb3QgYnVpbGQgYSBwZXJzb25hbCBwcm9maWxlIG9mIGEgc3R1ZGVudCBvdGhlciB0aGFuIGZvciBzdXBwb3J0aW5nIGF1dGhvcml6ZWQgZWR1Y2F0aW9uYWwvc2Nob29sIHB1cnBvc2VzIG9yIGFzIGF1dGhvcml6ZWQgYnkgdGhlIHBhcmVudC9zdHVkZW50LicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ05vdCBtYWtlIG1hdGVyaWFsIGNoYW5nZXMgdG8gc2Nob29sIHNlcnZpY2UgcHJvdmlkZXIgY29uc3VtZXIgcHJpdmFjeSBwb2xpY2llcyB3aXRob3V0IGZpcnN0IHByb3ZpZGluZyBwcm9taW5lbnQgbm90aWNlIHRvIHRoZSBhY2NvdW50IGhvbGRlcihzKSAoaS5lLiwgdGhlIGVkdWNhdGlvbmFsIGluc3RpdHV0aW9uL2FnZW5jeSwgb3IgdGhlIHBhcmVudC9zdHVkZW50IHdoZW4gdGhlIGluZm9ybWF0aW9uIGlzIGNvbGxlY3RlZCBkaXJlY3RseSBmcm9tIHRoZSBzdHVkZW50IHdpdGggc3R1ZGVudC9wYXJlbnQgY29uc2VudCkgYW5kIGFsbG93aW5nIHRoZW0gY2hvaWNlcyBiZWZvcmUgZGF0YSBpcyB1c2VkIGluIGFueSBtYW5uZXIgaW5jb25zaXN0ZW50IHdpdGggdGVybXMgdGhleSB3ZXJlIGluaXRpYWxseSBwcm92aWRlZDsgYW5kIG5vdCBtYWtlIG1hdGVyaWFsIGNoYW5nZXMgdG8gb3RoZXIgcG9saWNpZXMgb3IgcHJhY3RpY2VzIGdvdmVybmluZyB0aGUgdXNlIG9mIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24gdGhhdCBhcmUgaW5jb25zaXN0ZW50IHdpdGggY29udHJhY3R1YWwgcmVxdWlyZW1lbnRzLicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ0NvbGxlY3QsIHVzZSwgc2hhcmUsIGFuZCByZXRhaW4gc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiBvbmx5IGZvciBwdXJwb3NlcyBmb3Igd2hpY2ggd2Ugd2VyZSBhdXRob3JpemVkIGJ5IHRoZSBlZHVjYXRpb25hbCBpbnN0aXR1dGlvbi9hZ2VuY3ksIHRlYWNoZXIgb3IgdGhlIHBhcmVudC9zdHVkZW50LicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ0Rpc2Nsb3NlIGNsZWFybHkgaW4gY29udHJhY3RzIG9yIHByaXZhY3kgcG9saWNpZXMsIGluY2x1ZGluZyBpbiBhIG1hbm5lciBlYXN5IGZvciBwYXJlbnRzIHRvIHVuZGVyc3RhbmQsIHdoYXQgdHlwZXMgb2Ygc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiB3ZSBjb2xsZWN0LCBpZiBhbnksIGFuZCB0aGUgcHVycG9zZXMgZm9yIHdoaWNoIHRoZSBpbmZvcm1hdGlvbiB3ZSBtYWludGFpbiBpcyB1c2VkIG9yIHNoYXJlZCB3aXRoIHRoaXJkIHBhcnRpZXMuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnU3VwcG9ydCBhY2Nlc3MgdG8gYW5kIGNvcnJlY3Rpb24gb2Ygc3R1ZGVudCBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiBieSB0aGUgc3R1ZGVudCBvciB0aGVpciBhdXRob3JpemVkIHBhcmVudCwgZWl0aGVyIGJ5IGFzc2lzdGluZyB0aGUgZWR1Y2F0aW9uYWwgaW5zdGl0dXRpb24gaW4gbWVldGluZyBpdHMgcmVxdWlyZW1lbnRzIG9yIGRpcmVjdGx5IHdoZW4gdGhlIGluZm9ybWF0aW9uIGlzIGNvbGxlY3RlZCBkaXJlY3RseSBmcm9tIHRoZSBzdHVkZW50IHdpdGggc3R1ZGVudC9wYXJlbnQgY29uc2VudC4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdNYWludGFpbiBhIGNvbXByZWhlbnNpdmUgc2VjdXJpdHkgcHJvZ3JhbSB0aGF0IGlzIHJlYXNvbmFibHkgZGVzaWduZWQgdG8gcHJvdGVjdCB0aGUgc2VjdXJpdHksIHByaXZhY3ksIGNvbmZpZGVudGlhbGl0eSwgYW5kIGludGVncml0eSBvZiBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uIGFnYWluc3Qgcmlza3Mg4oCTIHN1Y2ggYXMgdW5hdXRob3JpemVkIGFjY2VzcyBvciB1c2UsIG9yIHVuaW50ZW5kZWQgb3IgaW5hcHByb3ByaWF0ZSBkaXNjbG9zdXJlIOKAkyB0aHJvdWdoIHRoZSB1c2Ugb2YgYWRtaW5pc3RyYXRpdmUsIHRlY2hub2xvZ2ljYWwsIGFuZCBwaHlzaWNhbCBzYWZlZ3VhcmRzIGFwcHJvcHJpYXRlIHRvIHRoZSBzZW5zaXRpdml0eSBvZiB0aGUgaW5mb3JtYXRpb24uJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnUmVxdWlyZSB0aGF0IG91ciB2ZW5kb3JzIHdpdGggd2hvbSBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uIGlzIHNoYXJlZCBpbiBvcmRlciB0byBkZWxpdmVyIHRoZSBlZHVjYXRpb25hbCBzZXJ2aWNlLCBpZiBhbnksIGFyZSBvYmxpZ2F0ZWQgdG8gaW1wbGVtZW50IHRoZXNlIHNhbWUgY29tbWl0bWVudHMgZm9yIHRoZSBnaXZlbiBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uLicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ0FsbG93IGEgc3VjY2Vzc29yIGVudGl0eSB0byBtYWludGFpbiB0aGUgc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiwgaW4gdGhlIGNhc2Ugb2Ygb3VyIG1lcmdlciBvciBhY3F1aXNpdGlvbiBieSBhbm90aGVyIGVudGl0eSwgcHJvdmlkZWQgdGhlIHN1Y2Nlc3NvciBlbnRpdHkgaXMgc3ViamVjdCB0byB0aGVzZSBzYW1lIGNvbW1pdG1lbnRzIGZvciB0aGUgcHJldmlvdXNseSBjb2xsZWN0ZWQgc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbi4nLFxuICB9LFxuXTtcblxuY2xhc3MgUHJpdmFjeUFuZFRlcm1zIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgUmVhY3RHQS5pbml0aWFsaXplKHdpbmRvdy5BcHAuZ29vZ2xlVHJhY2tpbmdJZCwge1xuICAgICAgZGVidWc6IGZhbHNlLFxuICAgIH0pO1xuICAgIFJlYWN0R0EucGFnZXZpZXcod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5iYW5uZXJTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nfT5Qcml2YWN5IFBvbGljeTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW50cm9Db250ZW50fT5cbiAgICAgICAgICB7aW50cm9Db250ZW50Lm1hcChjb250ZW50ID0+IChcbiAgICAgICAgICAgIDxkaXY+e2NvbnRlbnQuY29udGVudH08L2Rpdj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbW1pdH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tbWl0SGVhZGluZ30+V2UgQ29tbWl0IFRvOjwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbW1pdENvbnRlbnR9PlxuICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICB7Y29tbWl0Q29udGVudC5tYXAoY29udGVudCA9PiAoXG4gICAgICAgICAgICAgICAgPGxpPntjb250ZW50LmNvbnRlbnR9PC9saT5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICBXZSB1c2UgR29vZ2xlIEFuYWx5dGljcyBhcyBkZXNjcmliZWQgaW5cbiAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vcG9saWNpZXMuZ29vZ2xlLmNvbS90ZWNobm9sb2dpZXMvcGFydG5lci1zaXRlc1wiXG4gICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgdGV4dERlY29yYXRpb246ICd1bmRlcmxpbmUnIH19XG4gICAgICAgICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAmcXVvdDtIb3cgR29vZ2xlIHVzZXMgZGF0YSB3aGVuIHlvdSB1c2Ugb3VyIHBhcnRuZXImYXBvcztzXG4gICAgICAgICAgICAgICAgICBzaXRlcyBvciBhcHBzLiZxdW90O1xuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICBZb3UgY2FuIHBydmVudCB5b3VyIGRhdGEgZnJvbSBiZWluZyB1c2VkIGJ5IEdvb2dsZSBBbmFseXRpY3Mgb25cbiAgICAgICAgICAgICAgICBvdXIgd2Vic2l0ZXMgYnkgaW5zdGFsbGluZyB0aGUgR29vZ2xlIEFuYWx5dGljcyBvcHQtb3V0IGJyb3dzZXJcbiAgICAgICAgICAgICAgICBhZGQtb24uIEZvciBlbmhhbmNlZCBwcml2YWN5IHB1cnBvc2VzLHdlIGFsc28gZW1wbG95IElQIGFkZHJlc3NcbiAgICAgICAgICAgICAgICBtYXNraW5nLCBhIHRlY2huaXF1ZSB1c2VkIHRvIHRydW5jYXRlIElQIGFkZHJlc3NlcyBjb2xsZWN0ZWQgYnlcbiAgICAgICAgICAgICAgICBHb29nbGUgQW5hbHl0aWNzIGFuZCBzdG9yZSB0aGVtIGluIGFuIGFiYnJldmlhdGVkIGZvcm0gdG9cbiAgICAgICAgICAgICAgICBwcmV2ZW50IHRoZW0gZnJvbSBiZWluZyB0cmFjZWQgYmFjayB0byBpbmRpdmlkdWFsIHVzZXJzLlxuICAgICAgICAgICAgICAgIFBvcnRpb25zIG9mIG91ciB3ZWJzaXRlIG1heSBhbHNvIHVzZSBHb29nbGUgQW5hbHl0aWNzIGZvclxuICAgICAgICAgICAgICAgIERpc3BsYXkgQWR2ZXJ0aXNlcnMgaW5jbHVkaW5nIERvdWJsZUNsaWNrIG9yIER5bmFtaWMgUmVtYXJrZXRpbmdcbiAgICAgICAgICAgICAgICB3aGljaCBwcm92aWRlIGludGVyZXN0LWJhc2VkIGFkcyBiYXNlZCBvbiB5b3VyIHZpc2l0IHRvIHRoaXMgb3JcbiAgICAgICAgICAgICAgICBvdGhlciB3ZWJzaXRlcy4gWW91IGNhbiB1c2UgQWRzIFNldHRpbmdzIHRvIG1hbmFnZSB0aGUgR29vZ2xlXG4gICAgICAgICAgICAgICAgYWRzIHlvdSBzZWUgYW5kIG9wdC1vdXQgb2YgaW50ZXJlc3QtYmFzZWQgYWRzLiBXZSBhbHNvIHVzZSBBZG9iZVxuICAgICAgICAgICAgICAgIE1hcmtldGluZyBDbG91ZCBhcyBkZXNjcmliZWQuWW91IGNhbiBzaW1pbGFyeSBleGVyY2lzZSB5b3VyXG4gICAgICAgICAgICAgICAgcmlnaHRzIHdpdGggcmVzcGVjdCB0byB1c2Ugb2YgdGhpcyBkYXRhIGFzIGRlc2NyaWJlZCBpbiB0aGVcbiAgICAgICAgICAgICAgICAmcXVvdDtFeGVyY2lzaW5nIENob2ljZSZxdW90OyBzZWN0aW9uIGJlbG93LlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgR29vZ2xlIGFuZCBpdHMgQWZmaWxpYXRlcyBtYXkgcmV0YWluIGFuZCB1c2UsIHN1YmplY3QgdG8gdGhlXG4gICAgICAgICAgICAgICAgdGVybXMgb2YgaXRzIHByaXZhY3kgcG9saWN5IChsb2NhdGVkIGF0XG4gICAgICAgICAgICAgICAgd3d3Lmdvb2dsZS5jb20vcHJpdmFjeS5odG1sKSwgaW5mb3JtYXRpb24gY29sbGVjdGVkIGluIFlvdXIgdXNlXG4gICAgICAgICAgICAgICAgb2YgdGhlIFNlcnZpY2UuIEdvb2dsZSB3aWxsIG5vdCBzaGFyZVlvdXIgQ3VzdG9tZXIgRGF0YSBvciBhbnlcbiAgICAgICAgICAgICAgICBUaGlyZCBQYXJ0eSZhcG9zO3MgQ3VzdG9tZXIgRGF0YSB3aXRoIGFueSB0aGlyZCBwYXJ0aWVzIHVubGVzc1xuICAgICAgICAgICAgICAgIEdvb2dsZSAoaSkgaGFzIFlvdXIgY29uc2VudCBmb3IgYW55IEN1c3RvbWVyIERhdGEgb3IgYW55IFRoaXJkXG4gICAgICAgICAgICAgICAgUGFydHkmYXBvcztzIGNvbnNlbnQgZm9yIHRoZSBUaGlyZCBQYXJ0eSZhcG9zO3MgQ3VzdG9tZXIgRGF0YTtcbiAgICAgICAgICAgICAgICAoaWkpIGNvbmNsdWRlcyB0aGF0IGl0IGlzIHJlcXVpcmVkIGJ5IGxhdyBvciBoYXMgYSBnb29nbGUgZmFpdGhcbiAgICAgICAgICAgICAgICBiZWxpZWYgdGhhdCBhY2Nlc3MsIHByZXNlcnZhdGlvbiBvciBkaXNjbG9zdXJlIG9mIEN1c3RvbWVyIERhdGFcbiAgICAgICAgICAgICAgICBpcyByZWFzb25hYmx5IG5lY2Vzc2FyeSB0byBwcm90ZWN0IHRoZSByaWdodHMsIHByb3BlcnR5IG9yXG4gICAgICAgICAgICAgICAgc2FmdGV5IG9mIEdvb2dsZSwgaXRzIHVzZXJzIG9yIHRoZSBwdWJsaWM7IG9yIChpaWkpIHByb3ZpZGVzXG4gICAgICAgICAgICAgICAgQ3VzdG9tZXIgRGF0YSBpbiBjZXJ0YWluIGxpbWl0ZWQgY2lyY3Vtc3RhbmNlcyB0byB0aGlyZCBwYXJ0aWVzXG4gICAgICAgICAgICAgICAgdG8gY2Fycnkgb3V0IHRhc2tzIG9uIEdvb2dsZSZhcG9zO3MgYmVoYWxmKGUuZy4sIGJpbGxpbmcgb3IgZGF0YVxuICAgICAgICAgICAgICAgIHN0b3JhZ2UpIHdpdGggc3RyaWN0IHJlc3RyaWN0aW9ucyB0aGF0IHByZXZlbnQgdGhlIGRhdGEgZnJvbVxuICAgICAgICAgICAgICAgIGJlaW5nIHVzZWQgb3Igc2hhcmVkIGV4Y2VwdCBhcyBkaXJlY3RlZCBieSBHb29nbGUuIFdoZW4gdGhpcyBpc1xuICAgICAgICAgICAgICAgIGRvbmUsIGl0IGlzIHN1YmplY3QgdG8gYWdyZWVtZW50cyB0aGF0IG9ibGlnZSB0aG9zZSBwYXJ0aWVzIHRvXG4gICAgICAgICAgICAgICAgcHJvY2VzcyBDdXN0b21lciBEYXRhIG9ubHkgb24gR29vZ2xlJmFwb3M7cyBpbnN0cnVjdGlvbnMgYW5kIGluXG4gICAgICAgICAgICAgICAgY29tcGxpYW5jZSB3aXRoIHRoaXMgQWdyZWVtZW50IGFuZCBhcHByb3ByaWF0ZSBjb25maWRlbnRpYWxpdHlcbiAgICAgICAgICAgICAgICBhbmQgc2VjdXJpdHkgbWVhc3VyZXMuXG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8L3VsPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShQcml2YWN5QW5kVGVybXMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Qcml2YWN5QW5kVGVybXMuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Qcml2YWN5QW5kVGVybXMuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJpdmFjeUFuZFRlcm1zIGZyb20gJy4vUHJpdmFjeUFuZFRlcm1zJztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdQcml2YWN5IFBvbGljeSB8IEVnbmlmeScsXG4gICAgY2h1bmtzOiBbJ1ByaXZhY3lBbmRUZXJtcyddLFxuICAgIGNvbnRlbnQ6ICdHZXQgdG8ga25vdyBhYm91dCBvdXIgcHJpdmFjeSBwb2xpY3kuJyxcbiAgICBrZXl3b3JkczogJ3ByaXZhY3kgcG9saWN5JyxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxQcml2YWN5QW5kVGVybXMgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBd0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTBCQTtBQUNBO0FBL0VBO0FBQ0E7QUFnRkE7Ozs7Ozs7QUNySkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFXQTtBQUNBO0FBQ0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==