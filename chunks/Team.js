require("source-map-support").install();
exports.ids = ["Team"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/team/Team.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Team-heading-3CBSV {\n  font-size: 30px;\n  font-weight: 600;\n  letter-spacing: 1.3px;\n  color: #3e3e5f;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n}\n\n.Team-bannerSection-a9i4l {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: -o-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: linear-gradient(258deg, #ffae71, #ff315e);\n  padding-top: 64px;\n  padding-top: 4rem;\n}\n\n.Team-bannerSection-a9i4l .Team-heading-3CBSV {\n    font-size: 48px;\n    font-size: 3rem;\n    font-weight: 300;\n    color: #fff;\n  }\n\n.Team-bannerSection-a9i4l .Team-description-I_ug0 {\n    font-size: 24px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: normal;\n    letter-spacing: 1px;\n    color: #fff;\n    margin-top: 40px;\n    margin-top: 2.5rem;\n  }\n\n.Team-teamSection-1pCr7 {\n  padding-top: 48px;\n  padding-top: 3rem;\n  padding-bottom: 48px;\n  padding-bottom: 3rem;\n  background: #fff;\n}\n\n.Team-tagLine-2Wznz {\n  font-size: 12px;\n  line-height: 21px;\n  color: #5f6368;\n}\n\n@media only screen and (min-width: 960px) {\n  .Team-adviceMemberWrapper-2mqPb:nth-child(1) {\n    padding-left: 0 !important;\n  }\n\n  .Team-adviceMemberWrapper-2mqPb:nth-child(2) {\n    padding-right: 0 !important;\n  }\n}\n\n.Team-advisoryMemberCard-3sYFo {\n  margin-bottom: 36px;\n  margin-bottom: 2.25rem;\n  min-height: 200px;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n}\n\n.Team-advisoryMemberCard-3sYFo img {\n    width: 200px;\n    height: 200px;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-info-3Y35F {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n    width: calc(100% - 200px);\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-name-3STtX {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-tagLine-2Wznz {\n    font-size: 12px;\n    height: 24;\n    color: #5f6368;\n    margin-top: 32px;\n    margin-top: 2rem;\n  }\n\n@media only screen and (max-width: 959px) {\n    .Team-teamSection-1pCr7 .Team-heading-3CBSV {\n      text-align: center !important;\n    }\n\n  .Team-advisoryMemberCard-3sYFo {\n    min-height: 150px;\n  }\n\n    .Team-advisoryMemberCard-3sYFo img {\n      width: 150px;\n      height: 150px;\n      -o-object-fit: cover;\n         object-fit: cover;\n      -o-object-position: 100% 0;\n         object-position: 100% 0;\n    }\n\n    .Team-advisoryMemberCard-3sYFo .Team-info-3Y35F {\n      padding: 1.5rem 1rem;\n      width: calc(100% - 150px);\n    }\n}\n\n.Team-teamMemberCard-32YcH {\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.Team-teamMemberCard-32YcH img {\n    height: 200px;\n    width: 200px;\n    border-radius: 50%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.Team-teamMemberCard-32YcH .Team-info-3Y35F {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n  }\n\n.Team-teamMemberCard-32YcH .Team-name-3STtX {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.Team-teamMemberCard-32YcH .Team-title-1fhFk {\n    margin-top: 8px;\n    margin-top: 0.5rem;\n    font-size: 20px;\n    color: #5f6368;\n  }\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/Team.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,iBAAiB;EACjB,sBAAsB;EACtB,eAAe;EACf,oBAAoB;EACpB,sBAAsB;CACvB;;AAED;EACE,oBAAoB;EACpB,kBAAkB;EAClB,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;IACI,gBAAgB;IAChB,gBAAgB;IAChB,iBAAiB;IACjB,YAAY;GACb;;AAEH;IACI,gBAAgB;IAChB,oBAAoB;IACpB,mBAAmB;IACnB,qBAAqB;IACrB,oBAAoB;IACpB,oBAAoB;IACpB,YAAY;IACZ,iBAAiB;IACjB,mBAAmB;GACpB;;AAEH;EACE,kBAAkB;EAClB,kBAAkB;EAClB,qBAAqB;EACrB,qBAAqB;EACrB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;CAChB;;AAED;EACE;IACE,2BAA2B;GAC5B;;EAED;IACE,4BAA4B;GAC7B;CACF;;AAED;EACE,oBAAoB;EACpB,uBAAuB;EACvB,kBAAkB;EAClB,mBAAmB;EACnB,uBAAuB;EACvB,oDAAoD;UAC5C,4CAA4C;CACrD;;AAED;IACI,aAAa;IACb,cAAc;IACd,qBAAqB;OAClB,kBAAkB;IACrB,2BAA2B;OACxB,wBAAwB;GAC5B;;AAEH;IACI,mBAAmB;IACnB,qBAAqB;IACrB,0BAA0B;GAC3B;;AAEH;IACI,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;GAChB;;AAEH;IACI,gBAAgB;IAChB,WAAW;IACX,eAAe;IACf,iBAAiB;IACjB,iBAAiB;GAClB;;AAEH;IACI;MACE,8BAA8B;KAC/B;;EAEH;IACE,kBAAkB;GACnB;;IAEC;MACE,aAAa;MACb,cAAc;MACd,qBAAqB;SAClB,kBAAkB;MACrB,2BAA2B;SACxB,wBAAwB;KAC5B;;IAED;MACE,qBAAqB;MACrB,0BAA0B;KAC3B;CACJ;;AAED;EACE,oBAAoB;EACpB,oBAAoB;EACpB,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;CACpB;;AAED;IACI,cAAc;IACd,aAAa;IACb,mBAAmB;IACnB,qBAAqB;OAClB,kBAAkB;IACrB,2BAA2B;OACxB,wBAAwB;GAC5B;;AAEH;IACI,mBAAmB;IACnB,qBAAqB;GACtB;;AAEH;IACI,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;GAChB;;AAEH;IACI,gBAAgB;IAChB,mBAAmB;IACnB,gBAAgB;IAChB,eAAe;GAChB","file":"Team.scss","sourcesContent":[".heading {\n  font-size: 30px;\n  font-weight: 600;\n  letter-spacing: 1.3px;\n  color: #3e3e5f;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n}\n\n.bannerSection {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: -o-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: linear-gradient(258deg, #ffae71, #ff315e);\n  padding-top: 64px;\n  padding-top: 4rem;\n}\n\n.bannerSection .heading {\n    font-size: 48px;\n    font-size: 3rem;\n    font-weight: 300;\n    color: #fff;\n  }\n\n.bannerSection .description {\n    font-size: 24px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: normal;\n    letter-spacing: 1px;\n    color: #fff;\n    margin-top: 40px;\n    margin-top: 2.5rem;\n  }\n\n.teamSection {\n  padding-top: 48px;\n  padding-top: 3rem;\n  padding-bottom: 48px;\n  padding-bottom: 3rem;\n  background: #fff;\n}\n\n.tagLine {\n  font-size: 12px;\n  line-height: 21px;\n  color: #5f6368;\n}\n\n@media only screen and (min-width: 960px) {\n  .adviceMemberWrapper:nth-child(1) {\n    padding-left: 0 !important;\n  }\n\n  .adviceMemberWrapper:nth-child(2) {\n    padding-right: 0 !important;\n  }\n}\n\n.advisoryMemberCard {\n  margin-bottom: 36px;\n  margin-bottom: 2.25rem;\n  min-height: 200px;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n}\n\n.advisoryMemberCard img {\n    width: 200px;\n    height: 200px;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.advisoryMemberCard .info {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n    width: calc(100% - 200px);\n  }\n\n.advisoryMemberCard .name {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.advisoryMemberCard .tagLine {\n    font-size: 12px;\n    height: 24;\n    color: #5f6368;\n    margin-top: 32px;\n    margin-top: 2rem;\n  }\n\n@media only screen and (max-width: 959px) {\n    .teamSection .heading {\n      text-align: center !important;\n    }\n\n  .advisoryMemberCard {\n    min-height: 150px;\n  }\n\n    .advisoryMemberCard img {\n      width: 150px;\n      height: 150px;\n      -o-object-fit: cover;\n         object-fit: cover;\n      -o-object-position: 100% 0;\n         object-position: 100% 0;\n    }\n\n    .advisoryMemberCard .info {\n      padding: 1.5rem 1rem;\n      width: calc(100% - 150px);\n    }\n}\n\n.teamMemberCard {\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.teamMemberCard img {\n    height: 200px;\n    width: 200px;\n    border-radius: 50%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.teamMemberCard .info {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n  }\n\n.teamMemberCard .name {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.teamMemberCard .title {\n    margin-top: 8px;\n    margin-top: 0.5rem;\n    font-size: 20px;\n    color: #5f6368;\n  }\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"heading": "Team-heading-3CBSV",
	"bannerSection": "Team-bannerSection-a9i4l",
	"description": "Team-description-I_ug0",
	"teamSection": "Team-teamSection-1pCr7",
	"tagLine": "Team-tagLine-2Wznz",
	"adviceMemberWrapper": "Team-adviceMemberWrapper-2mqPb",
	"advisoryMemberCard": "Team-advisoryMemberCard-3sYFo",
	"info": "Team-info-3Y35F",
	"name": "Team-name-3STtX",
	"teamMemberCard": "Team-teamMemberCard-32YcH",
	"title": "Team-title-1fhFk"
};

/***/ }),

/***/ "./src/routes/team/Team.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Team_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/team/Team.scss");
/* harmony import */ var _Team_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Team_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/Team.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import cx from 'classnames';



 // import Link from 'components/Link';


const ADVISORY_BOARD = [{
  name: 'M.Y.S. Prasad',
  tagLine: 'Padmashri Awardee',
  detail1: 'Vice Chancellor, Vignan University',
  detail2: 'Former Director- SDSC, ISRO',
  image: '/team/Padma Shri M.Y.S.Prasad.jpg',
  url: 'https://en.wikipedia.org/wiki/M._Y._S._Prasad'
}, {
  name: 'Dr. T. Hanuman Chowdary',
  tagLine: 'Padmashri Awardee',
  detail1: 'Former IT Advisor, AP Govt.',
  detail2: 'Fellow, Tata Consulting Services (TCS)',
  image: '/team/Padma Shri Dr.T.Hanuman Chowdary.JPG',
  url: 'https://www.bloomberg.com/research/stocks/people/person.asp?personId=765564&privcapId=133103'
}];
const TEAMS = [{
  name: 'Advisory Board',
  members: [{
    name: 'M.Y.S. Prasad',
    title: 'Padmashri Awardee',
    detail1: 'Vice Chancellor, Vignan University',
    detail2: 'Former Director- SDSC, ISRO',
    image: '/team/M.Y.S. Prasad.png'
    /* linkedIn: 'https://en.wikipedia.org/wiki/M._Y._S._Prasad', */

  }, {
    name: 'Dr. T. Hanuman Chowdary',
    title: 'Padmashri Awardee',
    detail1: 'Former IT Advisor, AP Govt.',
    detail2: 'Fellow, Tata Consulting Services (TCS)',
    image: '/team/Padma Shri Dr.T.Hanuman Chowdary.png'
    /* linkedIn:
      'https://www.bloomberg.com/research/stocks/people/person.asp?personId=765564&privcapId=133103', */

  }]
}, {
  name: 'Leadership',
  members: [{
    name: 'Kiran Babu',
    title: 'Founder & CEO',
    image: '/team/Kiran-Babu.png',
    linkedIn: 'https://www.linkedin.com/in/yerranagu/'
  }, {
    name: 'Anjan Goswami',
    title: 'Product',
    image: '/team/Anjan-Goswami.png',
    linkedIn: 'https://www.linkedin.com/in/anjangoswami/'
  }, {
    name: 'Nikhil Sharma',
    title: 'Engineering',
    image: '/team/Nikhil-Sharma.png',
    linkedIn: 'https://www.linkedin.com/in/nykhylsharma/'
  }, {
    name: 'Shiva Krishna',
    title: 'Engineering',
    image: '/team/Shiva.jpg',
    linkedIn: 'https://www.linkedin.com/in/shivakrishnaah/'
  }, {
    name: 'ChandraSekhar',
    title: 'Operations',
    image: '/team/CHANDRA.jpg',
    linkedIn: 'https://www.linkedin.com/in/chandrasekkhar-m-v-b1762b13/'
  }]
}, {
  name: 'Team',
  members: [{
    name: 'LaxmanRao',
    image: '/team/LAXMAN.jpg',
    title: 'Sales'
  }, {
    name: 'Archana',
    image: '/team/Archana.jpg',
    title: 'Sales'
  }, {
    name: 'Vandana',
    image: '/team/Vandana.jpg',
    title: 'Sales'
  }, {
    name: 'Pranab Sarkar',
    title: 'Design',
    image: '/team/pranab.jpg'
    /* linkedIn: 'https://www.linkedin.com/in/pranab-sarkar-b5496699/', */

  }, {
    name: 'Srujan Sagar',
    title: 'Design',
    image: '/team/Srujan-Sagar.png'
    /* linkedIn: 'https://www.linkedin.com/in/srujansagar/', */

  }, {
    name: 'Srikant',
    image: '/team/Srikant 2.jpg',
    title: 'Design'
  }, {
    name: 'Sudhir Suguru',
    title: 'Talent Acquisition',
    image: '/team/SUDHIR.jpg'
    /* linkedIn: 'https://www.linkedin.com/in/sudhirsuguru/', */

  }, {
    name: 'Hari Krishna Salver',
    title: 'Engineering',
    image: '/team/Hari-Krishna-Salver.png'
    /* linkedIn: 'https://www.linkedin.com/in/harikrishnasalver/', */

  }, {
    name: 'Biswa Bhusan',
    image: '/team/biswa.jpg',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/biswa-bhusan-soren-a66583137/', */

  }, {
    name: 'Aslam Shaik',
    title: 'Engineering',
    image: '/team/Aslam-Shaik.png'
    /* linkedIn: 'https://www.linkedin.com/in/aslam-shaik-39a114135/', */

  }, {
    name: 'Vikram Nethi',
    image: '/team/vikram.png',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/vikram-nethi-bb5a88b5', */

  }, {
    name: 'Mohit Agarwal',
    image: '/team/Mohit.jpg',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/mohit-agarwal-03/', */

  }, {
    name: 'Srikanth',
    image: '/team/Srikant.jpg',
    title: 'Engineering' // linkedIn: 'https://angel.co/lonewolf3739',

  }, {
    name: 'Vikram Somavaram',
    image: '/team/VikramS.jpg',
    title: 'Engineering' // linkedIn: 'https://www.linkedin.com/in/vikram-somavaram-48a15165/',

  }, {
    name: 'Narasimha Maddi',
    title: 'Operations',
    image: '/team/Narasimha-Maddi.png'
  }]
}];

class Team extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "displayAdvisoryMembersSection", () => {
      const view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row hide",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, "Advisory Board"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, ADVISORY_BOARD.map(member => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: `col xs12 s6 ${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adviceMemberWrapper}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: `${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.advisoryMemberCard}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col no-padding",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: member.url,
        target: "_blank",
        rel: "noreferrer noopener",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 196
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: member.image ? `/images/${member.image}` : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: `col ${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.info}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.name,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, member.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, member.tagLine), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, " ", member.detail1), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, " ", member.detail2)))))));
      return view;
    });

    _defineProperty(this, "displayTeamsSection", () => {
      const view = TEAMS.map(team => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        style: {
          marginTop: '2rem'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 228
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 229
        },
        __self: this
      }, team.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }, team.members.map(member => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: member.name === 'M.Y.S. Prasad' || member.name === 'Dr. T. Hanuman Chowdary' ? `col xs12 s6 ${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamMemberCard}` : `col xs12 s4 ${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamMemberCard}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 232
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col xs12 l12 no-padding",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }, member.linkedIn ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: member.linkedIn,
        target: "_blank",
        rel: "noreferrer noopener",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: member.image ? `/images/${member.image}` : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 247
        },
        __self: this
      })) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: member.image ? `/images/${member.image}` : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: `col xs12 s12 ${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.info}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 267
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: `${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.name} hide-overflow`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268
        },
        __self: this
      }, member.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }, member.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      }, " ", member.detail1), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      }, " ", member.detail2)))))));
      return view;
    });
  }

  componentDidMount() {
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.initialize(window.App.googleTrackingId, {
      debug: false
    });
    react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.pageview(window.location.href);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 283
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bannerSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 284
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 285
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 286
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col xs12 l6 no-padding",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 287
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 288
      },
      __self: this
    }, "Meet the team behind this"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.description,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 289
      },
      __self: this
    }, `We believe work is exhilarating when you're surrounded by
                   extraordinary people who embrace being part of a team.`))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: `${_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamSection} container`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 297
      },
      __self: this
    }, this.displayAdvisoryMembersSection(), this.displayTeamsSection()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 301
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Team));

/***/ }),

/***/ "./src/routes/team/Team.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/team/Team.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/team/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Team__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/team/Team.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/index.js";




async function action() {
  return {
    title: 'Egnify',
    chunks: ['Team'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Team__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1RlYW0uanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3RlYW0vVGVhbS5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvdGVhbS9UZWFtLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvdGVhbS9UZWFtLnNjc3M/N2U5NSIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3RlYW0vaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5UZWFtLWhlYWRpbmctM0NCU1Yge1xcbiAgZm9udC1zaXplOiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxldHRlci1zcGFjaW5nOiAxLjNweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIG1hcmdpbi1ib3R0b206IDIuNXJlbTtcXG59XFxuXFxuLlRlYW0tYmFubmVyU2VjdGlvbi1hOWk0bCB7XFxuICBiYWNrZ3JvdW5kOiAjM2UzZTVmO1xcbiAgbWluLWhlaWdodDogMzY2cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgxOTJkZWcsICNmZmFlNzEsICNmZjMxNWUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDE5MmRlZywgI2ZmYWU3MSwgI2ZmMzE1ZSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjU4ZGVnLCAjZmZhZTcxLCAjZmYzMTVlKTtcXG4gIHBhZGRpbmctdG9wOiA2NHB4O1xcbiAgcGFkZGluZy10b3A6IDRyZW07XFxufVxcblxcbi5UZWFtLWJhbm5lclNlY3Rpb24tYTlpNGwgLlRlYW0taGVhZGluZy0zQ0JTViB7XFxuICAgIGZvbnQtc2l6ZTogNDhweDtcXG4gICAgZm9udC1zaXplOiAzcmVtO1xcbiAgICBmb250LXdlaWdodDogMzAwO1xcbiAgICBjb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4uVGVhbS1iYW5uZXJTZWN0aW9uLWE5aTRsIC5UZWFtLWRlc2NyaXB0aW9uLUlfdWcwIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XFxuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgICBjb2xvcjogI2ZmZjtcXG4gICAgbWFyZ2luLXRvcDogNDBweDtcXG4gICAgbWFyZ2luLXRvcDogMi41cmVtO1xcbiAgfVxcblxcbi5UZWFtLXRlYW1TZWN0aW9uLTFwQ3I3IHtcXG4gIHBhZGRpbmctdG9wOiA0OHB4O1xcbiAgcGFkZGluZy10b3A6IDNyZW07XFxuICBwYWRkaW5nLWJvdHRvbTogNDhweDtcXG4gIHBhZGRpbmctYm90dG9tOiAzcmVtO1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG59XFxuXFxuLlRlYW0tdGFnTGluZS0yV3pueiB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBsaW5lLWhlaWdodDogMjFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDk2MHB4KSB7XFxuICAuVGVhbS1hZHZpY2VNZW1iZXJXcmFwcGVyLTJtcVBiOm50aC1jaGlsZCgxKSB7XFxuICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xcbiAgfVxcblxcbiAgLlRlYW0tYWR2aWNlTWVtYmVyV3JhcHBlci0ybXFQYjpudGgtY2hpbGQoMikge1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XFxuICB9XFxufVxcblxcbi5UZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGbyB7XFxuICBtYXJnaW4tYm90dG9tOiAzNnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMi4yNXJlbTtcXG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG59XFxuXFxuLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIGltZyB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAtby1vYmplY3QtcG9zaXRpb246IDEwMCUgMDtcXG4gICAgICAgb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICB9XFxuXFxuLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIC5UZWFtLWluZm8tM1kzNUYge1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAycmVtO1xcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMjAwcHgpO1xcbiAgfVxcblxcbi5UZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGbyAuVGVhbS1uYW1lLTNTVHRYIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gIH1cXG5cXG4uVGVhbS1hZHZpc29yeU1lbWJlckNhcmQtM3NZRm8gLlRlYW0tdGFnTGluZS0yV3pueiB7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gICAgaGVpZ2h0OiAyNDtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICAgIG1hcmdpbi10b3A6IDJyZW07XFxuICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NTlweCkge1xcbiAgICAuVGVhbS10ZWFtU2VjdGlvbi0xcENyNyAuVGVhbS1oZWFkaW5nLTNDQlNWIHtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcXG4gICAgfVxcblxcbiAgLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIHtcXG4gICAgbWluLWhlaWdodDogMTUwcHg7XFxuICB9XFxuXFxuICAgIC5UZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGbyBpbWcge1xcbiAgICAgIHdpZHRoOiAxNTBweDtcXG4gICAgICBoZWlnaHQ6IDE1MHB4O1xcbiAgICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgIC1vLW9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICAgICAgIG9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICB9XFxuXFxuICAgIC5UZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGbyAuVGVhbS1pbmZvLTNZMzVGIHtcXG4gICAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgICB3aWR0aDogY2FsYygxMDAlIC0gMTUwcHgpO1xcbiAgICB9XFxufVxcblxcbi5UZWFtLXRlYW1NZW1iZXJDYXJkLTMyWWNIIHtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLlRlYW0tdGVhbU1lbWJlckNhcmQtMzJZY0ggaW1nIHtcXG4gICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgLW8tb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgICAgIG9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgfVxcblxcbi5UZWFtLXRlYW1NZW1iZXJDYXJkLTMyWWNIIC5UZWFtLWluZm8tM1kzNUYge1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAycmVtO1xcbiAgfVxcblxcbi5UZWFtLXRlYW1NZW1iZXJDYXJkLTMyWWNIIC5UZWFtLW5hbWUtM1NUdFgge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgfVxcblxcbi5UZWFtLXRlYW1NZW1iZXJDYXJkLTMyWWNIIC5UZWFtLXRpdGxlLTFmaEZrIHtcXG4gICAgbWFyZ2luLXRvcDogOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICB9XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3RlYW0vVGVhbS5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG9FQUFvRTtFQUNwRSwrREFBK0Q7RUFDL0QsNERBQTREO0VBQzVELGtCQUFrQjtFQUNsQixrQkFBa0I7Q0FDbkI7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixZQUFZO0dBQ2I7O0FBRUg7SUFDSSxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLG1CQUFtQjtHQUNwQjs7QUFFSDtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRTtJQUNFLDJCQUEyQjtHQUM1Qjs7RUFFRDtJQUNFLDRCQUE0QjtHQUM3QjtDQUNGOztBQUVEO0VBQ0Usb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixvREFBb0Q7VUFDNUMsNENBQTRDO0NBQ3JEOztBQUVEO0lBQ0ksYUFBYTtJQUNiLGNBQWM7SUFDZCxxQkFBcUI7T0FDbEIsa0JBQWtCO0lBQ3JCLDJCQUEyQjtPQUN4Qix3QkFBd0I7R0FDNUI7O0FBRUg7SUFDSSxtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLDBCQUEwQjtHQUMzQjs7QUFFSDtJQUNJLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZUFBZTtHQUNoQjs7QUFFSDtJQUNJLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixpQkFBaUI7R0FDbEI7O0FBRUg7SUFDSTtNQUNFLDhCQUE4QjtLQUMvQjs7RUFFSDtJQUNFLGtCQUFrQjtHQUNuQjs7SUFFQztNQUNFLGFBQWE7TUFDYixjQUFjO01BQ2QscUJBQXFCO1NBQ2xCLGtCQUFrQjtNQUNyQiwyQkFBMkI7U0FDeEIsd0JBQXdCO0tBQzVCOztJQUVEO01BQ0UscUJBQXFCO01BQ3JCLDBCQUEwQjtLQUMzQjtDQUNKOztBQUVEO0VBQ0Usb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtDQUNwQjs7QUFFRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHFCQUFxQjtPQUNsQixrQkFBa0I7SUFDckIsMkJBQTJCO09BQ3hCLHdCQUF3QjtHQUM1Qjs7QUFFSDtJQUNJLG1CQUFtQjtJQUNuQixxQkFBcUI7R0FDdEI7O0FBRUg7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGVBQWU7R0FDaEI7O0FBRUg7SUFDSSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixlQUFlO0dBQ2hCXCIsXCJmaWxlXCI6XCJUZWFtLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLmhlYWRpbmcge1xcbiAgZm9udC1zaXplOiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxldHRlci1zcGFjaW5nOiAxLjNweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIG1hcmdpbi1ib3R0b206IDIuNXJlbTtcXG59XFxuXFxuLmJhbm5lclNlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogIzNlM2U1ZjtcXG4gIG1pbi1oZWlnaHQ6IDM2NnB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoMTkyZGVnLCAjZmZhZTcxLCAjZmYzMTVlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudCgxOTJkZWcsICNmZmFlNzEsICNmZjMxNWUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDI1OGRlZywgI2ZmYWU3MSwgI2ZmMzE1ZSk7XFxuICBwYWRkaW5nLXRvcDogNjRweDtcXG4gIHBhZGRpbmctdG9wOiA0cmVtO1xcbn1cXG5cXG4uYmFubmVyU2VjdGlvbiAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogNDhweDtcXG4gICAgZm9udC1zaXplOiAzcmVtO1xcbiAgICBmb250LXdlaWdodDogMzAwO1xcbiAgICBjb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4uYmFubmVyU2VjdGlvbiAuZGVzY3JpcHRpb24ge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgICBtYXJnaW4tdG9wOiAyLjVyZW07XFxuICB9XFxuXFxuLnRlYW1TZWN0aW9uIHtcXG4gIHBhZGRpbmctdG9wOiA0OHB4O1xcbiAgcGFkZGluZy10b3A6IDNyZW07XFxuICBwYWRkaW5nLWJvdHRvbTogNDhweDtcXG4gIHBhZGRpbmctYm90dG9tOiAzcmVtO1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG59XFxuXFxuLnRhZ0xpbmUge1xcbiAgZm9udC1zaXplOiAxMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDIxcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5NjBweCkge1xcbiAgLmFkdmljZU1lbWJlcldyYXBwZXI6bnRoLWNoaWxkKDEpIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuICAuYWR2aWNlTWVtYmVyV3JhcHBlcjpudGgtY2hpbGQoMikge1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XFxuICB9XFxufVxcblxcbi5hZHZpc29yeU1lbWJlckNhcmQge1xcbiAgbWFyZ2luLWJvdHRvbTogMzZweDtcXG4gIG1hcmdpbi1ib3R0b206IDIuMjVyZW07XFxuICBtaW4taGVpZ2h0OiAyMDBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxufVxcblxcbi5hZHZpc29yeU1lbWJlckNhcmQgaW1nIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgIC1vLW9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICAgICBvYmplY3QtcG9zaXRpb246IDEwMCUgMDtcXG4gIH1cXG5cXG4uYWR2aXNvcnlNZW1iZXJDYXJkIC5pbmZvIHtcXG4gICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMnJlbTtcXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDIwMHB4KTtcXG4gIH1cXG5cXG4uYWR2aXNvcnlNZW1iZXJDYXJkIC5uYW1lIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gIH1cXG5cXG4uYWR2aXNvcnlNZW1iZXJDYXJkIC50YWdMaW5lIHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBoZWlnaHQ6IDI0O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLXRvcDogMzJweDtcXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcXG4gIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk1OXB4KSB7XFxuICAgIC50ZWFtU2VjdGlvbiAuaGVhZGluZyB7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XFxuICAgIH1cXG5cXG4gIC5hZHZpc29yeU1lbWJlckNhcmQge1xcbiAgICBtaW4taGVpZ2h0OiAxNTBweDtcXG4gIH1cXG5cXG4gICAgLmFkdmlzb3J5TWVtYmVyQ2FyZCBpbWcge1xcbiAgICAgIHdpZHRoOiAxNTBweDtcXG4gICAgICBoZWlnaHQ6IDE1MHB4O1xcbiAgICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgIC1vLW9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICAgICAgIG9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICB9XFxuXFxuICAgIC5hZHZpc29yeU1lbWJlckNhcmQgLmluZm8ge1xcbiAgICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAxNTBweCk7XFxuICAgIH1cXG59XFxuXFxuLnRlYW1NZW1iZXJDYXJkIHtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLnRlYW1NZW1iZXJDYXJkIGltZyB7XFxuICAgIGhlaWdodDogMjAwcHg7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgIC1vLW9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICAgICBvYmplY3QtcG9zaXRpb246IDEwMCUgMDtcXG4gIH1cXG5cXG4udGVhbU1lbWJlckNhcmQgLmluZm8ge1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAycmVtO1xcbiAgfVxcblxcbi50ZWFtTWVtYmVyQ2FyZCAubmFtZSB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICB9XFxuXFxuLnRlYW1NZW1iZXJDYXJkIC50aXRsZSB7XFxuICAgIG1hcmdpbi10b3A6IDhweDtcXG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgfVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJoZWFkaW5nXCI6IFwiVGVhbS1oZWFkaW5nLTNDQlNWXCIsXG5cdFwiYmFubmVyU2VjdGlvblwiOiBcIlRlYW0tYmFubmVyU2VjdGlvbi1hOWk0bFwiLFxuXHRcImRlc2NyaXB0aW9uXCI6IFwiVGVhbS1kZXNjcmlwdGlvbi1JX3VnMFwiLFxuXHRcInRlYW1TZWN0aW9uXCI6IFwiVGVhbS10ZWFtU2VjdGlvbi0xcENyN1wiLFxuXHRcInRhZ0xpbmVcIjogXCJUZWFtLXRhZ0xpbmUtMld6bnpcIixcblx0XCJhZHZpY2VNZW1iZXJXcmFwcGVyXCI6IFwiVGVhbS1hZHZpY2VNZW1iZXJXcmFwcGVyLTJtcVBiXCIsXG5cdFwiYWR2aXNvcnlNZW1iZXJDYXJkXCI6IFwiVGVhbS1hZHZpc29yeU1lbWJlckNhcmQtM3NZRm9cIixcblx0XCJpbmZvXCI6IFwiVGVhbS1pbmZvLTNZMzVGXCIsXG5cdFwibmFtZVwiOiBcIlRlYW0tbmFtZS0zU1R0WFwiLFxuXHRcInRlYW1NZW1iZXJDYXJkXCI6IFwiVGVhbS10ZWFtTWVtYmVyQ2FyZC0zMlljSFwiLFxuXHRcInRpdGxlXCI6IFwiVGVhbS10aXRsZS0xZmhGa1wiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG4vLyBpbXBvcnQgY3ggZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgUmVxdWVzdERlbW8gZnJvbSAnY29tcG9uZW50cy9SZXF1ZXN0RGVtbyc7XG4vLyBpbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IHMgZnJvbSAnLi9UZWFtLnNjc3MnO1xuXG5jb25zdCBBRFZJU09SWV9CT0FSRCA9IFtcbiAge1xuICAgIG5hbWU6ICdNLlkuUy4gUHJhc2FkJyxcbiAgICB0YWdMaW5lOiAnUGFkbWFzaHJpIEF3YXJkZWUnLFxuICAgIGRldGFpbDE6ICdWaWNlIENoYW5jZWxsb3IsIFZpZ25hbiBVbml2ZXJzaXR5JyxcbiAgICBkZXRhaWwyOiAnRm9ybWVyIERpcmVjdG9yLSBTRFNDLCBJU1JPJyxcbiAgICBpbWFnZTogJy90ZWFtL1BhZG1hIFNocmkgTS5ZLlMuUHJhc2FkLmpwZycsXG4gICAgdXJsOiAnaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTS5fWS5fUy5fUHJhc2FkJyxcbiAgfSxcbiAge1xuICAgIG5hbWU6ICdEci4gVC4gSGFudW1hbiBDaG93ZGFyeScsXG4gICAgdGFnTGluZTogJ1BhZG1hc2hyaSBBd2FyZGVlJyxcbiAgICBkZXRhaWwxOiAnRm9ybWVyIElUIEFkdmlzb3IsIEFQIEdvdnQuJyxcbiAgICBkZXRhaWwyOiAnRmVsbG93LCBUYXRhIENvbnN1bHRpbmcgU2VydmljZXMgKFRDUyknLFxuICAgIGltYWdlOiAnL3RlYW0vUGFkbWEgU2hyaSBEci5ULkhhbnVtYW4gQ2hvd2RhcnkuSlBHJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly93d3cuYmxvb21iZXJnLmNvbS9yZXNlYXJjaC9zdG9ja3MvcGVvcGxlL3BlcnNvbi5hc3A/cGVyc29uSWQ9NzY1NTY0JnByaXZjYXBJZD0xMzMxMDMnLFxuICB9LFxuXTtcblxuY29uc3QgVEVBTVMgPSBbXG4gIHtcbiAgICBuYW1lOiAnQWR2aXNvcnkgQm9hcmQnLFxuICAgIG1lbWJlcnM6IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ00uWS5TLiBQcmFzYWQnLFxuICAgICAgICB0aXRsZTogJ1BhZG1hc2hyaSBBd2FyZGVlJyxcbiAgICAgICAgZGV0YWlsMTogJ1ZpY2UgQ2hhbmNlbGxvciwgVmlnbmFuIFVuaXZlcnNpdHknLFxuICAgICAgICBkZXRhaWwyOiAnRm9ybWVyIERpcmVjdG9yLSBTRFNDLCBJU1JPJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9NLlkuUy4gUHJhc2FkLnBuZycsXG4gICAgICAgIC8qIGxpbmtlZEluOiAnaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTS5fWS5fUy5fUHJhc2FkJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdEci4gVC4gSGFudW1hbiBDaG93ZGFyeScsXG4gICAgICAgIHRpdGxlOiAnUGFkbWFzaHJpIEF3YXJkZWUnLFxuICAgICAgICBkZXRhaWwxOiAnRm9ybWVyIElUIEFkdmlzb3IsIEFQIEdvdnQuJyxcbiAgICAgICAgZGV0YWlsMjogJ0ZlbGxvdywgVGF0YSBDb25zdWx0aW5nIFNlcnZpY2VzIChUQ1MpJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9QYWRtYSBTaHJpIERyLlQuSGFudW1hbiBDaG93ZGFyeS5wbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjpcbiAgICAgICAgICAnaHR0cHM6Ly93d3cuYmxvb21iZXJnLmNvbS9yZXNlYXJjaC9zdG9ja3MvcGVvcGxlL3BlcnNvbi5hc3A/cGVyc29uSWQ9NzY1NTY0JnByaXZjYXBJZD0xMzMxMDMnLCAqL1xuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ0xlYWRlcnNoaXAnLFxuICAgIG1lbWJlcnM6IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0tpcmFuIEJhYnUnLFxuICAgICAgICB0aXRsZTogJ0ZvdW5kZXIgJiBDRU8nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL0tpcmFuLUJhYnUucG5nJyxcbiAgICAgICAgbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4veWVycmFuYWd1LycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQW5qYW4gR29zd2FtaScsXG4gICAgICAgIHRpdGxlOiAnUHJvZHVjdCcsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vQW5qYW4tR29zd2FtaS5wbmcnLFxuICAgICAgICBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9hbmphbmdvc3dhbWkvJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdOaWtoaWwgU2hhcm1hJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vTmlraGlsLVNoYXJtYS5wbmcnLFxuICAgICAgICBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9ueWtoeWxzaGFybWEvJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdTaGl2YSBLcmlzaG5hJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vU2hpdmEuanBnJyxcbiAgICAgICAgbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vc2hpdmFrcmlzaG5hYWgvJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdDaGFuZHJhU2VraGFyJyxcbiAgICAgICAgdGl0bGU6ICdPcGVyYXRpb25zJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9DSEFORFJBLmpwZycsXG4gICAgICAgIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL2NoYW5kcmFzZWtraGFyLW0tdi1iMTc2MmIxMy8nLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ1RlYW0nLFxuICAgIG1lbWJlcnM6IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0xheG1hblJhbycsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vTEFYTUFOLmpwZycsXG4gICAgICAgIHRpdGxlOiAnU2FsZXMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0FyY2hhbmEnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL0FyY2hhbmEuanBnJyxcbiAgICAgICAgdGl0bGU6ICdTYWxlcycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnVmFuZGFuYScsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vVmFuZGFuYS5qcGcnLFxuICAgICAgICB0aXRsZTogJ1NhbGVzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdQcmFuYWIgU2Fya2FyJyxcbiAgICAgICAgdGl0bGU6ICdEZXNpZ24nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL3ByYW5hYi5qcGcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9wcmFuYWItc2Fya2FyLWI1NDk2Njk5LycsICovXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU3J1amFuIFNhZ2FyJyxcbiAgICAgICAgdGl0bGU6ICdEZXNpZ24nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1NydWphbi1TYWdhci5wbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9zcnVqYW5zYWdhci8nLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1NyaWthbnQnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1NyaWthbnQgMi5qcGcnLFxuICAgICAgICB0aXRsZTogJ0Rlc2lnbicsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU3VkaGlyIFN1Z3VydScsXG4gICAgICAgIHRpdGxlOiAnVGFsZW50IEFjcXVpc2l0aW9uJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9TVURISVIuanBnJyxcbiAgICAgICAgLyogbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vc3VkaGlyc3VndXJ1LycsICovXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnSGFyaSBLcmlzaG5hIFNhbHZlcicsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL0hhcmktS3Jpc2huYS1TYWx2ZXIucG5nJyxcbiAgICAgICAgLyogbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vaGFyaWtyaXNobmFzYWx2ZXIvJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdCaXN3YSBCaHVzYW4nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL2Jpc3dhLmpwZycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9iaXN3YS1iaHVzYW4tc29yZW4tYTY2NTgzMTM3LycsICovXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQXNsYW0gU2hhaWsnLFxuICAgICAgICB0aXRsZTogJ0VuZ2luZWVyaW5nJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9Bc2xhbS1TaGFpay5wbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9hc2xhbS1zaGFpay0zOWExMTQxMzUvJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdWaWtyYW0gTmV0aGknLFxuICAgICAgICBpbWFnZTogJy90ZWFtL3Zpa3JhbS5wbmcnLFxuICAgICAgICB0aXRsZTogJ0VuZ2luZWVyaW5nJyxcbiAgICAgICAgLyogbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vdmlrcmFtLW5ldGhpLWJiNWE4OGI1JywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdNb2hpdCBBZ2Fyd2FsJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9Nb2hpdC5qcGcnLFxuICAgICAgICB0aXRsZTogJ0VuZ2luZWVyaW5nJyxcbiAgICAgICAgLyogbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vbW9oaXQtYWdhcndhbC0wMy8nLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1NyaWthbnRoJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9TcmlrYW50LmpwZycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICAvLyBsaW5rZWRJbjogJ2h0dHBzOi8vYW5nZWwuY28vbG9uZXdvbGYzNzM5JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdWaWtyYW0gU29tYXZhcmFtJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9WaWtyYW1TLmpwZycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICAvLyBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi92aWtyYW0tc29tYXZhcmFtLTQ4YTE1MTY1LycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnTmFyYXNpbWhhIE1hZGRpJyxcbiAgICAgICAgdGl0bGU6ICdPcGVyYXRpb25zJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9OYXJhc2ltaGEtTWFkZGkucG5nJyxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbl07XG5cbmNsYXNzIFRlYW0gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBSZWFjdEdBLmluaXRpYWxpemUod2luZG93LkFwcC5nb29nbGVUcmFja2luZ0lkLCB7XG4gICAgICBkZWJ1ZzogZmFsc2UsXG4gICAgfSk7XG4gICAgUmVhY3RHQS5wYWdldmlldyh3aW5kb3cubG9jYXRpb24uaHJlZik7XG4gIH1cblxuICBkaXNwbGF5QWR2aXNvcnlNZW1iZXJzU2VjdGlvbiA9ICgpID0+IHtcbiAgICBjb25zdCB2aWV3ID0gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgaGlkZVwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nfT5BZHZpc29yeSBCb2FyZDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgIHtBRFZJU09SWV9CT0FSRC5tYXAobWVtYmVyID0+IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgY29sIHhzMTIgczYgJHtzLmFkdmljZU1lbWJlcldyYXBwZXJ9YH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFkdmlzb3J5TWVtYmVyQ2FyZH1gfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbCBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICBocmVmPXttZW1iZXIudXJsfVxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgICAgICBtZW1iZXIuaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPyBgL2ltYWdlcy8ke21lbWJlci5pbWFnZX1gXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogJ2h0dHBzOi8veDEueGluZ2Fzc2V0cy5jb20vYXNzZXRzL2Zyb250ZW5kX21pbmlmaWVkL2ltZy91c2Vycy9ub2JvZHlfbS5vcmlnaW5hbC5qcGcnXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Bjb2wgJHtzLmluZm99YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5uYW1lfT57bWVtYmVyLm5hbWV9PC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWdMaW5lfT57bWVtYmVyLnRhZ0xpbmV9PC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWdMaW5lfT4ge21lbWJlci5kZXRhaWwxfTwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFnTGluZX0+IHttZW1iZXIuZGV0YWlsMn08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICAgIHJldHVybiB2aWV3O1xuICB9O1xuXG4gIGRpc3BsYXlUZWFtc1NlY3Rpb24gPSAoKSA9PiB7XG4gICAgY29uc3QgdmlldyA9IFRFQU1TLm1hcCh0ZWFtID0+IChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCIgc3R5bGU9e3sgbWFyZ2luVG9wOiAnMnJlbScgfX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmd9Pnt0ZWFtLm5hbWV9PC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAge3RlYW0ubWVtYmVycy5tYXAobWVtYmVyID0+IChcbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICBtZW1iZXIubmFtZSA9PT0gJ00uWS5TLiBQcmFzYWQnIHx8XG4gICAgICAgICAgICAgICAgbWVtYmVyLm5hbWUgPT09ICdEci4gVC4gSGFudW1hbiBDaG93ZGFyeSdcbiAgICAgICAgICAgICAgICAgID8gYGNvbCB4czEyIHM2ICR7cy50ZWFtTWVtYmVyQ2FyZH1gXG4gICAgICAgICAgICAgICAgICA6IGBjb2wgeHMxMiBzNCAke3MudGVhbU1lbWJlckNhcmR9YFxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sIHhzMTIgbDEyIG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICB7bWVtYmVyLmxpbmtlZEluID8gKFxuICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgaHJlZj17bWVtYmVyLmxpbmtlZElufVxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgICAgICBtZW1iZXIuaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPyBgL2ltYWdlcy8ke21lbWJlci5pbWFnZX1gXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogJ2h0dHBzOi8veDEueGluZ2Fzc2V0cy5jb20vYXNzZXRzL2Zyb250ZW5kX21pbmlmaWVkL2ltZy91c2Vycy9ub2JvZHlfbS5vcmlnaW5hbC5qcGcnXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgICBtZW1iZXIuaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgID8gYC9pbWFnZXMvJHttZW1iZXIuaW1hZ2V9YFxuICAgICAgICAgICAgICAgICAgICAgICAgOiAnaHR0cHM6Ly94MS54aW5nYXNzZXRzLmNvbS9hc3NldHMvZnJvbnRlbmRfbWluaWZpZWQvaW1nL3VzZXJzL25vYm9keV9tLm9yaWdpbmFsLmpwZydcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Bjb2wgeHMxMiBzMTIgJHtzLmluZm99YH0+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MubmFtZX0gaGlkZS1vdmVyZmxvd2B9PnttZW1iZXIubmFtZX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50aXRsZX0+e21lbWJlci50aXRsZX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50aXRsZX0+IHttZW1iZXIuZGV0YWlsMX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50aXRsZX0+IHttZW1iZXIuZGV0YWlsMn08L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApKTtcbiAgICByZXR1cm4gdmlldztcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT17cy5iYW5uZXJTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wgeHMxMiBsNiBuby1wYWRkaW5nXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ30+TWVldCB0aGUgdGVhbSBiZWhpbmQgdGhpczwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRlc2NyaXB0aW9ufT5cbiAgICAgICAgICAgICAgICAgIHtgV2UgYmVsaWV2ZSB3b3JrIGlzIGV4aGlsYXJhdGluZyB3aGVuIHlvdSdyZSBzdXJyb3VuZGVkIGJ5XG4gICAgICAgICAgICAgICAgICAgZXh0cmFvcmRpbmFyeSBwZW9wbGUgd2hvIGVtYnJhY2UgYmVpbmcgcGFydCBvZiBhIHRlYW0uYH1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9e2Ake3MudGVhbVNlY3Rpb259IGNvbnRhaW5lcmB9PlxuICAgICAgICAgIHt0aGlzLmRpc3BsYXlBZHZpc29yeU1lbWJlcnNTZWN0aW9uKCl9XG4gICAgICAgICAge3RoaXMuZGlzcGxheVRlYW1zU2VjdGlvbigpfVxuICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgIDxSZXF1ZXN0RGVtbyAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFRlYW0pO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZWFtLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vVGVhbS5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vVGVhbS5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgVGVhbSBmcm9tICcuL1RlYW0nO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0VnbmlmeScsXG4gICAgY2h1bmtzOiBbJ1RlYW0nXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxUZWFtIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFXQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFOQTtBQVhBO0FBdUJBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQTNCQTtBQW9DQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBbkZBO0FBQ0E7QUEyRkE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQVNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUE5Q0E7QUFnREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBS0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQXBHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUErRkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBOUhBO0FBQ0E7QUErSEE7Ozs7Ozs7QUNsVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVNBO0FBQ0E7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9