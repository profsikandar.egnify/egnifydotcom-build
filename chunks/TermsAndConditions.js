require("source-map-support").install();
exports.ids = ["TermsAndConditions"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".PrivacyAndTerms-bannerSection-1n7lh {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.PrivacyAndTerms-heading-d0iWW {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.PrivacyAndTerms-introContent-5tVQj {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.PrivacyAndTerms-introContent-5tVQj div {\n    padding-bottom: 40px;\n  }\n\n.PrivacyAndTerms-commit-2giFT {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.PrivacyAndTerms-commitHeading-C1Xyr {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1eV_7 {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1eV_7 li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .PrivacyAndTerms-heading-d0iWW {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .PrivacyAndTerms-heading-d0iWW {\n    font-size: 35px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.scss"],"names":[],"mappings":"AAAA;EACE,oBAAoB;EACpB,kBAAkB;EAClB,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,kBAAkB;EAClB,mBAAmB;EACnB,kBAAkB;CACnB;;AAED;IACI,qBAAqB;GACtB;;AAEH;EACE,kBAAkB;EAClB,2CAA2C;CAC5C;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,qBAAqB;CACtB;;AAED;EACE,kBAAkB;EAClB,mBAAmB;EACnB,qBAAqB;CACtB;;AAED;IACI,qBAAqB;IACrB,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,iCAAiC;GAClC;;AAEH;EACE;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,gBAAgB;GACjB;CACF","file":"PrivacyAndTerms.scss","sourcesContent":[".bannerSection {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.heading {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.introContent {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.introContent div {\n    padding-bottom: 40px;\n  }\n\n.commit {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.commitHeading {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.commitContent {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.commitContent li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .heading {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .heading {\n    font-size: 35px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bannerSection": "PrivacyAndTerms-bannerSection-1n7lh",
	"heading": "PrivacyAndTerms-heading-d0iWW",
	"introContent": "PrivacyAndTerms-introContent-5tVQj",
	"commit": "PrivacyAndTerms-commit-2giFT",
	"commitHeading": "PrivacyAndTerms-commitHeading-C1Xyr",
	"commitContent": "PrivacyAndTerms-commitContent-1eV_7"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.scss");
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const IntroContent = [{
  content: 'This document sets out the terms for use of the platform. It governs both our Teachers, Institute Owners and our consumers (“Students”). When we refer to “You”, we mean both our Tutors and Students or just our Tutors or our Students.'
}, {
  content: 'Our Terms of Service  (“Terms” or “Agreement”) also includes our Privacy Policy, and any other documents referred to by those agreements, and they, as a whole, govern any and all of the access to the platform, whether you are a visitor, guest, Tutor, Student, etc.'
}, {
  content: 'Our platform includes all of the subdomains, mobile applications, any other media, location, application, etc. owned, managed, operated, etc. by us. When referring to the infrastructure in this Agreement, we will be collectively referring to them as the “Platform”.'
}];
const SubContent = [{
  title: 'License to Teachers, Institute Owners & Students',
  content: [{
    text: 'We grant you a limited, non-exclusive license to access and use our platform for your own personal and commercial purposes. This license is only for your use and may not be assigned or sublicensed to anyone else, without the platform’s express written consent. Except as expressly permitted by the platform in writing, you will not try to reproduce the platform (legally that’s known as engaging in activity that would reproduce, redistribute, sell, create derivative works from, decompile, reverse engineer, or disassemble the Platform). You also agree that in exchange for this license You will not engage in any activity that would interfere with or damage or harm the Platform. All rights not expressly granted by the platform are reserved.'
  }]
}, {
  title: 'Code of Conduct',
  content: [{
    text: 'We have to set up some ground rules. Honestly, most of what we cover here should go without saying. However, it’s better to be upfront about things, rather than just assume everyone knows how they are expected to conduct themselves on our Platform.'
  }, {
    text: 'No Illegal Activity: This is about as simple as it gets. Do not use the Platform for any illegal activity. Period. You are not allowed to use our Platform to engage in any kind of conduct that violates any applicable state, local, or international law or regulation (including, without limitation, any laws regarding the export of data or software from the platform to India or any other countries)'
  }, {
    text: 'No Fraud: Yes, this is probably covered in the No Illegal Activity section above, but we want to make this very clear. Fraud will not be tolerated in any capacity'
  }, {
    text: 'No Exploitation: You will not use the Platform to try to gather personal information on anyone, outside of the limited permissible uses for offering your Content'
  }, {
    text: 'No Use Other Than Intended: You may not use the Platform or any content contained on the Platform for any purposes other than intended.'
  }, {
    text: 'If You violate this Code of Conduct we reserve the right to remove You and any of Your details from the Platform. Whether conduct violates our Code of Conduct will be determined at our discretion.'
  }]
}, {
  title: 'Teaching',
  content: [{
    text: 'The platform may provide the opportunity for Teachers to sell offerings (“Coaching”) to Students in addition to the courses that the Creator sells on the Platform (together Coaching and courses are referred to as Content). All Coaching shall be covered by this Agreement and are included in the definition of Content. Should the Coaching involve any third party content, You agree that the platform is not responsible in any way for such content, and makes no representations or guarantees as to its merchantability or fitness of use. The delivery of the Coaching is the sole responsibility of the Creator.'
  }]
}, {
  title: 'General Rights In Operating the Platform',
  content: [{
    text: 'The platform reserves the following rights over the entire platform:'
  }, {
    text: 'The platform may, but has no obligation to, monitor any content that appears on the platform or review any conduct occurring through the Platform, including any interactions between Tutors and Students.'
  }, {
    text: 'The platform has, in its sole discretion, an absolute right to modify, change, alter, suspend, or terminate any provision of this Agreement without warning at any time'
  }, {
    text: 'The platform does not guarantee that the Platform or any services offered through the Platform will be error-free or otherwise reliable, nor does the platform guarantee that defects will be corrected or that any offerings through the Platform will always be accessible. The platform may make improvements and/or changes to the Platform and their features and functionality at any time, and will use commercially reasonable efforts to avoid disrupting peak hours, though some downtime may occur. Errors in Content are the responsibility of the Creator who owns the Content.'
  }, {
    text: 'We reserve the right to amend the Platform, and any service or material we provide on the Platform, in our sole discretion without notice. We will not be liable if for any reason all or any part of the Platform is unavailable at any time or for any period. From time to time, we may restrict access to some or all of the Platform to Tutors and Students.'
  }]
}];

class PrivacyAndTerms extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "displayHeader", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.bannerSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, "Egnify Terms of Service")));

    _defineProperty(this, "displayMainContext", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.introContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, IntroContent.map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, item.content))));

    _defineProperty(this, "displaySubContext", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }, SubContent.map(sub => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.commit,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 113
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.commitHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114
      },
      __self: this
    }, sub.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a.commitContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, sub.content.map(subContent => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, subContent.text)))))));
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, this.displayHeader(), this.displayMainContext(), this.displaySubContext());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_2___default.a)(PrivacyAndTerms));

/***/ }),

/***/ "./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/privacyAndTerms/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/privacyAndTerms/PrivacyAndTerms.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/privacyAndTerms/index.js";




async function action() {
  return {
    title: ['GetRanks by Egnify: Assessment & Analytics Platform'],
    chunks: ['TermsAndConditions'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1Rlcm1zQW5kQ29uZGl0aW9ucy5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaXZhY3lBbmRUZXJtcy9Qcml2YWN5QW5kVGVybXMuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcml2YWN5QW5kVGVybXMvUHJpdmFjeUFuZFRlcm1zLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaXZhY3lBbmRUZXJtcy9Qcml2YWN5QW5kVGVybXMuc2Nzcz81M2M5IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaXZhY3lBbmRUZXJtcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlByaXZhY3lBbmRUZXJtcy1iYW5uZXJTZWN0aW9uLTFuN2xoIHtcXG4gIGJhY2tncm91bmQ6ICMzZTNlNWY7XFxuICBtaW4taGVpZ2h0OiAzNjZweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDE2NmRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMTY2ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyODRkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctZDBpV1cge1xcbiAgaGVpZ2h0OiA1OHB4O1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWludHJvQ29udGVudC01dFZRaiB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWludHJvQ29udGVudC01dFZRaiBkaXYge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWNvbW1pdC0yZ2lGVCB7XFxuICBwYWRkaW5nLXRvcDogNTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjE2LCAyMTYsIDIxNiwgMC4yKTtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1jb21taXRIZWFkaW5nLUMxWHlyIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBsaW5lLWhlaWdodDogMS4yNTtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxufVxcblxcbi5Qcml2YWN5QW5kVGVybXMtY29tbWl0Q29udGVudC0xZVZfNyB7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWNvbW1pdENvbnRlbnQtMWVWXzcgbGkge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbGlzdC1zdHlsZS10eXBlOiBkaXNjICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLlByaXZhY3lBbmRUZXJtcy1oZWFkaW5nLWQwaVdXIHtcXG4gICAgZm9udC1zaXplOiA0MHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQwMHB4KSB7XFxuICAuUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctZDBpV1cge1xcbiAgICBmb250LXNpemU6IDM1cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJpdmFjeUFuZFRlcm1zL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixvRUFBb0U7RUFDcEUsK0RBQStEO0VBQy9ELDREQUE0RDtFQUM1RCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0NBQ25COztBQUVEO0lBQ0kscUJBQXFCO0dBQ3RCOztBQUVIO0VBQ0Usa0JBQWtCO0VBQ2xCLDJDQUEyQztDQUM1Qzs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixxQkFBcUI7Q0FDdEI7O0FBRUQ7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsaUNBQWlDO0dBQ2xDOztBQUVIO0VBQ0U7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsZ0JBQWdCO0dBQ2pCO0NBQ0ZcIixcImZpbGVcIjpcIlByaXZhY3lBbmRUZXJtcy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5iYW5uZXJTZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICMzZTNlNWY7XFxuICBtaW4taGVpZ2h0OiAzNjZweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDE2NmRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMTY2ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyODRkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uaGVhZGluZyB7XFxuICBoZWlnaHQ6IDU4cHg7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5pbnRyb0NvbnRlbnQge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICBwYWRkaW5nLXRvcDogNDBweDtcXG59XFxuXFxuLmludHJvQ29udGVudCBkaXYge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4uY29tbWl0IHtcXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMTYsIDIxNiwgMjE2LCAwLjIpO1xcbn1cXG5cXG4uY29tbWl0SGVhZGluZyB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uY29tbWl0Q29udGVudCB7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uY29tbWl0Q29udGVudCBsaSB7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBsaXN0LXN0eWxlLXR5cGU6IGRpc2MgIWltcG9ydGFudDtcXG4gIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XFxuICAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MDBweCkge1xcbiAgLmhlYWRpbmcge1xcbiAgICBmb250LXNpemU6IDM1cHg7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJiYW5uZXJTZWN0aW9uXCI6IFwiUHJpdmFjeUFuZFRlcm1zLWJhbm5lclNlY3Rpb24tMW43bGhcIixcblx0XCJoZWFkaW5nXCI6IFwiUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctZDBpV1dcIixcblx0XCJpbnRyb0NvbnRlbnRcIjogXCJQcml2YWN5QW5kVGVybXMtaW50cm9Db250ZW50LTV0VlFqXCIsXG5cdFwiY29tbWl0XCI6IFwiUHJpdmFjeUFuZFRlcm1zLWNvbW1pdC0yZ2lGVFwiLFxuXHRcImNvbW1pdEhlYWRpbmdcIjogXCJQcml2YWN5QW5kVGVybXMtY29tbWl0SGVhZGluZy1DMVh5clwiLFxuXHRcImNvbW1pdENvbnRlbnRcIjogXCJQcml2YWN5QW5kVGVybXMtY29tbWl0Q29udGVudC0xZVZfN1wiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgcyBmcm9tICcuL1ByaXZhY3lBbmRUZXJtcy5zY3NzJztcblxuY29uc3QgSW50cm9Db250ZW50ID0gW1xuICB7XG4gICAgY29udGVudDpcbiAgICAgICdUaGlzIGRvY3VtZW50IHNldHMgb3V0IHRoZSB0ZXJtcyBmb3IgdXNlIG9mIHRoZSBwbGF0Zm9ybS4gSXQgZ292ZXJucyBib3RoIG91ciBUZWFjaGVycywgSW5zdGl0dXRlIE93bmVycyBhbmQgb3VyIGNvbnN1bWVycyAo4oCcU3R1ZGVudHPigJ0pLiBXaGVuIHdlIHJlZmVyIHRvIOKAnFlvdeKAnSwgd2UgbWVhbiBib3RoIG91ciBUdXRvcnMgYW5kIFN0dWRlbnRzIG9yIGp1c3Qgb3VyIFR1dG9ycyBvciBvdXIgU3R1ZGVudHMuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnT3VyIFRlcm1zIG9mIFNlcnZpY2UgICjigJxUZXJtc+KAnSBvciDigJxBZ3JlZW1lbnTigJ0pIGFsc28gaW5jbHVkZXMgb3VyIFByaXZhY3kgUG9saWN5LCBhbmQgYW55IG90aGVyIGRvY3VtZW50cyByZWZlcnJlZCB0byBieSB0aG9zZSBhZ3JlZW1lbnRzLCBhbmQgdGhleSwgYXMgYSB3aG9sZSwgZ292ZXJuIGFueSBhbmQgYWxsIG9mIHRoZSBhY2Nlc3MgdG8gdGhlIHBsYXRmb3JtLCB3aGV0aGVyIHlvdSBhcmUgYSB2aXNpdG9yLCBndWVzdCwgVHV0b3IsIFN0dWRlbnQsIGV0Yy4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdPdXIgcGxhdGZvcm0gaW5jbHVkZXMgYWxsIG9mIHRoZSBzdWJkb21haW5zLCBtb2JpbGUgYXBwbGljYXRpb25zLCBhbnkgb3RoZXIgbWVkaWEsIGxvY2F0aW9uLCBhcHBsaWNhdGlvbiwgZXRjLiBvd25lZCwgbWFuYWdlZCwgb3BlcmF0ZWQsIGV0Yy4gYnkgdXMuIFdoZW4gcmVmZXJyaW5nIHRvIHRoZSBpbmZyYXN0cnVjdHVyZSBpbiB0aGlzIEFncmVlbWVudCwgd2Ugd2lsbCBiZSBjb2xsZWN0aXZlbHkgcmVmZXJyaW5nIHRvIHRoZW0gYXMgdGhlIOKAnFBsYXRmb3Jt4oCdLicsXG4gIH0sXG5dO1xuXG5jb25zdCBTdWJDb250ZW50ID0gW1xuICB7XG4gICAgdGl0bGU6ICdMaWNlbnNlIHRvIFRlYWNoZXJzLCBJbnN0aXR1dGUgT3duZXJzICYgU3R1ZGVudHMnLFxuICAgIGNvbnRlbnQ6IFtcbiAgICAgIHtcbiAgICAgICAgdGV4dDpcbiAgICAgICAgICAnV2UgZ3JhbnQgeW91IGEgbGltaXRlZCwgbm9uLWV4Y2x1c2l2ZSBsaWNlbnNlIHRvIGFjY2VzcyBhbmQgdXNlIG91ciBwbGF0Zm9ybSBmb3IgeW91ciBvd24gcGVyc29uYWwgYW5kIGNvbW1lcmNpYWwgcHVycG9zZXMuIFRoaXMgbGljZW5zZSBpcyBvbmx5IGZvciB5b3VyIHVzZSBhbmQgbWF5IG5vdCBiZSBhc3NpZ25lZCBvciBzdWJsaWNlbnNlZCB0byBhbnlvbmUgZWxzZSwgd2l0aG91dCB0aGUgcGxhdGZvcm3igJlzIGV4cHJlc3Mgd3JpdHRlbiBjb25zZW50LiBFeGNlcHQgYXMgZXhwcmVzc2x5IHBlcm1pdHRlZCBieSB0aGUgcGxhdGZvcm0gaW4gd3JpdGluZywgeW91IHdpbGwgbm90IHRyeSB0byByZXByb2R1Y2UgdGhlIHBsYXRmb3JtIChsZWdhbGx5IHRoYXTigJlzIGtub3duIGFzIGVuZ2FnaW5nIGluIGFjdGl2aXR5IHRoYXQgd291bGQgcmVwcm9kdWNlLCByZWRpc3RyaWJ1dGUsIHNlbGwsIGNyZWF0ZSBkZXJpdmF0aXZlIHdvcmtzIGZyb20sIGRlY29tcGlsZSwgcmV2ZXJzZSBlbmdpbmVlciwgb3IgZGlzYXNzZW1ibGUgdGhlIFBsYXRmb3JtKS4gWW91IGFsc28gYWdyZWUgdGhhdCBpbiBleGNoYW5nZSBmb3IgdGhpcyBsaWNlbnNlIFlvdSB3aWxsIG5vdCBlbmdhZ2UgaW4gYW55IGFjdGl2aXR5IHRoYXQgd291bGQgaW50ZXJmZXJlIHdpdGggb3IgZGFtYWdlIG9yIGhhcm0gdGhlIFBsYXRmb3JtLiBBbGwgcmlnaHRzIG5vdCBleHByZXNzbHkgZ3JhbnRlZCBieSB0aGUgcGxhdGZvcm0gYXJlIHJlc2VydmVkLicsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0NvZGUgb2YgQ29uZHVjdCcsXG4gICAgY29udGVudDogW1xuICAgICAge1xuICAgICAgICB0ZXh0OlxuICAgICAgICAgICdXZSBoYXZlIHRvIHNldCB1cCBzb21lIGdyb3VuZCBydWxlcy4gSG9uZXN0bHksIG1vc3Qgb2Ygd2hhdCB3ZSBjb3ZlciBoZXJlIHNob3VsZCBnbyB3aXRob3V0IHNheWluZy4gSG93ZXZlciwgaXTigJlzIGJldHRlciB0byBiZSB1cGZyb250IGFib3V0IHRoaW5ncywgcmF0aGVyIHRoYW4ganVzdCBhc3N1bWUgZXZlcnlvbmUga25vd3MgaG93IHRoZXkgYXJlIGV4cGVjdGVkIHRvIGNvbmR1Y3QgdGhlbXNlbHZlcyBvbiBvdXIgUGxhdGZvcm0uJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHRleHQ6XG4gICAgICAgICAgJ05vIElsbGVnYWwgQWN0aXZpdHk6IFRoaXMgaXMgYWJvdXQgYXMgc2ltcGxlIGFzIGl0IGdldHMuIERvIG5vdCB1c2UgdGhlIFBsYXRmb3JtIGZvciBhbnkgaWxsZWdhbCBhY3Rpdml0eS4gUGVyaW9kLiBZb3UgYXJlIG5vdCBhbGxvd2VkIHRvIHVzZSBvdXIgUGxhdGZvcm0gdG8gZW5nYWdlIGluIGFueSBraW5kIG9mIGNvbmR1Y3QgdGhhdCB2aW9sYXRlcyBhbnkgYXBwbGljYWJsZSBzdGF0ZSwgbG9jYWwsIG9yIGludGVybmF0aW9uYWwgbGF3IG9yIHJlZ3VsYXRpb24gKGluY2x1ZGluZywgd2l0aG91dCBsaW1pdGF0aW9uLCBhbnkgbGF3cyByZWdhcmRpbmcgdGhlIGV4cG9ydCBvZiBkYXRhIG9yIHNvZnR3YXJlIGZyb20gdGhlIHBsYXRmb3JtIHRvIEluZGlhIG9yIGFueSBvdGhlciBjb3VudHJpZXMpJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHRleHQ6XG4gICAgICAgICAgJ05vIEZyYXVkOiBZZXMsIHRoaXMgaXMgcHJvYmFibHkgY292ZXJlZCBpbiB0aGUgTm8gSWxsZWdhbCBBY3Rpdml0eSBzZWN0aW9uIGFib3ZlLCBidXQgd2Ugd2FudCB0byBtYWtlIHRoaXMgdmVyeSBjbGVhci4gRnJhdWQgd2lsbCBub3QgYmUgdG9sZXJhdGVkIGluIGFueSBjYXBhY2l0eScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXh0OlxuICAgICAgICAgICdObyBFeHBsb2l0YXRpb246IFlvdSB3aWxsIG5vdCB1c2UgdGhlIFBsYXRmb3JtIHRvIHRyeSB0byBnYXRoZXIgcGVyc29uYWwgaW5mb3JtYXRpb24gb24gYW55b25lLCBvdXRzaWRlIG9mIHRoZSBsaW1pdGVkIHBlcm1pc3NpYmxlIHVzZXMgZm9yIG9mZmVyaW5nIHlvdXIgQ29udGVudCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXh0OlxuICAgICAgICAgICdObyBVc2UgT3RoZXIgVGhhbiBJbnRlbmRlZDogWW91IG1heSBub3QgdXNlIHRoZSBQbGF0Zm9ybSBvciBhbnkgY29udGVudCBjb250YWluZWQgb24gdGhlIFBsYXRmb3JtIGZvciBhbnkgcHVycG9zZXMgb3RoZXIgdGhhbiBpbnRlbmRlZC4nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGV4dDpcbiAgICAgICAgICAnSWYgWW91IHZpb2xhdGUgdGhpcyBDb2RlIG9mIENvbmR1Y3Qgd2UgcmVzZXJ2ZSB0aGUgcmlnaHQgdG8gcmVtb3ZlIFlvdSBhbmQgYW55IG9mIFlvdXIgZGV0YWlscyBmcm9tIHRoZSBQbGF0Zm9ybS4gV2hldGhlciBjb25kdWN0IHZpb2xhdGVzIG91ciBDb2RlIG9mIENvbmR1Y3Qgd2lsbCBiZSBkZXRlcm1pbmVkIGF0IG91ciBkaXNjcmV0aW9uLicsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ1RlYWNoaW5nJyxcbiAgICBjb250ZW50OiBbXG4gICAgICB7XG4gICAgICAgIHRleHQ6XG4gICAgICAgICAgJ1RoZSBwbGF0Zm9ybSBtYXkgcHJvdmlkZSB0aGUgb3Bwb3J0dW5pdHkgZm9yIFRlYWNoZXJzIHRvIHNlbGwgb2ZmZXJpbmdzICjigJxDb2FjaGluZ+KAnSkgdG8gU3R1ZGVudHMgaW4gYWRkaXRpb24gdG8gdGhlIGNvdXJzZXMgdGhhdCB0aGUgQ3JlYXRvciBzZWxscyBvbiB0aGUgUGxhdGZvcm0gKHRvZ2V0aGVyIENvYWNoaW5nIGFuZCBjb3Vyc2VzIGFyZSByZWZlcnJlZCB0byBhcyBDb250ZW50KS4gQWxsIENvYWNoaW5nIHNoYWxsIGJlIGNvdmVyZWQgYnkgdGhpcyBBZ3JlZW1lbnQgYW5kIGFyZSBpbmNsdWRlZCBpbiB0aGUgZGVmaW5pdGlvbiBvZiBDb250ZW50LiBTaG91bGQgdGhlIENvYWNoaW5nIGludm9sdmUgYW55IHRoaXJkIHBhcnR5IGNvbnRlbnQsIFlvdSBhZ3JlZSB0aGF0IHRoZSBwbGF0Zm9ybSBpcyBub3QgcmVzcG9uc2libGUgaW4gYW55IHdheSBmb3Igc3VjaCBjb250ZW50LCBhbmQgbWFrZXMgbm8gcmVwcmVzZW50YXRpb25zIG9yIGd1YXJhbnRlZXMgYXMgdG8gaXRzIG1lcmNoYW50YWJpbGl0eSBvciBmaXRuZXNzIG9mIHVzZS4gVGhlIGRlbGl2ZXJ5IG9mIHRoZSBDb2FjaGluZyBpcyB0aGUgc29sZSByZXNwb25zaWJpbGl0eSBvZiB0aGUgQ3JlYXRvci4nLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdHZW5lcmFsIFJpZ2h0cyBJbiBPcGVyYXRpbmcgdGhlIFBsYXRmb3JtJyxcbiAgICBjb250ZW50OiBbXG4gICAgICB7XG4gICAgICAgIHRleHQ6XG4gICAgICAgICAgJ1RoZSBwbGF0Zm9ybSByZXNlcnZlcyB0aGUgZm9sbG93aW5nIHJpZ2h0cyBvdmVyIHRoZSBlbnRpcmUgcGxhdGZvcm06JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHRleHQ6XG4gICAgICAgICAgJ1RoZSBwbGF0Zm9ybSBtYXksIGJ1dCBoYXMgbm8gb2JsaWdhdGlvbiB0bywgbW9uaXRvciBhbnkgY29udGVudCB0aGF0IGFwcGVhcnMgb24gdGhlIHBsYXRmb3JtIG9yIHJldmlldyBhbnkgY29uZHVjdCBvY2N1cnJpbmcgdGhyb3VnaCB0aGUgUGxhdGZvcm0sIGluY2x1ZGluZyBhbnkgaW50ZXJhY3Rpb25zIGJldHdlZW4gVHV0b3JzIGFuZCBTdHVkZW50cy4nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGV4dDpcbiAgICAgICAgICAnVGhlIHBsYXRmb3JtIGhhcywgaW4gaXRzIHNvbGUgZGlzY3JldGlvbiwgYW4gYWJzb2x1dGUgcmlnaHQgdG8gbW9kaWZ5LCBjaGFuZ2UsIGFsdGVyLCBzdXNwZW5kLCBvciB0ZXJtaW5hdGUgYW55IHByb3Zpc2lvbiBvZiB0aGlzIEFncmVlbWVudCB3aXRob3V0IHdhcm5pbmcgYXQgYW55IHRpbWUnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGV4dDpcbiAgICAgICAgICAnVGhlIHBsYXRmb3JtIGRvZXMgbm90IGd1YXJhbnRlZSB0aGF0IHRoZSBQbGF0Zm9ybSBvciBhbnkgc2VydmljZXMgb2ZmZXJlZCB0aHJvdWdoIHRoZSBQbGF0Zm9ybSB3aWxsIGJlIGVycm9yLWZyZWUgb3Igb3RoZXJ3aXNlIHJlbGlhYmxlLCBub3IgZG9lcyB0aGUgcGxhdGZvcm0gZ3VhcmFudGVlIHRoYXQgZGVmZWN0cyB3aWxsIGJlIGNvcnJlY3RlZCBvciB0aGF0IGFueSBvZmZlcmluZ3MgdGhyb3VnaCB0aGUgUGxhdGZvcm0gd2lsbCBhbHdheXMgYmUgYWNjZXNzaWJsZS4gVGhlIHBsYXRmb3JtIG1heSBtYWtlIGltcHJvdmVtZW50cyBhbmQvb3IgY2hhbmdlcyB0byB0aGUgUGxhdGZvcm0gYW5kIHRoZWlyIGZlYXR1cmVzIGFuZCBmdW5jdGlvbmFsaXR5IGF0IGFueSB0aW1lLCBhbmQgd2lsbCB1c2UgY29tbWVyY2lhbGx5IHJlYXNvbmFibGUgZWZmb3J0cyB0byBhdm9pZCBkaXNydXB0aW5nIHBlYWsgaG91cnMsIHRob3VnaCBzb21lIGRvd250aW1lIG1heSBvY2N1ci4gRXJyb3JzIGluIENvbnRlbnQgYXJlIHRoZSByZXNwb25zaWJpbGl0eSBvZiB0aGUgQ3JlYXRvciB3aG8gb3ducyB0aGUgQ29udGVudC4nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGV4dDpcbiAgICAgICAgICAnV2UgcmVzZXJ2ZSB0aGUgcmlnaHQgdG8gYW1lbmQgdGhlIFBsYXRmb3JtLCBhbmQgYW55IHNlcnZpY2Ugb3IgbWF0ZXJpYWwgd2UgcHJvdmlkZSBvbiB0aGUgUGxhdGZvcm0sIGluIG91ciBzb2xlIGRpc2NyZXRpb24gd2l0aG91dCBub3RpY2UuIFdlIHdpbGwgbm90IGJlIGxpYWJsZSBpZiBmb3IgYW55IHJlYXNvbiBhbGwgb3IgYW55IHBhcnQgb2YgdGhlIFBsYXRmb3JtIGlzIHVuYXZhaWxhYmxlIGF0IGFueSB0aW1lIG9yIGZvciBhbnkgcGVyaW9kLiBGcm9tIHRpbWUgdG8gdGltZSwgd2UgbWF5IHJlc3RyaWN0IGFjY2VzcyB0byBzb21lIG9yIGFsbCBvZiB0aGUgUGxhdGZvcm0gdG8gVHV0b3JzIGFuZCBTdHVkZW50cy4nLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuXTtcblxuY2xhc3MgUHJpdmFjeUFuZFRlcm1zIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgZGlzcGxheUhlYWRlciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5iYW5uZXJTZWN0aW9ufT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmd9PkVnbmlmeSBUZXJtcyBvZiBTZXJ2aWNlPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheU1haW5Db250ZXh0ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmludHJvQ29udGVudH0+XG4gICAgICB7SW50cm9Db250ZW50Lm1hcChpdGVtID0+IChcbiAgICAgICAgPGRpdj57aXRlbS5jb250ZW50fTwvZGl2PlxuICAgICAgKSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVN1YkNvbnRleHQgPSAoKSA9PiAoXG4gICAgPGRpdj5cbiAgICAgIHtTdWJDb250ZW50Lm1hcChzdWIgPT4gKFxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21taXR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbW1pdEhlYWRpbmd9PntzdWIudGl0bGV9PC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tbWl0Q29udGVudH0+XG4gICAgICAgICAgICB7c3ViLmNvbnRlbnQubWFwKHN1YkNvbnRlbnQgPT4gKFxuICAgICAgICAgICAgICA8bGk+e3N1YkNvbnRlbnQudGV4dH08L2xpPlxuICAgICAgICAgICAgKSl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICB7dGhpcy5kaXNwbGF5SGVhZGVyKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlNYWluQ29udGV4dCgpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U3ViQ29udGV4dCgpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFByaXZhY3lBbmRUZXJtcyk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpdmFjeUFuZFRlcm1zLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnY29tcG9uZW50cy9MYXlvdXQnO1xuaW1wb3J0IFByaXZhY3lBbmRUZXJtcyBmcm9tICcuL1ByaXZhY3lBbmRUZXJtcyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogWydHZXRSYW5rcyBieSBFZ25pZnk6IEFzc2Vzc21lbnQgJiBBbmFseXRpY3MgUGxhdGZvcm0nXSxcbiAgICBjaHVua3M6IFsnVGVybXNBbmRDb25kaXRpb25zJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8UHJpdmFjeUFuZFRlcm1zIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBTUE7QUFFQTtBQUNBO0FBRUE7QUFEQTtBQUhBO0FBVUE7QUFDQTtBQUVBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQXZCQTtBQThCQTtBQUNBO0FBRUE7QUFEQTtBQUhBO0FBVUE7QUFDQTtBQUVBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBbkJBO0FBQ0E7QUEwQkE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUpBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVhBO0FBZ0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRCQTtBQUNBO0FBNkJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBdkNBO0FBQ0E7QUF3Q0E7Ozs7Ozs7QUN2SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVNBO0FBQ0E7QUFDQTs7OztBIiwic291cmNlUm9vdCI6IiJ9