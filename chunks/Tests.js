require("source-map-support").install();
exports.ids = ["Tests"];
exports.modules = {

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slider-Player/SliderPlayer.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".SliderPlayer-root-1PJ5x {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.SliderPlayer-video-39D4o {\n  width: 400px;\n  height: 400px;\n}\n\n.SliderPlayer-desktopControls-36NJ2 {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.SliderPlayer-controlsWrapper-lXOHa {\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 96px;\n}\n\n.SliderPlayer-controls-1_uIj {\n  width: 240px;\n  margin: 0 16px;\n  margin: 0 1rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.SliderPlayer-outer-U9Tud {\n  width: 240px;\n  background-color: #d8d8d8;\n  height: 4px;\n  position: relative;\n  border-radius: 4px;\n  overflow-x: hidden;\n}\n\n.SliderPlayer-inner-3AXse {\n  position: absolute;\n  height: 4px;\n  top: 0;\n  left: 0;\n  z-index: 1;\n  background-color: #f36;\n  -webkit-transition: all 0.1s ease;\n  -o-transition: all 0.1s ease;\n  transition: all 0.1s ease;\n}\n\n.SliderPlayer-videoTitle-22XxD {\n  font-size: 24px;\n  line-height: 32px;\n  margin-top: 12px;\n  font-weight: 600;\n}\n\n@media only screen and (max-width: 990px) {\n  .SliderPlayer-video-39D4o {\n    width: 100%;\n    height: 100%;\n    max-width: 400px;\n    max-height: 300px;\n  }\n\n  .SliderPlayer-mobileTitlesWrapper-1yran {\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    max-width: 330px;\n    overflow-x: hidden;\n  }\n\n  .SliderPlayer-mobileControls-1h6o7 {\n    width: 100%;\n  }\n\n  .SliderPlayer-videoTitle-22XxD {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 20px;\n    margin-right: 12px;\n    min-width: 100px;\n    -webkit-transition: -webkit-transform 1s ease;\n    transition: -webkit-transform 1s ease;\n    -o-transition: transform 1s ease;\n    transition: transform 1s ease;\n    transition: transform 1s ease, -webkit-transform 1s ease;\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-actTitle-3350y {\n    font-weight: bold;\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut-zfY0h {\n    -webkit-transform: translateX(0);\n        -ms-transform: translateX(0);\n            transform: translateX(0);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut1-2iCv0 {\n    -webkit-transform: translateX(-100%);\n        -ms-transform: translateX(-100%);\n            transform: translateX(-100%);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut2-4nz6i {\n    -webkit-transform: translateX(-210%);\n        -ms-transform: translateX(-210%);\n            transform: translateX(-210%);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut3-1C_fo {\n    -webkit-transform: translateX(-320%);\n        -ms-transform: translateX(-320%);\n            transform: translateX(-320%);\n  }\n\n  .SliderPlayer-controlsWrapper-lXOHa {\n    width: 100%;\n    max-width: 330px;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 0;\n    overflow-x: hidden;\n    margin-top: 32px;\n  }\n\n  .SliderPlayer-controls-1_uIj {\n    width: 200px;\n    margin: 0 12px 0 0;\n  }\n\n  .SliderPlayer-desktopControls-36NJ2 {\n    overflow-x: scroll;\n  }\n\n  .SliderPlayer-outer-U9Tud {\n    width: 100%;\n  }\n\n  .SliderPlayer-outer-U9Tud.SliderPlayer-mobileTracker-mILab {\n    width: 100%;\n    margin: auto;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slider-Player/SliderPlayer.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,iBAAiB;CAClB;;AAED;EACE,aAAa;EACb,eAAe;EACf,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;CACjC;;AAED;EACE,aAAa;EACb,0BAA0B;EAC1B,YAAY;EACZ,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,OAAO;EACP,QAAQ;EACR,WAAW;EACX,uBAAuB;EACvB,kCAAkC;EAClC,6BAA6B;EAC7B,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,YAAY;IACZ,iBAAiB;IACjB,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;EAED;IACE,oBAAoB;IACpB,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;IACnB,iBAAiB;IACjB,8CAA8C;IAC9C,sCAAsC;IACtC,iCAAiC;IACjC,8BAA8B;IAC9B,yDAAyD;GAC1D;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,iCAAiC;QAC7B,6BAA6B;YACzB,yBAAyB;GAClC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,2BAA2B;QACvB,uBAAuB;IAC3B,WAAW;IACX,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,aAAa;IACb,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;EAED;IACE,YAAY;IACZ,aAAa;GACd;CACF","file":"SliderPlayer.scss","sourcesContent":[".root {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.video {\n  width: 400px;\n  height: 400px;\n}\n\n.desktopControls {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.controlsWrapper {\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 96px;\n}\n\n.controls {\n  width: 240px;\n  margin: 0 16px;\n  margin: 0 1rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.outer {\n  width: 240px;\n  background-color: #d8d8d8;\n  height: 4px;\n  position: relative;\n  border-radius: 4px;\n  overflow-x: hidden;\n}\n\n.inner {\n  position: absolute;\n  height: 4px;\n  top: 0;\n  left: 0;\n  z-index: 1;\n  background-color: #f36;\n  -webkit-transition: all 0.1s ease;\n  -o-transition: all 0.1s ease;\n  transition: all 0.1s ease;\n}\n\n.videoTitle {\n  font-size: 24px;\n  line-height: 32px;\n  margin-top: 12px;\n  font-weight: 600;\n}\n\n@media only screen and (max-width: 990px) {\n  .video {\n    width: 100%;\n    height: 100%;\n    max-width: 400px;\n    max-height: 300px;\n  }\n\n  .mobileTitlesWrapper {\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    max-width: 330px;\n    overflow-x: hidden;\n  }\n\n  .mobileControls {\n    width: 100%;\n  }\n\n  .videoTitle {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 20px;\n    margin-right: 12px;\n    min-width: 100px;\n    -webkit-transition: -webkit-transform 1s ease;\n    transition: -webkit-transform 1s ease;\n    -o-transition: transform 1s ease;\n    transition: transform 1s ease;\n    transition: transform 1s ease, -webkit-transform 1s ease;\n  }\n\n  .videoTitle.actTitle {\n    font-weight: bold;\n  }\n\n  .videoTitle.slideOut {\n    -webkit-transform: translateX(0);\n        -ms-transform: translateX(0);\n            transform: translateX(0);\n  }\n\n  .videoTitle.slideOut1 {\n    -webkit-transform: translateX(-100%);\n        -ms-transform: translateX(-100%);\n            transform: translateX(-100%);\n  }\n\n  .videoTitle.slideOut2 {\n    -webkit-transform: translateX(-210%);\n        -ms-transform: translateX(-210%);\n            transform: translateX(-210%);\n  }\n\n  .videoTitle.slideOut3 {\n    -webkit-transform: translateX(-320%);\n        -ms-transform: translateX(-320%);\n            transform: translateX(-320%);\n  }\n\n  .controlsWrapper {\n    width: 100%;\n    max-width: 330px;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 0;\n    overflow-x: hidden;\n    margin-top: 32px;\n  }\n\n  .controls {\n    width: 200px;\n    margin: 0 12px 0 0;\n  }\n\n  .desktopControls {\n    overflow-x: scroll;\n  }\n\n  .outer {\n    width: 100%;\n  }\n\n  .outer.mobileTracker {\n    width: 100%;\n    margin: auto;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "SliderPlayer-root-1PJ5x",
	"video": "SliderPlayer-video-39D4o",
	"desktopControls": "SliderPlayer-desktopControls-36NJ2",
	"controlsWrapper": "SliderPlayer-controlsWrapper-lXOHa",
	"controls": "SliderPlayer-controls-1_uIj",
	"outer": "SliderPlayer-outer-U9Tud",
	"inner": "SliderPlayer-inner-3AXse",
	"videoTitle": "SliderPlayer-videoTitle-22XxD",
	"mobileTitlesWrapper": "SliderPlayer-mobileTitlesWrapper-1yran",
	"mobileControls": "SliderPlayer-mobileControls-1h6o7",
	"actTitle": "SliderPlayer-actTitle-3350y",
	"slideOut": "SliderPlayer-slideOut-zfY0h",
	"slideOut1": "SliderPlayer-slideOut1-2iCv0",
	"slideOut2": "SliderPlayer-slideOut2-4nz6i",
	"slideOut3": "SliderPlayer-slideOut3-1C_fo",
	"mobileTracker": "SliderPlayer-mobileTracker-mILab"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Test/Test.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Test-maxContainer-1_QCO {\n  max-width: 1700px;\n  width: 100%;\n  position: relative;\n  margin: auto;\n}\n\n.Test-marginBottom126-2y-TJ {\n  margin-bottom: 126px;\n}\n\n.Test-reports_sub_title-1zU4V {\n  font-size: 28px;\n  line-height: 32px;\n  font-weight: 600;\n  margin-top: 12px;\n  color: #25282b;\n}\n\n.Test-reports_notice-1k7xY {\n  font-size: 20px;\n  line-height: 32px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-top: 4px;\n  margin-bottom: 12px;\n}\n\n.Test-row1-1pBIZ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n}\n\n.Test-pointContainer-yl6rU {\n  width: 5%;\n}\n\n.Test-point-2yHMB {\n  margin-bottom: 12px;\n}\n\n.Test-pointText-3omy9 {\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.Test-redDot-3mKH- {\n  width: 8px;\n  height: 8px;\n  border-radius: 4px;\n  background-color: #f36;\n  margin-right: 20px;\n  margin-top: 12px;\n}\n\n.Test-testsImage-3Z2de {\n  width: 72px;\n  height: 72px;\n  margin-top: -8px;\n  margin-right: 24px;\n}\n\n.Test-testsImage-3Z2de img {\n  width: 100%;\n  height: 100%;\n}\n\n.Test-section_container-2wA5m.Test-ashBackground-q1E-3 {\n  background-color: #f7f7f7;\n}\n\n.Test-videoSlider-28_ug {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 80px 64px;\n  padding: 5rem 4rem;\n  background-color: #f7f7f7;\n  position: relative;\n}\n\n.Test-maths-TucEU {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.Test-microscope-HsatG {\n  position: absolute;\n  top: 30%;\n  left: 3%;\n}\n\n.Test-triangle-2BUKY {\n  position: absolute;\n  top: 60%;\n  left: 10%;\n}\n\n.Test-scale-1c9I4 {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n}\n\n.Test-circle-2jW1w {\n  position: absolute;\n  top: 30%;\n  right: 3%;\n}\n\n.Test-chemistry-2eYOW {\n  position: absolute;\n  top: 60%;\n  right: 10%;\n}\n\n.Test-headerbackgroundmobile-3FN-6 {\n  position: absolute;\n  bottom: -56px;\n  right: 0;\n  width: 513px;\n  height: 560px;\n}\n\n.Test-headerbackgroundmobile-3FN-6 img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.Test-section_title-1u6t0 {\n  display: block;\n  margin: 0 0 16px;\n  font-size: 40px;\n  line-height: 48px;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Test-sub_section_title-3VRu3 {\n  font-size: 28px;\n  line-height: 32px;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 12px;\n  margin-top: 56px;\n}\n\n.Test-contentPart-vOxYm {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 45%;\n}\n\n.Test-contentPart-vOxYm .Test-content-2hs9s {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n  }\n\n.Test-contentPart-vOxYm .Test-content-2hs9s .Test-textcontent-3rWf1 {\n      font-size: 16px;\n      line-height: 24px;\n      color: #25282b;\n\n      /* .point {\n        width: 100%;\n        display: flex;\n        align-items: flex-start;\n        height: max-content;\n        margin: 6px 0;\n\n        .reddotwrapper {\n          width: 5%;\n          display: flex;\n          align-items: center;\n          justify-content: flex-start;\n          margin-top: 12px;\n\n          .reddot {\n            width: 8px;\n            height: 8px;\n            background-color: #f36;\n            border-radius: 50%;\n          }\n        }\n\n        .pointText {\n          width: 95%;\n        }\n      } */\n    }\n\n/* .customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 560px;\n\n  .customer_review {\n    order: 0;\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n\n    .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n\n      img {\n        width: 100%;\n        height: 100%;\n        object-fit: contain;\n      }\n    }\n\n    .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n    .authorWrapper {\n      display: flex;\n      flex-direction: column;\n\n      .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n      .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n    }\n\n    .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: flex;\n      flex-direction: row;\n      align-items: center;\n\n      img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        object-fit: contain;\n      }\n    }\n  }\n} */\n\n.Test-availableContainer-3xLT2 {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.Test-availableContainer-3xLT2 .Test-availableRow-2Qync {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-desktopImage-2kXNX {\n      width: 696px;\n      height: 371px;\n    }\n\n.Test-joinCommunity-3LBUs {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-joinFbText-1fdhM {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.Test-joinFbLink-Avcsf {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.Test-availableTitle-3NEfA {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Test-available-3lWLX {\n  color: #f36;\n}\n\n.Test-availableContentSection-UP4ln {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Test-platformContainer-2Ohnt {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.Test-platform-kOxAM {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.Test-platformOs-36PkG {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.Test-row-sE-Co {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.Test-store-3Cziz {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.Test-store-3Cziz .Test-playstore-3PKOC {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.Test-store-3Cziz .Test-appstore-1hO16 {\n    width: 136px;\n    height: 40px;\n  }\n\n.Test-scrollTop-14S8C {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Test-scrollTop-14S8C img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Test-teachContainer-4lLOo {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #f2e5fe;\n  position: relative;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 {\n  width: 316px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n}\n\n.Test-imagePart-FumwY .Test-emptyCard-2MxHA img {\n  width: 100%;\n  height: 100%;\n  // object-fit: contain;\n}\n\n.Test-imagePart-FumwY .Test-topCard-3A_No img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n}\n\n.Test-teachContainer-4lLOo .Test-contentText-2Hp6y {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-requestDemo-EyPHF {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ .Test-whatsapp-3-hIr {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ img {\n  width: 32px;\n  height: 32px;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI .Test-downloadText-2KLuA {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI .Test-playStoreIcon-3vbFK {\n  width: 132px;\n  height: 40px;\n}\n\n/* .teachContainer .actionsWrapper {\n  position: absolute;\n  bottom: 26px;\n  width: 100%;\n  display: flex;\n}\n.teachContainer .actionsWrapper .action {\n  width: fit-content;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: 24px;\n}\n.teachContainer .actionsWrapper .action span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  opacity: 0.7;\n  text-align: left;\n  margin-left: 8px;\n}\n.teachContainer .actionsWrapper .action .actionimgbox {\n  width: 24px;\n  height: 24px;\n  background-color: #fff;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.teachContainer .actionsWrapper .action .actionimgbox img {\n  width: 10px;\n  height: 9px;\n} */\n\n.Test-teachContainer-4lLOo .Test-breadcrum-1j15E {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.Test-displayClients-18jr5 span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Test-teachContainer-4lLOo .Test-breadcrum-1j15E span {\n  font-weight: 600;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP {\n  width: 40px;\n  height: 40px;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachSectionName-1fAAr {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #8800fe;\n  font-weight: bold;\n}\n\n/* .teachContainer .topSection .featuresSection {\n  display: flex;\n  align-items: center;\n  margin-left: 30%;\n}\n.teachContainer .topSection .featuresSection span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  margin-right: 32px;\n  font-weight: 500;\n} */\n\n.Test-displayClients-18jr5 {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.Test-displayClients-18jr5 h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP .Test-client-FzdBZ {\n  width: 172px;\n  height: 112px;\n}\n\n.Test-displayClients-18jr5 span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Test-displayClients-18jr5 span a:hover {\n  text-decoration: underline;\n}\n\n.Test-achievedContainer-1FrOF {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-clients-2p-cX {\n  background-color: #fff3eb;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-students-3C7ru {\n  background-color: #f7effe;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-tests-2gs17 {\n  background-color: #ffebf0;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-questions-3vWWW {\n  background-color: #ebffef;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-questionsHighlight-2_VNQ {\n  color: #00ac26;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-testsHighlight-2bD1f {\n  color: #f36;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-studentsHighlight-3p4cJ {\n  color: #8c00fe;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-clientsHighlight-yClDo {\n  color: #f60;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-subText-2Q8Rs {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.Test-section_container-2wA5m {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.Test-section_container-2wA5m .Test-section_contents-2WASW {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.Test-section_container_reverse-1IKFo {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.Test-section_container_reverse-1IKFo .Test-section_contents-2WASW {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.Test-imagePart-FumwY {\n  width: 50%;\n}\n\n.Test-imagePart-FumwY .Test-emptyCard-2MxHA {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  position: relative;\n}\n\n.Test-imagePart-FumwY .Test-topCard-3A_No {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.Test-imagePart-FumwY .Test-bottomCircle-2SECO {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.Test-imagePart-FumwY .Test-topCircle-2B2HP {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-topCircle-2B2HP {\n  top: -44px;\n  right: 50px;\n  background-color: #f2e5fe;\n}\n\n.Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-bottomCircle-2SECO {\n  bottom: -44px;\n  left: 79px;\n  background-color: #ffe0e8;\n}\n\n.Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-topCircle-2B2HP {\n  background-color: #3fc;\n  opacity: 0.3;\n  top: -44px;\n  right: 70px;\n}\n\n.Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-bottomCircle-2SECO {\n  background-color: #ffe0e8;\n  bottom: -44px;\n  left: 40px;\n}\n\n.Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-topCircle-2B2HP {\n  background-color: #0076ff;\n  opacity: 0.2;\n  top: -44px;\n  right: 70px;\n}\n\n.Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-bottomCircle-2SECO {\n  background-color: #feb546;\n  bottom: -44px;\n  left: 40px;\n  opacity: 0.2;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD {\n  width: 45%;\n  margin-bottom: 56px;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_title-1DOvX {\n  font-size: 20px;\n  font-weight: bold;\n  line-height: 30px;\n  margin-bottom: 8px;\n  color: #25282b;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_text-QKFHq {\n  font-size: 16px;\n  line-height: 24px;\n  color: #25282b;\n}\n\n/* .allcustomers p {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n} */\n\n.Test-content-2hs9s p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.Test-hide-3vPQU {\n  display: none;\n}\n\n.Test-show-LTOHo {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n@media only screen and (max-width: 1200px) {\n  .Test-availableContainer-3xLT2 {\n    padding: 5rem 64px 0 64px;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .Test-testsImage-3Z2de {\n    width: 48px;\n    height: 48px;\n    margin: auto;\n  }\n\n  .Test-section_title-1u6t0 {\n    font-size: 24px;\n    line-height: normal;\n    text-align: center;\n    margin-top: 16px;\n    margin-bottom: 24px;\n  }\n\n  .Test-point-2yHMB {\n    margin-bottom: 8px;\n  }\n\n  .Test-redDot-3mKH- {\n    margin-top: 8px;\n  }\n\n  .Test-pointText-3omy9 {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-imagePart-FumwY .Test-bottomCircle-2SECO {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .Test-imagePart-FumwY .Test-topCircle-2B2HP {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-topCircle-2B2HP {\n    top: -24px;\n    right: 50px;\n    background-color: #f2e5fe;\n  }\n\n  .Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-bottomCircle-2SECO {\n    bottom: -24px;\n    left: 79px;\n    background-color: #ffe0e8;\n  }\n\n  .Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-topCircle-2B2HP {\n    background-color: #3fc;\n    opacity: 0.3;\n    top: -24px;\n    right: 70px;\n  }\n\n  .Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-bottomCircle-2SECO {\n    background-color: #ffe0e8;\n    bottom: -24px;\n    left: 40px;\n  }\n\n  .Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-topCircle-2B2HP {\n    background-color: #0076ff;\n    opacity: 0.2;\n    top: -24px;\n    right: 70px;\n  }\n\n  .Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-bottomCircle-2SECO {\n    background-color: #feb546;\n    bottom: -24px;\n    left: 40px;\n    opacity: 0.2;\n  }\n\n  .Test-teachContainer-4lLOo .Test-downloadApp-h4dCI {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .Test-section_container-2wA5m {\n    min-height: 20rem;\n    padding: 2rem 1rem;\n  }\n\n  .Test-section_container-2wA5m .Test-maxContainer-1_QCO {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    max-width: 600px;\n  }\n\n  .Test-section_container-2wA5m .Test-section_contents-2WASW {\n    margin-top: 32px;\n    max-height: none;\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-section_container_reverse-1IKFo {\n    padding: 24px 20px 48px;\n  }\n\n  .Test-section_container_reverse-1IKFo .Test-section_contents-2WASW {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-contentPart-vOxYm {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding-bottom: 0;\n  }\n\n  .Test-contentPart-vOxYm > div:nth-child(2) {\n    width: 100%;\n    max-width: 400px;\n    margin: auto;\n  }\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s {\n    max-width: none;\n  }\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s .Test-textcontent-3rWf1 {\n    display: block;\n    width: 100%;\n    max-width: 600px;\n    text-align: left;\n    font-size: 14px;\n    line-height: 24px;\n    margin: auto;\n  }\n\n  /* .contentPart .content .textcontent .point .reddotwrapper {\n    margin-top: 9px;\n  }\n\n  .contentPart .content .textcontent .point .pointText {\n    text-align: left;\n  } */\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s .Test-section_title-1u6t0 {\n    font-size: 24px;\n    text-align: left;\n    margin-bottom: 16px;\n  }\n\n  .Test-imagePart-FumwY {\n    width: 100%;\n    margin-top: 48px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n  }\n\n  .Test-imagePart-FumwY .Test-emptyCard-2MxHA {\n    width: 100%;\n    max-width: 360px;\n    height: 180px;\n    margin: 0 auto 32px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD {\n    width: 100%;\n    margin-bottom: 32px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD:last-child {\n    margin-bottom: 0;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_title-1DOvX {\n    font-size: 16px;\n    margin-bottom: 12px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_text-QKFHq {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-mobilePapersContainer-1GPOu {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Test-mobilePapersContainer-1GPOu img {\n    width: 100%;\n    height: 100%;\n    max-width: 360px;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n  .Test-papersWrapper-1aINd {\n    max-width: 360px;\n    margin: auto;\n  }\n\n  .Test-reports_sub_title-1zU4V {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-reports_notice-1k7xY {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-sub_section_title-3VRu3 {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-headerbackgroundmobile-3FN-6 {\n    width: 232px;\n    height: 335px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .Test-displayClients-18jr5 {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .Test-displayClients-18jr5 h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP .Test-client-FzdBZ {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .Test-displayClients-18jr5 span {\n    max-width: 307px;\n  }\n\n  /* .displayClients .clientsWrapper .dots {\n    display: flex;\n  } */\n  .Test-achievedContainer-1FrOF {\n    padding: 1.5rem 1rem;\n    background: url('/images/home/Confetti.svg');\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO {\n    font-size: 1.5rem;\n    padding: 0;\n    margin-bottom: 1.5rem;\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO span::after {\n    content: '\\A';\n    white-space: pre;\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf {\n    grid-template-columns: 1fr;\n    gap: 0.75rem;\n    max-width: 20.5rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: auto;\n  }\n\n  /* .customers_container {\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    width: 100%;\n    align-items: center;\n\n    .customer_review {\n      order: 2;\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n\n      .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n\n      .authorWrapper {\n        .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n      }\n    }\n  } */\n\n  .Test-teachContainer-4lLOo .Test-maxContainer-1_QCO {\n    position: static;\n    max-width: 450px;\n  }\n\n  .Test-teachContainer-4lLOo .Test-breadcrum-1j15E {\n    display: none;\n    text-align: center;\n  }\n\n  .Test-teachContainer-4lLOo {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .Test-teachContainer-4lLOo .Test-heading-1oNs5 {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n    }\n\n      .Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 {\n        width: 210px;\n      }\n\n        .Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .Test-teachContainer-4lLOo .Test-contentText-2Hp6y {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-requestDemo-EyPHF {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ .Test-whatsapp-3-hIr {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .Test-teachContainer-4lLOo .Test-topSection-21bV3 {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP {\n          width: 32px;\n          height: 32px;\n        }\n\n        .Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachSectionName-1fAAr {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .Test-availableContainer-3xLT2 {\n    padding: 32px 28px;\n    overflow: hidden;\n  }\n\n    .Test-availableContainer-3xLT2 .Test-availableRow-2Qync {\n      grid-template-columns: 1fr;\n    }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-availableTitle-3NEfA {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co {\n        margin: 0;\n        margin-bottom: 32px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt .Test-platform-kOxAM {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt .Test-platformOs-36PkG {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt:last-child {\n          margin-right: 0;\n        }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-desktopImage-2kXNX {\n        width: 90%;\n        height: 161px;\n        margin: 0 auto 0 auto;\n      }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz {\n        -ms-flex-pack: center;\n            justify-content: center;\n        margin-bottom: 32px;\n      }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz .Test-playstore-3PKOC {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz .Test-appstore-1hO16 {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .Test-joinCommunity-3LBUs {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .Test-joinFbText-1fdhM {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .Test-joinFbLink-Avcsf {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-videoSlider-28_ug {\n    padding: 3rem 1rem;\n    min-height: 38rem;\n  }\n\n  .Test-maths-TucEU {\n    position: absolute;\n    left: 15%;\n    top: 84%;\n    width: 36px;\n    height: 36px;\n  }\n\n  .Test-microscope-HsatG {\n    position: absolute;\n    top: 87%;\n    left: 45%;\n    width: 40px;\n    height: 48px;\n  }\n\n  .Test-triangle-2BUKY {\n    position: absolute;\n    top: 85%;\n    left: 80%;\n    width: 36px;\n    height: 30px;\n  }\n\n  .Test-scale-1c9I4 {\n    position: absolute;\n    right: 80%;\n    top: 10%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .Test-circle-2jW1w {\n    position: absolute;\n    top: 3%;\n    right: 50%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .Test-chemistry-2eYOW {\n    position: absolute;\n    top: 8%;\n    right: 5%;\n    width: 36px;\n    height: 48px;\n  }\n\n  .Test-scrollTop-14S8C {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/Test.scss"],"names":[],"mappings":"AAAA;EACE,kBAAkB;EAClB,YAAY;EACZ,mBAAmB;EACnB,aAAa;CACd;;AAED;EACE,qBAAqB;CACtB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;EACb,gBAAgB;EAChB,oBAAoB;CACrB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;CACzB;;AAED;EACE,UAAU;CACX;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,UAAU;EACV,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,SAAS;CACV;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,WAAW;CACZ;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,SAAS;EACT,aAAa;EACb,cAAc;CACf;;AAED;IACI,YAAY;IACZ,aAAa;IACb,uBAAuB;OACpB,oBAAoB;GACxB;;AAEH;EACE,eAAe;EACf,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;EACpB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,WAAW;CACZ;;AAED;IACI,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;GAClB;;AAEH;MACM,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;;MAEf;;;;;;;;;;;;;;;;;;;;;;;;;UAyBI;KACL;;AAEL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IA0EI;;AAEJ;EACE,+BAA+B;EAC/B,+BAA+B;EAC/B,iBAAiB;EACjB,0BAA0B;CAC3B;;AAED;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;KACf;;AAEL;EACE,YAAY;EACZ,cAAc;EACd,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;CACb;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,sBAAsB;EACtB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,qBAAqB;MACjB,4BAA4B;EAChC,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,aAAa;IACb,aAAa;IACb,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,kBAAkB;EAClB,wBAAwB;EACxB,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,sBAAsB;EACtB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;CACxB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,QAAQ;EACR,YAAY;EACZ,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,gBAAgB;EAChB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,0BAA0B;EAC1B,qBAAqB;EACrB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,aAAa;EACb,aAAa;CACd;;AAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAiCI;;AAEJ;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;CACnB;;AAED;;;;;;;;;;;IAWI;;AAEJ;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;;IAEI;;AAEJ;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,sCAAsC;EACtC,UAAU;EACV,UAAU;EACV,aAAa;CACd;;AAED;EACE,aAAa;EACb,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,6BAA6B;EAC7B,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,sBAAsB;EACtB,+DAA+D;UACvD,uDAAuD;EAC/D,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;;;;;;;;;;;;;;IAcI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;EACvB,+BAA+B;EAC/B,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,gCAAgC;QAC5B,4BAA4B;IAChC,YAAY;IACZ,uBAAuB;QACnB,+BAA+B;GACpC;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;EAC1B,+BAA+B;EAC/B,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,wBAAwB;QACpB,oBAAoB;IACxB,YAAY;IACZ,uBAAuB;QACnB,+BAA+B;GACpC;;AAEH;EACE,WAAW;CACZ;;AAED;EACE,aAAa;EACb,cAAc;EACd,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,0BAA0B;CAC3B;;AAED;EACE,cAAc;EACd,WAAW;EACX,0BAA0B;CAC3B;;AAED;EACE,uBAAuB;EACvB,aAAa;EACb,WAAW;EACX,YAAY;CACb;;AAED;EACE,0BAA0B;EAC1B,cAAc;EACd,WAAW;CACZ;;AAED;EACE,0BAA0B;EAC1B,aAAa;EACb,WAAW;EACX,YAAY;CACb;;AAED;EACE,0BAA0B;EAC1B,cAAc;EACd,WAAW;EACX,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,uBAAuB;MACnB,+BAA+B;CACpC;;AAED;EACE,WAAW;EACX,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;CAChB;;AAED;;;;;IAKI;;AAEJ;EACE,gBAAgB;EAChB,kBAAkB;EAClB,uBAAuB;EACvB,eAAe;EACf,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE;IACE,0BAA0B;GAC3B;CACF;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,oBAAoB;IACpB,mBAAmB;IACnB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,WAAW;IACX,YAAY;IACZ,0BAA0B;GAC3B;;EAED;IACE,cAAc;IACd,WAAW;IACX,0BAA0B;GAC3B;;EAED;IACE,uBAAuB;IACvB,aAAa;IACb,WAAW;IACX,YAAY;GACb;;EAED;IACE,0BAA0B;IAC1B,cAAc;IACd,WAAW;GACZ;;EAED;IACE,0BAA0B;IAC1B,aAAa;IACb,WAAW;IACX,YAAY;GACb;;EAED;IACE,0BAA0B;IAC1B,cAAc;IACd,WAAW;IACX,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,aAAa;GACd;;EAED;IACE,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,iBAAiB;GAClB;;EAED;IACE,iBAAiB;IACjB,iBAAiB;IACjB,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,wBAAwB;GACzB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,YAAY;IACZ,2BAA2B;QACvB,uBAAuB;IAC3B,qBAAqB;QACjB,4BAA4B;IAChC,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,eAAe;IACf,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;IACjB,gBAAgB;IAChB,kBAAkB;IAClB,aAAa;GACd;;EAED;;;;;;MAMI;;EAEJ;IACE,gBAAgB;IAChB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,sBAAsB;QAClB,wBAAwB;IAC5B,2BAA2B;QACvB,uBAAuB;IAC3B,aAAa;GACd;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,cAAc;IACd,oBAAoB;GACrB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,YAAY;IACZ,oBAAoB;GACrB;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,uBAAuB;OACpB,oBAAoB;GACxB;;EAED;IACE,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,aAAa;IACb,cAAc;IACd,SAAS;IACT,SAAS;IACT,cAAc;IACd,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;GACd;;EAED;;MAEI;;EAEJ;IACE,iBAAiB;GAClB;;EAED;;MAEI;EACJ;IACE,qBAAqB;IACrB,6CAA6C;GAC9C;;EAED;IACE,kBAAkB;IAClB,WAAW;IACX,sBAAsB;GACvB;;EAED;IACE,cAAc;IACd,iBAAiB;GAClB;;EAED;IACE,2BAA2B;IAC3B,aAAa;IACb,mBAAmB;IACnB,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,aAAa;GACd;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAwCI;;EAEJ;IACE,iBAAiB;IACjB,iBAAiB;GAClB;;EAED;IACE,cAAc;IACd,mBAAmB;GACpB;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,mBAAmB;GACpB;;IAEC;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,gBAAgB;MAChB,yBAAyB;KAC1B;;MAEC;QACE,aAAa;OACd;;QAEC;UACE,QAAQ;UACR,iBAAiB;SAClB;;IAEL;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,iBAAiB;MACjB,yBAAyB;KAC1B;;IAED;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,iBAAiB;KAClB;;MAEC;QACE,kBAAkB;QAClB,gBAAgB;QAChB,kBAAkB;QAClB,gBAAgB;OACjB;;MAED;QACE,iBAAiB;QACjB,oBAAoB;OACrB;;QAEC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,WAAW;UACX,eAAe;SAChB;;QAED;UACE,YAAY;UACZ,aAAa;SACd;;IAEL;MACE,sBAAsB;UAClB,wBAAwB;MAC5B,cAAc;;MAEd;;UAEI;KACL;QACG;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,gBAAgB;UAChB,kBAAkB;SACnB;;EAEP;IACE,mBAAmB;IACnB,iBAAiB;GAClB;;IAEC;MACE,2BAA2B;KAC5B;;MAEC;QACE,gBAAgB;QAChB,mBAAmB;QACnB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,oBAAoB;QACpB,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,YAAY;UACZ,qBAAqB;cACjB,4BAA4B;UAChC,mBAAmB;UACnB,iBAAiB;SAClB;;UAEC;YACE,kBAAkB;YAClB,kBAAkB;WACnB;;UAED;YACE,gBAAgB;YAChB,kBAAkB;WACnB;;QAEH;UACE,gBAAgB;SACjB;;MAEH;QACE,WAAW;QACX,cAAc;QACd,sBAAsB;OACvB;;MAED;QACE,sBAAsB;YAClB,wBAAwB;QAC5B,oBAAoB;OACrB;;QAEC;UACE,aAAa;UACb,aAAa;UACb,mBAAmB;SACpB;;QAED;UACE,aAAa;UACb,aAAa;UACb,UAAU;SACX;;EAEP;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,UAAU;IACV,SAAS;IACT,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,SAAS;IACT,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,SAAS;IACT,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,WAAW;IACX,SAAS;IACT,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,WAAW;IACX,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;CACF","file":"Test.scss","sourcesContent":[".maxContainer {\n  max-width: 1700px;\n  width: 100%;\n  position: relative;\n  margin: auto;\n}\n\n.marginBottom126 {\n  margin-bottom: 126px;\n}\n\n.reports_sub_title {\n  font-size: 28px;\n  line-height: 32px;\n  font-weight: 600;\n  margin-top: 12px;\n  color: #25282b;\n}\n\n.reports_notice {\n  font-size: 20px;\n  line-height: 32px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-top: 4px;\n  margin-bottom: 12px;\n}\n\n.row1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n}\n\n.pointContainer {\n  width: 5%;\n}\n\n.point {\n  margin-bottom: 12px;\n}\n\n.pointText {\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.redDot {\n  width: 8px;\n  height: 8px;\n  border-radius: 4px;\n  background-color: #f36;\n  margin-right: 20px;\n  margin-top: 12px;\n}\n\n.testsImage {\n  width: 72px;\n  height: 72px;\n  margin-top: -8px;\n  margin-right: 24px;\n}\n\n.testsImage img {\n  width: 100%;\n  height: 100%;\n}\n\n.section_container.ashBackground {\n  background-color: #f7f7f7;\n}\n\n.videoSlider {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 80px 64px;\n  padding: 5rem 4rem;\n  background-color: #f7f7f7;\n  position: relative;\n}\n\n.maths {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.microscope {\n  position: absolute;\n  top: 30%;\n  left: 3%;\n}\n\n.triangle {\n  position: absolute;\n  top: 60%;\n  left: 10%;\n}\n\n.scale {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n}\n\n.circle {\n  position: absolute;\n  top: 30%;\n  right: 3%;\n}\n\n.chemistry {\n  position: absolute;\n  top: 60%;\n  right: 10%;\n}\n\n.headerbackgroundmobile {\n  position: absolute;\n  bottom: -56px;\n  right: 0;\n  width: 513px;\n  height: 560px;\n}\n\n.headerbackgroundmobile img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.section_title {\n  display: block;\n  margin: 0 0 16px;\n  font-size: 40px;\n  line-height: 48px;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.sub_section_title {\n  font-size: 28px;\n  line-height: 32px;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 12px;\n  margin-top: 56px;\n}\n\n.contentPart {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 45%;\n}\n\n.contentPart .content {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n  }\n\n.contentPart .content .textcontent {\n      font-size: 16px;\n      line-height: 24px;\n      color: #25282b;\n\n      /* .point {\n        width: 100%;\n        display: flex;\n        align-items: flex-start;\n        height: max-content;\n        margin: 6px 0;\n\n        .reddotwrapper {\n          width: 5%;\n          display: flex;\n          align-items: center;\n          justify-content: flex-start;\n          margin-top: 12px;\n\n          .reddot {\n            width: 8px;\n            height: 8px;\n            background-color: #f36;\n            border-radius: 50%;\n          }\n        }\n\n        .pointText {\n          width: 95%;\n        }\n      } */\n    }\n\n/* .customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 560px;\n\n  .customer_review {\n    order: 0;\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n\n    .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n\n      img {\n        width: 100%;\n        height: 100%;\n        object-fit: contain;\n      }\n    }\n\n    .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n    .authorWrapper {\n      display: flex;\n      flex-direction: column;\n\n      .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n      .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n    }\n\n    .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: flex;\n      flex-direction: row;\n      align-items: center;\n\n      img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        object-fit: contain;\n      }\n    }\n  }\n} */\n\n.availableContainer {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.availableContainer .availableRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.availableContainer .availableRow .desktopImage {\n      width: 696px;\n      height: 371px;\n    }\n\n.joinCommunity {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.joinFbText {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.joinFbLink {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.availableTitle {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.available {\n  color: #f36;\n}\n\n.availableContentSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.platformContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.platform {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.platformOs {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.row {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.store {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.store .playstore {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.store .appstore {\n    width: 136px;\n    height: 40px;\n  }\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.teachContainer {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #f2e5fe;\n  position: relative;\n}\n\n.teachContainer .heading {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n.teachContainer .heading .learningText {\n  width: 316px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n}\n\n.imagePart .emptyCard img {\n  width: 100%;\n  height: 100%;\n  // object-fit: contain;\n}\n\n.imagePart .topCard img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer .heading .learningText img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n}\n\n.teachContainer .contentText {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.teachContainer .buttonwrapper {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .requestDemo {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper img {\n  width: 32px;\n  height: 32px;\n}\n\n.teachContainer .downloadApp {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.teachContainer .downloadApp .downloadText {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.teachContainer .downloadApp .playStoreIcon {\n  width: 132px;\n  height: 40px;\n}\n\n/* .teachContainer .actionsWrapper {\n  position: absolute;\n  bottom: 26px;\n  width: 100%;\n  display: flex;\n}\n.teachContainer .actionsWrapper .action {\n  width: fit-content;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: 24px;\n}\n.teachContainer .actionsWrapper .action span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  opacity: 0.7;\n  text-align: left;\n  margin-left: 8px;\n}\n.teachContainer .actionsWrapper .action .actionimgbox {\n  width: 24px;\n  height: 24px;\n  background-color: #fff;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.teachContainer .actionsWrapper .action .actionimgbox img {\n  width: 10px;\n  height: 9px;\n} */\n\n.teachContainer .breadcrum {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.teachContainer .breadcrum span {\n  font-weight: 600;\n}\n\n.teachContainer .topSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.teachContainer .topSection .teachSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .topSection .teachSection .teachImgBox {\n  width: 40px;\n  height: 40px;\n}\n\n.teachContainer .topSection .teachSection .teachImgBox img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer .topSection .teachSection .teachSectionName {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #8800fe;\n  font-weight: bold;\n}\n\n/* .teachContainer .topSection .featuresSection {\n  display: flex;\n  align-items: center;\n  margin-left: 30%;\n}\n.teachContainer .topSection .featuresSection span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  margin-right: 32px;\n  font-weight: 500;\n} */\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients .clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.displayClients .clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.achievedContainer .achievedRow {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.achievedContainer .achievedRow .card {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n  background-color: #fff3eb;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n  background-color: #f7effe;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n  background-color: #ffebf0;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n  background-color: #ebffef;\n}\n\n.achievedContainer .achievedRow .card .highlight {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n  color: #00ac26;\n}\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n  color: #f36;\n}\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n  color: #8c00fe;\n}\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n  color: #f60;\n}\n\n.achievedContainer .achievedRow .card .subText {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.section_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.section_container .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.section_container_reverse {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.section_container_reverse .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.imagePart {\n  width: 50%;\n}\n\n.imagePart .emptyCard {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  position: relative;\n}\n\n.imagePart .topCard {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.imagePart .bottomCircle {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.imagePart .topCircle {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.imagePart.onlineTests .topCircle {\n  top: -44px;\n  right: 50px;\n  background-color: #f2e5fe;\n}\n\n.imagePart.onlineTests .bottomCircle {\n  bottom: -44px;\n  left: 79px;\n  background-color: #ffe0e8;\n}\n\n.imagePart.ownPapers .topCircle {\n  background-color: #3fc;\n  opacity: 0.3;\n  top: -44px;\n  right: 70px;\n}\n\n.imagePart.ownPapers .bottomCircle {\n  background-color: #ffe0e8;\n  bottom: -44px;\n  left: 40px;\n}\n\n.imagePart.questionBank .topCircle {\n  background-color: #0076ff;\n  opacity: 0.2;\n  top: -44px;\n  right: 70px;\n}\n\n.imagePart.questionBank .bottomCircle {\n  background-color: #feb546;\n  bottom: -44px;\n  left: 40px;\n  opacity: 0.2;\n}\n\n.imagePart .videoContents {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.imagePart .videoContents .video_section {\n  width: 45%;\n  margin-bottom: 56px;\n}\n\n.imagePart .videoContents .video_section .content_title {\n  font-size: 20px;\n  font-weight: bold;\n  line-height: 30px;\n  margin-bottom: 8px;\n  color: #25282b;\n}\n\n.imagePart .videoContents .video_section .content_text {\n  font-size: 16px;\n  line-height: 24px;\n  color: #25282b;\n}\n\n/* .allcustomers p {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n} */\n\n.content p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.hide {\n  display: none;\n}\n\n.show {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n@media only screen and (max-width: 1200px) {\n  .availableContainer {\n    padding: 5rem 64px 0 64px;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .testsImage {\n    width: 48px;\n    height: 48px;\n    margin: auto;\n  }\n\n  .section_title {\n    font-size: 24px;\n    line-height: normal;\n    text-align: center;\n    margin-top: 16px;\n    margin-bottom: 24px;\n  }\n\n  .point {\n    margin-bottom: 8px;\n  }\n\n  .redDot {\n    margin-top: 8px;\n  }\n\n  .pointText {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .imagePart .bottomCircle {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .imagePart .topCircle {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .imagePart.onlineTests .topCircle {\n    top: -24px;\n    right: 50px;\n    background-color: #f2e5fe;\n  }\n\n  .imagePart.onlineTests .bottomCircle {\n    bottom: -24px;\n    left: 79px;\n    background-color: #ffe0e8;\n  }\n\n  .imagePart.ownPapers .topCircle {\n    background-color: #3fc;\n    opacity: 0.3;\n    top: -24px;\n    right: 70px;\n  }\n\n  .imagePart.ownPapers .bottomCircle {\n    background-color: #ffe0e8;\n    bottom: -24px;\n    left: 40px;\n  }\n\n  .imagePart.questionBank .topCircle {\n    background-color: #0076ff;\n    opacity: 0.2;\n    top: -24px;\n    right: 70px;\n  }\n\n  .imagePart.questionBank .bottomCircle {\n    background-color: #feb546;\n    bottom: -24px;\n    left: 40px;\n    opacity: 0.2;\n  }\n\n  .teachContainer .downloadApp {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .section_container {\n    min-height: 20rem;\n    padding: 2rem 1rem;\n  }\n\n  .section_container .maxContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    max-width: 600px;\n  }\n\n  .section_container .section_contents {\n    margin-top: 32px;\n    max-height: none;\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .section_container_reverse {\n    padding: 24px 20px 48px;\n  }\n\n  .section_container_reverse .section_contents {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .contentPart {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding-bottom: 0;\n  }\n\n  .contentPart > div:nth-child(2) {\n    width: 100%;\n    max-width: 400px;\n    margin: auto;\n  }\n\n  .contentPart .content {\n    max-width: none;\n  }\n\n  .contentPart .content .textcontent {\n    display: block;\n    width: 100%;\n    max-width: 600px;\n    text-align: left;\n    font-size: 14px;\n    line-height: 24px;\n    margin: auto;\n  }\n\n  /* .contentPart .content .textcontent .point .reddotwrapper {\n    margin-top: 9px;\n  }\n\n  .contentPart .content .textcontent .point .pointText {\n    text-align: left;\n  } */\n\n  .contentPart .content .section_title {\n    font-size: 24px;\n    text-align: left;\n    margin-bottom: 16px;\n  }\n\n  .imagePart {\n    width: 100%;\n    margin-top: 48px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n  }\n\n  .imagePart .emptyCard {\n    width: 100%;\n    max-width: 360px;\n    height: 180px;\n    margin: 0 auto 32px;\n  }\n\n  .imagePart .videoContents {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .imagePart .videoContents .video_section {\n    width: 100%;\n    margin-bottom: 32px;\n  }\n\n  .imagePart .videoContents .video_section:last-child {\n    margin-bottom: 0;\n  }\n\n  .imagePart .videoContents .video_section .content_title {\n    font-size: 16px;\n    margin-bottom: 12px;\n  }\n\n  .imagePart .videoContents .video_section .content_text {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .mobilePapersContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .mobilePapersContainer img {\n    width: 100%;\n    height: 100%;\n    max-width: 360px;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n  .papersWrapper {\n    max-width: 360px;\n    margin: auto;\n  }\n\n  .reports_sub_title {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .reports_notice {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .sub_section_title {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .headerbackgroundmobile {\n    width: 232px;\n    height: 335px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .displayClients span {\n    max-width: 307px;\n  }\n\n  /* .displayClients .clientsWrapper .dots {\n    display: flex;\n  } */\n  .achievedContainer {\n    padding: 1.5rem 1rem;\n    background: url('/images/home/Confetti.svg');\n  }\n\n  .achievedContainer .achievedHeading {\n    font-size: 1.5rem;\n    padding: 0;\n    margin-bottom: 1.5rem;\n  }\n\n  .achievedContainer .achievedHeading span::after {\n    content: '\\a';\n    white-space: pre;\n  }\n\n  .achievedContainer .achievedRow {\n    grid-template-columns: 1fr;\n    gap: 0.75rem;\n    max-width: 20.5rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: auto;\n  }\n\n  /* .customers_container {\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    width: 100%;\n    align-items: center;\n\n    .customer_review {\n      order: 2;\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n\n      .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n\n      .authorWrapper {\n        .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n      }\n    }\n  } */\n\n  .teachContainer .maxContainer {\n    position: static;\n    max-width: 450px;\n  }\n\n  .teachContainer .breadcrum {\n    display: none;\n    text-align: center;\n  }\n\n  .teachContainer {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .teachContainer .heading {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n    }\n\n      .teachContainer .heading .learningText {\n        width: 210px;\n      }\n\n        .teachContainer .heading .learningText img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .teachContainer .contentText {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .teachContainer .buttonwrapper {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .teachContainer .buttonwrapper .requestDemo {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .teachContainer .buttonwrapper .whatsappwrapper {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .teachContainer .buttonwrapper .whatsappwrapper img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .teachContainer .topSection {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .teachContainer .topSection .teachSection .teachImgBox {\n          width: 32px;\n          height: 32px;\n        }\n\n        .teachContainer .topSection .teachSection .teachSectionName {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .availableContainer {\n    padding: 32px 28px;\n    overflow: hidden;\n  }\n\n    .availableContainer .availableRow {\n      grid-template-columns: 1fr;\n    }\n\n      .availableContainer .availableRow .availableTitle {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .availableContainer .availableRow .row {\n        margin: 0;\n        margin-bottom: 32px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .row .platformContainer {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .availableContainer .availableRow .row .platformContainer .platform {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .availableContainer .availableRow .row .platformContainer .platformOs {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .availableContainer .availableRow .row .platformContainer:last-child {\n          margin-right: 0;\n        }\n\n      .availableContainer .availableRow .desktopImage {\n        width: 90%;\n        height: 161px;\n        margin: 0 auto 0 auto;\n      }\n\n      .availableContainer .availableRow .store {\n        -ms-flex-pack: center;\n            justify-content: center;\n        margin-bottom: 32px;\n      }\n\n        .availableContainer .availableRow .store .playstore {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .availableContainer .availableRow .store .appstore {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .joinCommunity {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .joinFbText {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .joinFbLink {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .videoSlider {\n    padding: 3rem 1rem;\n    min-height: 38rem;\n  }\n\n  .maths {\n    position: absolute;\n    left: 15%;\n    top: 84%;\n    width: 36px;\n    height: 36px;\n  }\n\n  .microscope {\n    position: absolute;\n    top: 87%;\n    left: 45%;\n    width: 40px;\n    height: 48px;\n  }\n\n  .triangle {\n    position: absolute;\n    top: 85%;\n    left: 80%;\n    width: 36px;\n    height: 30px;\n  }\n\n  .scale {\n    position: absolute;\n    right: 80%;\n    top: 10%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .circle {\n    position: absolute;\n    top: 3%;\n    right: 50%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .chemistry {\n    position: absolute;\n    top: 8%;\n    right: 5%;\n    width: 36px;\n    height: 48px;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"maxContainer": "Test-maxContainer-1_QCO",
	"marginBottom126": "Test-marginBottom126-2y-TJ",
	"reports_sub_title": "Test-reports_sub_title-1zU4V",
	"reports_notice": "Test-reports_notice-1k7xY",
	"row1": "Test-row1-1pBIZ",
	"pointContainer": "Test-pointContainer-yl6rU",
	"point": "Test-point-2yHMB",
	"pointText": "Test-pointText-3omy9",
	"redDot": "Test-redDot-3mKH-",
	"testsImage": "Test-testsImage-3Z2de",
	"section_container": "Test-section_container-2wA5m",
	"ashBackground": "Test-ashBackground-q1E-3",
	"videoSlider": "Test-videoSlider-28_ug",
	"maths": "Test-maths-TucEU",
	"microscope": "Test-microscope-HsatG",
	"triangle": "Test-triangle-2BUKY",
	"scale": "Test-scale-1c9I4",
	"circle": "Test-circle-2jW1w",
	"chemistry": "Test-chemistry-2eYOW",
	"headerbackgroundmobile": "Test-headerbackgroundmobile-3FN-6",
	"section_title": "Test-section_title-1u6t0",
	"sub_section_title": "Test-sub_section_title-3VRu3",
	"contentPart": "Test-contentPart-vOxYm",
	"content": "Test-content-2hs9s",
	"textcontent": "Test-textcontent-3rWf1",
	"availableContainer": "Test-availableContainer-3xLT2",
	"availableRow": "Test-availableRow-2Qync",
	"desktopImage": "Test-desktopImage-2kXNX",
	"joinCommunity": "Test-joinCommunity-3LBUs",
	"joinFbText": "Test-joinFbText-1fdhM",
	"joinFbLink": "Test-joinFbLink-Avcsf",
	"availableTitle": "Test-availableTitle-3NEfA",
	"available": "Test-available-3lWLX",
	"availableContentSection": "Test-availableContentSection-UP4ln",
	"platformContainer": "Test-platformContainer-2Ohnt",
	"platform": "Test-platform-kOxAM",
	"platformOs": "Test-platformOs-36PkG",
	"row": "Test-row-sE-Co",
	"store": "Test-store-3Cziz",
	"playstore": "Test-playstore-3PKOC",
	"appstore": "Test-appstore-1hO16",
	"scrollTop": "Test-scrollTop-14S8C",
	"teachContainer": "Test-teachContainer-4lLOo",
	"heading": "Test-heading-1oNs5",
	"learningText": "Test-learningText-1PBX3",
	"imagePart": "Test-imagePart-FumwY",
	"emptyCard": "Test-emptyCard-2MxHA",
	"topCard": "Test-topCard-3A_No",
	"contentText": "Test-contentText-2Hp6y",
	"buttonwrapper": "Test-buttonwrapper-kmrpR",
	"requestDemo": "Test-requestDemo-EyPHF",
	"whatsappwrapper": "Test-whatsappwrapper-3DmTJ",
	"whatsapp": "Test-whatsapp-3-hIr",
	"downloadApp": "Test-downloadApp-h4dCI",
	"downloadText": "Test-downloadText-2KLuA",
	"playStoreIcon": "Test-playStoreIcon-3vbFK",
	"breadcrum": "Test-breadcrum-1j15E",
	"displayClients": "Test-displayClients-18jr5",
	"topSection": "Test-topSection-21bV3",
	"teachSection": "Test-teachSection-gnkFu",
	"teachImgBox": "Test-teachImgBox-32tPP",
	"teachSectionName": "Test-teachSectionName-1fAAr",
	"clientsWrapper": "Test-clientsWrapper-2fDeP",
	"client": "Test-client-FzdBZ",
	"achievedContainer": "Test-achievedContainer-1FrOF",
	"achievedHeading": "Test-achievedHeading-rtdeO",
	"achievedRow": "Test-achievedRow-1lCQf",
	"card": "Test-card-TZZcR",
	"achievedProfile": "Test-achievedProfile-247hX",
	"clients": "Test-clients-2p-cX",
	"students": "Test-students-3C7ru",
	"tests": "Test-tests-2gs17",
	"questions": "Test-questions-3vWWW",
	"highlight": "Test-highlight-RuaYQ",
	"questionsHighlight": "Test-questionsHighlight-2_VNQ",
	"testsHighlight": "Test-testsHighlight-2bD1f",
	"studentsHighlight": "Test-studentsHighlight-3p4cJ",
	"clientsHighlight": "Test-clientsHighlight-yClDo",
	"subText": "Test-subText-2Q8Rs",
	"section_contents": "Test-section_contents-2WASW",
	"section_container_reverse": "Test-section_container_reverse-1IKFo",
	"bottomCircle": "Test-bottomCircle-2SECO",
	"topCircle": "Test-topCircle-2B2HP",
	"onlineTests": "Test-onlineTests-2i4lu",
	"ownPapers": "Test-ownPapers-1Gy0r",
	"questionBank": "Test-questionBank-l-6mN",
	"videoContents": "Test-videoContents-pQqwC",
	"video_section": "Test-video_section-2C2PD",
	"content_title": "Test-content_title-1DOvX",
	"content_text": "Test-content_text-QKFHq",
	"hide": "Test-hide-3vPQU",
	"show": "Test-show-LTOHo",
	"mobilePapersContainer": "Test-mobilePapersContainer-1GPOu",
	"papersWrapper": "Test-papersWrapper-1aINd"
};

/***/ }),

/***/ "./src/components/Slider-Player/SliderPlayer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("react-player");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slider-Player/SliderPlayer.scss");
/* harmony import */ var _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slider-Player/SliderPlayer.js";





const move = {
  0: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut,
  1: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut1,
  2: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut2,
  3: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut3
};

const SliderPlayer = props => {
  const getInnerWidth = () => {
    const isWindowAvailable = typeof window !== 'undefined';

    if (isWindowAvailable) {
      return window.innerWidth;
    }

    return 1000;
  };

  const [active, setActive] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0);
  const [seekValue, setSeekValue] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0);
  const [isMobile, setIsMobile] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const playerRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const [videos, setVideos] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([...props.videosList]); // eslint-disable-line

  const {
    videosList
  } = props;
  const activeVideoObj = isMobile ? videos[active] : videosList[active];
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const resize = () => {
      setIsMobile(getInnerWidth() < 991);
    };

    resize();
    window.addEventListener('resize', resize);
    return () => {
      window.removeEventListener('resize', resize);
    };
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_2___default.a, {
    ref: playerRef,
    width: isMobile ? '100%' : '500px',
    height: isMobile ? '267px' : '400px',
    style: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.video,
    url: activeVideoObj.url,
    controls: false,
    config: {
      file: {
        attributes: {
          autoPlay: true,
          muted: true,
          disablePictureInPicture: true
        }
      }
    },
    onProgress: state => {
      const seek = Math.ceil(state.playedSeconds * 100 / playerRef.current.getDuration());
      setSeekValue(seek);
    },
    onEnded: () => {
      setSeekValue(100);

      if (isMobile) {
        const titles = document.getElementsByClassName(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle); // const lastOne = videos.shift();
        // videos.push(lastOne);

        setSeekValue(0);
        const prevActive = active;
        const currentActive = active + 1 === 4 ? 0 : active + 1;
        setActive(currentActive);
        Array.from(titles).forEach(ele => {
          ele.classList.add(move[currentActive]);
          ele.classList.remove(move[prevActive]);
        });
      } else {
        const isLast = active === videosList.length - 1;

        if (isLast) {
          setSeekValue(0);
          setActive(0);
        } else {
          setSeekValue(0);
          setActive(active + 1);
        }
      }
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.controlsWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: undefined
  }, isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileControls,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `${_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.outer} ${_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileTracker}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.inner,
    style: {
      width: `${seekValue}%`
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: undefined
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileTitlesWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: undefined
  }, videos.map((video, index) => {
    const act = active === index;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: act ? `${_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle} ${_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.actTitle}` : `${_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: undefined
    }, video.title);
  }))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.desktopControls,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: undefined
  }, videosList.map((videoObj, index) => {
    const currentSeek = index === active ? seekValue : 0;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.controls,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: undefined
    }, !isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.outer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: undefined
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.inner,
      style: {
        width: `${currentSeek}%`
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: undefined
    })) : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: undefined
    }, videoObj.title));
  }))));
};

SliderPlayer.propTypes = {
  videosList: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string]))
};
SliderPlayer.defaultProps = {
  videosList: []
};
/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(SliderPlayer));

/***/ }),

/***/ "./src/components/Slider-Player/SliderPlayer.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slider-Player/SliderPlayer.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/Test.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("isomorphic-style-loader/lib/withStyles");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Slider_Player_SliderPlayer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Slider-Player/SliderPlayer.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/Test/constants.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Test_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/get-ranks/Test/Test.scss");
/* harmony import */ var _Test_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Test_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/Test.js";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









class Test extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  // Toggle Button Code

  /* displayToggle = () => (
    <div
      role="presentation"
      className={
        this.state.liveToggle
          ? `${s.toggle_outer} ${s.toggle_on}`
          : s.toggle_outer
      }
    >
      <div className={s.toggle_inner} />
    </div>
  ); */
  constructor(props) {
    super(props);

    _defineProperty(this, "handleResize", () => {
      if (window.innerWidth < 991) {
        this.setState({
          isMobile: true
        });
      } else {
        this.setState({
          isMobile: false
        });
      }
    });

    _defineProperty(this, "handleScroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          showScroll: true
        });
      } else {
        this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(this, "handleScrollTop", () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      this.setState({
        showScroll: false
      });
    });

    _defineProperty(this, "displayTeachSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.breadcrum,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: this
    }, "Home / ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: this
    }, "Online Tests")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachImgBox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/New SubMenu Items/Test/test.svg",
      alt: "teach",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachSectionName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, "Online Tests"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.heading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }, "Plan, design, deliver and mark\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.learningText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, "assessments"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/live classes/underscore_for_title.png",
      alt: "underscore",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    })), "\xA0seamlessly - onsite or remotely."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, "An end-to-end assessment platform that support teachers, data entry operators with cutting edge features that cover all your assessment needs entirely on-screen."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.buttonwrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.requestDemo,
      onClick: this.handleShowTrial,
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, "GET SUBSCRIPTION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.whatsappwrapper,
      href: "https://api.whatsapp.com/send?phone=918800764909&text=Hi GetRanks, I would like to know more about your Online Platform.",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.whatsapp,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/whatsapp_logo.svg",
      alt: "whatsapp",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.downloadApp,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.downloadText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    }, "Download the app"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.playStoreIcon,
      src: "/images/home/platforms/playStore.webp",
      alt: "play_store",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headerbackgroundmobile,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/hero_img.webp",
      alt: "mobile_background",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 170
      },
      __self: this
    })))));

    _defineProperty(this, "displayClients", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.displayClients,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 177
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 178
      },
      __self: this
    }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179
      },
      __self: this
    }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clientsWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 182
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_CLIENTS_UPDATED"].map(client => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.client,
      key: client.id,
      src: client.icon,
      alt: "clinet",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 184
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 192
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
      to: "/customers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 193
      },
      __self: this
    }, "click here for more..."))));

    _defineProperty(this, "displayAchievedSoFar", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 199
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 200
      },
      __self: this
    }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 201
      },
      __self: this
    }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedRow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clients}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 209
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/clients.svg",
      alt: "profile",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 210
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clientsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 212
      },
      __self: this
    }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213
      },
      __self: this
    }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.students}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 219
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/students.svg",
      alt: "students",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 220
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.studentsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 222
      },
      __self: this
    }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 225
      },
      __self: this
    }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 227
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.tests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 231
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/tests.svg",
      alt: "tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 232
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 234
      },
      __self: this
    }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 237
      },
      __self: this
    }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 239
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questions}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 243
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/questions.svg",
      alt: "questions",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 244
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questionsHighlight}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 249
      },
      __self: this
    }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 252
      },
      __self: this
    }, "Questions Solved")))));

    _defineProperty(this, "displayPlayerSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoSlider,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 259
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slider_Player_SliderPlayer__WEBPACK_IMPORTED_MODULE_3__["default"], {
      videosList: _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["TEST_VIDEOS"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 260
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maths,
      src: "/images/icons/subject_icons/maths.svg",
      alt: "maths",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 261
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.microscope,
      src: "/images/icons/subject_icons/microscope.svg",
      alt: "microscope",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 266
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.triangle,
      src: "/images/icons/subject_icons/triangle.svg",
      alt: "triangle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 271
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.scale,
      src: "/images/icons/subject_icons/scale.svg",
      alt: "scale",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 276
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.circle,
      src: "/images/icons/subject_icons/circle.svg",
      alt: "circle",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 281
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.chemistry,
      src: "/images/icons/subject_icons/chemistry.svg",
      alt: "chemistry",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 286
      },
      __self: this
    })));

    _defineProperty(this, "displayOnlineTests", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 295
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 296
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 297
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 298
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 299
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/Tests_HeadSection.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 300
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 306
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 307
      },
      __self: this
    }, "Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 308
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_4__["onlineTestPoints"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 310
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 311
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 312
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 314
      },
      __self: this
    }, point.text)))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 320
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 321
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 322
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/online-tests.webp",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 323
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 325
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 326
      },
      __self: this
    }))))));

    _defineProperty(this, "displayOwnPapers", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 334
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 335
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 336
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 337
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 338
      },
      __self: this
    }, "Upload your own Question papers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.papersWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 341
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_4__["ownPapers"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 343
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 344
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 345
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 347
      },
      __self: this
    }, point.text)))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ownPapers}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 353
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 354
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 355
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/own-papers.webp",
      alt: "own-papers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 356
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 358
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 359
      },
      __self: this
    }))))));

    _defineProperty(this, "displayQuestionBank", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 367
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 368
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 369
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 370
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 371
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/Tests_HeadSection.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 372
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 378
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 379
      },
      __self: this
    }, "Inbuilt Questions Bank "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 380
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_4__["questionBank"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 382
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 383
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 384
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 386
      },
      __self: this
    }, point.text)))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questionBank}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 392
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 393
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 394
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/questionBank.webp",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 395
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 397
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 398
      },
      __self: this
    }))))));

    _defineProperty(this, "displayPapers", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 406
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 407
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 408
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 409
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 410
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/Analysis_HeadSection.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 411
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 417
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 418
      },
      __self: this
    }, "Papers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 419
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_4__["papers"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 421
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 422
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 423
      },
      __self: this
    })), point.subPoints ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 426
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 427
      },
      __self: this
    }, point.text), point.subPoints.map(text => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 429
      },
      __self: this
    }, text))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 433
      },
      __self: this
    }, point.text)))))), !this.state.isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 441
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 442
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 443
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/analysis-card.svg",
      alt: "papers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 444
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 446
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 447
      },
      __self: this
    }))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mobilePapersContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 451
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/mobile_papers.svg",
      alt: "moble_papers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 452
      },
      __self: this
    })))));

    _defineProperty(this, "displayAnalysis", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 460
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 461
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 462
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 463
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 464
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/Analysis_HeadSection.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 465
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 471
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 472
      },
      __self: this
    }, "Analysis"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 473
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 474
      },
      __self: this
    }, "Marks-based Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["marksBasedAnalysis"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 476
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 477
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 478
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 480
      },
      __self: this
    }, point.text))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 483
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126} ${this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 484
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 489
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/marksAnalysis.webp",
      alt: "marks-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 490
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 495
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 496
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 499
      },
      __self: this
    }, "Comparision Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["comparisionAnalysis"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 501
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 502
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 503
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 505
      },
      __self: this
    }, point.text))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 508
      },
      __self: this
    }, "Error Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["errorAnalysis"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 510
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 511
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 512
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 514
      },
      __self: this
    }, point.text))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 517
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126} ${this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 518
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 523
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/conceptAnalysis.webp",
      alt: "concept-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 524
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 529
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 530
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 533
      },
      __self: this
    }, "Concept-based Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["conceptAnalysis"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 535
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 536
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 537
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 539
      },
      __self: this
    }, point.text))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 542
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126} ${this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 543
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 548
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/errorAnalysis.webp",
      alt: "error-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 549
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 554
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 555
      },
      __self: this
    })))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests} ${this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 561
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 566
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 567
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/marksAnalysis.webp",
      alt: "marks-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 568
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 570
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 571
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 573
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 574
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/conceptAnalysis.webp",
      alt: "concept-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 575
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 580
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 581
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 583
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 584
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/errorAnalysis.webp",
      alt: "error-analysis",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 585
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 587
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 588
      },
      __self: this
    }))))));

    _defineProperty(this, "displayReports", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 596
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 597
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 598
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 599
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 600
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/Reports_HeadSection.svg",
      alt: "online-tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 601
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 607
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 608
      },
      __self: this
    }, "Reports"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.reports_sub_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 609
      },
      __self: this
    }, "9 Informative Reports"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.reports_notice,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 610
      },
      __self: this
    }, "(Downloadable for Notice Board)"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 613
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_4__["reports"].map(point => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 615
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 616
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 617
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 619
      },
      __self: this
    }, point.text)))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 625
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 626
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 627
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/reports.webp",
      alt: "reports",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 628
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 630
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 631
      },
      __self: this
    }))))));

    _defineProperty(this, "displayTestsSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 639
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 640
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 641
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 642
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 643
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 646
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} custom-scrollbar`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 654
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 655
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/demo.png",
      alt: "demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 656
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 658
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 659
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 660
      },
      __self: this
    }, "Independence"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 661
      },
      __self: this
    }, "Your Zoom account will be used. All the features and provisions of your Zoom account shall be available with an added advantage of login restrictions only for selected students.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 667
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 668
      },
      __self: this
    }, "Flexibility"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 669
      },
      __self: this
    }, "Any number of Zoom accounts can be used. Different Zoom accounts can be mapped with different Class Sessions thus allowing multiple teachers to use their personal accounts.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 675
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 676
      },
      __self: this
    }, "Secure Access"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 677
      },
      __self: this
    }, "Only selected students who have access to the App can now access Zoom class, no need to share Meeting ID everytime, No random access.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 683
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 684
      },
      __self: this
    }, "Multiple uses of Zoom"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 685
      },
      __self: this
    }, "Zoom facility available for live Class and Doubts modules. Soon Zoom is being integrated to the online Exam module for online Exam proctoring.")))))));

    _defineProperty(this, "displayAnalysisSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ashBackground}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 699
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 700
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 701
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 702
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 703
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 706
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} custom-scrollbar`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 714
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 715
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/demo.png",
      alt: "demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 716
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 718
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 719
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 720
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 723
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 730
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 731
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 734
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 741
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 742
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 745
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 752
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 753
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 756
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")))))));

    _defineProperty(this, "displayPractice", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 770
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 771
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 772
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 773
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 774
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 777
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} custom-scrollbar`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 796
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 797
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/demo.png",
      alt: "demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 798
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 800
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 801
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 802
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 805
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 812
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 813
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 816
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 823
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 824
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 827
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 834
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 835
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 838
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")))))));

    _defineProperty(this, "displayPapersSection", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ashBackground}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 851
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents} ${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer}`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 852
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 853
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 854
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 855
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 858
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `${_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart} custom-scrollbar`,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 877
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 878
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/Test/demo.png",
      alt: "demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 879
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 881
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 882
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 883
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 886
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 893
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 894
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 897
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 904
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 905
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 908
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 915
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 916
      },
      __self: this
    }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 919
      },
      __self: this
    }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")))))));

    _defineProperty(this, "displayAvailableOn", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 932
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableRow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 933
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableContentSection,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 934
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 935
      },
      __self: this
    }, "We\u2019re ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 936
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.available,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 937
      },
      __self: this
    }, "available"), " on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 939
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PLATFORMS"].map(item => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platformContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 941
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.icon,
      alt: "",
      height: "40px",
      width: "40px",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 942
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platform,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 943
      },
      __self: this
    }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platformOs,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 944
      },
      __self: this
    }, item.available)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.store,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 948
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
      target: "_blank",
      rel: "noopener noreferrer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 949
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/platforms/playStore.webp",
      alt: "Play Store",
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.playstore,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 954
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/platforms/appStore.webp",
      alt: "App Store",
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.appstore,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 960
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.desktopImage,
      src: "/images/home/platforms/platforms_v2.webp",
      alt: "platform",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 968
      },
      __self: this
    }))));

    _defineProperty(this, "displayJoinFacebook", () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinCommunity,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 978
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinFbText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 979
      },
      __self: this
    }, "Join our Facebook Community"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinFbLink,
      href: "https://www.facebook.com/Egnify/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 980
      },
      __self: this
    }, "Join Now")));

    _defineProperty(this, "displayScrollToTop", () => {
      const {
        showScroll
      } = this.state;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.scrollTop,
        role: "presentation",
        onClick: () => {
          this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 990
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 997
        },
        __self: this
      }));
    });

    this.state = {
      showScroll: false,
      isMobile: false
    };
  }

  componentDidMount() {
    this.handleScroll();
    this.handleResize();
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleResize);
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 1005
      },
      __self: this
    }, this.displayTeachSection(), this.displayClients(), this.displayAchievedSoFar(), this.displayOnlineTests(), this.displayOwnPapers(), this.displayQuestionBank(), this.displayPapers(), this.displayAnalysis(), this.displayReports(), this.displayAvailableOn(), this.displayJoinFacebook(), this.displayScrollToTop());
  }

}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Test));

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/Test.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Test/Test.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onlineTestPoints", function() { return onlineTestPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ownPapers", function() { return ownPapers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "questionBank", function() { return questionBank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "marksBasedAnalysis", function() { return marksBasedAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "comparisionAnalysis", function() { return comparisionAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorAnalysis", function() { return errorAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conceptAnalysis", function() { return conceptAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "papers", function() { return papers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reports", function() { return reports; });
const onlineTestPoints = [{
  text: 'Unlimitted test creation'
}, {
  text: 'Instant test result and Reports available online & offline - Rank list, Analysis, and Reports after every test.'
}, {
  text: 'Pre-defined marking schemes'
}, {
  text: 'Custom marking schemes'
}, {
  text: 'Getranks supports all 22 types of JEE avanced Marking Schemes starting from 2009 - 2019'
}, {
  text: 'Grace period option available'
}, {
  text: 'Live Attendance'
}];
const ownPapers = [{
  text: 'Upload your question paper online from word document within 5 mins with a single click'
}, {
  text: 'Getranks Parser can convert text / images / equations / graphs into your questions.'
}];
const questionBank = [{
  text: 'Question bank management system with 1.5 Lakh Questions organized by topic and difficulty for JEE, NEET, EAMCET and IPE.'
}, {
  text: 'All Types of Questions as per latest pattern'
}, {
  text: 'Questions are provided with Answer Key and Step by Step Detail Solution.'
}];
const marksBasedAnalysis = [{
  text: 'Marks distribution graph'
}, {
  text: 'Cut-off based analysis'
}, {
  text: 'Student-wise marks analysis'
}];
const comparisionAnalysis = [{
  text: 'Historic test averages'
}, {
  text: 'Student performance profile'
}, {
  text: 'Subject top marks vs topper'
}];
const errorAnalysis = [{
  text: 'Question error count'
}, {
  text: 'Students error mapping'
}, {
  text: 'Question paper referencing'
}];
const conceptAnalysis = [{
  text: 'Topic based analysis'
}, {
  text: 'Sub-topic based analysis'
}, {
  text: 'Difficulty level analysis'
}];
const papers = [{
  text: 'More than 1,50,000+ ready to use Comprehensive Question Bank and each question has been set with appropriate Difficulty Level.'
}, {
  text: 'All Types of Questions as per latest pattern:',
  subPoints: ['a)	Single correct, Multiple correct questions, Integer type, Matrix type,', 'b)	Assertion Reasoning and Paragraph Type']
}, {
  text: 'Design personalized question paper/s with Institute Name, Logo & Watermark'
}, {
  text: 'Questions are provided with Answer Key and Step by Step Detail Solution.'
}];
const reports = [{
  text: 'Test results'
}, {
  text: 'Student response report'
}, {
  text: 'Student response count'
}, {
  text: 'Marks distribution'
}, {
  text: 'Error count'
}, {
  text: 'Top marks'
}, {
  text: 'Student performance trend'
}, {
  text: 'Averages comparison'
}, {
  text: 'Concept based report'
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Test__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/Test/Test.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/index.js";




async function action() {
  return {
    title: 'Egnify teaching made easy',
    chunks: ['Tests'],
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      footerAsh: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Test__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }))
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

};;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2h1bmtzL1Rlc3RzLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvU2xpZGVyLVBsYXllci9TbGlkZXJQbGF5ZXIuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZXN0L1Rlc3Quc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9TbGlkZXItUGxheWVyL1NsaWRlclBsYXllci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9TbGlkZXItUGxheWVyL1NsaWRlclBsYXllci5zY3NzP2M1OTIiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVzdC9UZXN0LmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1Rlc3QvVGVzdC5zY3NzP2VjOTMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVzdC9jb25zdGFudHMuanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVzdC9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlNsaWRlclBsYXllci1yb290LTFQSjV4IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4uU2xpZGVyUGxheWVyLXZpZGVvLTM5RDRvIHtcXG4gIHdpZHRoOiA0MDBweDtcXG4gIGhlaWdodDogNDAwcHg7XFxufVxcblxcbi5TbGlkZXJQbGF5ZXItZGVza3RvcENvbnRyb2xzLTM2TkoyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLlNsaWRlclBsYXllci1jb250cm9sc1dyYXBwZXItbFhPSGEge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgbWFyZ2luLXRvcDogOTZweDtcXG59XFxuXFxuLlNsaWRlclBsYXllci1jb250cm9scy0xX3VJaiB7XFxuICB3aWR0aDogMjQwcHg7XFxuICBtYXJnaW46IDAgMTZweDtcXG4gIG1hcmdpbjogMCAxcmVtO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59XFxuXFxuLlNsaWRlclBsYXllci1vdXRlci1VOVR1ZCB7XFxuICB3aWR0aDogMjQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDhkOGQ4O1xcbiAgaGVpZ2h0OiA0cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxufVxcblxcbi5TbGlkZXJQbGF5ZXItaW5uZXItM0FYc2Uge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgaGVpZ2h0OiA0cHg7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgei1pbmRleDogMTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjFzIGVhc2U7XFxuICAtby10cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlO1xcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXMgZWFzZTtcXG59XFxuXFxuLlNsaWRlclBsYXllci12aWRlb1RpdGxlLTIyWHhEIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgbWFyZ2luLXRvcDogMTJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW8tMzlENG8ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xcbiAgICBtYXgtaGVpZ2h0OiAzMDBweDtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItbW9iaWxlVGl0bGVzV3JhcHBlci0xeXJhbiB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzMzBweDtcXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci1tb2JpbGVDb250cm9scy0xaDZvNyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci12aWRlb1RpdGxlLTIyWHhEIHtcXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgICBtaW4td2lkdGg6IDEwMHB4O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIC1vLXRyYW5zaXRpb246IHRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXMgZWFzZTtcXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzIGVhc2UsIC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLXZpZGVvVGl0bGUtMjJYeEQuU2xpZGVyUGxheWVyLWFjdFRpdGxlLTMzNTB5IHtcXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLXZpZGVvVGl0bGUtMjJYeEQuU2xpZGVyUGxheWVyLXNsaWRlT3V0LXpmWTBoIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RC5TbGlkZXJQbGF5ZXItc2xpZGVPdXQxLTJpQ3YwIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RC5TbGlkZXJQbGF5ZXItc2xpZGVPdXQyLTRuejZpIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTIxMCUpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMjEwJSk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0yMTAlKTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RC5TbGlkZXJQbGF5ZXItc2xpZGVPdXQzLTFDX2ZvIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTMyMCUpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMzIwJSk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0zMjAlKTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItY29udHJvbHNXcmFwcGVyLWxYT0hhIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogMzMwcHg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci1jb250cm9scy0xX3VJaiB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gICAgbWFyZ2luOiAwIDEycHggMCAwO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci1kZXNrdG9wQ29udHJvbHMtMzZOSjIge1xcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLW91dGVyLVU5VHVkIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLW91dGVyLVU5VHVkLlNsaWRlclBsYXllci1tb2JpbGVUcmFja2VyLW1JTGFiIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9TbGlkZXItUGxheWVyL1NsaWRlclBsYXllci5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QiwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztDQUNmOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IscUJBQXFCO01BQ2pCLDRCQUE0QjtDQUNqQzs7QUFFRDtFQUNFLGFBQWE7RUFDYiwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixPQUFPO0VBQ1AsUUFBUTtFQUNSLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsa0NBQWtDO0VBQ2xDLDZCQUE2QjtFQUM3QiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLDhDQUE4QztJQUM5QyxzQ0FBc0M7SUFDdEMsaUNBQWlDO0lBQ2pDLDhCQUE4QjtJQUM5Qix5REFBeUQ7R0FDMUQ7O0VBRUQ7SUFDRSxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxpQ0FBaUM7UUFDN0IsNkJBQTZCO1lBQ3pCLHlCQUF5QjtHQUNsQzs7RUFFRDtJQUNFLHFDQUFxQztRQUNqQyxpQ0FBaUM7WUFDN0IsNkJBQTZCO0dBQ3RDOztFQUVEO0lBQ0UscUNBQXFDO1FBQ2pDLGlDQUFpQztZQUM3Qiw2QkFBNkI7R0FDdEM7O0VBRUQ7SUFDRSxxQ0FBcUM7UUFDakMsaUNBQWlDO1lBQzdCLDZCQUE2QjtHQUN0Qzs7RUFFRDtJQUNFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkO0NBQ0ZcIixcImZpbGVcIjpcIlNsaWRlclBsYXllci5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5yb290IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4udmlkZW8ge1xcbiAgd2lkdGg6IDQwMHB4O1xcbiAgaGVpZ2h0OiA0MDBweDtcXG59XFxuXFxuLmRlc2t0b3BDb250cm9scyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5jb250cm9sc1dyYXBwZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgbWFyZ2luLXRvcDogOTZweDtcXG59XFxuXFxuLmNvbnRyb2xzIHtcXG4gIHdpZHRoOiAyNDBweDtcXG4gIG1hcmdpbjogMCAxNnB4O1xcbiAgbWFyZ2luOiAwIDFyZW07XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbn1cXG5cXG4ub3V0ZXIge1xcbiAgd2lkdGg6IDI0MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4ZDhkODtcXG4gIGhlaWdodDogNHB4O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbn1cXG5cXG4uaW5uZXIge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgaGVpZ2h0OiA0cHg7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgei1pbmRleDogMTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjFzIGVhc2U7XFxuICAtby10cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlO1xcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXMgZWFzZTtcXG59XFxuXFxuLnZpZGVvVGl0bGUge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLnZpZGVvIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiA0MDBweDtcXG4gICAgbWF4LWhlaWdodDogMzAwcHg7XFxuICB9XFxuXFxuICAubW9iaWxlVGl0bGVzV3JhcHBlciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzMzBweDtcXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbiAgfVxcblxcbiAgLm1vYmlsZUNvbnRyb2xzIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAudmlkZW9UaXRsZSB7XFxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTJweDtcXG4gICAgbWluLXdpZHRoOiAxMDBweDtcXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgICAtby10cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXMgZWFzZTtcXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAxcyBlYXNlLCAtd2Via2l0LXRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgfVxcblxcbiAgLnZpZGVvVGl0bGUuYWN0VGl0bGUge1xcbiAgICBmb250LXdlaWdodDogYm9sZDtcXG4gIH1cXG5cXG4gIC52aWRlb1RpdGxlLnNsaWRlT3V0IHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcXG4gIH1cXG5cXG4gIC52aWRlb1RpdGxlLnNsaWRlT3V0MSB7XFxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XFxuICB9XFxuXFxuICAudmlkZW9UaXRsZS5zbGlkZU91dDIge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMjEwJSk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0yMTAlKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTIxMCUpO1xcbiAgfVxcblxcbiAgLnZpZGVvVGl0bGUuc2xpZGVPdXQzIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTMyMCUpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMzIwJSk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0zMjAlKTtcXG4gIH1cXG5cXG4gIC5jb250cm9sc1dyYXBwZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzMzBweDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICB9XFxuXFxuICAuY29udHJvbHMge1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICAgIG1hcmdpbjogMCAxMnB4IDAgMDtcXG4gIH1cXG5cXG4gIC5kZXNrdG9wQ29udHJvbHMge1xcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XFxuICB9XFxuXFxuICAub3V0ZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5vdXRlci5tb2JpbGVUcmFja2VyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInJvb3RcIjogXCJTbGlkZXJQbGF5ZXItcm9vdC0xUEo1eFwiLFxuXHRcInZpZGVvXCI6IFwiU2xpZGVyUGxheWVyLXZpZGVvLTM5RDRvXCIsXG5cdFwiZGVza3RvcENvbnRyb2xzXCI6IFwiU2xpZGVyUGxheWVyLWRlc2t0b3BDb250cm9scy0zNk5KMlwiLFxuXHRcImNvbnRyb2xzV3JhcHBlclwiOiBcIlNsaWRlclBsYXllci1jb250cm9sc1dyYXBwZXItbFhPSGFcIixcblx0XCJjb250cm9sc1wiOiBcIlNsaWRlclBsYXllci1jb250cm9scy0xX3VJalwiLFxuXHRcIm91dGVyXCI6IFwiU2xpZGVyUGxheWVyLW91dGVyLVU5VHVkXCIsXG5cdFwiaW5uZXJcIjogXCJTbGlkZXJQbGF5ZXItaW5uZXItM0FYc2VcIixcblx0XCJ2aWRlb1RpdGxlXCI6IFwiU2xpZGVyUGxheWVyLXZpZGVvVGl0bGUtMjJYeERcIixcblx0XCJtb2JpbGVUaXRsZXNXcmFwcGVyXCI6IFwiU2xpZGVyUGxheWVyLW1vYmlsZVRpdGxlc1dyYXBwZXItMXlyYW5cIixcblx0XCJtb2JpbGVDb250cm9sc1wiOiBcIlNsaWRlclBsYXllci1tb2JpbGVDb250cm9scy0xaDZvN1wiLFxuXHRcImFjdFRpdGxlXCI6IFwiU2xpZGVyUGxheWVyLWFjdFRpdGxlLTMzNTB5XCIsXG5cdFwic2xpZGVPdXRcIjogXCJTbGlkZXJQbGF5ZXItc2xpZGVPdXQtemZZMGhcIixcblx0XCJzbGlkZU91dDFcIjogXCJTbGlkZXJQbGF5ZXItc2xpZGVPdXQxLTJpQ3YwXCIsXG5cdFwic2xpZGVPdXQyXCI6IFwiU2xpZGVyUGxheWVyLXNsaWRlT3V0Mi00bno2aVwiLFxuXHRcInNsaWRlT3V0M1wiOiBcIlNsaWRlclBsYXllci1zbGlkZU91dDMtMUNfZm9cIixcblx0XCJtb2JpbGVUcmFja2VyXCI6IFwiU2xpZGVyUGxheWVyLW1vYmlsZVRyYWNrZXItbUlMYWJcIlxufTsiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlRlc3QtbWF4Q29udGFpbmVyLTFfUUNPIHtcXG4gIG1heC13aWR0aDogMTcwMHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZXN0LW1hcmdpbkJvdHRvbTEyNi0yeS1USiB7XFxuICBtYXJnaW4tYm90dG9tOiAxMjZweDtcXG59XFxuXFxuLlRlc3QtcmVwb3J0c19zdWJfdGl0bGUtMXpVNFYge1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLXRvcDogMTJweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uVGVzdC1yZXBvcnRzX25vdGljZS0xazd4WSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbWFyZ2luLXRvcDogNHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLlRlc3Qtcm93MS0xcEJJWiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbn1cXG5cXG4uVGVzdC1wb2ludENvbnRhaW5lci15bDZyVSB7XFxuICB3aWR0aDogNSU7XFxufVxcblxcbi5UZXN0LXBvaW50LTJ5SE1CIHtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxufVxcblxcbi5UZXN0LXBvaW50VGV4dC0zb215OSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG59XFxuXFxuLlRlc3QtcmVkRG90LTNtS0gtIHtcXG4gIHdpZHRoOiA4cHg7XFxuICBoZWlnaHQ6IDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbn1cXG5cXG4uVGVzdC10ZXN0c0ltYWdlLTNaMmRlIHtcXG4gIHdpZHRoOiA3MnB4O1xcbiAgaGVpZ2h0OiA3MnB4O1xcbiAgbWFyZ2luLXRvcDogLThweDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuXFxuLlRlc3QtdGVzdHNJbWFnZS0zWjJkZSBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5UZXN0LXNlY3Rpb25fY29udGFpbmVyLTJ3QTVtLlRlc3QtYXNoQmFja2dyb3VuZC1xMUUtMyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uVGVzdC12aWRlb1NsaWRlci0yOF91ZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDgwcHggNjRweDtcXG4gIHBhZGRpbmc6IDVyZW0gNHJlbTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5UZXN0LW1hdGhzLVR1Y0VVIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGxlZnQ6IDEwJTtcXG4gIHRvcDogNSU7XFxufVxcblxcbi5UZXN0LW1pY3Jvc2NvcGUtSHNhdEcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAzMCU7XFxuICBsZWZ0OiAzJTtcXG59XFxuXFxuLlRlc3QtdHJpYW5nbGUtMkJVS1kge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA2MCU7XFxuICBsZWZ0OiAxMCU7XFxufVxcblxcbi5UZXN0LXNjYWxlLTFjOUk0IHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHJpZ2h0OiAxMCU7XFxuICB0b3A6IDUlO1xcbn1cXG5cXG4uVGVzdC1jaXJjbGUtMmpXMXcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAzMCU7XFxuICByaWdodDogMyU7XFxufVxcblxcbi5UZXN0LWNoZW1pc3RyeS0yZVlPVyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDYwJTtcXG4gIHJpZ2h0OiAxMCU7XFxufVxcblxcbi5UZXN0LWhlYWRlcmJhY2tncm91bmRtb2JpbGUtM0ZOLTYge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAtNTZweDtcXG4gIHJpZ2h0OiAwO1xcbiAgd2lkdGg6IDUxM3B4O1xcbiAgaGVpZ2h0OiA1NjBweDtcXG59XFxuXFxuLlRlc3QtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0zRk4tNiBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbi5UZXN0LXNlY3Rpb25fdGl0bGUtMXU2dDAge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgMCAxNnB4O1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5UZXN0LXN1Yl9zZWN0aW9uX3RpdGxlLTNWUnUzIHtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gIG1hcmdpbi10b3A6IDU2cHg7XFxufVxcblxcbi5UZXN0LWNvbnRlbnRQYXJ0LXZPeFltIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICB3aWR0aDogNDUlO1xcbn1cXG5cXG4uVGVzdC1jb250ZW50UGFydC12T3hZbSAuVGVzdC1jb250ZW50LTJoczlzIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1pbi13aWR0aDogMjkwcHg7XFxuICAgIG1heC13aWR0aDogNTQwcHg7XFxuICB9XFxuXFxuLlRlc3QtY29udGVudFBhcnQtdk94WW0gLlRlc3QtY29udGVudC0yaHM5cyAuVGVzdC10ZXh0Y29udGVudC0zcldmMSB7XFxuICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIGNvbG9yOiAjMjUyODJiO1xcblxcbiAgICAgIC8qIC5wb2ludCB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgICAgIGhlaWdodDogbWF4LWNvbnRlbnQ7XFxuICAgICAgICBtYXJnaW46IDZweCAwO1xcblxcbiAgICAgICAgLnJlZGRvdHdyYXBwZXIge1xcbiAgICAgICAgICB3aWR0aDogNSU7XFxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgICAgICAgbWFyZ2luLXRvcDogMTJweDtcXG5cXG4gICAgICAgICAgLnJlZGRvdCB7XFxuICAgICAgICAgICAgd2lkdGg6IDhweDtcXG4gICAgICAgICAgICBoZWlnaHQ6IDhweDtcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgICAgfVxcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnBvaW50VGV4dCB7XFxuICAgICAgICAgIHdpZHRoOiA5NSU7XFxuICAgICAgICB9XFxuICAgICAgfSAqL1xcbiAgICB9XFxuXFxuLyogLmN1c3RvbWVyc19jb250YWluZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGhlaWdodDogNTYwcHg7XFxuXFxuICAuY3VzdG9tZXJfcmV2aWV3IHtcXG4gICAgb3JkZXI6IDA7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICBwYWRkaW5nOiA0OHB4IDY0cHg7XFxuICAgIHdpZHRoOiA1MCU7XFxuXFxuICAgIC5jdXN0b21lckxvZ28ge1xcbiAgICAgIHdpZHRoOiAxMjBweDtcXG4gICAgICBoZWlnaHQ6IDgwcHg7XFxuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuXFxuICAgICAgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuICAgIH1cXG5cXG4gICAgLnNyaUNoYWl0YW55YVRleHQge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgfVxcblxcbiAgICAuYXV0aG9yV3JhcHBlciB7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcblxcbiAgICAgIC5hdXRob3JfdGl0bGUge1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5hYm91dF9hdXRob3Ige1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA2NHB4O1xcbiAgICAgIH1cXG4gICAgfVxcblxcbiAgICAuYWxsY3VzdG9tZXJzIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG5cXG4gICAgICBpbWcge1xcbiAgICAgICAgbWFyZ2luLXRvcDogM3B4O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gICAgICAgIHdpZHRoOiAyMHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuICAgIH1cXG4gIH1cXG59ICovXFxuXFxuLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIHtcXG4gIHBhZGRpbmc6IDgwcHggMTEzcHggNTZweCAxNjRweDtcXG4gIHBhZGRpbmc6IDVyZW0gMTEzcHggNTZweCAxNjRweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIHtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LWRlc2t0b3BJbWFnZS0ya1hOWCB7XFxuICAgICAgd2lkdGg6IDY5NnB4O1xcbiAgICAgIGhlaWdodDogMzcxcHg7XFxuICAgIH1cXG5cXG4uVGVzdC1qb2luQ29tbXVuaXR5LTNMQlVzIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxNjBweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IGJvdHRvbSwgbGVmdCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uVGVzdC1qb2luRmJUZXh0LTFmZGhNIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1yaWdodDogNjRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uVGVzdC1qb2luRmJMaW5rLUF2Y3NmIHtcXG4gIHBhZGRpbmc6IDE2cHggNDBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3JkZXItcmFkaXVzOiAxMDBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uVGVzdC1hdmFpbGFibGVUaXRsZS0zTkVmQSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uVGVzdC1hdmFpbGFibGUtM2xXTFgge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5UZXN0LWF2YWlsYWJsZUNvbnRlbnRTZWN0aW9uLVVQNGxuIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBtYXJnaW4tcmlnaHQ6IDJyZW07XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XFxufVxcblxcbi5UZXN0LXBsYXRmb3JtLWtPeEFNIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW46IDhweCAwIDRweCAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5UZXN0LXBsYXRmb3JtT3MtMzZQa0cge1xcbiAgZm9udC1zaXplOiAxMnB4O1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uVGVzdC1yb3ctc0UtQ28ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICBtYXJnaW46IDQ4cHggMCAzMnB4IDA7XFxuICBtYXJnaW46IDNyZW0gMCAycmVtIDA7XFxufVxcblxcbi5UZXN0LXN0b3JlLTNDeml6IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDRyZW07XFxufVxcblxcbi5UZXN0LXN0b3JlLTNDeml6IC5UZXN0LXBsYXlzdG9yZS0zUEtPQyB7XFxuICAgIHdpZHRoOiAxMzZweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICB9XFxuXFxuLlRlc3Qtc3RvcmUtM0N6aXogLlRlc3QtYXBwc3RvcmUtMWhPMTYge1xcbiAgICB3aWR0aDogMTM2cHg7XFxuICAgIGhlaWdodDogNDBweDtcXG4gIH1cXG5cXG4uVGVzdC1zY3JvbGxUb3AtMTRTOEMge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5UZXN0LXNjcm9sbFRvcC0xNFM4QyBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIHtcXG4gIG1pbi1oZWlnaHQ6IDcyMHB4O1xcbiAgcGFkZGluZzogMTZweCA2NHB4IDU2cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1oZWFkaW5nLTFvTnM1IHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiA2NnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXgtd2lkdGg6IDc0MHB4O1xcbiAgbWFyZ2luOiAxMnB4IDAgMCAwO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1oZWFkaW5nLTFvTnM1IC5UZXN0LWxlYXJuaW5nVGV4dC0xUEJYMyB7XFxuICB3aWR0aDogMzE2cHg7XFxuICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtZW1wdHlDYXJkLTJNeEhBIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC8vIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC10b3BDYXJkLTNBX05vIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1oZWFkaW5nLTFvTnM1IC5UZXN0LWxlYXJuaW5nVGV4dC0xUEJYMyBpbWcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAtMnB4O1xcbiAgbGVmdDogMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBtYXgtd2lkdGg6IDMwOHB4O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1jb250ZW50VGV4dC0ySHA2eSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIG1heC13aWR0aDogNjg3cHg7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIge1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1idXR0b253cmFwcGVyLWttcnBSIC5UZXN0LXJlcXVlc3REZW1vLUV5UEhGIHtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMTZweCAyNHB4O1xcbiAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1tYXgtY29udGVudDtcXG4gIHdpZHRoOiBtYXgtY29udGVudDtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiAuVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEoge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiAuVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEogLlRlc3Qtd2hhdHNhcHAtMy1oSXIge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgcGFkZGluZy1ib3R0b206IDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3Qtd2hhdHNhcHB3cmFwcGVyLTNEbVRKIGltZyB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMzJweDtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtZG93bmxvYWRBcHAtaDRkQ0kge1xcbiAgbWFyZ2luLXRvcDogNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1kb3dubG9hZEFwcC1oNGRDSSAuVGVzdC1kb3dubG9hZFRleHQtMktMdUEge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBvcGFjaXR5OiAwLjc7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWRvd25sb2FkQXBwLWg0ZENJIC5UZXN0LXBsYXlTdG9yZUljb24tM3ZiRksge1xcbiAgd2lkdGg6IDEzMnB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMjZweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIHtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxufVxcbi50ZWFjaENvbnRhaW5lciAuYWN0aW9uc1dyYXBwZXIgLmFjdGlvbiBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjc7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgbWFyZ2luLWxlZnQ6IDhweDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIC5hY3Rpb25pbWdib3gge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIC5hY3Rpb25pbWdib3ggaW1nIHtcXG4gIHdpZHRoOiAxMHB4O1xcbiAgaGVpZ2h0OiA5cHg7XFxufSAqL1xcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJyZWFkY3J1bS0xajE1RSB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG59XFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTE1MnB4O1xcbiAgbWFyZ2luOiAxMnB4IGF1dG8gMDtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnJlYWRjcnVtLTFqMTVFIHNwYW4ge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtdG9wU2VjdGlvbi0yMWJWMyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIC5UZXN0LXRlYWNoU2VjdGlvbi1nbmtGdSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LXRvcFNlY3Rpb24tMjFiVjMgLlRlc3QtdGVhY2hTZWN0aW9uLWdua0Z1IC5UZXN0LXRlYWNoSW1nQm94LTMydFBQIHtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIC5UZXN0LXRlYWNoU2VjdGlvbi1nbmtGdSAuVGVzdC10ZWFjaEltZ0JveC0zMnRQUCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtdG9wU2VjdGlvbi0yMWJWMyAuVGVzdC10ZWFjaFNlY3Rpb24tZ25rRnUgLlRlc3QtdGVhY2hTZWN0aW9uTmFtZS0xZkFBciB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzg4MDBmZTtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLmZlYXR1cmVzU2VjdGlvbiB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1sZWZ0OiAzMCU7XFxufVxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAuZmVhdHVyZXNTZWN0aW9uIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxufSAqL1xcblxcbi5UZXN0LWRpc3BsYXlDbGllbnRzLTE4anI1IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWluLWhlaWdodDogNDY0cHg7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4vKiAuZGlzcGxheUNsaWVudHMgLmRvdHMge1xcbiAgZGlzcGxheTogbm9uZTtcXG59ICovXFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgLlRlc3QtY2xpZW50c1dyYXBwZXItMmZEZVAge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg2LCAxZnIpO1xcbiAgZ2FwOiAyNHB4O1xcbiAgZ2FwOiAxLjVyZW07XFxuICBtYXJnaW46IDAgYXV0bztcXG59XFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgLlRlc3QtY2xpZW50c1dyYXBwZXItMmZEZVAgLlRlc3QtY2xpZW50LUZ6ZEJaIHtcXG4gIHdpZHRoOiAxNzJweDtcXG4gIGhlaWdodDogMTEycHg7XFxufVxcblxcbi5UZXN0LWRpc3BsYXlDbGllbnRzLTE4anI1IHNwYW4gYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG59XFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2ltYWdlcy9ob21lL25ld19jb25mZXR0aS5zdmcnKTtcXG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRIZWFkaW5nLXJ0ZGVPIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAvLyBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgZ2FwOiAxNnB4O1xcbiAgZ2FwOiAxcmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogMjAycHg7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXNpemU6IDEuNXJlbTtcXG4gIGNvbG9yOiByZ2JhKDM3LCA0MCwgNDMsIDAuNik7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICBwYWRkaW5nOiAxLjVyZW07XFxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3QtYWNoaWV2ZWRQcm9maWxlLTI0N2hYIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1hY2hpZXZlZFByb2ZpbGUtMjQ3aFguVGVzdC1jbGllbnRzLTJwLWNYIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYzZWI7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3QtYWNoaWV2ZWRQcm9maWxlLTI0N2hYLlRlc3Qtc3R1ZGVudHMtM0M3cnUge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZWZmZTtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1hY2hpZXZlZFByb2ZpbGUtMjQ3aFguVGVzdC10ZXN0cy0yZ3MxNyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWFjaGlldmVkUHJvZmlsZS0yNDdoWC5UZXN0LXF1ZXN0aW9ucy0zdldXVyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmVmO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWhpZ2hsaWdodC1SdWFZUSB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWhpZ2hsaWdodC1SdWFZUS5UZXN0LXF1ZXN0aW9uc0hpZ2hsaWdodC0yX1ZOUSB7XFxuICBjb2xvcjogIzAwYWMyNjtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1oaWdobGlnaHQtUnVhWVEuVGVzdC10ZXN0c0hpZ2hsaWdodC0yYkQxZiB7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1oaWdobGlnaHQtUnVhWVEuVGVzdC1zdHVkZW50c0hpZ2hsaWdodC0zcDRjSiB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1oaWdobGlnaHQtUnVhWVEuVGVzdC1jbGllbnRzSGlnaGxpZ2h0LXlDbERvIHtcXG4gIGNvbG9yOiAjZjYwO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LXN1YlRleHQtMlE4UnMge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi8qIC50b2dnbGVBdFJpZ2h0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDAgNjRweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZUF0TGVmdCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59ICovXFxuXFxuLlRlc3Qtc2VjdGlvbl9jb250YWluZXItMndBNW0ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBwYWRkaW5nOiAxNjBweCAzMnB4IDE2MHB4IDY0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLlRlc3Qtc2VjdGlvbl9jb250YWluZXItMndBNW0gLlRlc3Qtc2VjdGlvbl9jb250ZW50cy0yV0FTVyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIH1cXG5cXG4uVGVzdC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTFJS0ZvIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgcGFkZGluZzogMTYwcHggMzJweCAxNjBweCA2NHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZXN0LXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMUlLRm8gLlRlc3Qtc2VjdGlvbl9jb250ZW50cy0yV0FTVyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgfVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSB7XFxuICB3aWR0aDogNTAlO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtZW1wdHlDYXJkLTJNeEhBIHtcXG4gIHdpZHRoOiA2MDZweDtcXG4gIGhlaWdodDogMzQxcHg7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC10b3BDYXJkLTNBX05vIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHotaW5kZXg6IDE7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDg4cHg7XFxuICBoZWlnaHQ6IDg4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC10b3BDaXJjbGUtMkIySFAge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDg4cHg7XFxuICBoZWlnaHQ6IDg4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW9ubGluZVRlc3RzLTJpNGx1IC5UZXN0LXRvcENpcmNsZS0yQjJIUCB7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDUwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1vbmxpbmVUZXN0cy0yaTRsdSAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgYm90dG9tOiAtNDRweDtcXG4gIGxlZnQ6IDc5cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1vd25QYXBlcnMtMUd5MHIgLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICBvcGFjaXR5OiAwLjM7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDcwcHg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW93blBhcGVycy0xR3kwciAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gIGJvdHRvbTogLTQ0cHg7XFxuICBsZWZ0OiA0MHB4O1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1xdWVzdGlvbkJhbmstbC02bU4gLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDc2ZmY7XFxuICBvcGFjaXR5OiAwLjI7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDcwcHg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LXF1ZXN0aW9uQmFuay1sLTZtTiAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gIGJvdHRvbTogLTQ0cHg7XFxuICBsZWZ0OiA0MHB4O1xcbiAgb3BhY2l0eTogMC4yO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyAuVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEIHtcXG4gIHdpZHRoOiA0NSU7XFxuICBtYXJnaW4tYm90dG9tOiA1NnB4O1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyAuVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEIC5UZXN0LWNvbnRlbnRfdGl0bGUtMURPdlgge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyAuVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEIC5UZXN0LWNvbnRlbnRfdGV4dC1RS0ZIcSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4vKiAuYWxsY3VzdG9tZXJzIHAge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufSAqL1xcblxcbi5UZXN0LWNvbnRlbnQtMmhzOXMgcCBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uVGVzdC1oaWRlLTN2UFFVIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufVxcblxcbi5UZXN0LXNob3ctTFRPSG8ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcXG4gIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiB7XFxuICAgIHBhZGRpbmc6IDVyZW0gNjRweCAwIDY0cHg7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5UZXN0LXRlc3RzSW1hZ2UtM1oyZGUge1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuVGVzdC1zZWN0aW9uX3RpdGxlLTF1NnQwIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDE2cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1wb2ludC0yeUhNQiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXJlZERvdC0zbUtILSB7XFxuICAgIG1hcmdpbi10b3A6IDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXBvaW50VGV4dC0zb215OSB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtYm90dG9tQ2lyY2xlLTJTRUNPIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXRvcENpcmNsZS0yQjJIUCB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgd2lkdGg6IDQ4cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW9ubGluZVRlc3RzLTJpNGx1IC5UZXN0LXRvcENpcmNsZS0yQjJIUCB7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA1MHB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb25saW5lVGVzdHMtMmk0bHUgLlRlc3QtYm90dG9tQ2lyY2xlLTJTRUNPIHtcXG4gICAgYm90dG9tOiAtMjRweDtcXG4gICAgbGVmdDogNzlweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW93blBhcGVycy0xR3kwciAuVGVzdC10b3BDaXJjbGUtMkIySFAge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICBvcGFjaXR5OiAwLjM7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA3MHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb3duUGFwZXJzLTFHeTByIC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxuICAgIGJvdHRvbTogLTI0cHg7XFxuICAgIGxlZnQ6IDQwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1xdWVzdGlvbkJhbmstbC02bU4gLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgb3BhY2l0eTogMC4yO1xcbiAgICB0b3A6IC0yNHB4O1xcbiAgICByaWdodDogNzBweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LXF1ZXN0aW9uQmFuay1sLTZtTiAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICBib3R0b206IC0yNHB4O1xcbiAgICBsZWZ0OiA0MHB4O1xcbiAgICBvcGFjaXR5OiAwLjI7XFxuICB9XFxuXFxuICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1kb3dubG9hZEFwcC1oNGRDSSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bSB7XFxuICAgIG1pbi1oZWlnaHQ6IDIwcmVtO1xcbiAgICBwYWRkaW5nOiAycmVtIDFyZW07XFxuICB9XFxuXFxuICAuVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bSAuVGVzdC1tYXhDb250YWluZXItMV9RQ08ge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgfVxcblxcbiAgLlRlc3Qtc2VjdGlvbl9jb250YWluZXItMndBNW0gLlRlc3Qtc2VjdGlvbl9jb250ZW50cy0yV0FTVyB7XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICAgIG1heC1oZWlnaHQ6IG5vbmU7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5UZXN0LXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMUlLRm8ge1xcbiAgICBwYWRkaW5nOiAyNHB4IDIwcHggNDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMUlLRm8gLlRlc3Qtc2VjdGlvbl9jb250ZW50cy0yV0FTVyB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5UZXN0LWNvbnRlbnRQYXJ0LXZPeFltIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLlRlc3QtY29udGVudFBhcnQtdk94WW0gPiBkaXY6bnRoLWNoaWxkKDIpIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNDAwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZXN0LWNvbnRlbnRQYXJ0LXZPeFltIC5UZXN0LWNvbnRlbnQtMmhzOXMge1xcbiAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICB9XFxuXFxuICAuVGVzdC1jb250ZW50UGFydC12T3hZbSAuVGVzdC1jb250ZW50LTJoczlzIC5UZXN0LXRleHRjb250ZW50LTNyV2YxIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAvKiAuY29udGVudFBhcnQgLmNvbnRlbnQgLnRleHRjb250ZW50IC5wb2ludCAucmVkZG90d3JhcHBlciB7XFxuICAgIG1hcmdpbi10b3A6IDlweDtcXG4gIH1cXG5cXG4gIC5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQgLnBvaW50IC5wb2ludFRleHQge1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfSAqL1xcblxcbiAgLlRlc3QtY29udGVudFBhcnQtdk94WW0gLlRlc3QtY29udGVudC0yaHM5cyAuVGVzdC1zZWN0aW9uX3RpdGxlLTF1NnQwIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC1lbXB0eUNhcmQtMk14SEEge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzNjBweDtcXG4gICAgaGVpZ2h0OiAxODBweDtcXG4gICAgbWFyZ2luOiAwIGF1dG8gMzJweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXZpZGVvQ29udGVudHMtcFFxd0MgLlRlc3QtdmlkZW9fc2VjdGlvbi0yQzJQRCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXZpZGVvQ29udGVudHMtcFFxd0MgLlRlc3QtdmlkZW9fc2VjdGlvbi0yQzJQRDpsYXN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIC5UZXN0LXZpZGVvX3NlY3Rpb24tMkMyUEQgLlRlc3QtY29udGVudF90aXRsZS0xRE92WCB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIC5UZXN0LXZpZGVvX3NlY3Rpb24tMkMyUEQgLlRlc3QtY29udGVudF90ZXh0LVFLRkhxIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LW1vYmlsZVBhcGVyc0NvbnRhaW5lci0xR1BPdSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuVGVzdC1tb2JpbGVQYXBlcnNDb250YWluZXItMUdQT3UgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzNjBweDtcXG4gICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIH1cXG5cXG4gIC5UZXN0LXBhcGVyc1dyYXBwZXItMWFJTmQge1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuVGVzdC1yZXBvcnRzX3N1Yl90aXRsZS0xelU0ViB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1yZXBvcnRzX25vdGljZS0xazd4WSB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1zdWJfc2VjdGlvbl90aXRsZS0zVlJ1MyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTNGTi02IHtcXG4gICAgd2lkdGg6IDIzMnB4O1xcbiAgICBoZWlnaHQ6IDMzNXB4O1xcbiAgICByaWdodDogMDtcXG4gICAgbGVmdDogMCU7XFxuICAgIGJvdHRvbTogLTFyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZXN0LWRpc3BsYXlDbGllbnRzLTE4anI1IHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIG1pbi1oZWlnaHQ6IDI3cmVtO1xcbiAgfVxcblxcbiAgLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgLlRlc3QtY2xpZW50c1dyYXBwZXItMmZEZVAge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMCAwO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIG1hcmdpbjogMCBhdXRvO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBnYXA6IDFyZW07XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgfVxcblxcbiAgLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgLlRlc3QtY2xpZW50c1dyYXBwZXItMmZEZVAgLlRlc3QtY2xpZW50LUZ6ZEJaIHtcXG4gICAgd2lkdGg6IDkuNzVyZW07XFxuICAgIGhlaWdodDogN3JlbTtcXG4gIH1cXG5cXG4gIC8qIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudC5hY3RpdmUge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gIH0gKi9cXG5cXG4gIC5UZXN0LWRpc3BsYXlDbGllbnRzLTE4anI1IHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuZG90cyB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9ICovXFxuICAuVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJy9pbWFnZXMvaG9tZS9Db25mZXR0aS5zdmcnKTtcXG4gIH1cXG5cXG4gIC5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkSGVhZGluZy1ydGRlTyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XFxuICB9XFxuXFxuICAuVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZEhlYWRpbmctcnRkZU8gc3Bhbjo6YWZ0ZXIge1xcbiAgICBjb250ZW50OiAnXFxcXEEnO1xcbiAgICB3aGl0ZS1zcGFjZTogcHJlO1xcbiAgfVxcblxcbiAgLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYge1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgZ2FwOiAwLjc1cmVtO1xcbiAgICBtYXgtd2lkdGg6IDIwLjVyZW07XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAvKiAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgIC5jdXN0b21lcl9yZXZpZXcge1xcbiAgICAgIG9yZGVyOiAyO1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuXFxuICAgICAgLmN1c3RvbWVyTG9nbyB7XFxuICAgICAgICB3aWR0aDogODhweDtcXG4gICAgICAgIGhlaWdodDogNTZweDtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLmF1dGhvcldyYXBwZXIge1xcbiAgICAgICAgLmFib3V0X2F1dGhvciB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDM0cHg7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1tYXhDb250YWluZXItMV9RQ08ge1xcbiAgICBwb3NpdGlvbjogc3RhdGljO1xcbiAgICBtYXgtd2lkdGg6IDQ1MHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnJlYWRjcnVtLTFqMTVFIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28ge1xcbiAgICBoZWlnaHQ6IDY4cmVtO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIH1cXG5cXG4gICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtaGVhZGluZy0xb05zNSB7XFxuICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgbWFyZ2luOiAzMnB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtaGVhZGluZy0xb05zNSAuVGVzdC1sZWFybmluZ1RleHQtMVBCWDMge1xcbiAgICAgICAgd2lkdGg6IDIxMHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWhlYWRpbmctMW9OczUgLlRlc3QtbGVhcm5pbmdUZXh0LTFQQlgzIGltZyB7XFxuICAgICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICAgIG1pbi13aWR0aDogMTQwcHg7XFxuICAgICAgICB9XFxuXFxuICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWNvbnRlbnRUZXh0LTJIcDZ5IHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogNjUwcHg7XFxuICAgICAgbWFyZ2luOiAxNnB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIH1cXG5cXG4gICAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1idXR0b253cmFwcGVyLWttcnBSIC5UZXN0LXJlcXVlc3REZW1vLUV5UEhGIHtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtaW4td2lkdGg6IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3Qtd2hhdHNhcHB3cmFwcGVyLTNEbVRKIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3Qtd2hhdHNhcHB3cmFwcGVyLTNEbVRKIC5UZXN0LXdoYXRzYXBwLTMtaElyIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1idXR0b253cmFwcGVyLWttcnBSIC5UZXN0LXdoYXRzYXBwd3JhcHBlci0zRG1USiBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMjRweDtcXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIHtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIG1hcmdpbi10b3A6IDA7XFxuXFxuICAgICAgLyogLmZlYXR1cmVzU2VjdGlvbiB7XFxuICAgICAgICBkaXNwbGF5OiBub25lO1xcbiAgICAgIH0gKi9cXG4gICAgfVxcbiAgICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtdG9wU2VjdGlvbi0yMWJWMyAuVGVzdC10ZWFjaFNlY3Rpb24tZ25rRnUgLlRlc3QtdGVhY2hJbWdCb3gtMzJ0UFAge1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtdG9wU2VjdGlvbi0yMWJWMyAuVGVzdC10ZWFjaFNlY3Rpb24tZ25rRnUgLlRlc3QtdGVhY2hTZWN0aW9uTmFtZS0xZkFBciB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIge1xcbiAgICBwYWRkaW5nOiAzMnB4IDI4cHg7XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICB9XFxuXFxuICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1hdmFpbGFibGVUaXRsZS0zTkVmQSB7XFxuICAgICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LXJvdy1zRS1DbyB7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtcm93LXNFLUNvIC5UZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50IHtcXG4gICAgICAgICAgd2lkdGg6IDY4cHg7XFxuICAgICAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtcm93LXNFLUNvIC5UZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50IC5UZXN0LXBsYXRmb3JtLWtPeEFNIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEzLjNweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LXJvdy1zRS1DbyAuVGVzdC1wbGF0Zm9ybUNvbnRhaW5lci0yT2hudCAuVGVzdC1wbGF0Zm9ybU9zLTM2UGtHIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtcm93LXNFLUNvIC5UZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50Omxhc3QtY2hpbGQge1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1kZXNrdG9wSW1hZ2UtMmtYTlgge1xcbiAgICAgICAgd2lkdGg6IDkwJTtcXG4gICAgICAgIGhlaWdodDogMTYxcHg7XFxuICAgICAgICBtYXJnaW46IDAgYXV0byAwIGF1dG87XFxuICAgICAgfVxcblxcbiAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtc3RvcmUtM0N6aXoge1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LXN0b3JlLTNDeml6IC5UZXN0LXBsYXlzdG9yZS0zUEtPQyB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDAgMTZweCAwIDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LXN0b3JlLTNDeml6IC5UZXN0LWFwcHN0b3JlLTFoTzE2IHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIH1cXG5cXG4gIC5UZXN0LWpvaW5Db21tdW5pdHktM0xCVXMge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICBoZWlnaHQ6IDE0NHB4O1xcbiAgfVxcblxcbiAgLlRlc3Qtam9pbkZiVGV4dC0xZmRoTSB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLlRlc3Qtam9pbkZiTGluay1BdmNzZiB7XFxuICAgIHBhZGRpbmc6IDEycHggMzJweDtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXZpZGVvU2xpZGVyLTI4X3VnIHtcXG4gICAgcGFkZGluZzogM3JlbSAxcmVtO1xcbiAgICBtaW4taGVpZ2h0OiAzOHJlbTtcXG4gIH1cXG5cXG4gIC5UZXN0LW1hdGhzLVR1Y0VVIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAxNSU7XFxuICAgIHRvcDogODQlO1xcbiAgICB3aWR0aDogMzZweDtcXG4gICAgaGVpZ2h0OiAzNnB4O1xcbiAgfVxcblxcbiAgLlRlc3QtbWljcm9zY29wZS1Ic2F0RyB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA4NyU7XFxuICAgIGxlZnQ6IDQ1JTtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXRyaWFuZ2xlLTJCVUtZIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDg1JTtcXG4gICAgbGVmdDogODAlO1xcbiAgICB3aWR0aDogMzZweDtcXG4gICAgaGVpZ2h0OiAzMHB4O1xcbiAgfVxcblxcbiAgLlRlc3Qtc2NhbGUtMWM5STQge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHJpZ2h0OiA4MCU7XFxuICAgIHRvcDogMTAlO1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiAzMHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtY2lyY2xlLTJqVzF3IHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDMlO1xcbiAgICByaWdodDogNTAlO1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiAzMHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtY2hlbWlzdHJ5LTJlWU9XIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDglO1xcbiAgICByaWdodDogNSU7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDQ4cHg7XFxuICB9XFxuXFxuICAuVGVzdC1zY3JvbGxUb3AtMTRTOEMge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1Rlc3QvVGVzdC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLFVBQVU7Q0FDWDs7QUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixRQUFRO0NBQ1Q7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsU0FBUztFQUNULFNBQVM7Q0FDVjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsVUFBVTtDQUNYOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxRQUFRO0NBQ1Q7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7Q0FDWDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsV0FBVztDQUNaOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxTQUFTO0VBQ1QsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsdUJBQXVCO09BQ3BCLG9CQUFvQjtHQUN4Qjs7QUFFSDtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsV0FBVztDQUNaOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7R0FDbEI7O0FBRUg7TUFDTSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLGVBQWU7O01BRWY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7VUF5Qkk7S0FDTDs7QUFFTDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUEwRUk7O0FBRUo7RUFDRSwrQkFBK0I7RUFDL0IsK0JBQStCO0VBQy9CLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0FBRUQ7SUFDSSwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLGFBQWE7R0FDZDs7QUFFSDtNQUNNLGFBQWE7TUFDYixjQUFjO0tBQ2Y7O0FBRUw7RUFDRSxZQUFZO0VBQ1osY0FBYztFQUNkLDhGQUE4RjtFQUM5RixvRUFBb0U7RUFDcEUsK0RBQStEO0VBQy9ELDREQUE0RDtFQUM1RCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtDQUM3Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLG9CQUFvQjtNQUNoQixnQkFBZ0I7RUFDcEIsc0JBQXNCO0VBQ3RCLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQixxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLG9CQUFvQjtFQUNwQixvQkFBb0I7Q0FDckI7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsYUFBYTtJQUNiLG1CQUFtQjtHQUNwQjs7QUFFSDtJQUNJLGFBQWE7SUFDYixhQUFhO0dBQ2Q7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0Usa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QiwwQkFBMEI7RUFDMUIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtFQUNiLDRCQUE0QjtFQUM1Qix5QkFBeUI7RUFDekIsb0JBQW9CO0VBQ3BCLHNCQUFzQjtFQUN0QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsdUJBQXVCO0tBQ3BCLG9CQUFvQjtDQUN4Qjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsUUFBUTtFQUNSLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxZQUFZO0VBQ1oscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLDBCQUEwQjtFQUMxQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsYUFBYTtDQUNkOztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFpQ0k7O0FBRUo7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25COztBQUVEOzs7Ozs7Ozs7OztJQVdJOztBQUVKO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsMEJBQTBCO0NBQzNCOztBQUVEOztJQUVJOztBQUVKO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLHNDQUFzQztFQUN0QyxVQUFVO0VBQ1YsWUFBWTtFQUNaLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztDQUNmOztBQUVEO0VBQ0Usc0JBQXNCO0VBQ3RCLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsdURBQXVEO0VBQ3ZELHlCQUF5QjtFQUN6Qiw0QkFBNEI7RUFDNUIsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQiwwQkFBMEI7TUFDdEIsOEJBQThCO0NBQ25DOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLHNDQUFzQztFQUN0QyxVQUFVO0VBQ1YsVUFBVTtFQUNWLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0Isa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLCtEQUErRDtVQUN2RCx1REFBdUQ7RUFDL0QsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtDQUNwQjs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7SUFjSTs7QUFFSjtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7RUFDdkIsK0JBQStCO0VBQy9CLFlBQVk7RUFDWixhQUFhO0VBQ2IsYUFBYTtDQUNkOztBQUVEO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxnQ0FBZ0M7UUFDNUIsNEJBQTRCO0lBQ2hDLFlBQVk7SUFDWix1QkFBdUI7UUFDbkIsK0JBQStCO0dBQ3BDOztBQUVIO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLDBCQUEwQjtFQUMxQiwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLGFBQWE7RUFDYixhQUFhO0NBQ2Q7O0FBRUQ7SUFDSSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHdCQUF3QjtRQUNwQixvQkFBb0I7SUFDeEIsWUFBWTtJQUNaLHVCQUF1QjtRQUNuQiwrQkFBK0I7R0FDcEM7O0FBRUg7RUFDRSxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsV0FBVztFQUNYLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSx1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSwwQkFBMEI7RUFDMUIsY0FBYztFQUNkLFdBQVc7Q0FDWjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsV0FBVztFQUNYLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQixjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQix1QkFBdUI7TUFDbkIsK0JBQStCO0NBQ3BDOztBQUVEO0VBQ0UsV0FBVztFQUNYLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0NBQ2hCOztBQUVEOzs7OztJQUtJOztBQUVKO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLGFBQWE7RUFDYixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7Q0FDNUI7O0FBRUQ7RUFDRTtJQUNFLDBCQUEwQjtHQUMzQjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLFdBQVc7SUFDWCxZQUFZO0lBQ1osMEJBQTBCO0dBQzNCOztFQUVEO0lBQ0UsY0FBYztJQUNkLFdBQVc7SUFDWCwwQkFBMEI7R0FDM0I7O0VBRUQ7SUFDRSx1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLFdBQVc7SUFDWCxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSwwQkFBMEI7SUFDMUIsY0FBYztJQUNkLFdBQVc7R0FDWjs7RUFFRDtJQUNFLDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsV0FBVztJQUNYLFlBQVk7R0FDYjs7RUFFRDtJQUNFLDBCQUEwQjtJQUMxQixjQUFjO0lBQ2QsV0FBVztJQUNYLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQix1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGtCQUFrQjtJQUNsQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQiwyQkFBMkI7UUFDdkIsdUJBQXVCO0dBQzVCOztFQUVEO0lBQ0Usd0JBQXdCO0dBQ3pCOztFQUVEO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtHQUM1Qjs7RUFFRDtJQUNFLFlBQVk7SUFDWiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHFCQUFxQjtRQUNqQiw0QkFBNEI7SUFDaEMsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixhQUFhO0dBQ2Q7O0VBRUQ7Ozs7OztNQU1JOztFQUVKO0lBQ0UsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0dBQzVCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLHNCQUFzQjtRQUNsQix3QkFBd0I7R0FDN0I7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQix1QkFBdUI7T0FDcEIsb0JBQW9CO0dBQ3hCOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixjQUFjO0lBQ2QsU0FBUztJQUNULFNBQVM7SUFDVCxjQUFjO0lBQ2QsYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQ0FBc0M7SUFDdEMsbUNBQW1DO0lBQ25DLFVBQVU7SUFDViwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGVBQWU7SUFDZixhQUFhO0dBQ2Q7O0VBRUQ7O01BRUk7O0VBRUo7SUFDRSxpQkFBaUI7R0FDbEI7O0VBRUQ7O01BRUk7RUFDSjtJQUNFLHFCQUFxQjtJQUNyQiw2Q0FBNkM7R0FDOUM7O0VBRUQ7SUFDRSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLHNCQUFzQjtHQUN2Qjs7RUFFRDtJQUNFLGNBQWM7SUFDZCxpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSwyQkFBMkI7SUFDM0IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixhQUFhO0dBQ2Q7O0VBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7TUF3Q0k7O0VBRUo7SUFDRSxpQkFBaUI7SUFDakIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsY0FBYztJQUNkLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0UsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsZ0JBQWdCO01BQ2hCLHlCQUF5QjtLQUMxQjs7TUFFQztRQUNFLGFBQWE7T0FDZDs7UUFFQztVQUNFLFFBQVE7VUFDUixpQkFBaUI7U0FDbEI7O0lBRUw7TUFDRSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixpQkFBaUI7TUFDakIseUJBQXlCO0tBQzFCOztJQUVEO01BQ0UsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQixpQkFBaUI7S0FDbEI7O01BRUM7UUFDRSxrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixnQkFBZ0I7T0FDakI7O01BRUQ7UUFDRSxpQkFBaUI7UUFDakIsb0JBQW9CO09BQ3JCOztRQUVDO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixXQUFXO1VBQ1gsZUFBZTtTQUNoQjs7UUFFRDtVQUNFLFlBQVk7VUFDWixhQUFhO1NBQ2Q7O0lBRUw7TUFDRSxzQkFBc0I7VUFDbEIsd0JBQXdCO01BQzVCLGNBQWM7O01BRWQ7O1VBRUk7S0FDTDtRQUNHO1VBQ0UsWUFBWTtVQUNaLGFBQWE7U0FDZDs7UUFFRDtVQUNFLGdCQUFnQjtVQUNoQixrQkFBa0I7U0FDbkI7O0VBRVA7SUFDRSxtQkFBbUI7SUFDbkIsaUJBQWlCO0dBQ2xCOztJQUVDO01BQ0UsMkJBQTJCO0tBQzVCOztNQUVDO1FBQ0UsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixvQkFBb0I7T0FDckI7O01BRUQ7UUFDRSxVQUFVO1FBQ1Ysb0JBQW9CO1FBQ3BCLHNCQUFzQjtZQUNsQix3QkFBd0I7T0FDN0I7O1FBRUM7VUFDRSxZQUFZO1VBQ1oscUJBQXFCO2NBQ2pCLDRCQUE0QjtVQUNoQyxtQkFBbUI7VUFDbkIsaUJBQWlCO1NBQ2xCOztVQUVDO1lBQ0Usa0JBQWtCO1lBQ2xCLGtCQUFrQjtXQUNuQjs7VUFFRDtZQUNFLGdCQUFnQjtZQUNoQixrQkFBa0I7V0FDbkI7O1FBRUg7VUFDRSxnQkFBZ0I7U0FDakI7O01BRUg7UUFDRSxXQUFXO1FBQ1gsY0FBYztRQUNkLHNCQUFzQjtPQUN2Qjs7TUFFRDtRQUNFLHNCQUFzQjtZQUNsQix3QkFBd0I7UUFDNUIsb0JBQW9CO09BQ3JCOztRQUVDO1VBQ0UsYUFBYTtVQUNiLGFBQWE7VUFDYixtQkFBbUI7U0FDcEI7O1FBRUQ7VUFDRSxhQUFhO1VBQ2IsYUFBYTtVQUNiLFVBQVU7U0FDWDs7RUFFUDtJQUNFLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QixjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixTQUFTO0lBQ1QsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsU0FBUztJQUNULFVBQVU7SUFDVixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxTQUFTO0lBQ1QsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsUUFBUTtJQUNSLFVBQVU7SUFDVixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkO0NBQ0ZcIixcImZpbGVcIjpcIlRlc3Quc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIubWF4Q29udGFpbmVyIHtcXG4gIG1heC13aWR0aDogMTcwMHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5tYXJnaW5Cb3R0b20xMjYge1xcbiAgbWFyZ2luLWJvdHRvbTogMTI2cHg7XFxufVxcblxcbi5yZXBvcnRzX3N1Yl90aXRsZSB7XFxuICBmb250LXNpemU6IDI4cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5yZXBvcnRzX25vdGljZSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbWFyZ2luLXRvcDogNHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLnJvdzEge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG59XFxuXFxuLnBvaW50Q29udGFpbmVyIHtcXG4gIHdpZHRoOiA1JTtcXG59XFxuXFxuLnBvaW50IHtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxufVxcblxcbi5wb2ludFRleHQge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxufVxcblxcbi5yZWREb3Qge1xcbiAgd2lkdGg6IDhweDtcXG4gIGhlaWdodDogOHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIG1hcmdpbi1yaWdodDogMjBweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi50ZXN0c0ltYWdlIHtcXG4gIHdpZHRoOiA3MnB4O1xcbiAgaGVpZ2h0OiA3MnB4O1xcbiAgbWFyZ2luLXRvcDogLThweDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuXFxuLnRlc3RzSW1hZ2UgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uc2VjdGlvbl9jb250YWluZXIuYXNoQmFja2dyb3VuZCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4udmlkZW9TbGlkZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA4MHB4IDY0cHg7XFxuICBwYWRkaW5nOiA1cmVtIDRyZW07XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4ubWF0aHMge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMTAlO1xcbiAgdG9wOiA1JTtcXG59XFxuXFxuLm1pY3Jvc2NvcGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAzMCU7XFxuICBsZWZ0OiAzJTtcXG59XFxuXFxuLnRyaWFuZ2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogNjAlO1xcbiAgbGVmdDogMTAlO1xcbn1cXG5cXG4uc2NhbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDEwJTtcXG4gIHRvcDogNSU7XFxufVxcblxcbi5jaXJjbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAzMCU7XFxuICByaWdodDogMyU7XFxufVxcblxcbi5jaGVtaXN0cnkge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA2MCU7XFxuICByaWdodDogMTAlO1xcbn1cXG5cXG4uaGVhZGVyYmFja2dyb3VuZG1vYmlsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IC01NnB4O1xcbiAgcmlnaHQ6IDA7XFxuICB3aWR0aDogNTEzcHg7XFxuICBoZWlnaHQ6IDU2MHB4O1xcbn1cXG5cXG4uaGVhZGVyYmFja2dyb3VuZG1vYmlsZSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbi5zZWN0aW9uX3RpdGxlIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWFyZ2luOiAwIDAgMTZweDtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uc3ViX3NlY3Rpb25fdGl0bGUge1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG59XFxuXFxuLmNvbnRlbnRQYXJ0IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICB3aWR0aDogNDUlO1xcbn1cXG5cXG4uY29udGVudFBhcnQgLmNvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWluLXdpZHRoOiAyOTBweDtcXG4gICAgbWF4LXdpZHRoOiA1NDBweDtcXG4gIH1cXG5cXG4uY29udGVudFBhcnQgLmNvbnRlbnQgLnRleHRjb250ZW50IHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuXFxuICAgICAgLyogLnBvaW50IHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgaGVpZ2h0OiBtYXgtY29udGVudDtcXG4gICAgICAgIG1hcmdpbjogNnB4IDA7XFxuXFxuICAgICAgICAucmVkZG90d3JhcHBlciB7XFxuICAgICAgICAgIHdpZHRoOiA1JTtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xcblxcbiAgICAgICAgICAucmVkZG90IHtcXG4gICAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgICB9XFxuICAgICAgICB9XFxuXFxuICAgICAgICAucG9pbnRUZXh0IHtcXG4gICAgICAgICAgd2lkdGg6IDk1JTtcXG4gICAgICAgIH1cXG4gICAgICB9ICovXFxuICAgIH1cXG5cXG4vKiAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgaGVpZ2h0OiA1NjBweDtcXG5cXG4gIC5jdXN0b21lcl9yZXZpZXcge1xcbiAgICBvcmRlcjogMDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDQ4cHggNjRweDtcXG4gICAgd2lkdGg6IDUwJTtcXG5cXG4gICAgLmN1c3RvbWVyTG9nbyB7XFxuICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIGhlaWdodDogODBweDtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG5cXG4gICAgICBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG4gICAgfVxcblxcbiAgICAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICB9XFxuXFxuICAgIC5hdXRob3JXcmFwcGVyIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuXFxuICAgICAgLmF1dGhvcl90aXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmFib3V0X2F1dGhvciB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICAgICAgfVxcbiAgICB9XFxuXFxuICAgIC5hbGxjdXN0b21lcnMge1xcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICAgIGNvbG9yOiAjZmZmO1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcblxcbiAgICAgIGltZyB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgICAgd2lkdGg6IDIwcHg7XFxuICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG4gICAgfVxcbiAgfVxcbn0gKi9cXG5cXG4uYXZhaWxhYmxlQ29udGFpbmVyIHtcXG4gIHBhZGRpbmc6IDgwcHggMTEzcHggNTZweCAxNjRweDtcXG4gIHBhZGRpbmc6IDVyZW0gMTEzcHggNTZweCAxNjRweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuZGVza3RvcEltYWdlIHtcXG4gICAgICB3aWR0aDogNjk2cHg7XFxuICAgICAgaGVpZ2h0OiAzNzFweDtcXG4gICAgfVxcblxcbi5qb2luQ29tbXVuaXR5IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxNjBweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IGJvdHRvbSwgbGVmdCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uam9pbkZiVGV4dCB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tcmlnaHQ6IDY0cHg7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLmpvaW5GYkxpbmsge1xcbiAgcGFkZGluZzogMTZweCA0MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5hdmFpbGFibGVUaXRsZSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uYXZhaWxhYmxlIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uYXZhaWxhYmxlQ29udGVudFNlY3Rpb24ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLnBsYXRmb3JtQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBtYXJnaW4tcmlnaHQ6IDJyZW07XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XFxufVxcblxcbi5wbGF0Zm9ybSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luOiA4cHggMCA0cHggMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4ucGxhdGZvcm1PcyB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5yb3cge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICBtYXJnaW46IDQ4cHggMCAzMnB4IDA7XFxuICBtYXJnaW46IDNyZW0gMCAycmVtIDA7XFxufVxcblxcbi5zdG9yZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0cmVtO1xcbn1cXG5cXG4uc3RvcmUgLnBsYXlzdG9yZSB7XFxuICAgIHdpZHRoOiAxMzZweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICB9XFxuXFxuLnN0b3JlIC5hcHBzdG9yZSB7XFxuICAgIHdpZHRoOiAxMzZweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgfVxcblxcbi5zY3JvbGxUb3Age1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5zY3JvbGxUb3AgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4udGVhY2hDb250YWluZXIge1xcbiAgbWluLWhlaWdodDogNzIwcHg7XFxuICBwYWRkaW5nOiAxNnB4IDY0cHggNTZweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuaGVhZGluZyB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWF4LXdpZHRoOiA3NDBweDtcXG4gIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQge1xcbiAgd2lkdGg6IDMxNnB4O1xcbiAgaGVpZ2h0OiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiAtbW96LWZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLmltYWdlUGFydCAuZW1wdHlDYXJkIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC8vIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5pbWFnZVBhcnQgLnRvcENhcmQgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuaGVhZGluZyAubGVhcm5pbmdUZXh0IGltZyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IC0ycHg7XFxuICBsZWZ0OiAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDhweDtcXG4gIG1heC13aWR0aDogMzA4cHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuY29udGVudFRleHQge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICBtYXJnaW46IDEycHggMCAwIDA7XFxuICBtYXgtd2lkdGg6IDY4N3B4O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIge1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMTZweCAyNHB4O1xcbiAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1tYXgtY29udGVudDtcXG4gIHdpZHRoOiBtYXgtY29udGVudDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjMjUyODJiICFpbXBvcnRhbnQ7XFxuICBtYXJnaW46IDAgOHB4IDAgMzJweDtcXG4gIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICBoZWlnaHQ6IDMycHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAge1xcbiAgbWFyZ2luLXRvcDogNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmRvd25sb2FkQXBwIC5kb3dubG9hZFRleHQge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBvcGFjaXR5OiAwLjc7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAgLnBsYXlTdG9yZUljb24ge1xcbiAgd2lkdGg6IDEzMnB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMjZweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIHtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxufVxcbi50ZWFjaENvbnRhaW5lciAuYWN0aW9uc1dyYXBwZXIgLmFjdGlvbiBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjc7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgbWFyZ2luLWxlZnQ6IDhweDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIC5hY3Rpb25pbWdib3gge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIC5hY3Rpb25pbWdib3ggaW1nIHtcXG4gIHdpZHRoOiAxMHB4O1xcbiAgaGVpZ2h0OiA5cHg7XFxufSAqL1xcblxcbi50ZWFjaENvbnRhaW5lciAuYnJlYWRjcnVtIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTE1MnB4O1xcbiAgbWFyZ2luOiAxMnB4IGF1dG8gMDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5icmVhZGNydW0gc3BhbiB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaEltZ0JveCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoU2VjdGlvbk5hbWUge1xcbiAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgY29sb3I6ICM4ODAwZmU7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLyogLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC5mZWF0dXJlc1NlY3Rpb24ge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tbGVmdDogMzAlO1xcbn1cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLmZlYXR1cmVzU2VjdGlvbiBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbn0gKi9cXG5cXG4uZGlzcGxheUNsaWVudHMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiA0NjRweDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi8qIC5kaXNwbGF5Q2xpZW50cyAuZG90cyB7XFxuICBkaXNwbGF5OiBub25lO1xcbn0gKi9cXG5cXG4uZGlzcGxheUNsaWVudHMgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMTJweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4gYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4gYTpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAvLyBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgZ2FwOiAxNnB4O1xcbiAgZ2FwOiAxcmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogMjAycHg7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXNpemU6IDEuNXJlbTtcXG4gIGNvbG9yOiByZ2JhKDM3LCA0MCwgNDMsIDAuNik7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICBwYWRkaW5nOiAxLjVyZW07XFxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZSB7XFxuICB3aWR0aDogNTJweDtcXG4gIGhlaWdodDogNTJweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5jbGllbnRzIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYzZWI7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5zdHVkZW50cyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUudGVzdHMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnF1ZXN0aW9ucyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmVmO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQge1xcbiAgZm9udC1zaXplOiAzNnB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnF1ZXN0aW9uc0hpZ2hsaWdodCB7XFxuICBjb2xvcjogIzAwYWMyNjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnRlc3RzSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQuc3R1ZGVudHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICM4YzAwZmU7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5jbGllbnRzSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjZjYwO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5zdWJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4vKiAudG9nZ2xlQXRSaWdodCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxufVxcblxcbi50b2dnbGVBdExlZnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggMCA2NHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxufSAqL1xcblxcbi5zZWN0aW9uX2NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDE2MHB4IDMycHggMTYwcHggNjRweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uc2VjdGlvbl9jb250YWluZXIgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICB9XFxuXFxuLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2Uge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwYWRkaW5nOiAxNjBweCAzMnB4IDE2MHB4IDY0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIH1cXG5cXG4uaW1hZ2VQYXJ0IHtcXG4gIHdpZHRoOiA1MCU7XFxufVxcblxcbi5pbWFnZVBhcnQgLmVtcHR5Q2FyZCB7XFxuICB3aWR0aDogNjA2cHg7XFxuICBoZWlnaHQ6IDM0MXB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC50b3BDYXJkIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHotaW5kZXg6IDE7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxufVxcblxcbi5pbWFnZVBhcnQgLmJvdHRvbUNpcmNsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogODhweDtcXG4gIGhlaWdodDogODhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG59XFxuXFxuLmltYWdlUGFydCAudG9wQ2lyY2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA4OHB4O1xcbiAgaGVpZ2h0OiA4OHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0Lm9ubGluZVRlc3RzIC50b3BDaXJjbGUge1xcbiAgdG9wOiAtNDRweDtcXG4gIHJpZ2h0OiA1MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG59XFxuXFxuLmltYWdlUGFydC5vbmxpbmVUZXN0cyAuYm90dG9tQ2lyY2xlIHtcXG4gIGJvdHRvbTogLTQ0cHg7XFxuICBsZWZ0OiA3OXB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG59XFxuXFxuLmltYWdlUGFydC5vd25QYXBlcnMgLnRvcENpcmNsZSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgb3BhY2l0eTogMC4zO1xcbiAgdG9wOiAtNDRweDtcXG4gIHJpZ2h0OiA3MHB4O1xcbn1cXG5cXG4uaW1hZ2VQYXJ0Lm93blBhcGVycyAuYm90dG9tQ2lyY2xlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxuICBib3R0b206IC00NHB4O1xcbiAgbGVmdDogNDBweDtcXG59XFxuXFxuLmltYWdlUGFydC5xdWVzdGlvbkJhbmsgLnRvcENpcmNsZSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgb3BhY2l0eTogMC4yO1xcbiAgdG9wOiAtNDRweDtcXG4gIHJpZ2h0OiA3MHB4O1xcbn1cXG5cXG4uaW1hZ2VQYXJ0LnF1ZXN0aW9uQmFuayAuYm90dG9tQ2lyY2xlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICBib3R0b206IC00NHB4O1xcbiAgbGVmdDogNDBweDtcXG4gIG9wYWNpdHk6IDAuMjtcXG59XFxuXFxuLmltYWdlUGFydCAudmlkZW9Db250ZW50cyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIC52aWRlb19zZWN0aW9uIHtcXG4gIHdpZHRoOiA0NSU7XFxuICBtYXJnaW4tYm90dG9tOiA1NnB4O1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIC52aWRlb19zZWN0aW9uIC5jb250ZW50X3RpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLmltYWdlUGFydCAudmlkZW9Db250ZW50cyAudmlkZW9fc2VjdGlvbiAuY29udGVudF90ZXh0IHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi8qIC5hbGxjdXN0b21lcnMgcCB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59ICovXFxuXFxuLmNvbnRlbnQgcCBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uaGlkZSB7XFxuICBkaXNwbGF5OiBub25lO1xcbn1cXG5cXG4uc2hvdyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xcbiAgLmF2YWlsYWJsZUNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDVyZW0gNjRweCAwIDY0cHg7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC50ZXN0c0ltYWdlIHtcXG4gICAgd2lkdGg6IDQ4cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLnNlY3Rpb25fdGl0bGUge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogMTZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIH1cXG5cXG4gIC5wb2ludCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIH1cXG5cXG4gIC5yZWREb3Qge1xcbiAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICB9XFxuXFxuICAucG9pbnRUZXh0IHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLmJvdHRvbUNpcmNsZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgd2lkdGg6IDQ4cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLnRvcENpcmNsZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgd2lkdGg6IDQ4cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQub25saW5lVGVzdHMgLnRvcENpcmNsZSB7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA1MHB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgfVxcblxcbiAgLmltYWdlUGFydC5vbmxpbmVUZXN0cyAuYm90dG9tQ2lyY2xlIHtcXG4gICAgYm90dG9tOiAtMjRweDtcXG4gICAgbGVmdDogNzlweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQub3duUGFwZXJzIC50b3BDaXJjbGUge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICBvcGFjaXR5OiAwLjM7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA3MHB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydC5vd25QYXBlcnMgLmJvdHRvbUNpcmNsZSB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxuICAgIGJvdHRvbTogLTI0cHg7XFxuICAgIGxlZnQ6IDQwcHg7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0LnF1ZXN0aW9uQmFuayAudG9wQ2lyY2xlIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgb3BhY2l0eTogMC4yO1xcbiAgICB0b3A6IC0yNHB4O1xcbiAgICByaWdodDogNzBweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQucXVlc3Rpb25CYW5rIC5ib3R0b21DaXJjbGUge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICBib3R0b206IC0yNHB4O1xcbiAgICBsZWZ0OiA0MHB4O1xcbiAgICBvcGFjaXR5OiAwLjI7XFxuICB9XFxuXFxuICAudGVhY2hDb250YWluZXIgLmRvd25sb2FkQXBwIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lciB7XFxuICAgIG1pbi1oZWlnaHQ6IDIwcmVtO1xcbiAgICBwYWRkaW5nOiAycmVtIDFyZW07XFxuICB9XFxuXFxuICAuc2VjdGlvbl9jb250YWluZXIgLm1heENvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICB9XFxuXFxuICAuc2VjdGlvbl9jb250YWluZXIgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xcbiAgICBtYXgtaGVpZ2h0OiBub25lO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICB9XFxuXFxuICAuc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSB7XFxuICAgIHBhZGRpbmc6IDI0cHggMjBweCA0OHB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICB9XFxuXFxuICAuY29udGVudFBhcnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZy1ib3R0b206IDA7XFxuICB9XFxuXFxuICAuY29udGVudFBhcnQgPiBkaXY6bnRoLWNoaWxkKDIpIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNDAwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5jb250ZW50UGFydCAuY29udGVudCB7XFxuICAgIG1heC13aWR0aDogbm9uZTtcXG4gIH1cXG5cXG4gIC5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC8qIC5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQgLnBvaW50IC5yZWRkb3R3cmFwcGVyIHtcXG4gICAgbWFyZ2luLXRvcDogOXB4O1xcbiAgfVxcblxcbiAgLmNvbnRlbnRQYXJ0IC5jb250ZW50IC50ZXh0Y29udGVudCAucG9pbnQgLnBvaW50VGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICB9ICovXFxuXFxuICAuY29udGVudFBhcnQgLmNvbnRlbnQgLnNlY3Rpb25fdGl0bGUge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICBoZWlnaHQ6IDE4MHB4O1xcbiAgICBtYXJnaW46IDAgYXV0byAzMnB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAudmlkZW9Db250ZW50cyB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLnZpZGVvQ29udGVudHMgLnZpZGVvX3NlY3Rpb24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLnZpZGVvQ29udGVudHMgLnZpZGVvX3NlY3Rpb246bGFzdC1jaGlsZCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIC52aWRlb19zZWN0aW9uIC5jb250ZW50X3RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAudmlkZW9Db250ZW50cyAudmlkZW9fc2VjdGlvbiAuY29udGVudF90ZXh0IHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVQYXBlcnNDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLm1vYmlsZVBhcGVyc0NvbnRhaW5lciBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbiAgLnBhcGVyc1dyYXBwZXIge1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAucmVwb3J0c19zdWJfdGl0bGUge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLnJlcG9ydHNfbm90aWNlIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5zdWJfc2VjdGlvbl90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyYmFja2dyb3VuZG1vYmlsZSB7XFxuICAgIHdpZHRoOiAyMzJweDtcXG4gICAgaGVpZ2h0OiAzMzVweDtcXG4gICAgcmlnaHQ6IDA7XFxuICAgIGxlZnQ6IDAlO1xcbiAgICBib3R0b206IC0xcmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfSAqL1xcblxcbiAgLmRpc3BsYXlDbGllbnRzIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuZG90cyB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9ICovXFxuICAuYWNoaWV2ZWRDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgYmFja2dyb3VuZDogdXJsKCcvaW1hZ2VzL2hvbWUvQ29uZmV0dGkuc3ZnJyk7XFxuICB9XFxuXFxuICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XFxuICB9XFxuXFxuICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyBzcGFuOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICdcXFxcYSc7XFxuICAgIHdoaXRlLXNwYWNlOiBwcmU7XFxuICB9XFxuXFxuICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIGdhcDogMC43NXJlbTtcXG4gICAgbWF4LXdpZHRoOiAyMC41cmVtO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLyogLmN1c3RvbWVyc19jb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcblxcbiAgICAuY3VzdG9tZXJfcmV2aWV3IHtcXG4gICAgICBvcmRlcjogMjtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgaGVpZ2h0OiA1MCU7XFxuICAgICAgcGFkZGluZzogMzJweCAxNnB4O1xcblxcbiAgICAgIC5jdXN0b21lckxvZ28ge1xcbiAgICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgICBoZWlnaHQ6IDU2cHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLnNyaUNoYWl0YW55YVRleHQge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC5hdXRob3JXcmFwcGVyIHtcXG4gICAgICAgIC5hYm91dF9hdXRob3Ige1xcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAzNHB4O1xcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLnRlYWNoQ29udGFpbmVyIC5tYXhDb250YWluZXIge1xcbiAgICBwb3NpdGlvbjogc3RhdGljO1xcbiAgICBtYXgtd2lkdGg6IDQ1MHB4O1xcbiAgfVxcblxcbiAgLnRlYWNoQ29udGFpbmVyIC5icmVhZGNydW0ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAudGVhY2hDb250YWluZXIge1xcbiAgICBoZWlnaHQ6IDY4cmVtO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICBtYXJnaW46IDMycHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAudGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCB7XFxuICAgICAgICB3aWR0aDogMjEwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQgaW1nIHtcXG4gICAgICAgICAgbGVmdDogMDtcXG4gICAgICAgICAgbWluLXdpZHRoOiAxNDBweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5jb250ZW50VGV4dCB7XFxuICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IDY1MHB4O1xcbiAgICAgIG1hcmdpbjogMTZweCBhdXRvIDAgYXV0bztcXG4gICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIH1cXG5cXG4gICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtaW4td2lkdGg6IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG9wYWNpdHk6IDE7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIHtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIG1hcmdpbi10b3A6IDA7XFxuXFxuICAgICAgLyogLmZlYXR1cmVzU2VjdGlvbiB7XFxuICAgICAgICBkaXNwbGF5OiBub25lO1xcbiAgICAgIH0gKi9cXG4gICAgfVxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoSW1nQm94IHtcXG4gICAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaFNlY3Rpb25OYW1lIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gIC5hdmFpbGFibGVDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAzMnB4IDI4cHg7XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLmF2YWlsYWJsZVRpdGxlIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIHtcXG4gICAgICAgICAgd2lkdGg6IDY4cHg7XFxuICAgICAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lciAucGxhdGZvcm0ge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuM3B4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lciAucGxhdGZvcm1PcyB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXI6bGFzdC1jaGlsZCB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLmRlc2t0b3BJbWFnZSB7XFxuICAgICAgICB3aWR0aDogOTAlO1xcbiAgICAgICAgaGVpZ2h0OiAxNjFweDtcXG4gICAgICAgIG1hcmdpbjogMCBhdXRvIDAgYXV0bztcXG4gICAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5zdG9yZSB7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuc3RvcmUgLnBsYXlzdG9yZSB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDAgMTZweCAwIDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnN0b3JlIC5hcHBzdG9yZSB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuXFxuICAuam9pbkNvbW11bml0eSB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGhlaWdodDogMTQ0cHg7XFxuICB9XFxuXFxuICAuam9pbkZiVGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLmpvaW5GYkxpbmsge1xcbiAgICBwYWRkaW5nOiAxMnB4IDMycHg7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAudmlkZW9TbGlkZXIge1xcbiAgICBwYWRkaW5nOiAzcmVtIDFyZW07XFxuICAgIG1pbi1oZWlnaHQ6IDM4cmVtO1xcbiAgfVxcblxcbiAgLm1hdGhzIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAxNSU7XFxuICAgIHRvcDogODQlO1xcbiAgICB3aWR0aDogMzZweDtcXG4gICAgaGVpZ2h0OiAzNnB4O1xcbiAgfVxcblxcbiAgLm1pY3Jvc2NvcGUge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogODclO1xcbiAgICBsZWZ0OiA0NSU7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDQ4cHg7XFxuICB9XFxuXFxuICAudHJpYW5nbGUge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogODUlO1xcbiAgICBsZWZ0OiA4MCU7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDMwcHg7XFxuICB9XFxuXFxuICAuc2NhbGUge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHJpZ2h0OiA4MCU7XFxuICAgIHRvcDogMTAlO1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiAzMHB4O1xcbiAgfVxcblxcbiAgLmNpcmNsZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiAzJTtcXG4gICAgcmlnaHQ6IDUwJTtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogMzBweDtcXG4gIH1cXG5cXG4gIC5jaGVtaXN0cnkge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogOCU7XFxuICAgIHJpZ2h0OiA1JTtcXG4gICAgd2lkdGg6IDM2cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwibWF4Q29udGFpbmVyXCI6IFwiVGVzdC1tYXhDb250YWluZXItMV9RQ09cIixcblx0XCJtYXJnaW5Cb3R0b20xMjZcIjogXCJUZXN0LW1hcmdpbkJvdHRvbTEyNi0yeS1USlwiLFxuXHRcInJlcG9ydHNfc3ViX3RpdGxlXCI6IFwiVGVzdC1yZXBvcnRzX3N1Yl90aXRsZS0xelU0VlwiLFxuXHRcInJlcG9ydHNfbm90aWNlXCI6IFwiVGVzdC1yZXBvcnRzX25vdGljZS0xazd4WVwiLFxuXHRcInJvdzFcIjogXCJUZXN0LXJvdzEtMXBCSVpcIixcblx0XCJwb2ludENvbnRhaW5lclwiOiBcIlRlc3QtcG9pbnRDb250YWluZXIteWw2clVcIixcblx0XCJwb2ludFwiOiBcIlRlc3QtcG9pbnQtMnlITUJcIixcblx0XCJwb2ludFRleHRcIjogXCJUZXN0LXBvaW50VGV4dC0zb215OVwiLFxuXHRcInJlZERvdFwiOiBcIlRlc3QtcmVkRG90LTNtS0gtXCIsXG5cdFwidGVzdHNJbWFnZVwiOiBcIlRlc3QtdGVzdHNJbWFnZS0zWjJkZVwiLFxuXHRcInNlY3Rpb25fY29udGFpbmVyXCI6IFwiVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bVwiLFxuXHRcImFzaEJhY2tncm91bmRcIjogXCJUZXN0LWFzaEJhY2tncm91bmQtcTFFLTNcIixcblx0XCJ2aWRlb1NsaWRlclwiOiBcIlRlc3QtdmlkZW9TbGlkZXItMjhfdWdcIixcblx0XCJtYXRoc1wiOiBcIlRlc3QtbWF0aHMtVHVjRVVcIixcblx0XCJtaWNyb3Njb3BlXCI6IFwiVGVzdC1taWNyb3Njb3BlLUhzYXRHXCIsXG5cdFwidHJpYW5nbGVcIjogXCJUZXN0LXRyaWFuZ2xlLTJCVUtZXCIsXG5cdFwic2NhbGVcIjogXCJUZXN0LXNjYWxlLTFjOUk0XCIsXG5cdFwiY2lyY2xlXCI6IFwiVGVzdC1jaXJjbGUtMmpXMXdcIixcblx0XCJjaGVtaXN0cnlcIjogXCJUZXN0LWNoZW1pc3RyeS0yZVlPV1wiLFxuXHRcImhlYWRlcmJhY2tncm91bmRtb2JpbGVcIjogXCJUZXN0LWhlYWRlcmJhY2tncm91bmRtb2JpbGUtM0ZOLTZcIixcblx0XCJzZWN0aW9uX3RpdGxlXCI6IFwiVGVzdC1zZWN0aW9uX3RpdGxlLTF1NnQwXCIsXG5cdFwic3ViX3NlY3Rpb25fdGl0bGVcIjogXCJUZXN0LXN1Yl9zZWN0aW9uX3RpdGxlLTNWUnUzXCIsXG5cdFwiY29udGVudFBhcnRcIjogXCJUZXN0LWNvbnRlbnRQYXJ0LXZPeFltXCIsXG5cdFwiY29udGVudFwiOiBcIlRlc3QtY29udGVudC0yaHM5c1wiLFxuXHRcInRleHRjb250ZW50XCI6IFwiVGVzdC10ZXh0Y29udGVudC0zcldmMVwiLFxuXHRcImF2YWlsYWJsZUNvbnRhaW5lclwiOiBcIlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyXCIsXG5cdFwiYXZhaWxhYmxlUm93XCI6IFwiVGVzdC1hdmFpbGFibGVSb3ctMlF5bmNcIixcblx0XCJkZXNrdG9wSW1hZ2VcIjogXCJUZXN0LWRlc2t0b3BJbWFnZS0ya1hOWFwiLFxuXHRcImpvaW5Db21tdW5pdHlcIjogXCJUZXN0LWpvaW5Db21tdW5pdHktM0xCVXNcIixcblx0XCJqb2luRmJUZXh0XCI6IFwiVGVzdC1qb2luRmJUZXh0LTFmZGhNXCIsXG5cdFwiam9pbkZiTGlua1wiOiBcIlRlc3Qtam9pbkZiTGluay1BdmNzZlwiLFxuXHRcImF2YWlsYWJsZVRpdGxlXCI6IFwiVGVzdC1hdmFpbGFibGVUaXRsZS0zTkVmQVwiLFxuXHRcImF2YWlsYWJsZVwiOiBcIlRlc3QtYXZhaWxhYmxlLTNsV0xYXCIsXG5cdFwiYXZhaWxhYmxlQ29udGVudFNlY3Rpb25cIjogXCJUZXN0LWF2YWlsYWJsZUNvbnRlbnRTZWN0aW9uLVVQNGxuXCIsXG5cdFwicGxhdGZvcm1Db250YWluZXJcIjogXCJUZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50XCIsXG5cdFwicGxhdGZvcm1cIjogXCJUZXN0LXBsYXRmb3JtLWtPeEFNXCIsXG5cdFwicGxhdGZvcm1Pc1wiOiBcIlRlc3QtcGxhdGZvcm1Pcy0zNlBrR1wiLFxuXHRcInJvd1wiOiBcIlRlc3Qtcm93LXNFLUNvXCIsXG5cdFwic3RvcmVcIjogXCJUZXN0LXN0b3JlLTNDeml6XCIsXG5cdFwicGxheXN0b3JlXCI6IFwiVGVzdC1wbGF5c3RvcmUtM1BLT0NcIixcblx0XCJhcHBzdG9yZVwiOiBcIlRlc3QtYXBwc3RvcmUtMWhPMTZcIixcblx0XCJzY3JvbGxUb3BcIjogXCJUZXN0LXNjcm9sbFRvcC0xNFM4Q1wiLFxuXHRcInRlYWNoQ29udGFpbmVyXCI6IFwiVGVzdC10ZWFjaENvbnRhaW5lci00bExPb1wiLFxuXHRcImhlYWRpbmdcIjogXCJUZXN0LWhlYWRpbmctMW9OczVcIixcblx0XCJsZWFybmluZ1RleHRcIjogXCJUZXN0LWxlYXJuaW5nVGV4dC0xUEJYM1wiLFxuXHRcImltYWdlUGFydFwiOiBcIlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZXCIsXG5cdFwiZW1wdHlDYXJkXCI6IFwiVGVzdC1lbXB0eUNhcmQtMk14SEFcIixcblx0XCJ0b3BDYXJkXCI6IFwiVGVzdC10b3BDYXJkLTNBX05vXCIsXG5cdFwiY29udGVudFRleHRcIjogXCJUZXN0LWNvbnRlbnRUZXh0LTJIcDZ5XCIsXG5cdFwiYnV0dG9ud3JhcHBlclwiOiBcIlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUlwiLFxuXHRcInJlcXVlc3REZW1vXCI6IFwiVGVzdC1yZXF1ZXN0RGVtby1FeVBIRlwiLFxuXHRcIndoYXRzYXBwd3JhcHBlclwiOiBcIlRlc3Qtd2hhdHNhcHB3cmFwcGVyLTNEbVRKXCIsXG5cdFwid2hhdHNhcHBcIjogXCJUZXN0LXdoYXRzYXBwLTMtaElyXCIsXG5cdFwiZG93bmxvYWRBcHBcIjogXCJUZXN0LWRvd25sb2FkQXBwLWg0ZENJXCIsXG5cdFwiZG93bmxvYWRUZXh0XCI6IFwiVGVzdC1kb3dubG9hZFRleHQtMktMdUFcIixcblx0XCJwbGF5U3RvcmVJY29uXCI6IFwiVGVzdC1wbGF5U3RvcmVJY29uLTN2YkZLXCIsXG5cdFwiYnJlYWRjcnVtXCI6IFwiVGVzdC1icmVhZGNydW0tMWoxNUVcIixcblx0XCJkaXNwbGF5Q2xpZW50c1wiOiBcIlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjVcIixcblx0XCJ0b3BTZWN0aW9uXCI6IFwiVGVzdC10b3BTZWN0aW9uLTIxYlYzXCIsXG5cdFwidGVhY2hTZWN0aW9uXCI6IFwiVGVzdC10ZWFjaFNlY3Rpb24tZ25rRnVcIixcblx0XCJ0ZWFjaEltZ0JveFwiOiBcIlRlc3QtdGVhY2hJbWdCb3gtMzJ0UFBcIixcblx0XCJ0ZWFjaFNlY3Rpb25OYW1lXCI6IFwiVGVzdC10ZWFjaFNlY3Rpb25OYW1lLTFmQUFyXCIsXG5cdFwiY2xpZW50c1dyYXBwZXJcIjogXCJUZXN0LWNsaWVudHNXcmFwcGVyLTJmRGVQXCIsXG5cdFwiY2xpZW50XCI6IFwiVGVzdC1jbGllbnQtRnpkQlpcIixcblx0XCJhY2hpZXZlZENvbnRhaW5lclwiOiBcIlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0ZcIixcblx0XCJhY2hpZXZlZEhlYWRpbmdcIjogXCJUZXN0LWFjaGlldmVkSGVhZGluZy1ydGRlT1wiLFxuXHRcImFjaGlldmVkUm93XCI6IFwiVGVzdC1hY2hpZXZlZFJvdy0xbENRZlwiLFxuXHRcImNhcmRcIjogXCJUZXN0LWNhcmQtVFpaY1JcIixcblx0XCJhY2hpZXZlZFByb2ZpbGVcIjogXCJUZXN0LWFjaGlldmVkUHJvZmlsZS0yNDdoWFwiLFxuXHRcImNsaWVudHNcIjogXCJUZXN0LWNsaWVudHMtMnAtY1hcIixcblx0XCJzdHVkZW50c1wiOiBcIlRlc3Qtc3R1ZGVudHMtM0M3cnVcIixcblx0XCJ0ZXN0c1wiOiBcIlRlc3QtdGVzdHMtMmdzMTdcIixcblx0XCJxdWVzdGlvbnNcIjogXCJUZXN0LXF1ZXN0aW9ucy0zdldXV1wiLFxuXHRcImhpZ2hsaWdodFwiOiBcIlRlc3QtaGlnaGxpZ2h0LVJ1YVlRXCIsXG5cdFwicXVlc3Rpb25zSGlnaGxpZ2h0XCI6IFwiVGVzdC1xdWVzdGlvbnNIaWdobGlnaHQtMl9WTlFcIixcblx0XCJ0ZXN0c0hpZ2hsaWdodFwiOiBcIlRlc3QtdGVzdHNIaWdobGlnaHQtMmJEMWZcIixcblx0XCJzdHVkZW50c0hpZ2hsaWdodFwiOiBcIlRlc3Qtc3R1ZGVudHNIaWdobGlnaHQtM3A0Y0pcIixcblx0XCJjbGllbnRzSGlnaGxpZ2h0XCI6IFwiVGVzdC1jbGllbnRzSGlnaGxpZ2h0LXlDbERvXCIsXG5cdFwic3ViVGV4dFwiOiBcIlRlc3Qtc3ViVGV4dC0yUThSc1wiLFxuXHRcInNlY3Rpb25fY29udGVudHNcIjogXCJUZXN0LXNlY3Rpb25fY29udGVudHMtMldBU1dcIixcblx0XCJzZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlXCI6IFwiVGVzdC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTFJS0ZvXCIsXG5cdFwiYm90dG9tQ2lyY2xlXCI6IFwiVGVzdC1ib3R0b21DaXJjbGUtMlNFQ09cIixcblx0XCJ0b3BDaXJjbGVcIjogXCJUZXN0LXRvcENpcmNsZS0yQjJIUFwiLFxuXHRcIm9ubGluZVRlc3RzXCI6IFwiVGVzdC1vbmxpbmVUZXN0cy0yaTRsdVwiLFxuXHRcIm93blBhcGVyc1wiOiBcIlRlc3Qtb3duUGFwZXJzLTFHeTByXCIsXG5cdFwicXVlc3Rpb25CYW5rXCI6IFwiVGVzdC1xdWVzdGlvbkJhbmstbC02bU5cIixcblx0XCJ2aWRlb0NvbnRlbnRzXCI6IFwiVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDXCIsXG5cdFwidmlkZW9fc2VjdGlvblwiOiBcIlRlc3QtdmlkZW9fc2VjdGlvbi0yQzJQRFwiLFxuXHRcImNvbnRlbnRfdGl0bGVcIjogXCJUZXN0LWNvbnRlbnRfdGl0bGUtMURPdlhcIixcblx0XCJjb250ZW50X3RleHRcIjogXCJUZXN0LWNvbnRlbnRfdGV4dC1RS0ZIcVwiLFxuXHRcImhpZGVcIjogXCJUZXN0LWhpZGUtM3ZQUVVcIixcblx0XCJzaG93XCI6IFwiVGVzdC1zaG93LUxUT0hvXCIsXG5cdFwibW9iaWxlUGFwZXJzQ29udGFpbmVyXCI6IFwiVGVzdC1tb2JpbGVQYXBlcnNDb250YWluZXItMUdQT3VcIixcblx0XCJwYXBlcnNXcmFwcGVyXCI6IFwiVGVzdC1wYXBlcnNXcmFwcGVyLTFhSU5kXCJcbn07IiwiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VSZWYsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBSZWFjdFBsYXllciBmcm9tICdyZWFjdC1wbGF5ZXInO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzIGZyb20gJy4vU2xpZGVyUGxheWVyLnNjc3MnO1xuXG5jb25zdCBtb3ZlID0ge1xuICAwOiBzLnNsaWRlT3V0LFxuICAxOiBzLnNsaWRlT3V0MSxcbiAgMjogcy5zbGlkZU91dDIsXG4gIDM6IHMuc2xpZGVPdXQzLFxufTtcblxuY29uc3QgU2xpZGVyUGxheWVyID0gcHJvcHMgPT4ge1xuICBjb25zdCBnZXRJbm5lcldpZHRoID0gKCkgPT4ge1xuICAgIGNvbnN0IGlzV2luZG93QXZhaWxhYmxlID0gdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCc7XG4gICAgaWYgKGlzV2luZG93QXZhaWxhYmxlKSB7XG4gICAgICByZXR1cm4gd2luZG93LmlubmVyV2lkdGg7XG4gICAgfVxuICAgIHJldHVybiAxMDAwO1xuICB9O1xuXG4gIGNvbnN0IFthY3RpdmUsIHNldEFjdGl2ZV0gPSB1c2VTdGF0ZSgwKTtcbiAgY29uc3QgW3NlZWtWYWx1ZSwgc2V0U2Vla1ZhbHVlXSA9IHVzZVN0YXRlKDApO1xuICBjb25zdCBbaXNNb2JpbGUsIHNldElzTW9iaWxlXSA9IHVzZVN0YXRlKGZhbHNlKTtcbiAgY29uc3QgcGxheWVyUmVmID0gdXNlUmVmKG51bGwpO1xuICBjb25zdCBbdmlkZW9zLCBzZXRWaWRlb3NdID0gdXNlU3RhdGUoWy4uLnByb3BzLnZpZGVvc0xpc3RdKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICBjb25zdCB7IHZpZGVvc0xpc3QgfSA9IHByb3BzO1xuICBjb25zdCBhY3RpdmVWaWRlb09iaiA9IGlzTW9iaWxlID8gdmlkZW9zW2FjdGl2ZV0gOiB2aWRlb3NMaXN0W2FjdGl2ZV07XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBjb25zdCByZXNpemUgPSAoKSA9PiB7XG4gICAgICBzZXRJc01vYmlsZShnZXRJbm5lcldpZHRoKCkgPCA5OTEpO1xuICAgIH07XG4gICAgcmVzaXplKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHJlc2l6ZSk7XG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCByZXNpemUpO1xuICAgIH07XG4gIH0sIFtdKTtcblxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJvb3R9PlxuICAgICAgPFJlYWN0UGxheWVyXG4gICAgICAgIHJlZj17cGxheWVyUmVmfVxuICAgICAgICB3aWR0aD17aXNNb2JpbGUgPyAnMTAwJScgOiAnNTAwcHgnfVxuICAgICAgICBoZWlnaHQ9e2lzTW9iaWxlID8gJzI2N3B4JyA6ICc0MDBweCd9XG4gICAgICAgIHN0eWxlPXtzLnZpZGVvfVxuICAgICAgICB1cmw9e2FjdGl2ZVZpZGVvT2JqLnVybH1cbiAgICAgICAgY29udHJvbHM9e2ZhbHNlfVxuICAgICAgICBjb25maWc9e3tcbiAgICAgICAgICBmaWxlOiB7XG4gICAgICAgICAgICBhdHRyaWJ1dGVzOiB7XG4gICAgICAgICAgICAgIGF1dG9QbGF5OiB0cnVlLFxuICAgICAgICAgICAgICBtdXRlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgZGlzYWJsZVBpY3R1cmVJblBpY3R1cmU6IHRydWUsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIH0sXG4gICAgICAgIH19XG4gICAgICAgIG9uUHJvZ3Jlc3M9e3N0YXRlID0+IHtcbiAgICAgICAgICBjb25zdCBzZWVrID0gTWF0aC5jZWlsKFxuICAgICAgICAgICAgKHN0YXRlLnBsYXllZFNlY29uZHMgKiAxMDApIC8gcGxheWVyUmVmLmN1cnJlbnQuZ2V0RHVyYXRpb24oKSxcbiAgICAgICAgICApO1xuICAgICAgICAgIHNldFNlZWtWYWx1ZShzZWVrKTtcbiAgICAgICAgfX1cbiAgICAgICAgb25FbmRlZD17KCkgPT4ge1xuICAgICAgICAgIHNldFNlZWtWYWx1ZSgxMDApO1xuICAgICAgICAgIGlmIChpc01vYmlsZSkge1xuICAgICAgICAgICAgY29uc3QgdGl0bGVzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShzLnZpZGVvVGl0bGUpO1xuICAgICAgICAgICAgLy8gY29uc3QgbGFzdE9uZSA9IHZpZGVvcy5zaGlmdCgpO1xuICAgICAgICAgICAgLy8gdmlkZW9zLnB1c2gobGFzdE9uZSk7XG4gICAgICAgICAgICBzZXRTZWVrVmFsdWUoMCk7XG4gICAgICAgICAgICBjb25zdCBwcmV2QWN0aXZlID0gYWN0aXZlO1xuICAgICAgICAgICAgY29uc3QgY3VycmVudEFjdGl2ZSA9IGFjdGl2ZSArIDEgPT09IDQgPyAwIDogYWN0aXZlICsgMTtcbiAgICAgICAgICAgIHNldEFjdGl2ZShjdXJyZW50QWN0aXZlKTtcbiAgICAgICAgICAgIEFycmF5LmZyb20odGl0bGVzKS5mb3JFYWNoKGVsZSA9PiB7XG4gICAgICAgICAgICAgIGVsZS5jbGFzc0xpc3QuYWRkKG1vdmVbY3VycmVudEFjdGl2ZV0pO1xuICAgICAgICAgICAgICBlbGUuY2xhc3NMaXN0LnJlbW92ZShtb3ZlW3ByZXZBY3RpdmVdKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zdCBpc0xhc3QgPSBhY3RpdmUgPT09IHZpZGVvc0xpc3QubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgIGlmIChpc0xhc3QpIHtcbiAgICAgICAgICAgICAgc2V0U2Vla1ZhbHVlKDApO1xuICAgICAgICAgICAgICBzZXRBY3RpdmUoMCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBzZXRTZWVrVmFsdWUoMCk7XG4gICAgICAgICAgICAgIHNldEFjdGl2ZShhY3RpdmUgKyAxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH19XG4gICAgICAvPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udHJvbHNXcmFwcGVyfT5cbiAgICAgICAge2lzTW9iaWxlID8gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vYmlsZUNvbnRyb2xzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLm91dGVyfSAke3MubW9iaWxlVHJhY2tlcn1gfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW5uZXJ9IHN0eWxlPXt7IHdpZHRoOiBgJHtzZWVrVmFsdWV9JWAgfX0gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9iaWxlVGl0bGVzV3JhcHBlcn0+XG4gICAgICAgICAgICAgIHt2aWRlb3MubWFwKCh2aWRlbywgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBhY3QgPSBhY3RpdmUgPT09IGluZGV4O1xuICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgICAgIGFjdCA/IGAke3MudmlkZW9UaXRsZX0gJHtzLmFjdFRpdGxlfWAgOiBgJHtzLnZpZGVvVGl0bGV9YFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHt2aWRlby50aXRsZX1cbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApIDogKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRlc2t0b3BDb250cm9sc30+XG4gICAgICAgICAgICB7dmlkZW9zTGlzdC5tYXAoKHZpZGVvT2JqLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICBjb25zdCBjdXJyZW50U2VlayA9IGluZGV4ID09PSBhY3RpdmUgPyBzZWVrVmFsdWUgOiAwO1xuICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRyb2xzfT5cbiAgICAgICAgICAgICAgICAgIHshaXNNb2JpbGUgPyAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm91dGVyfT5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuaW5uZXJ9XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogYCR7Y3VycmVudFNlZWt9JWAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgICAgICAgICAgey8qIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICByZWY9e2luZGV4ID09PSBhY3RpdmUgPyBzbGlkZXJSZWYgOiBudWxsfVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Muc2Vla0Jhcn1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2N1cnJlbnRTZWVrfVxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicmFuZ2VcIlxuICAgICAgICAgICAgICAgICAgICBtaW49ezB9XG4gICAgICAgICAgICAgICAgICAgIG1heD17MTAwfVxuICAgICAgICAgICAgICAgICAgLz4gKi99XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudmlkZW9UaXRsZX0+e3ZpZGVvT2JqLnRpdGxlfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0pfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG59O1xuU2xpZGVyUGxheWVyLnByb3BUeXBlcyA9IHtcbiAgdmlkZW9zTGlzdDogUHJvcFR5cGVzLmFycmF5T2YoXG4gICAgUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICApLFxufTtcblNsaWRlclBsYXllci5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZGVvc0xpc3Q6IFtdLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShTbGlkZXJQbGF5ZXIpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9TbGlkZXJQbGF5ZXIuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9TbGlkZXJQbGF5ZXIuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1NsaWRlclBsYXllci5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsvTGluayc7XG5pbXBvcnQgU2xpZGVyUGxheWVyIGZyb20gJ2NvbXBvbmVudHMvU2xpZGVyLVBsYXllci9TbGlkZXJQbGF5ZXInO1xuaW1wb3J0IHtcbiAgb25saW5lVGVzdFBvaW50cyxcbiAgb3duUGFwZXJzLFxuICBxdWVzdGlvbkJhbmssXG4gIG1hcmtzQmFzZWRBbmFseXNpcyxcbiAgZXJyb3JBbmFseXNpcyxcbiAgY29tcGFyaXNpb25BbmFseXNpcyxcbiAgY29uY2VwdEFuYWx5c2lzLFxuICBwYXBlcnMsXG4gIHJlcG9ydHMsXG59IGZyb20gJy4vY29uc3RhbnRzJztcbmltcG9ydCB7XG4gIFBMQVRGT1JNUyxcbiAgSE9NRV9DTElFTlRTX1VQREFURUQsXG4gIFRFU1RfVklERU9TLFxufSBmcm9tICcuLi9HZXRSYW5rc0NvbnN0YW50cyc7XG5pbXBvcnQgcyBmcm9tICcuL1Rlc3Quc2Nzcyc7XG5cbmNsYXNzIFRlc3QgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAvLyBUb2dnbGUgQnV0dG9uIENvZGVcbiAgLyogZGlzcGxheVRvZ2dsZSA9ICgpID0+IChcbiAgICA8ZGl2XG4gICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgIHRoaXMuc3RhdGUubGl2ZVRvZ2dsZVxuICAgICAgICAgID8gYCR7cy50b2dnbGVfb3V0ZXJ9ICR7cy50b2dnbGVfb259YFxuICAgICAgICAgIDogcy50b2dnbGVfb3V0ZXJcbiAgICAgIH1cbiAgICA+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b2dnbGVfaW5uZXJ9IC8+XG4gICAgPC9kaXY+XG4gICk7ICovXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICAgIGlzTW9iaWxlOiBmYWxzZSxcbiAgICB9O1xuICB9XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgdGhpcy5oYW5kbGVSZXNpemUoKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZVJlc2l6ZSk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZVJlc2l6ZSk7XG4gIH1cblxuICBoYW5kbGVSZXNpemUgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgOTkxKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaXNNb2JpbGU6IHRydWUsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGlzTW9iaWxlOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5zY3JvbGxZID4gNTAwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlU2Nyb2xsVG9wID0gKCkgPT4ge1xuICAgIHdpbmRvdy5zY3JvbGxUbyh7XG4gICAgICB0b3A6IDAsXG4gICAgICBiZWhhdmlvcjogJ3Ntb290aCcsXG4gICAgfSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICB9KTtcbiAgfTtcbiAgLy8gVGVhY2ggU2VjdGlvblxuICBkaXNwbGF5VGVhY2hTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1heENvbnRhaW5lcn0+XG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5icmVhZGNydW19PlxuICAgICAgICAgIEhvbWUgLyA8c3Bhbj5PbmxpbmUgVGVzdHM8L3NwYW4+XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wU2VjdGlvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hTZWN0aW9ufT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoSW1nQm94fT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L3Rlc3Quc3ZnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJ0ZWFjaFwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZWFjaFNlY3Rpb25OYW1lfT5PbmxpbmUgVGVzdHM8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8aDIgY2xhc3NOYW1lPXtzLmhlYWRpbmd9PlxuICAgICAgICAgIFBsYW4sIGRlc2lnbiwgZGVsaXZlciBhbmQgbWFyayZuYnNwO1xuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlYXJuaW5nVGV4dH0+XG4gICAgICAgICAgICA8c3Bhbj5hc3Nlc3NtZW50czwvc3Bhbj5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL2xpdmUgY2xhc3Nlcy91bmRlcnNjb3JlX2Zvcl90aXRsZS5wbmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJ1bmRlcnNjb3JlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgJm5ic3A7c2VhbWxlc3NseSAtIG9uc2l0ZSBvciByZW1vdGVseS5cbiAgICAgICAgPC9oMj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPXtzLmNvbnRlbnRUZXh0fT5cbiAgICAgICAgICBBbiBlbmQtdG8tZW5kIGFzc2Vzc21lbnQgcGxhdGZvcm0gdGhhdCBzdXBwb3J0IHRlYWNoZXJzLCBkYXRhIGVudHJ5XG4gICAgICAgICAgb3BlcmF0b3JzIHdpdGggY3V0dGluZyBlZGdlIGZlYXR1cmVzIHRoYXQgY292ZXIgYWxsIHlvdXIgYXNzZXNzbWVudFxuICAgICAgICAgIG5lZWRzIGVudGlyZWx5IG9uLXNjcmVlbi5cbiAgICAgICAgPC9wPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5idXR0b253cmFwcGVyfT5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBjbGFzc05hbWU9e3MucmVxdWVzdERlbW99XG4gICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZVNob3dUcmlhbH1cbiAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIEdFVCBTVUJTQ1JJUFRJT05cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8YVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLndoYXRzYXBwd3JhcHBlcn1cbiAgICAgICAgICAgIGhyZWY9XCJodHRwczovL2FwaS53aGF0c2FwcC5jb20vc2VuZD9waG9uZT05MTg4MDA3NjQ5MDkmdGV4dD1IaSBHZXRSYW5rcywgSSB3b3VsZCBsaWtlIHRvIGtub3cgbW9yZSBhYm91dCB5b3VyIE9ubGluZSBQbGF0Zm9ybS5cIlxuICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLndoYXRzYXBwfT5DaGF0IG9uPC9kaXY+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0c2FwcF9sb2dvLnN2Z1wiIGFsdD1cIndoYXRzYXBwXCIgLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9e3MuYWN0aW9uc1dyYXBwZXJ9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjdGlvbn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY3Rpb25pbWdib3h9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9wbGF5X3B1cnBsZS5zdmdcIiBhbHQ9XCJ3YXRjaCB2aWRlb1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxzcGFuPldhdGNoIHZpZGVvPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjdGlvbn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY3Rpb25pbWdib3h9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9icm9jaHVyZV9wdXJwbGUuc3ZnXCIgYWx0PVwiYnJvY2h1cmVcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8c3Bhbj5Eb3dubG9hZCBicm9jaHVyZTwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+ICovfVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3dubG9hZEFwcH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG93bmxvYWRUZXh0fT5Eb3dubG9hZCB0aGUgYXBwPC9kaXY+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3BsYXkuZ29vZ2xlLmNvbS9zdG9yZS9hcHBzL2RldGFpbHM/aWQ9Y29tLmVnbmlmeS5nZXRyYW5rcyZobD1lbl9JTiZnbD1VU1wiXG4gICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucGxheVN0b3JlSWNvbn1cbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL3BsYXRmb3Jtcy9wbGF5U3RvcmUud2VicFwiXG4gICAgICAgICAgICAgIGFsdD1cInBsYXlfc3RvcmVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlfT5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9oZXJvX2ltZy53ZWJwXCIgYWx0PVwibW9iaWxlX2JhY2tncm91bmRcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDbGllbnRzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRpc3BsYXlDbGllbnRzfT5cbiAgICAgIDxoMz5cbiAgICAgICAgVHJ1c3RlZCBieSA8c3BhbiAvPlxuICAgICAgICBsZWFkaW5nIEVkdWNhdGlvbmFsIEluc3RpdHV0aW9uc1xuICAgICAgPC9oMz5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNsaWVudHNXcmFwcGVyfT5cbiAgICAgICAge0hPTUVfQ0xJRU5UU19VUERBVEVELm1hcChjbGllbnQgPT4gKFxuICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgIGNsYXNzTmFtZT17cy5jbGllbnR9XG4gICAgICAgICAgICBrZXk9e2NsaWVudC5pZH1cbiAgICAgICAgICAgIHNyYz17Y2xpZW50Lmljb259XG4gICAgICAgICAgICBhbHQ9XCJjbGluZXRcIlxuICAgICAgICAgIC8+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgICA8c3Bhbj5cbiAgICAgICAgPExpbmsgdG89XCIvY3VzdG9tZXJzXCI+Y2xpY2sgaGVyZSBmb3IgbW9yZS4uLjwvTGluaz5cbiAgICAgIDwvc3Bhbj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5QWNoaWV2ZWRTb0ZhciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZENvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZEhlYWRpbmd9PlxuICAgICAgICBXaGF0IHdlIGhhdmUgYWNoaWV2ZWQgPHNwYW4gLz5cbiAgICAgICAgc28gZmFyXG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkUm93fT5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDI1NSwgMTAyLCAwLCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MuY2xpZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvY2xpZW50cy5zdmdcIiBhbHQ9XCJwcm9maWxlXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MuY2xpZW50c0hpZ2hsaWdodH1gfT4xNTArPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPkNsaWVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5zdHVkZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvc3R1ZGVudHMuc3ZnXCIgYWx0PVwic3R1ZGVudHNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5zdHVkZW50c0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDMgTGFraCtcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+U3R1ZGVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMCwgMTE1LCAyNTUsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy50ZXN0c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvdGVzdHMuc3ZnXCIgYWx0PVwidGVzdHNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy50ZXN0c0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDMgTWlsbGlvbitcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+VGVzdHMgQXR0ZW1wdGVkPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDAsIDE3MiwgMzgsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5xdWVzdGlvbnN9YH0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3F1ZXN0aW9ucy5zdmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJxdWVzdGlvbnNcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MucXVlc3Rpb25zSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMjAgTWlsbGlvbitcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnN1YlRleHR9PlF1ZXN0aW9ucyBTb2x2ZWQ8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVBsYXllclNlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9TbGlkZXJ9PlxuICAgICAgPFNsaWRlclBsYXllciB2aWRlb3NMaXN0PXtURVNUX1ZJREVPU30gLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLm1hdGhzfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvbWF0aHMuc3ZnXCJcbiAgICAgICAgYWx0PVwibWF0aHNcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLm1pY3Jvc2NvcGV9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy9taWNyb3Njb3BlLnN2Z1wiXG4gICAgICAgIGFsdD1cIm1pY3Jvc2NvcGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLnRyaWFuZ2xlfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvdHJpYW5nbGUuc3ZnXCJcbiAgICAgICAgYWx0PVwidHJpYW5nbGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLnNjYWxlfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvc2NhbGUuc3ZnXCJcbiAgICAgICAgYWx0PVwic2NhbGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLmNpcmNsZX1cbiAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9zdWJqZWN0X2ljb25zL2NpcmNsZS5zdmdcIlxuICAgICAgICBhbHQ9XCJjaXJjbGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLmNoZW1pc3RyeX1cbiAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9zdWJqZWN0X2ljb25zL2NoZW1pc3RyeS5zdmdcIlxuICAgICAgICBhbHQ9XCJjaGVtaXN0cnlcIlxuICAgICAgLz5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5T25saW5lVGVzdHMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0c0ltYWdlfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9UZXN0c19IZWFkU2VjdGlvbi5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+T25saW5lIFRlc3RzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICB7b25saW5lVGVzdFBvaW50cy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9vbmxpbmUtdGVzdHMud2VicFwiIGFsdD1cIm9ubGluZS10ZXN0c1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5T3duUGFwZXJzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PlxuICAgICAgICAgICAgICBVcGxvYWQgeW91ciBvd24gUXVlc3Rpb24gcGFwZXJzXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBhcGVyc1dyYXBwZXJ9PlxuICAgICAgICAgICAgICB7b3duUGFwZXJzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5vd25QYXBlcnN9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9vd24tcGFwZXJzLndlYnBcIiBhbHQ9XCJvd24tcGFwZXJzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlRdWVzdGlvbkJhbmsgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0c0ltYWdlfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9UZXN0c19IZWFkU2VjdGlvbi5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+SW5idWlsdCBRdWVzdGlvbnMgQmFuayA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIHtxdWVzdGlvbkJhbmsubWFwKHBvaW50ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb3cxfSAke3MucG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludENvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZERvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLnF1ZXN0aW9uQmFua31gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L3F1ZXN0aW9uQmFuay53ZWJwXCIgYWx0PVwib25saW5lLXRlc3RzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlQYXBlcnMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVzdHNJbWFnZX0+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1Rlc3QvQW5hbHlzaXNfSGVhZFNlY3Rpb24uc3ZnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJvbmxpbmUtdGVzdHNcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PlBhcGVyczwvZGl2PlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAge3BhcGVycy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICB7cG9pbnQuc3ViUG9pbnRzID8gKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAge3BvaW50LnN1YlBvaW50cy5tYXAodGV4dCA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pnt0ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgeyF0aGlzLnN0YXRlLmlzTW9iaWxlID8gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm9ubGluZVRlc3RzfWB9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvYW5hbHlzaXMtY2FyZC5zdmdcIiBhbHQ9XCJwYXBlcnNcIiAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSA6IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2JpbGVQYXBlcnNDb250YWluZXJ9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvbW9iaWxlX3BhcGVycy5zdmdcIiBhbHQ9XCJtb2JsZV9wYXBlcnNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUFuYWx5c2lzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2V9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVzdHNJbWFnZX0+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1Rlc3QvQW5hbHlzaXNfSGVhZFNlY3Rpb24uc3ZnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJvbmxpbmUtdGVzdHNcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PkFuYWx5c2lzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJfc2VjdGlvbl90aXRsZX0+TWFya3MtYmFzZWQgQW5hbHlzaXM8L2Rpdj5cbiAgICAgICAgICAgICAge21hcmtzQmFzZWRBbmFseXNpcy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9YH0+XG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmVtcHR5Q2FyZH0gJHtzLm1hcmdpbkJvdHRvbTEyNn0gJHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5pc01vYmlsZSA/IHMuc2hvdyA6IHMuaGlkZVxuICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1Rlc3QvbWFya3NBbmFseXNpcy53ZWJwXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJtYXJrcy1hbmFseXNpc1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Yl9zZWN0aW9uX3RpdGxlfT5Db21wYXJpc2lvbiBBbmFseXNpczwvZGl2PlxuICAgICAgICAgICAgICB7Y29tcGFyaXNpb25BbmFseXNpcy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViX3NlY3Rpb25fdGl0bGV9PkVycm9yIEFuYWx5c2lzPC9kaXY+XG4gICAgICAgICAgICAgIHtlcnJvckFuYWx5c2lzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5vbmxpbmVUZXN0c31gfT5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuZW1wdHlDYXJkfSAke3MubWFyZ2luQm90dG9tMTI2fSAke1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmlzTW9iaWxlID8gcy5zaG93IDogcy5oaWRlXG4gICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9jb25jZXB0QW5hbHlzaXMud2VicFwiXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiY29uY2VwdC1hbmFseXNpc1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Yl9zZWN0aW9uX3RpdGxlfT5Db25jZXB0LWJhc2VkIEFuYWx5c2lzPC9kaXY+XG4gICAgICAgICAgICAgIHtjb25jZXB0QW5hbHlzaXMubWFwKHBvaW50ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb3cxfSAke3MucG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludENvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZERvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm9ubGluZVRlc3RzfWB9PlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5lbXB0eUNhcmR9ICR7cy5tYXJnaW5Cb3R0b20xMjZ9ICR7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuaXNNb2JpbGUgPyBzLnNob3cgOiBzLmhpZGVcbiAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L2Vycm9yQW5hbHlzaXMud2VicFwiXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiZXJyb3ItYW5hbHlzaXNcIlxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm9ubGluZVRlc3RzfSAke1xuICAgICAgICAgICAgdGhpcy5zdGF0ZS5pc01vYmlsZSA/IHMuaGlkZSA6IHMuc2hvd1xuICAgICAgICAgIH1gfVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZW1wdHlDYXJkfSAke3MubWFyZ2luQm90dG9tMTI2fWB9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L21hcmtzQW5hbHlzaXMud2VicFwiIGFsdD1cIm1hcmtzLWFuYWx5c2lzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmVtcHR5Q2FyZH0gJHtzLm1hcmdpbkJvdHRvbTEyNn1gfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L2NvbmNlcHRBbmFseXNpcy53ZWJwXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJjb25jZXB0LWFuYWx5c2lzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmVtcHR5Q2FyZH1gfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9lcnJvckFuYWx5c2lzLndlYnBcIiBhbHQ9XCJlcnJvci1hbmFseXNpc1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5UmVwb3J0cyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0c0ltYWdlfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9SZXBvcnRzX0hlYWRTZWN0aW9uLnN2Z1wiXG4gICAgICAgICAgICAgICAgYWx0PVwib25saW5lLXRlc3RzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5SZXBvcnRzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZXBvcnRzX3N1Yl90aXRsZX0+OSBJbmZvcm1hdGl2ZSBSZXBvcnRzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZXBvcnRzX25vdGljZX0+XG4gICAgICAgICAgICAgIChEb3dubG9hZGFibGUgZm9yIE5vdGljZSBCb2FyZClcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAge3JlcG9ydHMubWFwKHBvaW50ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb3cxfSAke3MucG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludENvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZERvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm9ubGluZVRlc3RzfWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvcmVwb3J0cy53ZWJwXCIgYWx0PVwicmVwb3J0c1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5VGVzdHNTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGFpbmVyfWB9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Muc2VjdGlvbl9jb250ZW50c30gJHtzLm1heENvbnRhaW5lcn1gfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PlxuICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWwgSkVFXG4gICAgICAgICAgICAgIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aCBvbmxpbmUgYW5kXG4gICAgICAgICAgICAgIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYSB3aG9sZSBmb3IgYWxsIHRoZVxuICAgICAgICAgICAgICBzdHVkZW50c1xuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9IGN1c3RvbS1zY3JvbGxiYXJgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvZGVtby5wbmdcIiBhbHQ9XCJkZW1vXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb0NvbnRlbnRzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+SW5kZXBlbmRlbmNlPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIFlvdXIgWm9vbSBhY2NvdW50IHdpbGwgYmUgdXNlZC4gQWxsIHRoZSBmZWF0dXJlcyBhbmQgcHJvdmlzaW9uc1xuICAgICAgICAgICAgICAgIG9mIHlvdXIgWm9vbSBhY2NvdW50IHNoYWxsIGJlIGF2YWlsYWJsZSB3aXRoIGFuIGFkZGVkIGFkdmFudGFnZVxuICAgICAgICAgICAgICAgIG9mIGxvZ2luIHJlc3RyaWN0aW9ucyBvbmx5IGZvciBzZWxlY3RlZCBzdHVkZW50cy5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+RmxleGliaWxpdHk8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgQW55IG51bWJlciBvZiBab29tIGFjY291bnRzIGNhbiBiZSB1c2VkLiBEaWZmZXJlbnQgWm9vbSBhY2NvdW50c1xuICAgICAgICAgICAgICAgIGNhbiBiZSBtYXBwZWQgd2l0aCBkaWZmZXJlbnQgQ2xhc3MgU2Vzc2lvbnMgdGh1cyBhbGxvd2luZ1xuICAgICAgICAgICAgICAgIG11bHRpcGxlIHRlYWNoZXJzIHRvIHVzZSB0aGVpciBwZXJzb25hbCBhY2NvdW50cy5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+U2VjdXJlIEFjY2Vzczwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBPbmx5IHNlbGVjdGVkIHN0dWRlbnRzIHdobyBoYXZlIGFjY2VzcyB0byB0aGUgQXBwIGNhbiBub3cgYWNjZXNzXG4gICAgICAgICAgICAgICAgWm9vbSBjbGFzcywgbm8gbmVlZCB0byBzaGFyZSBNZWV0aW5nIElEIGV2ZXJ5dGltZSwgTm8gcmFuZG9tXG4gICAgICAgICAgICAgICAgYWNjZXNzLlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5NdWx0aXBsZSB1c2VzIG9mIFpvb208L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgWm9vbSBmYWNpbGl0eSBhdmFpbGFibGUgZm9yIGxpdmUgQ2xhc3MgYW5kIERvdWJ0cyBtb2R1bGVzLiBTb29uXG4gICAgICAgICAgICAgICAgWm9vbSBpcyBiZWluZyBpbnRlZ3JhdGVkIHRvIHRoZSBvbmxpbmUgRXhhbSBtb2R1bGUgZm9yIG9ubGluZVxuICAgICAgICAgICAgICAgIEV4YW0gcHJvY3RvcmluZy5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICAvLyBMZWN0dXJlIHNlY3Rpb25cbiAgZGlzcGxheUFuYWx5c2lzU2VjdGlvbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRhaW5lcn0gJHtzLmFzaEJhY2tncm91bmR9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRlbnRzfSAke3MubWF4Q29udGFpbmVyfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+XG4gICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbCBKRUVcbiAgICAgICAgICAgICAgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoIG9ubGluZSBhbmRcbiAgICAgICAgICAgICAgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhIHdob2xlIGZvciBhbGwgdGhlXG4gICAgICAgICAgICAgIHN0dWRlbnRzXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gY3VzdG9tLXNjcm9sbGJhcmB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9kZW1vLnBuZ1wiIGFsdD1cImRlbW9cIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvQ29udGVudHN9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5UHJhY3RpY2UgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e2Ake3Muc2VjdGlvbl9jb250YWluZXJ9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRlbnRzfSAke3MubWF4Q29udGFpbmVyfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+XG4gICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbCBKRUVcbiAgICAgICAgICAgICAgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoIG9ubGluZSBhbmRcbiAgICAgICAgICAgICAgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhIHdob2xlIGZvciBhbGwgdGhlXG4gICAgICAgICAgICAgIHN0dWRlbnRzXG4gICAgICAgICAgICAgIHsvKiB7VEVTVFNfS0VZX1BPSU5UUy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYga2V5PXtwb2ludC5pZH0gY2xhc3NOYW1lPXtzLnBvaW50fT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZGRvdHdyYXBwZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWRkb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnBvaW50fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX0gKi99XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIHsvKiA8YSBjbGFzc05hbWU9e2Ake3Mudmlld21vcmV9ICR7cy5tYXJnaW5MZWZ0fWB9IGhyZWY9XCIvI1wiPlxuICAgICAgICAgICAgICBWaWV3IG1vcmUgPGltZyBzcmM9XCJpbWFnZXMvaWNvbnMvcmlnaHRhcnJvd19ibHVlLnN2Z1wiIGFsdD1cIlwiIC8+XG4gICAgICAgICAgICA8L2E+ICovfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSBjdXN0b20tc2Nyb2xsYmFyYH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2RlbW8ucG5nXCIgYWx0PVwiZGVtb1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9Db250ZW50c30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5UGFwZXJzU2VjdGlvbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRhaW5lcn0gJHtzLmFzaEJhY2tncm91bmR9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRlbnRzfSAke3MubWF4Q29udGFpbmVyfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+XG4gICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbCBKRUVcbiAgICAgICAgICAgICAgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoIG9ubGluZSBhbmRcbiAgICAgICAgICAgICAgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhIHdob2xlIGZvciBhbGwgdGhlXG4gICAgICAgICAgICAgIHN0dWRlbnRzXG4gICAgICAgICAgICAgIHsvKiB7VEVTVFNfS0VZX1BPSU5UUy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYga2V5PXtwb2ludC5pZH0gY2xhc3NOYW1lPXtzLnBvaW50fT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZGRvdHdyYXBwZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWRkb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnBvaW50fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX0gKi99XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIHsvKiA8YSBjbGFzc05hbWU9e2Ake3Mudmlld21vcmV9ICR7cy5tYXJnaW5MZWZ0fWB9IGhyZWY9XCIvI1wiPlxuICAgICAgICAgICAgICBWaWV3IG1vcmUgPGltZyBzcmM9XCJpbWFnZXMvaWNvbnMvcmlnaHRhcnJvd19ibHVlLnN2Z1wiIGFsdD1cIlwiIC8+XG4gICAgICAgICAgICA8L2E+ICovfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSBjdXN0b20tc2Nyb2xsYmFyYH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2RlbW8ucG5nXCIgYWx0PVwiZGVtb1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9Db250ZW50c30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5QXZhaWxhYmxlT24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZVJvd30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZUNvbnRlbnRTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdmFpbGFibGVUaXRsZX0+XG4gICAgICAgICAgICBXZeKAmXJlIDxiciAvPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmF2YWlsYWJsZX0+YXZhaWxhYmxlPC9zcGFuPiBvblxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJvd30+XG4gICAgICAgICAgICB7UExBVEZPUk1TLm1hcChpdGVtID0+IChcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhdGZvcm1Db250YWluZXJ9PlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtpdGVtLmljb259IGFsdD1cIlwiIGhlaWdodD1cIjQwcHhcIiB3aWR0aD1cIjQwcHhcIiAvPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYXRmb3JtfT57aXRlbS5sYWJlbH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGF0Zm9ybU9zfT57aXRlbS5hdmFpbGFibGV9PC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3RvcmV9PlxuICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vcGxheS5nb29nbGUuY29tL3N0b3JlL2FwcHMvZGV0YWlscz9pZD1jb20uZWduaWZ5LmdldHJhbmtzJmhsPWVuX0lOJmdsPVVTXCJcbiAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXlTdG9yZS53ZWJwXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJQbGF5IFN0b3JlXCJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MucGxheXN0b3JlfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL2FwcFN0b3JlLndlYnBcIlxuICAgICAgICAgICAgICBhbHQ9XCJBcHAgU3RvcmVcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuYXBwc3RvcmV9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8aW1nXG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmRlc2t0b3BJbWFnZX1cbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXRmb3Jtc192Mi53ZWJwXCJcbiAgICAgICAgICBhbHQ9XCJwbGF0Zm9ybVwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Sm9pbkZhY2Vib29rID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmpvaW5Db21tdW5pdHl9PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmpvaW5GYlRleHR9PkpvaW4gb3VyIEZhY2Vib29rIENvbW11bml0eTwvc3Bhbj5cbiAgICAgIDxhIGNsYXNzTmFtZT17cy5qb2luRmJMaW5rfSBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL0VnbmlmeS9cIj5cbiAgICAgICAgSm9pbiBOb3dcbiAgICAgIDwvYT5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBzaG93U2Nyb2xsIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5zY3JvbGxUb3B9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICB7dGhpcy5kaXNwbGF5VGVhY2hTZWN0aW9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlDbGllbnRzKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlBY2hpZXZlZFNvRmFyKCl9XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlQbGF5ZXJTZWN0aW9uKCkgKi99XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlUZXN0c1NlY3Rpb24oKSAqL31cbiAgICAgICAge3RoaXMuZGlzcGxheU9ubGluZVRlc3RzKCl9XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlBbmFseXNpcygpICovfVxuICAgICAgICB7dGhpcy5kaXNwbGF5T3duUGFwZXJzKCl9XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlQcmFjdGljZSgpICovfVxuICAgICAgICB7dGhpcy5kaXNwbGF5UXVlc3Rpb25CYW5rKCl9XG4gICAgICAgIHsvKiB0aGlzLmRpc3BsYXlQYXBlcnMoKSAqL31cbiAgICAgICAge3RoaXMuZGlzcGxheVBhcGVycygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5QW5hbHlzaXMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVJlcG9ydHMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUF2YWlsYWJsZU9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlKb2luRmFjZWJvb2soKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoVGVzdCk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1Rlc3Quc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZXN0LnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZXN0LnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiZXhwb3J0IGNvbnN0IG9ubGluZVRlc3RQb2ludHMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnVW5saW1pdHRlZCB0ZXN0IGNyZWF0aW9uJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6XG4gICAgICAnSW5zdGFudCB0ZXN0IHJlc3VsdCBhbmQgUmVwb3J0cyBhdmFpbGFibGUgb25saW5lICYgb2ZmbGluZSAtIFJhbmsgbGlzdCwgQW5hbHlzaXMsIGFuZCBSZXBvcnRzIGFmdGVyIGV2ZXJ5IHRlc3QuJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdQcmUtZGVmaW5lZCBtYXJraW5nIHNjaGVtZXMnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0N1c3RvbSBtYXJraW5nIHNjaGVtZXMnLFxuICB9LFxuICB7XG4gICAgdGV4dDpcbiAgICAgICdHZXRyYW5rcyBzdXBwb3J0cyBhbGwgMjIgdHlwZXMgb2YgSkVFIGF2YW5jZWQgTWFya2luZyBTY2hlbWVzIHN0YXJ0aW5nIGZyb20gMjAwOSAtIDIwMTknLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0dyYWNlIHBlcmlvZCBvcHRpb24gYXZhaWxhYmxlJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdMaXZlIEF0dGVuZGFuY2UnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IG93blBhcGVycyA9IFtcbiAge1xuICAgIHRleHQ6XG4gICAgICAnVXBsb2FkIHlvdXIgcXVlc3Rpb24gcGFwZXIgb25saW5lIGZyb20gd29yZCBkb2N1bWVudCB3aXRoaW4gNSBtaW5zIHdpdGggYSBzaW5nbGUgY2xpY2snLFxuICB9LFxuICB7XG4gICAgdGV4dDpcbiAgICAgICdHZXRyYW5rcyBQYXJzZXIgY2FuIGNvbnZlcnQgdGV4dCAvIGltYWdlcyAvIGVxdWF0aW9ucyAvIGdyYXBocyBpbnRvIHlvdXIgcXVlc3Rpb25zLicsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgcXVlc3Rpb25CYW5rID0gW1xuICB7XG4gICAgdGV4dDpcbiAgICAgICdRdWVzdGlvbiBiYW5rIG1hbmFnZW1lbnQgc3lzdGVtIHdpdGggMS41IExha2ggUXVlc3Rpb25zIG9yZ2FuaXplZCBieSB0b3BpYyBhbmQgZGlmZmljdWx0eSBmb3IgSkVFLCBORUVULCBFQU1DRVQgYW5kIElQRS4nLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0FsbCBUeXBlcyBvZiBRdWVzdGlvbnMgYXMgcGVyIGxhdGVzdCBwYXR0ZXJuJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6XG4gICAgICAnUXVlc3Rpb25zIGFyZSBwcm92aWRlZCB3aXRoIEFuc3dlciBLZXkgYW5kIFN0ZXAgYnkgU3RlcCBEZXRhaWwgU29sdXRpb24uJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBtYXJrc0Jhc2VkQW5hbHlzaXMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnTWFya3MgZGlzdHJpYnV0aW9uIGdyYXBoJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdDdXQtb2ZmIGJhc2VkIGFuYWx5c2lzJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdTdHVkZW50LXdpc2UgbWFya3MgYW5hbHlzaXMnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IGNvbXBhcmlzaW9uQW5hbHlzaXMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnSGlzdG9yaWMgdGVzdCBhdmVyYWdlcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3R1ZGVudCBwZXJmb3JtYW5jZSBwcm9maWxlJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdTdWJqZWN0IHRvcCBtYXJrcyB2cyB0b3BwZXInLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IGVycm9yQW5hbHlzaXMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnUXVlc3Rpb24gZXJyb3IgY291bnQnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1N0dWRlbnRzIGVycm9yIG1hcHBpbmcnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1F1ZXN0aW9uIHBhcGVyIHJlZmVyZW5jaW5nJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBjb25jZXB0QW5hbHlzaXMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnVG9waWMgYmFzZWQgYW5hbHlzaXMnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1N1Yi10b3BpYyBiYXNlZCBhbmFseXNpcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnRGlmZmljdWx0eSBsZXZlbCBhbmFseXNpcycsXG4gIH0sXG5dO1xuZXhwb3J0IGNvbnN0IHBhcGVycyA9IFtcbiAge1xuICAgIHRleHQ6XG4gICAgICAnTW9yZSB0aGFuIDEsNTAsMDAwKyByZWFkeSB0byB1c2UgQ29tcHJlaGVuc2l2ZSBRdWVzdGlvbiBCYW5rIGFuZCBlYWNoIHF1ZXN0aW9uIGhhcyBiZWVuIHNldCB3aXRoIGFwcHJvcHJpYXRlIERpZmZpY3VsdHkgTGV2ZWwuJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdBbGwgVHlwZXMgb2YgUXVlc3Rpb25zIGFzIHBlciBsYXRlc3QgcGF0dGVybjonLFxuICAgIHN1YlBvaW50czogW1xuICAgICAgJ2EpXHRTaW5nbGUgY29ycmVjdCwgTXVsdGlwbGUgY29ycmVjdCBxdWVzdGlvbnMsIEludGVnZXIgdHlwZSwgTWF0cml4IHR5cGUsJyxcbiAgICAgICdiKVx0QXNzZXJ0aW9uIFJlYXNvbmluZyBhbmQgUGFyYWdyYXBoIFR5cGUnLFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0ZXh0OlxuICAgICAgJ0Rlc2lnbiBwZXJzb25hbGl6ZWQgcXVlc3Rpb24gcGFwZXIvcyB3aXRoIEluc3RpdHV0ZSBOYW1lLCBMb2dvICYgV2F0ZXJtYXJrJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6XG4gICAgICAnUXVlc3Rpb25zIGFyZSBwcm92aWRlZCB3aXRoIEFuc3dlciBLZXkgYW5kIFN0ZXAgYnkgU3RlcCBEZXRhaWwgU29sdXRpb24uJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCByZXBvcnRzID0gW1xuICB7XG4gICAgdGV4dDogJ1Rlc3QgcmVzdWx0cycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3R1ZGVudCByZXNwb25zZSByZXBvcnQnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1N0dWRlbnQgcmVzcG9uc2UgY291bnQnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ01hcmtzIGRpc3RyaWJ1dGlvbicsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnRXJyb3IgY291bnQnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1RvcCBtYXJrcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3R1ZGVudCBwZXJmb3JtYW5jZSB0cmVuZCcsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnQXZlcmFnZXMgY29tcGFyaXNvbicsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnQ29uY2VwdCBiYXNlZCByZXBvcnQnLFxuICB9LFxuXTtcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgVGVzdHNNb2R1bGUgZnJvbSAnLi9UZXN0JztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnRWduaWZ5IHRlYWNoaW5nIG1hZGUgZWFzeScsXG4gICAgY2h1bmtzOiBbJ1Rlc3RzJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0IGZvb3RlckFzaD5cbiAgICAgICAgPFRlc3RzTW9kdWxlIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQURBO0FBU0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE5Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0RBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFNQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBS0E7QUFDQTtBQURBO0FBSUE7Ozs7Ozs7QUN6SkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBV0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBb0JBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUE5QkE7QUFnQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQTFDQTtBQTRDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFwREE7QUFzREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBdElBO0FBNElBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE3SkE7QUFrS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF4TkE7QUE4TkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBMVBBO0FBa1FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbFNBO0FBeVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFuVUE7QUEwVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUExV0E7QUFpWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBaGFBO0FBdWFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF4aUJBO0FBK2lCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFubEJBO0FBMGxCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF6b0JBO0FBc3BCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFodEJBO0FBNnRCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbHlCQTtBQTh5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQW4zQkE7QUErM0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXA2QkE7QUE2NkJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFoN0JBO0FBczdCQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQW44QkE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbzdCQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBb0JBO0FBQ0E7QUEzK0JBO0FBQ0E7QUE0K0JBOzs7Ozs7O0FDbmdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7O0FDN0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFFQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBTUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFLQTtBQUVBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBRUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUVBO0FBREE7QUFLQTtBQUNBO0FBRkE7QUFRQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBOzs7Ozs7Ozs7Ozs7OztBQ2pKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBU0E7QUFDQTtBQUNBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=