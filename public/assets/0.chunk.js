(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Footer/Footer.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "footer {\n  background: #fff;\n  padding-top: 40px;\n  padding-top: 2.5rem;\n  padding-bottom: 40px;\n  padding-bottom: 2.5rem;\n}\n\n.Footer-addGray-3ecfx {\n  background: #f7f7f7;\n}\n\n.Footer-logo-1W3uT {\n  height: 56px;\n  height: 3.5rem;\n  width: 113.984px;\n  width: 7.124rem;\n  margin-left: 64px;\n  margin-bottom: 24px;\n}\n\n.Footer-mobile_logo_wrapper-1VMhP {\n  display: none;\n  visibility: hidden;\n  max-width: 124px;\n  max-height: 60px;\n  margin: auto;\n}\n\n.Footer-linksSectionTitle-DmwjI {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 24px;\n  color: #25282b;\n  margin-bottom: 4px;\n  text-transform: capitalize;\n}\n\n.Footer-importantLinkContainer-ArOTF {\n  margin-bottom: 0;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  padding: 0 64px;\n  // flex-wrap: wrap;\n}\n\n.Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n    display: -ms-flexbox;\n    display: flex;\n    padding-right: 24px;\n    -ms-flex-order: 2;\n        order: 2;\n  }\n\n.Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE section {\n      width: 100%;\n    }\n\n.Footer-addressDiv-YTXxe {\n  left: 0;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n    width: 192px;\n  }\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO {\n      width: 100%;\n      font-weight: normal;\n      font-style: normal;\n      font-stretch: normal;\n      letter-spacing: normal;\n      color: #5f6368;\n    }\n\n.Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO p {\n        font-size: 14px;\n        line-height: 24px;\n        margin: 0;\n      }\n\n.Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n    width: 1px;\n    height: 100%;\n    background-color: #25282b;\n    opacity: 0.1;\n    -webkit-transform: rotate(90);\n        -ms-transform: rotate(90);\n            transform: rotate(90);\n    margin: 0 32px;\n  }\n\n.Footer-importantLink-3aQPB {\n  margin: 6px 0;\n}\n\n.Footer-importantLink-3aQPB a {\n    font-size: 14px;\n    line-height: 1.5;\n    text-transform: capitalize;\n  }\n\n.Footer-importantLink-3aQPB a:hover {\n    text-decoration: underline;\n  }\n\n.Footer-social-l2arY {\n  padding-left: 32px;\n  border-left: 1px solid rgba(37, 40, 43, 0.1);\n  margin-left: 0;\n  -ms-flex-order: 3;\n      order: 3;\n}\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-linksSectionTitle1-34zFu {\n      font-size: 16px;\n      margin-bottom: 16px;\n      line-height: 24px;\n      color: #000;\n      font-weight: 600;\n    }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n      display: grid;\n      grid-template-columns: repeat(3, 1fr);\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n    }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv {\n        float: left;\n        margin-right: 24px;\n        margin-bottom: 12px;\n        width: 32px;\n        height: 32px;\n      }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv i {\n          font-size: 32px;\n          font-size: 2rem;\n          color: #5f6368;\n        }\n\n.Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv:last-child {\n        margin-right: 0;\n      }\n\n@media only screen and (max-width: 1290px) {\n  .Footer-logo-1W3uT {\n    margin-left: 32px;\n  }\n\n  .Footer-importantLinkContainer-ArOTF {\n    padding: 0 32px;\n  }\n\n    .Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n      padding-right: 12px;\n    }\n    .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n      width: 172px;\n    }\n\n    .Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n      margin: 0 12px;\n    }\n\n  .Footer-social-l2arY {\n    padding-left: 12px;\n    -ms-flex: 1 1;\n        flex: 1 1;\n  }\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        display: grid;\n        grid-template-columns: repeat(4, 1fr);\n        margin-left: 0;\n        margin-right: 0;\n      }\n\n        .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E .Footer-socialLink-1QvRv {\n          margin-right: 12px;\n          margin-bottom: 8px;\n        }\n}\n\n@media only screen and (max-width: 1110px) {\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        display: grid;\n        grid-template-columns: repeat(2, 1fr);\n      }\n}\n\n@media only screen and (max-width: 990px) {\n  .Footer-logo-1W3uT {\n    display: none;\n  }\n\n  footer {\n    padding: 0;\n  }\n\n  .Footer-mobile_logo_wrapper-1VMhP {\n    margin-top: 48px;\n    width: 7.75rem;\n    height: 3.75rem;\n    display: -ms-flexbox;\n    display: flex;\n    visibility: visible;\n    padding-left: 0;\n    max-width: none;\n    max-height: none;\n  }\n\n  .Footer-mobile_logo-jUB-e {\n    width: 100%;\n    height: 100%;\n  }\n\n  .Footer-linksSectionTitle-DmwjI {\n    margin-bottom: 0.5rem;\n    font-size: 14px;\n    font-weight: 600;\n    text-align: center;\n    margin-top: 42px;\n    width: 100%;\n  }\n\n  .Footer-importantLinkContainer-ArOTF {\n    display: block;\n    padding: 32px 20px;\n  }\n\n    .Footer-importantLinkContainer-ArOTF section {\n      margin: -48px auto 0 auto;\n      // background-color: #fff !important;\n    }\n\n    .Footer-importantLinkContainer-ArOTF .Footer-sectionDiv-1JpSE {\n      margin: auto;\n      display: block;\n      padding-right: 0;\n    }\n\n  .Footer-social-l2arY {\n    padding-left: 0;\n    border-left: none;\n  }\n\n    .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ {\n      width: 200px;\n      margin: auto;\n      margin-top: 1.5rem !important;\n    }\n\n      .Footer-social-l2arY .Footer-socialLinkContainer-1Bjq_ .Footer-socialLinksWrapper-21o9E {\n        grid-template-columns: repeat(5, 1fr);\n      }\n\n  .Footer-socialLink-1QvRv i {\n    font-size: 2.5rem;\n  }\n\n  .Footer-addressDiv-YTXxe {\n    width: 100%;\n    margin: auto;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 16px;\n  }\n\n    .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s {\n      width: 100%;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      -ms-flex-align: center;\n          align-items: center;\n      margin: 0 auto 24px auto;\n      max-width: 280px;\n    }\n\n      .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-linksSectionTitle-DmwjI {\n        margin-top: 0;\n      }\n\n      .Footer-addressDiv-YTXxe .Footer-addressContainer-2ZY-s .Footer-address-rFyBO {\n        width: 100%;\n        text-align: center;\n        font-size: 13px;\n        line-height: 1.71;\n        opacity: 0.6;\n      }\n\n    .Footer-addressDiv-YTXxe .Footer-verticalLine-A1fI5 {\n      display: none;\n    }\n\n  .Footer-importantLink-3aQPB {\n    text-align: center;\n    margin: 1rem 0;\n  }\n\n    .Footer-importantLink-3aQPB a {\n      font-weight: normal;\n    }\n\n  .Footer-linksSectionTitle1-34zFu {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Footer/Footer.scss"],"names":[],"mappings":"AAAA;EACE,iBAAiB;EACjB,kBAAkB;EAClB,oBAAoB;EACpB,qBAAqB;EACrB,uBAAuB;CACxB;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,aAAa;EACb,eAAe;EACf,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,cAAc;EACd,mBAAmB;EACnB,iBAAiB;EACjB,iBAAiB;EACjB,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,eAAe;EACf,mBAAmB;EACnB,2BAA2B;CAC5B;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,oBAAoB;IACpB,kBAAkB;QACd,SAAS;GACd;;AAEH;MACM,YAAY;KACb;;AAEL;EACE,QAAQ;EACR,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,kBAAkB;MACd,SAAS;CACd;;AAED;IACI,aAAa;GACd;;AAEH;MACM,YAAY;MACZ,oBAAoB;MACpB,mBAAmB;MACnB,qBAAqB;MACrB,uBAAuB;MACvB,eAAe;KAChB;;AAEL;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,UAAU;OACX;;AAEP;IACI,WAAW;IACX,aAAa;IACb,0BAA0B;IAC1B,aAAa;IACb,8BAA8B;QAC1B,0BAA0B;YACtB,sBAAsB;IAC9B,eAAe;GAChB;;AAEH;EACE,cAAc;CACf;;AAED;IACI,gBAAgB;IAChB,iBAAiB;IACjB,2BAA2B;GAC5B;;AAEH;IACI,2BAA2B;GAC5B;;AAEH;EACE,mBAAmB;EACnB,6CAA6C;EAC7C,eAAe;EACf,kBAAkB;MACd,SAAS;CACd;;AAED;MACM,gBAAgB;MAChB,oBAAoB;MACpB,kBAAkB;MAClB,YAAY;MACZ,iBAAiB;KAClB;;AAEL;MACM,cAAc;MACd,sCAAsC;MACtC,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;KACpB;;AAEL;QACQ,YAAY;QACZ,mBAAmB;QACnB,oBAAoB;QACpB,YAAY;QACZ,aAAa;OACd;;AAEP;UACU,YAAY;UACZ,aAAa;UACb,uBAAuB;aACpB,oBAAoB;SACxB;;AAET;UACU,gBAAgB;UAChB,gBAAgB;UAChB,eAAe;SAChB;;AAET;QACQ,gBAAgB;OACjB;;AAEP;EACE;IACE,kBAAkB;GACnB;;EAED;IACE,gBAAgB;GACjB;;IAEC;MACE,oBAAoB;KACrB;IACD;MACE,aAAa;KACd;;IAED;MACE,eAAe;KAChB;;EAEH;IACE,mBAAmB;IACnB,cAAc;QACV,UAAU;GACf;MACG;QACE,cAAc;QACd,sCAAsC;QACtC,eAAe;QACf,gBAAgB;OACjB;;QAEC;UACE,mBAAmB;UACnB,mBAAmB;SACpB;CACR;;AAED;MACM;QACE,cAAc;QACd,sCAAsC;OACvC;CACN;;AAED;EACE;IACE,cAAc;GACf;;EAED;IACE,WAAW;GACZ;;EAED;IACE,iBAAiB;IACjB,eAAe;IACf,gBAAgB;IAChB,qBAAqB;IACrB,cAAc;IACd,oBAAoB;IACpB,gBAAgB;IAChB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,sBAAsB;IACtB,gBAAgB;IAChB,iBAAiB;IACjB,mBAAmB;IACnB,iBAAiB;IACjB,YAAY;GACb;;EAED;IACE,eAAe;IACf,mBAAmB;GACpB;;IAEC;MACE,0BAA0B;MAC1B,qCAAqC;KACtC;;IAED;MACE,aAAa;MACb,eAAe;MACf,iBAAiB;KAClB;;EAEH;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;IAEC;MACE,aAAa;MACb,aAAa;MACb,8BAA8B;KAC/B;;MAEC;QACE,sCAAsC;OACvC;;EAEL;IACE,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,aAAa;IACb,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;GAClB;;IAEC;MACE,YAAY;MACZ,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,sBAAsB;UAClB,wBAAwB;MAC5B,uBAAuB;UACnB,oBAAoB;MACxB,yBAAyB;MACzB,iBAAiB;KAClB;;MAEC;QACE,cAAc;OACf;;MAED;QACE,YAAY;QACZ,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;QAClB,aAAa;OACd;;IAEH;MACE,cAAc;KACf;;EAEH;IACE,mBAAmB;IACnB,eAAe;GAChB;;IAEC;MACE,oBAAoB;KACrB;;EAEH;IACE,cAAc;GACf;CACF","file":"Footer.scss","sourcesContent":["footer {\n  background: #fff;\n  padding-top: 40px;\n  padding-top: 2.5rem;\n  padding-bottom: 40px;\n  padding-bottom: 2.5rem;\n}\n\n.addGray {\n  background: #f7f7f7;\n}\n\n.logo {\n  height: 56px;\n  height: 3.5rem;\n  width: 113.984px;\n  width: 7.124rem;\n  margin-left: 64px;\n  margin-bottom: 24px;\n}\n\n.mobile_logo_wrapper {\n  display: none;\n  visibility: hidden;\n  max-width: 124px;\n  max-height: 60px;\n  margin: auto;\n}\n\n.linksSectionTitle {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 24px;\n  color: #25282b;\n  margin-bottom: 4px;\n  text-transform: capitalize;\n}\n\n.importantLinkContainer {\n  margin-bottom: 0;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  padding: 0 64px;\n  // flex-wrap: wrap;\n}\n\n.importantLinkContainer .sectionDiv {\n    display: -ms-flexbox;\n    display: flex;\n    padding-right: 24px;\n    -ms-flex-order: 2;\n        order: 2;\n  }\n\n.importantLinkContainer .sectionDiv section {\n      width: 100%;\n    }\n\n.addressDiv {\n  left: 0;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n.addressDiv .addressContainer {\n    width: 192px;\n  }\n\n.addressDiv .addressContainer .address {\n      width: 100%;\n      font-weight: normal;\n      font-style: normal;\n      font-stretch: normal;\n      letter-spacing: normal;\n      color: #5f6368;\n    }\n\n.addressDiv .addressContainer .address p {\n        font-size: 14px;\n        line-height: 24px;\n        margin: 0;\n      }\n\n.addressDiv .verticalLine {\n    width: 1px;\n    height: 100%;\n    background-color: #25282b;\n    opacity: 0.1;\n    -webkit-transform: rotate(90);\n        -ms-transform: rotate(90);\n            transform: rotate(90);\n    margin: 0 32px;\n  }\n\n.importantLink {\n  margin: 6px 0;\n}\n\n.importantLink a {\n    font-size: 14px;\n    line-height: 1.5;\n    text-transform: capitalize;\n  }\n\n.importantLink a:hover {\n    text-decoration: underline;\n  }\n\n.social {\n  padding-left: 32px;\n  border-left: 1px solid rgba(37, 40, 43, 0.1);\n  margin-left: 0;\n  -ms-flex-order: 3;\n      order: 3;\n}\n\n.social .socialLinkContainer .linksSectionTitle1 {\n      font-size: 16px;\n      margin-bottom: 16px;\n      line-height: 24px;\n      color: #000;\n      font-weight: 600;\n    }\n\n.social .socialLinkContainer .socialLinksWrapper {\n      display: grid;\n      grid-template-columns: repeat(3, 1fr);\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n    }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink {\n        float: left;\n        margin-right: 24px;\n        margin-bottom: 12px;\n        width: 32px;\n        height: 32px;\n      }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink i {\n          font-size: 32px;\n          font-size: 2rem;\n          color: #5f6368;\n        }\n\n.social .socialLinkContainer .socialLinksWrapper .socialLink:last-child {\n        margin-right: 0;\n      }\n\n@media only screen and (max-width: 1290px) {\n  .logo {\n    margin-left: 32px;\n  }\n\n  .importantLinkContainer {\n    padding: 0 32px;\n  }\n\n    .importantLinkContainer .sectionDiv {\n      padding-right: 12px;\n    }\n    .addressDiv .addressContainer {\n      width: 172px;\n    }\n\n    .addressDiv .verticalLine {\n      margin: 0 12px;\n    }\n\n  .social {\n    padding-left: 12px;\n    -ms-flex: 1 1;\n        flex: 1 1;\n  }\n      .social .socialLinkContainer .socialLinksWrapper {\n        display: grid;\n        grid-template-columns: repeat(4, 1fr);\n        margin-left: 0;\n        margin-right: 0;\n      }\n\n        .social .socialLinkContainer .socialLinksWrapper .socialLink {\n          margin-right: 12px;\n          margin-bottom: 8px;\n        }\n}\n\n@media only screen and (max-width: 1110px) {\n      .social .socialLinkContainer .socialLinksWrapper {\n        display: grid;\n        grid-template-columns: repeat(2, 1fr);\n      }\n}\n\n@media only screen and (max-width: 990px) {\n  .logo {\n    display: none;\n  }\n\n  footer {\n    padding: 0;\n  }\n\n  .mobile_logo_wrapper {\n    margin-top: 48px;\n    width: 7.75rem;\n    height: 3.75rem;\n    display: -ms-flexbox;\n    display: flex;\n    visibility: visible;\n    padding-left: 0;\n    max-width: none;\n    max-height: none;\n  }\n\n  .mobile_logo {\n    width: 100%;\n    height: 100%;\n  }\n\n  .linksSectionTitle {\n    margin-bottom: 0.5rem;\n    font-size: 14px;\n    font-weight: 600;\n    text-align: center;\n    margin-top: 42px;\n    width: 100%;\n  }\n\n  .importantLinkContainer {\n    display: block;\n    padding: 32px 20px;\n  }\n\n    .importantLinkContainer section {\n      margin: -48px auto 0 auto;\n      // background-color: #fff !important;\n    }\n\n    .importantLinkContainer .sectionDiv {\n      margin: auto;\n      display: block;\n      padding-right: 0;\n    }\n\n  .social {\n    padding-left: 0;\n    border-left: none;\n  }\n\n    .social .socialLinkContainer {\n      width: 200px;\n      margin: auto;\n      margin-top: 1.5rem !important;\n    }\n\n      .social .socialLinkContainer .socialLinksWrapper {\n        grid-template-columns: repeat(5, 1fr);\n      }\n\n  .socialLink i {\n    font-size: 2.5rem;\n  }\n\n  .addressDiv {\n    width: 100%;\n    margin: auto;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 16px;\n  }\n\n    .addressDiv .addressContainer {\n      width: 100%;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      -ms-flex-align: center;\n          align-items: center;\n      margin: 0 auto 24px auto;\n      max-width: 280px;\n    }\n\n      .addressDiv .addressContainer .linksSectionTitle {\n        margin-top: 0;\n      }\n\n      .addressDiv .addressContainer .address {\n        width: 100%;\n        text-align: center;\n        font-size: 13px;\n        line-height: 1.71;\n        opacity: 0.6;\n      }\n\n    .addressDiv .verticalLine {\n      display: none;\n    }\n\n  .importantLink {\n    text-align: center;\n    margin: 1rem 0;\n  }\n\n    .importantLink a {\n      font-weight: normal;\n    }\n\n  .linksSectionTitle1 {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"addGray": "Footer-addGray-3ecfx",
	"logo": "Footer-logo-1W3uT",
	"mobile_logo_wrapper": "Footer-mobile_logo_wrapper-1VMhP",
	"linksSectionTitle": "Footer-linksSectionTitle-DmwjI",
	"importantLinkContainer": "Footer-importantLinkContainer-ArOTF",
	"sectionDiv": "Footer-sectionDiv-1JpSE",
	"addressDiv": "Footer-addressDiv-YTXxe",
	"addressContainer": "Footer-addressContainer-2ZY-s",
	"address": "Footer-address-rFyBO",
	"verticalLine": "Footer-verticalLine-A1fI5",
	"importantLink": "Footer-importantLink-3aQPB",
	"social": "Footer-social-l2arY",
	"socialLinkContainer": "Footer-socialLinkContainer-1Bjq_",
	"linksSectionTitle1": "Footer-linksSectionTitle1-34zFu",
	"socialLinksWrapper": "Footer-socialLinksWrapper-21o9E",
	"socialLink": "Footer-socialLink-1QvRv",
	"mobile_logo": "Footer-mobile_logo-jUB-e"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/HeaderV2/HeaderV2.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/* .root {\n  height: 100%;\n  width: 100%;\n  background-color: #fff;\n  box-sizing: border-box;\n  overflow-x: hidden;\n} */\n\n/* .bodywrapper {\n  position: relative;\n  padding-top: 70px;\n} */\n\n/*\n.navbar{\n  width: 100%;\n  background-color: #fff;\n  min-height: 75px;\n  padding: 2rem 4rem;\n  justify-content: space-between;\n\n} */\n\n/* .navbar-collapse {\n  min-height: 100vh;\n  flex-grow: 0;\n}\n.dropdown-toggle::after {\n  display:none\n}\n.dropdown-menu {\n  border: none;\n\n}\n.navbar-toggler {\n  border: none;\n}\n.navbar-toggler:focus {\n  box-shadow: none;\n}\n@media only screen and (min-width : 992px) {\n  .navbar-expand-lg {\n    justify-content: space-between;\n    padding:1rem 4rem;\n  }\n  .navbar-collapse {\n    min-height: auto;\n  }\n} */\n\n/* .headerRoot {\n  height: 88px;\n  width: 100%;\n  padding: 27px 5%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.left {\n  height: 51px;\n  width: 105px;\n  display: flex;\n}\n\n.right {\n  height: 100%;\n  margin-right: 14px;\n}\n\n.requestDemoContainer {\n  width: 83px;\n  height: 40px;\n  border-radius: 4px;\n  background-color: #ffebf0;\n  padding: 8px 16px;\n  cursor: pointer;\n  float: right;\n} */\n\n/* .requestDemo {\n//   font-weight: 600;\n//   font-size: 16px;\n//   line-height: 1.5;\n//   color: #f36;\n// } */\n\n.HeaderV2-scheduleDemo-h_CeP {\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 1.5;\n  color: #f36;\n  border: 1px solid #f36;\n  padding: 8px 12px;\n  cursor: pointer;\n  border-radius: 4px;\n  text-transform: uppercase;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-right: 12px;\n  background: none;\n  -ms-flex-order: 0;\n      order: 0;\n}\n\n.HeaderV2-scheduleDemo-h_CeP:hover {\n  background-color: rgb(255, 51, 102);\n  color: #fff;\n  -webkit-transition: 0.5s ease-in-out;\n  -o-transition: 0.5s ease-in-out;\n  transition: 0.5s ease-in-out;\n}\n\n.HeaderV2-navbarWrapper-3MC6c {\n  width: 100%;\n  top: 0%;\n  left: 0%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  background-color: #fff;\n  opacity: 1;\n  z-index: 2;\n  margin: auto;\n  position: fixed;\n  -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n  padding: 16px;\n  padding: 1rem;\n}\n\n.HeaderV2-navbar-1ZdUK {\n  width: 100%;\n  max-width: 1700px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-item-align: center;\n      align-self: center;\n}\n\n/* .featureitem img {\n  width: 20px;\n  height: 20px;\n} */\n\n/* .featureheading img {\n  width: 24px;\n  height: 24px;\n} */\n\n.HeaderV2-menubtn-2MJna {\n  color: #000;\n  font-size: 22.4px;\n  font-size: 1.4rem;\n  margin-left: 0;\n  -ms-flex-order: 2;\n      order: 2;\n}\n\n.HeaderV2-menubtn-2MJna img {\n    width: 20px;\n    height: 20px;\n  }\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI {\n  height: 40px;\n  height: 2.5rem;\n  width: 80px;\n  width: 5rem;\n  cursor: pointer;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI img {\n    width: 100%;\n    height: 100%;\n  }\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH {\n  display: block;\n  position: fixed;\n  left: -100%;\n  top: 69px;\n  width: 100%;\n  height: 100vh;\n  list-style: none;\n  padding: 24px 0;\n  padding: 1.5rem 0;\n  padding-left: 16px;\n  padding-left: 1rem;\n  padding-right: 16px;\n  padding-right: 1rem;\n  -webkit-transition: 0.4s;\n  -o-transition: 0.4s;\n  transition: 0.4s;\n  background-color: #fff;\n  overflow: auto;\n  z-index: 1;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH.HeaderV2-active-vZX84 {\n  left: 0%;\n}\n\n/* .navbar .menu li .featuresdropdown {\n  display: none;\n  flex-direction: column;\n} */\n\n/* .navbar .menu li .featuresdropdown.activefeatures {\n  display: flex;\n} */\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D {\n  display: none;\n  margin-left: 10px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D.HeaderV2-activedropdowns-_cvw6 {\n  display: block;\n  opacity: 1;\n  visibility: visible;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  cursor: pointer;\n  position: relative;\n  -webkit-transition: all 0.4s;\n  -o-transition: all 0.4s;\n  transition: all 0.4s;\n  margin: 0 12px 24px;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.HeaderV2-btngroup-1738m {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* featureheading span {\n  margin-left: 8px;\n  font-size: 16px;\n  font-weight: 600;\n} */\n\n/* .featureitem span {\n  margin-left: 8px;\n  color: #25282b;\n  line-height: 20px;\n  font-size: 14px;\n} */\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n  color: #000;\n  opacity: 0.6;\n  font-size: 14px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span > img {\n  margin-top: 3px;\n  margin-left: 5px;\n  border-bottom: none;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > a {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul {\n  width: 100%;\n  list-style: none;\n  display: none;\n  opacity: 0;\n  z-index: 2;\n}\n\n.HeaderV2-activedropdowns-_cvw6 {\n  display: block;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li,\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a {\n  width: 100%;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a {\n  color: #000;\n  opacity: 0.6;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 0;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li a img {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n}\n\n.HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:nth-child(1) a {\n  padding: 24px 0 12px 0;\n}\n\n.HeaderV2-menubtn-2MJna.HeaderV2-menuactive-1ufv2 {\n  z-index: 3;\n}\n\n.HeaderV2-login-31QGs {\n  padding: 8px 12px;\n  font-weight: 600;\n  text-transform: uppercase;\n  border-radius: 4px;\n  margin-right: 12px;\n  line-height: 20px;\n  background: #ffebf0;\n  color: #f36;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n/* .featurewrapper {\n  display: flex;\n  flex-direction: row;\n  align-items: flex-end;\n  justify-content: space-between;\n  padding: 24px 0;\n  border-bottom: 1px solid rgba(37, 40, 43, 0.1);\n\n  .feature {\n    height: 100%;\n    width: 60%;\n\n    .featureitem {\n      display: flex;\n      align-items: center;\n      margin: 24px 0;\n    }\n\n    .featureitem:last-child {\n      margin-bottom: 0;\n    }\n  }\n} */\n\n/* .featurewrapper:last-child {\n  border-bottom: none;\n} */\n\n/* .viewmorewrapper {\n  height: 27px;\n  position: relative;\n  width: 95px;\n} */\n\n/* .featureheading {\n  display: flex;\n  align-items: center;\n} */\n\n/* .viewmore {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: none;\n  color: #0076ff;\n  padding: 0;\n  text-transform: none;\n  font-size: 14px;\n  line-height: 20px;\n  font-weight: 400;\n  text-align: center;\n} */\n\n/* .search-icon {\n  font-size: 1.4rem;\n   margin-left: 6rem;\n  margin-right: 15px;\n   color: #fff;\n   justify-content: flex-end;\n\n.btngroup {\n  margin-left: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.login {\n  border: none;\n}\n\n@media only screen and (max-width: 1200px) {\n  .navbar {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n*/\n\n@media only screen and (min-width: 990px) {\n  /* .bodywrapper {\n    padding-top: 0;\n  } */\n\n  /* .featureheading img {\n    width: 24px;\n    height: 24px;\n    margin-right: 5px;\n  } */\n\n  .HeaderV2-scheduleDemo-h_CeP {\n    padding: 8px 16px;\n    -ms-flex-order: 0;\n        order: 0;\n    margin: 0 12px;\n  }\n\n  .HeaderV2-login-31QGs {\n    padding: 8px 16px;\n    -ms-flex-order: 1;\n        order: 1;\n    margin: 0 12px;\n  }\n\n  .HeaderV2-navbarWrapper-3MC6c {\n    width: 100%;\n    -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n            box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n    padding: 16px 64px;\n    position: fixed;\n  }\n\n  .HeaderV2-navbar-1ZdUK {\n    width: 100%;\n    max-width: 1700px;\n    margin: auto;\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n    .HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI {\n      width: 6.125rem;\n      height: 3rem;\n    }\n\n      .HeaderV2-navbar-1ZdUK .HeaderV2-logo-34AJI img {\n        width: 6.125rem;\n        height: 3rem;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH {\n    position: relative;\n    top: 0;\n    left: 0;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    background: none;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n    padding: 0;\n    //margin-right: 30px;\n    overflow: initial;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li {\n    border: none;\n    padding: 0;\n    margin-bottom: 0;\n  }\n\n  /* .featureheading span {\n    margin-left: 5px;\n    margin-top: 0;\n  } */\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span {\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li a {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D {\n    position: absolute;\n    top: 0%;\n    right: 0;\n    width: 190px;\n    border-radius: 8px;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    display: block;\n    margin-top: 30px;\n    margin-left: 0;\n    background: #fff;\n    opacity: 0;\n    visibility: hidden;\n    overflow: hidden;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li {\n    border-radius: 4px;\n    margin: 4px;\n    width: auto;\n    padding: 0 12px;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:hover {\n    background-color: rgba(236, 76, 111, 0.1);\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li ul li:nth-child(1) a {\n    padding-top: 12px;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li .HeaderV2-companydropdown-3kG4D.HeaderV2-activedropdowns-_cvw6 {\n    opacity: 1;\n    visibility: visible;\n  }\n\n  /* .featuresdropdown {\n    flex-direction: row-reverse;\n    flex-wrap: wrap;\n  } */\n\n  /* .navbar .menu li .featuresdropdown {\n    position: absolute;\n    top: 40%;\n    left: -150%;\n    width: 888px;\n    height: 356px;\n    list-style: none;\n    display: flex;\n    // justify-content: space-between;\n    opacity: 0;\n    visibility: hidden;\n    background-color: #fff;\n    margin-top: 30px;\n    padding: 42px;\n    //border-radius: 8px;\n    box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  } */\n\n  /* .navbar .menu li .featuresdropdown.activefeatures {\n    opacity: 1;\n    visibility: visible;\n    flex-direction: row;\n  } */\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH li > span {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n  }\n\n  .HeaderV2-featureName-3l-J0:hover {\n    text-decoration: underline;\n  }\n\n  .HeaderV2-navbar-1ZdUK .HeaderV2-menu-3tVbH > li > span > .HeaderV2-featureName-3l-J0:hover {\n    text-decoration: underline;\n    -webkit-text-decoration-color: rgba(0, 0, 0, 0.6);\n            text-decoration-color: rgba(0, 0, 0, 0.6);\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n  }\n\n  /* .featurewrapper {\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 0;\n    max-width: 156px;\n    border-bottom: none;\n    margin-right: 31px;\n\n    .feature {\n      width: 100%;\n      height: 100%;\n      padding-right: 31px;\n      border-right: 1px solid rgb(37, 40, 43, 0.1);\n    }\n  }\n\n  .featurewrapper:nth-child(2) {\n    .feature {\n      padding-right: 48px;\n    }\n  } */\n\n  /* .featurewrapper:last-child {\n    margin-right: 0;\n\n    .feature {\n      padding: 0;\n      border-right: none;\n    }\n  } */\n\n  /* .viewmorewrapper {\n    width: auto;\n    height: fit-content;\n    //margin-top: -15px;\n    //padding-bottom: 10px;\n\n    .viewmore {\n      padding: 0;\n    }\n  } */\n\n  /* .featureitem {\n    display: flex;\n    align-items: center;\n    justify-content: flex-start;\n    margin: 15px 0;\n    font-size: 12px;\n  }\n\n  .featureitem:hover {\n    text-decoration: underline;\n    text-decoration-color: #25282b;\n  } */\n\n  /* .featureheading {\n    font-size: 16px;\n    display: flex;\n    justify-content: flex-start;\n  } */\n\n  /* .navbar .menu li:hover .featuresdropdown {\n  //   opacity: 1;\n  //   visibility: visible;\n  // } */\n  .HeaderV2-menubtn-2MJna {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/HeaderV2/HeaderV2.scss"],"names":[],"mappings":"AAAA;;;;;;IAMI;;AAEJ;;;IAGI;;AAEJ;;;;;;;;IAQI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;;IAyBI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;;;;IA2BI;;AAEJ;;;;;OAKO;;AAEP;EACE,iBAAiB;EACjB,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,uBAAuB;EACvB,kBAAkB;EAClB,gBAAgB;EAChB,mBAAmB;EACnB,0BAA0B;EAC1B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;MACd,SAAS;CACd;;AAED;EACE,oCAAoC;EACpC,YAAY;EACZ,qCAAqC;EACrC,gCAAgC;EAChC,6BAA6B;CAC9B;;AAED;EACE,YAAY;EACZ,QAAQ;EACR,SAAS;EACT,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,uBAAuB;EACvB,WAAW;EACX,WAAW;EACX,aAAa;EACb,gBAAgB;EAChB,qDAAqD;UAC7C,6CAA6C;EACrD,cAAc;EACd,cAAc;CACf;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,uBAAuB;MACnB,oBAAoB;EACxB,4BAA4B;MACxB,mBAAmB;CACxB;;AAED;;;IAGI;;AAEJ;;;IAGI;;AAEJ;EACE,YAAY;EACZ,kBAAkB;EAClB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;MACd,SAAS;CACd;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,aAAa;EACb,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,gBAAgB;EAChB,yCAAyC;CAC1C;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,eAAe;EACf,gBAAgB;EAChB,YAAY;EACZ,UAAU;EACV,YAAY;EACZ,cAAc;EACd,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;EACnB,mBAAmB;EACnB,oBAAoB;EACpB,oBAAoB;EACpB,yBAAyB;EACzB,oBAAoB;EACpB,iBAAiB;EACjB,uBAAuB;EACvB,eAAe;EACf,WAAW;CACZ;;AAED;EACE,SAAS;CACV;;AAED;;;IAGI;;AAEJ;;IAEI;;AAEJ;EACE,cAAc;EACd,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,WAAW;EACX,oBAAoB;CACrB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,+BAA+B;EACnC,gBAAgB;EAChB,mBAAmB;EACnB,6BAA6B;EAC7B,wBAAwB;EACxB,qBAAqB;EACrB,oBAAoB;EACpB,yCAAyC;CAC1C;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;;;;IAII;;AAEJ;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,8BAA8B;EAC9B,oBAAoB;EACpB,YAAY;EACZ,aAAa;EACb,gBAAgB;CACjB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,8BAA8B;EAC9B,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,iBAAiB;EACjB,cAAc;EACd,WAAW;EACX,WAAW;CACZ;;AAED;EACE,eAAe;CAChB;;AAED;;EAEE,YAAY;CACb;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,uBAAuB;MACnB,oBAAoB;EACxB,gBAAgB;CACjB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,kBAAkB;CACnB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,WAAW;CACZ;;AAED;EACE,kBAAkB;EAClB,iBAAiB;EACjB,0BAA0B;EAC1B,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,oBAAoB;EACpB,YAAY;EACZ,kBAAkB;MACd,SAAS;CACd;;AAED;;;;;;;;;;;;;;;;;;;;;;IAsBI;;AAEJ;;IAEI;;AAEJ;;;;IAII;;AAEJ;;;IAGI;;AAEJ;;;;;;;;;;;;IAYI;;AAEJ;;;;;;;;;;;;;;;;;;;;;;;;EAwBE;;AAEF;EACE;;MAEI;;EAEJ;;;;MAII;;EAEJ;IACE,kBAAkB;IAClB,kBAAkB;QACd,SAAS;IACb,eAAe;GAChB;;EAED;IACE,kBAAkB;IAClB,kBAAkB;QACd,SAAS;IACb,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,qDAAqD;YAC7C,6CAA6C;IACrD,mBAAmB;IACnB,gBAAgB;GACjB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,aAAa;IACb,qBAAqB;IACrB,cAAc;GACf;;IAEC;MACE,gBAAgB;MAChB,aAAa;KACd;;MAEC;QACE,gBAAgB;QAChB,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;EAEL;IACE,mBAAmB;IACnB,OAAO;IACP,QAAQ;IACR,4BAA4B;IAC5B,yBAAyB;IACzB,oBAAoB;IACpB,iBAAiB;IACjB,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;QACf,0BAA0B;IAC9B,WAAW;IACX,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,aAAa;IACb,WAAW;IACX,iBAAiB;GAClB;;EAED;;;MAGI;;EAEJ;IACE,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,YAAY;IACZ,aAAa;IACb,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,SAAS;IACT,aAAa;IACb,mBAAmB;IACnB,qDAAqD;YAC7C,6CAA6C;IACrD,eAAe;IACf,iBAAiB;IACjB,eAAe;IACf,iBAAiB;IACjB,WAAW;IACX,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,YAAY;IACZ,gBAAgB;GACjB;;EAED;IACE,0CAA0C;GAC3C;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,WAAW;IACX,oBAAoB;GACrB;;EAED;;;MAGI;;EAEJ;;;;;;;;;;;;;;;;MAgBI;;EAEJ;;;;MAII;;EAEJ;IACE,gBAAgB;IAChB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,2BAA2B;GAC5B;;EAED;IACE,2BAA2B;IAC3B,kDAAkD;YAC1C,0CAA0C;IAClD,+BAA+B;YACvB,uBAAuB;GAChC;;EAED;;;;;;;;;;;;;;;;;;;;MAoBI;;EAEJ;;;;;;;MAOI;;EAEJ;;;;;;;;;MASI;;EAEJ;;;;;;;;;;;MAWI;;EAEJ;;;;MAII;;EAEJ;;;SAGO;EACP;IACE,cAAc;GACf;CACF","file":"HeaderV2.scss","sourcesContent":["/* .root {\n  height: 100%;\n  width: 100%;\n  background-color: #fff;\n  box-sizing: border-box;\n  overflow-x: hidden;\n} */\n\n/* .bodywrapper {\n  position: relative;\n  padding-top: 70px;\n} */\n\n/*\n.navbar{\n  width: 100%;\n  background-color: #fff;\n  min-height: 75px;\n  padding: 2rem 4rem;\n  justify-content: space-between;\n\n} */\n\n/* .navbar-collapse {\n  min-height: 100vh;\n  flex-grow: 0;\n}\n.dropdown-toggle::after {\n  display:none\n}\n.dropdown-menu {\n  border: none;\n\n}\n.navbar-toggler {\n  border: none;\n}\n.navbar-toggler:focus {\n  box-shadow: none;\n}\n@media only screen and (min-width : 992px) {\n  .navbar-expand-lg {\n    justify-content: space-between;\n    padding:1rem 4rem;\n  }\n  .navbar-collapse {\n    min-height: auto;\n  }\n} */\n\n/* .headerRoot {\n  height: 88px;\n  width: 100%;\n  padding: 27px 5%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.left {\n  height: 51px;\n  width: 105px;\n  display: flex;\n}\n\n.right {\n  height: 100%;\n  margin-right: 14px;\n}\n\n.requestDemoContainer {\n  width: 83px;\n  height: 40px;\n  border-radius: 4px;\n  background-color: #ffebf0;\n  padding: 8px 16px;\n  cursor: pointer;\n  float: right;\n} */\n\n/* .requestDemo {\n//   font-weight: 600;\n//   font-size: 16px;\n//   line-height: 1.5;\n//   color: #f36;\n// } */\n\n.scheduleDemo {\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 1.5;\n  color: #f36;\n  border: 1px solid #f36;\n  padding: 8px 12px;\n  cursor: pointer;\n  border-radius: 4px;\n  text-transform: uppercase;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-right: 12px;\n  background: none;\n  -ms-flex-order: 0;\n      order: 0;\n}\n\n.scheduleDemo:hover {\n  background-color: rgb(255, 51, 102);\n  color: #fff;\n  -webkit-transition: 0.5s ease-in-out;\n  -o-transition: 0.5s ease-in-out;\n  transition: 0.5s ease-in-out;\n}\n\n.navbarWrapper {\n  width: 100%;\n  top: 0%;\n  left: 0%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  background-color: #fff;\n  opacity: 1;\n  z-index: 2;\n  margin: auto;\n  position: fixed;\n  -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n  padding: 16px;\n  padding: 1rem;\n}\n\n.navbar {\n  width: 100%;\n  max-width: 1700px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-item-align: center;\n      align-self: center;\n}\n\n/* .featureitem img {\n  width: 20px;\n  height: 20px;\n} */\n\n/* .featureheading img {\n  width: 24px;\n  height: 24px;\n} */\n\n.menubtn {\n  color: #000;\n  font-size: 22.4px;\n  font-size: 1.4rem;\n  margin-left: 0;\n  -ms-flex-order: 2;\n      order: 2;\n}\n\n.menubtn img {\n    width: 20px;\n    height: 20px;\n  }\n\n.navbar .logo {\n  height: 40px;\n  height: 2.5rem;\n  width: 80px;\n  width: 5rem;\n  cursor: pointer;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.navbar .logo img {\n    width: 100%;\n    height: 100%;\n  }\n\n.navbar .menu {\n  display: block;\n  position: fixed;\n  left: -100%;\n  top: 69px;\n  width: 100%;\n  height: 100vh;\n  list-style: none;\n  padding: 24px 0;\n  padding: 1.5rem 0;\n  padding-left: 16px;\n  padding-left: 1rem;\n  padding-right: 16px;\n  padding-right: 1rem;\n  -webkit-transition: 0.4s;\n  -o-transition: 0.4s;\n  transition: 0.4s;\n  background-color: #fff;\n  overflow: auto;\n  z-index: 1;\n}\n\n.navbar .menu.active {\n  left: 0%;\n}\n\n/* .navbar .menu li .featuresdropdown {\n  display: none;\n  flex-direction: column;\n} */\n\n/* .navbar .menu li .featuresdropdown.activefeatures {\n  display: flex;\n} */\n\n.navbar .menu li .companydropdown {\n  display: none;\n  margin-left: 10px;\n}\n\n.navbar .menu li .companydropdown.activedropdowns {\n  display: block;\n  opacity: 1;\n  visibility: visible;\n}\n\n.navbar .menu > li {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  cursor: pointer;\n  position: relative;\n  -webkit-transition: all 0.4s;\n  -o-transition: all 0.4s;\n  transition: all 0.4s;\n  margin: 0 12px 24px;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.btngroup {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* featureheading span {\n  margin-left: 8px;\n  font-size: 16px;\n  font-weight: 600;\n} */\n\n/* .featureitem span {\n  margin-left: 8px;\n  color: #25282b;\n  line-height: 20px;\n  font-size: 14px;\n} */\n\n.navbar .menu > li > span {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n  color: #000;\n  opacity: 0.6;\n  font-size: 14px;\n}\n\n.navbar .menu > li > span > img {\n  margin-top: 3px;\n  margin-left: 5px;\n  border-bottom: none;\n}\n\n.navbar .menu > li > a {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n  border-bottom: 1px solid #ccc;\n  padding: 0 0 24px 0;\n}\n\n.navbar .menu li ul {\n  width: 100%;\n  list-style: none;\n  display: none;\n  opacity: 0;\n  z-index: 2;\n}\n\n.activedropdowns {\n  display: block;\n}\n\n.navbar .menu li ul li,\n.navbar .menu li ul li a {\n  width: 100%;\n}\n\n.navbar .menu li ul li a {\n  color: #000;\n  opacity: 0.6;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 0;\n}\n\n.navbar .menu li ul li a img {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n}\n\n.navbar .menu li ul li:nth-child(1) a {\n  padding: 24px 0 12px 0;\n}\n\n.menubtn.menuactive {\n  z-index: 3;\n}\n\n.login {\n  padding: 8px 12px;\n  font-weight: 600;\n  text-transform: uppercase;\n  border-radius: 4px;\n  margin-right: 12px;\n  line-height: 20px;\n  background: #ffebf0;\n  color: #f36;\n  -ms-flex-order: 1;\n      order: 1;\n}\n\n/* .featurewrapper {\n  display: flex;\n  flex-direction: row;\n  align-items: flex-end;\n  justify-content: space-between;\n  padding: 24px 0;\n  border-bottom: 1px solid rgba(37, 40, 43, 0.1);\n\n  .feature {\n    height: 100%;\n    width: 60%;\n\n    .featureitem {\n      display: flex;\n      align-items: center;\n      margin: 24px 0;\n    }\n\n    .featureitem:last-child {\n      margin-bottom: 0;\n    }\n  }\n} */\n\n/* .featurewrapper:last-child {\n  border-bottom: none;\n} */\n\n/* .viewmorewrapper {\n  height: 27px;\n  position: relative;\n  width: 95px;\n} */\n\n/* .featureheading {\n  display: flex;\n  align-items: center;\n} */\n\n/* .viewmore {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: none;\n  color: #0076ff;\n  padding: 0;\n  text-transform: none;\n  font-size: 14px;\n  line-height: 20px;\n  font-weight: 400;\n  text-align: center;\n} */\n\n/* .search-icon {\n  font-size: 1.4rem;\n   margin-left: 6rem;\n  margin-right: 15px;\n   color: #fff;\n   justify-content: flex-end;\n\n.btngroup {\n  margin-left: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.login {\n  border: none;\n}\n\n@media only screen and (max-width: 1200px) {\n  .navbar {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n*/\n\n@media only screen and (min-width: 990px) {\n  /* .bodywrapper {\n    padding-top: 0;\n  } */\n\n  /* .featureheading img {\n    width: 24px;\n    height: 24px;\n    margin-right: 5px;\n  } */\n\n  .scheduleDemo {\n    padding: 8px 16px;\n    -ms-flex-order: 0;\n        order: 0;\n    margin: 0 12px;\n  }\n\n  .login {\n    padding: 8px 16px;\n    -ms-flex-order: 1;\n        order: 1;\n    margin: 0 12px;\n  }\n\n  .navbarWrapper {\n    width: 100%;\n    -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n            box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.08);\n    padding: 16px 64px;\n    position: fixed;\n  }\n\n  .navbar {\n    width: 100%;\n    max-width: 1700px;\n    margin: auto;\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n    .navbar .logo {\n      width: 6.125rem;\n      height: 3rem;\n    }\n\n      .navbar .logo img {\n        width: 6.125rem;\n        height: 3rem;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .navbar .menu {\n    position: relative;\n    top: 0;\n    left: 0;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    background: none;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n    padding: 0;\n    //margin-right: 30px;\n    overflow: initial;\n  }\n\n  .navbar .menu li {\n    border: none;\n    padding: 0;\n    margin-bottom: 0;\n  }\n\n  /* .featureheading span {\n    margin-left: 5px;\n    margin-top: 0;\n  } */\n\n  .navbar .menu > li > span {\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .navbar .menu li a {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n    border-bottom: none;\n    padding-bottom: 0;\n  }\n\n  .navbar .menu li .companydropdown {\n    position: absolute;\n    top: 0%;\n    right: 0;\n    width: 190px;\n    border-radius: 8px;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    display: block;\n    margin-top: 30px;\n    margin-left: 0;\n    background: #fff;\n    opacity: 0;\n    visibility: hidden;\n    overflow: hidden;\n  }\n\n  .navbar .menu li ul li {\n    border-radius: 4px;\n    margin: 4px;\n    width: auto;\n    padding: 0 12px;\n  }\n\n  .navbar .menu li ul li:hover {\n    background-color: rgba(236, 76, 111, 0.1);\n  }\n\n  .navbar .menu li ul li:nth-child(1) a {\n    padding-top: 12px;\n  }\n\n  .navbar .menu li .companydropdown.activedropdowns {\n    opacity: 1;\n    visibility: visible;\n  }\n\n  /* .featuresdropdown {\n    flex-direction: row-reverse;\n    flex-wrap: wrap;\n  } */\n\n  /* .navbar .menu li .featuresdropdown {\n    position: absolute;\n    top: 40%;\n    left: -150%;\n    width: 888px;\n    height: 356px;\n    list-style: none;\n    display: flex;\n    // justify-content: space-between;\n    opacity: 0;\n    visibility: hidden;\n    background-color: #fff;\n    margin-top: 30px;\n    padding: 42px;\n    //border-radius: 8px;\n    box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  } */\n\n  /* .navbar .menu li .featuresdropdown.activefeatures {\n    opacity: 1;\n    visibility: visible;\n    flex-direction: row;\n  } */\n\n  .navbar .menu li > span {\n    font-size: 14px;\n    color: #000;\n    opacity: 0.6;\n  }\n\n  .featureName:hover {\n    text-decoration: underline;\n  }\n\n  .navbar .menu > li > span > .featureName:hover {\n    text-decoration: underline;\n    -webkit-text-decoration-color: rgba(0, 0, 0, 0.6);\n            text-decoration-color: rgba(0, 0, 0, 0.6);\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n  }\n\n  /* .featurewrapper {\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 0;\n    max-width: 156px;\n    border-bottom: none;\n    margin-right: 31px;\n\n    .feature {\n      width: 100%;\n      height: 100%;\n      padding-right: 31px;\n      border-right: 1px solid rgb(37, 40, 43, 0.1);\n    }\n  }\n\n  .featurewrapper:nth-child(2) {\n    .feature {\n      padding-right: 48px;\n    }\n  } */\n\n  /* .featurewrapper:last-child {\n    margin-right: 0;\n\n    .feature {\n      padding: 0;\n      border-right: none;\n    }\n  } */\n\n  /* .viewmorewrapper {\n    width: auto;\n    height: fit-content;\n    //margin-top: -15px;\n    //padding-bottom: 10px;\n\n    .viewmore {\n      padding: 0;\n    }\n  } */\n\n  /* .featureitem {\n    display: flex;\n    align-items: center;\n    justify-content: flex-start;\n    margin: 15px 0;\n    font-size: 12px;\n  }\n\n  .featureitem:hover {\n    text-decoration: underline;\n    text-decoration-color: #25282b;\n  } */\n\n  /* .featureheading {\n    font-size: 16px;\n    display: flex;\n    justify-content: flex-start;\n  } */\n\n  /* .navbar .menu li:hover .featuresdropdown {\n  //   opacity: 1;\n  //   visibility: visible;\n  // } */\n  .menubtn {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"scheduleDemo": "HeaderV2-scheduleDemo-h_CeP",
	"navbarWrapper": "HeaderV2-navbarWrapper-3MC6c",
	"navbar": "HeaderV2-navbar-1ZdUK",
	"menubtn": "HeaderV2-menubtn-2MJna",
	"logo": "HeaderV2-logo-34AJI",
	"menu": "HeaderV2-menu-3tVbH",
	"active": "HeaderV2-active-vZX84",
	"companydropdown": "HeaderV2-companydropdown-3kG4D",
	"activedropdowns": "HeaderV2-activedropdowns-_cvw6",
	"btngroup": "HeaderV2-btngroup-1738m",
	"menuactive": "HeaderV2-menuactive-1ufv2",
	"login": "HeaderV2-login-31QGs",
	"featureName": "HeaderV2-featureName-3l-J0"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*\n * Base styles\n * ========================================================================== */\n\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n::selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n/*\n * A better looking default horizontal rule\n */\n\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0;\n}\n\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\n\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle;\n}\n\n/*\n * Remove default fieldset styles.\n */\n\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0;\n}\n\n/*\n * Allow only vertical resizing of textareas.\n */\n\ntextarea {\n  resize: vertical;\n}\n\n.Layout-body-daVCC {\n  margin-top: 77px;\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Layout/Layout.css"],"names":[],"mappings":"AAAA;;gFAEgF;;AAEhF;;;;;;GAMG;;AAEH;EACE,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;EACE,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;;GAEG;;AAEH;EACE,eAAe;EACf,YAAY;EACZ,UAAU;EACV,2BAA2B;EAC3B,cAAc;EACd,WAAW;CACZ;;AAED;;;;GAIG;;AAEH;;;;;;EAME,uBAAuB;CACxB;;AAED;;GAEG;;AAEH;EACE,UAAU;EACV,UAAU;EACV,WAAW;CACZ;;AAED;;GAEG;;AAEH;EACE,iBAAiB;CAClB;;AAED;EACE,iBAAiB;CAClB","file":"Layout.css","sourcesContent":["/*\n * Base styles\n * ========================================================================== */\n\n/*\n * Remove text-shadow in selection highlight:\n * https://twitter.com/miketaylr/status/12228805301\n *\n * These selection rule sets have to be separate.\n * Customize the background color to match your design.\n */\n\n::-moz-selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n::selection {\n  background: #b3d4fc;\n  text-shadow: none;\n}\n\n/*\n * A better looking default horizontal rule\n */\n\nhr {\n  display: block;\n  height: 1px;\n  border: 0;\n  border-top: 1px solid #ccc;\n  margin: 1em 0;\n  padding: 0;\n}\n\n/*\n * Remove the gap between audio, canvas, iframes,\n * images, videos and the bottom of their containers:\n * https://github.com/h5bp/html5-boilerplate/issues/440\n */\n\naudio,\ncanvas,\niframe,\nimg,\nsvg,\nvideo {\n  vertical-align: middle;\n}\n\n/*\n * Remove default fieldset styles.\n */\n\nfieldset {\n  border: 0;\n  margin: 0;\n  padding: 0;\n}\n\n/*\n * Allow only vertical resizing of textareas.\n */\n\ntextarea {\n  resize: vertical;\n}\n\n.body {\n  margin-top: 77px;\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"body": "Layout-body-daVCC"
};

/***/ }),

/***/ "./src/components/Footer/Footer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Footer_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Footer/Footer.scss");
/* harmony import */ var _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Footer_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Footer/Footer.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var FOOTER_LINKS = [
/* {
  title: 'Company',
  links: [
    { label: 'About Us', url: '/about-us' },
    { label: 'Team', url: '/team' },
    { label: 'Culture', url: '/culture' },
    { label: 'Careers', url: '/careers' },
  ],
},
{
  title: 'Product',
  links: [
    { label: 'GetRanks', url: '/products/jeet' },
    { label: 'ACADS', url: '/products/acads' },
    { label: 'Prep', url: '/products/prep' },
    { label: 'QMS', url: '/products/qms' },
  ],
}, */
{
  title: 'Links',
  links: [{
    label: 'About Us',
    url: '/company/aboutus'
  }, {
    label: 'Pricing Plan',
    url: '/pricing'
  }, {
    label: 'Contact us',
    url: '/request-demo'
  }, {
    label: 'Privacy Policy',
    url: '/privacy-and-terms'
  }, {
    label: 'Cancellation & Return Policy',
    url: '/cancellation-and-return'
  }, {
    label: 'Terms & Conditions',
    url: '/terms-and-conditions'
  }]
}];
var SOCIAL_LINKS = [{
  key: 'facebook',
  url: 'https://www.facebook.com/Egnify/',
  icon: '/images/footer/facebook.svg'
}, {
  key: 'youtube',
  url: 'https://www.youtube.com/user/yerranagu',
  icon: '/images/footer/youtube.svg'
}, {
  key: 'linkedin',
  url: 'https://www.linkedin.com/company/egnify',
  icon: '/images/footer/linkedin.svg'
}, {
  key: 'instagram',
  url: 'https://www.instagram.com/egnify',
  icon: '/images/footer/instagram.svg'
}, {
  key: 'twitter',
  url: 'https://twitter.com/egnify',
  icon: '/images/footer/twitter.svg'
}];

var Footer =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Footer, _React$Component);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, _getPrototypeOf(Footer).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: "render",
    value: function render() {
      var isAsh = this.props.isAsh;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
        className: isAsh ? _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addGray : null,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/icons/getranks-marketing-new.svg",
        alt: "getranks by egnify",
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.logo,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row ".concat(_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.importantLinkContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.sectionDiv,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, FOOTER_LINKS.map(function (section) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 87
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 88
          },
          __self: this
        }, section.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 89
          },
          __self: this
        }, section.links.map(function (sectionLink) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.importantLink,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 91
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
            href: sectionLink.url,
            target: sectionLink.label === 'Blog' ? '_blank' : '',
            rel: "noreferrer noopener",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 92
            },
            __self: this
          }, sectionLink.label));
        })));
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.mobile_logo_wrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/icons/getranks-marketing-new.svg",
        alt: "getranks by egnify",
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.mobile_logo,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressDiv,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, "Hyderabad Office-1"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, "Krishe Emerald, Kondapur Main Road, Laxmi Cyber City, Whitefields, Kondapur, Hyderabad, Telangana 500081"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, "Hyderabad Office-2"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, "1-2/1/24/A/1,7, Plot No 17 Opp. Bharat Petrol Bunk JNTU Rd, HI-TECH City Hyderabad, Telangana 500084"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.addressContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, "Bengaluru Office"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.address,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }, "Prestige Atlanta, 80 Feet Main Road, Koramangala 1A Block, Bengaluru,Karnataka 560034"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.verticalLine,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.social,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row ".concat(_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLinkContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.linksSectionTitle1,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148
        },
        __self: this
      }, "Social"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row ".concat(_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLinksWrapper),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }, SOCIAL_LINKS.map(function (socialLink) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          className: _Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a.socialLink,
          href: socialLink.url,
          target: "_blank",
          rel: "noreferrer noopener",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 151
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: socialLink.icon,
          alt: socialLink.key,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 157
          },
          __self: this
        }));
      }))))))));
    }
  }]);

  return Footer;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(Footer, "propTypes", {
  isAsh: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool.isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Footer_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Footer));

/***/ }),

/***/ "./src/components/Footer/Footer.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Footer/Footer.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/HeaderV2/HeaderV2.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/HeaderV2/HeaderV2.scss");
/* harmony import */ var _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/HeaderV2/HeaderV2.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var HeaderV2 =
/*#__PURE__*/
function (_Component) {
  _inherits(HeaderV2, _Component);

  function HeaderV2(props) {
    var _this;

    _classCallCheck(this, HeaderV2);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HeaderV2).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handlesize", function () {
      if (window.innerWidth < 990) {
        _this.setState({
          mobile: true
        });
      } else {
        _this.setState({
          mobile: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "toggleNavbar", function () {
      var close = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (close) {
        _this.setState({
          navOpen: false
        });
      } else {
        _this.setState({
          navOpen: !_this.state.navOpen
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleshowcompany", function () {
      _this.setState({
        showcompany: !_this.state.showcompany,
        showProducts: false // showresources: false,

      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleshowProduct", function () {
      _this.setState({
        showcompany: false,
        showProducts: !_this.state.showProducts
      });
    });

    _this.state = {
      navOpen: false,
      mobile: false,
      showcompany: false,
      showProducts: false
    };
    return _this;
  }

  _createClass(HeaderV2, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.handlesize();
      window.addEventListener('resize', this.handlesize);
      document.addEventListener('click', function (event) {
        if (
        /*! document.getElementById('featureslabel').contains(event.target) && */
        document.getElementById('companylabel') && !document.getElementById('companylabel').contains(event.target) // document.getElementById('productslabel') &&
        // !document.getElementById('productslabel').contains(event.target)

        /* !document.getElementById('resourceslabel').contains(event.target) */
        ) {
            _this2.setState({
              // showfeatures: false,
              showcompany: false // showProducts: false,
              // showresources: false,

            });
          }
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this3 = this;

      window.removeEventListener('resize', this.handlesize);
      document.removeEventListener('click', function (event) {
        if ( // !document.getElementById('productslabel').contains(event.target)
        !document.getElementById('companylabel').contains(event.target) // !document.getElementById('resourceslabel').contains(event.target)
        ) {
            _this3.setState({
              // showfeatures: false,
              showcompany: false // showProducts: false,
              // showresources: false,

            });
          }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navbarWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.logo,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/icons/getranks-marketing-new.svg",
        alt: "getranks",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: this.state.navOpen ? "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menu, " ").concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.active) : "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menu),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        id: "productslabel",
        onClick: this.handleshowProduct,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 180
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 185
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 186
        },
        __self: this
      }, "Products"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: this.state.showProducts ? '/images/icons/chevron-up.svg' : '/images/icons/chevron-down.svg',
        alt: "down-angle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: this.state.showProducts ? "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown, " ").concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.activedropdowns) : "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 196
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/modules/tests",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Test/module_test.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, "Online Tests"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/modules/liveclasses",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Teach/old_Live.svg",
        alt: "live-classes",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }, "Live Classes"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 221
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/modules/doubts",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 222
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Teach/old_Doubts.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 227
        },
        __self: this
      }, "Doubts"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/modules/assignments",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Teach/old_Assignments.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 232
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, "Assignments"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/modules/connect",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Connect/new_connect.svg",
        alt: "connect",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 241
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 245
        },
        __self: this
      }, "Connect"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/customers",
        onClick: function onClick() {
          return _this4.toggleNavbar(false);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 251
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }, "Customers")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/pricing",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 258
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 259
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 260
        },
        __self: this
      }, "Pricing")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        id: "companylabel",
        onClick: this.handleshowcompany,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 264
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      }, "Company"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: this.state.showcompany ? '/images/icons/chevron-up.svg' : '/images/icons/chevron-down.svg',
        alt: "down-angle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: this.state.showcompany ? "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown, " ").concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.activedropdowns) : "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.companydropdown),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 280
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 288
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/company/aboutus",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289
        },
        __self: this
      }, "About Us")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 293
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/company/press",
        onClick: this.toggleNavbar,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 294
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }, "Press"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 298
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/company/culture",
        onClick: function onClick() {
          return _this4.toggleNavbar(false);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 299
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 303
        },
        __self: this
      }, "Culture"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "/company/careers",
        onClick: function onClick() {
          return _this4.toggleNavbar(false);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 307
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311
        },
        __self: this
      }, "Careers"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        role: "presentation",
        onClick: function onClick() {
          window.open('https://blog.egnify.com', 'blank');
        },
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.featureName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348
        },
        __self: this
      }, "Blog"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.btngroup,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 360
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/request-demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 361
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.scheduleDemo,
        role: "presentation",
        onClick: function onClick() {
          return _this4.toggleNavbar(true);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 362
        },
        __self: this
      }, this.state.mobile ? 'Demo' : 'SCHEDULE DEMO')), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://account.getranks.in/signin?host=https%3A%2F%2Fgetranks.in%2F",
        className: _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.login,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370
        },
        __self: this
      }, "Login"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        onClick: function onClick() {
          return _this4.toggleNavbar(false);
        },
        role: "presentation",
        className: this.state.navOpen ? "".concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menubtn, " ").concat(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menuactive) : _HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a.menubtn,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 376
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: !this.state.navOpen ? '/images/icons/drawer.svg' : '/images/icons/close.svg',
        alt: "toggle button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 383
        },
        __self: this
      })))));
    }
  }]);

  return HeaderV2;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_HeaderV2_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(HeaderV2));

/***/ }),

/***/ "./src/components/HeaderV2/HeaderV2.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/HeaderV2/HeaderV2.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/Layout/Layout.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Layout/Layout.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/components/Layout/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Layout_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Layout/Layout.css");
/* harmony import */ var _Layout_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Layout_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _HeaderV2_HeaderV2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/HeaderV2/HeaderV2.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/Footer/Footer.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Layout/Layout.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



 // external-global styles must be imported in your JS.





var Layout =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Layout, _React$Component);

  function Layout() {
    _classCallCheck(this, Layout);

    return _possibleConstructorReturn(this, _getPrototypeOf(Layout).apply(this, arguments));
  }

  _createClass(Layout, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HeaderV2_HeaderV2__WEBPACK_IMPORTED_MODULE_4__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row ".concat(_Layout_css__WEBPACK_IMPORTED_MODULE_3___default.a.body),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      }, this.props.children), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
        isAsh: this.props.footerAsh,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 21
        },
        __self: this
      }));
    }
  }]);

  return Layout;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(Layout, "propTypes", {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  footerAsh: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
});

Layout.defaultProps = {
  footerAsh: false
};
/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Layout_css__WEBPACK_IMPORTED_MODULE_3___default.a)(Layout));

/***/ }),

/***/ "./src/components/Link/Link.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _history__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/history.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Link/Link.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

var Link =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Link)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleClick", function (event) {
      if (_this.props.onClick) {
        _this.props.onClick(event);
      }

      if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
        return;
      }

      if (event.defaultPrevented === true) {
        return;
      }

      event.preventDefault();
      _history__WEBPACK_IMPORTED_MODULE_2__["default"].push(_this.props.to);
    });

    return _this;
  }

  _createClass(Link, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          to = _this$props.to,
          children = _this$props.children,
          props = _objectWithoutProperties(_this$props, ["to", "children"]);

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", _extends({
        href: to
      }, props, {
        onClick: this.handleClick,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }), children);
    }
  }]);

  return Link;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(Link, "propTypes", {
  to: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
});

_defineProperty(Link, "defaultProps", {
  onClick: null
});

/* harmony default export */ __webpack_exports__["default"] = (Link);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9IZWFkZXJWMi9IZWFkZXJWMi5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvRm9vdGVyL0Zvb3Rlci5zY3NzPzJjNmEiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvSGVhZGVyVjIvSGVhZGVyVjIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvSGVhZGVyVjIvSGVhZGVyVjIuc2Nzcz9lNDRiIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzPzE3NjYiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvTGF5b3V0L0xheW91dC5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9MaW5rL0xpbmsuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcImZvb3RlciB7XFxuICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgcGFkZGluZy10b3A6IDQwcHg7XFxuICBwYWRkaW5nLXRvcDogMi41cmVtO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxuICBwYWRkaW5nLWJvdHRvbTogMi41cmVtO1xcbn1cXG5cXG4uRm9vdGVyLWFkZEdyYXktM2VjZngge1xcbiAgYmFja2dyb3VuZDogI2Y3ZjdmNztcXG59XFxuXFxuLkZvb3Rlci1sb2dvLTFXM3VUIHtcXG4gIGhlaWdodDogNTZweDtcXG4gIGhlaWdodDogMy41cmVtO1xcbiAgd2lkdGg6IDExMy45ODRweDtcXG4gIHdpZHRoOiA3LjEyNHJlbTtcXG4gIG1hcmdpbi1sZWZ0OiA2NHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLkZvb3Rlci1tb2JpbGVfbG9nb193cmFwcGVyLTFWTWhQIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICB2aXNpYmlsaXR5OiBoaWRkZW47XFxuICBtYXgtd2lkdGg6IDEyNHB4O1xcbiAgbWF4LWhlaWdodDogNjBweDtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZS1EbXdqSSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1ib3R0b206IDRweDtcXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xcbn1cXG5cXG4uRm9vdGVyLWltcG9ydGFudExpbmtDb250YWluZXItQXJPVEYge1xcbiAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogMCA2NHB4O1xcbiAgLy8gZmxleC13cmFwOiB3cmFwO1xcbn1cXG5cXG4uRm9vdGVyLWltcG9ydGFudExpbmtDb250YWluZXItQXJPVEYgLkZvb3Rlci1zZWN0aW9uRGl2LTFKcFNFIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDI0cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAyO1xcbiAgICAgICAgb3JkZXI6IDI7XFxuICB9XFxuXFxuLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIC5Gb290ZXItc2VjdGlvbkRpdi0xSnBTRSBzZWN0aW9uIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbi5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSB7XFxuICBsZWZ0OiAwO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICBvcmRlcjogMTtcXG59XFxuXFxuLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItYWRkcmVzc0NvbnRhaW5lci0yWlktcyB7XFxuICAgIHdpZHRoOiAxOTJweDtcXG4gIH1cXG5cXG4uRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zIC5Gb290ZXItYWRkcmVzcy1yRnlCTyB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgICBmb250LXN0eWxlOiBub3JtYWw7XFxuICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XFxuICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcXG4gICAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgfVxcblxcbi5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLWFkZHJlc3NDb250YWluZXItMlpZLXMgLkZvb3Rlci1hZGRyZXNzLXJGeUJPIHAge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgfVxcblxcbi5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLXZlcnRpY2FsTGluZS1BMWZJNSB7XFxuICAgIHdpZHRoOiAxcHg7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI1MjgyYjtcXG4gICAgb3BhY2l0eTogMC4xO1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDkwKTtcXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg5MCk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTApO1xcbiAgICBtYXJnaW46IDAgMzJweDtcXG4gIH1cXG5cXG4uRm9vdGVyLWltcG9ydGFudExpbmstM2FRUEIge1xcbiAgbWFyZ2luOiA2cHggMDtcXG59XFxuXFxuLkZvb3Rlci1pbXBvcnRhbnRMaW5rLTNhUVBCIGEge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xcbiAgfVxcblxcbi5Gb290ZXItaW1wb3J0YW50TGluay0zYVFQQiBhOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICB9XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkge1xcbiAgcGFkZGluZy1sZWZ0OiAzMnB4O1xcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMSk7XFxuICBtYXJnaW4tbGVmdDogMDtcXG4gIC1tcy1mbGV4LW9yZGVyOiAzO1xcbiAgICAgIG9yZGVyOiAzO1xcbn1cXG5cXG4uRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1saW5rc1NlY3Rpb25UaXRsZTEtMzR6RnUge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIGNvbG9yOiAjMDAwO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG4uRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUUge1xcbiAgICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIH1cXG5cXG4uRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUUgLkZvb3Rlci1zb2NpYWxMaW5rLTFRdlJ2IHtcXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgIH1cXG5cXG4uRm9vdGVyLXNvY2lhbC1sMmFyWSAuRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV8gLkZvb3Rlci1zb2NpYWxMaW5rc1dyYXBwZXItMjFvOUUgLkZvb3Rlci1zb2NpYWxMaW5rLTFRdlJ2IGltZyB7XFxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICB9XFxuXFxuLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIC5Gb290ZXItc29jaWFsTGluay0xUXZSdiBpIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgICAgICBmb250LXNpemU6IDJyZW07XFxuICAgICAgICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICAgICAgfVxcblxcbi5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSAuRm9vdGVyLXNvY2lhbExpbmstMVF2UnY6bGFzdC1jaGlsZCB7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTI5MHB4KSB7XFxuICAuRm9vdGVyLWxvZ28tMVczdVQge1xcbiAgICBtYXJnaW4tbGVmdDogMzJweDtcXG4gIH1cXG5cXG4gIC5Gb290ZXItaW1wb3J0YW50TGlua0NvbnRhaW5lci1Bck9URiB7XFxuICAgIHBhZGRpbmc6IDAgMzJweDtcXG4gIH1cXG5cXG4gICAgLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIC5Gb290ZXItc2VjdGlvbkRpdi0xSnBTRSB7XFxuICAgICAgcGFkZGluZy1yaWdodDogMTJweDtcXG4gICAgfVxcbiAgICAuRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zIHtcXG4gICAgICB3aWR0aDogMTcycHg7XFxuICAgIH1cXG5cXG4gICAgLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItdmVydGljYWxMaW5lLUExZkk1IHtcXG4gICAgICBtYXJnaW46IDAgMTJweDtcXG4gICAgfVxcblxcbiAgLkZvb3Rlci1zb2NpYWwtbDJhclkge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XFxuICAgIC1tcy1mbGV4OiAxIDE7XFxuICAgICAgICBmbGV4OiAxIDE7XFxuICB9XFxuICAgICAgLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIHtcXG4gICAgICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgfVxcblxcbiAgICAgICAgLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIC5Gb290ZXItc29jaWFsTGluay0xUXZSdiB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTJweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgICAgICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDExMTBweCkge1xcbiAgICAgIC5Gb290ZXItc29jaWFsLWwyYXJZIC5Gb290ZXItc29jaWFsTGlua0NvbnRhaW5lci0xQmpxXyAuRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RSB7XFxuICAgICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5Gb290ZXItbG9nby0xVzN1VCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICBmb290ZXIge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLkZvb3Rlci1tb2JpbGVfbG9nb193cmFwcGVyLTFWTWhQIHtcXG4gICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgd2lkdGg6IDcuNzVyZW07XFxuICAgIGhlaWdodDogMy43NXJlbTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICAgIHBhZGRpbmctbGVmdDogMDtcXG4gICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICBtYXgtaGVpZ2h0OiBub25lO1xcbiAgfVxcblxcbiAgLkZvb3Rlci1tb2JpbGVfbG9nby1qVUItZSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuICAuRm9vdGVyLWxpbmtzU2VjdGlvblRpdGxlLURtd2pJIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNDJweDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuRm9vdGVyLWltcG9ydGFudExpbmtDb250YWluZXItQXJPVEYge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgcGFkZGluZzogMzJweCAyMHB4O1xcbiAgfVxcblxcbiAgICAuRm9vdGVyLWltcG9ydGFudExpbmtDb250YWluZXItQXJPVEYgc2VjdGlvbiB7XFxuICAgICAgbWFyZ2luOiAtNDhweCBhdXRvIDAgYXV0bztcXG4gICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XFxuICAgIH1cXG5cXG4gICAgLkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGIC5Gb290ZXItc2VjdGlvbkRpdi0xSnBTRSB7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICAgIH1cXG5cXG4gIC5Gb290ZXItc29jaWFsLWwyYXJZIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBib3JkZXItbGVmdDogbm9uZTtcXG4gIH1cXG5cXG4gICAgLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIHtcXG4gICAgICB3aWR0aDogMjAwcHg7XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIG1hcmdpbi10b3A6IDEuNXJlbSAhaW1wb3J0YW50O1xcbiAgICB9XFxuXFxuICAgICAgLkZvb3Rlci1zb2NpYWwtbDJhclkgLkZvb3Rlci1zb2NpYWxMaW5rQ29udGFpbmVyLTFCanFfIC5Gb290ZXItc29jaWFsTGlua3NXcmFwcGVyLTIxbzlFIHtcXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDUsIDFmcik7XFxuICAgICAgfVxcblxcbiAgLkZvb3Rlci1zb2NpYWxMaW5rLTFRdlJ2IGkge1xcbiAgICBmb250LXNpemU6IDIuNXJlbTtcXG4gIH1cXG5cXG4gIC5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDE2cHg7XFxuICB9XFxuXFxuICAgIC5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLWFkZHJlc3NDb250YWluZXItMlpZLXMge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIG1hcmdpbjogMCBhdXRvIDI0cHggYXV0bztcXG4gICAgICBtYXgtd2lkdGg6IDI4MHB4O1xcbiAgICB9XFxuXFxuICAgICAgLkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlIC5Gb290ZXItYWRkcmVzc0NvbnRhaW5lci0yWlktcyAuRm9vdGVyLWxpbmtzU2VjdGlvblRpdGxlLURtd2pJIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XFxuICAgICAgfVxcblxcbiAgICAgIC5Gb290ZXItYWRkcmVzc0Rpdi1ZVFh4ZSAuRm9vdGVyLWFkZHJlc3NDb250YWluZXItMlpZLXMgLkZvb3Rlci1hZGRyZXNzLXJGeUJPIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNzE7XFxuICAgICAgICBvcGFjaXR5OiAwLjY7XFxuICAgICAgfVxcblxcbiAgICAuRm9vdGVyLWFkZHJlc3NEaXYtWVRYeGUgLkZvb3Rlci12ZXJ0aWNhbExpbmUtQTFmSTUge1xcbiAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIH1cXG5cXG4gIC5Gb290ZXItaW1wb3J0YW50TGluay0zYVFQQiB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAxcmVtIDA7XFxuICB9XFxuXFxuICAgIC5Gb290ZXItaW1wb3J0YW50TGluay0zYVFQQiBhIHtcXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgICB9XFxuXFxuICAuRm9vdGVyLWxpbmtzU2VjdGlvblRpdGxlMS0zNHpGdSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvRm9vdGVyL0Zvb3Rlci5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIscUJBQXFCO0VBQ3JCLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixtQkFBbUI7Q0FDcEI7O0FBRUQ7SUFDSSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLG9CQUFvQjtJQUNwQixrQkFBa0I7UUFDZCxTQUFTO0dBQ2Q7O0FBRUg7TUFDTSxZQUFZO0tBQ2I7O0FBRUw7RUFDRSxRQUFRO0VBQ1IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxvQkFBb0I7TUFDaEIsZ0JBQWdCO0VBQ3BCLGtCQUFrQjtNQUNkLFNBQVM7Q0FDZDs7QUFFRDtJQUNJLGFBQWE7R0FDZDs7QUFFSDtNQUNNLFlBQVk7TUFDWixvQkFBb0I7TUFDcEIsbUJBQW1CO01BQ25CLHFCQUFxQjtNQUNyQix1QkFBdUI7TUFDdkIsZUFBZTtLQUNoQjs7QUFFTDtRQUNRLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsVUFBVTtPQUNYOztBQUVQO0lBQ0ksV0FBVztJQUNYLGFBQWE7SUFDYiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLDhCQUE4QjtRQUMxQiwwQkFBMEI7WUFDdEIsc0JBQXNCO0lBQzlCLGVBQWU7R0FDaEI7O0FBRUg7RUFDRSxjQUFjO0NBQ2Y7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLDJCQUEyQjtHQUM1Qjs7QUFFSDtJQUNJLDJCQUEyQjtHQUM1Qjs7QUFFSDtFQUNFLG1CQUFtQjtFQUNuQiw2Q0FBNkM7RUFDN0MsZUFBZTtFQUNmLGtCQUFrQjtNQUNkLFNBQVM7Q0FDZDs7QUFFRDtNQUNNLGdCQUFnQjtNQUNoQixvQkFBb0I7TUFDcEIsa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixpQkFBaUI7S0FDbEI7O0FBRUw7TUFDTSxjQUFjO01BQ2Qsc0NBQXNDO01BQ3RDLDJCQUEyQjtNQUMzQix3QkFBd0I7TUFDeEIsbUJBQW1CO0tBQ3BCOztBQUVMO1FBQ1EsWUFBWTtRQUNaLG1CQUFtQjtRQUNuQixvQkFBb0I7UUFDcEIsWUFBWTtRQUNaLGFBQWE7T0FDZDs7QUFFUDtVQUNVLFlBQVk7VUFDWixhQUFhO1VBQ2IsdUJBQXVCO2FBQ3BCLG9CQUFvQjtTQUN4Qjs7QUFFVDtVQUNVLGdCQUFnQjtVQUNoQixnQkFBZ0I7VUFDaEIsZUFBZTtTQUNoQjs7QUFFVDtRQUNRLGdCQUFnQjtPQUNqQjs7QUFFUDtFQUNFO0lBQ0Usa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztJQUVDO01BQ0Usb0JBQW9CO0tBQ3JCO0lBQ0Q7TUFDRSxhQUFhO0tBQ2Q7O0lBRUQ7TUFDRSxlQUFlO0tBQ2hCOztFQUVIO0lBQ0UsbUJBQW1CO0lBQ25CLGNBQWM7UUFDVixVQUFVO0dBQ2Y7TUFDRztRQUNFLGNBQWM7UUFDZCxzQ0FBc0M7UUFDdEMsZUFBZTtRQUNmLGdCQUFnQjtPQUNqQjs7UUFFQztVQUNFLG1CQUFtQjtVQUNuQixtQkFBbUI7U0FDcEI7Q0FDUjs7QUFFRDtNQUNNO1FBQ0UsY0FBYztRQUNkLHNDQUFzQztPQUN2QztDQUNOOztBQUVEO0VBQ0U7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLFlBQVk7R0FDYjs7RUFFRDtJQUNFLGVBQWU7SUFDZixtQkFBbUI7R0FDcEI7O0lBRUM7TUFDRSwwQkFBMEI7TUFDMUIscUNBQXFDO0tBQ3RDOztJQUVEO01BQ0UsYUFBYTtNQUNiLGVBQWU7TUFDZixpQkFBaUI7S0FDbEI7O0VBRUg7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztJQUVDO01BQ0UsYUFBYTtNQUNiLGFBQWE7TUFDYiw4QkFBOEI7S0FDL0I7O01BRUM7UUFDRSxzQ0FBc0M7T0FDdkM7O0VBRUw7SUFDRSxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixpQkFBaUI7R0FDbEI7O0lBRUM7TUFDRSxZQUFZO01BQ1oscUJBQXFCO01BQ3JCLGNBQWM7TUFDZCwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLHNCQUFzQjtVQUNsQix3QkFBd0I7TUFDNUIsdUJBQXVCO1VBQ25CLG9CQUFvQjtNQUN4Qix5QkFBeUI7TUFDekIsaUJBQWlCO0tBQ2xCOztNQUVDO1FBQ0UsY0FBYztPQUNmOztNQUVEO1FBQ0UsWUFBWTtRQUNaLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGFBQWE7T0FDZDs7SUFFSDtNQUNFLGNBQWM7S0FDZjs7RUFFSDtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0dBQ2hCOztJQUVDO01BQ0Usb0JBQW9CO0tBQ3JCOztFQUVIO0lBQ0UsY0FBYztHQUNmO0NBQ0ZcIixcImZpbGVcIjpcIkZvb3Rlci5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcImZvb3RlciB7XFxuICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgcGFkZGluZy10b3A6IDQwcHg7XFxuICBwYWRkaW5nLXRvcDogMi41cmVtO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxuICBwYWRkaW5nLWJvdHRvbTogMi41cmVtO1xcbn1cXG5cXG4uYWRkR3JheSB7XFxuICBiYWNrZ3JvdW5kOiAjZjdmN2Y3O1xcbn1cXG5cXG4ubG9nbyB7XFxuICBoZWlnaHQ6IDU2cHg7XFxuICBoZWlnaHQ6IDMuNXJlbTtcXG4gIHdpZHRoOiAxMTMuOTg0cHg7XFxuICB3aWR0aDogNy4xMjRyZW07XFxuICBtYXJnaW4tbGVmdDogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxufVxcblxcbi5tb2JpbGVfbG9nb193cmFwcGVyIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICB2aXNpYmlsaXR5OiBoaWRkZW47XFxuICBtYXgtd2lkdGg6IDEyNHB4O1xcbiAgbWF4LWhlaWdodDogNjBweDtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLmxpbmtzU2VjdGlvblRpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luLWJvdHRvbTogNHB4O1xcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XFxufVxcblxcbi5pbXBvcnRhbnRMaW5rQ29udGFpbmVyIHtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDAgNjRweDtcXG4gIC8vIGZsZXgtd3JhcDogd3JhcDtcXG59XFxuXFxuLmltcG9ydGFudExpbmtDb250YWluZXIgLnNlY3Rpb25EaXYge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgcGFkZGluZy1yaWdodDogMjRweDtcXG4gICAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgICBvcmRlcjogMjtcXG4gIH1cXG5cXG4uaW1wb3J0YW50TGlua0NvbnRhaW5lciAuc2VjdGlvbkRpdiBzZWN0aW9uIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbi5hZGRyZXNzRGl2IHtcXG4gIGxlZnQ6IDA7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIC1tcy1mbGV4LW9yZGVyOiAxO1xcbiAgICAgIG9yZGVyOiAxO1xcbn1cXG5cXG4uYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciB7XFxuICAgIHdpZHRoOiAxOTJweDtcXG4gIH1cXG5cXG4uYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciAuYWRkcmVzcyB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgICBmb250LXN0eWxlOiBub3JtYWw7XFxuICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XFxuICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcXG4gICAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgfVxcblxcbi5hZGRyZXNzRGl2IC5hZGRyZXNzQ29udGFpbmVyIC5hZGRyZXNzIHAge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgfVxcblxcbi5hZGRyZXNzRGl2IC52ZXJ0aWNhbExpbmUge1xcbiAgICB3aWR0aDogMXB4O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmI7XFxuICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MCk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoOTApO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwKTtcXG4gICAgbWFyZ2luOiAwIDMycHg7XFxuICB9XFxuXFxuLmltcG9ydGFudExpbmsge1xcbiAgbWFyZ2luOiA2cHggMDtcXG59XFxuXFxuLmltcG9ydGFudExpbmsgYSB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XFxuICB9XFxuXFxuLmltcG9ydGFudExpbmsgYTpob3ZlciB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgfVxcblxcbi5zb2NpYWwge1xcbiAgcGFkZGluZy1sZWZ0OiAzMnB4O1xcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMSk7XFxuICBtYXJnaW4tbGVmdDogMDtcXG4gIC1tcy1mbGV4LW9yZGVyOiAzO1xcbiAgICAgIG9yZGVyOiAzO1xcbn1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5saW5rc1NlY3Rpb25UaXRsZTEge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIGNvbG9yOiAjMDAwO1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIge1xcbiAgICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIH1cXG5cXG4uc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIgLnNvY2lhbExpbmsge1xcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICBoZWlnaHQ6IDMycHg7XFxuICAgICAgfVxcblxcbi5zb2NpYWwgLnNvY2lhbExpbmtDb250YWluZXIgLnNvY2lhbExpbmtzV3JhcHBlciAuc29jaWFsTGluayBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgfVxcblxcbi5zb2NpYWwgLnNvY2lhbExpbmtDb250YWluZXIgLnNvY2lhbExpbmtzV3JhcHBlciAuc29jaWFsTGluayBpIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgICAgICBmb250LXNpemU6IDJyZW07XFxuICAgICAgICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICAgICAgfVxcblxcbi5zb2NpYWwgLnNvY2lhbExpbmtDb250YWluZXIgLnNvY2lhbExpbmtzV3JhcHBlciAuc29jaWFsTGluazpsYXN0LWNoaWxkIHtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjkwcHgpIHtcXG4gIC5sb2dvIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDMycHg7XFxuICB9XFxuXFxuICAuaW1wb3J0YW50TGlua0NvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDAgMzJweDtcXG4gIH1cXG5cXG4gICAgLmltcG9ydGFudExpbmtDb250YWluZXIgLnNlY3Rpb25EaXYge1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XFxuICAgIH1cXG4gICAgLmFkZHJlc3NEaXYgLmFkZHJlc3NDb250YWluZXIge1xcbiAgICAgIHdpZHRoOiAxNzJweDtcXG4gICAgfVxcblxcbiAgICAuYWRkcmVzc0RpdiAudmVydGljYWxMaW5lIHtcXG4gICAgICBtYXJnaW46IDAgMTJweDtcXG4gICAgfVxcblxcbiAgLnNvY2lhbCB7XFxuICAgIHBhZGRpbmctbGVmdDogMTJweDtcXG4gICAgLW1zLWZsZXg6IDEgMTtcXG4gICAgICAgIGZsZXg6IDEgMTtcXG4gIH1cXG4gICAgICAuc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIge1xcbiAgICAgICAgZGlzcGxheTogZ3JpZDtcXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XFxuICAgICAgICBtYXJnaW4tbGVmdDogMDtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICB9XFxuXFxuICAgICAgICAuc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIgLnNvY2lhbExpbmsge1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICAgIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMTEwcHgpIHtcXG4gICAgICAuc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIge1xcbiAgICAgICAgZGlzcGxheTogZ3JpZDtcXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgICAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAubG9nbyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICBmb290ZXIge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLm1vYmlsZV9sb2dvX3dyYXBwZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0OHB4O1xcbiAgICB3aWR0aDogNy43NXJlbTtcXG4gICAgaGVpZ2h0OiAzLjc1cmVtO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgIG1heC1oZWlnaHQ6IG5vbmU7XFxuICB9XFxuXFxuICAubW9iaWxlX2xvZ28ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbiAgLmxpbmtzU2VjdGlvblRpdGxlIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNDJweDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuaW1wb3J0YW50TGlua0NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBwYWRkaW5nOiAzMnB4IDIwcHg7XFxuICB9XFxuXFxuICAgIC5pbXBvcnRhbnRMaW5rQ29udGFpbmVyIHNlY3Rpb24ge1xcbiAgICAgIG1hcmdpbjogLTQ4cHggYXV0byAwIGF1dG87XFxuICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xcbiAgICB9XFxuXFxuICAgIC5pbXBvcnRhbnRMaW5rQ29udGFpbmVyIC5zZWN0aW9uRGl2IHtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgZGlzcGxheTogYmxvY2s7XFxuICAgICAgcGFkZGluZy1yaWdodDogMDtcXG4gICAgfVxcblxcbiAgLnNvY2lhbCB7XFxuICAgIHBhZGRpbmctbGVmdDogMDtcXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XFxuICB9XFxuXFxuICAgIC5zb2NpYWwgLnNvY2lhbExpbmtDb250YWluZXIge1xcbiAgICAgIHdpZHRoOiAyMDBweDtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgbWFyZ2luLXRvcDogMS41cmVtICFpbXBvcnRhbnQ7XFxuICAgIH1cXG5cXG4gICAgICAuc29jaWFsIC5zb2NpYWxMaW5rQ29udGFpbmVyIC5zb2NpYWxMaW5rc1dyYXBwZXIge1xcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNSwgMWZyKTtcXG4gICAgICB9XFxuXFxuICAuc29jaWFsTGluayBpIHtcXG4gICAgZm9udC1zaXplOiAyLjVyZW07XFxuICB9XFxuXFxuICAuYWRkcmVzc0RpdiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbi10b3A6IDE2cHg7XFxuICB9XFxuXFxuICAgIC5hZGRyZXNzRGl2IC5hZGRyZXNzQ29udGFpbmVyIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICBtYXJnaW46IDAgYXV0byAyNHB4IGF1dG87XFxuICAgICAgbWF4LXdpZHRoOiAyODBweDtcXG4gICAgfVxcblxcbiAgICAgIC5hZGRyZXNzRGl2IC5hZGRyZXNzQ29udGFpbmVyIC5saW5rc1NlY3Rpb25UaXRsZSB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWRkcmVzc0RpdiAuYWRkcmVzc0NvbnRhaW5lciAuYWRkcmVzcyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjcxO1xcbiAgICAgICAgb3BhY2l0eTogMC42O1xcbiAgICAgIH1cXG5cXG4gICAgLmFkZHJlc3NEaXYgLnZlcnRpY2FsTGluZSB7XFxuICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgfVxcblxcbiAgLmltcG9ydGFudExpbmsge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogMXJlbSAwO1xcbiAgfVxcblxcbiAgICAuaW1wb3J0YW50TGluayBhIHtcXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgICB9XFxuXFxuICAubGlua3NTZWN0aW9uVGl0bGUxIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImFkZEdyYXlcIjogXCJGb290ZXItYWRkR3JheS0zZWNmeFwiLFxuXHRcImxvZ29cIjogXCJGb290ZXItbG9nby0xVzN1VFwiLFxuXHRcIm1vYmlsZV9sb2dvX3dyYXBwZXJcIjogXCJGb290ZXItbW9iaWxlX2xvZ29fd3JhcHBlci0xVk1oUFwiLFxuXHRcImxpbmtzU2VjdGlvblRpdGxlXCI6IFwiRm9vdGVyLWxpbmtzU2VjdGlvblRpdGxlLURtd2pJXCIsXG5cdFwiaW1wb3J0YW50TGlua0NvbnRhaW5lclwiOiBcIkZvb3Rlci1pbXBvcnRhbnRMaW5rQ29udGFpbmVyLUFyT1RGXCIsXG5cdFwic2VjdGlvbkRpdlwiOiBcIkZvb3Rlci1zZWN0aW9uRGl2LTFKcFNFXCIsXG5cdFwiYWRkcmVzc0RpdlwiOiBcIkZvb3Rlci1hZGRyZXNzRGl2LVlUWHhlXCIsXG5cdFwiYWRkcmVzc0NvbnRhaW5lclwiOiBcIkZvb3Rlci1hZGRyZXNzQ29udGFpbmVyLTJaWS1zXCIsXG5cdFwiYWRkcmVzc1wiOiBcIkZvb3Rlci1hZGRyZXNzLXJGeUJPXCIsXG5cdFwidmVydGljYWxMaW5lXCI6IFwiRm9vdGVyLXZlcnRpY2FsTGluZS1BMWZJNVwiLFxuXHRcImltcG9ydGFudExpbmtcIjogXCJGb290ZXItaW1wb3J0YW50TGluay0zYVFQQlwiLFxuXHRcInNvY2lhbFwiOiBcIkZvb3Rlci1zb2NpYWwtbDJhcllcIixcblx0XCJzb2NpYWxMaW5rQ29udGFpbmVyXCI6IFwiRm9vdGVyLXNvY2lhbExpbmtDb250YWluZXItMUJqcV9cIixcblx0XCJsaW5rc1NlY3Rpb25UaXRsZTFcIjogXCJGb290ZXItbGlua3NTZWN0aW9uVGl0bGUxLTM0ekZ1XCIsXG5cdFwic29jaWFsTGlua3NXcmFwcGVyXCI6IFwiRm9vdGVyLXNvY2lhbExpbmtzV3JhcHBlci0yMW85RVwiLFxuXHRcInNvY2lhbExpbmtcIjogXCJGb290ZXItc29jaWFsTGluay0xUXZSdlwiLFxuXHRcIm1vYmlsZV9sb2dvXCI6IFwiRm9vdGVyLW1vYmlsZV9sb2dvLWpVQi1lXCJcbn07IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi8qIC5yb290IHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxufSAqL1xcblxcbi8qIC5ib2R5d3JhcHBlciB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBwYWRkaW5nLXRvcDogNzBweDtcXG59ICovXFxuXFxuLypcXG4ubmF2YmFye1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgbWluLWhlaWdodDogNzVweDtcXG4gIHBhZGRpbmc6IDJyZW0gNHJlbTtcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG5cXG59ICovXFxuXFxuLyogLm5hdmJhci1jb2xsYXBzZSB7XFxuICBtaW4taGVpZ2h0OiAxMDB2aDtcXG4gIGZsZXgtZ3JvdzogMDtcXG59XFxuLmRyb3Bkb3duLXRvZ2dsZTo6YWZ0ZXIge1xcbiAgZGlzcGxheTpub25lXFxufVxcbi5kcm9wZG93bi1tZW51IHtcXG4gIGJvcmRlcjogbm9uZTtcXG5cXG59XFxuLm5hdmJhci10b2dnbGVyIHtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuLm5hdmJhci10b2dnbGVyOmZvY3VzIHtcXG4gIGJveC1zaGFkb3c6IG5vbmU7XFxufVxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk5MnB4KSB7XFxuICAubmF2YmFyLWV4cGFuZC1sZyB7XFxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gICAgcGFkZGluZzoxcmVtIDRyZW07XFxuICB9XFxuICAubmF2YmFyLWNvbGxhcHNlIHtcXG4gICAgbWluLWhlaWdodDogYXV0bztcXG4gIH1cXG59ICovXFxuXFxuLyogLmhlYWRlclJvb3Qge1xcbiAgaGVpZ2h0OiA4OHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiAyN3B4IDUlO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG59XFxuXFxuLmxlZnQge1xcbiAgaGVpZ2h0OiA1MXB4O1xcbiAgd2lkdGg6IDEwNXB4O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLnJpZ2h0IHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1hcmdpbi1yaWdodDogMTRweDtcXG59XFxuXFxuLnJlcXVlc3REZW1vQ29udGFpbmVyIHtcXG4gIHdpZHRoOiA4M3B4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG4gIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgZmxvYXQ6IHJpZ2h0O1xcbn0gKi9cXG5cXG4vKiAucmVxdWVzdERlbW8ge1xcbi8vICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4vLyAgIGZvbnQtc2l6ZTogMTZweDtcXG4vLyAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuLy8gICBjb2xvcjogI2YzNjtcXG4vLyB9ICovXFxuXFxuLkhlYWRlclYyLXNjaGVkdWxlRGVtby1oX0NlUCB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgYm9yZGVyOiAxcHggc29saWQgI2YzNjtcXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXG4gIHdpZHRoOiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XFxuICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICBiYWNrZ3JvdW5kOiBub25lO1xcbiAgLW1zLWZsZXgtb3JkZXI6IDA7XFxuICAgICAgb3JkZXI6IDA7XFxufVxcblxcbi5IZWFkZXJWMi1zY2hlZHVsZURlbW8taF9DZVA6aG92ZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgNTEsIDEwMik7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC41cyBlYXNlLWluLW91dDtcXG4gIC1vLXRyYW5zaXRpb246IDAuNXMgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiAwLjVzIGVhc2UtaW4tb3V0O1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyV3JhcHBlci0zTUM2YyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHRvcDogMCU7XFxuICBsZWZ0OiAwJTtcXG4gIGhlaWdodDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIGhlaWdodDogLW1vei1maXQtY29udGVudDtcXG4gIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgb3BhY2l0eTogMTtcXG4gIHotaW5kZXg6IDI7XFxuICBtYXJnaW46IGF1dG87XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gIHBhZGRpbmc6IDE2cHg7XFxuICBwYWRkaW5nOiAxcmVtO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWl0ZW0tYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XFxufVxcblxcbi8qIC5mZWF0dXJlaXRlbSBpbWcge1xcbiAgd2lkdGg6IDIwcHg7XFxuICBoZWlnaHQ6IDIwcHg7XFxufSAqL1xcblxcbi8qIC5mZWF0dXJlaGVhZGluZyBpbWcge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxufSAqL1xcblxcbi5IZWFkZXJWMi1tZW51YnRuLTJNSm5hIHtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgZm9udC1zaXplOiAyMi40cHg7XFxuICBmb250LXNpemU6IDEuNHJlbTtcXG4gIG1hcmdpbi1sZWZ0OiAwO1xcbiAgLW1zLWZsZXgtb3JkZXI6IDI7XFxuICAgICAgb3JkZXI6IDI7XFxufVxcblxcbi5IZWFkZXJWMi1tZW51YnRuLTJNSm5hIGltZyB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbG9nby0zNEFKSSB7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICBoZWlnaHQ6IDIuNXJlbTtcXG4gIHdpZHRoOiA4MHB4O1xcbiAgd2lkdGg6IDVyZW07XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHRyYW5zcGFyZW50O1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1sb2dvLTM0QUpJIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGxlZnQ6IC0xMDAlO1xcbiAgdG9wOiA2OXB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMHZoO1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIHBhZGRpbmc6IDI0cHggMDtcXG4gIHBhZGRpbmc6IDEuNXJlbSAwO1xcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xcbiAgcGFkZGluZy1yaWdodDogMTZweDtcXG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XFxuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XFxuICAtby10cmFuc2l0aW9uOiAwLjRzO1xcbiAgdHJhbnNpdGlvbjogMC40cztcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBvdmVyZmxvdzogYXV0bztcXG4gIHotaW5kZXg6IDE7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkguSGVhZGVyVjItYWN0aXZlLXZaWDg0IHtcXG4gIGxlZnQ6IDAlO1xcbn1cXG5cXG4vKiAubmF2YmFyIC5tZW51IGxpIC5mZWF0dXJlc2Ryb3Bkb3duIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn0gKi9cXG5cXG4vKiAubmF2YmFyIC5tZW51IGxpIC5mZWF0dXJlc2Ryb3Bkb3duLmFjdGl2ZWZlYXR1cmVzIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufSAqL1xcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgLkhlYWRlclYyLWNvbXBhbnlkcm9wZG93bi0za0c0RCB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgLkhlYWRlclYyLWNvbXBhbnlkcm9wZG93bi0za0c0RC5IZWFkZXJWMi1hY3RpdmVkcm9wZG93bnMtX2N2dzYge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBvcGFjaXR5OiAxO1xcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCA+IGxpIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICAtby10cmFuc2l0aW9uOiBhbGwgMC40cztcXG4gIHRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgbWFyZ2luOiAwIDEycHggMjRweDtcXG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxufVxcblxcbi5IZWFkZXJWMi1idG5ncm91cC0xNzM4bSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi8qIGZlYXR1cmVoZWFkaW5nIHNwYW4ge1xcbiAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufSAqL1xcblxcbi8qIC5mZWF0dXJlaXRlbSBzcGFuIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbn0gKi9cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIID4gbGkgPiBzcGFuIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XFxuICBwYWRkaW5nOiAwIDAgMjRweCAwO1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXNpemU6IDE0cHg7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggPiBsaSA+IHNwYW4gPiBpbWcge1xcbiAgbWFyZ2luLXRvcDogM3B4O1xcbiAgbWFyZ2luLWxlZnQ6IDVweDtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggPiBsaSA+IGEge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcXG4gIHBhZGRpbmc6IDAgMCAyNHB4IDA7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBsaXN0LXN0eWxlOiBub25lO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIG9wYWNpdHk6IDA7XFxuICB6LWluZGV4OiAyO1xcbn1cXG5cXG4uSGVhZGVyVjItYWN0aXZlZHJvcGRvd25zLV9jdnc2IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn1cXG5cXG4uSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIGxpLFxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwgbGkgYSB7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaSBhIHtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiAxMnB4IDA7XFxufVxcblxcbi5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkgdWwgbGkgYSBpbWcge1xcbiAgd2lkdGg6IDIwcHg7XFxuICBoZWlnaHQ6IDIwcHg7XFxuICBtYXJnaW4tcmlnaHQ6IDhweDtcXG59XFxuXFxuLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaTpudGgtY2hpbGQoMSkgYSB7XFxuICBwYWRkaW5nOiAyNHB4IDAgMTJweCAwO1xcbn1cXG5cXG4uSGVhZGVyVjItbWVudWJ0bi0yTUpuYS5IZWFkZXJWMi1tZW51YWN0aXZlLTF1ZnYyIHtcXG4gIHotaW5kZXg6IDM7XFxufVxcblxcbi5IZWFkZXJWMi1sb2dpbi0zMVFHcyB7XFxuICBwYWRkaW5nOiA4cHggMTJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBiYWNrZ3JvdW5kOiAjZmZlYmYwO1xcbiAgY29sb3I6ICNmMzY7XFxuICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICBvcmRlcjogMTtcXG59XFxuXFxuLyogLmZlYXR1cmV3cmFwcGVyIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgcGFkZGluZzogMjRweCAwO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMzcsIDQwLCA0MywgMC4xKTtcXG5cXG4gIC5mZWF0dXJlIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICB3aWR0aDogNjAlO1xcblxcbiAgICAuZmVhdHVyZWl0ZW0ge1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICBtYXJnaW46IDI0cHggMDtcXG4gICAgfVxcblxcbiAgICAuZmVhdHVyZWl0ZW06bGFzdC1jaGlsZCB7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgfVxcbiAgfVxcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZXdyYXBwZXI6bGFzdC1jaGlsZCB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn0gKi9cXG5cXG4vKiAudmlld21vcmV3cmFwcGVyIHtcXG4gIGhlaWdodDogMjdweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiA5NXB4O1xcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZWhlYWRpbmcge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufSAqL1xcblxcbi8qIC52aWV3bW9yZSB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYmFja2dyb3VuZDogbm9uZTtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgcGFkZGluZzogMDtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBmb250LXdlaWdodDogNDAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn0gKi9cXG5cXG4vKiAuc2VhcmNoLWljb24ge1xcbiAgZm9udC1zaXplOiAxLjRyZW07XFxuICAgbWFyZ2luLWxlZnQ6IDZyZW07XFxuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XFxuICAgY29sb3I6ICNmZmY7XFxuICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG5cXG4uYnRuZ3JvdXAge1xcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4ubG9naW4ge1xcbiAgYm9yZGVyOiBub25lO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xcbiAgLm5hdmJhciB7XFxuICAgIHBhZGRpbmctbGVmdDogMnJlbTtcXG4gICAgcGFkZGluZy1yaWdodDogMnJlbTtcXG4gIH1cXG59XFxuKi9cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDk5MHB4KSB7XFxuICAvKiAuYm9keXdyYXBwZXIge1xcbiAgICBwYWRkaW5nLXRvcDogMDtcXG4gIH0gKi9cXG5cXG4gIC8qIC5mZWF0dXJlaGVhZGluZyBpbWcge1xcbiAgICB3aWR0aDogMjRweDtcXG4gICAgaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcXG4gIH0gKi9cXG5cXG4gIC5IZWFkZXJWMi1zY2hlZHVsZURlbW8taF9DZVAge1xcbiAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgLW1zLWZsZXgtb3JkZXI6IDA7XFxuICAgICAgICBvcmRlcjogMDtcXG4gICAgbWFyZ2luOiAwIDEycHg7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbG9naW4tMzFRR3Mge1xcbiAgICBwYWRkaW5nOiA4cHggMTZweDtcXG4gICAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgICBvcmRlcjogMTtcXG4gICAgbWFyZ2luOiAwIDEycHg7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyV3JhcHBlci0zTUM2YyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgICBwYWRkaW5nOiAxNnB4IDY0cHg7XFxuICAgIHBvc2l0aW9uOiBmaXhlZDtcXG4gIH1cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9XFxuXFxuICAgIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLWxvZ28tMzRBSkkge1xcbiAgICAgIHdpZHRoOiA2LjEyNXJlbTtcXG4gICAgICBoZWlnaHQ6IDNyZW07XFxuICAgIH1cXG5cXG4gICAgICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1sb2dvLTM0QUpJIGltZyB7XFxuICAgICAgICB3aWR0aDogNi4xMjVyZW07XFxuICAgICAgICBoZWlnaHQ6IDNyZW07XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIHtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB0b3A6IDA7XFxuICAgIGxlZnQ6IDA7XFxuICAgIGhlaWdodDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgaGVpZ2h0OiAtbW96LWZpdC1jb250ZW50O1xcbiAgICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIC8vbWFyZ2luLXJpZ2h0OiAzMHB4O1xcbiAgICBvdmVyZmxvdzogaW5pdGlhbDtcXG4gIH1cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggbGkge1xcbiAgICBib3JkZXI6IG5vbmU7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICB9XFxuXFxuICAvKiAuZmVhdHVyZWhlYWRpbmcgc3BhbiB7XFxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICAgIG1hcmdpbi10b3A6IDA7XFxuICB9ICovXFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIID4gbGkgPiBzcGFuIHtcXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gICAgcGFkZGluZy1ib3R0b206IDA7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIGEge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMDAwO1xcbiAgICBvcGFjaXR5OiAwLjY7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSAuSGVhZGVyVjItY29tcGFueWRyb3Bkb3duLTNrRzREIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDAlO1xcbiAgICByaWdodDogMDtcXG4gICAgd2lkdGg6IDE5MHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xcbiAgICBtYXJnaW4tbGVmdDogMDtcXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcXG4gICAgb3BhY2l0eTogMDtcXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgfVxcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSB1bCBsaSB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gICAgbWFyZ2luOiA0cHg7XFxuICAgIHdpZHRoOiBhdXRvO1xcbiAgICBwYWRkaW5nOiAwIDEycHg7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIGxpOmhvdmVyIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMzYsIDc2LCAxMTEsIDAuMSk7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIHVsIGxpOm50aC1jaGlsZCgxKSBhIHtcXG4gICAgcGFkZGluZy10b3A6IDEycHg7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItbmF2YmFyLTFaZFVLIC5IZWFkZXJWMi1tZW51LTN0VmJIIGxpIC5IZWFkZXJWMi1jb21wYW55ZHJvcGRvd24tM2tHNEQuSGVhZGVyVjItYWN0aXZlZHJvcGRvd25zLV9jdnc2IHtcXG4gICAgb3BhY2l0eTogMTtcXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gIH1cXG5cXG4gIC8qIC5mZWF0dXJlc2Ryb3Bkb3duIHtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICB9ICovXFxuXFxuICAvKiAubmF2YmFyIC5tZW51IGxpIC5mZWF0dXJlc2Ryb3Bkb3duIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDQwJTtcXG4gICAgbGVmdDogLTE1MCU7XFxuICAgIHdpZHRoOiA4ODhweDtcXG4gICAgaGVpZ2h0OiAzNTZweDtcXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLy8ganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgICBvcGFjaXR5OiAwO1xcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbi10b3A6IDMwcHg7XFxuICAgIHBhZGRpbmc6IDQycHg7XFxuICAgIC8vYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIH0gKi9cXG5cXG4gIC8qIC5uYXZiYXIgLm1lbnUgbGkgLmZlYXR1cmVzZHJvcGRvd24uYWN0aXZlZmVhdHVyZXMge1xcbiAgICBvcGFjaXR5OiAxO1xcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgfSAqL1xcblxcbiAgLkhlYWRlclYyLW5hdmJhci0xWmRVSyAuSGVhZGVyVjItbWVudS0zdFZiSCBsaSA+IHNwYW4ge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMDAwO1xcbiAgICBvcGFjaXR5OiAwLjY7XFxuICB9XFxuXFxuICAuSGVhZGVyVjItZmVhdHVyZU5hbWUtM2wtSjA6aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIH1cXG5cXG4gIC5IZWFkZXJWMi1uYXZiYXItMVpkVUsgLkhlYWRlclYyLW1lbnUtM3RWYkggPiBsaSA+IHNwYW4gPiAuSGVhZGVyVjItZmVhdHVyZU5hbWUtM2wtSjA6aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gICAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb24tY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcXG4gICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICB9XFxuXFxuICAvKiAuZmVhdHVyZXdyYXBwZXIge1xcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWF4LXdpZHRoOiAxNTZweDtcXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gICAgbWFyZ2luLXJpZ2h0OiAzMXB4O1xcblxcbiAgICAuZmVhdHVyZSB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDMxcHg7XFxuICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgcmdiKDM3LCA0MCwgNDMsIDAuMSk7XFxuICAgIH1cXG4gIH1cXG5cXG4gIC5mZWF0dXJld3JhcHBlcjpudGgtY2hpbGQoMikge1xcbiAgICAuZmVhdHVyZSB7XFxuICAgICAgcGFkZGluZy1yaWdodDogNDhweDtcXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLyogLmZlYXR1cmV3cmFwcGVyOmxhc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuXFxuICAgIC5mZWF0dXJlIHtcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICAgIGJvcmRlci1yaWdodDogbm9uZTtcXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLyogLnZpZXdtb3Jld3JhcHBlciB7XFxuICAgIHdpZHRoOiBhdXRvO1xcbiAgICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgICAvL21hcmdpbi10b3A6IC0xNXB4O1xcbiAgICAvL3BhZGRpbmctYm90dG9tOiAxMHB4O1xcblxcbiAgICAudmlld21vcmUge1xcbiAgICAgIHBhZGRpbmc6IDA7XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC8qIC5mZWF0dXJlaXRlbSB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgbWFyZ2luOiAxNXB4IDA7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gIH1cXG5cXG4gIC5mZWF0dXJlaXRlbTpob3ZlciB7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgICB0ZXh0LWRlY29yYXRpb24tY29sb3I6ICMyNTI4MmI7XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZWhlYWRpbmcge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIH0gKi9cXG5cXG4gIC8qIC5uYXZiYXIgLm1lbnUgbGk6aG92ZXIgLmZlYXR1cmVzZHJvcGRvd24ge1xcbiAgLy8gICBvcGFjaXR5OiAxO1xcbiAgLy8gICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xcbiAgLy8gfSAqL1xcbiAgLkhlYWRlclYyLW1lbnVidG4tMk1KbmEge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0hlYWRlclYyL0hlYWRlclYyLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7Ozs7OztJQU1JOztBQUVKOzs7SUFHSTs7QUFFSjs7Ozs7Ozs7SUFRSTs7QUFFSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXlCSTs7QUFFSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBMkJJOztBQUVKOzs7OztPQUtPOztBQUVQO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7TUFDZCxTQUFTO0NBQ2Q7O0FBRUQ7RUFDRSxvQ0FBb0M7RUFDcEMsWUFBWTtFQUNaLHFDQUFxQztFQUNyQyxnQ0FBZ0M7RUFDaEMsNkJBQTZCO0NBQzlCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLFFBQVE7RUFDUixTQUFTO0VBQ1QsNEJBQTRCO0VBQzVCLHlCQUF5QjtFQUN6QixvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLFdBQVc7RUFDWCxXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELGNBQWM7RUFDZCxjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLDRCQUE0QjtNQUN4QixtQkFBbUI7Q0FDeEI7O0FBRUQ7OztJQUdJOztBQUVKOzs7SUFHSTs7QUFFSjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7TUFDZCxTQUFTO0NBQ2Q7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsYUFBYTtFQUNiLGVBQWU7RUFDZixZQUFZO0VBQ1osWUFBWTtFQUNaLGdCQUFnQjtFQUNoQix5Q0FBeUM7Q0FDMUM7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osVUFBVTtFQUNWLFlBQVk7RUFDWixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLHlCQUF5QjtFQUN6QixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsV0FBVztDQUNaOztBQUVEO0VBQ0UsU0FBUztDQUNWOztBQUVEOzs7SUFHSTs7QUFFSjs7SUFFSTs7QUFFSjtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLHlDQUF5QztDQUMxQzs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDs7OztJQUlJOztBQUVKOzs7OztJQUtJOztBQUVKO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLDhCQUE4QjtFQUM5QixvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLGFBQWE7RUFDYixnQkFBZ0I7Q0FDakI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVztDQUNaOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDs7RUFFRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsWUFBWTtFQUNaLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLGtCQUFrQjtNQUNkLFNBQVM7Q0FDZDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXNCSTs7QUFFSjs7SUFFSTs7QUFFSjs7OztJQUlJOztBQUVKOzs7SUFHSTs7QUFFSjs7Ozs7Ozs7Ozs7O0lBWUk7O0FBRUo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQXdCRTs7QUFFRjtFQUNFOztNQUVJOztFQUVKOzs7O01BSUk7O0VBRUo7SUFDRSxrQkFBa0I7SUFDbEIsa0JBQWtCO1FBQ2QsU0FBUztJQUNiLGVBQWU7R0FDaEI7O0VBRUQ7SUFDRSxrQkFBa0I7SUFDbEIsa0JBQWtCO1FBQ2QsU0FBUztJQUNiLGVBQWU7R0FDaEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1oscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCxtQkFBbUI7SUFDbkIsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IscUJBQXFCO0lBQ3JCLGNBQWM7R0FDZjs7SUFFQztNQUNFLGdCQUFnQjtNQUNoQixhQUFhO0tBQ2Q7O01BRUM7UUFDRSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLHVCQUF1QjtXQUNwQixvQkFBb0I7T0FDeEI7O0VBRUw7SUFDRSxtQkFBbUI7SUFDbkIsT0FBTztJQUNQLFFBQVE7SUFDUiw0QkFBNEI7SUFDNUIseUJBQXlCO0lBQ3pCLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLG1CQUFtQjtRQUNmLDBCQUEwQjtJQUM5QixXQUFXO0lBQ1gscUJBQXFCO0lBQ3JCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixXQUFXO0lBQ1gsaUJBQWlCO0dBQ2xCOztFQUVEOzs7TUFHSTs7RUFFSjtJQUNFLG9CQUFvQjtJQUNwQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFFBQVE7SUFDUixTQUFTO0lBQ1QsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixxREFBcUQ7WUFDN0MsNkNBQTZDO0lBQ3JELGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtJQUNaLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLDBDQUEwQztHQUMzQzs7RUFFRDtJQUNFLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLFdBQVc7SUFDWCxvQkFBb0I7R0FDckI7O0VBRUQ7OztNQUdJOztFQUVKOzs7Ozs7Ozs7Ozs7Ozs7O01BZ0JJOztFQUVKOzs7O01BSUk7O0VBRUo7SUFDRSxnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLDJCQUEyQjtHQUM1Qjs7RUFFRDtJQUNFLDJCQUEyQjtJQUMzQixrREFBa0Q7WUFDMUMsMENBQTBDO0lBQ2xELCtCQUErQjtZQUN2Qix1QkFBdUI7R0FDaEM7O0VBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01Bb0JJOztFQUVKOzs7Ozs7O01BT0k7O0VBRUo7Ozs7Ozs7OztNQVNJOztFQUVKOzs7Ozs7Ozs7OztNQVdJOztFQUVKOzs7O01BSUk7O0VBRUo7OztTQUdPO0VBQ1A7SUFDRSxjQUFjO0dBQ2Y7Q0FDRlwiLFwiZmlsZVwiOlwiSGVhZGVyVjIuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIvKiAucm9vdCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xcbn0gKi9cXG5cXG4vKiAuYm9keXdyYXBwZXIge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgcGFkZGluZy10b3A6IDcwcHg7XFxufSAqL1xcblxcbi8qXFxuLm5hdmJhcntcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG1pbi1oZWlnaHQ6IDc1cHg7XFxuICBwYWRkaW5nOiAycmVtIDRyZW07XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuXFxufSAqL1xcblxcbi8qIC5uYXZiYXItY29sbGFwc2Uge1xcbiAgbWluLWhlaWdodDogMTAwdmg7XFxuICBmbGV4LWdyb3c6IDA7XFxufVxcbi5kcm9wZG93bi10b2dnbGU6OmFmdGVyIHtcXG4gIGRpc3BsYXk6bm9uZVxcbn1cXG4uZHJvcGRvd24tbWVudSB7XFxuICBib3JkZXI6IG5vbmU7XFxuXFxufVxcbi5uYXZiYXItdG9nZ2xlciB7XFxuICBib3JkZXI6IG5vbmU7XFxufVxcbi5uYXZiYXItdG9nZ2xlcjpmb2N1cyB7XFxuICBib3gtc2hhZG93OiBub25lO1xcbn1cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5OTJweCkge1xcbiAgLm5hdmJhci1leHBhbmQtbGcge1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAgIHBhZGRpbmc6MXJlbSA0cmVtO1xcbiAgfVxcbiAgLm5hdmJhci1jb2xsYXBzZSB7XFxuICAgIG1pbi1oZWlnaHQ6IGF1dG87XFxuICB9XFxufSAqL1xcblxcbi8qIC5oZWFkZXJSb290IHtcXG4gIGhlaWdodDogODhweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogMjdweCA1JTtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5sZWZ0IHtcXG4gIGhlaWdodDogNTFweDtcXG4gIHdpZHRoOiAxMDVweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5yaWdodCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtYXJnaW4tcmlnaHQ6IDE0cHg7XFxufVxcblxcbi5yZXF1ZXN0RGVtb0NvbnRhaW5lciB7XFxuICB3aWR0aDogODNweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmViZjA7XFxuICBwYWRkaW5nOiA4cHggMTZweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZsb2F0OiByaWdodDtcXG59ICovXFxuXFxuLyogLnJlcXVlc3REZW1vIHtcXG4vLyAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuLy8gICBmb250LXNpemU6IDE2cHg7XFxuLy8gICBsaW5lLWhlaWdodDogMS41O1xcbi8vICAgY29sb3I6ICNmMzY7XFxuLy8gfSAqL1xcblxcbi5zY2hlZHVsZURlbW8ge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogI2YzNjtcXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmMzY7XFxuICBwYWRkaW5nOiA4cHggMTJweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgYmFja2dyb3VuZDogbm9uZTtcXG4gIC1tcy1mbGV4LW9yZGVyOiAwO1xcbiAgICAgIG9yZGVyOiAwO1xcbn1cXG5cXG4uc2NoZWR1bGVEZW1vOmhvdmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDUxLCAxMDIpO1xcbiAgY29sb3I6ICNmZmY7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNXMgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiAwLjVzIGVhc2UtaW4tb3V0O1xcbiAgdHJhbnNpdGlvbjogMC41cyBlYXNlLWluLW91dDtcXG59XFxuXFxuLm5hdmJhcldyYXBwZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICB0b3A6IDAlO1xcbiAgbGVmdDogMCU7XFxuICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG9wYWNpdHk6IDE7XFxuICB6LWluZGV4OiAyO1xcbiAgbWFyZ2luOiBhdXRvO1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMjVweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICBwYWRkaW5nOiAxNnB4O1xcbiAgcGFkZGluZzogMXJlbTtcXG59XFxuXFxuLm5hdmJhciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTcwMHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1pdGVtLWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xcbn1cXG5cXG4vKiAuZmVhdHVyZWl0ZW0gaW1nIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZWhlYWRpbmcgaW1nIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbn0gKi9cXG5cXG4ubWVudWJ0biB7XFxuICBjb2xvcjogIzAwMDtcXG4gIGZvbnQtc2l6ZTogMjIuNHB4O1xcbiAgZm9udC1zaXplOiAxLjRyZW07XFxuICBtYXJnaW4tbGVmdDogMDtcXG4gIC1tcy1mbGV4LW9yZGVyOiAyO1xcbiAgICAgIG9yZGVyOiAyO1xcbn1cXG5cXG4ubWVudWJ0biBpbWcge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbi5uYXZiYXIgLmxvZ28ge1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgaGVpZ2h0OiAyLjVyZW07XFxuICB3aWR0aDogODBweDtcXG4gIHdpZHRoOiA1cmVtO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcXG59XFxuXFxuLm5hdmJhciAubG9nbyBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5uYXZiYXIgLm1lbnUge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBsZWZ0OiAtMTAwJTtcXG4gIHRvcDogNjlweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICBwYWRkaW5nOiAyNHB4IDA7XFxuICBwYWRkaW5nOiAxLjVyZW0gMDtcXG4gIHBhZGRpbmctbGVmdDogMTZweDtcXG4gIHBhZGRpbmctbGVmdDogMXJlbTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XFxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xcbiAgLW8tdHJhbnNpdGlvbjogMC40cztcXG4gIHRyYW5zaXRpb246IDAuNHM7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgb3ZlcmZsb3c6IGF1dG87XFxuICB6LWluZGV4OiAxO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51LmFjdGl2ZSB7XFxuICBsZWZ0OiAwJTtcXG59XFxuXFxuLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93biB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59ICovXFxuXFxuLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93bi5hY3RpdmVmZWF0dXJlcyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn0gKi9cXG5cXG4ubmF2YmFyIC5tZW51IGxpIC5jb21wYW55ZHJvcGRvd24ge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51IGxpIC5jb21wYW55ZHJvcGRvd24uYWN0aXZlZHJvcGRvd25zIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgb3BhY2l0eTogMTtcXG4gIHZpc2liaWxpdHk6IHZpc2libGU7XFxufVxcblxcbi5uYXZiYXIgLm1lbnUgPiBsaSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC40cztcXG4gIG1hcmdpbjogMCAxMnB4IDI0cHg7XFxuICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHRyYW5zcGFyZW50O1xcbn1cXG5cXG4uYnRuZ3JvdXAge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4vKiBmZWF0dXJlaGVhZGluZyBzcGFuIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn0gKi9cXG5cXG4vKiAuZmVhdHVyZWl0ZW0gc3BhbiB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG59ICovXFxuXFxuLm5hdmJhciAubWVudSA+IGxpID4gc3BhbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xcbiAgcGFkZGluZzogMCAwIDI0cHggMDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51ID4gbGkgPiBzcGFuID4gaW1nIHtcXG4gIG1hcmdpbi10b3A6IDNweDtcXG4gIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51ID4gbGkgPiBhIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgb3BhY2l0eTogMC42O1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XFxuICBwYWRkaW5nOiAwIDAgMjRweCAwO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51IGxpIHVsIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBvcGFjaXR5OiAwO1xcbiAgei1pbmRleDogMjtcXG59XFxuXFxuLmFjdGl2ZWRyb3Bkb3ducyB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuXFxuLm5hdmJhciAubWVudSBsaSB1bCBsaSxcXG4ubmF2YmFyIC5tZW51IGxpIHVsIGxpIGEge1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi5uYXZiYXIgLm1lbnUgbGkgdWwgbGkgYSB7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogMTJweCAwO1xcbn1cXG5cXG4ubmF2YmFyIC5tZW51IGxpIHVsIGxpIGEgaW1nIHtcXG4gIHdpZHRoOiAyMHB4O1xcbiAgaGVpZ2h0OiAyMHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XFxufVxcblxcbi5uYXZiYXIgLm1lbnUgbGkgdWwgbGk6bnRoLWNoaWxkKDEpIGEge1xcbiAgcGFkZGluZzogMjRweCAwIDEycHggMDtcXG59XFxuXFxuLm1lbnVidG4ubWVudWFjdGl2ZSB7XFxuICB6LWluZGV4OiAzO1xcbn1cXG5cXG4ubG9naW4ge1xcbiAgcGFkZGluZzogOHB4IDEycHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIG1hcmdpbi1yaWdodDogMTJweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgYmFja2dyb3VuZDogI2ZmZWJmMDtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgLW1zLWZsZXgtb3JkZXI6IDE7XFxuICAgICAgb3JkZXI6IDE7XFxufVxcblxcbi8qIC5mZWF0dXJld3JhcHBlciB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHBhZGRpbmc6IDI0cHggMDtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDM3LCA0MCwgNDMsIDAuMSk7XFxuXFxuICAuZmVhdHVyZSB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDYwJTtcXG5cXG4gICAgLmZlYXR1cmVpdGVtIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luOiAyNHB4IDA7XFxuICAgIH1cXG5cXG4gICAgLmZlYXR1cmVpdGVtOmxhc3QtY2hpbGQge1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG4gIH1cXG59ICovXFxuXFxuLyogLmZlYXR1cmV3cmFwcGVyOmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59ICovXFxuXFxuLyogLnZpZXdtb3Jld3JhcHBlciB7XFxuICBoZWlnaHQ6IDI3cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogOTVweDtcXG59ICovXFxuXFxuLyogLmZlYXR1cmVoZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn0gKi9cXG5cXG4vKiAudmlld21vcmUge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQ6IG5vbmU7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIHBhZGRpbmc6IDA7XFxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDQwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59ICovXFxuXFxuLyogLnNlYXJjaC1pY29uIHtcXG4gIGZvbnQtc2l6ZTogMS40cmVtO1xcbiAgIG1hcmdpbi1sZWZ0OiA2cmVtO1xcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xcbiAgIGNvbG9yOiAjZmZmO1xcbiAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxuXFxuLmJ0bmdyb3VwIHtcXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLmxvZ2luIHtcXG4gIGJvcmRlcjogbm9uZTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcXG4gIC5uYXZiYXIge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW07XFxuICAgIHBhZGRpbmctcmlnaHQ6IDJyZW07XFxuICB9XFxufVxcbiovXFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTBweCkge1xcbiAgLyogLmJvZHl3cmFwcGVyIHtcXG4gICAgcGFkZGluZy10b3A6IDA7XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZWhlYWRpbmcgaW1nIHtcXG4gICAgd2lkdGg6IDI0cHg7XFxuICAgIGhlaWdodDogMjRweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XFxuICB9ICovXFxuXFxuICAuc2NoZWR1bGVEZW1vIHtcXG4gICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAwO1xcbiAgICAgICAgb3JkZXI6IDA7XFxuICAgIG1hcmdpbjogMCAxMnB4O1xcbiAgfVxcblxcbiAgLmxvZ2luIHtcXG4gICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIC1tcy1mbGV4LW9yZGVyOiAxO1xcbiAgICAgICAgb3JkZXI6IDE7XFxuICAgIG1hcmdpbjogMCAxMnB4O1xcbiAgfVxcblxcbiAgLm5hdmJhcldyYXBwZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgcGFkZGluZzogMTZweCA2NHB4O1xcbiAgICBwb3NpdGlvbjogZml4ZWQ7XFxuICB9XFxuXFxuICAubmF2YmFyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogMTcwMHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgfVxcblxcbiAgICAubmF2YmFyIC5sb2dvIHtcXG4gICAgICB3aWR0aDogNi4xMjVyZW07XFxuICAgICAgaGVpZ2h0OiAzcmVtO1xcbiAgICB9XFxuXFxuICAgICAgLm5hdmJhciAubG9nbyBpbWcge1xcbiAgICAgICAgd2lkdGg6IDYuMTI1cmVtO1xcbiAgICAgICAgaGVpZ2h0OiAzcmVtO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcblxcbiAgLm5hdmJhciAubWVudSB7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgdG9wOiAwO1xcbiAgICBsZWZ0OiAwO1xcbiAgICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogLW1vei1maXQtY29udGVudDtcXG4gICAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICAvL21hcmdpbi1yaWdodDogMzBweDtcXG4gICAgb3ZlcmZsb3c6IGluaXRpYWw7XFxuICB9XFxuXFxuICAubmF2YmFyIC5tZW51IGxpIHtcXG4gICAgYm9yZGVyOiBub25lO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLyogLmZlYXR1cmVoZWFkaW5nIHNwYW4ge1xcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfSAqL1xcblxcbiAgLm5hdmJhciAubWVudSA+IGxpID4gc3BhbiB7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSBsaSBhIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzAwMDtcXG4gICAgb3BhY2l0eTogMC42O1xcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgLmNvbXBhbnlkcm9wZG93biB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiAwJTtcXG4gICAgcmlnaHQ6IDA7XFxuICAgIHdpZHRoOiAxOTBweDtcXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgbWFyZ2luLXRvcDogMzBweDtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgIGJhY2tncm91bmQ6ICNmZmY7XFxuICAgIG9wYWNpdHk6IDA7XFxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIH1cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgdWwgbGkge1xcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgIG1hcmdpbjogNHB4O1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgcGFkZGluZzogMCAxMnB4O1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSBsaSB1bCBsaTpob3ZlciB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjM2LCA3NiwgMTExLCAwLjEpO1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSBsaSB1bCBsaTpudGgtY2hpbGQoMSkgYSB7XFxuICAgIHBhZGRpbmctdG9wOiAxMnB4O1xcbiAgfVxcblxcbiAgLm5hdmJhciAubWVudSBsaSAuY29tcGFueWRyb3Bkb3duLmFjdGl2ZWRyb3Bkb3ducyB7XFxuICAgIG9wYWNpdHk6IDE7XFxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XFxuICB9XFxuXFxuICAvKiAuZmVhdHVyZXNkcm9wZG93biB7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgZmxleC13cmFwOiB3cmFwO1xcbiAgfSAqL1xcblxcbiAgLyogLm5hdmJhciAubWVudSBsaSAuZmVhdHVyZXNkcm9wZG93biB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA0MCU7XFxuICAgIGxlZnQ6IC0xNTAlO1xcbiAgICB3aWR0aDogODg4cHg7XFxuICAgIGhlaWdodDogMzU2cHg7XFxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC8vIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gICAgb3BhY2l0eTogMDtcXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xcbiAgICBwYWRkaW5nOiA0MnB4O1xcbiAgICAvL2JvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICB9ICovXFxuXFxuICAvKiAubmF2YmFyIC5tZW51IGxpIC5mZWF0dXJlc2Ryb3Bkb3duLmFjdGl2ZWZlYXR1cmVzIHtcXG4gICAgb3BhY2l0eTogMTtcXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIH0gKi9cXG5cXG4gIC5uYXZiYXIgLm1lbnUgbGkgPiBzcGFuIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogIzAwMDtcXG4gICAgb3BhY2l0eTogMC42O1xcbiAgfVxcblxcbiAgLmZlYXR1cmVOYW1lOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICB9XFxuXFxuICAubmF2YmFyIC5tZW51ID4gbGkgPiBzcGFuID4gLmZlYXR1cmVOYW1lOmhvdmVyIHtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICAgIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgfVxcblxcbiAgLyogLmZlYXR1cmV3cmFwcGVyIHtcXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1heC13aWR0aDogMTU2cHg7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIG1hcmdpbi1yaWdodDogMzFweDtcXG5cXG4gICAgLmZlYXR1cmUge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAzMXB4O1xcbiAgICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYigzNywgNDAsIDQzLCAwLjEpO1xcbiAgICB9XFxuICB9XFxuXFxuICAuZmVhdHVyZXdyYXBwZXI6bnRoLWNoaWxkKDIpIHtcXG4gICAgLmZlYXR1cmUge1xcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDQ4cHg7XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC8qIC5mZWF0dXJld3JhcHBlcjpsYXN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcblxcbiAgICAuZmVhdHVyZSB7XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC8qIC52aWV3bW9yZXdyYXBwZXIge1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gICAgLy9tYXJnaW4tdG9wOiAtMTVweDtcXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMTBweDtcXG5cXG4gICAgLnZpZXdtb3JlIHtcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAvKiAuZmVhdHVyZWl0ZW0ge1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIG1hcmdpbjogMTVweCAwO1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICB9XFxuXFxuICAuZmVhdHVyZWl0ZW06aG92ZXIge1xcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gICAgdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiAjMjUyODJiO1xcbiAgfSAqL1xcblxcbiAgLyogLmZlYXR1cmVoZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICB9ICovXFxuXFxuICAvKiAubmF2YmFyIC5tZW51IGxpOmhvdmVyIC5mZWF0dXJlc2Ryb3Bkb3duIHtcXG4gIC8vICAgb3BhY2l0eTogMTtcXG4gIC8vICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcXG4gIC8vIH0gKi9cXG4gIC5tZW51YnRuIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInNjaGVkdWxlRGVtb1wiOiBcIkhlYWRlclYyLXNjaGVkdWxlRGVtby1oX0NlUFwiLFxuXHRcIm5hdmJhcldyYXBwZXJcIjogXCJIZWFkZXJWMi1uYXZiYXJXcmFwcGVyLTNNQzZjXCIsXG5cdFwibmF2YmFyXCI6IFwiSGVhZGVyVjItbmF2YmFyLTFaZFVLXCIsXG5cdFwibWVudWJ0blwiOiBcIkhlYWRlclYyLW1lbnVidG4tMk1KbmFcIixcblx0XCJsb2dvXCI6IFwiSGVhZGVyVjItbG9nby0zNEFKSVwiLFxuXHRcIm1lbnVcIjogXCJIZWFkZXJWMi1tZW51LTN0VmJIXCIsXG5cdFwiYWN0aXZlXCI6IFwiSGVhZGVyVjItYWN0aXZlLXZaWDg0XCIsXG5cdFwiY29tcGFueWRyb3Bkb3duXCI6IFwiSGVhZGVyVjItY29tcGFueWRyb3Bkb3duLTNrRzREXCIsXG5cdFwiYWN0aXZlZHJvcGRvd25zXCI6IFwiSGVhZGVyVjItYWN0aXZlZHJvcGRvd25zLV9jdnc2XCIsXG5cdFwiYnRuZ3JvdXBcIjogXCJIZWFkZXJWMi1idG5ncm91cC0xNzM4bVwiLFxuXHRcIm1lbnVhY3RpdmVcIjogXCJIZWFkZXJWMi1tZW51YWN0aXZlLTF1ZnYyXCIsXG5cdFwibG9naW5cIjogXCJIZWFkZXJWMi1sb2dpbi0zMVFHc1wiLFxuXHRcImZlYXR1cmVOYW1lXCI6IFwiSGVhZGVyVjItZmVhdHVyZU5hbWUtM2wtSjBcIlxufTsiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLypcXG4gKiBCYXNlIHN0eWxlc1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuLypcXG4gKiBSZW1vdmUgdGV4dC1zaGFkb3cgaW4gc2VsZWN0aW9uIGhpZ2hsaWdodDpcXG4gKiBodHRwczovL3R3aXR0ZXIuY29tL21pa2V0YXlsci9zdGF0dXMvMTIyMjg4MDUzMDFcXG4gKlxcbiAqIFRoZXNlIHNlbGVjdGlvbiBydWxlIHNldHMgaGF2ZSB0byBiZSBzZXBhcmF0ZS5cXG4gKiBDdXN0b21pemUgdGhlIGJhY2tncm91bmQgY29sb3IgdG8gbWF0Y2ggeW91ciBkZXNpZ24uXFxuICovXFxuXFxuOjotbW96LXNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7XFxufVxcblxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTtcXG59XFxuXFxuLypcXG4gKiBBIGJldHRlciBsb29raW5nIGRlZmF1bHQgaG9yaXpvbnRhbCBydWxlXFxuICovXFxuXFxuaHIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJvcmRlcjogMDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIHBhZGRpbmc6IDA7XFxufVxcblxcbi8qXFxuICogUmVtb3ZlIHRoZSBnYXAgYmV0d2VlbiBhdWRpbywgY2FudmFzLCBpZnJhbWVzLFxcbiAqIGltYWdlcywgdmlkZW9zIGFuZCB0aGUgYm90dG9tIG9mIHRoZWlyIGNvbnRhaW5lcnM6XFxuICogaHR0cHM6Ly9naXRodWIuY29tL2g1YnAvaHRtbDUtYm9pbGVycGxhdGUvaXNzdWVzLzQ0MFxcbiAqL1xcblxcbmF1ZGlvLFxcbmNhbnZhcyxcXG5pZnJhbWUsXFxuaW1nLFxcbnN2ZyxcXG52aWRlbyB7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xcbn1cXG5cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwO1xcbn1cXG5cXG4vKlxcbiAqIEFsbG93IG9ubHkgdmVydGljYWwgcmVzaXppbmcgb2YgdGV4dGFyZWFzLlxcbiAqL1xcblxcbnRleHRhcmVhIHtcXG4gIHJlc2l6ZTogdmVydGljYWw7XFxufVxcblxcbi5MYXlvdXQtYm9keS1kYVZDQyB7XFxuICBtYXJnaW4tdG9wOiA3N3B4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0xheW91dC9MYXlvdXQuY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBOztnRkFFZ0Y7O0FBRWhGOzs7Ozs7R0FNRzs7QUFFSDtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCO0NBQ25COztBQUVEOztHQUVHOztBQUVIO0VBQ0UsZUFBZTtFQUNmLFlBQVk7RUFDWixVQUFVO0VBQ1YsMkJBQTJCO0VBQzNCLGNBQWM7RUFDZCxXQUFXO0NBQ1o7O0FBRUQ7Ozs7R0FJRzs7QUFFSDs7Ozs7O0VBTUUsdUJBQXVCO0NBQ3hCOztBQUVEOztHQUVHOztBQUVIO0VBQ0UsVUFBVTtFQUNWLFVBQVU7RUFDVixXQUFXO0NBQ1o7O0FBRUQ7O0dBRUc7O0FBRUg7RUFDRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxpQkFBaUI7Q0FDbEJcIixcImZpbGVcIjpcIkxheW91dC5jc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLypcXG4gKiBCYXNlIHN0eWxlc1xcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXFxuXFxuLypcXG4gKiBSZW1vdmUgdGV4dC1zaGFkb3cgaW4gc2VsZWN0aW9uIGhpZ2hsaWdodDpcXG4gKiBodHRwczovL3R3aXR0ZXIuY29tL21pa2V0YXlsci9zdGF0dXMvMTIyMjg4MDUzMDFcXG4gKlxcbiAqIFRoZXNlIHNlbGVjdGlvbiBydWxlIHNldHMgaGF2ZSB0byBiZSBzZXBhcmF0ZS5cXG4gKiBDdXN0b21pemUgdGhlIGJhY2tncm91bmQgY29sb3IgdG8gbWF0Y2ggeW91ciBkZXNpZ24uXFxuICovXFxuXFxuOjotbW96LXNlbGVjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjYjNkNGZjO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7XFxufVxcblxcbjo6c2VsZWN0aW9uIHtcXG4gIGJhY2tncm91bmQ6ICNiM2Q0ZmM7XFxuICB0ZXh0LXNoYWRvdzogbm9uZTtcXG59XFxuXFxuLypcXG4gKiBBIGJldHRlciBsb29raW5nIGRlZmF1bHQgaG9yaXpvbnRhbCBydWxlXFxuICovXFxuXFxuaHIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDFweDtcXG4gIGJvcmRlcjogMDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIHBhZGRpbmc6IDA7XFxufVxcblxcbi8qXFxuICogUmVtb3ZlIHRoZSBnYXAgYmV0d2VlbiBhdWRpbywgY2FudmFzLCBpZnJhbWVzLFxcbiAqIGltYWdlcywgdmlkZW9zIGFuZCB0aGUgYm90dG9tIG9mIHRoZWlyIGNvbnRhaW5lcnM6XFxuICogaHR0cHM6Ly9naXRodWIuY29tL2g1YnAvaHRtbDUtYm9pbGVycGxhdGUvaXNzdWVzLzQ0MFxcbiAqL1xcblxcbmF1ZGlvLFxcbmNhbnZhcyxcXG5pZnJhbWUsXFxuaW1nLFxcbnN2ZyxcXG52aWRlbyB7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xcbn1cXG5cXG4vKlxcbiAqIFJlbW92ZSBkZWZhdWx0IGZpZWxkc2V0IHN0eWxlcy5cXG4gKi9cXG5cXG5maWVsZHNldCB7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiAwO1xcbn1cXG5cXG4vKlxcbiAqIEFsbG93IG9ubHkgdmVydGljYWwgcmVzaXppbmcgb2YgdGV4dGFyZWFzLlxcbiAqL1xcblxcbnRleHRhcmVhIHtcXG4gIHJlc2l6ZTogdmVydGljYWw7XFxufVxcblxcbi5ib2R5IHtcXG4gIG1hcmdpbi10b3A6IDc3cHg7XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJib2R5XCI6IFwiTGF5b3V0LWJvZHktZGFWQ0NcIlxufTsiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzIGZyb20gJy4vRm9vdGVyLnNjc3MnO1xuXG5jb25zdCBGT09URVJfTElOS1MgPSBbXG4gIC8qIHtcbiAgICB0aXRsZTogJ0NvbXBhbnknLFxuICAgIGxpbmtzOiBbXG4gICAgICB7IGxhYmVsOiAnQWJvdXQgVXMnLCB1cmw6ICcvYWJvdXQtdXMnIH0sXG4gICAgICB7IGxhYmVsOiAnVGVhbScsIHVybDogJy90ZWFtJyB9LFxuICAgICAgeyBsYWJlbDogJ0N1bHR1cmUnLCB1cmw6ICcvY3VsdHVyZScgfSxcbiAgICAgIHsgbGFiZWw6ICdDYXJlZXJzJywgdXJsOiAnL2NhcmVlcnMnIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHRpdGxlOiAnUHJvZHVjdCcsXG4gICAgbGlua3M6IFtcbiAgICAgIHsgbGFiZWw6ICdHZXRSYW5rcycsIHVybDogJy9wcm9kdWN0cy9qZWV0JyB9LFxuICAgICAgeyBsYWJlbDogJ0FDQURTJywgdXJsOiAnL3Byb2R1Y3RzL2FjYWRzJyB9LFxuICAgICAgeyBsYWJlbDogJ1ByZXAnLCB1cmw6ICcvcHJvZHVjdHMvcHJlcCcgfSxcbiAgICAgIHsgbGFiZWw6ICdRTVMnLCB1cmw6ICcvcHJvZHVjdHMvcW1zJyB9LFxuICAgIF0sXG4gIH0sICovXG4gIHtcbiAgICB0aXRsZTogJ0xpbmtzJyxcbiAgICBsaW5rczogW1xuICAgICAgeyBsYWJlbDogJ0Fib3V0IFVzJywgdXJsOiAnL2NvbXBhbnkvYWJvdXR1cycgfSxcbiAgICAgIHsgbGFiZWw6ICdQcmljaW5nIFBsYW4nLCB1cmw6ICcvcHJpY2luZycgfSxcbiAgICAgIHsgbGFiZWw6ICdDb250YWN0IHVzJywgdXJsOiAnL3JlcXVlc3QtZGVtbycgfSxcbiAgICAgIHsgbGFiZWw6ICdQcml2YWN5IFBvbGljeScsIHVybDogJy9wcml2YWN5LWFuZC10ZXJtcycgfSxcbiAgICAgIHtcbiAgICAgICAgbGFiZWw6ICdDYW5jZWxsYXRpb24gJiBSZXR1cm4gUG9saWN5JyxcbiAgICAgICAgdXJsOiAnL2NhbmNlbGxhdGlvbi1hbmQtcmV0dXJuJyxcbiAgICAgIH0sXG4gICAgICB7IGxhYmVsOiAnVGVybXMgJiBDb25kaXRpb25zJywgdXJsOiAnL3Rlcm1zLWFuZC1jb25kaXRpb25zJyB9LFxuICAgIF0sXG4gIH0sXG5dO1xuXG5jb25zdCBTT0NJQUxfTElOS1MgPSBbXG4gIHtcbiAgICBrZXk6ICdmYWNlYm9vaycsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL0VnbmlmeS8nLFxuICAgIGljb246ICcvaW1hZ2VzL2Zvb3Rlci9mYWNlYm9vay5zdmcnLFxuICB9LFxuICB7XG4gICAga2V5OiAneW91dHViZScsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vdXNlci95ZXJyYW5hZ3UnLFxuICAgIGljb246ICcvaW1hZ2VzL2Zvb3Rlci95b3V0dWJlLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBrZXk6ICdsaW5rZWRpbicsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2NvbXBhbnkvZWduaWZ5JyxcbiAgICBpY29uOiAnL2ltYWdlcy9mb290ZXIvbGlua2VkaW4uc3ZnJyxcbiAgfSxcbiAge1xuICAgIGtleTogJ2luc3RhZ3JhbScsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cuaW5zdGFncmFtLmNvbS9lZ25pZnknLFxuICAgIGljb246ICcvaW1hZ2VzL2Zvb3Rlci9pbnN0YWdyYW0uc3ZnJyxcbiAgfSxcbiAge1xuICAgIGtleTogJ3R3aXR0ZXInLFxuICAgIHVybDogJ2h0dHBzOi8vdHdpdHRlci5jb20vZWduaWZ5JyxcbiAgICBpY29uOiAnL2ltYWdlcy9mb290ZXIvdHdpdHRlci5zdmcnLFxuICB9LFxuXTtcblxuY2xhc3MgRm9vdGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBpc0FzaDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgfTtcbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgaXNBc2ggfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxmb290ZXIgY2xhc3NOYW1lPXtpc0FzaCA/IHMuYWRkR3JheSA6IG51bGx9PlxuICAgICAgICA8aW1nXG4gICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9nZXRyYW5rcy1tYXJrZXRpbmctbmV3LnN2Z1wiXG4gICAgICAgICAgYWx0PVwiZ2V0cmFua3MgYnkgZWduaWZ5XCJcbiAgICAgICAgICBjbGFzc05hbWU9e3MubG9nb31cbiAgICAgICAgLz5cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Byb3cgJHtzLmltcG9ydGFudExpbmtDb250YWluZXJ9YH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25EaXZ9PlxuICAgICAgICAgICAgICAgIHtGT09URVJfTElOS1MubWFwKHNlY3Rpb24gPT4gKFxuICAgICAgICAgICAgICAgICAgPHNlY3Rpb24+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxpbmtzU2VjdGlvblRpdGxlfT57c2VjdGlvbi50aXRsZX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICAgIHtzZWN0aW9uLmxpbmtzLm1hcChzZWN0aW9uTGluayA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPXtzLmltcG9ydGFudExpbmt9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9e3NlY3Rpb25MaW5rLnVybH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VjdGlvbkxpbmsubGFiZWwgPT09ICdCbG9nJyA/ICdfYmxhbmsnIDogJydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7c2VjdGlvbkxpbmsubGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2JpbGVfbG9nb193cmFwcGVyfT5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL2dldHJhbmtzLW1hcmtldGluZy1uZXcuc3ZnXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cImdldHJhbmtzIGJ5IGVnbmlmeVwiXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MubW9iaWxlX2xvZ299XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3NEaXZ9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3NDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGlua3NTZWN0aW9uVGl0bGV9Pkh5ZGVyYWJhZCBPZmZpY2UtMTwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWRkcmVzc30+XG4gICAgICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgICAgIEtyaXNoZSBFbWVyYWxkLCBLb25kYXB1ciBNYWluIFJvYWQsIExheG1pIEN5YmVyIENpdHksXG4gICAgICAgICAgICAgICAgICAgICAgV2hpdGVmaWVsZHMsIEtvbmRhcHVyLCBIeWRlcmFiYWQsIFRlbGFuZ2FuYSA1MDAwODFcbiAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmVydGljYWxMaW5lfSAvPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFkZHJlc3NDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGlua3NTZWN0aW9uVGl0bGV9Pkh5ZGVyYWJhZCBPZmZpY2UtMjwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWRkcmVzc30+XG4gICAgICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgICAgIDEtMi8xLzI0L0EvMSw3LCBQbG90IE5vIDE3IE9wcC4gQmhhcmF0IFBldHJvbCBCdW5rIEpOVFVcbiAgICAgICAgICAgICAgICAgICAgICBSZCwgSEktVEVDSCBDaXR5IEh5ZGVyYWJhZCwgVGVsYW5nYW5hIDUwMDA4NFxuICAgICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52ZXJ0aWNhbExpbmV9IC8+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWRkcmVzc0NvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5saW5rc1NlY3Rpb25UaXRsZX0+QmVuZ2FsdXJ1IE9mZmljZTwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWRkcmVzc30+XG4gICAgICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgICAgIFByZXN0aWdlIEF0bGFudGEsIDgwIEZlZXQgTWFpbiBSb2FkLCBLb3JhbWFuZ2FsYSAxQSBCbG9jayxcbiAgICAgICAgICAgICAgICAgICAgICBCZW5nYWx1cnUsS2FybmF0YWthIDU2MDAzNFxuICAgICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52ZXJ0aWNhbExpbmV9IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zb2NpYWx9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgcm93ICR7cy5zb2NpYWxMaW5rQ29udGFpbmVyfWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGlua3NTZWN0aW9uVGl0bGUxfT5Tb2NpYWw8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgcm93ICR7cy5zb2NpYWxMaW5rc1dyYXBwZXJ9YH0+XG4gICAgICAgICAgICAgICAgICAgIHtTT0NJQUxfTElOS1MubWFwKHNvY2lhbExpbmsgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Muc29jaWFsTGlua31cbiAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9e3NvY2lhbExpbmsudXJsfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxuICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtzb2NpYWxMaW5rLmljb259IGFsdD17c29jaWFsTGluay5rZXl9IC8+XG4gICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZm9vdGVyPlxuICAgICk7XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoRm9vdGVyKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vRm9vdGVyLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vRm9vdGVyLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Gb290ZXIuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExpbmsgZnJvbSAnY29tcG9uZW50cy9MaW5rL0xpbmsnO1xuaW1wb3J0IHMgZnJvbSAnLi9IZWFkZXJWMi5zY3NzJztcblxuY2xhc3MgSGVhZGVyVjIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgbmF2T3BlbjogZmFsc2UsXG4gICAgICBtb2JpbGU6IGZhbHNlLFxuICAgICAgc2hvd2NvbXBhbnk6IGZhbHNlLFxuICAgICAgc2hvd1Byb2R1Y3RzOiBmYWxzZSxcbiAgICB9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5oYW5kbGVzaXplKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlc2l6ZSk7XG5cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgIGlmIChcbiAgICAgICAgLyohIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmZWF0dXJlc2xhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KSAmJiAqL1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29tcGFueWxhYmVsJykgJiZcbiAgICAgICAgIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb21wYW55bGFiZWwnKS5jb250YWlucyhldmVudC50YXJnZXQpXG4gICAgICAgIC8vIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwcm9kdWN0c2xhYmVsJykgJiZcbiAgICAgICAgLy8gIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwcm9kdWN0c2xhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KVxuICAgICAgICAvKiAhZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jlc291cmNlc2xhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KSAqL1xuICAgICAgKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIC8vIHNob3dmZWF0dXJlczogZmFsc2UsXG4gICAgICAgICAgc2hvd2NvbXBhbnk6IGZhbHNlLFxuICAgICAgICAgIC8vIHNob3dQcm9kdWN0czogZmFsc2UsXG4gICAgICAgICAgLy8gc2hvd3Jlc291cmNlczogZmFsc2UsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmhhbmRsZXNpemUpO1xuICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4ge1xuICAgICAgaWYgKFxuICAgICAgICAvLyAhZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Byb2R1Y3RzbGFiZWwnKS5jb250YWlucyhldmVudC50YXJnZXQpXG4gICAgICAgICFkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29tcGFueWxhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KVxuICAgICAgICAvLyAhZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jlc291cmNlc2xhYmVsJykuY29udGFpbnMoZXZlbnQudGFyZ2V0KVxuICAgICAgKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIC8vIHNob3dmZWF0dXJlczogZmFsc2UsXG4gICAgICAgICAgc2hvd2NvbXBhbnk6IGZhbHNlLFxuICAgICAgICAgIC8vIHNob3dQcm9kdWN0czogZmFsc2UsXG4gICAgICAgICAgLy8gc2hvd3Jlc291cmNlczogZmFsc2UsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgaGFuZGxlc2l6ZSA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTApIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb2JpbGU6IHRydWUgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb2JpbGU6IGZhbHNlIH0pO1xuICAgIH1cbiAgfTtcblxuICB0b2dnbGVOYXZiYXIgPSAoY2xvc2UgPSBmYWxzZSkgPT4ge1xuICAgIGlmIChjbG9zZSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG5hdk9wZW46IGZhbHNlIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgbmF2T3BlbjogIXRoaXMuc3RhdGUubmF2T3BlbiB9KTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlc2hvd2NvbXBhbnkgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93Y29tcGFueTogIXRoaXMuc3RhdGUuc2hvd2NvbXBhbnksXG4gICAgICBzaG93UHJvZHVjdHM6IGZhbHNlLFxuICAgICAgLy8gc2hvd3Jlc291cmNlczogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlc2hvd1Byb2R1Y3QgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93Y29tcGFueTogZmFsc2UsXG4gICAgICBzaG93UHJvZHVjdHM6ICF0aGlzLnN0YXRlLnNob3dQcm9kdWN0cyxcbiAgICB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm5hdmJhcldyYXBwZXJ9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5uYXZiYXJ9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxvZ299PlxuICAgICAgICAgICAgPExpbmsgdG89XCIvXCI+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL2dldHJhbmtzLW1hcmtldGluZy1uZXcuc3ZnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJnZXRyYW5rc1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHVsXG4gICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICB0aGlzLnN0YXRlLm5hdk9wZW4gPyBgJHtzLm1lbnV9ICR7cy5hY3RpdmV9YCA6IGAke3MubWVudX1gXG4gICAgICAgICAgICB9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgey8qIDxsaVxuICAgICAgICAgICAgICBpZD1cImZlYXR1cmVzbGFiZWxcIlxuICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZWZlYXR1cmVzfVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5GZWF0dXJlczwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dmZWF0dXJlc1xuICAgICAgICAgICAgICAgICAgICAgID8gJ2ltYWdlcy9pY29ucy9jaGV2cm9uLXVwLnN2ZydcbiAgICAgICAgICAgICAgICAgICAgICA6ICdpbWFnZXMvaWNvbnMvY2hldnJvbi1kb3duLnN2ZydcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGFsdD1cImRvd24tYW5nbGVcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dmZWF0dXJlc1xuICAgICAgICAgICAgICAgICAgICA/IGAke3MuZmVhdHVyZXNkcm9wZG93bn0gJHtzLmFjdGl2ZWZlYXR1cmVzfWBcbiAgICAgICAgICAgICAgICAgICAgOiBgJHtzLmZlYXR1cmVzZHJvcGRvd259YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtPTERfTElWRUNMQVNTRVNEQVRBLm1hcChmZWF0dXJlID0+IChcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmZlYXR1cmV3cmFwcGVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZmVhdHVyZX0+XG4gICAgICAgICAgICAgICAgICAgICAge2ZlYXR1cmUuZmVhdHVyZXMubWFwKGl0ZW0gPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICFpdGVtLmlzSGVhZGluZyA/IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5mZWF0dXJlaXRlbX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZXJvdXRpbmcoaXRlbS5yb3V0ZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29ufSBhbHQ9e2l0ZW0udGl0bGV9IC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2l0ZW0udGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuZmVhdHVyZWhlYWRpbmd9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZXJvdXRpbmcoaXRlbS5yb3V0ZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29ufSBhbHQ9e2l0ZW0udGl0bGV9IC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2l0ZW0udGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Mudmlld21vcmV3cmFwcGVyfVxuICAgICAgICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlcm91dGluZyhmZWF0dXJlLnJvdXRlKTtcbiAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9e3Mudmlld21vcmV9PkxlYXJuIG1vcmUgJiM4NTk0OzwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9saT4gKi99XG4gICAgICAgICAgICB7LyogPGxpPlxuICAgICAgICAgICAgICA8TGluayB0bz1cIi9jdXN0b21lcnNcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PldoeSBlZ25pZnk8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICA8L2xpPiAqL31cbiAgICAgICAgICAgIHsvKiA8bGk+XG4gICAgICAgICAgICAgIDxMaW5rIHRvPVwiL2hvdy1lZ25pZnktd29ya3NcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PkhvdyBpdCB3b3Jrczwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDwvbGk+ICovfVxuICAgICAgICAgICAgPGxpXG4gICAgICAgICAgICAgIGlkPVwicHJvZHVjdHNsYWJlbFwiXG4gICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuaGFuZGxlc2hvd1Byb2R1Y3R9XG4gICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PlByb2R1Y3RzPC9zcGFuPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuc2hvd1Byb2R1Y3RzXG4gICAgICAgICAgICAgICAgICAgICAgPyAnL2ltYWdlcy9pY29ucy9jaGV2cm9uLXVwLnN2ZydcbiAgICAgICAgICAgICAgICAgICAgICA6ICcvaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmcnXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJkb3duLWFuZ2xlXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDx1bFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dQcm9kdWN0c1xuICAgICAgICAgICAgICAgICAgICA/IGAke3MuY29tcGFueWRyb3Bkb3dufSAke3MuYWN0aXZlZHJvcGRvd25zfWBcbiAgICAgICAgICAgICAgICAgICAgOiBgJHtzLmNvbXBhbnlkcm9wZG93bn1gXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbW9kdWxlcy90ZXN0c1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L21vZHVsZV90ZXN0LnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwib25saW5lLXRlc3RzXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+T25saW5lIFRlc3RzPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbW9kdWxlcy9saXZlY2xhc3Nlc1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfTGl2ZS5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cImxpdmUtY2xhc3Nlc1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPkxpdmUgQ2xhc3Nlczwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL21vZHVsZXMvZG91YnRzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9Eb3VidHMuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJvbmxpbmUtdGVzdHNcIlxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj5Eb3VidHM8L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8TGluayB0bz1cIi9tb2R1bGVzL2Fzc2lnbm1lbnRzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9Bc3NpZ25tZW50cy5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPkFzc2lnbm1lbnRzPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbW9kdWxlcy9jb25uZWN0XCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0Nvbm5lY3QvbmV3X2Nvbm5lY3Quc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJjb25uZWN0XCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+Q29ubmVjdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgPExpbmsgdG89XCIvY3VzdG9tZXJzXCIgb25DbGljaz17KCkgPT4gdGhpcy50b2dnbGVOYXZiYXIoZmFsc2UpfT5cbiAgICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5mZWF0dXJlTmFtZX0+Q3VzdG9tZXJzPC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgPExpbmsgdG89XCIvcHJpY2luZ1wiIG9uQ2xpY2s9e3RoaXMudG9nZ2xlTmF2YmFyfT5cbiAgICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5mZWF0dXJlTmFtZX0+UHJpY2luZzwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8bGlcbiAgICAgICAgICAgICAgaWQ9XCJjb21wYW55bGFiZWxcIlxuICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZXNob3djb21wYW55fVxuICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfT5Db21wYW55PC9zcGFuPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuc2hvd2NvbXBhbnlcbiAgICAgICAgICAgICAgICAgICAgICA/ICcvaW1hZ2VzL2ljb25zL2NoZXZyb24tdXAuc3ZnJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJy9pbWFnZXMvaWNvbnMvY2hldnJvbi1kb3duLnN2ZydcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGFsdD1cImRvd24tYW5nbGVcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPHVsXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuc2hvd2NvbXBhbnlcbiAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmNvbXBhbnlkcm9wZG93bn0gJHtzLmFjdGl2ZWRyb3Bkb3duc31gXG4gICAgICAgICAgICAgICAgICAgIDogYCR7cy5jb21wYW55ZHJvcGRvd259YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHsvKiA8bGk+QWJvdXQgVXM8L2xpPiAqL31cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8TGluayB0bz1cIi9jb21wYW55L2Fib3V0dXNcIiBvbkNsaWNrPXt0aGlzLnRvZ2dsZU5hdmJhcn0+XG4gICAgICAgICAgICAgICAgICAgIEFib3V0IFVzXG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8TGluayB0bz1cIi9jb21wYW55L3ByZXNzXCIgb25DbGljaz17dGhpcy50b2dnbGVOYXZiYXJ9PlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj5QcmVzczwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rXG4gICAgICAgICAgICAgICAgICAgIHRvPVwiL2NvbXBhbnkvY3VsdHVyZVwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMudG9nZ2xlTmF2YmFyKGZhbHNlKX1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+Q3VsdHVyZTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIvY29tcGFueS9jYXJlZXJzXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy50b2dnbGVOYXZiYXIoZmFsc2UpfVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj5DYXJlZXJzPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgey8qIDxsaVxuICAgICAgICAgICAgICBpZD1cInJlc291cmNlc2xhYmVsXCJcbiAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVyZXNvdXJjZXN9XG4gICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuZmVhdHVyZU5hbWV9PlJlc291cmNlczwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dyZXNvdXJjZXNcbiAgICAgICAgICAgICAgICAgICAgICA/ICdpbWFnZXMvaWNvbnMvY2hldnJvbi11cC5zdmcnXG4gICAgICAgICAgICAgICAgICAgICAgOiAnaW1hZ2VzL2ljb25zL2NoZXZyb24tZG93bi5zdmcnXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJkb3duLWFuZ2xlXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDx1bFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnNob3dyZXNvdXJjZXNcbiAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmNvbXBhbnlkcm9wZG93bn0gJHtzLmFjdGl2ZWRyb3Bkb3duc31gXG4gICAgICAgICAgICAgICAgICAgIDogYCR7cy5jb21wYW55ZHJvcGRvd259YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxsaVxuICAgICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZXJvdXRpbmcoJ1Byb2R1Y3RUb3VyJyl9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgUHJvZHVjdCBUb3VyXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgIDwvbGk+ICovfVxuICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgIHdpbmRvdy5vcGVuKCdodHRwczovL2Jsb2cuZWduaWZ5LmNvbScsICdibGFuaycpO1xuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmZlYXR1cmVOYW1lfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgQmxvZ1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgIDwvdWw+XG5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5idG5ncm91cH0+XG4gICAgICAgICAgICA8TGluayB0bz1cIi9yZXF1ZXN0LWRlbW9cIj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5zY2hlZHVsZURlbW99XG4gICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy50b2dnbGVOYXZiYXIodHJ1ZSl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5tb2JpbGUgPyAnRGVtbycgOiAnU0NIRURVTEUgREVNTyd9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vYWNjb3VudC5nZXRyYW5rcy5pbi9zaWduaW4/aG9zdD1odHRwcyUzQSUyRiUyRmdldHJhbmtzLmluJTJGXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmxvZ2lufVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBMb2dpblxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLnRvZ2dsZU5hdmJhcihmYWxzZSl9XG4gICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUubmF2T3BlbiA/IGAke3MubWVudWJ0bn0gJHtzLm1lbnVhY3RpdmV9YCA6IHMubWVudWJ0blxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgIXRoaXMuc3RhdGUubmF2T3BlblxuICAgICAgICAgICAgICAgICAgICA/ICcvaW1hZ2VzL2ljb25zL2RyYXdlci5zdmcnXG4gICAgICAgICAgICAgICAgICAgIDogJy9pbWFnZXMvaWNvbnMvY2xvc2Uuc3ZnJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBhbHQ9XCJ0b2dnbGUgYnV0dG9uXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoSGVhZGVyVjIpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9IZWFkZXJWMi5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0hlYWRlclYyLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9IZWFkZXJWMi5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vTGF5b3V0LmNzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9MYXlvdXQuY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vTGF5b3V0LmNzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcblxuLy8gZXh0ZXJuYWwtZ2xvYmFsIHN0eWxlcyBtdXN0IGJlIGltcG9ydGVkIGluIHlvdXIgSlMuXG5pbXBvcnQgcyBmcm9tICcuL0xheW91dC5jc3MnO1xuaW1wb3J0IEhlYWRlclYyIGZyb20gJy4uL0hlYWRlclYyL0hlYWRlclYyJztcbmltcG9ydCBGb290ZXIgZnJvbSAnLi4vRm9vdGVyJztcblxuY2xhc3MgTGF5b3V0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBmb290ZXJBc2g6IFByb3BUeXBlcy5ib29sLFxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPEhlYWRlclYyIC8+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgcm93ICR7cy5ib2R5fWB9Pnt0aGlzLnByb3BzLmNoaWxkcmVufTwvZGl2PlxuICAgICAgICA8Rm9vdGVyIGlzQXNoPXt0aGlzLnByb3BzLmZvb3RlckFzaH0gLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuTGF5b3V0LmRlZmF1bHRQcm9wcyA9IHtcbiAgZm9vdGVyQXNoOiBmYWxzZSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoTGF5b3V0KTtcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGhpc3RvcnkgZnJvbSAnLi4vLi4vaGlzdG9yeSc7XG5cbmZ1bmN0aW9uIGlzTGVmdENsaWNrRXZlbnQoZXZlbnQpIHtcbiAgcmV0dXJuIGV2ZW50LmJ1dHRvbiA9PT0gMDtcbn1cblxuZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn1cblxuY2xhc3MgTGluayBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgdG86IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcbiAgICBvbkNsaWNrOiBQcm9wVHlwZXMuZnVuYyxcbiAgfTtcblxuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIG9uQ2xpY2s6IG51bGwsXG4gIH07XG5cbiAgaGFuZGxlQ2xpY2sgPSBldmVudCA9PiB7XG4gICAgaWYgKHRoaXMucHJvcHMub25DbGljaykge1xuICAgICAgdGhpcy5wcm9wcy5vbkNsaWNrKGV2ZW50KTtcbiAgICB9XG5cbiAgICBpZiAoaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB8fCAhaXNMZWZ0Q2xpY2tFdmVudChldmVudCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoZXZlbnQuZGVmYXVsdFByZXZlbnRlZCA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgaGlzdG9yeS5wdXNoKHRoaXMucHJvcHMudG8pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IHRvLCBjaGlsZHJlbiwgLi4ucHJvcHMgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxhIGhyZWY9e3RvfSB7Li4ucHJvcHN9IG9uQ2xpY2s9e3RoaXMuaGFuZGxlQ2xpY2t9PlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L2E+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBMaW5rO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQVhBO0FBZ0JBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBTUE7Ozs7Ozs7Ozs7Ozs7QUFJQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFHQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBSkE7QUFxQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQWtCQTs7OztBQW5HQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBQ0E7QUFtR0E7Ozs7Ozs7QUN6S0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQW1EQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBekRBO0FBMERBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBakVBO0FBbUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBekVBO0FBMkVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQTlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFGQTtBQVFBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBTUE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBTUE7QUFDQTtBQUNBOzs7QUFpQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlGQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBS0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUtBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFvQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUtBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7Ozs7QUF0WUE7QUFDQTtBQXVZQTs7Ozs7OztBQzdZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7O0FBZEE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUZBO0FBQ0E7QUFlQTtBQUNBO0FBREE7QUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBbkNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFGQTtBQVFBO0FBREE7QUFDQTtBQThCQTs7OztBIiwic291cmNlUm9vdCI6IiJ9