(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./src/routes/products/get-ranks/GetRanksConstants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENTS", function() { return HOME_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENTS_UPDATED", function() { return HOME_CLIENTS_UPDATED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_BENEFITS", function() { return HOME_BENEFITS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_BENEFITS_DATA", function() { return HOME_BENEFITS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_AWARDS", function() { return HOME_AWARDS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_NEWS", function() { return HOME_NEWS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_MEDIA", function() { return HOME_MEDIA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_APPROACH", function() { return HOME_APPROACH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOME_CLIENT_SAY", function() { return HOME_CLIENT_SAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ASSESS_INDEPTH", function() { return ASSESS_INDEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_CONTENT_1", function() { return ANALYZE_CONTENT_1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_BEHAVIOUR_ANALYSIS", function() { return ANALYZE_BEHAVIOUR_ANALYSIS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANALYZE_CONTENT_2", function() { return ANALYZE_CONTENT_2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRACTICE_CONTENT", function() { return PRACTICE_CONTENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STUDENTS_RECORDS", function() { return STUDENTS_RECORDS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SLIDESHOW", function() { return SLIDESHOW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEACHFOR", function() { return TEACHFOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLATFORMS", function() { return PLATFORMS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMERS_STATS", function() { return CUSTOMERS_STATS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMER_REVIEW", function() { return CUSTOMER_REVIEW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CLIENTS_SET1", function() { return CLIENTS_SET1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CLIENTS_SET2", function() { return CLIENTS_SET2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LIVECLASSESDATA", function() { return LIVECLASSESDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OLD_LIVECLASSESDATA", function() { return OLD_LIVECLASSESDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MEDIA_COVERAGE", function() { return MEDIA_COVERAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRESS_VIDEO_COVERAGE", function() { return PRESS_VIDEO_COVERAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VIEWMORE", function() { return VIEWMORE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TESTS_KEY_POINTS", function() { return TESTS_KEY_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newModulesData", function() { return newModulesData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "moduleKeyPoints", function() { return moduleKeyPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cloneDeeply", function() { return cloneDeeply; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOW_WORKS", function() { return HOW_WORKS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOW_WORKS_TITLES", function() { return HOW_WORKS_TITLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEST_VIDEOS", function() { return TEST_VIDEOS; });
var HOME_CLIENTS = [{
  icon: '/images/request-demo/clients/gov_of_telangana.png'
}, {
  icon: '/images/request-demo/clients/sri_chaithanya.png'
}, {
  icon: '/images/request-demo/clients/rankguru.png'
}, {
  icon: '/images/request-demo/clients/ms.png'
}, {
  icon: '/images/request-demo/clients/motion.png'
}, {
  icon: '/images/request-demo/clients/tirumala.png'
}, {
  icon: '/images/request-demo/clients/trinity.png'
}, {
  icon: '/images/request-demo/clients/sri_vishwa.png'
}, {
  icon: '/images/request-demo/clients/sri_bhavishya.png'
}, {
  icon: '/images/request-demo/clients/sarath_academy.png'
}, {
  icon: '/images/request-demo/clients/pragathi.png'
}, {
  icon: '/images/request-demo/clients/sri_gayathri.png'
}];
var HOME_CLIENTS_UPDATED = [{
  id: 'gov_of_telangana',
  icon: '/images/home/clients/gov_of_telangana_black.png'
}, {
  id: 'sri_chaithanya',
  icon: '/images/home/clients/sri_chaithanya_black.png'
}, {
  id: 'rank_guru',
  icon: '/images/home/clients/rankguru_black.png'
}, {
  id: 'ms',
  icon: '/images/home/clients/ms_black.png'
}, {
  id: 'motion',
  icon: '/images/home/clients/Motion_black.png'
}, {
  id: 'tirumala',
  icon: '/images/home/clients/Tirumala_black.png'
}, {
  id: 'trinity',
  icon: '/images/home/clients/trinity_black.png'
}, {
  id: 'sri_vishwa',
  icon: '/images/home/clients/sri_vishwa_black.png'
}, {
  id: 'sri_bhavishya',
  icon: '/images/home/clients/sri_bhavishya_black.png'
}, {
  id: 'sarath_Academy',
  icon: '/images/home/clients/sarath_Academy_black.png'
}, {
  id: 'pragathi',
  icon: '/images/home/clients/pragathi_black.png'
}, {
  id: 'sri_gayathri',
  icon: '/images/home/clients/sri_gayathri_black.png'
}];
var HOME_BENEFITS = [{
  heading: 'Institutes',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Teachers',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Students',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Parents',
  icon: '/images/products/get-ranks/dummy.svg'
}];
var HOME_BENEFITS_DATA = [{
  heading: 'Smarter Automation',
  content: 'GetRanks gets you up and running in no time and with our hands-on customer success team and automated processes you can conduct tests and analyse student data in minutes',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Intelligent Insights',
  content: 'Our detailed dashboards help you understand students’ performance and help them remediate in a personalised manner suited to their needs',
  icon: '/images/products/get-ranks/dummy.svg'
}, {
  heading: 'Online Testing Platform',
  content: 'GetRanks helps you simulate the actual test conditions and platform which helps your students prepare well for the actual exam',
  icon: '/images/products/get-ranks/dummy.svg'
}];
var HOME_AWARDS = [{
  icon: '/images/home/achievements/wec1.png',
  content: '50 Fabulous Edutech Leaders',
  icon1: '/images/home/achievements/wec.png'
}, {
  icon: '/images/home/achievements/iit-varanasi.png',
  content: 'Best Analytics App of 2017',
  icon1: '/images/home/achievements/bla.png'
}, {
  icon: '/images/home/achievements/brainfeed.png',
  content: 'Best Learning Analytics Company of 2018',
  icon1: '/images/home/achievements/bf.png'
}, {
  icon: '/images/home/achievements/higherEducation.png',
  content: 'Best Adaptive Technology Solution',
  icon1: '/images/home/achievements/hep.png'
}, {
  icon: '/images/home/achievements/is.png',
  content: 'Outstanding EdTech Startups',
  icon1: '/images/home/achievements/is.png'
}, {
  icon: '/images/home/achievements/cio.svg',
  content: '20 Most promising Ed-Tech startups of 2018',
  icon1: '/images/home/achievements/cio.png'
}];
var HOME_NEWS = [{
  icon: '/images/home/media/on_news/etv_telangana.webp',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M'
}, {
  icon: '/images/home/media/on_news/kiran_sir_on_T news.webp',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}, {
  icon: '/images/home/media/on_news/students_on_v6.webp',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}];
var HOME_MEDIA = [{
  icon: '/images/home/media/financial_Express.webp',
  name: 'financial_Express',
  url: 'https://www.financialexpress.com/industry/surprise-predictive-analysis-of-students-future-course-of-action-for-career-development-now-possible-egnify-rolls-out-solution/881282/'
}, {
  icon: '/images/home/media/yourstory.webp',
  name: 'yourstory',
  url: 'https://yourstory.com/2017/12/hyderabad-startup-students-exam/'
}, {
  icon: '/images/home/media/indian-express.webp',
  name: 'indian-express',
  url: 'http://www.newindianexpress.com/cities/hyderabad/2017/sep/19/impetus-to-excel-1659699.html'
}, {
  icon: '/images/home/media/asia500.png',
  name: 'indian-asia500',
  url: 'http://www.asiainc500.com/wp-content/uploads/2018/10/Asia-Inc.-500-Magazine-Oct-2018-D91.pdf'
}, {
  icon: '/images/home/media/deccan.webp',
  name: 'indian-deccan',
  url: 'https://www.deccanchronicle.com/nation/current-affairs/120118/pilot-project-to-mould-state-school-kids-in-telangana.html'
}, {
  icon: '/images/home/media/etv.webp',
  name: 'etv',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M'
}, {
  icon: '/images/home/media/news_minute.webp',
  name: 'news_minute',
  url: 'https://www.thenewsminute.com/article/telangana-ties-t-hub-startup-egnify-improve-education-govt-schools-74652'
}, {
  icon: '/images/home/media/telangana-today.webp',
  name: 'telangana-today',
  url: 'https://telanganatoday.com/hyderabad-based-startup-egnify-technologies-helps-students-excel'
}, {
  icon: '/images/home/media/andhrajyothi.webp',
  name: 'andhrajyothi',
  url: 'http://www.andhrajyothy.com/artical?SID=461827'
}, {
  icon: '/images/home/media/v6n1.webp',
  name: 'V6 news',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}, {
  icon: '/images/home/media/namasthe-telangana.webp',
  name: 'namasthe-telangana',
  url: 'http://epaper.ntnews.com/c/22959899'
}, {
  icon: '/images/home/media/t-news.webp',
  name: 't-news',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}];
var HOME_APPROACH = [{
  heading: 'For Institutes',
  content: 'Our platform helps institutes to conduct online and offline tests for their students across multiple test patterns. Our detailed data dashboards helps you understand your students performance and helps you guide them to success'
}, {
  heading: 'For Students',
  content: 'GetRanks empowers students with in-depth analysis of their test performance across Competence and Behavioural aspects to help them improve their performance and pave the way to their dream college'
}];
var HOME_CLIENT_SAY = [{
  content: "Our institute chose this app for sending us online lectures and for conducting exams and I gotta say,\n    it really works well. The classes have high quality visual and you can change the playback speed as well.\n     You can't take screenshots during the exam and after the exam we get individual marks for\n     each subject and the total as well. We can even compare our previous scores. We can even analyse the toppers\n     score along with ours. Overall, it is a wonderful app.",
  name: 'Stacy Cordelia',
  designation: 'Student',
  institute: '/images/home/clients/sri-chaitanya.png',
  defaultimage: '/images/icons/person_blue.svg'
}, {
  content: "This app is so good and there is no problem in quality of classes posted in the app. The way of conducting exam\n     in the app is so good that every student will be comfortable to write. This app is also good for studies. Online exams\n      are also so good.Thank for supporting our institution in keeping exams and online classes So everybody download getranks\n      and request your Institute to conduct exams and post classes in getranks",
  name: "lechireddy Surekha",
  designation: "Student",
  institute: '/images/home/clients/pinegrove.jpeg',
  defaultimage: '/images/icons/person_blue.svg'
}, {
  content: "This is way too useful for JEE aspirants and the User Interface is too simple to use and this app is adhered\n     to privacy of each and every user. Apart from the institution's main exams they also provide us with the practice\n      tests which are efficient too. The idea of including zoom meetings in the app was too incredible\uD83D\uDC4D.",
  name: 'Hemubunny Kasireddy',
  designation: "Student",
  institute: '/images/home/clients/Meluha.png',
  defaultimage: '/images/icons/person_blue.svg'
},
/* {
  content: `An Excellent app for academics .This app helps us to compare our performance with other people .It also
   helps us to compare our performance with the previous one. It helps us in understanding our strengths & weakness .
   There is graphical analysis to make comparison even more easier . The app also shows the time we have spent on each
    question & time spent for doing right & wrong answers .The app also enables us to watch video lectures uploaded
     by institute . Overall I think the app is awesome .`,
  name: `Sri Ranga`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/icons/person_blue.svg',
}, */
{
  content: "This app has excellent top notch features which helps students get up to the mark even in the pandemic.\n     There are no errors and it works perfectly fine. Even when all the academics shifted to a whole online process,\n      this app keeps its mark up. Good one and i really suggest",
  name: "Srilakshmi kandula",
  designation: "Parent",
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/icons/person_blue.svg'
}
/* {
  content: `This app is very helpful to me to study. Now, it is very easy to me to listen my classes or to practice
   any subject.It is really a gud thing ,it has no ads.And very interesting tests.I used tbisapp
    very well I wish thip app will helpful to many of the students who are interestedto study.
     I congratulate the app for the work they made..... Thank you !.....👍`,
  name: `Vanaparthi Madhavi`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `The app is very convenient and helpful. The way it shows the Analysis of the exams and the review format
   of the exams are excellent.. Thanks for the app.`,
  name: ``,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `Best and excellent app. It clearly shows me percentage and the percentile of my performance in both the mains
  and advance exam. And the best thing of this app is, it had practice papers even.Thankyou for creating such an
  excellent app.`,
  name: `Satyavathi Nuni`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy_profile.jpg',
},
{
  content: `Getranks is a good app.where we can see and listen to lectures posted by our teachers and we can write the tests.
  once we completed our test ,we get our result immediately without any delay..we can also get the solutions for
  those questions in the test..`,
  name: `Seethala Srinidhi`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `This is one of the best app for eductional purpose, they were giving new features for every update as like
   conducting zoom meetings in app it self . thanks for all who were working under this app by giving your best for us.`,
  name: `HASITH VARDHAN`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `The best app for any student .This app is one of the most useful app .It really shows me whether I had
  improved or not in the examination by giving my accuracy and attempt for each subject . The option compare with the
  topper really shows me still how much I have to improve . Thanks for the creators who created such an wonderful app.`,
  name: `Surya Bhaskara Rao Chekuri`,
  designation: `Student`,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
},
{
  content: `Nice app for jee mains preparation in these circumstances. There are invidual practice tests of subject and
  chapter practice . Which can help to strengthen our weak areas with jee main type mock test. Thank you team egnify`,
  name: `Rishitha s99`,
  designation: ``,
  institute: '/images/home/clients/MOTION.png',
  defaultimage: '/images/products/get-ranks/dummy.svg',
}, */
];
var ASSESS_INDEPTH = [{
  heading: 'Marks Analysis',
  content: 'Our Marks Analysis helps you identify the at-risk and just-missed students and enables you to set customised goals and nudge them forward.',
  icon: '/images/products/get-ranks/assess-screenshot4.svg'
}, {
  heading: 'Error Analysis',
  content: 'Negative marking is the bane that pulls your students performance down. Pinpoint the areas that needs to be addressed and boost the performance of students by minimising errors',
  icon: '/images/products/get-ranks/assess-screenshot5.svg'
}, {
  heading: 'Concept Analysis',
  content: 'Students performance in Subjects is just not enough. GetRanks goes further and gives you a detailed picture of the health of you students across topics and subtopics covered in the test.',
  icon: '/images/products/get-ranks/assess-screenshot6.svg'
}];
var ANALYZE_CONTENT_1 = [{
  headerContent: 'Dedicated Student Profile',
  mainContent: 'The one stop that gives you the detailed student performance analysis with easy to understand dashboards and performance trends across your Marks, Topics and Behavioural aspects',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot1.svg'
}, {
  headerContent: 'Question Review',
  mainContent: 'Review your test performance question-by-question and get an understanding of how your peers performed in each of them - across competence and behavioural aspects',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot2.svg'
}, {
  headerContent: 'Mark Analysis',
  mainContent: 'Know your marks across each subject and difficulty levels. The attempt and accuracy rates help you understand your weak attempts improve the same going forward',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/analyze-screenshot3.svg'
}, {
  headerContent: 'Concept Analysis',
  mainContent: 'Understand the weightage of each topic and your performance in it across all the tests attempted during the year',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot4.svg'
}];
var ANALYZE_BEHAVIOUR_ANALYSIS = [{
  heading: 'Time Analysis',
  content: 'Time management is the most important aspect of test taking. GetRanks tracks your time during the test and provides an easily understandable time map to help you improve your time management skills'
}, {
  heading: 'Question Analysis',
  content: 'Every question is different and we treat it as such; we analyse how you performed in each question across various dimensions and nudge you to improve'
}, {
  heading: 'Difficulty Analysis',
  content: 'The difficulty of the paper has a direct impact on students performance and we help you understand your difficulty profile so that you can improve your accuracy and reduce negative marking'
}];
var ANALYZE_CONTENT_2 = [{
  headerContent: 'Compare ',
  mainContent: 'Compare your performance with the toppers and figure out their way of climbing to the top',
  button: 'COMPARE NOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot6.svg'
}, {
  headerContent: 'Rank Predictor',
  mainContent: 'Find out where you stand at the national level. Set goals and chart the path to your dream college!',
  button: 'KNOW MORE',
  screenImage: '/images/products/get-ranks/analyze-screenshot7.svg'
}];
var PRACTICE_CONTENT = [{
  headerContent: 'Ask Disha!',
  mainContent: 'Having trouble deciding what to practice? Don’t worry, just ask Disha and she’ll take you through the optimal learning path to achieve your goals.',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/practice-screenshot1.svg'
}, {
  headerContent: 'Custom Practice',
  mainContent: 'Practice at your own pace. Choose your own chapters, difficulty and time, feel comfortable in the test taking experience and targetedly improve your scores.',
  button: 'TAKE A TEST',
  screenImage: '/images/products/get-ranks/practice-screenshot2.svg'
}, {
  headerContent: 'All India Test Series',
  mainContent: 'Compare your performance with all your competitors across the country. Evaluate where you stand and target your approach accordingly.',
  button: 'CLICK TO KNOW',
  screenImage: '/images/products/get-ranks/analyze-screenshot3.svg'
}];
var STUDENTS_RECORDS = [{
  rank: 'AIR 354',
  imgUrl: '/images/home/students/navneet.png',
  name: 'V NAVANEETH',
  app_no: 'APP.NO.190310534282'
}, {
  rank: 'AIR 359',
  imgUrl: '/images/home/students/nityanand.png',
  name: 'P.S.S. NITHYANAND',
  app_no: 'APP.NO.190310298638'
}, {
  rank: 'AIR 441',
  imgUrl: '/images/home/students/rohitraj.png',
  name: 'R.ROHITRAJ',
  app_no: 'APP.NO.190310522538'
}, {
  rank: 'AIR 505',
  imgUrl: '/images/home/students/chandu.png',
  name: 'CH.CHANDU',
  app_no: 'APP.NO.190310150759'
}, {
  rank: 'AIR 618',
  imgUrl: '/images/home/students/saipavan.png',
  name: 'R SAI PAVAN',
  app_no: 'APP.NO.190310218940'
}, {
  rank: 'AIR 723',
  imgUrl: '/images/home/students/tejaswi.png',
  name: 'P.TEJASWI',
  app_no: 'APP.NO.190310117457'
}, {
  rank: 'AIR 972',
  imgUrl: '/images/home/students/premchand.png',
  name: 'G PREM CHAND',
  app_no: 'APP.NO.190310218630'
}, {
  rank: 'AIR 1039',
  imgUrl: '/images/home/students/saikrishna.png',
  name: 'A SAI KRISHNA',
  app_no: 'APP.NO.190310476368'
}, {
  rank: 'AIR 1118',
  imgUrl: '/images/home/students/abhilash.png',
  name: 'K.ABHILASH',
  app_no: 'APP.NO.190310218379'
}, {
  rank: 'AIR 1150',
  imgUrl: '/images/home/students/varsha.png',
  name: 'A.VARSHA',
  app_no: 'APP.NO.190310801602'
}];
var SLIDESHOW = [{
  title: 'JEE MAIN',
  doubleDigitRanks: 70,
  tripleDigitRanks: '401',
  bestRanks: '1, 11, 13, 14, 24'
}, {
  title: 'JEE ADV.',
  doubleDigitRanks: 30,
  tripleDigitRanks: '130',
  bestRanks: '1, 7, 17, 24, 28'
}];
var TEACHFOR = [{
  title: 'Academics',
  courses: ['CLASS 6', ' CLASS 7', ' CLASS 8', ' CLASS 9', ' CLASS 10', ' CLASS 11', ' CLASS 12']
}, {
  title: 'Competitive Exams',
  courses: ['IIT JEE', ' NEET', ' EAMCET', 'CBSE', 'BITSAT', 'COMED-K', 'VITEEE', 'AIIMS', 'JIPMER']
}, {
  title: 'Extracurricular',
  courses: ['MUSIC', 'PAINTING', 'DANCING', 'YOGA', 'MEDITATION', 'ABACUS', 'PHOTOGRAPHY']
}];
var PLATFORMS = [{
  label: 'Mobile',
  available: 'Android & iOS',
  icon: '/images/home/platforms/mobile.svg'
}, {
  label: 'Desktop',
  available: 'Chrome',
  icon: '/images/home/platforms/desktop.svg'
}, {
  label: 'Tablet',
  available: 'Android & iOS',
  icon: '/images/home/platforms/tablet.svg'
}];
var CUSTOMERS_STATS = [{
  icon: '/images/customers/customers_new.svg',
  number: '1 Million +',
  text: 'Satisfied Students',
  label: 'Customers Logo'
}, {
  icon: '/images/customers/institution_new.svg',
  number: '100+',
  text: 'Satisfied Institutions',
  label: 'Institutions Logo'
}];
var CUSTOMER_REVIEW = [{
  logo: 'images/customers/sriChaitanya_logo.jpg',
  pic: 'images/customers/srujan.jpg',
  label: 'Sri Chaitanya',
  text: "\" Our institute chose this app for sending us online lectures and for\n    conducting exams and I gotta say, it really works well. The classes\n    have high quality visual and you can change the playback speed as well.\n    You can't take screenshots during the exam and after the exam we get individual marks\n    for each subject and the total as well. We can even compare our previous scores.\n     We can even analyse the toppers score along with ours. Overall, it is a wonderful app.\"",
  name: 'Stacy Cordelia',
  designation: 'Student'
}, {
  logo: 'images/customers/rankguru_logo.jpg',
  pic: 'images/customers/laxman.jpg',
  label: 'Rankguru',
  text: "\"This app is so good and there is no problem in quality of classes posted in the app. The way of conducting exam\n     in the app is so good that every student will be comfortable to write. This app is also good for studies. Online exams\n      are also so good.Thank for supporting our institution in keeping exams and online classes So everybody download\n       getranks and request your Institute to conduct exams and post classes in getranks\"",
  name: 'lechireddy Surekha',
  designation: 'Student'
}];
var CLIENTS_SET1 = [{
  icon: '/images/customers/ts.png',
  label: 'TS Govt.'
}, {
  icon: '/images/customers/sri_chaithanya.png',
  label: 'Sri Chaithanya'
}, {
  icon: '/images/customers/Rankguru.png',
  label: 'RankGuru'
}, {
  icon: '/images/customers/motion.png',
  label: 'Motion'
}, {
  icon: '/images/customers/ms.png',
  label: 'MS'
}, {
  icon: '/images/customers/tirumala.png',
  label: 'Tirumala'
}, {
  icon: '/images/customers/trinity.png',
  label: 'Trinity'
}, {
  icon: '/images/customers/viswa.png',
  label: 'Sri Viswa'
}, {
  icon: '/images/customers/bhavishya.png',
  label: 'Bhavishya'
}, {
  icon: '/images/customers/pragathi_jr.png',
  label: 'Prgathi Jr.'
}, {
  icon: '/images/customers/sarath.png',
  label: 'Sarath Academy'
}, {
  icon: '/images/customers/pragathi.png',
  label: 'Pragathi'
}, {
  icon: '/images/customers/saraswathi_groups.png',
  label: 'Saraswathi_Groups'
}, {
  icon: '/images/customers/shivani.png',
  label: 'Shivani'
}, {
  icon: '/images/customers/prathibha_jr.png',
  label: 'Prathibha_Jr'
}, {
  icon: '/images/customers/Resonance.png',
  label: 'Resonance'
}, {
  icon: '/images/customers/vijetha.png',
  label: 'Vijetha'
}, {
  icon: '/images/customers/toppers.png',
  label: 'Toppers'
}];
var CLIENTS_SET2 = [{
  icon: 'images/customers/abv.png',
  label: 'ABV'
}, {
  icon: 'images/customers/master_minds.png',
  label: 'Master Minds'
}, {
  icon: 'images/customers/sri_gayathri.png',
  label: 'Sri Gayathri'
}, {
  icon: 'images/customers/pinegrove.png',
  label: 'Pinegrove'
}, {
  icon: 'images/customers/sri_akshara.png',
  label: 'Sri Akshara'
}, {
  icon: 'images/customers/urbane.png',
  label: 'Urbane'
}, {
  icon: 'images/customers/sri_prakash.png',
  label: 'Sri Prakash'
}, {
  icon: 'images/customers/vishra.png',
  label: 'Vishra'
}, {
  icon: 'images/customers/navodaya.png',
  label: 'Navodaya'
}, {
  icon: 'images/customers/newera.png',
  label: 'New Era'
}, {
  icon: 'images/customers/akshaya.png',
  label: 'Akshaya'
}, {
  icon: 'images/customers/gyanam.png',
  label: 'Gyanam'
}, {
  icon: 'images/customers/elegance.png',
  label: 'Elegance'
}, {
  icon: 'images/customers/kakathiya.png',
  label: 'Kakathiya'
}, {
  icon: 'images/customers/ramabanam.png',
  label: 'RamaBanam'
}, {
  icon: 'images/customers/geetham.png',
  label: 'Geetham'
}, {
  icon: 'images/customers/sri_vaishnavi.png',
  label: 'Sri_Vaishnavi'
}, {
  icon: '/images/customers/dhiksha.png',
  label: 'Dhiksha'
}];
var LIVECLASSESDATA = [{
  route: 'Teach',
  features: [{
    icon: 'images/home/New SubMenu Items/Teach/teach.svg',
    title: 'Teach',
    isHeading: true,
    route: 'Teach'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Live.svg',
    title: 'Live',
    isHeading: false,
    route: 'Live'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Assignments.svg',
    title: 'Assignments',
    isHeading: false,
    route: 'Assignments'
  }, {
    icon: 'images/home/New SubMenu Items/Teach/Doubts.svg',
    title: 'Doubts',
    isHeading: false,
    route: 'Doubts'
  }]
}, {
  route: 'Test',
  features: [{
    icon: 'images/home/New SubMenu Items/Test/test.svg',
    title: 'Test',
    isHeading: true,
    route: 'Test'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Tests.svg',
    title: 'Tests',
    isHeading: false,
    route: 'Tests'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Analysis.svg',
    title: 'Analysis',
    isHeading: false,
    route: 'Analysis'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Practice.svg',
    title: 'Practice',
    isHeading: false,
    route: 'Practice'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Papers.svg',
    title: 'Papers',
    isHeading: false,
    route: 'Papers'
  }, {
    icon: 'images/home/New SubMenu Items/Test/Reports.svg',
    title: 'Reports',
    isHeading: false,
    route: 'Reports'
  }]
}, {
  route: 'Connect',
  features: [{
    icon: 'images/home/New SubMenu Items/Connect/connect.svg',
    title: 'Connect',
    isHeading: true,
    route: 'Connect'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/schedule.svg',
    title: 'Schedule',
    isHeading: false,
    route: 'Schedule'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/messenger.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }, {
    icon: 'images/home/New SubMenu Items/Connect/notifications.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }]
}, {
  route: 'Collect',
  features: [{
    icon: 'images/home/New SubMenu Items/Collect/collect.svg',
    title: 'Collect',
    isHeading: true,
    route: 'Collect'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/fee_collection.svg',
    title: 'Fee Collection',
    isHeading: false,
    route: 'FeeCollect'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/E-Receipts.svg',
    title: 'E-Receipts',
    isHeading: false,
    route: 'Receipts'
  }, {
    icon: 'images/home/New SubMenu Items/Collect/auto_payment_remainders.svg',
    title: 'Auto Payment Remainders',
    isHeading: false,
    route: 'AutoPayments'
  }]
}, {
  route: 'Manage',
  features: [{
    icon: 'images/home/New SubMenu Items/Manage/manage.svg',
    title: 'Manage',
    isHeading: true,
    route: 'Manage'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/admissions.svg',
    title: 'Admissions',
    isHeading: false,
    route: 'Admissions'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/attendences.svg',
    title: 'Attendance',
    isHeading: false,
    route: 'Attendance'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/students.svg',
    title: 'Students',
    isHeading: false,
    route: 'Students'
  }, {
    icon: 'images/home/New SubMenu Items/Manage/roles.svg',
    title: 'Roles',
    isHeading: false,
    route: 'Roles'
  }]
}];
var OLD_LIVECLASSESDATA = [{
  route: 'Teach',
  features: [{
    icon: 'images/home/submenuitems/Teach/Teach.svg',
    title: 'Teach',
    isHeading: true,
    route: 'Teach'
  }, {
    icon: 'images/home/submenuitems/Teach/Live.svg',
    title: 'Live',
    isHeading: false,
    route: 'Live'
  }, {
    icon: 'images/home/submenuitems/Teach/Assignments.svg',
    title: 'Assignments',
    isHeading: false,
    route: 'Assignments'
  }, {
    icon: 'images/home/submenuitems/Teach/Doubts.svg',
    title: 'Doubts',
    isHeading: false,
    route: 'Doubts'
  }]
}, {
  route: 'Test',
  features: [{
    icon: 'images/home/submenuitems/Test/Test.svg',
    title: 'Test',
    isHeading: true,
    route: 'Test'
  }, {
    icon: 'images/home/submenuitems/Test/Tests.svg',
    title: 'Tests',
    isHeading: false,
    route: 'Tests'
  }, {
    icon: 'images/home/submenuitems/Test/Analysis.svg',
    title: 'Analysis',
    isHeading: false,
    route: 'Analysis'
  }, {
    icon: 'images/home/submenuitems/Test/Practice.svg',
    title: 'Practice',
    isHeading: false,
    route: 'Practice'
  }, {
    icon: 'images/home/submenuitems/Test/Papers.svg',
    title: 'Papers',
    isHeading: false,
    route: 'Papers'
  }, {
    icon: 'images/home/submenuitems/Test/Reports.svg',
    title: 'Reports',
    isHeading: false,
    route: 'Reports'
  }]
}, {
  route: 'Connect',
  features: [{
    icon: 'images/home/submenuitems/Connect/Connect.svg',
    title: 'Connect',
    isHeading: true,
    route: 'Connect'
  }, {
    icon: 'images/home/submenuitems/Connect/Schedule.svg',
    title: 'Schedule',
    isHeading: false,
    route: 'Schedule'
  }, {
    icon: 'images/home/submenuitems/Connect/Messenger.svg',
    title: 'Messenger',
    isHeading: false,
    route: 'Messenger'
  }, {
    icon: 'images/home/submenuitems/Connect/Notifications.svg',
    title: 'Notifications',
    isHeading: false,
    route: 'Messenger'
  }]
}, {
  route: 'Collect',
  features: [{
    icon: 'images/home/submenuitems/Collect/Collect.svg',
    title: 'Collect',
    isHeading: true,
    route: 'Collect'
  }, {
    icon: 'images/home/submenuitems/Collect/FeeCollection.svg',
    title: 'Fee Collection',
    isHeading: false,
    route: 'FeeCollect'
  }, {
    icon: 'images/home/submenuitems/Collect/EReceipts.svg',
    title: 'E-Receipts',
    isHeading: false,
    route: 'EReceipts'
  }, {
    icon: 'images/home/submenuitems/Collect/AutoPaymentRemainders.svg',
    title: 'Auto Payment Remainders',
    isHeading: false,
    route: 'AutoPayments'
  }]
}, {
  route: 'Manage',
  features: [{
    icon: 'images/home/submenuitems/Manage/Manage.svg',
    title: 'Manage',
    isHeading: true,
    route: 'Manage'
  }, {
    icon: 'images/home/submenuitems/Manage/Admissions.svg',
    title: 'Admissions',
    isHeading: false,
    route: 'Admissions'
  }, {
    icon: 'images/home/submenuitems/Manage/Attendance.svg',
    title: 'Attendance',
    isHeading: false,
    route: 'Attendance'
  }, {
    icon: 'images/home/submenuitems/Manage/Students.svg',
    title: 'Students',
    isHeading: false,
    route: 'Students'
  }, {
    icon: 'images/home/submenuitems/Manage/Roles.svg',
    title: 'Roles',
    isHeading: false,
    route: 'Roles'
  }]
}];
var MEDIA_COVERAGE = [{
  icon: '/images/press/financial_express-1.png',
  title: 'Surprise! Predictive analysis of students future course of action for career development now possible; Egnify rolls out solution',
  author: 'By: BV Mahalakshmi',
  content: 'For students, it is always learn, relearn and unlearn to hit the top score. But neither the educators nor parents or the students can do a predictive analysis about their future course of action for career development and the dilemma continues for generations.',
  url: 'https://www.financialexpress.com/industry/surprise-predictive-analysis-of-students-future-course-of-action-for-career-development-now-possible-egnify-rolls-out-solution/881282/'
}, {
  icon: '/images/press/yourstory-1.png',
  title: 'This Hyderabad startup is helping students perform better in exams',
  author: 'By Think Change India',
  content: 'Started as an edutech startup and based out of T-Hub, Hyderabad, Egnify employs deep learning, Machine Learning and data science to make a keen analysis of the performance of students. Based on this report, the startup gets to describe where the students need to improve upon, along with a detailed breakdown of their marks.',
  url: 'https://yourstory.com/2017/12/hyderabad-startup-students-exam/'
}, {
  icon: '/images/press/indian_express-1.png',
  title: 'Impetus to excel',
  author: 'By Shyam Yadagiri, Express News Service',
  content: 'Founded in 2015 by Kiran Babu, Egnify is a cloud-based plug-and-play analytics and assessments platform for educational institutions.',
  url: 'http://www.newindianexpress.com/cities/hyderabad/2017/sep/19/impetus-to-excel-1659699.html'
}, {
  icon: '/images/press/asia-1.png',
  title: 'Egnify Improving Education using the deep-tech analytics: Performance',
  author: 'ASIA INC.500',
  content: 'Founded in 2015 by Kiran Babu, Egnify is a cloud-based Assessment and Learning Analytics platform integrated with world-class Analytics to enhance conceptual clarity and Exam Readiness of the student. It is a team of 20 people (IITs, IIITs, IIMs, NITs, Stanford) serving 4,27,000 students, 28,000+ teachers in 400+ institutes across 28 cities in 4 states. Our target is to reach 10 lakh students by end of the academic year 2018.',
  url: 'http://www.asiainc500.com/wp-content/uploads/2018/10/Asia-Inc.-500-Magazine-Oct-2018-D91.pdf'
}, {
  icon: '/images/press/deccan-1.png',
  title: 'Pilot project to mould state school kids in Telangana',
  author: 'DECCAN CHRONICLE. | NAVEENA GHANATE',
  content: 'Hyderabad: Emulating the model followed by private institutions, the Telangana state government is running a pilot project to strengthen the weak areas of students in government schools through technology. This would eventually raise the pass percentage.',
  url: 'https://www.deccanchronicle.com/nation/current-affairs/120118/pilot-project-to-mould-state-school-kids-in-telangana.html'
}, {
  icon: '/images/press/the_news_minute-1.png',
  title: 'Telangana ties up with T-Hub startup Egnify to improve education in govt schools',
  author: 'Shilpa S Ranipeta',
  content: 'The Telangana government is looking to increase quality of education in government schools across the state, and wants to use technology for this. In the latest development, it wants to measure the strengths and weaknesses of students in government schools across subjects, assess it and look for ways to improve the same using technology.',
  url: 'https://www.thenewsminute.com/article/telangana-ties-t-hub-startup-egnify-improve-education-govt-schools-74652'
}, {
  icon: '/images/press/telangana_today-1.png',
  title: 'Hyderabad-based startup Egnify Technologies helps students excel',
  author: 'By Sruti Venugopal',
  content: 'Working out of T-Hub, the startup is a cloud-based holistic student performance improvement platform that through data and artificial intelligence measures the weaknesses and strengths of a student and provides specific solution.',
  url: 'https://telanganatoday.com/hyderabad-based-startup-egnify-technologies-helps-students-excel'
}, {
  icon: '/images/press/andhrajyothi-1.png',
  title: 'మార్కుల విశ్లేషణకూ ఓ టెక్నిక్‌',
  content: 'పరీక్షలో ఒక విద్యార్థికి 52 మార్కులు వచ్చాయి. సరే, మరి తతిమా 48 మార్కులు ఎందుకు రాలేదు అన్నది వెంటనే కలగాల్సిన సందేహం. ఎక్కడ వీక్‌గా ఉన్నారో తెలిస్తే సవరించుకోవచ్చు. తద్వారా మార్కులను పెంచుకోవచ్చు. దీన్ని టెక్నాలజీ సహకారంతో విద్యార్థి ప్రతిభను విశ్లేషించే పనిని చేపట్టిన సంస్థ ఎగ్నిఫై.',
  url: 'http://www.andhrajyothy.com/artical?SID=461827'
}];
var PRESS_VIDEO_COVERAGE = [{
  icon: '/images/home/media/on_news/etv_telangana.webp',
  icon1: '/images/home/media/on_news/etv_telangana-1.png',
  url: 'https://www.youtube.com/watch?v=wuZceYRr3_M',
  title: 'Govt Ties Up with Getranks by Egnify Startup to Analyze School Students Lagging Behind in Education'
}, {
  icon: '/images/home/media/on_news/kiran_sir_on_T 20news.webp',
  icon1: '/images/home/media/on_news/kiran_sir_on_T news-1.png',
  title: 'Getranks by Egnify_CEO Kiran Babu_Hyderabad_TNews_T Hub',
  url: 'https://www.youtube.com/watch?v=ObszP9ujUx4'
}, {
  icon: '/images/home/media/on_news/students_on_v6.webp',
  icon1: '/images/home/media/on_news/students_on_v6-1.png',
  title: 'Special Story on Getranks by Egnify in Hyderabad V6 News',
  url: 'https://www.youtube.com/watch?v=hmX1jyzat9Q'
}];
var VIEWMORE = [{
  color: '#ff6400',
  icon: '/images/home/New SubMenu Items/arrows/teach-arrow.svg'
}, {
  color: '#8800fe',
  icon: '/images/home/New SubMenu Items/arrows/test-arrow.svg'
}, {
  color: '#00b800',
  icon: '/images/home/New SubMenu Items/arrows/connect-arrow.svg'
}, {
  color: '#be00cc',
  icon: '/images/home/New SubMenu Items/arrows/collect-arrow.svg'
}, {
  color: '#e5ac00',
  icon: '/images/home/New SubMenu Items/arrows/manage-arrow.svg'
}];
var TESTS_KEY_POINTS = [{
  id: 1,
  point: 'Unlimitted test creation'
}, {
  id: 2,
  point: 'Instant test result and Reports available online & offline - Rank list, Analysis, and Reports after every test.'
}, {
  id: 3,
  point: 'Pre-defined marking schemes'
}, {
  id: 4,
  point: 'Custom marking schemes'
}, {
  id: 5,
  point: 'Getranks supports all 22 types of JEE avanced Marking Schemes starting from 2009 - 2019'
}, {
  id: 6,
  point: 'Grace period option available'
}, {
  id: 7,
  point: 'Live Attendance'
}];
var newModulesData = [{
  name: 'Live',
  icon: 'images/home/New SubMenu Items/Teach/old_Live.svg'
}, {
  name: 'Assignments',
  icon: 'images/home/New SubMenu Items/Teach/old_Assignments.svg'
}, {
  name: 'Doubts',
  icon: 'images/home/New SubMenu Items/Teach/old_Doubts.svg'
}, {
  name: 'Tests',
  icon: 'images/home/New SubMenu Items/Test/module_test.svg'
}, {
  name: 'Connect',
  icon: 'images/home/New SubMenu Items/Connect/new_connect.svg'
}];
var moduleKeyPoints = {
  tests: {
    points: ['Unlimited test creation', '100,000 student concurrency', 'Conduct online and offline tests', 'Advanced Proctoring features', 'Pre-defined marking schemas and custom marking schemas', 'Grace period option avaialbe', 'Live attendance dashboard']
  },
  assignments: {
    points: ['Administer and grade online and in-class assessments', 'Grade multiple assessment types, including handwritten assessments', 'Evaluate using mobile app and in a web browser', 'Improve student feedback loop and provide an opportunity to enrich the learning experience through analytics']
  },
  doubts: {
    points: ['Doubts can turn up any time', 'Ask until it is resolved', 'Answer doubt anytime, anywhere', 'Access and Analysis doubts']
  },
  schedule: {
    points: ['Save time and make the most of every day', "Bring your student, teacher's timetable to life and make it easy to access what's ahead with links, attachments all other details", 'All your activities in one place']
  }
};
var cloneDeeply = function cloneDeeply(obj) {
  var newObj = JSON.parse(JSON.stringify(obj));
  return newObj;
};
var HOW_WORKS = [{
  title: 'Onboarding',
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.",
  img: "/images/Test/demo.png"
}, {
  title: 'Institution Setup',
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.",
  img: "/images/Test/demo.png"
}, {
  title: 'Add Hierarchies',
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.",
  img: "/images/Test/demo.png"
}, {
  title: 'Add Students',
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit.",
  img: "/images/Test/demo.png"
}];
var HOW_WORKS_TITLES = ['Onboarding', 'Institution Setup', 'Add Hierarchies', 'Add Students'];
var TEST_VIDEOS = [{
  url: '/images/Test/videos/Conduct offline-online Tests.mp4',
  title: 'Conduct Offline/ Online Test'
}, {
  url: '/images/Test/videos/Create Own Question Papers.mp4',
  title: 'Upload Own Question Papers'
}, {
  url: '/images/Test/videos/Upload own question paper.mp4',
  title: 'Create Own Question Papers'
}, {
  url: '/images/Test/videos/Indepth Analysis.mp4',
  title: 'Indepth Analysis'
}];

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMS5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL0dldFJhbmtzQ29uc3RhbnRzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBIT01FX0NMSUVOVFMgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9nb3Zfb2ZfdGVsYW5nYW5hLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9zcmlfY2hhaXRoYW55YS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvcmFua2d1cnUucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3JlcXVlc3QtZGVtby9jbGllbnRzL21zLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9tb3Rpb24ucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3JlcXVlc3QtZGVtby9jbGllbnRzL3RpcnVtYWxhLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy90cmluaXR5LnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9zcmlfdmlzaHdhLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9zcmlfYmhhdmlzaHlhLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9yZXF1ZXN0LWRlbW8vY2xpZW50cy9zYXJhdGhfYWNhZGVteS5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcmVxdWVzdC1kZW1vL2NsaWVudHMvcHJhZ2F0aGkucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3JlcXVlc3QtZGVtby9jbGllbnRzL3NyaV9nYXlhdGhyaS5wbmcnLFxuICB9LFxuXTtcbmV4cG9ydCBjb25zdCBIT01FX0NMSUVOVFNfVVBEQVRFRCA9IFtcbiAge1xuICAgIGlkOiAnZ292X29mX3RlbGFuZ2FuYScsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9jbGllbnRzL2dvdl9vZl90ZWxhbmdhbmFfYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAnc3JpX2NoYWl0aGFueWEnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9zcmlfY2hhaXRoYW55YV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdyYW5rX2d1cnUnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9yYW5rZ3VydV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdtcycsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9jbGllbnRzL21zX2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ21vdGlvbicsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01vdGlvbl9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICd0aXJ1bWFsYScsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9jbGllbnRzL1RpcnVtYWxhX2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3RyaW5pdHknLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy90cmluaXR5X2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3NyaV92aXNod2EnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9zcmlfdmlzaHdhX2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3NyaV9iaGF2aXNoeWEnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9zcmlfYmhhdmlzaHlhX2JsYWNrLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpZDogJ3NhcmF0aF9BY2FkZW15JyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvc2FyYXRoX0FjYWRlbXlfYmxhY2sucG5nJyxcbiAgfSxcbiAge1xuICAgIGlkOiAncHJhZ2F0aGknLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9wcmFnYXRoaV9ibGFjay5wbmcnLFxuICB9LFxuICB7XG4gICAgaWQ6ICdzcmlfZ2F5YXRocmknLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9zcmlfZ2F5YXRocmlfYmxhY2sucG5nJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBIT01FX0JFTkVGSVRTID0gW1xuICB7XG4gICAgaGVhZGluZzogJ0luc3RpdHV0ZXMnLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGluZzogJ1RlYWNoZXJzJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXkuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdTdHVkZW50cycsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnUGFyZW50cycsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9NRV9CRU5FRklUU19EQVRBID0gW1xuICB7XG4gICAgaGVhZGluZzogJ1NtYXJ0ZXIgQXV0b21hdGlvbicsXG4gICAgY29udGVudDpcbiAgICAgICdHZXRSYW5rcyBnZXRzIHlvdSB1cCBhbmQgcnVubmluZyBpbiBubyB0aW1lIGFuZCB3aXRoIG91ciBoYW5kcy1vbiBjdXN0b21lciBzdWNjZXNzIHRlYW0gYW5kIGF1dG9tYXRlZCBwcm9jZXNzZXMgeW91IGNhbiBjb25kdWN0IHRlc3RzIGFuZCBhbmFseXNlIHN0dWRlbnQgZGF0YSBpbiBtaW51dGVzJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXkuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdJbnRlbGxpZ2VudCBJbnNpZ2h0cycsXG4gICAgY29udGVudDpcbiAgICAgICdPdXIgZGV0YWlsZWQgZGFzaGJvYXJkcyBoZWxwIHlvdSB1bmRlcnN0YW5kIHN0dWRlbnRz4oCZIHBlcmZvcm1hbmNlIGFuZCBoZWxwIHRoZW0gcmVtZWRpYXRlIGluIGEgcGVyc29uYWxpc2VkIG1hbm5lciBzdWl0ZWQgdG8gdGhlaXIgbmVlZHMnLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9kdW1teS5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGluZzogJ09ubGluZSBUZXN0aW5nIFBsYXRmb3JtJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0dldFJhbmtzIGhlbHBzIHlvdSBzaW11bGF0ZSB0aGUgYWN0dWFsIHRlc3QgY29uZGl0aW9ucyBhbmQgcGxhdGZvcm0gd2hpY2ggaGVscHMgeW91ciBzdHVkZW50cyBwcmVwYXJlIHdlbGwgZm9yIHRoZSBhY3R1YWwgZXhhbScsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9NRV9BV0FSRFMgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy93ZWMxLnBuZycsXG4gICAgY29udGVudDogJzUwIEZhYnVsb3VzIEVkdXRlY2ggTGVhZGVycycsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL3dlYy5wbmcnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvaWl0LXZhcmFuYXNpLnBuZycsXG4gICAgY29udGVudDogJ0Jlc3QgQW5hbHl0aWNzIEFwcCBvZiAyMDE3JyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvYmxhLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9icmFpbmZlZWQucG5nJyxcbiAgICBjb250ZW50OiAnQmVzdCBMZWFybmluZyBBbmFseXRpY3MgQ29tcGFueSBvZiAyMDE4JyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvYmYucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2hpZ2hlckVkdWNhdGlvbi5wbmcnLFxuICAgIGNvbnRlbnQ6ICdCZXN0IEFkYXB0aXZlIFRlY2hub2xvZ3kgU29sdXRpb24nLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9oZXAucG5nJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2lzLnBuZycsXG4gICAgY29udGVudDogJ091dHN0YW5kaW5nIEVkVGVjaCBTdGFydHVwcycsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvYWNoaWV2ZW1lbnRzL2lzLnBuZycsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL2FjaGlldmVtZW50cy9jaW8uc3ZnJyxcbiAgICBjb250ZW50OiAnMjAgTW9zdCBwcm9taXNpbmcgRWQtVGVjaCBzdGFydHVwcyBvZiAyMDE4JyxcbiAgICBpY29uMTogJy9pbWFnZXMvaG9tZS9hY2hpZXZlbWVudHMvY2lvLnBuZycsXG4gIH0sXG5dO1xuZXhwb3J0IGNvbnN0IEhPTUVfTkVXUyA9IFtcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9ldHZfdGVsYW5nYW5hLndlYnAnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9d3VaY2VZUnIzX00nLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9vbl9uZXdzL2tpcmFuX3Npcl9vbl9UIG5ld3Mud2VicCcsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1PYnN6UDl1alV4NCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3Mvc3R1ZGVudHNfb25fdjYud2VicCcsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1obVgxanl6YXQ5UScsXG4gIH0sXG5dO1xuZXhwb3J0IGNvbnN0IEhPTUVfTUVESUEgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2ZpbmFuY2lhbF9FeHByZXNzLndlYnAnLFxuICAgIG5hbWU6ICdmaW5hbmNpYWxfRXhwcmVzcycsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vd3d3LmZpbmFuY2lhbGV4cHJlc3MuY29tL2luZHVzdHJ5L3N1cnByaXNlLXByZWRpY3RpdmUtYW5hbHlzaXMtb2Ytc3R1ZGVudHMtZnV0dXJlLWNvdXJzZS1vZi1hY3Rpb24tZm9yLWNhcmVlci1kZXZlbG9wbWVudC1ub3ctcG9zc2libGUtZWduaWZ5LXJvbGxzLW91dC1zb2x1dGlvbi84ODEyODIvJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEveW91cnN0b3J5LndlYnAnLFxuICAgIG5hbWU6ICd5b3Vyc3RvcnknLFxuICAgIHVybDogJ2h0dHBzOi8veW91cnN0b3J5LmNvbS8yMDE3LzEyL2h5ZGVyYWJhZC1zdGFydHVwLXN0dWRlbnRzLWV4YW0vJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvaW5kaWFuLWV4cHJlc3Mud2VicCcsXG4gICAgbmFtZTogJ2luZGlhbi1leHByZXNzJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cDovL3d3dy5uZXdpbmRpYW5leHByZXNzLmNvbS9jaXRpZXMvaHlkZXJhYmFkLzIwMTcvc2VwLzE5L2ltcGV0dXMtdG8tZXhjZWwtMTY1OTY5OS5odG1sJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvYXNpYTUwMC5wbmcnLFxuICAgIG5hbWU6ICdpbmRpYW4tYXNpYTUwMCcsXG4gICAgdXJsOlxuICAgICAgJ2h0dHA6Ly93d3cuYXNpYWluYzUwMC5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTgvMTAvQXNpYS1JbmMuLTUwMC1NYWdhemluZS1PY3QtMjAxOC1EOTEucGRmJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvZGVjY2FuLndlYnAnLFxuICAgIG5hbWU6ICdpbmRpYW4tZGVjY2FuJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly93d3cuZGVjY2FuY2hyb25pY2xlLmNvbS9uYXRpb24vY3VycmVudC1hZmZhaXJzLzEyMDExOC9waWxvdC1wcm9qZWN0LXRvLW1vdWxkLXN0YXRlLXNjaG9vbC1raWRzLWluLXRlbGFuZ2FuYS5odG1sJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvZXR2LndlYnAnLFxuICAgIG5hbWU6ICdldHYnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9d3VaY2VZUnIzX00nLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9uZXdzX21pbnV0ZS53ZWJwJyxcbiAgICBuYW1lOiAnbmV3c19taW51dGUnLFxuICAgIHVybDpcbiAgICAgICdodHRwczovL3d3dy50aGVuZXdzbWludXRlLmNvbS9hcnRpY2xlL3RlbGFuZ2FuYS10aWVzLXQtaHViLXN0YXJ0dXAtZWduaWZ5LWltcHJvdmUtZWR1Y2F0aW9uLWdvdnQtc2Nob29scy03NDY1MicsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL3RlbGFuZ2FuYS10b2RheS53ZWJwJyxcbiAgICBuYW1lOiAndGVsYW5nYW5hLXRvZGF5JyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly90ZWxhbmdhbmF0b2RheS5jb20vaHlkZXJhYmFkLWJhc2VkLXN0YXJ0dXAtZWduaWZ5LXRlY2hub2xvZ2llcy1oZWxwcy1zdHVkZW50cy1leGNlbCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL2FuZGhyYWp5b3RoaS53ZWJwJyxcbiAgICBuYW1lOiAnYW5kaHJhanlvdGhpJyxcbiAgICB1cmw6ICdodHRwOi8vd3d3LmFuZGhyYWp5b3RoeS5jb20vYXJ0aWNhbD9TSUQ9NDYxODI3JyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvdjZuMS53ZWJwJyxcbiAgICBuYW1lOiAnVjYgbmV3cycsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1obVgxanl6YXQ5UScsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL25hbWFzdGhlLXRlbGFuZ2FuYS53ZWJwJyxcbiAgICBuYW1lOiAnbmFtYXN0aGUtdGVsYW5nYW5hJyxcbiAgICB1cmw6ICdodHRwOi8vZXBhcGVyLm50bmV3cy5jb20vYy8yMjk1OTg5OScsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL21lZGlhL3QtbmV3cy53ZWJwJyxcbiAgICBuYW1lOiAndC1uZXdzJyxcbiAgICB1cmw6ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PU9ic3pQOXVqVXg0JyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBIT01FX0FQUFJPQUNIID0gW1xuICB7XG4gICAgaGVhZGluZzogJ0ZvciBJbnN0aXR1dGVzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ091ciBwbGF0Zm9ybSBoZWxwcyBpbnN0aXR1dGVzIHRvIGNvbmR1Y3Qgb25saW5lIGFuZCBvZmZsaW5lIHRlc3RzIGZvciB0aGVpciBzdHVkZW50cyBhY3Jvc3MgbXVsdGlwbGUgdGVzdCBwYXR0ZXJucy4gT3VyIGRldGFpbGVkIGRhdGEgZGFzaGJvYXJkcyBoZWxwcyB5b3UgdW5kZXJzdGFuZCB5b3VyIHN0dWRlbnRzIHBlcmZvcm1hbmNlIGFuZCBoZWxwcyB5b3UgZ3VpZGUgdGhlbSB0byBzdWNjZXNzJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdGb3IgU3R1ZGVudHMnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnR2V0UmFua3MgZW1wb3dlcnMgc3R1ZGVudHMgd2l0aCBpbi1kZXB0aCBhbmFseXNpcyBvZiB0aGVpciB0ZXN0IHBlcmZvcm1hbmNlIGFjcm9zcyBDb21wZXRlbmNlIGFuZCBCZWhhdmlvdXJhbCBhc3BlY3RzIHRvIGhlbHAgdGhlbSBpbXByb3ZlIHRoZWlyIHBlcmZvcm1hbmNlIGFuZCBwYXZlIHRoZSB3YXkgdG8gdGhlaXIgZHJlYW0gY29sbGVnZScsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgSE9NRV9DTElFTlRfU0FZID0gW1xuICB7XG4gICAgY29udGVudDogYE91ciBpbnN0aXR1dGUgY2hvc2UgdGhpcyBhcHAgZm9yIHNlbmRpbmcgdXMgb25saW5lIGxlY3R1cmVzIGFuZCBmb3IgY29uZHVjdGluZyBleGFtcyBhbmQgSSBnb3R0YSBzYXksXG4gICAgaXQgcmVhbGx5IHdvcmtzIHdlbGwuIFRoZSBjbGFzc2VzIGhhdmUgaGlnaCBxdWFsaXR5IHZpc3VhbCBhbmQgeW91IGNhbiBjaGFuZ2UgdGhlIHBsYXliYWNrIHNwZWVkIGFzIHdlbGwuXG4gICAgIFlvdSBjYW4ndCB0YWtlIHNjcmVlbnNob3RzIGR1cmluZyB0aGUgZXhhbSBhbmQgYWZ0ZXIgdGhlIGV4YW0gd2UgZ2V0IGluZGl2aWR1YWwgbWFya3MgZm9yXG4gICAgIGVhY2ggc3ViamVjdCBhbmQgdGhlIHRvdGFsIGFzIHdlbGwuIFdlIGNhbiBldmVuIGNvbXBhcmUgb3VyIHByZXZpb3VzIHNjb3Jlcy4gV2UgY2FuIGV2ZW4gYW5hbHlzZSB0aGUgdG9wcGVyc1xuICAgICBzY29yZSBhbG9uZyB3aXRoIG91cnMuIE92ZXJhbGwsIGl0IGlzIGEgd29uZGVyZnVsIGFwcC5gLFxuICAgIG5hbWU6ICdTdGFjeSBDb3JkZWxpYScsXG4gICAgZGVzaWduYXRpb246ICdTdHVkZW50JyxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9zcmktY2hhaXRhbnlhLnBuZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9pY29ucy9wZXJzb25fYmx1ZS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYFRoaXMgYXBwIGlzIHNvIGdvb2QgYW5kIHRoZXJlIGlzIG5vIHByb2JsZW0gaW4gcXVhbGl0eSBvZiBjbGFzc2VzIHBvc3RlZCBpbiB0aGUgYXBwLiBUaGUgd2F5IG9mIGNvbmR1Y3RpbmcgZXhhbVxuICAgICBpbiB0aGUgYXBwIGlzIHNvIGdvb2QgdGhhdCBldmVyeSBzdHVkZW50IHdpbGwgYmUgY29tZm9ydGFibGUgdG8gd3JpdGUuIFRoaXMgYXBwIGlzIGFsc28gZ29vZCBmb3Igc3R1ZGllcy4gT25saW5lIGV4YW1zXG4gICAgICBhcmUgYWxzbyBzbyBnb29kLlRoYW5rIGZvciBzdXBwb3J0aW5nIG91ciBpbnN0aXR1dGlvbiBpbiBrZWVwaW5nIGV4YW1zIGFuZCBvbmxpbmUgY2xhc3NlcyBTbyBldmVyeWJvZHkgZG93bmxvYWQgZ2V0cmFua3NcbiAgICAgIGFuZCByZXF1ZXN0IHlvdXIgSW5zdGl0dXRlIHRvIGNvbmR1Y3QgZXhhbXMgYW5kIHBvc3QgY2xhc3NlcyBpbiBnZXRyYW5rc2AsXG4gICAgbmFtZTogYGxlY2hpcmVkZHkgU3VyZWtoYWAsXG4gICAgZGVzaWduYXRpb246IGBTdHVkZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9waW5lZ3JvdmUuanBlZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9pY29ucy9wZXJzb25fYmx1ZS5zdmcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYFRoaXMgaXMgd2F5IHRvbyB1c2VmdWwgZm9yIEpFRSBhc3BpcmFudHMgYW5kIHRoZSBVc2VyIEludGVyZmFjZSBpcyB0b28gc2ltcGxlIHRvIHVzZSBhbmQgdGhpcyBhcHAgaXMgYWRoZXJlZFxuICAgICB0byBwcml2YWN5IG9mIGVhY2ggYW5kIGV2ZXJ5IHVzZXIuIEFwYXJ0IGZyb20gdGhlIGluc3RpdHV0aW9uJ3MgbWFpbiBleGFtcyB0aGV5IGFsc28gcHJvdmlkZSB1cyB3aXRoIHRoZSBwcmFjdGljZVxuICAgICAgdGVzdHMgd2hpY2ggYXJlIGVmZmljaWVudCB0b28uIFRoZSBpZGVhIG9mIGluY2x1ZGluZyB6b29tIG1lZXRpbmdzIGluIHRoZSBhcHAgd2FzIHRvbyBpbmNyZWRpYmxl8J+RjS5gLFxuICAgIG5hbWU6ICdIZW11YnVubnkgS2FzaXJlZGR5JyxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01lbHVoYS5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvaWNvbnMvcGVyc29uX2JsdWUuc3ZnJyxcbiAgfSxcbiAgLyoge1xuICAgIGNvbnRlbnQ6IGBBbiBFeGNlbGxlbnQgYXBwIGZvciBhY2FkZW1pY3MgLlRoaXMgYXBwIGhlbHBzIHVzIHRvIGNvbXBhcmUgb3VyIHBlcmZvcm1hbmNlIHdpdGggb3RoZXIgcGVvcGxlIC5JdCBhbHNvXG4gICAgIGhlbHBzIHVzIHRvIGNvbXBhcmUgb3VyIHBlcmZvcm1hbmNlIHdpdGggdGhlIHByZXZpb3VzIG9uZS4gSXQgaGVscHMgdXMgaW4gdW5kZXJzdGFuZGluZyBvdXIgc3RyZW5ndGhzICYgd2Vha25lc3MgLlxuICAgICBUaGVyZSBpcyBncmFwaGljYWwgYW5hbHlzaXMgdG8gbWFrZSBjb21wYXJpc29uIGV2ZW4gbW9yZSBlYXNpZXIgLiBUaGUgYXBwIGFsc28gc2hvd3MgdGhlIHRpbWUgd2UgaGF2ZSBzcGVudCBvbiBlYWNoXG4gICAgICBxdWVzdGlvbiAmIHRpbWUgc3BlbnQgZm9yIGRvaW5nIHJpZ2h0ICYgd3JvbmcgYW5zd2VycyAuVGhlIGFwcCBhbHNvIGVuYWJsZXMgdXMgdG8gd2F0Y2ggdmlkZW8gbGVjdHVyZXMgdXBsb2FkZWRcbiAgICAgICBieSBpbnN0aXR1dGUgLiBPdmVyYWxsIEkgdGhpbmsgdGhlIGFwcCBpcyBhd2Vzb21lIC5gLFxuICAgIG5hbWU6IGBTcmkgUmFuZ2FgLFxuICAgIGRlc2lnbmF0aW9uOiBgU3R1ZGVudGAsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvTU9USU9OLnBuZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9pY29ucy9wZXJzb25fYmx1ZS5zdmcnLFxuICB9LCAqL1xuICB7XG4gICAgY29udGVudDogYFRoaXMgYXBwIGhhcyBleGNlbGxlbnQgdG9wIG5vdGNoIGZlYXR1cmVzIHdoaWNoIGhlbHBzIHN0dWRlbnRzIGdldCB1cCB0byB0aGUgbWFyayBldmVuIGluIHRoZSBwYW5kZW1pYy5cbiAgICAgVGhlcmUgYXJlIG5vIGVycm9ycyBhbmQgaXQgd29ya3MgcGVyZmVjdGx5IGZpbmUuIEV2ZW4gd2hlbiBhbGwgdGhlIGFjYWRlbWljcyBzaGlmdGVkIHRvIGEgd2hvbGUgb25saW5lIHByb2Nlc3MsXG4gICAgICB0aGlzIGFwcCBrZWVwcyBpdHMgbWFyayB1cC4gR29vZCBvbmUgYW5kIGkgcmVhbGx5IHN1Z2dlc3RgLFxuICAgIG5hbWU6IGBTcmlsYWtzaG1pIGthbmR1bGFgLFxuICAgIGRlc2lnbmF0aW9uOiBgUGFyZW50YCxcbiAgICBpbnN0aXR1dGU6ICcvaW1hZ2VzL2hvbWUvY2xpZW50cy9NT1RJT04ucG5nJyxcbiAgICBkZWZhdWx0aW1hZ2U6ICcvaW1hZ2VzL2ljb25zL3BlcnNvbl9ibHVlLnN2ZycsXG4gIH0sXG4gIC8qIHtcbiAgICBjb250ZW50OiBgVGhpcyBhcHAgaXMgdmVyeSBoZWxwZnVsIHRvIG1lIHRvIHN0dWR5LiBOb3csIGl0IGlzIHZlcnkgZWFzeSB0byBtZSB0byBsaXN0ZW4gbXkgY2xhc3NlcyBvciB0byBwcmFjdGljZVxuICAgICBhbnkgc3ViamVjdC5JdCBpcyByZWFsbHkgYSBndWQgdGhpbmcgLGl0IGhhcyBubyBhZHMuQW5kIHZlcnkgaW50ZXJlc3RpbmcgdGVzdHMuSSB1c2VkIHRiaXNhcHBcbiAgICAgIHZlcnkgd2VsbCBJIHdpc2ggdGhpcCBhcHAgd2lsbCBoZWxwZnVsIHRvIG1hbnkgb2YgdGhlIHN0dWRlbnRzIHdobyBhcmUgaW50ZXJlc3RlZHRvIHN0dWR5LlxuICAgICAgIEkgY29uZ3JhdHVsYXRlIHRoZSBhcHAgZm9yIHRoZSB3b3JrIHRoZXkgbWFkZS4uLi4uIFRoYW5rIHlvdSAhLi4uLi7wn5GNYCxcbiAgICBuYW1lOiBgVmFuYXBhcnRoaSBNYWRoYXZpYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OiBgVGhlIGFwcCBpcyB2ZXJ5IGNvbnZlbmllbnQgYW5kIGhlbHBmdWwuIFRoZSB3YXkgaXQgc2hvd3MgdGhlIEFuYWx5c2lzIG9mIHRoZSBleGFtcyBhbmQgdGhlIHJldmlldyBmb3JtYXRcbiAgICAgb2YgdGhlIGV4YW1zIGFyZSBleGNlbGxlbnQuLiBUaGFua3MgZm9yIHRoZSBhcHAuYCxcbiAgICBuYW1lOiBgYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OiBgQmVzdCBhbmQgZXhjZWxsZW50IGFwcC4gSXQgY2xlYXJseSBzaG93cyBtZSBwZXJjZW50YWdlIGFuZCB0aGUgcGVyY2VudGlsZSBvZiBteSBwZXJmb3JtYW5jZSBpbiBib3RoIHRoZSBtYWluc1xuICAgIGFuZCBhZHZhbmNlIGV4YW0uIEFuZCB0aGUgYmVzdCB0aGluZyBvZiB0aGlzIGFwcCBpcywgaXQgaGFkIHByYWN0aWNlIHBhcGVycyBldmVuLlRoYW5reW91IGZvciBjcmVhdGluZyBzdWNoIGFuXG4gICAgZXhjZWxsZW50IGFwcC5gLFxuICAgIG5hbWU6IGBTYXR5YXZhdGhpIE51bmlgLFxuICAgIGRlc2lnbmF0aW9uOiBgU3R1ZGVudGAsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvTU9USU9OLnBuZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXlfcHJvZmlsZS5qcGcnLFxuICB9LFxuICB7XG4gICAgY29udGVudDogYEdldHJhbmtzIGlzIGEgZ29vZCBhcHAud2hlcmUgd2UgY2FuIHNlZSBhbmQgbGlzdGVuIHRvIGxlY3R1cmVzIHBvc3RlZCBieSBvdXIgdGVhY2hlcnMgYW5kIHdlIGNhbiB3cml0ZSB0aGUgdGVzdHMuXG4gICAgb25jZSB3ZSBjb21wbGV0ZWQgb3VyIHRlc3QgLHdlIGdldCBvdXIgcmVzdWx0IGltbWVkaWF0ZWx5IHdpdGhvdXQgYW55IGRlbGF5Li53ZSBjYW4gYWxzbyBnZXQgdGhlIHNvbHV0aW9ucyBmb3JcbiAgICB0aG9zZSBxdWVzdGlvbnMgaW4gdGhlIHRlc3QuLmAsXG4gICAgbmFtZTogYFNlZXRoYWxhIFNyaW5pZGhpYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OiBgVGhpcyBpcyBvbmUgb2YgdGhlIGJlc3QgYXBwIGZvciBlZHVjdGlvbmFsIHB1cnBvc2UsIHRoZXkgd2VyZSBnaXZpbmcgbmV3IGZlYXR1cmVzIGZvciBldmVyeSB1cGRhdGUgYXMgbGlrZVxuICAgICBjb25kdWN0aW5nIHpvb20gbWVldGluZ3MgaW4gYXBwIGl0IHNlbGYgLiB0aGFua3MgZm9yIGFsbCB3aG8gd2VyZSB3b3JraW5nIHVuZGVyIHRoaXMgYXBwIGJ5IGdpdmluZyB5b3VyIGJlc3QgZm9yIHVzLmAsXG4gICAgbmFtZTogYEhBU0lUSCBWQVJESEFOYCxcbiAgICBkZXNpZ25hdGlvbjogYFN0dWRlbnRgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OiBgVGhlIGJlc3QgYXBwIGZvciBhbnkgc3R1ZGVudCAuVGhpcyBhcHAgaXMgb25lIG9mIHRoZSBtb3N0IHVzZWZ1bCBhcHAgLkl0IHJlYWxseSBzaG93cyBtZSB3aGV0aGVyIEkgaGFkXG4gICAgaW1wcm92ZWQgb3Igbm90IGluIHRoZSBleGFtaW5hdGlvbiBieSBnaXZpbmcgbXkgYWNjdXJhY3kgYW5kIGF0dGVtcHQgZm9yIGVhY2ggc3ViamVjdCAuIFRoZSBvcHRpb24gY29tcGFyZSB3aXRoIHRoZVxuICAgIHRvcHBlciByZWFsbHkgc2hvd3MgbWUgc3RpbGwgaG93IG11Y2ggSSBoYXZlIHRvIGltcHJvdmUgLiBUaGFua3MgZm9yIHRoZSBjcmVhdG9ycyB3aG8gY3JlYXRlZCBzdWNoIGFuIHdvbmRlcmZ1bCBhcHAuYCxcbiAgICBuYW1lOiBgU3VyeWEgQmhhc2thcmEgUmFvIENoZWt1cmlgLFxuICAgIGRlc2lnbmF0aW9uOiBgU3R1ZGVudGAsXG4gICAgaW5zdGl0dXRlOiAnL2ltYWdlcy9ob21lL2NsaWVudHMvTU9USU9OLnBuZycsXG4gICAgZGVmYXVsdGltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvZHVtbXkuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6IGBOaWNlIGFwcCBmb3IgamVlIG1haW5zIHByZXBhcmF0aW9uIGluIHRoZXNlIGNpcmN1bXN0YW5jZXMuIFRoZXJlIGFyZSBpbnZpZHVhbCBwcmFjdGljZSB0ZXN0cyBvZiBzdWJqZWN0IGFuZFxuICAgIGNoYXB0ZXIgcHJhY3RpY2UgLiBXaGljaCBjYW4gaGVscCB0byBzdHJlbmd0aGVuIG91ciB3ZWFrIGFyZWFzIHdpdGggamVlIG1haW4gdHlwZSBtb2NrIHRlc3QuIFRoYW5rIHlvdSB0ZWFtIGVnbmlmeWAsXG4gICAgbmFtZTogYFJpc2hpdGhhIHM5OWAsXG4gICAgZGVzaWduYXRpb246IGBgLFxuICAgIGluc3RpdHV0ZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL01PVElPTi5wbmcnLFxuICAgIGRlZmF1bHRpbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2R1bW15LnN2ZycsXG4gIH0sICovXG5dO1xuXG5leHBvcnQgY29uc3QgQVNTRVNTX0lOREVQVEggPSBbXG4gIHtcbiAgICBoZWFkaW5nOiAnTWFya3MgQW5hbHlzaXMnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnT3VyIE1hcmtzIEFuYWx5c2lzIGhlbHBzIHlvdSBpZGVudGlmeSB0aGUgYXQtcmlzayBhbmQganVzdC1taXNzZWQgc3R1ZGVudHMgYW5kIGVuYWJsZXMgeW91IHRvIHNldCBjdXN0b21pc2VkIGdvYWxzIGFuZCBudWRnZSB0aGVtIGZvcndhcmQuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYXNzZXNzLXNjcmVlbnNob3Q0LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkaW5nOiAnRXJyb3IgQW5hbHlzaXMnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTmVnYXRpdmUgbWFya2luZyBpcyB0aGUgYmFuZSB0aGF0IHB1bGxzIHlvdXIgc3R1ZGVudHMgcGVyZm9ybWFuY2UgZG93bi4gUGlucG9pbnQgdGhlIGFyZWFzIHRoYXQgbmVlZHMgdG8gYmUgYWRkcmVzc2VkIGFuZCBib29zdCB0aGUgcGVyZm9ybWFuY2Ugb2Ygc3R1ZGVudHMgYnkgbWluaW1pc2luZyBlcnJvcnMnLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9hc3Nlc3Mtc2NyZWVuc2hvdDUuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdDb25jZXB0IEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ1N0dWRlbnRzIHBlcmZvcm1hbmNlIGluIFN1YmplY3RzIGlzIGp1c3Qgbm90IGVub3VnaC4gR2V0UmFua3MgZ29lcyBmdXJ0aGVyIGFuZCBnaXZlcyB5b3UgYSBkZXRhaWxlZCBwaWN0dXJlIG9mIHRoZSBoZWFsdGggb2YgeW91IHN0dWRlbnRzIGFjcm9zcyB0b3BpY3MgYW5kIHN1YnRvcGljcyBjb3ZlcmVkIGluIHRoZSB0ZXN0LicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2Fzc2Vzcy1zY3JlZW5zaG90Ni5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEFOQUxZWkVfQ09OVEVOVF8xID0gW1xuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0RlZGljYXRlZCBTdHVkZW50IFByb2ZpbGUnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1RoZSBvbmUgc3RvcCB0aGF0IGdpdmVzIHlvdSB0aGUgZGV0YWlsZWQgc3R1ZGVudCBwZXJmb3JtYW5jZSBhbmFseXNpcyB3aXRoIGVhc3kgdG8gdW5kZXJzdGFuZCBkYXNoYm9hcmRzIGFuZCBwZXJmb3JtYW5jZSB0cmVuZHMgYWNyb3NzIHlvdXIgTWFya3MsIFRvcGljcyBhbmQgQmVoYXZpb3VyYWwgYXNwZWN0cycsXG4gICAgYnV0dG9uOiAnQ0xJQ0sgVE8gS05PVycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9hbmFseXplLXNjcmVlbnNob3QxLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnUXVlc3Rpb24gUmV2aWV3JyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdSZXZpZXcgeW91ciB0ZXN0IHBlcmZvcm1hbmNlIHF1ZXN0aW9uLWJ5LXF1ZXN0aW9uIGFuZCBnZXQgYW4gdW5kZXJzdGFuZGluZyBvZiBob3cgeW91ciBwZWVycyBwZXJmb3JtZWQgaW4gZWFjaCBvZiB0aGVtIC0gYWNyb3NzIGNvbXBldGVuY2UgYW5kIGJlaGF2aW91cmFsIGFzcGVjdHMnLFxuICAgIGJ1dHRvbjogJ0NMSUNLIFRPIEtOT1cnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90Mi5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ01hcmsgQW5hbHlzaXMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0tub3cgeW91ciBtYXJrcyBhY3Jvc3MgZWFjaCBzdWJqZWN0IGFuZCBkaWZmaWN1bHR5IGxldmVscy4gVGhlIGF0dGVtcHQgYW5kIGFjY3VyYWN5IHJhdGVzIGhlbHAgeW91IHVuZGVyc3RhbmQgeW91ciB3ZWFrIGF0dGVtcHRzIGltcHJvdmUgdGhlIHNhbWUgZ29pbmcgZm9yd2FyZCcsXG4gICAgYnV0dG9uOiAnVEFLRSBBIFRFU1QnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90My5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0NvbmNlcHQgQW5hbHlzaXMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1VuZGVyc3RhbmQgdGhlIHdlaWdodGFnZSBvZiBlYWNoIHRvcGljIGFuZCB5b3VyIHBlcmZvcm1hbmNlIGluIGl0IGFjcm9zcyBhbGwgdGhlIHRlc3RzIGF0dGVtcHRlZCBkdXJpbmcgdGhlIHllYXInLFxuICAgIGJ1dHRvbjogJ0NMSUNLIFRPIEtOT1cnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90NC5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEFOQUxZWkVfQkVIQVZJT1VSX0FOQUxZU0lTID0gW1xuICB7XG4gICAgaGVhZGluZzogJ1RpbWUgQW5hbHlzaXMnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnVGltZSBtYW5hZ2VtZW50IGlzIHRoZSBtb3N0IGltcG9ydGFudCBhc3BlY3Qgb2YgdGVzdCB0YWtpbmcuIEdldFJhbmtzIHRyYWNrcyB5b3VyIHRpbWUgZHVyaW5nIHRoZSB0ZXN0IGFuZCBwcm92aWRlcyBhbiBlYXNpbHkgdW5kZXJzdGFuZGFibGUgdGltZSBtYXAgdG8gaGVscCB5b3UgaW1wcm92ZSB5b3VyIHRpbWUgbWFuYWdlbWVudCBza2lsbHMnLFxuICB9LFxuICB7XG4gICAgaGVhZGluZzogJ1F1ZXN0aW9uIEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0V2ZXJ5IHF1ZXN0aW9uIGlzIGRpZmZlcmVudCBhbmQgd2UgdHJlYXQgaXQgYXMgc3VjaDsgd2UgYW5hbHlzZSBob3cgeW91IHBlcmZvcm1lZCBpbiBlYWNoIHF1ZXN0aW9uIGFjcm9zcyB2YXJpb3VzIGRpbWVuc2lvbnMgYW5kIG51ZGdlIHlvdSB0byBpbXByb3ZlJyxcbiAgfSxcbiAge1xuICAgIGhlYWRpbmc6ICdEaWZmaWN1bHR5IEFuYWx5c2lzJyxcbiAgICBjb250ZW50OlxuICAgICAgJ1RoZSBkaWZmaWN1bHR5IG9mIHRoZSBwYXBlciBoYXMgYSBkaXJlY3QgaW1wYWN0IG9uIHN0dWRlbnRzIHBlcmZvcm1hbmNlIGFuZCB3ZSBoZWxwIHlvdSB1bmRlcnN0YW5kIHlvdXIgZGlmZmljdWx0eSBwcm9maWxlIHNvIHRoYXQgeW91IGNhbiBpbXByb3ZlIHlvdXIgYWNjdXJhY3kgYW5kIHJlZHVjZSBuZWdhdGl2ZSBtYXJraW5nJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBBTkFMWVpFX0NPTlRFTlRfMiA9IFtcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdDb21wYXJlICcsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnQ29tcGFyZSB5b3VyIHBlcmZvcm1hbmNlIHdpdGggdGhlIHRvcHBlcnMgYW5kIGZpZ3VyZSBvdXQgdGhlaXIgd2F5IG9mIGNsaW1iaW5nIHRvIHRoZSB0b3AnLFxuICAgIGJ1dHRvbjogJ0NPTVBBUkUgTk9XJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2FuYWx5emUtc2NyZWVuc2hvdDYuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdSYW5rIFByZWRpY3RvcicsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnRmluZCBvdXQgd2hlcmUgeW91IHN0YW5kIGF0IHRoZSBuYXRpb25hbCBsZXZlbC4gU2V0IGdvYWxzIGFuZCBjaGFydCB0aGUgcGF0aCB0byB5b3VyIGRyZWFtIGNvbGxlZ2UhJyxcbiAgICBidXR0b246ICdLTk9XIE1PUkUnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9nZXQtcmFua3MvYW5hbHl6ZS1zY3JlZW5zaG90Ny5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IFBSQUNUSUNFX0NPTlRFTlQgPSBbXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnQXNrIERpc2hhIScsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnSGF2aW5nIHRyb3VibGUgZGVjaWRpbmcgd2hhdCB0byBwcmFjdGljZT8gRG9u4oCZdCB3b3JyeSwganVzdCBhc2sgRGlzaGEgYW5kIHNoZeKAmWxsIHRha2UgeW91IHRocm91Z2ggdGhlIG9wdGltYWwgbGVhcm5pbmcgcGF0aCB0byBhY2hpZXZlIHlvdXIgZ29hbHMuJyxcbiAgICBidXR0b246ICdUQUtFIEEgVEVTVCcsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmFjdGljZS1zY3JlZW5zaG90MS5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0N1c3RvbSBQcmFjdGljZScsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnUHJhY3RpY2UgYXQgeW91ciBvd24gcGFjZS4gQ2hvb3NlIHlvdXIgb3duIGNoYXB0ZXJzLCBkaWZmaWN1bHR5IGFuZCB0aW1lLCBmZWVsIGNvbWZvcnRhYmxlIGluIHRoZSB0ZXN0IHRha2luZyBleHBlcmllbmNlIGFuZCB0YXJnZXRlZGx5IGltcHJvdmUgeW91ciBzY29yZXMuJyxcbiAgICBidXR0b246ICdUQUtFIEEgVEVTVCcsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmFjdGljZS1zY3JlZW5zaG90Mi5zdmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0FsbCBJbmRpYSBUZXN0IFNlcmllcycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnQ29tcGFyZSB5b3VyIHBlcmZvcm1hbmNlIHdpdGggYWxsIHlvdXIgY29tcGV0aXRvcnMgYWNyb3NzIHRoZSBjb3VudHJ5LiBFdmFsdWF0ZSB3aGVyZSB5b3Ugc3RhbmQgYW5kIHRhcmdldCB5b3VyIGFwcHJvYWNoIGFjY29yZGluZ2x5LicsXG4gICAgYnV0dG9uOiAnQ0xJQ0sgVE8gS05PVycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2dldC1yYW5rcy9hbmFseXplLXNjcmVlbnNob3QzLnN2ZycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgU1RVREVOVFNfUkVDT1JEUyA9IFtcbiAge1xuICAgIHJhbms6ICdBSVIgMzU0JyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvbmF2bmVldC5wbmcnLFxuICAgIG5hbWU6ICdWIE5BVkFORUVUSCcsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDUzNDI4MicsXG4gIH0sXG4gIHtcbiAgICByYW5rOiAnQUlSIDM1OScsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL25pdHlhbmFuZC5wbmcnLFxuICAgIG5hbWU6ICdQLlMuUy4gTklUSFlBTkFORCcsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDI5ODYzOCcsXG4gIH0sXG4gIHtcbiAgICByYW5rOiAnQUlSIDQ0MScsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL3JvaGl0cmFqLnBuZycsXG4gICAgbmFtZTogJ1IuUk9ISVRSQUonLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTA1MjI1MzgnLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiA1MDUnLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy9jaGFuZHUucG5nJyxcbiAgICBuYW1lOiAnQ0guQ0hBTkRVJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwMTUwNzU5JyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgNjE4JyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvc2FpcGF2YW4ucG5nJyxcbiAgICBuYW1lOiAnUiBTQUkgUEFWQU4nLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTAyMTg5NDAnLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiA3MjMnLFxuICAgIGltZ1VybDogJy9pbWFnZXMvaG9tZS9zdHVkZW50cy90ZWphc3dpLnBuZycsXG4gICAgbmFtZTogJ1AuVEVKQVNXSScsXG4gICAgYXBwX25vOiAnQVBQLk5PLjE5MDMxMDExNzQ1NycsXG4gIH0sXG4gIHtcbiAgICByYW5rOiAnQUlSIDk3MicsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL3ByZW1jaGFuZC5wbmcnLFxuICAgIG5hbWU6ICdHIFBSRU0gQ0hBTkQnLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTAyMTg2MzAnLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiAxMDM5JyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvc2Fpa3Jpc2huYS5wbmcnLFxuICAgIG5hbWU6ICdBIFNBSSBLUklTSE5BJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwNDc2MzY4JyxcbiAgfSxcbiAge1xuICAgIHJhbms6ICdBSVIgMTExOCcsXG4gICAgaW1nVXJsOiAnL2ltYWdlcy9ob21lL3N0dWRlbnRzL2FiaGlsYXNoLnBuZycsXG4gICAgbmFtZTogJ0suQUJISUxBU0gnLFxuICAgIGFwcF9ubzogJ0FQUC5OTy4xOTAzMTAyMTgzNzknLFxuICB9LFxuICB7XG4gICAgcmFuazogJ0FJUiAxMTUwJyxcbiAgICBpbWdVcmw6ICcvaW1hZ2VzL2hvbWUvc3R1ZGVudHMvdmFyc2hhLnBuZycsXG4gICAgbmFtZTogJ0EuVkFSU0hBJyxcbiAgICBhcHBfbm86ICdBUFAuTk8uMTkwMzEwODAxNjAyJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBTTElERVNIT1cgPSBbXG4gIHtcbiAgICB0aXRsZTogJ0pFRSBNQUlOJyxcbiAgICBkb3VibGVEaWdpdFJhbmtzOiA3MCxcbiAgICB0cmlwbGVEaWdpdFJhbmtzOiAnNDAxJyxcbiAgICBiZXN0UmFua3M6ICcxLCAxMSwgMTMsIDE0LCAyNCcsXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0pFRSBBRFYuJyxcbiAgICBkb3VibGVEaWdpdFJhbmtzOiAzMCxcbiAgICB0cmlwbGVEaWdpdFJhbmtzOiAnMTMwJyxcbiAgICBiZXN0UmFua3M6ICcxLCA3LCAxNywgMjQsIDI4JyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBURUFDSEZPUiA9IFtcbiAge1xuICAgIHRpdGxlOiAnQWNhZGVtaWNzJyxcbiAgICBjb3Vyc2VzOiBbXG4gICAgICAnQ0xBU1MgNicsXG4gICAgICAnIENMQVNTIDcnLFxuICAgICAgJyBDTEFTUyA4JyxcbiAgICAgICcgQ0xBU1MgOScsXG4gICAgICAnIENMQVNTIDEwJyxcbiAgICAgICcgQ0xBU1MgMTEnLFxuICAgICAgJyBDTEFTUyAxMicsXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHRpdGxlOiAnQ29tcGV0aXRpdmUgRXhhbXMnLFxuICAgIGNvdXJzZXM6IFtcbiAgICAgICdJSVQgSkVFJyxcbiAgICAgICcgTkVFVCcsXG4gICAgICAnIEVBTUNFVCcsXG4gICAgICAnQ0JTRScsXG4gICAgICAnQklUU0FUJyxcbiAgICAgICdDT01FRC1LJyxcbiAgICAgICdWSVRFRUUnLFxuICAgICAgJ0FJSU1TJyxcbiAgICAgICdKSVBNRVInLFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0V4dHJhY3VycmljdWxhcicsXG4gICAgY291cnNlczogW1xuICAgICAgJ01VU0lDJyxcbiAgICAgICdQQUlOVElORycsXG4gICAgICAnREFOQ0lORycsXG4gICAgICAnWU9HQScsXG4gICAgICAnTUVESVRBVElPTicsXG4gICAgICAnQUJBQ1VTJyxcbiAgICAgICdQSE9UT0dSQVBIWScsXG4gICAgXSxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBQTEFURk9STVMgPSBbXG4gIHtcbiAgICBsYWJlbDogJ01vYmlsZScsXG4gICAgYXZhaWxhYmxlOiAnQW5kcm9pZCAmIGlPUycsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvbW9iaWxlLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0Rlc2t0b3AnLFxuICAgIGF2YWlsYWJsZTogJ0Nocm9tZScsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvZGVza3RvcC5zdmcnLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdUYWJsZXQnLFxuICAgIGF2YWlsYWJsZTogJ0FuZHJvaWQgJiBpT1MnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3RhYmxldC5zdmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IENVU1RPTUVSU19TVEFUUyA9IFtcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9jdXN0b21lcnNfbmV3LnN2ZycsXG4gICAgbnVtYmVyOiAnMSBNaWxsaW9uICsnLFxuICAgIHRleHQ6ICdTYXRpc2ZpZWQgU3R1ZGVudHMnLFxuICAgIGxhYmVsOiAnQ3VzdG9tZXJzIExvZ28nLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL2luc3RpdHV0aW9uX25ldy5zdmcnLFxuICAgIG51bWJlcjogJzEwMCsnLFxuICAgIHRleHQ6ICdTYXRpc2ZpZWQgSW5zdGl0dXRpb25zJyxcbiAgICBsYWJlbDogJ0luc3RpdHV0aW9ucyBMb2dvJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBDVVNUT01FUl9SRVZJRVcgPSBbXG4gIHtcbiAgICBsb2dvOiAnaW1hZ2VzL2N1c3RvbWVycy9zcmlDaGFpdGFueWFfbG9nby5qcGcnLFxuICAgIHBpYzogJ2ltYWdlcy9jdXN0b21lcnMvc3J1amFuLmpwZycsXG4gICAgbGFiZWw6ICdTcmkgQ2hhaXRhbnlhJyxcbiAgICB0ZXh0OiBgXCIgT3VyIGluc3RpdHV0ZSBjaG9zZSB0aGlzIGFwcCBmb3Igc2VuZGluZyB1cyBvbmxpbmUgbGVjdHVyZXMgYW5kIGZvclxuICAgIGNvbmR1Y3RpbmcgZXhhbXMgYW5kIEkgZ290dGEgc2F5LCBpdCByZWFsbHkgd29ya3Mgd2VsbC4gVGhlIGNsYXNzZXNcbiAgICBoYXZlIGhpZ2ggcXVhbGl0eSB2aXN1YWwgYW5kIHlvdSBjYW4gY2hhbmdlIHRoZSBwbGF5YmFjayBzcGVlZCBhcyB3ZWxsLlxuICAgIFlvdSBjYW4ndCB0YWtlIHNjcmVlbnNob3RzIGR1cmluZyB0aGUgZXhhbSBhbmQgYWZ0ZXIgdGhlIGV4YW0gd2UgZ2V0IGluZGl2aWR1YWwgbWFya3NcbiAgICBmb3IgZWFjaCBzdWJqZWN0IGFuZCB0aGUgdG90YWwgYXMgd2VsbC4gV2UgY2FuIGV2ZW4gY29tcGFyZSBvdXIgcHJldmlvdXMgc2NvcmVzLlxuICAgICBXZSBjYW4gZXZlbiBhbmFseXNlIHRoZSB0b3BwZXJzIHNjb3JlIGFsb25nIHdpdGggb3Vycy4gT3ZlcmFsbCwgaXQgaXMgYSB3b25kZXJmdWwgYXBwLlwiYCxcbiAgICBuYW1lOiAnU3RhY3kgQ29yZGVsaWEnLFxuICAgIGRlc2lnbmF0aW9uOiAnU3R1ZGVudCcsXG4gIH0sXG4gIHtcbiAgICBsb2dvOiAnaW1hZ2VzL2N1c3RvbWVycy9yYW5rZ3VydV9sb2dvLmpwZycsXG4gICAgcGljOiAnaW1hZ2VzL2N1c3RvbWVycy9sYXhtYW4uanBnJyxcbiAgICBsYWJlbDogJ1JhbmtndXJ1JyxcbiAgICB0ZXh0OiBgXCJUaGlzIGFwcCBpcyBzbyBnb29kIGFuZCB0aGVyZSBpcyBubyBwcm9ibGVtIGluIHF1YWxpdHkgb2YgY2xhc3NlcyBwb3N0ZWQgaW4gdGhlIGFwcC4gVGhlIHdheSBvZiBjb25kdWN0aW5nIGV4YW1cbiAgICAgaW4gdGhlIGFwcCBpcyBzbyBnb29kIHRoYXQgZXZlcnkgc3R1ZGVudCB3aWxsIGJlIGNvbWZvcnRhYmxlIHRvIHdyaXRlLiBUaGlzIGFwcCBpcyBhbHNvIGdvb2QgZm9yIHN0dWRpZXMuIE9ubGluZSBleGFtc1xuICAgICAgYXJlIGFsc28gc28gZ29vZC5UaGFuayBmb3Igc3VwcG9ydGluZyBvdXIgaW5zdGl0dXRpb24gaW4ga2VlcGluZyBleGFtcyBhbmQgb25saW5lIGNsYXNzZXMgU28gZXZlcnlib2R5IGRvd25sb2FkXG4gICAgICAgZ2V0cmFua3MgYW5kIHJlcXVlc3QgeW91ciBJbnN0aXR1dGUgdG8gY29uZHVjdCBleGFtcyBhbmQgcG9zdCBjbGFzc2VzIGluIGdldHJhbmtzXCJgLFxuICAgIG5hbWU6ICdsZWNoaXJlZGR5IFN1cmVraGEnLFxuICAgIGRlc2lnbmF0aW9uOiAnU3R1ZGVudCcsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgQ0xJRU5UU19TRVQxID0gW1xuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy90cy5wbmcnLCBsYWJlbDogJ1RTIEdvdnQuJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9zcmlfY2hhaXRoYW55YS5wbmcnLCBsYWJlbDogJ1NyaSBDaGFpdGhhbnlhJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9SYW5rZ3VydS5wbmcnLCBsYWJlbDogJ1JhbmtHdXJ1JyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9tb3Rpb24ucG5nJywgbGFiZWw6ICdNb3Rpb24nIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL21zLnBuZycsIGxhYmVsOiAnTVMnIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL3RpcnVtYWxhLnBuZycsIGxhYmVsOiAnVGlydW1hbGEnIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL3RyaW5pdHkucG5nJywgbGFiZWw6ICdUcmluaXR5JyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy92aXN3YS5wbmcnLCBsYWJlbDogJ1NyaSBWaXN3YScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvYmhhdmlzaHlhLnBuZycsIGxhYmVsOiAnQmhhdmlzaHlhJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9wcmFnYXRoaV9qci5wbmcnLCBsYWJlbDogJ1ByZ2F0aGkgSnIuJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9zYXJhdGgucG5nJywgbGFiZWw6ICdTYXJhdGggQWNhZGVteScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvcHJhZ2F0aGkucG5nJywgbGFiZWw6ICdQcmFnYXRoaScgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9zYXJhc3dhdGhpX2dyb3Vwcy5wbmcnLFxuICAgIGxhYmVsOiAnU2FyYXN3YXRoaV9Hcm91cHMnLFxuICB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9zaGl2YW5pLnBuZycsIGxhYmVsOiAnU2hpdmFuaScgfSxcbiAgeyBpY29uOiAnL2ltYWdlcy9jdXN0b21lcnMvcHJhdGhpYmhhX2pyLnBuZycsIGxhYmVsOiAnUHJhdGhpYmhhX0pyJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9SZXNvbmFuY2UucG5nJywgbGFiZWw6ICdSZXNvbmFuY2UnIH0sXG4gIHsgaWNvbjogJy9pbWFnZXMvY3VzdG9tZXJzL3ZpamV0aGEucG5nJywgbGFiZWw6ICdWaWpldGhhJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy90b3BwZXJzLnBuZycsIGxhYmVsOiAnVG9wcGVycycgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBDTElFTlRTX1NFVDIgPSBbXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvYWJ2LnBuZycsIGxhYmVsOiAnQUJWJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL21hc3Rlcl9taW5kcy5wbmcnLCBsYWJlbDogJ01hc3RlciBNaW5kcycgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9zcmlfZ2F5YXRocmkucG5nJywgbGFiZWw6ICdTcmkgR2F5YXRocmknIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvcGluZWdyb3ZlLnBuZycsIGxhYmVsOiAnUGluZWdyb3ZlJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3NyaV9ha3NoYXJhLnBuZycsIGxhYmVsOiAnU3JpIEFrc2hhcmEnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvdXJiYW5lLnBuZycsIGxhYmVsOiAnVXJiYW5lJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3NyaV9wcmFrYXNoLnBuZycsIGxhYmVsOiAnU3JpIFByYWthc2gnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvdmlzaHJhLnBuZycsIGxhYmVsOiAnVmlzaHJhJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL25hdm9kYXlhLnBuZycsIGxhYmVsOiAnTmF2b2RheWEnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvbmV3ZXJhLnBuZycsIGxhYmVsOiAnTmV3IEVyYScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9ha3NoYXlhLnBuZycsIGxhYmVsOiAnQWtzaGF5YScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9neWFuYW0ucG5nJywgbGFiZWw6ICdHeWFuYW0nIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvZWxlZ2FuY2UucG5nJywgbGFiZWw6ICdFbGVnYW5jZScgfSxcbiAgeyBpY29uOiAnaW1hZ2VzL2N1c3RvbWVycy9rYWthdGhpeWEucG5nJywgbGFiZWw6ICdLYWthdGhpeWEnIH0sXG4gIHsgaWNvbjogJ2ltYWdlcy9jdXN0b21lcnMvcmFtYWJhbmFtLnBuZycsIGxhYmVsOiAnUmFtYUJhbmFtJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL2dlZXRoYW0ucG5nJywgbGFiZWw6ICdHZWV0aGFtJyB9LFxuICB7IGljb246ICdpbWFnZXMvY3VzdG9tZXJzL3NyaV92YWlzaG5hdmkucG5nJywgbGFiZWw6ICdTcmlfVmFpc2huYXZpJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL2N1c3RvbWVycy9kaGlrc2hhLnBuZycsIGxhYmVsOiAnRGhpa3NoYScgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBMSVZFQ0xBU1NFU0RBVEEgPSBbXG4gIHtcbiAgICByb3V0ZTogJ1RlYWNoJyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvdGVhY2guc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdUZWFjaCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdUZWFjaCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvTGl2ZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ0xpdmUnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0xpdmUnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL0Fzc2lnbm1lbnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQXNzaWdubWVudHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9Eb3VidHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdEb3VidHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0RvdWJ0cycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICByb3V0ZTogJ1Rlc3QnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L3Rlc3Quc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdUZXN0JyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ1Rlc3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvVGVzdHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdUZXN0cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnVGVzdHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvQW5hbHlzaXMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdBbmFseXNpcycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQW5hbHlzaXMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvUHJhY3RpY2Uuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdQcmFjdGljZScsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUHJhY3RpY2UnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvUGFwZXJzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnUGFwZXJzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdQYXBlcnMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvUmVwb3J0cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1JlcG9ydHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1JlcG9ydHMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdDb25uZWN0JyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29ubmVjdC9jb25uZWN0LnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQ29ubmVjdCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdDb25uZWN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L3NjaGVkdWxlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnU2NoZWR1bGUnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1NjaGVkdWxlJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L21lc3Nlbmdlci5zdmcnLFxuICAgICAgICB0aXRsZTogJ01lc3NlbmdlcicsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnTWVzc2VuZ2VyJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L25vdGlmaWNhdGlvbnMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdNZXNzZW5nZXInLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ01lc3NlbmdlcicsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICByb3V0ZTogJ0NvbGxlY3QnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db2xsZWN0L2NvbGxlY3Quc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdDb2xsZWN0JyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ0NvbGxlY3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0NvbGxlY3QvZmVlX2NvbGxlY3Rpb24uc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdGZWUgQ29sbGVjdGlvbicsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnRmVlQ29sbGVjdCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29sbGVjdC9FLVJlY2VpcHRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnRS1SZWNlaXB0cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnUmVjZWlwdHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjpcbiAgICAgICAgICAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvQ29sbGVjdC9hdXRvX3BheW1lbnRfcmVtYWluZGVycy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0F1dG8gUGF5bWVudCBSZW1haW5kZXJzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBdXRvUGF5bWVudHMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdNYW5hZ2UnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9NYW5hZ2UvbWFuYWdlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnTWFuYWdlJyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ01hbmFnZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvTWFuYWdlL2FkbWlzc2lvbnMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdBZG1pc3Npb25zJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBZG1pc3Npb25zJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9NYW5hZ2UvYXR0ZW5kZW5jZXMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdBdHRlbmRhbmNlJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdBdHRlbmRhbmNlJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9NYW5hZ2Uvc3R1ZGVudHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdTdHVkZW50cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnU3R1ZGVudHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL01hbmFnZS9yb2xlcy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1JvbGVzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdSb2xlcycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG5dO1xuZXhwb3J0IGNvbnN0IE9MRF9MSVZFQ0xBU1NFU0RBVEEgPSBbXG4gIHtcbiAgICByb3V0ZTogJ1RlYWNoJyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1RlYWNoL1RlYWNoLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnVGVhY2gnLFxuICAgICAgICBpc0hlYWRpbmc6IHRydWUsXG4gICAgICAgIHJvdXRlOiAnVGVhY2gnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZWFjaC9MaXZlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnTGl2ZScsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnTGl2ZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1RlYWNoL0Fzc2lnbm1lbnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQXNzaWdubWVudHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvVGVhY2gvRG91YnRzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnRG91YnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdEb3VidHMnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcm91dGU6ICdUZXN0JyxcbiAgICBmZWF0dXJlczogW1xuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1Rlc3QvVGVzdC5zdmcnLFxuICAgICAgICB0aXRsZTogJ1Rlc3QnLFxuICAgICAgICBpc0hlYWRpbmc6IHRydWUsXG4gICAgICAgIHJvdXRlOiAnVGVzdCcsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1Rlc3QvVGVzdHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdUZXN0cycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnVGVzdHMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZXN0L0FuYWx5c2lzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQW5hbHlzaXMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0FuYWx5c2lzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvVGVzdC9QcmFjdGljZS5zdmcnLFxuICAgICAgICB0aXRsZTogJ1ByYWN0aWNlJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdQcmFjdGljZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL1Rlc3QvUGFwZXJzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnUGFwZXJzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdQYXBlcnMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9UZXN0L1JlcG9ydHMuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdSZXBvcnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdSZXBvcnRzJyxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHJvdXRlOiAnQ29ubmVjdCcsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9Db25uZWN0L0Nvbm5lY3Quc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdDb25uZWN0JyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ0Nvbm5lY3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9Db25uZWN0L1NjaGVkdWxlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnU2NoZWR1bGUnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ1NjaGVkdWxlJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29ubmVjdC9NZXNzZW5nZXIuc3ZnJyxcbiAgICAgICAgdGl0bGU6ICdNZXNzZW5nZXInLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ01lc3NlbmdlcicsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL0Nvbm5lY3QvTm90aWZpY2F0aW9ucy5zdmcnLFxuICAgICAgICB0aXRsZTogJ05vdGlmaWNhdGlvbnMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ01lc3NlbmdlcicsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICByb3V0ZTogJ0NvbGxlY3QnLFxuICAgIGZlYXR1cmVzOiBbXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29sbGVjdC9Db2xsZWN0LnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQ29sbGVjdCcsXG4gICAgICAgIGlzSGVhZGluZzogdHJ1ZSxcbiAgICAgICAgcm91dGU6ICdDb2xsZWN0JyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGljb246ICdpbWFnZXMvaG9tZS9zdWJtZW51aXRlbXMvQ29sbGVjdC9GZWVDb2xsZWN0aW9uLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnRmVlIENvbGxlY3Rpb24nLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0ZlZUNvbGxlY3QnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9Db2xsZWN0L0VSZWNlaXB0cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ0UtUmVjZWlwdHMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0VSZWNlaXB0cycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL0NvbGxlY3QvQXV0b1BheW1lbnRSZW1haW5kZXJzLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQXV0byBQYXltZW50IFJlbWFpbmRlcnMnLFxuICAgICAgICBpc0hlYWRpbmc6IGZhbHNlLFxuICAgICAgICByb3V0ZTogJ0F1dG9QYXltZW50cycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICByb3V0ZTogJ01hbmFnZScsXG4gICAgZmVhdHVyZXM6IFtcbiAgICAgIHtcbiAgICAgICAgaWNvbjogJ2ltYWdlcy9ob21lL3N1Ym1lbnVpdGVtcy9NYW5hZ2UvTWFuYWdlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnTWFuYWdlJyxcbiAgICAgICAgaXNIZWFkaW5nOiB0cnVlLFxuICAgICAgICByb3V0ZTogJ01hbmFnZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL01hbmFnZS9BZG1pc3Npb25zLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQWRtaXNzaW9ucycsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQWRtaXNzaW9ucycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL01hbmFnZS9BdHRlbmRhbmNlLnN2ZycsXG4gICAgICAgIHRpdGxlOiAnQXR0ZW5kYW5jZScsXG4gICAgICAgIGlzSGVhZGluZzogZmFsc2UsXG4gICAgICAgIHJvdXRlOiAnQXR0ZW5kYW5jZScsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL01hbmFnZS9TdHVkZW50cy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1N0dWRlbnRzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdTdHVkZW50cycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpY29uOiAnaW1hZ2VzL2hvbWUvc3VibWVudWl0ZW1zL01hbmFnZS9Sb2xlcy5zdmcnLFxuICAgICAgICB0aXRsZTogJ1JvbGVzJyxcbiAgICAgICAgaXNIZWFkaW5nOiBmYWxzZSxcbiAgICAgICAgcm91dGU6ICdSb2xlcycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgTUVESUFfQ09WRVJBR0UgPSBbXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy9maW5hbmNpYWxfZXhwcmVzcy0xLnBuZycsXG4gICAgdGl0bGU6XG4gICAgICAnU3VycHJpc2UhIFByZWRpY3RpdmUgYW5hbHlzaXMgb2Ygc3R1ZGVudHMgZnV0dXJlIGNvdXJzZSBvZiBhY3Rpb24gZm9yIGNhcmVlciBkZXZlbG9wbWVudCBub3cgcG9zc2libGU7IEVnbmlmeSByb2xscyBvdXQgc29sdXRpb24nLFxuICAgIGF1dGhvcjogJ0J5OiBCViBNYWhhbGFrc2htaScsXG4gICAgY29udGVudDpcbiAgICAgICdGb3Igc3R1ZGVudHMsIGl0IGlzIGFsd2F5cyBsZWFybiwgcmVsZWFybiBhbmQgdW5sZWFybiB0byBoaXQgdGhlIHRvcCBzY29yZS4gQnV0IG5laXRoZXIgdGhlIGVkdWNhdG9ycyBub3IgcGFyZW50cyBvciB0aGUgc3R1ZGVudHMgY2FuIGRvIGEgcHJlZGljdGl2ZSBhbmFseXNpcyBhYm91dCB0aGVpciBmdXR1cmUgY291cnNlIG9mIGFjdGlvbiBmb3IgY2FyZWVyIGRldmVsb3BtZW50IGFuZCB0aGUgZGlsZW1tYSBjb250aW51ZXMgZm9yIGdlbmVyYXRpb25zLicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHBzOi8vd3d3LmZpbmFuY2lhbGV4cHJlc3MuY29tL2luZHVzdHJ5L3N1cnByaXNlLXByZWRpY3RpdmUtYW5hbHlzaXMtb2Ytc3R1ZGVudHMtZnV0dXJlLWNvdXJzZS1vZi1hY3Rpb24tZm9yLWNhcmVlci1kZXZlbG9wbWVudC1ub3ctcG9zc2libGUtZWduaWZ5LXJvbGxzLW91dC1zb2x1dGlvbi84ODEyODIvJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL3lvdXJzdG9yeS0xLnBuZycsXG4gICAgdGl0bGU6ICdUaGlzIEh5ZGVyYWJhZCBzdGFydHVwIGlzIGhlbHBpbmcgc3R1ZGVudHMgcGVyZm9ybSBiZXR0ZXIgaW4gZXhhbXMnLFxuICAgIGF1dGhvcjogJ0J5IFRoaW5rIENoYW5nZSBJbmRpYScsXG4gICAgY29udGVudDpcbiAgICAgICdTdGFydGVkIGFzIGFuIGVkdXRlY2ggc3RhcnR1cCBhbmQgYmFzZWQgb3V0IG9mIFQtSHViLCBIeWRlcmFiYWQsIEVnbmlmeSBlbXBsb3lzIGRlZXAgbGVhcm5pbmcsIE1hY2hpbmUgTGVhcm5pbmcgYW5kIGRhdGEgc2NpZW5jZSB0byBtYWtlIGEga2VlbiBhbmFseXNpcyBvZiB0aGUgcGVyZm9ybWFuY2Ugb2Ygc3R1ZGVudHMuIEJhc2VkIG9uIHRoaXMgcmVwb3J0LCB0aGUgc3RhcnR1cCBnZXRzIHRvIGRlc2NyaWJlIHdoZXJlIHRoZSBzdHVkZW50cyBuZWVkIHRvIGltcHJvdmUgdXBvbiwgYWxvbmcgd2l0aCBhIGRldGFpbGVkIGJyZWFrZG93biBvZiB0aGVpciBtYXJrcy4nLFxuICAgIHVybDogJ2h0dHBzOi8veW91cnN0b3J5LmNvbS8yMDE3LzEyL2h5ZGVyYWJhZC1zdGFydHVwLXN0dWRlbnRzLWV4YW0vJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL2luZGlhbl9leHByZXNzLTEucG5nJyxcbiAgICB0aXRsZTogJ0ltcGV0dXMgdG8gZXhjZWwnLFxuICAgIGF1dGhvcjogJ0J5IFNoeWFtIFlhZGFnaXJpLCBFeHByZXNzIE5ld3MgU2VydmljZScsXG4gICAgY29udGVudDpcbiAgICAgICdGb3VuZGVkIGluIDIwMTUgYnkgS2lyYW4gQmFidSwgRWduaWZ5IGlzIGEgY2xvdWQtYmFzZWQgcGx1Zy1hbmQtcGxheSBhbmFseXRpY3MgYW5kIGFzc2Vzc21lbnRzIHBsYXRmb3JtIGZvciBlZHVjYXRpb25hbCBpbnN0aXR1dGlvbnMuJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cDovL3d3dy5uZXdpbmRpYW5leHByZXNzLmNvbS9jaXRpZXMvaHlkZXJhYmFkLzIwMTcvc2VwLzE5L2ltcGV0dXMtdG8tZXhjZWwtMTY1OTY5OS5odG1sJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL2FzaWEtMS5wbmcnLFxuICAgIHRpdGxlOlxuICAgICAgJ0VnbmlmeSBJbXByb3ZpbmcgRWR1Y2F0aW9uIHVzaW5nIHRoZSBkZWVwLXRlY2ggYW5hbHl0aWNzOiBQZXJmb3JtYW5jZScsXG4gICAgYXV0aG9yOiAnQVNJQSBJTkMuNTAwJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0ZvdW5kZWQgaW4gMjAxNSBieSBLaXJhbiBCYWJ1LCBFZ25pZnkgaXMgYSBjbG91ZC1iYXNlZCBBc3Nlc3NtZW50IGFuZCBMZWFybmluZyBBbmFseXRpY3MgcGxhdGZvcm0gaW50ZWdyYXRlZCB3aXRoIHdvcmxkLWNsYXNzIEFuYWx5dGljcyB0byBlbmhhbmNlIGNvbmNlcHR1YWwgY2xhcml0eSBhbmQgRXhhbSBSZWFkaW5lc3Mgb2YgdGhlIHN0dWRlbnQuIEl0IGlzIGEgdGVhbSBvZiAyMCBwZW9wbGUgKElJVHMsIElJSVRzLCBJSU1zLCBOSVRzLCBTdGFuZm9yZCkgc2VydmluZyA0LDI3LDAwMCBzdHVkZW50cywgMjgsMDAwKyB0ZWFjaGVycyBpbiA0MDArIGluc3RpdHV0ZXMgYWNyb3NzIDI4IGNpdGllcyBpbiA0IHN0YXRlcy4gT3VyIHRhcmdldCBpcyB0byByZWFjaCAxMCBsYWtoIHN0dWRlbnRzIGJ5IGVuZCBvZiB0aGUgYWNhZGVtaWMgeWVhciAyMDE4LicsXG4gICAgdXJsOlxuICAgICAgJ2h0dHA6Ly93d3cuYXNpYWluYzUwMC5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTgvMTAvQXNpYS1JbmMuLTUwMC1NYWdhemluZS1PY3QtMjAxOC1EOTEucGRmJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3ByZXNzL2RlY2Nhbi0xLnBuZycsXG4gICAgdGl0bGU6ICdQaWxvdCBwcm9qZWN0IHRvIG1vdWxkIHN0YXRlIHNjaG9vbCBraWRzIGluIFRlbGFuZ2FuYScsXG4gICAgYXV0aG9yOiAnREVDQ0FOIENIUk9OSUNMRS4gfCBOQVZFRU5BIEdIQU5BVEUnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSHlkZXJhYmFkOiBFbXVsYXRpbmcgdGhlIG1vZGVsIGZvbGxvd2VkIGJ5IHByaXZhdGUgaW5zdGl0dXRpb25zLCB0aGUgVGVsYW5nYW5hIHN0YXRlIGdvdmVybm1lbnQgaXMgcnVubmluZyBhIHBpbG90IHByb2plY3QgdG8gc3RyZW5ndGhlbiB0aGUgd2VhayBhcmVhcyBvZiBzdHVkZW50cyBpbiBnb3Zlcm5tZW50IHNjaG9vbHMgdGhyb3VnaCB0ZWNobm9sb2d5LiBUaGlzIHdvdWxkIGV2ZW50dWFsbHkgcmFpc2UgdGhlIHBhc3MgcGVyY2VudGFnZS4nLFxuICAgIHVybDpcbiAgICAgICdodHRwczovL3d3dy5kZWNjYW5jaHJvbmljbGUuY29tL25hdGlvbi9jdXJyZW50LWFmZmFpcnMvMTIwMTE4L3BpbG90LXByb2plY3QtdG8tbW91bGQtc3RhdGUtc2Nob29sLWtpZHMtaW4tdGVsYW5nYW5hLmh0bWwnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcHJlc3MvdGhlX25ld3NfbWludXRlLTEucG5nJyxcbiAgICB0aXRsZTpcbiAgICAgICdUZWxhbmdhbmEgdGllcyB1cCB3aXRoIFQtSHViIHN0YXJ0dXAgRWduaWZ5IHRvIGltcHJvdmUgZWR1Y2F0aW9uIGluIGdvdnQgc2Nob29scycsXG4gICAgYXV0aG9yOiAnU2hpbHBhIFMgUmFuaXBldGEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnVGhlIFRlbGFuZ2FuYSBnb3Zlcm5tZW50IGlzIGxvb2tpbmcgdG8gaW5jcmVhc2UgcXVhbGl0eSBvZiBlZHVjYXRpb24gaW4gZ292ZXJubWVudCBzY2hvb2xzIGFjcm9zcyB0aGUgc3RhdGUsIGFuZCB3YW50cyB0byB1c2UgdGVjaG5vbG9neSBmb3IgdGhpcy4gSW4gdGhlIGxhdGVzdCBkZXZlbG9wbWVudCwgaXQgd2FudHMgdG8gbWVhc3VyZSB0aGUgc3RyZW5ndGhzIGFuZCB3ZWFrbmVzc2VzIG9mIHN0dWRlbnRzIGluIGdvdmVybm1lbnQgc2Nob29scyBhY3Jvc3Mgc3ViamVjdHMsIGFzc2VzcyBpdCBhbmQgbG9vayBmb3Igd2F5cyB0byBpbXByb3ZlIHRoZSBzYW1lIHVzaW5nIHRlY2hub2xvZ3kuJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly93d3cudGhlbmV3c21pbnV0ZS5jb20vYXJ0aWNsZS90ZWxhbmdhbmEtdGllcy10LWh1Yi1zdGFydHVwLWVnbmlmeS1pbXByb3ZlLWVkdWNhdGlvbi1nb3Z0LXNjaG9vbHMtNzQ2NTInLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvcHJlc3MvdGVsYW5nYW5hX3RvZGF5LTEucG5nJyxcbiAgICB0aXRsZTogJ0h5ZGVyYWJhZC1iYXNlZCBzdGFydHVwIEVnbmlmeSBUZWNobm9sb2dpZXMgaGVscHMgc3R1ZGVudHMgZXhjZWwnLFxuICAgIGF1dGhvcjogJ0J5IFNydXRpIFZlbnVnb3BhbCcsXG4gICAgY29udGVudDpcbiAgICAgICdXb3JraW5nIG91dCBvZiBULUh1YiwgdGhlIHN0YXJ0dXAgaXMgYSBjbG91ZC1iYXNlZCBob2xpc3RpYyBzdHVkZW50IHBlcmZvcm1hbmNlIGltcHJvdmVtZW50IHBsYXRmb3JtIHRoYXQgdGhyb3VnaCBkYXRhIGFuZCBhcnRpZmljaWFsIGludGVsbGlnZW5jZSBtZWFzdXJlcyB0aGUgd2Vha25lc3NlcyBhbmQgc3RyZW5ndGhzIG9mIGEgc3R1ZGVudCBhbmQgcHJvdmlkZXMgc3BlY2lmaWMgc29sdXRpb24uJyxcbiAgICB1cmw6XG4gICAgICAnaHR0cHM6Ly90ZWxhbmdhbmF0b2RheS5jb20vaHlkZXJhYmFkLWJhc2VkLXN0YXJ0dXAtZWduaWZ5LXRlY2hub2xvZ2llcy1oZWxwcy1zdHVkZW50cy1leGNlbCcsXG4gIH0sXG4gIHtcbiAgICBpY29uOiAnL2ltYWdlcy9wcmVzcy9hbmRocmFqeW90aGktMS5wbmcnLFxuICAgIHRpdGxlOiAn4LCu4LC+4LCw4LGN4LCV4LGB4LCyIOCwteCwv+CwtuCxjeCwsuCxh+Cwt+Cwo+CwleCxgiDgsJMg4LCf4LGG4LCV4LGN4LCo4LC/4LCV4LGN4oCMJyxcbiAgICBjb250ZW50OlxuICAgICAgJ+CwquCwsOCxgOCwleCxjeCwt+CwsuCxiyDgsJLgsJUg4LC14LC/4LCm4LGN4LCv4LC+4LCw4LGN4LCl4LC/4LCV4LC/IDUyIOCwruCwvuCwsOCxjeCwleCxgeCwsuCxgSDgsLXgsJrgsY3gsJrgsL7gsK/gsL8uIOCwuOCwsOCxhywg4LCu4LCw4LC/IOCwpOCwpOCwv+CwruCwviA0OCDgsK7gsL7gsLDgsY3gsJXgsYHgsLLgsYEg4LCO4LCC4LCm4LGB4LCV4LGBIOCwsOCwvuCwsuCxh+CwpuCxgSDgsIXgsKjgsY3gsKjgsKbgsL8g4LC14LGG4LCC4LCf4LCo4LGHIOCwleCwsuCwl+CwvuCwsuCxjeCwuOCwv+CwqCDgsLjgsILgsKbgsYfgsLngsIIuIOCwjuCwleCxjeCwleCwoSDgsLXgsYDgsJXgsY3igIzgsJfgsL4g4LCJ4LCo4LGN4LCo4LC+4LCw4LGLIOCwpOCxhuCwsuCwv+CwuOCxjeCwpOCxhyDgsLjgsLXgsLDgsL/gsILgsJrgsYHgsJXgsYvgsLXgsJrgsY3gsJrgsYEuIOCwpOCwpuCxjeCwteCwvuCwsOCwviDgsK7gsL7gsLDgsY3gsJXgsYHgsLLgsKjgsYEg4LCq4LGG4LCC4LCa4LGB4LCV4LGL4LC14LCa4LGN4LCa4LGBLiDgsKbgsYDgsKjgsY3gsKjgsL8g4LCf4LGG4LCV4LGN4LCo4LC+4LCy4LCc4LGAIOCwuOCwueCwleCwvuCwsOCwguCwpOCxiyDgsLXgsL/gsKbgsY3gsK/gsL7gsLDgsY3gsKXgsL8g4LCq4LGN4LCw4LCk4LC/4LCt4LCo4LGBIOCwteCwv+CwtuCxjeCwsuCxh+Cwt+Cwv+CwguCwmuCxhyDgsKrgsKjgsL/gsKjgsL8g4LCa4LGH4LCq4LCf4LGN4LCf4LC/4LCoIOCwuOCwguCwuOCxjeCwpSDgsI7gsJfgsY3gsKjgsL/gsKvgsYguJyxcbiAgICB1cmw6ICdodHRwOi8vd3d3LmFuZGhyYWp5b3RoeS5jb20vYXJ0aWNhbD9TSUQ9NDYxODI3JyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBQUkVTU19WSURFT19DT1ZFUkFHRSA9IFtcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9ldHZfdGVsYW5nYW5hLndlYnAnLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3MvZXR2X3RlbGFuZ2FuYS0xLnBuZycsXG4gICAgdXJsOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj13dVpjZVlScjNfTScsXG4gICAgdGl0bGU6XG4gICAgICAnR292dCBUaWVzIFVwIHdpdGggR2V0cmFua3MgYnkgRWduaWZ5IFN0YXJ0dXAgdG8gQW5hbHl6ZSBTY2hvb2wgU3R1ZGVudHMgTGFnZ2luZyBCZWhpbmQgaW4gRWR1Y2F0aW9uJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9raXJhbl9zaXJfb25fVCAyMG5ld3Mud2VicCcsXG4gICAgaWNvbjE6ICcvaW1hZ2VzL2hvbWUvbWVkaWEvb25fbmV3cy9raXJhbl9zaXJfb25fVCBuZXdzLTEucG5nJyxcbiAgICB0aXRsZTogJ0dldHJhbmtzIGJ5IEVnbmlmeV9DRU8gS2lyYW4gQmFidV9IeWRlcmFiYWRfVE5ld3NfVCBIdWInLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9T2JzelA5dWpVeDQnLFxuICB9LFxuICB7XG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9tZWRpYS9vbl9uZXdzL3N0dWRlbnRzX29uX3Y2LndlYnAnLFxuICAgIGljb24xOiAnL2ltYWdlcy9ob21lL21lZGlhL29uX25ld3Mvc3R1ZGVudHNfb25fdjYtMS5wbmcnLFxuICAgIHRpdGxlOiAnU3BlY2lhbCBTdG9yeSBvbiBHZXRyYW5rcyBieSBFZ25pZnkgaW4gSHlkZXJhYmFkIFY2IE5ld3MnLFxuICAgIHVybDogJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9aG1YMWp5emF0OVEnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IFZJRVdNT1JFID0gW1xuICB7XG4gICAgY29sb3I6ICcjZmY2NDAwJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL2Fycm93cy90ZWFjaC1hcnJvdy5zdmcnLFxuICB9LFxuICB7XG4gICAgY29sb3I6ICcjODgwMGZlJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL2Fycm93cy90ZXN0LWFycm93LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBjb2xvcjogJyMwMGI4MDAnLFxuICAgIGljb246ICcvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvYXJyb3dzL2Nvbm5lY3QtYXJyb3cuc3ZnJyxcbiAgfSxcbiAge1xuICAgIGNvbG9yOiAnI2JlMDBjYycsXG4gICAgaWNvbjogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9hcnJvd3MvY29sbGVjdC1hcnJvdy5zdmcnLFxuICB9LFxuICB7XG4gICAgY29sb3I6ICcjZTVhYzAwJyxcbiAgICBpY29uOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL2Fycm93cy9tYW5hZ2UtYXJyb3cuc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBURVNUU19LRVlfUE9JTlRTID0gW1xuICB7XG4gICAgaWQ6IDEsXG4gICAgcG9pbnQ6ICdVbmxpbWl0dGVkIHRlc3QgY3JlYXRpb24nLFxuICB9LFxuICB7XG4gICAgaWQ6IDIsXG4gICAgcG9pbnQ6XG4gICAgICAnSW5zdGFudCB0ZXN0IHJlc3VsdCBhbmQgUmVwb3J0cyBhdmFpbGFibGUgb25saW5lICYgb2ZmbGluZSAtIFJhbmsgbGlzdCwgQW5hbHlzaXMsIGFuZCBSZXBvcnRzIGFmdGVyIGV2ZXJ5IHRlc3QuJyxcbiAgfSxcbiAge1xuICAgIGlkOiAzLFxuICAgIHBvaW50OiAnUHJlLWRlZmluZWQgbWFya2luZyBzY2hlbWVzJyxcbiAgfSxcbiAge1xuICAgIGlkOiA0LFxuICAgIHBvaW50OiAnQ3VzdG9tIG1hcmtpbmcgc2NoZW1lcycsXG4gIH0sXG4gIHtcbiAgICBpZDogNSxcbiAgICBwb2ludDpcbiAgICAgICdHZXRyYW5rcyBzdXBwb3J0cyBhbGwgMjIgdHlwZXMgb2YgSkVFIGF2YW5jZWQgTWFya2luZyBTY2hlbWVzIHN0YXJ0aW5nIGZyb20gMjAwOSAtIDIwMTknLFxuICB9LFxuICB7XG4gICAgaWQ6IDYsXG4gICAgcG9pbnQ6ICdHcmFjZSBwZXJpb2Qgb3B0aW9uIGF2YWlsYWJsZScsXG4gIH0sXG4gIHtcbiAgICBpZDogNyxcbiAgICBwb2ludDogJ0xpdmUgQXR0ZW5kYW5jZScsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgbmV3TW9kdWxlc0RhdGEgPSBbXG4gIHtcbiAgICBuYW1lOiAnTGl2ZScsXG4gICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9MaXZlLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnQXNzaWdubWVudHMnLFxuICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfQXNzaWdubWVudHMuc3ZnJyxcbiAgfSxcbiAge1xuICAgIG5hbWU6ICdEb3VidHMnLFxuICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfRG91YnRzLnN2ZycsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnVGVzdHMnLFxuICAgIGljb246ICdpbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L21vZHVsZV90ZXN0LnN2ZycsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnQ29ubmVjdCcsXG4gICAgaWNvbjogJ2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL0Nvbm5lY3QvbmV3X2Nvbm5lY3Quc3ZnJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBtb2R1bGVLZXlQb2ludHMgPSB7XG4gIHRlc3RzOiB7XG4gICAgcG9pbnRzOiBbXG4gICAgICAnVW5saW1pdGVkIHRlc3QgY3JlYXRpb24nLFxuICAgICAgJzEwMCwwMDAgc3R1ZGVudCBjb25jdXJyZW5jeScsXG4gICAgICAnQ29uZHVjdCBvbmxpbmUgYW5kIG9mZmxpbmUgdGVzdHMnLFxuICAgICAgJ0FkdmFuY2VkIFByb2N0b3JpbmcgZmVhdHVyZXMnLFxuICAgICAgJ1ByZS1kZWZpbmVkIG1hcmtpbmcgc2NoZW1hcyBhbmQgY3VzdG9tIG1hcmtpbmcgc2NoZW1hcycsXG4gICAgICAnR3JhY2UgcGVyaW9kIG9wdGlvbiBhdmFpYWxiZScsXG4gICAgICAnTGl2ZSBhdHRlbmRhbmNlIGRhc2hib2FyZCcsXG4gICAgXSxcbiAgfSxcbiAgYXNzaWdubWVudHM6IHtcbiAgICBwb2ludHM6IFtcbiAgICAgICdBZG1pbmlzdGVyIGFuZCBncmFkZSBvbmxpbmUgYW5kIGluLWNsYXNzIGFzc2Vzc21lbnRzJyxcbiAgICAgICdHcmFkZSBtdWx0aXBsZSBhc3Nlc3NtZW50IHR5cGVzLCBpbmNsdWRpbmcgaGFuZHdyaXR0ZW4gYXNzZXNzbWVudHMnLFxuICAgICAgJ0V2YWx1YXRlIHVzaW5nIG1vYmlsZSBhcHAgYW5kIGluIGEgd2ViIGJyb3dzZXInLFxuICAgICAgJ0ltcHJvdmUgc3R1ZGVudCBmZWVkYmFjayBsb29wIGFuZCBwcm92aWRlIGFuIG9wcG9ydHVuaXR5IHRvIGVucmljaCB0aGUgbGVhcm5pbmcgZXhwZXJpZW5jZSB0aHJvdWdoIGFuYWx5dGljcycsXG4gICAgXSxcbiAgfSxcbiAgZG91YnRzOiB7XG4gICAgcG9pbnRzOiBbXG4gICAgICAnRG91YnRzIGNhbiB0dXJuIHVwIGFueSB0aW1lJyxcbiAgICAgICdBc2sgdW50aWwgaXQgaXMgcmVzb2x2ZWQnLFxuICAgICAgJ0Fuc3dlciBkb3VidCBhbnl0aW1lLCBhbnl3aGVyZScsXG4gICAgICAnQWNjZXNzIGFuZCBBbmFseXNpcyBkb3VidHMnLFxuICAgIF0sXG4gIH0sXG4gIHNjaGVkdWxlOiB7XG4gICAgcG9pbnRzOiBbXG4gICAgICAnU2F2ZSB0aW1lIGFuZCBtYWtlIHRoZSBtb3N0IG9mIGV2ZXJ5IGRheScsXG4gICAgICBgQnJpbmcgeW91ciBzdHVkZW50LCB0ZWFjaGVyJ3MgdGltZXRhYmxlIHRvIGxpZmUgYW5kIG1ha2UgaXQgZWFzeSB0byBhY2Nlc3Mgd2hhdCdzIGFoZWFkIHdpdGggbGlua3MsIGF0dGFjaG1lbnRzIGFsbCBvdGhlciBkZXRhaWxzYCxcbiAgICAgICdBbGwgeW91ciBhY3Rpdml0aWVzIGluIG9uZSBwbGFjZScsXG4gICAgXSxcbiAgfSxcbn07XG5cbmV4cG9ydCBjb25zdCBjbG9uZURlZXBseSA9IG9iaiA9PiB7XG4gIGNvbnN0IG5ld09iaiA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkob2JqKSk7XG4gIHJldHVybiBuZXdPYmo7XG59O1xuXG5leHBvcnQgY29uc3QgSE9XX1dPUktTID0gW1xuICB7XG4gICAgdGl0bGU6ICdPbmJvYXJkaW5nJyxcbiAgICBjb250ZW50OiBgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC5gLFxuICAgIGltZzogYC9pbWFnZXMvVGVzdC9kZW1vLnBuZ2AsXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0luc3RpdHV0aW9uIFNldHVwJyxcbiAgICBjb250ZW50OiBgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC5gLFxuICAgIGltZzogYC9pbWFnZXMvVGVzdC9kZW1vLnBuZ2AsXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ0FkZCBIaWVyYXJjaGllcycsXG4gICAgY29udGVudDogYExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuYCxcbiAgICBpbWc6IGAvaW1hZ2VzL1Rlc3QvZGVtby5wbmdgLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdBZGQgU3R1ZGVudHMnLFxuICAgIGNvbnRlbnQ6IGBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LmAsXG4gICAgaW1nOiBgL2ltYWdlcy9UZXN0L2RlbW8ucG5nYCxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBIT1dfV09SS1NfVElUTEVTID0gW1xuICAnT25ib2FyZGluZycsXG4gICdJbnN0aXR1dGlvbiBTZXR1cCcsXG4gICdBZGQgSGllcmFyY2hpZXMnLFxuICAnQWRkIFN0dWRlbnRzJyxcbl07XG5cbmV4cG9ydCBjb25zdCBURVNUX1ZJREVPUyA9IFtcbiAge1xuICAgIHVybDogJy9pbWFnZXMvVGVzdC92aWRlb3MvQ29uZHVjdCBvZmZsaW5lLW9ubGluZSBUZXN0cy5tcDQnLFxuICAgIHRpdGxlOiAnQ29uZHVjdCBPZmZsaW5lLyBPbmxpbmUgVGVzdCcsXG4gIH0sXG4gIHtcbiAgICB1cmw6ICcvaW1hZ2VzL1Rlc3QvdmlkZW9zL0NyZWF0ZSBPd24gUXVlc3Rpb24gUGFwZXJzLm1wNCcsXG4gICAgdGl0bGU6ICdVcGxvYWQgT3duIFF1ZXN0aW9uIFBhcGVycycsXG4gIH0sXG4gIHtcbiAgICB1cmw6ICcvaW1hZ2VzL1Rlc3QvdmlkZW9zL1VwbG9hZCBvd24gcXVlc3Rpb24gcGFwZXIubXA0JyxcbiAgICB0aXRsZTogJ0NyZWF0ZSBPd24gUXVlc3Rpb24gUGFwZXJzJyxcbiAgfSxcbiAge1xuICAgIHVybDogJy9pbWFnZXMvVGVzdC92aWRlb3MvSW5kZXB0aCBBbmFseXNpcy5tcDQnLFxuICAgIHRpdGxlOiAnSW5kZXB0aCBBbmFseXNpcycsXG4gIH0sXG5dO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQU1BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFFQTtBQUNBO0FBRUE7QUFKQTtBQU9BO0FBQ0E7QUFFQTtBQUpBO0FBT0E7QUFDQTtBQUVBO0FBSkE7QUFRQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUVBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU9BO0FBRUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBWUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW5EQTtBQWtIQTtBQUVBO0FBQ0E7QUFFQTtBQUpBO0FBT0E7QUFDQTtBQUVBO0FBSkE7QUFPQTtBQUNBO0FBRUE7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVNBO0FBRUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBT0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBU0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUZBO0FBYUE7QUFDQTtBQUZBO0FBZUE7QUFDQTtBQUZBO0FBY0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFYQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQVRBO0FBYUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFyQkE7QUE4QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQWpDQTtBQTBDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQXJCQTtBQThCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFMQTtBQXJCQTtBQStCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUEzQkE7QUFvQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQXJCQTtBQThCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBakNBO0FBMENBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBckJBO0FBOEJBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBckJBO0FBOEJBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQTNCQTtBQXFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFQQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFOQTtBQVVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFQQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFOQTtBQVVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFQQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFOQTtBQVVBO0FBQ0E7QUFDQTtBQUVBO0FBTEE7QUFTQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBTUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFNQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQURBO0FBV0E7QUFDQTtBQURBO0FBUUE7QUFDQTtBQURBO0FBUUE7QUFDQTtBQURBO0FBNUJBO0FBcUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBT0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==