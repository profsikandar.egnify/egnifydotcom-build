(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Acads"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/acads/Acads.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Acads-root-3-heU {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Acads-intro-Iq0xD {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Acads-introLeftPane-Ffbam {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Acads-jeet-14gOb {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Acads-coachingInstitutes-2phRA {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Acads-introContent-3XnFF {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Acads-introRightPane-2C-70 {\n  padding-top: 5% !important;\n}\n\n.Acads-introRightPane-2C-70 img {\n    width: 100%;\n  }\n\n.Acads-statsDiv-1tAWc {\n  width: 100%;\n  background-color: #3e3e5f;\n  display: block;\n  height: 366px;\n}\n\n.Acads-eachDiv-3xxQk {\n  height: 100%;\n}\n\n.Acads-statValue-dcJDg {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  font-size: 48px;\n  color: #ffab00;\n}\n\n.Acads-statName-1_Y1- {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n\n.Acads-statImage-yK9D_ {\n  height: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: end;\n      align-items: flex-end;\n}\n\n.Acads-content-1LDCG {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Acads-contentImageDiv-2dR0P {\n  height: 136px;\n  width: 136px;\n}\n\n.Acads-screenImage-1lkol {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Acads-headingContent-3xvoa {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Acads-mainContent-2MdSe {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Acads-testimonialsTitle-2qWlZ {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Acads-testimonialsTitle-2qWlZ::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Acads-contentSubdiv-2ZMdJ {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.Acads-list-1sB8o {\n  margin-bottom: 12px;\n  list-style-type: disc !important;\n  margin-left: 4%;\n}\n\n@media only screen and (max-width: 960px) {\n  .Acads-root-3-heU {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Acads-statsDiv-1tAWc {\n    height: 100%;\n    width: 100%;\n    display: block;\n    text-align: center;\n    margin-bottom: 2%;\n  }\n\n    .Acads-statsDiv-1tAWc .Acads-eachDiv-3xxQk {\n      width: 100%;\n      margin-bottom: 10%;\n      margin-top: 10%;\n    }\n\n  .Acads-statValue-dcJDg {\n    font-size: 32px;\n    padding: 6%;\n    border-right: none !important;\n  }\n\n  .Acads-contentForSmall-1jI7D {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Acads-contentImageDivSmall-32b99 {\n    width: 100%;\n  }\n\n    .Acads-contentImageDivSmall-32b99 img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Acads-screenImageSmall-2lGUc {\n    text-align: center;\n  }\n\n  .Acads-headingContentSmall-19R8B {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Acads-mainContentSmall-7EZPQ {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n    text-align: left;\n  }\n\n  .Acads-testimonialsTitle-2qWlZ {\n    margin-bottom: 18%;\n  }\n\n  .Acads-testimonialsTitle-2qWlZ::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Acads-introLeftPane-Ffbam {\n    width: 100%;\n  }\n\n  .Acads-jeet-14gOb {\n    text-align: center;\n  }\n\n  .Acads-coachingInstitutes-2phRA {\n    text-align: center;\n  }\n\n  .Acads-introContent-3XnFF {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Acads-link-1gCaS {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .Acads-statValue-dcJDg {\n    font-size: 60px;\n    height: 25%;\n  }\n\n  .Acads-statsDiv-1tAWc {\n    margin-bottom: 40px;\n  }\n\n  .Acads-introContent-3XnFF {\n    width: 300px;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Acads-introContent-3XnFF {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/Acads.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,YAAY;EACZ,0BAA0B;EAC1B,eAAe;EACf,cAAc;CACf;;AAED;EACE,aAAa;CACd;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;MAChB,sBAAsB;CAC3B;;AAED;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,oBAAoB;EACpB,iCAAiC;EACjC,gBAAgB;CACjB;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,eAAe;IACf,mBAAmB;IACnB,kBAAkB;GACnB;;IAEC;MACE,YAAY;MACZ,mBAAmB;MACnB,gBAAgB;KACjB;;EAEH;IACE,gBAAgB;IAChB,YAAY;IACZ,8BAA8B;GAC/B;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;IAClB,iBAAiB;GAClB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;CACF;;AAED;EACE;IACE,gBAAgB;IAChB,YAAY;GACb;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,aAAa;GACd;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;CACF","file":"Acads.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.statsDiv {\n  width: 100%;\n  background-color: #3e3e5f;\n  display: block;\n  height: 366px;\n}\n\n.eachDiv {\n  height: 100%;\n}\n\n.statValue {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  font-size: 48px;\n  color: #ffab00;\n}\n\n.statName {\n  height: 30%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n\n.statImage {\n  height: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: end;\n      align-items: flex-end;\n}\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.list {\n  margin-bottom: 12px;\n  list-style-type: disc !important;\n  margin-left: 4%;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .statsDiv {\n    height: 100%;\n    width: 100%;\n    display: block;\n    text-align: center;\n    margin-bottom: 2%;\n  }\n\n    .statsDiv .eachDiv {\n      width: 100%;\n      margin-bottom: 10%;\n      margin-top: 10%;\n    }\n\n  .statValue {\n    font-size: 32px;\n    padding: 6%;\n    border-right: none !important;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n    text-align: left;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .statValue {\n    font-size: 60px;\n    height: 25%;\n  }\n\n  .statsDiv {\n    margin-bottom: 40px;\n  }\n\n  .introContent {\n    width: 300px;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Acads-root-3-heU",
	"intro": "Acads-intro-Iq0xD",
	"introLeftPane": "Acads-introLeftPane-Ffbam",
	"jeet": "Acads-jeet-14gOb",
	"coachingInstitutes": "Acads-coachingInstitutes-2phRA",
	"introContent": "Acads-introContent-3XnFF",
	"introRightPane": "Acads-introRightPane-2C-70",
	"statsDiv": "Acads-statsDiv-1tAWc",
	"eachDiv": "Acads-eachDiv-3xxQk",
	"statValue": "Acads-statValue-dcJDg",
	"statName": "Acads-statName-1_Y1-",
	"statImage": "Acads-statImage-yK9D_",
	"content": "Acads-content-1LDCG",
	"contentImageDiv": "Acads-contentImageDiv-2dR0P",
	"screenImage": "Acads-screenImage-1lkol",
	"headingContent": "Acads-headingContent-3xvoa",
	"mainContent": "Acads-mainContent-2MdSe",
	"testimonialsTitle": "Acads-testimonialsTitle-2qWlZ",
	"contentSubdiv": "Acads-contentSubdiv-2ZMdJ",
	"list": "Acads-list-1sB8o",
	"contentForSmall": "Acads-contentForSmall-1jI7D",
	"contentImageDivSmall": "Acads-contentImageDivSmall-32b99",
	"screenImageSmall": "Acads-screenImageSmall-2lGUc",
	"headingContentSmall": "Acads-headingContentSmall-19R8B",
	"mainContentSmall": "Acads-mainContentSmall-7EZPQ",
	"link": "Acads-link-1gCaS"
};

/***/ }),

/***/ "./src/routes/products/acads/Acads.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Acads_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/acads/Acads.scss");
/* harmony import */ var _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Acads_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/Acads.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







var STATS = [{
  icon: '/images/products/jeet/institute.svg',
  stat: '150+',
  name: 'INSTITUTES'
}, {
  icon: '/images/products/jeet/mortar-board.svg',
  stat: '100k+',
  name: 'STUDENTS'
}, {
  icon: '/images/products/jeet/india.svg',
  stat: '4',
  name: 'STATES'
}, {
  icon: '/images/products/jeet/city.svg',
  stat: '7',
  name: 'CITIES'
}];
var CONTENT = [{
  headerContent: 'Re-imagine progress reports',
  mainContent: ['Holistic, Insightful and Informative reports.', 'Generate reports in less than 5 min for a class.', 'Actionable information in an understandable format.', 'Facilitates productive dialogue among educators, parents & students.'],
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/acads/01-1x.png'
}, {
  headerContent: 'Go Beyond Grades',
  mainContent: ['Data-driven diagnosis to identify student learning needs and identify areas for targeted instruction.', 'Actionable insights to spot patterns, suggest actions and make predictions.', 'Track the progress of a single student, see the overall class performance and compare classes.'],
  icon: '/images/products/jeet/blub.svg',
  screenImage: '/images/products/acads/02-1x.png'
}, {
  headerContent: 'Performance Management System',
  mainContent: ['Formative Assessments Marks.', 'Summative Assessments Marks.', 'Attendance Management.'],
  icon: '/images/products/jeet/weights.svg',
  screenImage: '/images/products/acads/4-1x.png'
} // {
//   headerContent: 'Co-Scholastics',
//   mainContent: [
//     'Formative Assessments Marks',
//     'Summative Assessments Marks',
//     'Attendance Management',
//   ],
//   icon: '/images/products/jeet/group.svg',
//   screenImage: '/images/products/acads/4-1x.png',
// },
];
var SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

var Acads =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Acads, _React$Component);

  function Acads() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Acads);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Acads)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "makeAlignment", function (contentIndex, key) {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });

    return _this;
  }

  _createClass(Acads, [{
    key: "componentDidMount",
    // static propTypes = {
    //
    // };
    value: function componentDidMount() {
      react_ga__WEBPACK_IMPORTED_MODULE_3__["default"].initialize(window.App.googleTrackingId, {
        debug: false
      });
      react_ga__WEBPACK_IMPORTED_MODULE_3__["default"].pageview(window.location.href);
    }
    /**
     * @description Returns what alignment should the content take eg. flex-start flex end center
     * @param contentIndex,key
     * @author Sushruth
     * */

  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane, " col m4"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, "ACADS"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }, "For Schools"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, "ACADS is a performance management system for schools which helps improve exam performance and maximize learning outcomes of their students."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/request-demo",
        className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane, " col m8"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/products/acads/hero.svg",
        alt: "",
        height: "100%",
        width: "100%",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statsDiv, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }, STATS.map(function (stat, statIndex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.eachDiv, " col m3 "),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 154
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statImage,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 155
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: stat.icon,
          alt: "",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 156
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statValue,
          style: {
            borderRight: statIndex === 3 ? 'none' : 'solid 1px rgba(139, 139, 223, 0.3)'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 158
          },
          __self: this
        }, stat.stat, ' '), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.statName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 169
          },
          __self: this
        }, stat.name, " "));
      })), CONTENT.map(function (content, contentIndex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "row",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 174
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content, " row hide-on-xs"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 175
          },
          __self: this
        }, [0, 1].map(function (key) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: contentIndex % 2 === key ? "col m5 s12 ".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv) : "col m7 s12 ".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv),
            style: {
              justifyContent: _this2.makeAlignment(contentIndex, key)
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 177
            },
            __self: this
          }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 188
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 189
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.icon,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 190
            },
            __self: this
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 192
            },
            __self: this
          }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 195
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 196
            },
            __self: this
          }, content.mainContent.map(function (eachPoint) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
              className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.list,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 198
              },
              __self: this
            }, eachPoint);
          })))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 204
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.screenImage,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 205
            },
            __self: this
          })));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall, " row hide-on-m-and-up"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 211
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 212
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 213
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 215
          },
          __self: this
        }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 218
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 219
          },
          __self: this
        }, content.mainContent.map(function (eachPoint) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.list,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 221
            },
            __self: this
          }, eachPoint);
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 225
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.screenImage,
          width: "87.6%",
          height: "54.45%",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 226
          },
          __self: this
        }))));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        style: {
          paddingTop: '64px',
          marginBottom: '5%'
        },
        className: "row hide",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }, "Testimonials"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_4__["default"], {
        data: SLIDESHOW,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 241
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }));
      return view;
    }
  }]);

  return Acads;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Acads_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Acads));

/***/ }),

/***/ "./src/routes/products/acads/Acads.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/acads/Acads.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/acads/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Acads__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/acads/Acads.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/acads/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify',
              chunks: ['Acads'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Acads__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWNhZHMuY2h1bmsuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2FjYWRzL0FjYWRzLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9hY2Fkcy9BY2Fkcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2FjYWRzL0FjYWRzLnNjc3M/MzZjMiIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2FjYWRzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuQWNhZHMtcm9vdC0zLWhlVSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5BY2Fkcy1pbnRyby1JcTB4RCB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogMyU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTtcXG59XFxuXFxuLkFjYWRzLWludHJvTGVmdFBhbmUtRmZiYW0ge1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLkFjYWRzLWplZXQtMTRnT2Ige1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLkFjYWRzLWNvYWNoaW5nSW5zdGl0dXRlcy0ycGhSQSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uQWNhZHMtaW50cm9Db250ZW50LTNYbkZGIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcXG4gIHdpZHRoOiA0MjBweDtcXG59XFxuXFxuLkFjYWRzLWludHJvUmlnaHRQYW5lLTJDLTcwIHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uQWNhZHMtaW50cm9SaWdodFBhbmUtMkMtNzAgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuLkFjYWRzLXN0YXRzRGl2LTF0QVdjIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNlM2U1ZjtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgaGVpZ2h0OiAzNjZweDtcXG59XFxuXFxuLkFjYWRzLWVhY2hEaXYtM3h4UWsge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uQWNhZHMtc3RhdFZhbHVlLWRjSkRnIHtcXG4gIGhlaWdodDogMzAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgY29sb3I6ICNmZmFiMDA7XFxufVxcblxcbi5BY2Fkcy1zdGF0TmFtZS0xX1kxLSB7XFxuICBoZWlnaHQ6IDMwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLkFjYWRzLXN0YXRJbWFnZS15SzlEXyB7XFxuICBoZWlnaHQ6IDQwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBlbmQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xcbn1cXG5cXG4uQWNhZHMtY29udGVudC0xTERDRyB7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG59XFxuXFxuLkFjYWRzLWNvbnRlbnRJbWFnZURpdi0yZFIwUCB7XFxuICBoZWlnaHQ6IDEzNnB4O1xcbiAgd2lkdGg6IDEzNnB4O1xcbn1cXG5cXG4uQWNhZHMtc2NyZWVuSW1hZ2UtMWxrb2wge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDY4cHg7XFxufVxcblxcbi5BY2Fkcy1oZWFkaW5nQ29udGVudC0zeHZvYSB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLkFjYWRzLW1haW5Db250ZW50LTJNZFNlIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4uQWNhZHMtdGVzdGltb25pYWxzVGl0bGUtMnFXbFoge1xcbiAgaGVpZ2h0OiAzOHB4O1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxuICBwYWRkaW5nOiAyJTtcXG59XFxuXFxuLkFjYWRzLXRlc3RpbW9uaWFsc1RpdGxlLTJxV2xaOjphZnRlciB7XFxuICBjb250ZW50OiAnJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogMnB4O1xcbiAgYm9yZGVyLXRvcDogc29saWQgM3B4ICNlYzRjNmY7XFxuICBwYWRkaW5nLXRvcDogMTJweDtcXG59XFxuXFxuLkFjYWRzLWNvbnRlbnRTdWJkaXYtMlpNZEoge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4uQWNhZHMtbGlzdC0xc0I4byB7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgbGlzdC1zdHlsZS10eXBlOiBkaXNjICFpbXBvcnRhbnQ7XFxuICBtYXJnaW4tbGVmdDogNCU7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5BY2Fkcy1yb290LTMtaGVVIHtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLkFjYWRzLXN0YXRzRGl2LTF0QVdjIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLWJvdHRvbTogMiU7XFxuICB9XFxuXFxuICAgIC5BY2Fkcy1zdGF0c0Rpdi0xdEFXYyAuQWNhZHMtZWFjaERpdi0zeHhRayB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICAgIG1hcmdpbi10b3A6IDEwJTtcXG4gICAgfVxcblxcbiAgLkFjYWRzLXN0YXRWYWx1ZS1kY0pEZyB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgfVxcblxcbiAgLkFjYWRzLWNvbnRlbnRGb3JTbWFsbC0xakk3RCB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1jb250ZW50SW1hZ2VEaXZTbWFsbC0zMmI5OSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgICAuQWNhZHMtY29udGVudEltYWdlRGl2U21hbGwtMzJiOTkgaW1nIHtcXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xcbiAgICAgIHdpZHRoOiAxMDBweDtcXG4gICAgfVxcblxcbiAgLkFjYWRzLXNjcmVlbkltYWdlU21hbGwtMmxHVWMge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQWNhZHMtaGVhZGluZ0NvbnRlbnRTbWFsbC0xOVI4QiB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLkFjYWRzLW1haW5Db250ZW50U21hbGwtN0VaUFEge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIH1cXG5cXG4gIC5BY2Fkcy10ZXN0aW1vbmlhbHNUaXRsZS0ycVdsWiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC5BY2Fkcy10ZXN0aW1vbmlhbHNUaXRsZS0ycVdsWjo6YWZ0ZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0cHg7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLkFjYWRzLWludHJvTGVmdFBhbmUtRmZiYW0ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1qZWV0LTE0Z09iIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkFjYWRzLWNvYWNoaW5nSW5zdGl0dXRlcy0ycGhSQSB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1pbnRyb0NvbnRlbnQtM1huRkYge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgfVxcblxcbiAgLkFjYWRzLWxpbmstMWdDYVMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKGRldmljZS13aWR0aDogMTAyNHB4KSB7XFxuICAuQWNhZHMtc3RhdFZhbHVlLWRjSkRnIHtcXG4gICAgZm9udC1zaXplOiA2MHB4O1xcbiAgICBoZWlnaHQ6IDI1JTtcXG4gIH1cXG5cXG4gIC5BY2Fkcy1zdGF0c0Rpdi0xdEFXYyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuICAuQWNhZHMtaW50cm9Db250ZW50LTNYbkZGIHtcXG4gICAgd2lkdGg6IDMwMHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XFxuICAuQWNhZHMtaW50cm9Db250ZW50LTNYbkZGIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvYWNhZHMvQWNhZHMuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLDREQUE0RDtVQUNwRCxvREFBb0Q7Q0FDN0Q7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsMkJBQTJCO0VBQzNCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7QUFFRDtJQUNJLFlBQVk7R0FDYjs7QUFFSDtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixnQkFBZ0I7RUFDaEIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLG9CQUFvQjtNQUNoQixzQkFBc0I7Q0FDM0I7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixlQUFlO0VBQ2YsWUFBWTtFQUNaLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxvQkFBb0I7RUFDcEIsaUNBQWlDO0VBQ2pDLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztJQUNYLFVBQVU7R0FDWDs7RUFFRDtJQUNFLGFBQWE7SUFDYixZQUFZO0lBQ1osZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixrQkFBa0I7R0FDbkI7O0lBRUM7TUFDRSxZQUFZO01BQ1osbUJBQW1CO01BQ25CLGdCQUFnQjtLQUNqQjs7RUFFSDtJQUNFLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osOEJBQThCO0dBQy9COztFQUVEO0lBQ0UsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0lBRUM7TUFDRSxjQUFjO01BQ2QsYUFBYTtLQUNkOztFQUVIO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3QjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxnQkFBZ0I7SUFDaEIsWUFBWTtHQUNiOztFQUVEO0lBQ0Usb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsYUFBYTtHQUNkO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtHQUNsQjtDQUNGXCIsXCJmaWxlXCI6XCJBY2Fkcy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5yb290IHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbmJ1dHRvbiB7XFxuICBoZWlnaHQ6IDU2cHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWM0YzZmO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDJweCA0cHggMTJweCAwIHJnYmEoMjM2LCA3NiwgMTExLCAwLjI0KTtcXG59XFxuXFxuLmludHJvIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAzJTtcXG4gIG1hcmdpbi1ib3R0b206IDUlO1xcbn1cXG5cXG4uaW50cm9MZWZ0UGFuZSB7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uamVldCB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uY29hY2hpbmdJbnN0aXR1dGVzIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjhweDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5pbnRyb0NvbnRlbnQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgd2lkdGg6IDQyMHB4O1xcbn1cXG5cXG4uaW50cm9SaWdodFBhbmUge1xcbiAgcGFkZGluZy10b3A6IDUlICFpbXBvcnRhbnQ7XFxufVxcblxcbi5pbnRyb1JpZ2h0UGFuZSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4uc3RhdHNEaXYge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2UzZTVmO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDM2NnB4O1xcbn1cXG5cXG4uZWFjaERpdiB7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5zdGF0VmFsdWUge1xcbiAgaGVpZ2h0OiAzMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBjb2xvcjogI2ZmYWIwMDtcXG59XFxuXFxuLnN0YXROYW1lIHtcXG4gIGhlaWdodDogMzAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uc3RhdEltYWdlIHtcXG4gIGhlaWdodDogNDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGVuZDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XFxufVxcblxcbi5jb250ZW50IHtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbn1cXG5cXG4uY29udGVudEltYWdlRGl2IHtcXG4gIGhlaWdodDogMTM2cHg7XFxuICB3aWR0aDogMTM2cHg7XFxufVxcblxcbi5zY3JlZW5JbWFnZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNjhweDtcXG59XFxuXFxuLmhlYWRpbmdDb250ZW50IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4ubWFpbkNvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS42O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICBoZWlnaHQ6IDM4cHg7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG4gIHBhZGRpbmc6IDIlO1xcbn1cXG5cXG4udGVzdGltb25pYWxzVGl0bGU6OmFmdGVyIHtcXG4gIGNvbnRlbnQ6ICcnO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiAycHg7XFxuICBib3JkZXItdG9wOiBzb2xpZCAzcHggI2VjNGM2ZjtcXG4gIHBhZGRpbmctdG9wOiAxMnB4O1xcbn1cXG5cXG4uY29udGVudFN1YmRpdiB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5saXN0IHtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICBsaXN0LXN0eWxlLXR5cGU6IGRpc2MgIWltcG9ydGFudDtcXG4gIG1hcmdpbi1sZWZ0OiA0JTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NjBweCkge1xcbiAgLnJvb3Qge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuc3RhdHNEaXYge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW4tYm90dG9tOiAyJTtcXG4gIH1cXG5cXG4gICAgLnN0YXRzRGl2IC5lYWNoRGl2IHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgICAgbWFyZ2luLXRvcDogMTAlO1xcbiAgICB9XFxuXFxuICAuc3RhdFZhbHVlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICBwYWRkaW5nOiA2JTtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuICAuY29udGVudEZvclNtYWxsIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBwYWRkaW5nOiA2JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmNvbnRlbnRJbWFnZURpdlNtYWxsIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAgIC5jb250ZW50SW1hZ2VEaXZTbWFsbCBpbWcge1xcbiAgICAgIGhlaWdodDogMTAwcHg7XFxuICAgICAgd2lkdGg6IDEwMHB4O1xcbiAgICB9XFxuXFxuICAuc2NyZWVuSW1hZ2VTbWFsbCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5oZWFkaW5nQ29udGVudFNtYWxsIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAubWFpbkNvbnRlbnRTbWFsbCB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfVxcblxcbiAgLnRlc3RpbW9uaWFsc1RpdGxlIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTglO1xcbiAgfVxcblxcbiAgLnRlc3RpbW9uaWFsc1RpdGxlOjphZnRlciB7XFxuICAgIG1hcmdpbi10b3A6IDRweDtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XFxuICAuaW50cm9MZWZ0UGFuZSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLmplZXQge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuY29hY2hpbmdJbnN0aXR1dGVzIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmludHJvQ29udGVudCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gICAgcGFkZGluZy1yaWdodDogMTUlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICB9XFxuXFxuICAubGluayB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAoZGV2aWNlLXdpZHRoOiAxMDI0cHgpIHtcXG4gIC5zdGF0VmFsdWUge1xcbiAgICBmb250LXNpemU6IDYwcHg7XFxuICAgIGhlaWdodDogMjUlO1xcbiAgfVxcblxcbiAgLnN0YXRzRGl2IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMzAwcHg7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJyb290XCI6IFwiQWNhZHMtcm9vdC0zLWhlVVwiLFxuXHRcImludHJvXCI6IFwiQWNhZHMtaW50cm8tSXEweERcIixcblx0XCJpbnRyb0xlZnRQYW5lXCI6IFwiQWNhZHMtaW50cm9MZWZ0UGFuZS1GZmJhbVwiLFxuXHRcImplZXRcIjogXCJBY2Fkcy1qZWV0LTE0Z09iXCIsXG5cdFwiY29hY2hpbmdJbnN0aXR1dGVzXCI6IFwiQWNhZHMtY29hY2hpbmdJbnN0aXR1dGVzLTJwaFJBXCIsXG5cdFwiaW50cm9Db250ZW50XCI6IFwiQWNhZHMtaW50cm9Db250ZW50LTNYbkZGXCIsXG5cdFwiaW50cm9SaWdodFBhbmVcIjogXCJBY2Fkcy1pbnRyb1JpZ2h0UGFuZS0yQy03MFwiLFxuXHRcInN0YXRzRGl2XCI6IFwiQWNhZHMtc3RhdHNEaXYtMXRBV2NcIixcblx0XCJlYWNoRGl2XCI6IFwiQWNhZHMtZWFjaERpdi0zeHhRa1wiLFxuXHRcInN0YXRWYWx1ZVwiOiBcIkFjYWRzLXN0YXRWYWx1ZS1kY0pEZ1wiLFxuXHRcInN0YXROYW1lXCI6IFwiQWNhZHMtc3RhdE5hbWUtMV9ZMS1cIixcblx0XCJzdGF0SW1hZ2VcIjogXCJBY2Fkcy1zdGF0SW1hZ2UteUs5RF9cIixcblx0XCJjb250ZW50XCI6IFwiQWNhZHMtY29udGVudC0xTERDR1wiLFxuXHRcImNvbnRlbnRJbWFnZURpdlwiOiBcIkFjYWRzLWNvbnRlbnRJbWFnZURpdi0yZFIwUFwiLFxuXHRcInNjcmVlbkltYWdlXCI6IFwiQWNhZHMtc2NyZWVuSW1hZ2UtMWxrb2xcIixcblx0XCJoZWFkaW5nQ29udGVudFwiOiBcIkFjYWRzLWhlYWRpbmdDb250ZW50LTN4dm9hXCIsXG5cdFwibWFpbkNvbnRlbnRcIjogXCJBY2Fkcy1tYWluQ29udGVudC0yTWRTZVwiLFxuXHRcInRlc3RpbW9uaWFsc1RpdGxlXCI6IFwiQWNhZHMtdGVzdGltb25pYWxzVGl0bGUtMnFXbFpcIixcblx0XCJjb250ZW50U3ViZGl2XCI6IFwiQWNhZHMtY29udGVudFN1YmRpdi0yWk1kSlwiLFxuXHRcImxpc3RcIjogXCJBY2Fkcy1saXN0LTFzQjhvXCIsXG5cdFwiY29udGVudEZvclNtYWxsXCI6IFwiQWNhZHMtY29udGVudEZvclNtYWxsLTFqSTdEXCIsXG5cdFwiY29udGVudEltYWdlRGl2U21hbGxcIjogXCJBY2Fkcy1jb250ZW50SW1hZ2VEaXZTbWFsbC0zMmI5OVwiLFxuXHRcInNjcmVlbkltYWdlU21hbGxcIjogXCJBY2Fkcy1zY3JlZW5JbWFnZVNtYWxsLTJsR1VjXCIsXG5cdFwiaGVhZGluZ0NvbnRlbnRTbWFsbFwiOiBcIkFjYWRzLWhlYWRpbmdDb250ZW50U21hbGwtMTlSOEJcIixcblx0XCJtYWluQ29udGVudFNtYWxsXCI6IFwiQWNhZHMtbWFpbkNvbnRlbnRTbWFsbC03RVpQUVwiLFxuXHRcImxpbmtcIjogXCJBY2Fkcy1saW5rLTFnQ2FTXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0Jztcbi8vIGltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IFJlYWN0R0EgZnJvbSAncmVhY3QtZ2EnO1xuaW1wb3J0IFNsaWRlc2hvdyBmcm9tICdjb21wb25lbnRzL1NsaWRlc2hvdyc7XG5pbXBvcnQgUmVxdWVzdERlbW8gZnJvbSAnY29tcG9uZW50cy9SZXF1ZXN0RGVtbyc7XG5pbXBvcnQgcyBmcm9tICcuL0FjYWRzLnNjc3MnO1xuXG5jb25zdCBTVEFUUyA9IFtcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvaW5zdGl0dXRlLnN2ZycsXG4gICAgc3RhdDogJzE1MCsnLFxuICAgIG5hbWU6ICdJTlNUSVRVVEVTJyxcbiAgfSxcbiAge1xuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvbW9ydGFyLWJvYXJkLnN2ZycsXG4gICAgc3RhdDogJzEwMGsrJyxcbiAgICBuYW1lOiAnU1RVREVOVFMnLFxuICB9LFxuICB7IGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvaW5kaWEuc3ZnJywgc3RhdDogJzQnLCBuYW1lOiAnU1RBVEVTJyB9LFxuICB7IGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvY2l0eS5zdmcnLCBzdGF0OiAnNycsIG5hbWU6ICdDSVRJRVMnIH0sXG5dO1xuXG5jb25zdCBDT05URU5UID0gW1xuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ1JlLWltYWdpbmUgcHJvZ3Jlc3MgcmVwb3J0cycsXG4gICAgbWFpbkNvbnRlbnQ6IFtcbiAgICAgICdIb2xpc3RpYywgSW5zaWdodGZ1bCBhbmQgSW5mb3JtYXRpdmUgcmVwb3J0cy4nLFxuICAgICAgJ0dlbmVyYXRlIHJlcG9ydHMgaW4gbGVzcyB0aGFuIDUgbWluIGZvciBhIGNsYXNzLicsXG4gICAgICAnQWN0aW9uYWJsZSBpbmZvcm1hdGlvbiBpbiBhbiB1bmRlcnN0YW5kYWJsZSBmb3JtYXQuJyxcbiAgICAgICdGYWNpbGl0YXRlcyBwcm9kdWN0aXZlIGRpYWxvZ3VlIGFtb25nIGVkdWNhdG9ycywgcGFyZW50cyAmIHN0dWRlbnRzLicsXG4gICAgXSxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3BlcmNlbnQuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvYWNhZHMvMDEtMXgucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdHbyBCZXlvbmQgR3JhZGVzJyxcbiAgICBtYWluQ29udGVudDogW1xuICAgICAgJ0RhdGEtZHJpdmVuIGRpYWdub3NpcyB0byBpZGVudGlmeSBzdHVkZW50IGxlYXJuaW5nIG5lZWRzIGFuZCBpZGVudGlmeSBhcmVhcyBmb3IgdGFyZ2V0ZWQgaW5zdHJ1Y3Rpb24uJyxcbiAgICAgICdBY3Rpb25hYmxlIGluc2lnaHRzIHRvIHNwb3QgcGF0dGVybnMsIHN1Z2dlc3QgYWN0aW9ucyBhbmQgbWFrZSBwcmVkaWN0aW9ucy4nLFxuICAgICAgJ1RyYWNrIHRoZSBwcm9ncmVzcyBvZiBhIHNpbmdsZSBzdHVkZW50LCBzZWUgdGhlIG92ZXJhbGwgY2xhc3MgcGVyZm9ybWFuY2UgYW5kIGNvbXBhcmUgY2xhc3Nlcy4nLFxuICAgIF0sXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9ibHViLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2FjYWRzLzAyLTF4LnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnUGVyZm9ybWFuY2UgTWFuYWdlbWVudCBTeXN0ZW0nLFxuICAgIG1haW5Db250ZW50OiBbXG4gICAgICAnRm9ybWF0aXZlIEFzc2Vzc21lbnRzIE1hcmtzLicsXG4gICAgICAnU3VtbWF0aXZlIEFzc2Vzc21lbnRzIE1hcmtzLicsXG4gICAgICAnQXR0ZW5kYW5jZSBNYW5hZ2VtZW50LicsXG4gICAgXSxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3dlaWdodHMuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvYWNhZHMvNC0xeC5wbmcnLFxuICB9LFxuICAvLyB7XG4gIC8vICAgaGVhZGVyQ29udGVudDogJ0NvLVNjaG9sYXN0aWNzJyxcbiAgLy8gICBtYWluQ29udGVudDogW1xuICAvLyAgICAgJ0Zvcm1hdGl2ZSBBc3Nlc3NtZW50cyBNYXJrcycsXG4gIC8vICAgICAnU3VtbWF0aXZlIEFzc2Vzc21lbnRzIE1hcmtzJyxcbiAgLy8gICAgICdBdHRlbmRhbmNlIE1hbmFnZW1lbnQnLFxuICAvLyAgIF0sXG4gIC8vICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9ncm91cC5zdmcnLFxuICAvLyAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9hY2Fkcy80LTF4LnBuZycsXG4gIC8vIH0sXG5dO1xuXG5jb25zdCBTTElERVNIT1cgPSBbXG4gIHtcbiAgICBpbWFnZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL3RlbGFuZ2FuYS5wbmcnLFxuICAgIG5hbWU6ICdNci4gU3Jpbml2YXMgS2FubmFuJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIFNyaW5pdmFzIE11cnRoeScsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBBbmRyZXcgVGFnJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIE11c3NvbGluaScsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG5dO1xuXG5jbGFzcyBBY2FkcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIC8vIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gIC8vXG4gIC8vIH07XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgUmVhY3RHQS5pbml0aWFsaXplKHdpbmRvdy5BcHAuZ29vZ2xlVHJhY2tpbmdJZCwge1xuICAgICAgZGVidWc6IGZhbHNlLFxuICAgIH0pO1xuICAgIFJlYWN0R0EucGFnZXZpZXcod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBkZXNjcmlwdGlvbiBSZXR1cm5zIHdoYXQgYWxpZ25tZW50IHNob3VsZCB0aGUgY29udGVudCB0YWtlIGVnLiBmbGV4LXN0YXJ0IGZsZXggZW5kIGNlbnRlclxuICAgKiBAcGFyYW0gY29udGVudEluZGV4LGtleVxuICAgKiBAYXV0aG9yIFN1c2hydXRoXG4gICAqICovXG4gIG1ha2VBbGlnbm1lbnQgPSAoY29udGVudEluZGV4LCBrZXkpID0+IHtcbiAgICBpZiAoY29udGVudEluZGV4ICUgMiA9PT0ga2V5KSB7XG4gICAgICBpZiAoa2V5ID09PSAwKSB7XG4gICAgICAgIHJldHVybiAnZmxleC1zdGFydCc7XG4gICAgICB9XG4gICAgICByZXR1cm4gJ2ZsZXgtZW5kJztcbiAgICB9XG4gICAgcmV0dXJuICdjZW50ZXInO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB2aWV3ID0gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm9vdH0gcm93YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvfSByb3dgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb0xlZnRQYW5lfSBjb2wgbTRgfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmplZXR9PkFDQURTPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb2FjaGluZ0luc3RpdHV0ZXN9PkZvciBTY2hvb2xzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbnRyb0NvbnRlbnR9PlxuICAgICAgICAgICAgICBBQ0FEUyBpcyBhIHBlcmZvcm1hbmNlIG1hbmFnZW1lbnQgc3lzdGVtIGZvciBzY2hvb2xzIHdoaWNoIGhlbHBzXG4gICAgICAgICAgICAgIGltcHJvdmUgZXhhbSBwZXJmb3JtYW5jZSBhbmQgbWF4aW1pemUgbGVhcm5pbmcgb3V0Y29tZXMgb2YgdGhlaXJcbiAgICAgICAgICAgICAgc3R1ZGVudHMuXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL3JlcXVlc3QtZGVtb1wiIGNsYXNzTmFtZT17cy5saW5rfT5cbiAgICAgICAgICAgICAgPGJ1dHRvbj5SRVFVRVNUIEEgREVNTzwvYnV0dG9uPlxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvUmlnaHRQYW5lfSBjb2wgbThgfT5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9wcm9kdWN0cy9hY2Fkcy9oZXJvLnN2Z1wiXG4gICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgIGhlaWdodD1cIjEwMCVcIlxuICAgICAgICAgICAgICB3aWR0aD1cIjEwMCVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnN0YXRzRGl2fSByb3dgfT5cbiAgICAgICAgICB7U1RBVFMubWFwKChzdGF0LCBzdGF0SW5kZXgpID0+IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmVhY2hEaXZ9IGNvbCBtMyBgfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3RhdEltYWdlfT5cbiAgICAgICAgICAgICAgICA8aW1nIHNyYz17c3RhdC5pY29ufSBhbHQ9XCJcIiAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5zdGF0VmFsdWV9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlclJpZ2h0OlxuICAgICAgICAgICAgICAgICAgICBzdGF0SW5kZXggPT09IDNcbiAgICAgICAgICAgICAgICAgICAgICA/ICdub25lJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJ3NvbGlkIDFweCByZ2JhKDEzOSwgMTM5LCAyMjMsIDAuMyknLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7c3RhdC5zdGF0fXsnICd9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdGF0TmFtZX0+e3N0YXQubmFtZX0gPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHtDT05URU5ULm1hcCgoY29udGVudCwgY29udGVudEluZGV4KSA9PiAoXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmNvbnRlbnR9IHJvdyBoaWRlLW9uLXhzYH0+XG4gICAgICAgICAgICAgIHtbMCwgMV0ubWFwKGtleSA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgICAgY29udGVudEluZGV4ICUgMiA9PT0ga2V5XG4gICAgICAgICAgICAgICAgICAgICAgPyBgY29sIG01IHMxMiAke3MuY29udGVudFN1YmRpdn1gXG4gICAgICAgICAgICAgICAgICAgICAgOiBgY29sIG03IHMxMiAke3MuY29udGVudFN1YmRpdn1gXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogdGhpcy5tYWtlQWxpZ25tZW50KGNvbnRlbnRJbmRleCwga2V5KSxcbiAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge2NvbnRlbnRJbmRleCAlIDIgPT09IGtleSA/IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50SW1hZ2VEaXZ9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBhbHQ9XCJcIiBzcmM9e2NvbnRlbnQuaWNvbn0gLz5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nQ29udGVudH0+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Y29udGVudC5oZWFkZXJDb250ZW50fVxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1haW5Db250ZW50fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAge2NvbnRlbnQubWFpbkNvbnRlbnQubWFwKGVhY2hQb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT17cy5saXN0fT57ZWFjaFBvaW50fTwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zY3JlZW5JbWFnZX0+XG4gICAgICAgICAgICAgICAgICAgICAgPGltZyBhbHQ9XCJcIiBzcmM9e2NvbnRlbnQuc2NyZWVuSW1hZ2V9IC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmNvbnRlbnRGb3JTbWFsbH0gcm93IGhpZGUtb24tbS1hbmQtdXBgfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudEltYWdlRGl2U21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50Lmljb259IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nQ29udGVudFNtYWxsfT5cbiAgICAgICAgICAgICAgICB7Y29udGVudC5oZWFkZXJDb250ZW50fVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbkNvbnRlbnRTbWFsbH0+XG4gICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAge2NvbnRlbnQubWFpbkNvbnRlbnQubWFwKGVhY2hQb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9e3MubGlzdH0+e2VhY2hQb2ludH08L2xpPlxuICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNjcmVlbkltYWdlU21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICBzcmM9e2NvbnRlbnQuc2NyZWVuSW1hZ2V9XG4gICAgICAgICAgICAgICAgICB3aWR0aD1cIjg3LjYlXCJcbiAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjU0LjQ1JVwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSl9XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nVG9wOiAnNjRweCcsIG1hcmdpbkJvdHRvbTogJzUlJyB9fVxuICAgICAgICAgIGNsYXNzTmFtZT1cInJvdyBoaWRlXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RpbW9uaWFsc1RpdGxlfT5UZXN0aW1vbmlhbHM8L2Rpdj5cbiAgICAgICAgICA8U2xpZGVzaG93IGRhdGE9e1NMSURFU0hPV30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxSZXF1ZXN0RGVtbyAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgICByZXR1cm4gdmlldztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEFjYWRzKTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQWNhZHMuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9BY2Fkcy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQWNhZHMuc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IEFjYWRzIGZyb20gJy4vQWNhZHMnO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0VnbmlmeScsXG4gICAgY2h1bmtzOiBbJ0FjYWRzJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8QWNhZHMgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBTUE7QUFDQTtBQVRBO0FBWUE7QUFDQTtBQUtBO0FBQ0E7QUFSQTtBQVdBO0FBQ0E7QUFLQTtBQUNBO0FBUkE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF6Q0E7QUE0Q0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFRQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7Ozs7O0FBekJBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBOzs7Ozs7OztBQWVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaEJBO0FBb0JBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFLQTtBQUNBO0FBREE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBN0JBO0FBbUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFyREE7QUFnRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBOzs7O0FBbkpBO0FBQ0E7QUFxSkE7Ozs7Ozs7QUN6UEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFZQTs7OztBIiwic291cmNlUm9vdCI6IiJ9