(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["CancellationAndReturn"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".CancellationAndReturn-header-2bs9o {\n  width: 100%;\n  height: 240px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n}\n\n.CancellationAndReturn-headerText-32yBV {\n  font-size: 48px;\n  line-height: 62px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.CancellationAndReturn-body-2xpfB {\n  padding: 40px 196px 30px 260px;\n}\n\n.CancellationAndReturn-definitionWrapper-2As2f {\n  margin-bottom: 24px;\n}\n\n.CancellationAndReturn-heading-3CYAo {\n  font-size: 20px;\n  line-height: 32px;\n  font-weight: bold;\n  display: block;\n}\n\n.CancellationAndReturn-text-bengf {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  margin-bottom: 8px;\n}\n\n.CancellationAndReturn-pointWrapper-14eoT {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 8px;\n}\n\n.CancellationAndReturn-dotWrapper-3Z1hF {\n  width: 8px;\n  margin-right: 24px;\n}\n\n.CancellationAndReturn-dot-26bTv {\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background-color: #f36;\n  margin-top: 11px;\n}\n\n.CancellationAndReturn-infoPoint-3Cu6J {\n  line-height: 32px;\n  margin-bottom: 0;\n}\n\n.CancellationAndReturn-subpointsWrapper-20a4a {\n  margin-left: 24px;\n}\n\n.CancellationAndReturn-subHeading-26VqC {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  font-weight: bold;\n}\n\n.CancellationAndReturn-subpoint-FzgLL {\n  line-height: 28px;\n  margin-bottom: 0;\n}\n\n@media only screen and (max-width: 990px) {\n  .CancellationAndReturn-header-2bs9o {\n    height: 104px;\n  }\n\n  .CancellationAndReturn-headerText-32yBV {\n    text-align: center;\n    font-size: 24px;\n    line-height: 32px;\n    padding: 16px;\n  }\n\n  .CancellationAndReturn-body-2xpfB {\n    padding: 24px 16px 40px 24px;\n  }\n\n  .CancellationAndReturn-definitionWrapper-2As2f {\n    margin-bottom: 20px;\n  }\n\n  .CancellationAndReturn-heading-3CYAo {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .CancellationAndReturn-text-bengf {\n    font-size: 14px;\n    line-height: 24px;\n    margin-bottom: 4px;\n  }\n\n  .CancellationAndReturn-dot-26bTv {\n    margin-top: 8px;\n  }\n\n  .CancellationAndReturn-infoPoint-3Cu6J {\n    margin-bottom: 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss"],"names":[],"mappings":"AAAA;EACE,YAAY;EACZ,cAAc;EACd,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;CAC7D;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,+BAA+B;CAChC;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,eAAe;CAChB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,mBAAmB;EACnB,uBAAuB;EACvB,iBAAiB;CAClB;;AAED;EACE,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE;IACE,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,cAAc;GACf;;EAED;IACE,6BAA6B;GAC9B;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,iBAAiB;GAClB;CACF","file":"CancellationAndReturn.scss","sourcesContent":[".header {\n  width: 100%;\n  height: 240px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n}\n\n.headerText {\n  font-size: 48px;\n  line-height: 62px;\n  color: #fff;\n  font-weight: 600;\n}\n\n.body {\n  padding: 40px 196px 30px 260px;\n}\n\n.definitionWrapper {\n  margin-bottom: 24px;\n}\n\n.heading {\n  font-size: 20px;\n  line-height: 32px;\n  font-weight: bold;\n  display: block;\n}\n\n.text {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  margin-bottom: 8px;\n}\n\n.pointWrapper {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 8px;\n}\n\n.dotWrapper {\n  width: 8px;\n  margin-right: 24px;\n}\n\n.dot {\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background-color: #f36;\n  margin-top: 11px;\n}\n\n.infoPoint {\n  line-height: 32px;\n  margin-bottom: 0;\n}\n\n.subpointsWrapper {\n  margin-left: 24px;\n}\n\n.subHeading {\n  display: block;\n  font-size: 16px;\n  line-height: 28px;\n  font-weight: bold;\n}\n\n.subpoint {\n  line-height: 28px;\n  margin-bottom: 0;\n}\n\n@media only screen and (max-width: 990px) {\n  .header {\n    height: 104px;\n  }\n\n  .headerText {\n    text-align: center;\n    font-size: 24px;\n    line-height: 32px;\n    padding: 16px;\n  }\n\n  .body {\n    padding: 24px 16px 40px 24px;\n  }\n\n  .definitionWrapper {\n    margin-bottom: 20px;\n  }\n\n  .heading {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .text {\n    font-size: 14px;\n    line-height: 24px;\n    margin-bottom: 4px;\n  }\n\n  .dot {\n    margin-top: 8px;\n  }\n\n  .infoPoint {\n    margin-bottom: 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"header": "CancellationAndReturn-header-2bs9o",
	"headerText": "CancellationAndReturn-headerText-32yBV",
	"body": "CancellationAndReturn-body-2xpfB",
	"definitionWrapper": "CancellationAndReturn-definitionWrapper-2As2f",
	"heading": "CancellationAndReturn-heading-3CYAo",
	"text": "CancellationAndReturn-text-bengf",
	"pointWrapper": "CancellationAndReturn-pointWrapper-14eoT",
	"dotWrapper": "CancellationAndReturn-dotWrapper-3Z1hF",
	"dot": "CancellationAndReturn-dot-26bTv",
	"infoPoint": "CancellationAndReturn-infoPoint-3Cu6J",
	"subpointsWrapper": "CancellationAndReturn-subpointsWrapper-20a4a",
	"subHeading": "CancellationAndReturn-subHeading-26VqC",
	"subpoint": "CancellationAndReturn-subpoint-FzgLL"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/constants.js");
/* harmony import */ var _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss");
/* harmony import */ var _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var CancellationAndReturn =
/*#__PURE__*/
function (_Component) {
  _inherits(CancellationAndReturn, _Component);

  function CancellationAndReturn() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CancellationAndReturn);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CancellationAndReturn)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "displayHeader", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 8
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headerText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 9
        },
        __self: this
      }, "Cancellation and Return Policy"));
    });

    _defineProperty(_assertThisInitialized(_this), "displayDefinition", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.definitionWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 14
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 15
        },
        __self: this
      }, "Definition:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16
        },
        __self: this
      }, "'Return' is defined as the action of giving back the Product purchased by the Buyer to the Seller on the Website."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      }, "'Replacement' is the action or process of replacing something in place of another"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        },
        __self: this
      }, "A Buyer can request for return/replacement of the purchased Product only if:"), _constants__WEBPACK_IMPORTED_MODULE_2__["definitionPoints"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 29
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 30
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 31
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "".concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text, " ").concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoPoint),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 33
          },
          __self: this
        }, point));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayNotedPoints", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, "Points to be noted:"), _constants__WEBPACK_IMPORTED_MODULE_2__["notedPoints"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 44
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 45
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 46
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "".concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text, " ").concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoPoint),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 48
          },
          __self: this
        }, point.point)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subpointsWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 50
          },
          __self: this
        }, point.subHeading ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subHeading,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 52
          },
          __self: this
        }, point.subHeading) : null, point.subpoints ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 55
          },
          __self: this
        }, point.subpoints.map(function (subpoint) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.pointWrapper,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 57
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dotWrapper,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 58
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.dot,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 59
            },
            __self: this
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
            className: "".concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.text, " ").concat(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.subpoint),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 61
            },
            __self: this
          }, subpoint));
        })) : null));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayBody", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a.body,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _this.displayDefinition(), _this.displayNotedPoints());
    });

    return _this;
  }

  _createClass(CancellationAndReturn, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, this.displayHeader(), this.displayBody());
    }
  }]);

  return CancellationAndReturn;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_CancellationAndReturn_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(CancellationAndReturn));

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "definitionPoints", function() { return definitionPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notedPoints", function() { return notedPoints; });
var definitionPoints = ["The Product is unopened and unused", "The Product is defective/damaged", "Product(s) was/were missing", "Wrong Product was sent by the Seller", "Unsatisfactory Products - Inauthentic/Low Quality/Expired"];
var notedPoints = [{
  point: 'Seller can always accept the return/replacement irrespective of the policy.'
}, {
  point: "Some Products sold on the Website cannot be returned/replaced. We encourage the Buyer to review the listing before making the purchase decision. In case the Buyer orders a wrong item or orders an item that is not eligible for return/replacement, the Buyer shall not be entitled to any return/replacement/refund."
}, {
  point: "Buyer needs to return/request replacement of any purchased Product within, the later of, 3 days of the date of receipt of the Product or the return period identified on the Website for each Product listed therein.",
  subHeading: "For Returns:",
  subpoints: ["Except for Cash On Delivery transaction, refund, if any, shall be made at the same Issuing Bank from where Transaction Price was received.", "For Cash On Delivery transactions, refunds, if any, will be made through payment facility using NEFT / RTGS or any other online banking / electronic funds transfer system approved by Reserve Bank India (RBI), 10 business days after the necessary banking/account details are provided by the Buyer).", "Refund shall be made in Indian Rupees only and shall be equivalent to the Transaction Price received in Indian Rupees subject to any applicable adjustments.", "For electronic payments, refunds shall be made through payment facility using NEFT /RTGS or any other online banking / electronic funds transfer system approved by Reserve Bank India (RBI)."]
}, {
  point: "Refund/replacement shall be conditional and shall be with recourse available to Egnify Consultants in case of any misuse by Buyer."
}, {
  point: "Refund/replacement shall be subject to Buyer complying with Egnify Consultants Policies."
}, {
  point: "Refund/replacement shall be subject to Buyer complying with Egnify Consultants Policies.",
  subpoints: ["Buyer is asked for \"Reason for Return/Replacement\" - the Product is unopened and unused; the Product is defective/damaged; Product(s) was/were missing; wrong Product was sent by the Seller.", "An intimation shall be provided to the Seller seeking either \"approval\" or \"rejection\" of the request.", "In case the Seller accepts the return/replacement request, Buyer shall be required to return the product to the Seller and only after return of the Product in conformance with the conditions listed herein below, Seller shall be obliged to issue a refund to the Buyer or send a replacement Product to the Buyer, as applicable.", "Incase Seller rejects the return/replacement request, Buyer can choose to raise a dispute by writing to kiran@egnify.com giving details of the order and the return request. Egnify  shall endeavour to respond to such emails within 48 hours of receipt.", "In case the Seller doesn't pick up the item from the Buyer within three (3) days from the date of the return/replacement request, Buyer can choose to raise a dispute by writing to kiran@egnify.com giving details of the order and the request. Egnify shall endeavour to respond to such emails within 48 hours of receipt.", "For returns/replacement request being made by Buyer to the Seller of the product, the returned Product(s) should be unopened and \"UNUSED\" and returned with original packaging, otherwise the Buyer shall not be entitled to any refund of money or replacement of the Product (as applicable)"]
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/cancellationAndReturn/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _CancellationAndReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/cancellationAndReturn/CancellationAndReturn.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/cancellationAndReturn/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'GetRanks by Egnify: Assessment & Analytics Platform',
              chunks: ['CancellationAndReturn'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CancellationAndReturn__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuL0NhbmNlbGxhdGlvbkFuZFJldHVybi5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2NhbmNlbGxhdGlvbkFuZFJldHVybi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuL0NhbmNlbGxhdGlvbkFuZFJldHVybi5zY3NzPzFhMDIiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuL2NvbnN0YW50cy5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYW5jZWxsYXRpb25BbmRSZXR1cm4vaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5DYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGVyLTJiczlvIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAyNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IGJvdHRvbSwgbGVmdCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNlYTRjNzAsICNiMjQ1N2MpO1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRlclRleHQtMzJ5QlYge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgbGluZS1oZWlnaHQ6IDYycHg7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tYm9keS0yeHBmQiB7XFxuICBwYWRkaW5nOiA0MHB4IDE5NnB4IDMwcHggMjYwcHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tZGVmaW5pdGlvbldyYXBwZXItMkFzMmYge1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1oZWFkaW5nLTNDWUFvIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi10ZXh0LWJlbmdmIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tcG9pbnRXcmFwcGVyLTE0ZW9UIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1kb3RXcmFwcGVyLTNaMWhGIHtcXG4gIHdpZHRoOiA4cHg7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tZG90LTI2YlR2IHtcXG4gIHdpZHRoOiA4cHg7XFxuICBoZWlnaHQ6IDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBtYXJnaW4tdG9wOiAxMXB4O1xcbn1cXG5cXG4uQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWluZm9Qb2ludC0zQ3U2SiB7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxufVxcblxcbi5DYW5jZWxsYXRpb25BbmRSZXR1cm4tc3VicG9pbnRzV3JhcHBlci0yMGE0YSB7XFxuICBtYXJnaW4tbGVmdDogMjRweDtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1zdWJIZWFkaW5nLTI2VnFDIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLkNhbmNlbGxhdGlvbkFuZFJldHVybi1zdWJwb2ludC1GemdMTCB7XFxuICBsaW5lLWhlaWdodDogMjhweDtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5DYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGVyLTJiczlvIHtcXG4gICAgaGVpZ2h0OiAxMDRweDtcXG4gIH1cXG5cXG4gIC5DYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGVyVGV4dC0zMnlCViB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgcGFkZGluZzogMTZweDtcXG4gIH1cXG5cXG4gIC5DYW5jZWxsYXRpb25BbmRSZXR1cm4tYm9keS0yeHBmQiB7XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweCA0MHB4IDI0cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRlZmluaXRpb25XcmFwcGVyLTJBczJmIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC5DYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGluZy0zQ1lBbyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXRleHQtYmVuZ2Yge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XFxuICB9XFxuXFxuICAuQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRvdC0yNmJUdiB7XFxuICAgIG1hcmdpbi10b3A6IDhweDtcXG4gIH1cXG5cXG4gIC5DYW5jZWxsYXRpb25BbmRSZXR1cm4taW5mb1BvaW50LTNDdTZKIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jYW5jZWxsYXRpb25BbmRSZXR1cm4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxZQUFZO0VBQ1osY0FBYztFQUNkLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLDhGQUE4RjtFQUM5RixvRUFBb0U7RUFDcEUsK0RBQStEO0VBQy9ELDREQUE0RDtDQUM3RDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLCtCQUErQjtDQUNoQzs7QUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsV0FBVztFQUNYLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsY0FBYztHQUNmOztFQUVEO0lBQ0UsNkJBQTZCO0dBQzlCOztFQUVEO0lBQ0Usb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsaUJBQWlCO0dBQ2xCO0NBQ0ZcIixcImZpbGVcIjpcIkNhbmNlbGxhdGlvbkFuZFJldHVybi5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5oZWFkZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDI0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgYm90dG9tLCBsZWZ0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxufVxcblxcbi5oZWFkZXJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiA2MnB4O1xcbiAgY29sb3I6ICNmZmY7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uYm9keSB7XFxuICBwYWRkaW5nOiA0MHB4IDE5NnB4IDMwcHggMjYwcHg7XFxufVxcblxcbi5kZWZpbml0aW9uV3JhcHBlciB7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbn1cXG5cXG4uaGVhZGluZyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcblxcbi50ZXh0IHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5wb2ludFdyYXBwZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uZG90V3JhcHBlciB7XFxuICB3aWR0aDogOHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbn1cXG5cXG4uZG90IHtcXG4gIHdpZHRoOiA4cHg7XFxuICBoZWlnaHQ6IDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBtYXJnaW4tdG9wOiAxMXB4O1xcbn1cXG5cXG4uaW5mb1BvaW50IHtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMDtcXG59XFxuXFxuLnN1YnBvaW50c1dyYXBwZXIge1xcbiAgbWFyZ2luLWxlZnQ6IDI0cHg7XFxufVxcblxcbi5zdWJIZWFkaW5nIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLnN1YnBvaW50IHtcXG4gIGxpbmUtaGVpZ2h0OiAyOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLmhlYWRlciB7XFxuICAgIGhlaWdodDogMTA0cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyVGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgcGFkZGluZzogMTZweDtcXG4gIH1cXG5cXG4gIC5ib2R5IHtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4IDQwcHggMjRweDtcXG4gIH1cXG5cXG4gIC5kZWZpbml0aW9uV3JhcHBlciB7XFxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XFxuICB9XFxuXFxuICAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAudGV4dCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDRweDtcXG4gIH1cXG5cXG4gIC5kb3Qge1xcbiAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICB9XFxuXFxuICAuaW5mb1BvaW50IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImhlYWRlclwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1oZWFkZXItMmJzOW9cIixcblx0XCJoZWFkZXJUZXh0XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWhlYWRlclRleHQtMzJ5QlZcIixcblx0XCJib2R5XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWJvZHktMnhwZkJcIixcblx0XCJkZWZpbml0aW9uV3JhcHBlclwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1kZWZpbml0aW9uV3JhcHBlci0yQXMyZlwiLFxuXHRcImhlYWRpbmdcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4taGVhZGluZy0zQ1lBb1wiLFxuXHRcInRleHRcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tdGV4dC1iZW5nZlwiLFxuXHRcInBvaW50V3JhcHBlclwiOiBcIkNhbmNlbGxhdGlvbkFuZFJldHVybi1wb2ludFdyYXBwZXItMTRlb1RcIixcblx0XCJkb3RXcmFwcGVyXCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWRvdFdyYXBwZXItM1oxaEZcIixcblx0XCJkb3RcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tZG90LTI2YlR2XCIsXG5cdFwiaW5mb1BvaW50XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLWluZm9Qb2ludC0zQ3U2SlwiLFxuXHRcInN1YnBvaW50c1dyYXBwZXJcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tc3VicG9pbnRzV3JhcHBlci0yMGE0YVwiLFxuXHRcInN1YkhlYWRpbmdcIjogXCJDYW5jZWxsYXRpb25BbmRSZXR1cm4tc3ViSGVhZGluZy0yNlZxQ1wiLFxuXHRcInN1YnBvaW50XCI6IFwiQ2FuY2VsbGF0aW9uQW5kUmV0dXJuLXN1YnBvaW50LUZ6Z0xMXCJcbn07IiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCB7IGRlZmluaXRpb25Qb2ludHMsIG5vdGVkUG9pbnRzIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuaW1wb3J0IHMgZnJvbSAnLi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzcyc7XG5cbmNsYXNzIENhbmNlbGxhdGlvbkFuZFJldHVybiBleHRlbmRzIENvbXBvbmVudCB7XG4gIGRpc3BsYXlIZWFkZXIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyfT5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5oZWFkZXJUZXh0fT5DYW5jZWxsYXRpb24gYW5kIFJldHVybiBQb2xpY3k8L3NwYW4+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheURlZmluaXRpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuZGVmaW5pdGlvbldyYXBwZXJ9PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmhlYWRpbmd9PkRlZmluaXRpb246PC9zcGFuPlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHR9PlxuICAgICAgICAmYXBvcztSZXR1cm4mYXBvczsgaXMgZGVmaW5lZCBhcyB0aGUgYWN0aW9uIG9mIGdpdmluZyBiYWNrIHRoZSBQcm9kdWN0XG4gICAgICAgIHB1cmNoYXNlZCBieSB0aGUgQnV5ZXIgdG8gdGhlIFNlbGxlciBvbiB0aGUgV2Vic2l0ZS5cbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0fT5cbiAgICAgICAgJmFwb3M7UmVwbGFjZW1lbnQmYXBvczsgaXMgdGhlIGFjdGlvbiBvciBwcm9jZXNzIG9mIHJlcGxhY2luZyBzb21ldGhpbmdcbiAgICAgICAgaW4gcGxhY2Ugb2YgYW5vdGhlclxuICAgICAgPC9zcGFuPlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHR9PlxuICAgICAgICBBIEJ1eWVyIGNhbiByZXF1ZXN0IGZvciByZXR1cm4vcmVwbGFjZW1lbnQgb2YgdGhlIHB1cmNoYXNlZCBQcm9kdWN0IG9ubHlcbiAgICAgICAgaWY6XG4gICAgICA8L3NwYW4+XG4gICAgICB7ZGVmaW5pdGlvblBvaW50cy5tYXAocG9pbnQgPT4gKFxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFdyYXBwZXJ9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdFdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90fSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy50ZXh0fSAke3MuaW5mb1BvaW50fWB9Pntwb2ludH08L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheU5vdGVkUG9pbnRzID0gKCkgPT4gKFxuICAgIDxkaXY+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3MuaGVhZGluZ30+UG9pbnRzIHRvIGJlIG5vdGVkOjwvc3Bhbj5cbiAgICAgIHtub3RlZFBvaW50cy5tYXAocG9pbnQgPT4gKFxuICAgICAgICA8PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90fSAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MudGV4dH0gJHtzLmluZm9Qb2ludH1gfT57cG9pbnQucG9pbnR9PC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1YnBvaW50c1dyYXBwZXJ9PlxuICAgICAgICAgICAge3BvaW50LnN1YkhlYWRpbmcgPyAoXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zdWJIZWFkaW5nfT57cG9pbnQuc3ViSGVhZGluZ308L3NwYW4+XG4gICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgIHtwb2ludC5zdWJwb2ludHMgPyAoXG4gICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAge3BvaW50LnN1YnBvaW50cy5tYXAoc3VicG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRXcmFwcGVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90fSAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLnRleHR9ICR7cy5zdWJwb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgICAgICB7c3VicG9pbnR9XG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8Lz5cbiAgICAgICkpfVxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlCb2R5ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvZHl9PlxuICAgICAge3RoaXMuZGlzcGxheURlZmluaXRpb24oKX1cbiAgICAgIHt0aGlzLmRpc3BsYXlOb3RlZFBvaW50cygpfVxuICAgIDwvZGl2PlxuICApO1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAge3RoaXMuZGlzcGxheUhlYWRlcigpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5Qm9keSgpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShDYW5jZWxsYXRpb25BbmRSZXR1cm4pO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DYW5jZWxsYXRpb25BbmRSZXR1cm4uc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0NhbmNlbGxhdGlvbkFuZFJldHVybi5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImV4cG9ydCBjb25zdCBkZWZpbml0aW9uUG9pbnRzID0gW1xuICBgVGhlIFByb2R1Y3QgaXMgdW5vcGVuZWQgYW5kIHVudXNlZGAsXG4gIGBUaGUgUHJvZHVjdCBpcyBkZWZlY3RpdmUvZGFtYWdlZGAsXG4gIGBQcm9kdWN0KHMpIHdhcy93ZXJlIG1pc3NpbmdgLFxuICBgV3JvbmcgUHJvZHVjdCB3YXMgc2VudCBieSB0aGUgU2VsbGVyYCxcbiAgYFVuc2F0aXNmYWN0b3J5IFByb2R1Y3RzIC0gSW5hdXRoZW50aWMvTG93IFF1YWxpdHkvRXhwaXJlZGAsXG5dO1xuXG5leHBvcnQgY29uc3Qgbm90ZWRQb2ludHMgPSBbXG4gIHtcbiAgICBwb2ludDpcbiAgICAgICdTZWxsZXIgY2FuIGFsd2F5cyBhY2NlcHQgdGhlIHJldHVybi9yZXBsYWNlbWVudCBpcnJlc3BlY3RpdmUgb2YgdGhlIHBvbGljeS4nLFxuICB9LFxuICB7XG4gICAgcG9pbnQ6IGBTb21lIFByb2R1Y3RzIHNvbGQgb24gdGhlIFdlYnNpdGUgY2Fubm90IGJlIHJldHVybmVkL3JlcGxhY2VkLiBXZSBlbmNvdXJhZ2UgdGhlIEJ1eWVyIHRvIHJldmlldyB0aGUgbGlzdGluZyBiZWZvcmUgbWFraW5nIHRoZSBwdXJjaGFzZSBkZWNpc2lvbi4gSW4gY2FzZSB0aGUgQnV5ZXIgb3JkZXJzIGEgd3JvbmcgaXRlbSBvciBvcmRlcnMgYW4gaXRlbSB0aGF0IGlzIG5vdCBlbGlnaWJsZSBmb3IgcmV0dXJuL3JlcGxhY2VtZW50LCB0aGUgQnV5ZXIgc2hhbGwgbm90IGJlIGVudGl0bGVkIHRvIGFueSByZXR1cm4vcmVwbGFjZW1lbnQvcmVmdW5kLmAsXG4gIH0sXG4gIHtcbiAgICBwb2ludDogYEJ1eWVyIG5lZWRzIHRvIHJldHVybi9yZXF1ZXN0IHJlcGxhY2VtZW50IG9mIGFueSBwdXJjaGFzZWQgUHJvZHVjdCB3aXRoaW4sIHRoZSBsYXRlciBvZiwgMyBkYXlzIG9mIHRoZSBkYXRlIG9mIHJlY2VpcHQgb2YgdGhlIFByb2R1Y3Qgb3IgdGhlIHJldHVybiBwZXJpb2QgaWRlbnRpZmllZCBvbiB0aGUgV2Vic2l0ZSBmb3IgZWFjaCBQcm9kdWN0IGxpc3RlZCB0aGVyZWluLmAsXG4gICAgc3ViSGVhZGluZzogYEZvciBSZXR1cm5zOmAsXG4gICAgc3VicG9pbnRzOiBbXG4gICAgICBgRXhjZXB0IGZvciBDYXNoIE9uIERlbGl2ZXJ5IHRyYW5zYWN0aW9uLCByZWZ1bmQsIGlmIGFueSwgc2hhbGwgYmUgbWFkZSBhdCB0aGUgc2FtZSBJc3N1aW5nIEJhbmsgZnJvbSB3aGVyZSBUcmFuc2FjdGlvbiBQcmljZSB3YXMgcmVjZWl2ZWQuYCxcbiAgICAgIGBGb3IgQ2FzaCBPbiBEZWxpdmVyeSB0cmFuc2FjdGlvbnMsIHJlZnVuZHMsIGlmIGFueSwgd2lsbCBiZSBtYWRlIHRocm91Z2ggcGF5bWVudCBmYWNpbGl0eSB1c2luZyBORUZUIC8gUlRHUyBvciBhbnkgb3RoZXIgb25saW5lIGJhbmtpbmcgLyBlbGVjdHJvbmljIGZ1bmRzIHRyYW5zZmVyIHN5c3RlbSBhcHByb3ZlZCBieSBSZXNlcnZlIEJhbmsgSW5kaWEgKFJCSSksIDEwIGJ1c2luZXNzIGRheXMgYWZ0ZXIgdGhlIG5lY2Vzc2FyeSBiYW5raW5nL2FjY291bnQgZGV0YWlscyBhcmUgcHJvdmlkZWQgYnkgdGhlIEJ1eWVyKS5gLFxuICAgICAgYFJlZnVuZCBzaGFsbCBiZSBtYWRlIGluIEluZGlhbiBSdXBlZXMgb25seSBhbmQgc2hhbGwgYmUgZXF1aXZhbGVudCB0byB0aGUgVHJhbnNhY3Rpb24gUHJpY2UgcmVjZWl2ZWQgaW4gSW5kaWFuIFJ1cGVlcyBzdWJqZWN0IHRvIGFueSBhcHBsaWNhYmxlIGFkanVzdG1lbnRzLmAsXG4gICAgICBgRm9yIGVsZWN0cm9uaWMgcGF5bWVudHMsIHJlZnVuZHMgc2hhbGwgYmUgbWFkZSB0aHJvdWdoIHBheW1lbnQgZmFjaWxpdHkgdXNpbmcgTkVGVCAvUlRHUyBvciBhbnkgb3RoZXIgb25saW5lIGJhbmtpbmcgLyBlbGVjdHJvbmljIGZ1bmRzIHRyYW5zZmVyIHN5c3RlbSBhcHByb3ZlZCBieSBSZXNlcnZlIEJhbmsgSW5kaWEgKFJCSSkuYCxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgcG9pbnQ6IGBSZWZ1bmQvcmVwbGFjZW1lbnQgc2hhbGwgYmUgY29uZGl0aW9uYWwgYW5kIHNoYWxsIGJlIHdpdGggcmVjb3Vyc2UgYXZhaWxhYmxlIHRvIEVnbmlmeSBDb25zdWx0YW50cyBpbiBjYXNlIG9mIGFueSBtaXN1c2UgYnkgQnV5ZXIuYCxcbiAgfSxcbiAge1xuICAgIHBvaW50OiBgUmVmdW5kL3JlcGxhY2VtZW50IHNoYWxsIGJlIHN1YmplY3QgdG8gQnV5ZXIgY29tcGx5aW5nIHdpdGggRWduaWZ5IENvbnN1bHRhbnRzIFBvbGljaWVzLmAsXG4gIH0sXG4gIHtcbiAgICBwb2ludDogYFJlZnVuZC9yZXBsYWNlbWVudCBzaGFsbCBiZSBzdWJqZWN0IHRvIEJ1eWVyIGNvbXBseWluZyB3aXRoIEVnbmlmeSBDb25zdWx0YW50cyBQb2xpY2llcy5gLFxuICAgIHN1YnBvaW50czogW1xuICAgICAgYEJ1eWVyIGlzIGFza2VkIGZvciBcIlJlYXNvbiBmb3IgUmV0dXJuL1JlcGxhY2VtZW50XCIgLSB0aGUgUHJvZHVjdCBpcyB1bm9wZW5lZCBhbmQgdW51c2VkOyB0aGUgUHJvZHVjdCBpcyBkZWZlY3RpdmUvZGFtYWdlZDsgUHJvZHVjdChzKSB3YXMvd2VyZSBtaXNzaW5nOyB3cm9uZyBQcm9kdWN0IHdhcyBzZW50IGJ5IHRoZSBTZWxsZXIuYCxcbiAgICAgIGBBbiBpbnRpbWF0aW9uIHNoYWxsIGJlIHByb3ZpZGVkIHRvIHRoZSBTZWxsZXIgc2Vla2luZyBlaXRoZXIgXCJhcHByb3ZhbFwiIG9yIFwicmVqZWN0aW9uXCIgb2YgdGhlIHJlcXVlc3QuYCxcbiAgICAgIGBJbiBjYXNlIHRoZSBTZWxsZXIgYWNjZXB0cyB0aGUgcmV0dXJuL3JlcGxhY2VtZW50IHJlcXVlc3QsIEJ1eWVyIHNoYWxsIGJlIHJlcXVpcmVkIHRvIHJldHVybiB0aGUgcHJvZHVjdCB0byB0aGUgU2VsbGVyIGFuZCBvbmx5IGFmdGVyIHJldHVybiBvZiB0aGUgUHJvZHVjdCBpbiBjb25mb3JtYW5jZSB3aXRoIHRoZSBjb25kaXRpb25zIGxpc3RlZCBoZXJlaW4gYmVsb3csIFNlbGxlciBzaGFsbCBiZSBvYmxpZ2VkIHRvIGlzc3VlIGEgcmVmdW5kIHRvIHRoZSBCdXllciBvciBzZW5kIGEgcmVwbGFjZW1lbnQgUHJvZHVjdCB0byB0aGUgQnV5ZXIsIGFzIGFwcGxpY2FibGUuYCxcbiAgICAgIGBJbmNhc2UgU2VsbGVyIHJlamVjdHMgdGhlIHJldHVybi9yZXBsYWNlbWVudCByZXF1ZXN0LCBCdXllciBjYW4gY2hvb3NlIHRvIHJhaXNlIGEgZGlzcHV0ZSBieSB3cml0aW5nIHRvIGtpcmFuQGVnbmlmeS5jb20gZ2l2aW5nIGRldGFpbHMgb2YgdGhlIG9yZGVyIGFuZCB0aGUgcmV0dXJuIHJlcXVlc3QuIEVnbmlmeSAgc2hhbGwgZW5kZWF2b3VyIHRvIHJlc3BvbmQgdG8gc3VjaCBlbWFpbHMgd2l0aGluIDQ4IGhvdXJzIG9mIHJlY2VpcHQuYCxcbiAgICAgIGBJbiBjYXNlIHRoZSBTZWxsZXIgZG9lc24ndCBwaWNrIHVwIHRoZSBpdGVtIGZyb20gdGhlIEJ1eWVyIHdpdGhpbiB0aHJlZSAoMykgZGF5cyBmcm9tIHRoZSBkYXRlIG9mIHRoZSByZXR1cm4vcmVwbGFjZW1lbnQgcmVxdWVzdCwgQnV5ZXIgY2FuIGNob29zZSB0byByYWlzZSBhIGRpc3B1dGUgYnkgd3JpdGluZyB0byBraXJhbkBlZ25pZnkuY29tIGdpdmluZyBkZXRhaWxzIG9mIHRoZSBvcmRlciBhbmQgdGhlIHJlcXVlc3QuIEVnbmlmeSBzaGFsbCBlbmRlYXZvdXIgdG8gcmVzcG9uZCB0byBzdWNoIGVtYWlscyB3aXRoaW4gNDggaG91cnMgb2YgcmVjZWlwdC5gLFxuICAgICAgYEZvciByZXR1cm5zL3JlcGxhY2VtZW50IHJlcXVlc3QgYmVpbmcgbWFkZSBieSBCdXllciB0byB0aGUgU2VsbGVyIG9mIHRoZSBwcm9kdWN0LCB0aGUgcmV0dXJuZWQgUHJvZHVjdChzKSBzaG91bGQgYmUgdW5vcGVuZWQgYW5kIFwiVU5VU0VEXCIgYW5kIHJldHVybmVkIHdpdGggb3JpZ2luYWwgcGFja2FnaW5nLCBvdGhlcndpc2UgdGhlIEJ1eWVyIHNoYWxsIG5vdCBiZSBlbnRpdGxlZCB0byBhbnkgcmVmdW5kIG9mIG1vbmV5IG9yIHJlcGxhY2VtZW50IG9mIHRoZSBQcm9kdWN0IChhcyBhcHBsaWNhYmxlKWAsXG4gICAgXSxcbiAgfSxcbl07XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dCc7XG5pbXBvcnQgQ2FuY2VsbGF0aW9uQW5kUmV0dXJuIGZyb20gJy4vQ2FuY2VsbGF0aW9uQW5kUmV0dXJuJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnR2V0UmFua3MgYnkgRWduaWZ5OiBBc3Nlc3NtZW50ICYgQW5hbHl0aWNzIFBsYXRmb3JtJyxcbiAgICBjaHVua3M6IFsnQ2FuY2VsbGF0aW9uQW5kUmV0dXJuJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8Q2FuY2VsbGF0aW9uQW5kUmV0dXJuIC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQ0E7QUFLQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQWZBO0FBQ0E7QUF5QkE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFkQTtBQUhBO0FBQ0E7QUFrQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBOzs7O0FBbEZBO0FBQ0E7QUFtRkE7Ozs7Ozs7QUN6RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7OztBQzdCQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFIQTtBQVdBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFDQTtBQUZBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFZQTs7OztBIiwic291cmNlUm9vdCI6IiJ9