(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Culture"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/culture/Culture.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Culture-header_container-2aO9f {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  height: 80vh;\n  width: 100%;\n  overflow: hidden;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n}\n\n.Culture-scrollTop-1szFV {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Culture-scrollTop-1szFV img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Culture-header_contents-2Zv0J {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  padding: 10% 2% 10% 10%;\n}\n\n.Culture-header_contents-2Zv0J h2 {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: left;\n  color: #fff;\n}\n\n.Culture-header_contents-2Zv0J p {\n  font-size: 20px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.Culture-header_image-3ZvP9 {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 10% 10% 10% 0%;\n}\n\n.Culture-header_image-3ZvP9 img {\n  width: 442px;\n  height: 315px;\n}\n\n.Culture-rightSideImageConatainer-2qt-l {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #fff;\n}\n\n.Culture-content-QdxRQ {\n  width: 50%;\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-left: 10%;\n  padding-right: 8%;\n}\n\n.Culture-content-QdxRQ h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.Culture-content-QdxRQ p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n.Culture-image-325bz {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Culture-image-325bz img {\n  width: 60%;\n}\n\n.Culture-leftSideImageConatainer-k1SrS {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.Culture-leftSideImageContent-ZxM8e {\n  width: 50%;\n  padding-right: 10%;\n  padding-left: 8%;\n}\n\n.Culture-leftSideImageContent-ZxM8e h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.Culture-leftSideImageContent-ZxM8e p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n@media only screen and (max-width: 989px) {\n  .Culture-scrollTop-1szFV {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Culture-header_container-2aO9f {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 560px;\n    padding: 32px 16px;\n  }\n\n  .Culture-header_contents-2Zv0J {\n    width: 100%;\n    padding: 0;\n  }\n\n  .Culture-header_contents-2Zv0J h2 {\n    font-size: 32px;\n    line-height: 40px;\n    text-align: center;\n    margin: 0;\n    margin-bottom: 16px;\n    padding: 0 24px;\n  }\n\n  .Culture-header_contents-2Zv0J p {\n    font-size: 14px;\n    line-height: 26px;\n    margin: 0;\n    text-align: center;\n    letter-spacing: -0.42px;\n  }\n\n  .Culture-header_image-3ZvP9 {\n    width: 292px;\n    height: 208px;\n    margin-top: 32px;\n    padding: 0 19px;\n  }\n\n    .Culture-header_image-3ZvP9 img {\n      width: 100%;\n      height: 100%;\n      -o-object-fit: cover;\n         object-fit: cover;\n    }\n\n  .Culture-rightSideImageConatainer-2qt-l {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ {\n      width: 100%;\n      height: auto;\n      padding: 0;\n    }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ h2 {\n        margin: 8px 0;\n        text-align: center;\n        font-size: 24px;\n        line-height: normal;\n      }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-content-QdxRQ p {\n        margin: 0;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n      }\n\n    .Culture-rightSideImageConatainer-2qt-l .Culture-image-325bz {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .Culture-rightSideImageConatainer-2qt-l .Culture-image-325bz img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .Culture-leftSideImageConatainer-k1SrS {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e {\n      width: 100%;\n      height: auto;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      padding: 0;\n    }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e h2 {\n        text-align: center;\n        font-size: 24px;\n        padding: 0 35px;\n        margin-top: 0;\n        line-height: normal;\n      }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-leftSideImageContent-ZxM8e p {\n        font-size: 14px;\n        text-align: center;\n        margin: 0;\n        line-height: 24px;\n      }\n\n    .Culture-leftSideImageConatainer-k1SrS .Culture-image-325bz {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .Culture-leftSideImageConatainer-k1SrS .Culture-image-325bz img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/Culture.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;EACb,YAAY;EACZ,iBAAiB;EACjB,4FAA4F;EAC5F,0EAA0E;EAC1E,qEAAqE;EACrE,sEAAsE;CACvE;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,wBAAwB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,wBAAwB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;CACd;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,gCAAgC;MAC5B,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;EACxB,0BAA0B;CAC3B;;AAED;EACE,WAAW;EACX,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;CACd;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,0BAA0B;QACtB,8BAA8B;IAClC,uBAAuB;QACnB,oBAAoB;IACxB,YAAY;IACZ,cAAc;IACd,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;IACnB,UAAU;IACV,oBAAoB;IACpB,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,UAAU;IACV,mBAAmB;IACnB,wBAAwB;GACzB;;EAED;IACE,aAAa;IACb,cAAc;IACd,iBAAiB;IACjB,gBAAgB;GACjB;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,qBAAqB;SAClB,kBAAkB;KACtB;;EAEH;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,aAAa;GACd;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,WAAW;KACZ;;MAEC;QACE,cAAc;QACd,mBAAmB;QACnB,gBAAgB;QAChB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;OACnB;;IAEH;MACE,WAAW;MACX,iBAAiB;MACjB,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;EAEL;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,aAAa;GACd;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,sBAAsB;UAClB,wBAAwB;MAC5B,WAAW;KACZ;;MAEC;QACE,mBAAmB;QACnB,gBAAgB;QAChB,gBAAgB;QAChB,cAAc;QACd,oBAAoB;OACrB;;MAED;QACE,gBAAgB;QAChB,mBAAmB;QACnB,UAAU;QACV,kBAAkB;OACnB;;IAEH;MACE,WAAW;MACX,iBAAiB;MACjB,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;CACN","file":"Culture.scss","sourcesContent":[".header_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  height: 80vh;\n  width: 100%;\n  overflow: hidden;\n  background-image: -webkit-gradient(linear, left top, right top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: -o-linear-gradient(left, #ea4c70 0%, #b2457c 100%);\n  background-image: linear-gradient(to right, #ea4c70 0%, #b2457c 100%);\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.header_contents {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  padding: 10% 2% 10% 10%;\n}\n\n.header_contents h2 {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: left;\n  color: #fff;\n}\n\n.header_contents p {\n  font-size: 20px;\n  line-height: 32px;\n  color: #fff;\n}\n\n.header_image {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 10% 10% 10% 0%;\n}\n\n.header_image img {\n  width: 442px;\n  height: 315px;\n}\n\n.rightSideImageConatainer {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #fff;\n}\n\n.content {\n  width: 50%;\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-left: 10%;\n  padding-right: 8%;\n}\n\n.content h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.content p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n.image {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.image img {\n  width: 60%;\n}\n\n.leftSideImageConatainer {\n  width: 100%;\n  height: 60vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.leftSideImageContent {\n  width: 50%;\n  padding-right: 10%;\n  padding-left: 8%;\n}\n\n.leftSideImageContent h2 {\n  font-size: 32px;\n  line-height: 48px;\n  color: #25282b;\n  text-align: left;\n}\n\n.leftSideImageContent p {\n  font-size: 16px;\n  line-height: 30px;\n  color: #25282b;\n  opacity: 0.7;\n}\n\n@media only screen and (max-width: 989px) {\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .header_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 560px;\n    padding: 32px 16px;\n  }\n\n  .header_contents {\n    width: 100%;\n    padding: 0;\n  }\n\n  .header_contents h2 {\n    font-size: 32px;\n    line-height: 40px;\n    text-align: center;\n    margin: 0;\n    margin-bottom: 16px;\n    padding: 0 24px;\n  }\n\n  .header_contents p {\n    font-size: 14px;\n    line-height: 26px;\n    margin: 0;\n    text-align: center;\n    letter-spacing: -0.42px;\n  }\n\n  .header_image {\n    width: 292px;\n    height: 208px;\n    margin-top: 32px;\n    padding: 0 19px;\n  }\n\n    .header_image img {\n      width: 100%;\n      height: 100%;\n      -o-object-fit: cover;\n         object-fit: cover;\n    }\n\n  .rightSideImageConatainer {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .rightSideImageConatainer .content {\n      width: 100%;\n      height: auto;\n      padding: 0;\n    }\n\n      .rightSideImageConatainer .content h2 {\n        margin: 8px 0;\n        text-align: center;\n        font-size: 24px;\n        line-height: normal;\n      }\n\n      .rightSideImageConatainer .content p {\n        margin: 0;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n      }\n\n    .rightSideImageConatainer .image {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .rightSideImageConatainer .image img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n  .leftSideImageConatainer {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 24px 16px;\n    height: auto;\n  }\n\n    .leftSideImageConatainer .leftSideImageContent {\n      width: 100%;\n      height: auto;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-pack: center;\n          justify-content: center;\n      padding: 0;\n    }\n\n      .leftSideImageConatainer .leftSideImageContent h2 {\n        text-align: center;\n        font-size: 24px;\n        padding: 0 35px;\n        margin-top: 0;\n        line-height: normal;\n      }\n\n      .leftSideImageConatainer .leftSideImageContent p {\n        font-size: 14px;\n        text-align: center;\n        margin: 0;\n        line-height: 24px;\n      }\n\n    .leftSideImageConatainer .image {\n      width: 80%;\n      min-width: 232px;\n      height: 200px;\n    }\n\n      .leftSideImageConatainer .image img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"header_container": "Culture-header_container-2aO9f",
	"scrollTop": "Culture-scrollTop-1szFV",
	"header_contents": "Culture-header_contents-2Zv0J",
	"header_image": "Culture-header_image-3ZvP9",
	"rightSideImageConatainer": "Culture-rightSideImageConatainer-2qt-l",
	"content": "Culture-content-QdxRQ",
	"image": "Culture-image-325bz",
	"leftSideImageConatainer": "Culture-leftSideImageConatainer-k1SrS",
	"leftSideImageContent": "Culture-leftSideImageContent-ZxM8e"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/Culture.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Culture_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/culture/Culture.scss");
/* harmony import */ var _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Culture_scss__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/Culture.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Tests =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tests, _React$Component);

  function Tests(props) {
    var _this;

    _classCallCheck(this, Tests);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tests).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleScroll", function () {
      if (window.scrollY > 500) {
        _this.setState({
          showScroll: true
        });
      } else {
        _this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleScrollTop", function () {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });

      _this.setState({
        showScroll: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "displayScrollToTop", function () {
      var showScroll = _this.state.showScroll;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.scrollTop,
        role: "presentation",
        onClick: function onClick() {
          _this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayHeader", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, "Culture is a funny thing", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }), "to describe in words"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, "We believe culture is the most important driver of organizational success and of the satisfaction we feel at the end of each day. But when put into words, it can sound contrived. So we\u2019ll just tell you what you\u2019d observe as you wander the halls.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.header_image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/culture-banner.svg",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayThinkBig", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, "Think Big - 10X not 10%"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, "We believe culture is the most important driver of organizational success and of the satisfaction we feel at the end of each day. But when put into words,it can sound contrived. So we\u2019ll just tell you what you\u2019d observe as you wander the halls.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/think-big.svg",
        alt: "think-big",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayProblems", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, "Solve whole and hard", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }), " problems that matter"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }, "Users have numerous small problems but we have learned to look at the bigger picture. We don\u2019t just believe in telling the user where they are weak at, we would offer you a learning plan to improve on it.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/problems.svg",
        alt: "problems",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayExperience", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, "Design the experience not the product"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, "A product that solves the user problem in a simple, easy and efficient manner. An amazing experience on the other hand creates customer delight and puts a smile on their face along with solving the problem.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/experience.svg",
        alt: "experience",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAccountability", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, "Accountability and", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }), " ownership"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, "We don\u2019t micromanage; period. We believe that when employees take ownership and have autonomy they achieve more. The accountability in-built in their success, and that\u2019s the way we want it..")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/accountability-and-ownership.svg",
        alt: "accountability and ownership",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displaySmart", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, "Develop and retain smart and interesting people"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 158
        },
        __self: this
      }, "People are the key to any organisation and here at Egnify we hire the best, smart and interesting people. It\u2019s not just your resume that we\u2019re interested in, but we want to know the distance travelled and the wounds suffered along the way.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/smart.svg",
        alt: "smart",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayFailure", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 172
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.leftSideImageContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174
        },
        __self: this
      }, "Failure tolerance"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 175
        },
        __self: this
      }, "Failures are the pillars of success - we deeply believe in this. We want to move fast and so things will break and mistakes will be made, the idea is to learn from them and move faster.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 181
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/failure.svg",
        alt: "failure",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayFun", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.rightSideImageConatainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 188
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, "Have fun!"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, "We enjoy what we do, and we also enjoy a couple of beers after work in the middle of the week.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 196
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/culture/have-fun.svg",
        alt: "have-fun",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 197
        },
        __self: this
      })));
    });

    _this.state = {};
    return _this;
  }

  _createClass(Tests, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleScroll();
      window.addEventListener('scroll', this.handleScroll);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }, this.displayHeader()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }, this.displayThinkBig()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, this.displayProblems()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      }, this.displayExperience()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, this.displayAccountability()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }, this.displaySmart()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }, this.displayFailure()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, this.displayFun()), this.displayScrollToTop());
    }
  }]);

  return Tests;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Culture_scss__WEBPACK_IMPORTED_MODULE_2___default.a)(Tests));

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/Culture.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/culture/Culture.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/culture/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Culture__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/culture/Culture.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/culture/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Culture - Organizational success',
              chunks: ['Culture'],
              content: 'We are with a strong culture defined with organizational sucess and steady progress.',
              keywords: 'digital evaluation, assessment App',
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 13
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Culture__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 14
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VsdHVyZS5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1bHR1cmUvQ3VsdHVyZS5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1bHR1cmUvQ3VsdHVyZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9jdWx0dXJlL0N1bHR1cmUuc2Nzcz9jN2QxIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1bHR1cmUvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5DdWx0dXJlLWhlYWRlcl9jb250YWluZXItMmFPOWYge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGhlaWdodDogODB2aDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IHRvcCwgcmlnaHQgdG9wLCBmcm9tKCNlYTRjNzApLCB0bygjYjI0NTdjKSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KGxlZnQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxufVxcblxcbi5DdWx0dXJlLXNjcm9sbFRvcC0xc3pGViB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLkN1bHR1cmUtc2Nyb2xsVG9wLTFzekZWIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLkN1bHR1cmUtaGVhZGVyX2NvbnRlbnRzLTJadjBKIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgcGFkZGluZzogMTAlIDIlIDEwJSAxMCU7XFxufVxcblxcbi5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiBoMiB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNjBweDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLkN1bHR1cmUtaGVhZGVyX2NvbnRlbnRzLTJadjBKIHAge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLkN1bHR1cmUtaGVhZGVyX2ltYWdlLTNadlA5IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZzogMTAlIDEwJSAxMCUgMCU7XFxufVxcblxcbi5DdWx0dXJlLWhlYWRlcl9pbWFnZS0zWnZQOSBpbWcge1xcbiAgd2lkdGg6IDQ0MnB4O1xcbiAgaGVpZ2h0OiAzMTVweDtcXG59XFxuXFxuLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA2MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5DdWx0dXJlLWNvbnRlbnQtUWR4UlEge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLWxlZnQ6IDEwJTtcXG4gIHBhZGRpbmctcmlnaHQ6IDglO1xcbn1cXG5cXG4uQ3VsdHVyZS1jb250ZW50LVFkeFJRIGgyIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4uQ3VsdHVyZS1jb250ZW50LVFkeFJRIHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLkN1bHR1cmUtaW1hZ2UtMzI1Ynoge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5DdWx0dXJlLWltYWdlLTMyNWJ6IGltZyB7XFxuICB3aWR0aDogNjAlO1xcbn1cXG5cXG4uQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29uYXRhaW5lci1rMVNyUyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNjB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbnRlbnQtWnhNOGUge1xcbiAgd2lkdGg6IDUwJTtcXG4gIHBhZGRpbmctcmlnaHQ6IDEwJTtcXG4gIHBhZGRpbmctbGVmdDogOCU7XFxufVxcblxcbi5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb250ZW50LVp4TThlIGgyIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4uQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29udGVudC1aeE04ZSBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjc7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTg5cHgpIHtcXG4gIC5DdWx0dXJlLXNjcm9sbFRvcC0xc3pGViB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuQ3VsdHVyZS1oZWFkZXJfY29udGFpbmVyLTJhTzlmIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogNTYwcHg7XFxuICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG4gIH1cXG5cXG4gIC5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLkN1bHR1cmUtaGVhZGVyX2NvbnRlbnRzLTJadjBKIGgyIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW46IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICAgIHBhZGRpbmc6IDAgMjRweDtcXG4gIH1cXG5cXG4gIC5DdWx0dXJlLWhlYWRlcl9jb250ZW50cy0yWnYwSiBwIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjZweDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxldHRlci1zcGFjaW5nOiAtMC40MnB4O1xcbiAgfVxcblxcbiAgLkN1bHR1cmUtaGVhZGVyX2ltYWdlLTNadlA5IHtcXG4gICAgd2lkdGg6IDI5MnB4O1xcbiAgICBoZWlnaHQ6IDIwOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xcbiAgICBwYWRkaW5nOiAwIDE5cHg7XFxuICB9XFxuXFxuICAgIC5DdWx0dXJlLWhlYWRlcl9pbWFnZS0zWnZQOSBpbWcge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgfVxcblxcbiAgLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBwYWRkaW5nOiAyNHB4IDE2cHg7XFxuICAgIGhlaWdodDogYXV0bztcXG4gIH1cXG5cXG4gICAgLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIC5DdWx0dXJlLWNvbnRlbnQtUWR4UlEge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogYXV0bztcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICB9XFxuXFxuICAgICAgLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIC5DdWx0dXJlLWNvbnRlbnQtUWR4UlEgaDIge1xcbiAgICAgICAgbWFyZ2luOiA4cHggMDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgfVxcblxcbiAgICAgIC5DdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbCAuQ3VsdHVyZS1jb250ZW50LVFkeFJRIHAge1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgfVxcblxcbiAgICAuQ3VsdHVyZS1yaWdodFNpZGVJbWFnZUNvbmF0YWluZXItMnF0LWwgLkN1bHR1cmUtaW1hZ2UtMzI1Ynoge1xcbiAgICAgIHdpZHRoOiA4MCU7XFxuICAgICAgbWluLXdpZHRoOiAyMzJweDtcXG4gICAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICB9XFxuXFxuICAgICAgLkN1bHR1cmUtcmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyLTJxdC1sIC5DdWx0dXJlLWltYWdlLTMyNWJ6IGltZyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG5cXG4gIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBwYWRkaW5nOiAyNHB4IDE2cHg7XFxuICAgIGhlaWdodDogYXV0bztcXG4gIH1cXG5cXG4gICAgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclMgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbnRlbnQtWnhNOGUge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogYXV0bztcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgfVxcblxcbiAgICAgIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyLWsxU3JTIC5DdWx0dXJlLWxlZnRTaWRlSW1hZ2VDb250ZW50LVp4TThlIGgyIHtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgIHBhZGRpbmc6IDAgMzVweDtcXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAuQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29uYXRhaW5lci1rMVNyUyAuQ3VsdHVyZS1sZWZ0U2lkZUltYWdlQ29udGVudC1aeE04ZSBwIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclMgLkN1bHR1cmUtaW1hZ2UtMzI1Ynoge1xcbiAgICAgIHdpZHRoOiA4MCU7XFxuICAgICAgbWluLXdpZHRoOiAyMzJweDtcXG4gICAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICB9XFxuXFxuICAgICAgLkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclMgLkN1bHR1cmUtaW1hZ2UtMzI1YnogaW1nIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1bHR1cmUvQ3VsdHVyZS5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixhQUFhO0VBQ2IsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQiw0RkFBNEY7RUFDNUYsMEVBQTBFO0VBQzFFLHFFQUFxRTtFQUNyRSxzRUFBc0U7Q0FDdkU7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix3QkFBd0I7Q0FDekI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix3QkFBd0I7Q0FDekI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4Qix1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGtCQUFrQjtFQUNsQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7Q0FDN0I7O0FBRUQ7RUFDRSxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsZ0NBQWdDO01BQzVCLDRCQUE0QjtFQUNoQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsYUFBYTtDQUNkOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsMEJBQTBCO1FBQ3RCLDhCQUE4QjtJQUNsQyx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLFlBQVk7SUFDWixjQUFjO0lBQ2QsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFdBQVc7R0FDWjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixvQkFBb0I7SUFDcEIsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLHdCQUF3QjtHQUN6Qjs7RUFFRDtJQUNFLGFBQWE7SUFDYixjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLGdCQUFnQjtHQUNqQjs7SUFFQztNQUNFLFlBQVk7TUFDWixhQUFhO01BQ2IscUJBQXFCO1NBQ2xCLGtCQUFrQjtLQUN0Qjs7RUFFSDtJQUNFLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsbUJBQW1CO0lBQ25CLGFBQWE7R0FDZDs7SUFFQztNQUNFLFlBQVk7TUFDWixhQUFhO01BQ2IsV0FBVztLQUNaOztNQUVDO1FBQ0UsY0FBYztRQUNkLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsVUFBVTtRQUNWLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsa0JBQWtCO09BQ25COztJQUVIO01BQ0UsV0FBVztNQUNYLGlCQUFpQjtNQUNqQixjQUFjO0tBQ2Y7O01BRUM7UUFDRSxZQUFZO1FBQ1osYUFBYTtRQUNiLHVCQUF1QjtXQUNwQixvQkFBb0I7T0FDeEI7O0VBRUw7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLG1CQUFtQjtJQUNuQixhQUFhO0dBQ2Q7O0lBRUM7TUFDRSxZQUFZO01BQ1osYUFBYTtNQUNiLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQixzQkFBc0I7VUFDbEIsd0JBQXdCO01BQzVCLFdBQVc7S0FDWjs7TUFFQztRQUNFLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsZ0JBQWdCO1FBQ2hCLGNBQWM7UUFDZCxvQkFBb0I7T0FDckI7O01BRUQ7UUFDRSxnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLFVBQVU7UUFDVixrQkFBa0I7T0FDbkI7O0lBRUg7TUFDRSxXQUFXO01BQ1gsaUJBQWlCO01BQ2pCLGNBQWM7S0FDZjs7TUFFQztRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsdUJBQXVCO1dBQ3BCLG9CQUFvQjtPQUN4QjtDQUNOXCIsXCJmaWxlXCI6XCJDdWx0dXJlLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLmhlYWRlcl9jb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGhlaWdodDogODB2aDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IHRvcCwgcmlnaHQgdG9wLCBmcm9tKCNlYTRjNzApLCB0bygjYjI0NTdjKSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjZWE0YzcwIDAlLCAjYjI0NTdjIDEwMCUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KGxlZnQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNlYTRjNzAgMCUsICNiMjQ1N2MgMTAwJSk7XFxufVxcblxcbi5zY3JvbGxUb3Age1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5zY3JvbGxUb3AgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4uaGVhZGVyX2NvbnRlbnRzIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgcGFkZGluZzogMTAlIDIlIDEwJSAxMCU7XFxufVxcblxcbi5oZWFkZXJfY29udGVudHMgaDIge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDYwcHg7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5oZWFkZXJfY29udGVudHMgcCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uaGVhZGVyX2ltYWdlIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZzogMTAlIDEwJSAxMCUgMCU7XFxufVxcblxcbi5oZWFkZXJfaW1hZ2UgaW1nIHtcXG4gIHdpZHRoOiA0NDJweDtcXG4gIGhlaWdodDogMzE1cHg7XFxufVxcblxcbi5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDYwdmg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLmNvbnRlbnQge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLWxlZnQ6IDEwJTtcXG4gIHBhZGRpbmctcmlnaHQ6IDglO1xcbn1cXG5cXG4uY29udGVudCBoMiB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuXFxuLmNvbnRlbnQgcCB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgb3BhY2l0eTogMC43O1xcbn1cXG5cXG4uaW1hZ2Uge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5pbWFnZSBpbWcge1xcbiAgd2lkdGg6IDYwJTtcXG59XFxuXFxuLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA2MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4ubGVmdFNpZGVJbWFnZUNvbnRlbnQge1xcbiAgd2lkdGg6IDUwJTtcXG4gIHBhZGRpbmctcmlnaHQ6IDEwJTtcXG4gIHBhZGRpbmctbGVmdDogOCU7XFxufVxcblxcbi5sZWZ0U2lkZUltYWdlQ29udGVudCBoMiB7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG59XFxuXFxuLmxlZnRTaWRlSW1hZ2VDb250ZW50IHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5ODlweCkge1xcbiAgLnNjcm9sbFRvcCB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyX2NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDU2MHB4O1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyX2NvbnRlbnRzIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAuaGVhZGVyX2NvbnRlbnRzIGgyIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW46IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICAgIHBhZGRpbmc6IDAgMjRweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJfY29udGVudHMgcCB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XFxuICAgIG1hcmdpbjogMDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuNDJweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJfaW1hZ2Uge1xcbiAgICB3aWR0aDogMjkycHg7XFxuICAgIGhlaWdodDogMjA4cHg7XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICAgIHBhZGRpbmc6IDAgMTlweDtcXG4gIH1cXG5cXG4gICAgLmhlYWRlcl9pbWFnZSBpbWcge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgfVxcblxcbiAgLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lciB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBoZWlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAgIC5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXIgLmNvbnRlbnQge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogYXV0bztcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICB9XFxuXFxuICAgICAgLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lciAuY29udGVudCBoMiB7XFxuICAgICAgICBtYXJnaW46IDhweCAwO1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICB9XFxuXFxuICAgICAgLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lciAuY29udGVudCBwIHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lciAuaW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA4MCU7XFxuICAgICAgbWluLXdpZHRoOiAyMzJweDtcXG4gICAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICB9XFxuXFxuICAgICAgLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lciAuaW1hZ2UgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcblxcbiAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBwYWRkaW5nOiAyNHB4IDE2cHg7XFxuICAgIGhlaWdodDogYXV0bztcXG4gIH1cXG5cXG4gICAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIC5sZWZ0U2lkZUltYWdlQ29udGVudCB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgaGVpZ2h0OiBhdXRvO1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICBwYWRkaW5nOiAwO1xcbiAgICB9XFxuXFxuICAgICAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIC5sZWZ0U2lkZUltYWdlQ29udGVudCBoMiB7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICBwYWRkaW5nOiAwIDM1cHg7XFxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICB9XFxuXFxuICAgICAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIC5sZWZ0U2lkZUltYWdlQ29udGVudCBwIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIH1cXG5cXG4gICAgLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyIC5pbWFnZSB7XFxuICAgICAgd2lkdGg6IDgwJTtcXG4gICAgICBtaW4td2lkdGg6IDIzMnB4O1xcbiAgICAgIGhlaWdodDogMjAwcHg7XFxuICAgIH1cXG5cXG4gICAgICAubGVmdFNpZGVJbWFnZUNvbmF0YWluZXIgLmltYWdlIGltZyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImhlYWRlcl9jb250YWluZXJcIjogXCJDdWx0dXJlLWhlYWRlcl9jb250YWluZXItMmFPOWZcIixcblx0XCJzY3JvbGxUb3BcIjogXCJDdWx0dXJlLXNjcm9sbFRvcC0xc3pGVlwiLFxuXHRcImhlYWRlcl9jb250ZW50c1wiOiBcIkN1bHR1cmUtaGVhZGVyX2NvbnRlbnRzLTJadjBKXCIsXG5cdFwiaGVhZGVyX2ltYWdlXCI6IFwiQ3VsdHVyZS1oZWFkZXJfaW1hZ2UtM1p2UDlcIixcblx0XCJyaWdodFNpZGVJbWFnZUNvbmF0YWluZXJcIjogXCJDdWx0dXJlLXJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lci0ycXQtbFwiLFxuXHRcImNvbnRlbnRcIjogXCJDdWx0dXJlLWNvbnRlbnQtUWR4UlFcIixcblx0XCJpbWFnZVwiOiBcIkN1bHR1cmUtaW1hZ2UtMzI1YnpcIixcblx0XCJsZWZ0U2lkZUltYWdlQ29uYXRhaW5lclwiOiBcIkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbmF0YWluZXItazFTclNcIixcblx0XCJsZWZ0U2lkZUltYWdlQ29udGVudFwiOiBcIkN1bHR1cmUtbGVmdFNpZGVJbWFnZUNvbnRlbnQtWnhNOGVcIlxufTsiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9DdWx0dXJlLnNjc3MnO1xuXG5jbGFzcyBUZXN0cyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cblxuICBoYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5zY3JvbGxZID4gNTAwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlU2Nyb2xsVG9wID0gKCkgPT4ge1xuICAgIHdpbmRvdy5zY3JvbGxUbyh7XG4gICAgICB0b3A6IDAsXG4gICAgICBiZWhhdmlvcjogJ3Ntb290aCcsXG4gICAgfSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICB9KTtcbiAgfTtcblxuICBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBzaG93U2Nyb2xsIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICBzaG93U2Nyb2xsICYmIChcbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5zY3JvbGxUb3B9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGxUb3AoKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvc2Nyb2xsVG9wLnN2Z1wiIGFsdD1cInNjcm9sbFRvcFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheUhlYWRlciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJfY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlcl9jb250ZW50c30+XG4gICAgICAgIDxoMj5cbiAgICAgICAgICBDdWx0dXJlIGlzIGEgZnVubnkgdGhpbmdcbiAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICB0byBkZXNjcmliZSBpbiB3b3Jkc1xuICAgICAgICA8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBXZSBiZWxpZXZlIGN1bHR1cmUgaXMgdGhlIG1vc3QgaW1wb3J0YW50IGRyaXZlciBvZiBvcmdhbml6YXRpb25hbFxuICAgICAgICAgIHN1Y2Nlc3MgYW5kIG9mIHRoZSBzYXRpc2ZhY3Rpb24gd2UgZmVlbCBhdCB0aGUgZW5kIG9mIGVhY2ggZGF5LiBCdXRcbiAgICAgICAgICB3aGVuIHB1dCBpbnRvIHdvcmRzLCBpdCBjYW4gc291bmQgY29udHJpdmVkLiBTbyB3ZeKAmWxsIGp1c3QgdGVsbCB5b3VcbiAgICAgICAgICB3aGF0IHlvdeKAmWQgb2JzZXJ2ZSBhcyB5b3Ugd2FuZGVyIHRoZSBoYWxscy5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJfaW1hZ2V9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS9jdWx0dXJlLWJhbm5lci5zdmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVRoaW5rQmlnID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJpZ2h0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgPGgyPlRoaW5rIEJpZyAtIDEwWCBub3QgMTAlPC9oMj5cbiAgICAgICAgPHA+XG4gICAgICAgICAgV2UgYmVsaWV2ZSBjdWx0dXJlIGlzIHRoZSBtb3N0IGltcG9ydGFudCBkcml2ZXIgb2Ygb3JnYW5pemF0aW9uYWxcbiAgICAgICAgICBzdWNjZXNzIGFuZCBvZiB0aGUgc2F0aXNmYWN0aW9uIHdlIGZlZWwgYXQgdGhlIGVuZCBvZiBlYWNoIGRheS4gQnV0XG4gICAgICAgICAgd2hlbiBwdXQgaW50byB3b3JkcyxpdCBjYW4gc291bmQgY29udHJpdmVkLiBTbyB3ZeKAmWxsIGp1c3QgdGVsbCB5b3VcbiAgICAgICAgICB3aGF0IHlvdeKAmWQgb2JzZXJ2ZSBhcyB5b3Ugd2FuZGVyIHRoZSBoYWxscy5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9jdWx0dXJlL3RoaW5rLWJpZy5zdmdcIiBhbHQ9XCJ0aGluay1iaWdcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVByb2JsZW1zID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlZnRTaWRlSW1hZ2VDb250ZW50fT5cbiAgICAgICAgPGgyPlxuICAgICAgICAgIFNvbHZlIHdob2xlIGFuZCBoYXJkXG4gICAgICAgICAgPGJyIC8+IHByb2JsZW1zIHRoYXQgbWF0dGVyXG4gICAgICAgIDwvaDI+XG4gICAgICAgIDxwPlxuICAgICAgICAgIFVzZXJzIGhhdmUgbnVtZXJvdXMgc21hbGwgcHJvYmxlbXMgYnV0IHdlIGhhdmUgbGVhcm5lZCB0byBsb29rIGF0IHRoZVxuICAgICAgICAgIGJpZ2dlciBwaWN0dXJlLiBXZSBkb27igJl0IGp1c3QgYmVsaWV2ZSBpbiB0ZWxsaW5nIHRoZSB1c2VyIHdoZXJlIHRoZXlcbiAgICAgICAgICBhcmUgd2VhayBhdCwgd2Ugd291bGQgb2ZmZXIgeW91IGEgbGVhcm5pbmcgcGxhbiB0byBpbXByb3ZlIG9uIGl0LlxuICAgICAgICA8L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltYWdlfT5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2N1bHR1cmUvcHJvYmxlbXMuc3ZnXCIgYWx0PVwicHJvYmxlbXNcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUV4cGVyaWVuY2UgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MucmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICA8aDI+RGVzaWduIHRoZSBleHBlcmllbmNlIG5vdCB0aGUgcHJvZHVjdDwvaDI+XG4gICAgICAgIDxwPlxuICAgICAgICAgIEEgcHJvZHVjdCB0aGF0IHNvbHZlcyB0aGUgdXNlciBwcm9ibGVtIGluIGEgc2ltcGxlLCBlYXN5IGFuZCBlZmZpY2llbnRcbiAgICAgICAgICBtYW5uZXIuIEFuIGFtYXppbmcgZXhwZXJpZW5jZSBvbiB0aGUgb3RoZXIgaGFuZCBjcmVhdGVzIGN1c3RvbWVyXG4gICAgICAgICAgZGVsaWdodCBhbmQgcHV0cyBhIHNtaWxlIG9uIHRoZWlyIGZhY2UgYWxvbmcgd2l0aCBzb2x2aW5nIHRoZSBwcm9ibGVtLlxuICAgICAgICA8L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltYWdlfT5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2N1bHR1cmUvZXhwZXJpZW5jZS5zdmdcIiBhbHQ9XCJleHBlcmllbmNlXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlBY2NvdW50YWJpbGl0eSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29uYXRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5sZWZ0U2lkZUltYWdlQ29udGVudH0+XG4gICAgICAgIDxoMj5cbiAgICAgICAgICBBY2NvdW50YWJpbGl0eSBhbmRcbiAgICAgICAgICA8YnIgLz4gb3duZXJzaGlwXG4gICAgICAgIDwvaDI+XG4gICAgICAgIDxwPlxuICAgICAgICAgIFdlIGRvbuKAmXQgbWljcm9tYW5hZ2U7IHBlcmlvZC4gV2UgYmVsaWV2ZSB0aGF0IHdoZW4gZW1wbG95ZWVzIHRha2VcbiAgICAgICAgICBvd25lcnNoaXAgYW5kIGhhdmUgYXV0b25vbXkgdGhleSBhY2hpZXZlIG1vcmUuIFRoZSBhY2NvdW50YWJpbGl0eVxuICAgICAgICAgIGluLWJ1aWx0IGluIHRoZWlyIHN1Y2Nlc3MsIGFuZCB0aGF04oCZcyB0aGUgd2F5IHdlIHdhbnQgaXQuLlxuICAgICAgICA8L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltYWdlfT5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS9hY2NvdW50YWJpbGl0eS1hbmQtb3duZXJzaGlwLnN2Z1wiXG4gICAgICAgICAgYWx0PVwiYWNjb3VudGFiaWxpdHkgYW5kIG93bmVyc2hpcFwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5U21hcnQgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MucmlnaHRTaWRlSW1hZ2VDb25hdGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICA8aDI+RGV2ZWxvcCBhbmQgcmV0YWluIHNtYXJ0IGFuZCBpbnRlcmVzdGluZyBwZW9wbGU8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBQZW9wbGUgYXJlIHRoZSBrZXkgdG8gYW55IG9yZ2FuaXNhdGlvbiBhbmQgaGVyZSBhdCBFZ25pZnkgd2UgaGlyZSB0aGVcbiAgICAgICAgICBiZXN0LCBzbWFydCBhbmQgaW50ZXJlc3RpbmcgcGVvcGxlLiBJdOKAmXMgbm90IGp1c3QgeW91ciByZXN1bWUgdGhhdFxuICAgICAgICAgIHdl4oCZcmUgaW50ZXJlc3RlZCBpbiwgYnV0IHdlIHdhbnQgdG8ga25vdyB0aGUgZGlzdGFuY2UgdHJhdmVsbGVkIGFuZFxuICAgICAgICAgIHRoZSB3b3VuZHMgc3VmZmVyZWQgYWxvbmcgdGhlIHdheS5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9jdWx0dXJlL3NtYXJ0LnN2Z1wiIGFsdD1cInNtYXJ0XCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlGYWlsdXJlID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlZnRTaWRlSW1hZ2VDb25hdGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlZnRTaWRlSW1hZ2VDb250ZW50fT5cbiAgICAgICAgPGgyPkZhaWx1cmUgdG9sZXJhbmNlPC9oMj5cbiAgICAgICAgPHA+XG4gICAgICAgICAgRmFpbHVyZXMgYXJlIHRoZSBwaWxsYXJzIG9mIHN1Y2Nlc3MgLSB3ZSBkZWVwbHkgYmVsaWV2ZSBpbiB0aGlzLiBXZVxuICAgICAgICAgIHdhbnQgdG8gbW92ZSBmYXN0IGFuZCBzbyB0aGluZ3Mgd2lsbCBicmVhayBhbmQgbWlzdGFrZXMgd2lsbCBiZSBtYWRlLFxuICAgICAgICAgIHRoZSBpZGVhIGlzIHRvIGxlYXJuIGZyb20gdGhlbSBhbmQgbW92ZSBmYXN0ZXIuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2V9PlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvY3VsdHVyZS9mYWlsdXJlLnN2Z1wiIGFsdD1cImZhaWx1cmVcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUZ1biA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5yaWdodFNpZGVJbWFnZUNvbmF0YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgIDxoMj5IYXZlIGZ1biE8L2gyPlxuICAgICAgICA8cD5cbiAgICAgICAgICBXZSBlbmpveSB3aGF0IHdlIGRvLCBhbmQgd2UgYWxzbyBlbmpveSBhIGNvdXBsZSBvZiBiZWVycyBhZnRlciB3b3JrIGluXG4gICAgICAgICAgdGhlIG1pZGRsZSBvZiB0aGUgd2Vlay5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZX0+XG4gICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9jdWx0dXJlL2hhdmUtZnVuLnN2Z1wiIGFsdD1cImhhdmUtZnVuXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5SGVhZGVyKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheVRoaW5rQmlnKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheVByb2JsZW1zKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUV4cGVyaWVuY2UoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5QWNjb3VudGFiaWxpdHkoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5U21hcnQoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5RmFpbHVyZSgpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlGdW4oKX08L2Rpdj5cbiAgICAgICAge3RoaXMuZGlzcGxheVNjcm9sbFRvVG9wKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoVGVzdHMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DdWx0dXJlLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ3VsdHVyZS5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vQ3VsdHVyZS5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgQ3VsdHVyZSBmcm9tICcuL0N1bHR1cmUnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdDdWx0dXJlIC0gT3JnYW5pemF0aW9uYWwgc3VjY2VzcycsXG4gICAgY2h1bmtzOiBbJ0N1bHR1cmUnXSxcbiAgICBjb250ZW50OlxuICAgICAgJ1dlIGFyZSB3aXRoIGEgc3Ryb25nIGN1bHR1cmUgZGVmaW5lZCB3aXRoIG9yZ2FuaXphdGlvbmFsIHN1Y2VzcyBhbmQgc3RlYWR5IHByb2dyZXNzLicsXG4gICAga2V5d29yZHM6ICdkaWdpdGFsIGV2YWx1YXRpb24sIGFzc2Vzc21lbnQgQXBwJyxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQgZm9vdGVyQXNoPlxuICAgICAgICA8Q3VsdHVyZSAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQWVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUF6QkE7QUEyQkE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFuQ0E7QUFvQ0E7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQXBEQTtBQXFEQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFoQkE7QUFDQTtBQXREQTtBQTBFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFaQTtBQUNBO0FBM0VBO0FBMkZBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWRBO0FBQ0E7QUE1RkE7QUE4R0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWEE7QUFDQTtBQS9HQTtBQThIQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQUNBO0FBL0hBO0FBb0pBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVpBO0FBQ0E7QUFySkE7QUFxS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWEE7QUFDQTtBQXRLQTtBQXFMQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWQTtBQUNBO0FBcExBO0FBRkE7QUFHQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUF3TEE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBbk5BO0FBQ0E7QUFxTkE7Ozs7Ozs7QUMxTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFSQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFlQTs7OztBIiwic291cmNlUm9vdCI6IiJ9