(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Customers"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/customers/Customers.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Customers-headerContainer-3EA99 {\n  height: 80vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 64px;\n  background-color: #f7f7f7;\n}\n\n.Customers-scrollTop-2r3qa {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Customers-scrollTop-2r3qa img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Customers-header_text-1Nc_1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  width: 41%;\n}\n\n.Customers-trustedByTitle-3pPTH {\n  font-size: 64px;\n  font-size: 4rem;\n  font-weight: 600;\n  color: #25282b;\n}\n\n/* .trustedBy_text {\n  color: #000;\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 1.5;\n} */\n\n.Customers-trustedBy-3FuEk {\n  color: #f36;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-header_box_container-3ATaO {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  gap: 32px;\n}\n\n.Customers-box-2g2DN {\n  width: 320px;\n  height: 320px;\n  -webkit-box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n          box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n  padding: 54px 40px;\n  border-radius: 8px;\n  text-align: center;\n}\n\n.Customers-box_contents_tag-1-7OE {\n  font-size: 20px;\n  color: #25282b;\n  opacity: 0.6;\n  font-weight: normal;\n  margin-bottom: 24px;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-box_contents-1_6Sy {\n  font-size: 48px;\n  font-size: 3rem;\n  font-weight: 600;\n  color: #25282b;\n  margin: 16px 0;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.Customers-profilebox-YjxD8 {\n  width: 72px;\n  height: 72px;\n  border-radius: 50%;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Customers-profilebox-YjxD8 img {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Customers-newtworkContainer-WUsSr {\n  height: auto;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 32px 64px;\n  padding: 2rem 4rem;\n  background-color: #fff;\n}\n\n.Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm img {\n  width: 400px;\n  height: 450px;\n}\n\n.Customers-network-3OQoG {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: start;\n      align-items: flex-start;\n}\n\n.Customers-networkTitle-e7bL- {\n  font-size: 40px;\n  font-weight: 600;\n  color: #25282b;\n  text-align: left;\n}\n\n/* .network_content p {\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 2;\n} */\n\n.Customers-customers_container-22ceD {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #f7f7f7;\n}\n\n.Customers-customers_container2-X7fw- {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #fff;\n}\n\n.Customers-customer_review-c1UsK {\n  width: 50%;\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 64px;\n  padding: 4rem;\n}\n\n/* .customerLogo {\n  border-radius: 8%;\n} */\n\n.Customers-sriChaitanyaText-2FAda {\n  font-size: 24px;\n  font-weight: normal;\n  line-height: 1.5;\n  text-align: left;\n}\n\n.Customers-author-1T0Ew {\n  font-size: 14px;\n  line-height: 1.43;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-top: 16px;\n}\n\n.Customers-author_title-aiead {\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.2;\n  margin: 12px 0 8px 0;\n}\n\n.Customers-customers-1ngOt {\n  width: 50%;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  grid-row-gap: 0;\n  grid-column-gap: 0;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m {\n  -ms-flex: 1 1;\n      flex: 1 1;\n  height: 106px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m:nth-of-type(even) {\n  background-color: #fff;\n}\n\n.Customers-customers-1ngOt .Customers-client-dHi1m img {\n  -o-object-fit: contain;\n     object-fit: contain;\n  max-width: 200px;\n}\n\n@media only screen and (max-width: 990px) {\n  .Customers-scrollTop-2r3qa {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Customers-headerContainer-3EA99 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    text-align: center;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding: 24px;\n    background-color: #fff;\n  }\n\n  .Customers-header_text-1Nc_1 {\n    width: 100%;\n  }\n\n  /* .network_content > p {\n    font-size: 14px;\n    text-align: center;\n    line-height: 1.71;\n  } */\n\n  /* .trustedBy_text {\n    text-align: center;\n    max-width: 600px;\n    font-size: 14px;\n    line-height: 24px;\n    letter-spacing: -0.42px;\n    margin: 8px auto 0 auto;\n\n    p {\n      margin: 0;\n    }\n\n    p:nth-child(1) {\n      margin-bottom: 8px;\n    }\n  } */\n\n  .Customers-trustedByTitle-3pPTH {\n    font-size: 32px;\n  }\n\n  .Customers-header_box_container-3ATaO {\n    grid-template-columns: 1fr;\n    gap: 24px;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin-top: 24px;\n  }\n\n  .Customers-box-2g2DN {\n    width: 240px;\n    height: 240px;\n    padding: 36px 32px;\n  }\n\n    .Customers-box-2g2DN img {\n      width: 55px;\n      height: 55px;\n    }\n\n    .Customers-box-2g2DN .Customers-box_contents-1_6Sy {\n      font-size: 32px;\n      margin: 18px 0 12px 0;\n    }\n\n    .Customers-box-2g2DN .Customers-box_contents_tag-1-7OE {\n      font-size: 15px;\n      margin-bottom: 0;\n    }\n\n  .Customers-newtworkContainer-WUsSr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 100%;\n    padding: 1.5rem;\n  }\n\n  .Customers-newtworkContainer-WUsSr .Customers-countryimg-18rEm {\n    width: 100%;\n  }\n\n  .Customers-network-3OQoG {\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .Customers-networkTitle-e7bL- {\n    font-size: 32px;\n    text-align: center;\n  }\n\n  .Customers-customers_container-22ceD {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Customers-customer_review-c1UsK {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    padding: 32px 16px 24px;\n  }\n\n  .Customers-author-1T0Ew {\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  /* .customerLogo {\n    border-radius: 8px;\n  } */\n\n  .Customers-sriChaitanyaText-2FAda {\n    width: auto;\n    text-align: center;\n    font-size: 14px;\n    line-height: 1.7;\n  }\n\n  .Customers-customers-1ngOt {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: 0 auto;\n    grid-template-columns: repeat(3, 1fr);\n  }\n\n  .Customers-customers-1ngOt img {\n    width: 126px;\n    height: 68px;\n  }\n\n  .Customers-customers_container2-X7fw- {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/Customers.scss"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,uBAAuB;MACnB,oBAAoB;EACxB,cAAc;EACd,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,WAAW;CACZ;;AAED;EACE,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;;;;;;IAMI;;AAEJ;EACE,YAAY;EACZ,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,+BAA+B;EAC/B,UAAU;CACX;;AAED;EACE,aAAa;EACb,cAAc;EACd,0DAA0D;UAClD,kDAAkD;EAC1D,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,aAAa;EACb,oBAAoB;EACpB,oBAAoB;EACpB,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,eAAe;EACf,iBAAiB;EACjB,2BAA2B;KACxB,wBAAwB;CAC5B;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,mBAAmB;EACnB,mBAAmB;EACnB,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,YAAY;EACZ,0BAA0B;CAC3B;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,YAAY;EACZ,uBAAuB;CACxB;;AAED;EACE,WAAW;EACX,uBAAuB;EACvB,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,+BAA+B;EACnC,cAAc;EACd,cAAc;CACf;;AAED;;IAEI;;AAEJ;EACE,gBAAgB;EAChB,oBAAoB;EACpB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,qBAAqB;CACtB;;AAED;EACE,WAAW;EACX,cAAc;EACd,sCAAsC;EACtC,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,cAAc;MACV,UAAU;EACd,cAAc;EACd,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,0BAA0B;CAC3B;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,uBAAuB;KACpB,oBAAoB;EACvB,iBAAiB;CAClB;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,4BAA4B;IAC5B,yBAAyB;IACzB,oBAAoB;IACpB,mBAAmB;IACnB,qBAAqB;QACjB,4BAA4B;IAChC,cAAc;IACd,uBAAuB;GACxB;;EAED;IACE,YAAY;GACb;;EAED;;;;MAII;;EAEJ;;;;;;;;;;;;;;;MAeI;;EAEJ;IACE,gBAAgB;GACjB;;EAED;IACE,2BAA2B;IAC3B,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,aAAa;IACb,cAAc;IACd,mBAAmB;GACpB;;IAEC;MACE,YAAY;MACZ,aAAa;KACd;;IAED;MACE,gBAAgB;MAChB,sBAAsB;KACvB;;IAED;MACE,gBAAgB;MAChB,iBAAiB;KAClB;;EAEH;IACE,qBAAqB;IACrB,cAAc;IACd,mCAAmC;QAC/B,+BAA+B;IACnC,uBAAuB;QACnB,oBAAoB;IACxB,YAAY;IACZ,aAAa;IACb,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;;EAED;IACE,YAAY;IACZ,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;IACE,gBAAgB;IAChB,mBAAmB;GACpB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,YAAY;IACZ,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;IACnB,wBAAwB;GACzB;;EAED;IACE,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;;MAEI;;EAEJ;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;GACvC;;EAED;IACE,aAAa;IACb,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,mCAAmC;QAC/B,+BAA+B;GACpC;CACF","file":"Customers.scss","sourcesContent":[".headerContainer {\n  height: 80vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 64px;\n  background-color: #f7f7f7;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.header_text {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  width: 41%;\n}\n\n.trustedByTitle {\n  font-size: 64px;\n  font-size: 4rem;\n  font-weight: 600;\n  color: #25282b;\n}\n\n/* .trustedBy_text {\n  color: #000;\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 1.5;\n} */\n\n.trustedBy {\n  color: #f36;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.header_box_container {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  gap: 32px;\n}\n\n.box {\n  width: 320px;\n  height: 320px;\n  -webkit-box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n          box-shadow: 0 4px 40px 0 rgba(255, 51, 102, 0.12);\n  padding: 54px 40px;\n  border-radius: 8px;\n  text-align: center;\n}\n\n.box_contents_tag {\n  font-size: 20px;\n  color: #25282b;\n  opacity: 0.6;\n  font-weight: normal;\n  margin-bottom: 24px;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.box_contents {\n  font-size: 48px;\n  font-size: 3rem;\n  font-weight: 600;\n  color: #25282b;\n  margin: 16px 0;\n  overflow: hidden;\n  -o-text-overflow: ellipsis;\n     text-overflow: ellipsis;\n}\n\n.profilebox {\n  width: 72px;\n  height: 72px;\n  border-radius: 50%;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.profilebox img {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.newtworkContainer {\n  height: auto;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 32px 64px;\n  padding: 2rem 4rem;\n  background-color: #fff;\n}\n\n.newtworkContainer .countryimg {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.newtworkContainer .countryimg img {\n  width: 400px;\n  height: 450px;\n}\n\n.network {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: start;\n      align-items: flex-start;\n}\n\n.networkTitle {\n  font-size: 40px;\n  font-weight: 600;\n  color: #25282b;\n  text-align: left;\n}\n\n/* .network_content p {\n  opacity: 0.6;\n  font-size: 16px;\n  text-align: left;\n  line-height: 2;\n} */\n\n.customers_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #f7f7f7;\n}\n\n.customers_container2 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  width: 100%;\n  background-color: #fff;\n}\n\n.customer_review {\n  width: 50%;\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  padding: 64px;\n  padding: 4rem;\n}\n\n/* .customerLogo {\n  border-radius: 8%;\n} */\n\n.sriChaitanyaText {\n  font-size: 24px;\n  font-weight: normal;\n  line-height: 1.5;\n  text-align: left;\n}\n\n.author {\n  font-size: 14px;\n  line-height: 1.43;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-top: 16px;\n}\n\n.author_title {\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.2;\n  margin: 12px 0 8px 0;\n}\n\n.customers {\n  width: 50%;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  grid-row-gap: 0;\n  grid-column-gap: 0;\n}\n\n.customers .client {\n  -ms-flex: 1 1;\n      flex: 1 1;\n  height: 106px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background-color: #f7f7f7;\n}\n\n.customers .client:nth-of-type(even) {\n  background-color: #fff;\n}\n\n.customers .client img {\n  -o-object-fit: contain;\n     object-fit: contain;\n  max-width: 200px;\n}\n\n@media only screen and (max-width: 990px) {\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .headerContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    text-align: center;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding: 24px;\n    background-color: #fff;\n  }\n\n  .header_text {\n    width: 100%;\n  }\n\n  /* .network_content > p {\n    font-size: 14px;\n    text-align: center;\n    line-height: 1.71;\n  } */\n\n  /* .trustedBy_text {\n    text-align: center;\n    max-width: 600px;\n    font-size: 14px;\n    line-height: 24px;\n    letter-spacing: -0.42px;\n    margin: 8px auto 0 auto;\n\n    p {\n      margin: 0;\n    }\n\n    p:nth-child(1) {\n      margin-bottom: 8px;\n    }\n  } */\n\n  .trustedByTitle {\n    font-size: 32px;\n  }\n\n  .header_box_container {\n    grid-template-columns: 1fr;\n    gap: 24px;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin-top: 24px;\n  }\n\n  .box {\n    width: 240px;\n    height: 240px;\n    padding: 36px 32px;\n  }\n\n    .box img {\n      width: 55px;\n      height: 55px;\n    }\n\n    .box .box_contents {\n      font-size: 32px;\n      margin: 18px 0 12px 0;\n    }\n\n    .box .box_contents_tag {\n      font-size: 15px;\n      margin-bottom: 0;\n    }\n\n  .newtworkContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n    -ms-flex-align: center;\n        align-items: center;\n    width: 100%;\n    height: 100%;\n    padding: 1.5rem;\n  }\n\n  .newtworkContainer .countryimg {\n    width: 100%;\n  }\n\n  .network {\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .networkTitle {\n    font-size: 32px;\n    text-align: center;\n  }\n\n  .customers_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .customer_review {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    padding: 32px 16px 24px;\n  }\n\n  .author {\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  /* .customerLogo {\n    border-radius: 8px;\n  } */\n\n  .sriChaitanyaText {\n    width: auto;\n    text-align: center;\n    font-size: 14px;\n    line-height: 1.7;\n  }\n\n  .customers {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: 0 auto;\n    grid-template-columns: repeat(3, 1fr);\n  }\n\n  .customers img {\n    width: 126px;\n    height: 68px;\n  }\n\n  .customers_container2 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column-reverse;\n        flex-direction: column-reverse;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"headerContainer": "Customers-headerContainer-3EA99",
	"scrollTop": "Customers-scrollTop-2r3qa",
	"header_text": "Customers-header_text-1Nc_1",
	"trustedByTitle": "Customers-trustedByTitle-3pPTH",
	"trustedBy": "Customers-trustedBy-3FuEk",
	"header_box_container": "Customers-header_box_container-3ATaO",
	"box": "Customers-box-2g2DN",
	"box_contents_tag": "Customers-box_contents_tag-1-7OE",
	"box_contents": "Customers-box_contents-1_6Sy",
	"profilebox": "Customers-profilebox-YjxD8",
	"newtworkContainer": "Customers-newtworkContainer-WUsSr",
	"countryimg": "Customers-countryimg-18rEm",
	"network": "Customers-network-3OQoG",
	"networkTitle": "Customers-networkTitle-e7bL-",
	"customers_container": "Customers-customers_container-22ceD",
	"customers_container2": "Customers-customers_container2-X7fw-",
	"customer_review": "Customers-customer_review-c1UsK",
	"sriChaitanyaText": "Customers-sriChaitanyaText-2FAda",
	"author": "Customers-author-1T0Ew",
	"author_title": "Customers-author_title-aiead",
	"customers": "Customers-customers-1ngOt",
	"client": "Customers-client-dHi1m"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/Customers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Customers_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/customers/Customers.scss");
/* harmony import */ var _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Customers_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/Customers.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var Customers =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Customers, _React$Component);

  function Customers(props) {
    var _this;

    _classCallCheck(this, Customers);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Customers).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleScroll", function () {
      if (window.scrollY > 500) {
        _this.setState({
          showScroll: true
        });
      } else {
        _this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleScrollTop", function () {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });

      _this.setState({
        showScroll: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "displayScrollToTop", function () {
      var showScroll = _this.state.showScroll;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.scrollTop,
        role: "presentation",
        onClick: function onClick() {
          _this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayTrustedBy", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.headerContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.trustedByTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, "Trusted by", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.trustedBy,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, " 100+ Institutions"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.header_box_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMERS_STATS"].map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 87
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: item.icon,
          alt: item.label,
          height: "64px",
          width: "64px",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 88
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box_contents,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 89
          },
          __self: this
        }, item.number), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.box_contents_tag,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 90
          },
          __self: this
        }, item.text));
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayNetwork", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.newtworkContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.countryimg,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "images/customers/Group 1128.jpg",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.network,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.networkTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }, "A large network servicing ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }), "the whole country")));
    });

    _defineProperty(_assertThisInitialized(_this), "displayCustomersReview", function (data) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customer_review,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.sriChaitanyaText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, data.text)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.author,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.profilebox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/icons/person_blue.svg",
        alt: "profile",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.author_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, data.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, data.designation))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayCustomersGrid", function (data) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, data.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.client,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 141
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: item.icon,
          alt: item.label,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 142
          },
          __self: this
        }));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayCustomers1", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }, _this.displayCustomersReview(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMER_REVIEW"][0]), _this.displayCustomersGrid(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CLIENTS_SET1"]));
    });

    _defineProperty(_assertThisInitialized(_this), "displayCustomers2", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a.customers_container2,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }, _this.displayCustomersGrid(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CLIENTS_SET2"]), _this.displayCustomersReview(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_2__["CUSTOMER_REVIEW"][1]));
    });

    _this.state = {};
    return _this;
  }

  _createClass(Customers, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleScroll();
      window.addEventListener('scroll', this.handleScroll);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, this.displayTrustedBy()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, this.displayNetwork()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167
        },
        __self: this
      }, this.displayCustomers1()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, this.displayCustomers2()), this.displayScrollToTop());
    }
  }]);

  return Customers;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Customers_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Customers));

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/Customers.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/customers/Customers.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/customers/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Customers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/customers/Customers.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/customers/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: "Customers - Egnify's trusted online institutes and students",
              content: 'Our online education platform is enabling tutors and coaching institutes to go online instantly.',
              chunks: ['Customers'],
              keywords: 'students, teachers',
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 13
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Customers__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 14
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tZXJzLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY3VzdG9tZXJzL0N1c3RvbWVycy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycy9DdXN0b21lcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY3VzdG9tZXJzL0N1c3RvbWVycy5zY3NzP2JmZWQiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvY3VzdG9tZXJzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuQ3VzdG9tZXJzLWhlYWRlckNvbnRhaW5lci0zRUE5OSB7XFxuICBoZWlnaHQ6IDgwdmg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA2NHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLkN1c3RvbWVycy1zY3JvbGxUb3AtMnIzcWEge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5DdXN0b21lcnMtc2Nyb2xsVG9wLTJyM3FhIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLkN1c3RvbWVycy1oZWFkZXJfdGV4dC0xTmNfMSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgd2lkdGg6IDQxJTtcXG59XFxuXFxuLkN1c3RvbWVycy10cnVzdGVkQnlUaXRsZS0zcFBUSCB7XFxuICBmb250LXNpemU6IDY0cHg7XFxuICBmb250LXNpemU6IDRyZW07XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi8qIC50cnVzdGVkQnlfdGV4dCB7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbn0gKi9cXG5cXG4uQ3VzdG9tZXJzLXRydXN0ZWRCeS0zRnVFayB7XFxuICBjb2xvcjogI2YzNjtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWhlYWRlcl9ib3hfY29udGFpbmVyLTNBVGFPIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgZ2FwOiAzMnB4O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWJveC0yZzJETiB7XFxuICB3aWR0aDogMzIwcHg7XFxuICBoZWlnaHQ6IDMyMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCA0MHB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCA0MHB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTIpO1xcbiAgcGFkZGluZzogNTRweCA0MHB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWJveF9jb250ZW50c190YWctMS03T0Uge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWJveF9jb250ZW50cy0xXzZTeSB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBmb250LXNpemU6IDNyZW07XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW46IDE2cHggMDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLXByb2ZpbGVib3gtWWp4RDgge1xcbiAgd2lkdGg6IDcycHg7XFxuICBoZWlnaHQ6IDcycHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLXByb2ZpbGVib3gtWWp4RDggaW1nIHtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLW5ld3R3b3JrQ29udGFpbmVyLVdVc1NyIHtcXG4gIGhlaWdodDogYXV0bztcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHBhZGRpbmc6IDMycHggNjRweDtcXG4gIHBhZGRpbmc6IDJyZW0gNHJlbTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5DdXN0b21lcnMtbmV3dHdvcmtDb250YWluZXItV1VzU3IgLkN1c3RvbWVycy1jb3VudHJ5aW1nLTE4ckVtIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5DdXN0b21lcnMtbmV3dHdvcmtDb250YWluZXItV1VzU3IgLkN1c3RvbWVycy1jb3VudHJ5aW1nLTE4ckVtIGltZyB7XFxuICB3aWR0aDogNDAwcHg7XFxuICBoZWlnaHQ6IDQ1MHB4O1xcbn1cXG5cXG4uQ3VzdG9tZXJzLW5ldHdvcmstM09Rb0cge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxufVxcblxcbi5DdXN0b21lcnMtbmV0d29ya1RpdGxlLWU3YkwtIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblxcbi8qIC5uZXR3b3JrX2NvbnRlbnQgcCB7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgbGluZS1oZWlnaHQ6IDI7XFxufSAqL1xcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzX2NvbnRhaW5lci0yMmNlRCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzX2NvbnRhaW5lcjItWDdmdy0ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uQ3VzdG9tZXJzLWN1c3RvbWVyX3Jldmlldy1jMVVzSyB7XFxuICB3aWR0aDogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgcGFkZGluZzogNjRweDtcXG4gIHBhZGRpbmc6IDRyZW07XFxufVxcblxcbi8qIC5jdXN0b21lckxvZ28ge1xcbiAgYm9yZGVyLXJhZGl1czogOCU7XFxufSAqL1xcblxcbi5DdXN0b21lcnMtc3JpQ2hhaXRhbnlhVGV4dC0yRkFkYSB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblxcbi5DdXN0b21lcnMtYXV0aG9yLTFUMEV3IHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjQzO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi10b3A6IDE2cHg7XFxufVxcblxcbi5DdXN0b21lcnMtYXV0aG9yX3RpdGxlLWFpZWFkIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgbWFyZ2luOiAxMnB4IDAgOHB4IDA7XFxufVxcblxcbi5DdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIGdyaWQtcm93LWdhcDogMDtcXG4gIGdyaWQtY29sdW1uLWdhcDogMDtcXG59XFxuXFxuLkN1c3RvbWVycy1jdXN0b21lcnMtMW5nT3QgLkN1c3RvbWVycy1jbGllbnQtZEhpMW0ge1xcbiAgLW1zLWZsZXg6IDEgMTtcXG4gICAgICBmbGV4OiAxIDE7XFxuICBoZWlnaHQ6IDEwNnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLkN1c3RvbWVycy1jdXN0b21lcnMtMW5nT3QgLkN1c3RvbWVycy1jbGllbnQtZEhpMW06bnRoLW9mLXR5cGUoZXZlbikge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLkN1c3RvbWVycy1jdXN0b21lcnMtMW5nT3QgLkN1c3RvbWVycy1jbGllbnQtZEhpMW0gaW1nIHtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgbWF4LXdpZHRoOiAyMDBweDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLkN1c3RvbWVycy1zY3JvbGxUb3AtMnIzcWEge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1oZWFkZXJDb250YWluZXItM0VBOTkge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogLW1vei1maXQtY29udGVudDtcXG4gICAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZzogMjRweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtaGVhZGVyX3RleHQtMU5jXzEge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC8qIC5uZXR3b3JrX2NvbnRlbnQgPiBwIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjcxO1xcbiAgfSAqL1xcblxcbiAgLyogLnRydXN0ZWRCeV90ZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuNDJweDtcXG4gICAgbWFyZ2luOiA4cHggYXV0byAwIGF1dG87XFxuXFxuICAgIHAge1xcbiAgICAgIG1hcmdpbjogMDtcXG4gICAgfVxcblxcbiAgICBwOm50aC1jaGlsZCgxKSB7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAuQ3VzdG9tZXJzLXRydXN0ZWRCeVRpdGxlLTNwUFRIIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1oZWFkZXJfYm94X2NvbnRhaW5lci0zQVRhTyB7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICBnYXA6IDI0cHg7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBtYXJnaW4tdG9wOiAyNHB4O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1ib3gtMmcyRE4ge1xcbiAgICB3aWR0aDogMjQwcHg7XFxuICAgIGhlaWdodDogMjQwcHg7XFxuICAgIHBhZGRpbmc6IDM2cHggMzJweDtcXG4gIH1cXG5cXG4gICAgLkN1c3RvbWVycy1ib3gtMmcyRE4gaW1nIHtcXG4gICAgICB3aWR0aDogNTVweDtcXG4gICAgICBoZWlnaHQ6IDU1cHg7XFxuICAgIH1cXG5cXG4gICAgLkN1c3RvbWVycy1ib3gtMmcyRE4gLkN1c3RvbWVycy1ib3hfY29udGVudHMtMV82U3kge1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBtYXJnaW46IDE4cHggMCAxMnB4IDA7XFxuICAgIH1cXG5cXG4gICAgLkN1c3RvbWVycy1ib3gtMmcyRE4gLkN1c3RvbWVycy1ib3hfY29udGVudHNfdGFnLTEtN09FIHtcXG4gICAgICBmb250LXNpemU6IDE1cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgfVxcblxcbiAgLkN1c3RvbWVycy1uZXd0d29ya0NvbnRhaW5lci1XVXNTciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogMS41cmVtO1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1uZXd0d29ya0NvbnRhaW5lci1XVXNTciAuQ3VzdG9tZXJzLWNvdW50cnlpbWctMThyRW0ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtbmV0d29yay0zT1FvRyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtbmV0d29ya1RpdGxlLWU3YkwtIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLWN1c3RvbWVyc19jb250YWluZXItMjJjZUQge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtY3VzdG9tZXJfcmV2aWV3LWMxVXNLIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHBhZGRpbmc6IDMycHggMTZweCAyNHB4O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1hdXRob3ItMVQwRXcge1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC8qIC5jdXN0b21lckxvZ28ge1xcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICB9ICovXFxuXFxuICAuQ3VzdG9tZXJzLXNyaUNoYWl0YW55YVRleHQtMkZBZGEge1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjc7XFxuICB9XFxuXFxuICAuQ3VzdG9tZXJzLWN1c3RvbWVycy0xbmdPdCB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIH1cXG5cXG4gIC5DdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090IGltZyB7XFxuICAgIHdpZHRoOiAxMjZweDtcXG4gICAgaGVpZ2h0OiA2OHB4O1xcbiAgfVxcblxcbiAgLkN1c3RvbWVycy1jdXN0b21lcnNfY29udGFpbmVyMi1YN2Z3LSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycy9DdXN0b21lcnMuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGNBQWM7RUFDZCwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLFdBQVc7Q0FDWjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7Ozs7OztJQU1JOztBQUVKO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQiwyQkFBMkI7S0FDeEIsd0JBQXdCO0NBQzVCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLCtCQUErQjtFQUMvQixVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztFQUNkLDBEQUEwRDtVQUNsRCxrREFBa0Q7RUFDMUQsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQiwyQkFBMkI7S0FDeEIsd0JBQXdCO0NBQzVCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMkJBQTJCO0tBQ3hCLHdCQUF3QjtDQUM1Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQix1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztDQUNmOztBQUVEO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHNCQUFzQjtNQUNsQix3QkFBd0I7Q0FDN0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7Ozs7O0lBS0k7O0FBRUo7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsdUJBQXVCO01BQ25CLCtCQUErQjtFQUNuQyxZQUFZO0VBQ1osMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsWUFBWTtFQUNaLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsK0JBQStCO0VBQ25DLGNBQWM7RUFDZCxjQUFjO0NBQ2Y7O0FBRUQ7O0lBRUk7O0FBRUo7RUFDRSxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLGdCQUFnQjtFQUNoQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxjQUFjO01BQ1YsVUFBVTtFQUNkLGNBQWM7RUFDZCxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSx1QkFBdUI7S0FDcEIsb0JBQW9CO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLDRCQUE0QjtJQUM1Qix5QkFBeUI7SUFDekIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixxQkFBcUI7UUFDakIsNEJBQTRCO0lBQ2hDLGNBQWM7SUFDZCx1QkFBdUI7R0FDeEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7Ozs7TUFJSTs7RUFFSjs7Ozs7Ozs7Ozs7Ozs7O01BZUk7O0VBRUo7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSwyQkFBMkI7SUFDM0IsVUFBVTtJQUNWLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixjQUFjO0lBQ2QsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0UsWUFBWTtNQUNaLGFBQWE7S0FDZDs7SUFFRDtNQUNFLGdCQUFnQjtNQUNoQixzQkFBc0I7S0FDdkI7O0lBRUQ7TUFDRSxnQkFBZ0I7TUFDaEIsaUJBQWlCO0tBQ2xCOztFQUVIO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxtQ0FBbUM7UUFDL0IsK0JBQStCO0lBQ25DLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsWUFBWTtJQUNaLGFBQWE7SUFDYixnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osdUJBQXVCO1FBQ25CLG9CQUFvQjtHQUN6Qjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3Qjs7RUFFRDtJQUNFLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixtQkFBbUI7SUFDbkIsd0JBQXdCO0dBQ3pCOztFQUVEO0lBQ0UsdUJBQXVCO1FBQ25CLG9CQUFvQjtHQUN6Qjs7RUFFRDs7TUFFSTs7RUFFSjtJQUNFLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQ0FBc0M7R0FDdkM7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxtQ0FBbUM7UUFDL0IsK0JBQStCO0dBQ3BDO0NBQ0ZcIixcImZpbGVcIjpcIkN1c3RvbWVycy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5oZWFkZXJDb250YWluZXIge1xcbiAgaGVpZ2h0OiA4MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogNjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5zY3JvbGxUb3Age1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5zY3JvbGxUb3AgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4uaGVhZGVyX3RleHQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHdpZHRoOiA0MSU7XFxufVxcblxcbi50cnVzdGVkQnlUaXRsZSB7XFxuICBmb250LXNpemU6IDY0cHg7XFxuICBmb250LXNpemU6IDRyZW07XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi8qIC50cnVzdGVkQnlfdGV4dCB7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbn0gKi9cXG5cXG4udHJ1c3RlZEJ5IHtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIC1vLXRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxufVxcblxcbi5oZWFkZXJfYm94X2NvbnRhaW5lciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gIGdhcDogMzJweDtcXG59XFxuXFxuLmJveCB7XFxuICB3aWR0aDogMzIwcHg7XFxuICBoZWlnaHQ6IDMyMHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCA0MHB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCA0MHB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTIpO1xcbiAgcGFkZGluZzogNTRweCA0MHB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uYm94X2NvbnRlbnRzX3RhZyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIC1vLXRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xcbiAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxufVxcblxcbi5ib3hfY29udGVudHMge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgZm9udC1zaXplOiAzcmVtO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luOiAxNnB4IDA7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG59XFxuXFxuLnByb2ZpbGVib3gge1xcbiAgd2lkdGg6IDcycHg7XFxuICBoZWlnaHQ6IDcycHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4ucHJvZmlsZWJveCBpbWcge1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5uZXd0d29ya0NvbnRhaW5lciB7XFxuICBoZWlnaHQ6IGF1dG87XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBwYWRkaW5nOiAzMnB4IDY0cHg7XFxuICBwYWRkaW5nOiAycmVtIDRyZW07XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4ubmV3dHdvcmtDb250YWluZXIgLmNvdW50cnlpbWcge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLm5ld3R3b3JrQ29udGFpbmVyIC5jb3VudHJ5aW1nIGltZyB7XFxuICB3aWR0aDogNDAwcHg7XFxuICBoZWlnaHQ6IDQ1MHB4O1xcbn1cXG5cXG4ubmV0d29yayB7XFxuICB3aWR0aDogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG59XFxuXFxuLm5ldHdvcmtUaXRsZSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cXG4vKiAubmV0d29ya19jb250ZW50IHAge1xcbiAgb3BhY2l0eTogMC42O1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGxpbmUtaGVpZ2h0OiAyO1xcbn0gKi9cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyMiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICB3aWR0aDogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5jdXN0b21lcl9yZXZpZXcge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHBhZGRpbmc6IDY0cHg7XFxuICBwYWRkaW5nOiA0cmVtO1xcbn1cXG5cXG4vKiAuY3VzdG9tZXJMb2dvIHtcXG4gIGJvcmRlci1yYWRpdXM6IDglO1xcbn0gKi9cXG5cXG4uc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblxcbi5hdXRob3Ige1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNDM7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLXRvcDogMTZweDtcXG59XFxuXFxuLmF1dGhvcl90aXRsZSB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gIG1hcmdpbjogMTJweCAwIDhweCAwO1xcbn1cXG5cXG4uY3VzdG9tZXJzIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIGdyaWQtcm93LWdhcDogMDtcXG4gIGdyaWQtY29sdW1uLWdhcDogMDtcXG59XFxuXFxuLmN1c3RvbWVycyAuY2xpZW50IHtcXG4gIC1tcy1mbGV4OiAxIDE7XFxuICAgICAgZmxleDogMSAxO1xcbiAgaGVpZ2h0OiAxMDZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5jdXN0b21lcnMgLmNsaWVudDpudGgtb2YtdHlwZShldmVuKSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uY3VzdG9tZXJzIC5jbGllbnQgaW1nIHtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgbWF4LXdpZHRoOiAyMDBweDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLnNjcm9sbFRvcCB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuaGVhZGVyQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgaGVpZ2h0OiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDI0cHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB9XFxuXFxuICAuaGVhZGVyX3RleHQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC8qIC5uZXR3b3JrX2NvbnRlbnQgPiBwIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjcxO1xcbiAgfSAqL1xcblxcbiAgLyogLnRydXN0ZWRCeV90ZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuNDJweDtcXG4gICAgbWFyZ2luOiA4cHggYXV0byAwIGF1dG87XFxuXFxuICAgIHAge1xcbiAgICAgIG1hcmdpbjogMDtcXG4gICAgfVxcblxcbiAgICBwOm50aC1jaGlsZCgxKSB7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgICB9XFxuICB9ICovXFxuXFxuICAudHJ1c3RlZEJ5VGl0bGUge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICB9XFxuXFxuICAuaGVhZGVyX2JveF9jb250YWluZXIge1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgZ2FwOiAyNHB4O1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgbWFyZ2luLXRvcDogMjRweDtcXG4gIH1cXG5cXG4gIC5ib3gge1xcbiAgICB3aWR0aDogMjQwcHg7XFxuICAgIGhlaWdodDogMjQwcHg7XFxuICAgIHBhZGRpbmc6IDM2cHggMzJweDtcXG4gIH1cXG5cXG4gICAgLmJveCBpbWcge1xcbiAgICAgIHdpZHRoOiA1NXB4O1xcbiAgICAgIGhlaWdodDogNTVweDtcXG4gICAgfVxcblxcbiAgICAuYm94IC5ib3hfY29udGVudHMge1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBtYXJnaW46IDE4cHggMCAxMnB4IDA7XFxuICAgIH1cXG5cXG4gICAgLmJveCAuYm94X2NvbnRlbnRzX3RhZyB7XFxuICAgICAgZm9udC1zaXplOiAxNXB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG5cXG4gIC5uZXd0d29ya0NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbi1yZXZlcnNlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogMS41cmVtO1xcbiAgfVxcblxcbiAgLm5ld3R3b3JrQ29udGFpbmVyIC5jb3VudHJ5aW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAubmV0d29yayB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5uZXR3b3JrVGl0bGUge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jdXN0b21lcnNfY29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuY3VzdG9tZXJfcmV2aWV3IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHBhZGRpbmc6IDMycHggMTZweCAyNHB4O1xcbiAgfVxcblxcbiAgLmF1dGhvciB7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgLyogLmN1c3RvbWVyTG9nbyB7XFxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIH0gKi9cXG5cXG4gIC5zcmlDaGFpdGFueWFUZXh0IHtcXG4gICAgd2lkdGg6IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMS43O1xcbiAgfVxcblxcbiAgLmN1c3RvbWVycyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIH1cXG5cXG4gIC5jdXN0b21lcnMgaW1nIHtcXG4gICAgd2lkdGg6IDEyNnB4O1xcbiAgICBoZWlnaHQ6IDY4cHg7XFxuICB9XFxuXFxuICAuY3VzdG9tZXJzX2NvbnRhaW5lcjIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImhlYWRlckNvbnRhaW5lclwiOiBcIkN1c3RvbWVycy1oZWFkZXJDb250YWluZXItM0VBOTlcIixcblx0XCJzY3JvbGxUb3BcIjogXCJDdXN0b21lcnMtc2Nyb2xsVG9wLTJyM3FhXCIsXG5cdFwiaGVhZGVyX3RleHRcIjogXCJDdXN0b21lcnMtaGVhZGVyX3RleHQtMU5jXzFcIixcblx0XCJ0cnVzdGVkQnlUaXRsZVwiOiBcIkN1c3RvbWVycy10cnVzdGVkQnlUaXRsZS0zcFBUSFwiLFxuXHRcInRydXN0ZWRCeVwiOiBcIkN1c3RvbWVycy10cnVzdGVkQnktM0Z1RWtcIixcblx0XCJoZWFkZXJfYm94X2NvbnRhaW5lclwiOiBcIkN1c3RvbWVycy1oZWFkZXJfYm94X2NvbnRhaW5lci0zQVRhT1wiLFxuXHRcImJveFwiOiBcIkN1c3RvbWVycy1ib3gtMmcyRE5cIixcblx0XCJib3hfY29udGVudHNfdGFnXCI6IFwiQ3VzdG9tZXJzLWJveF9jb250ZW50c190YWctMS03T0VcIixcblx0XCJib3hfY29udGVudHNcIjogXCJDdXN0b21lcnMtYm94X2NvbnRlbnRzLTFfNlN5XCIsXG5cdFwicHJvZmlsZWJveFwiOiBcIkN1c3RvbWVycy1wcm9maWxlYm94LVlqeEQ4XCIsXG5cdFwibmV3dHdvcmtDb250YWluZXJcIjogXCJDdXN0b21lcnMtbmV3dHdvcmtDb250YWluZXItV1VzU3JcIixcblx0XCJjb3VudHJ5aW1nXCI6IFwiQ3VzdG9tZXJzLWNvdW50cnlpbWctMThyRW1cIixcblx0XCJuZXR3b3JrXCI6IFwiQ3VzdG9tZXJzLW5ldHdvcmstM09Rb0dcIixcblx0XCJuZXR3b3JrVGl0bGVcIjogXCJDdXN0b21lcnMtbmV0d29ya1RpdGxlLWU3YkwtXCIsXG5cdFwiY3VzdG9tZXJzX2NvbnRhaW5lclwiOiBcIkN1c3RvbWVycy1jdXN0b21lcnNfY29udGFpbmVyLTIyY2VEXCIsXG5cdFwiY3VzdG9tZXJzX2NvbnRhaW5lcjJcIjogXCJDdXN0b21lcnMtY3VzdG9tZXJzX2NvbnRhaW5lcjItWDdmdy1cIixcblx0XCJjdXN0b21lcl9yZXZpZXdcIjogXCJDdXN0b21lcnMtY3VzdG9tZXJfcmV2aWV3LWMxVXNLXCIsXG5cdFwic3JpQ2hhaXRhbnlhVGV4dFwiOiBcIkN1c3RvbWVycy1zcmlDaGFpdGFueWFUZXh0LTJGQWRhXCIsXG5cdFwiYXV0aG9yXCI6IFwiQ3VzdG9tZXJzLWF1dGhvci0xVDBFd1wiLFxuXHRcImF1dGhvcl90aXRsZVwiOiBcIkN1c3RvbWVycy1hdXRob3JfdGl0bGUtYWllYWRcIixcblx0XCJjdXN0b21lcnNcIjogXCJDdXN0b21lcnMtY3VzdG9tZXJzLTFuZ090XCIsXG5cdFwiY2xpZW50XCI6IFwiQ3VzdG9tZXJzLWNsaWVudC1kSGkxbVwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQge1xuICBDTElFTlRTX1NFVDEsXG4gIENMSUVOVFNfU0VUMixcbiAgQ1VTVE9NRVJTX1NUQVRTLFxuICBDVVNUT01FUl9SRVZJRVcsXG59IGZyb20gJy4uL0dldFJhbmtzQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vQ3VzdG9tZXJzLnNjc3MnO1xuXG5jbGFzcyBDdXN0b21lcnMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmhhbmRsZVNjcm9sbCgpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICB9XG5cbiAgaGFuZGxlU2Nyb2xsID0gKCkgPT4ge1xuICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+IDUwMCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IHRydWUsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZVNjcm9sbFRvcCA9ICgpID0+IHtcbiAgICB3aW5kb3cuc2Nyb2xsVG8oe1xuICAgICAgdG9wOiAwLFxuICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnLFxuICAgIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgZGlzcGxheVNjcm9sbFRvVG9wID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgc2hvd1Njcm9sbCB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgc2hvd1Njcm9sbCAmJiAoXG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3Muc2Nyb2xsVG9wfVxuICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlU2Nyb2xsVG9wKCk7XG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3Njcm9sbFRvcC5zdmdcIiBhbHQ9XCJzY3JvbGxUb3BcIiAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIClcbiAgICApO1xuICB9O1xuXG4gIGRpc3BsYXlUcnVzdGVkQnkgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGVyQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlcl90ZXh0fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudHJ1c3RlZEJ5VGl0bGV9PlxuICAgICAgICAgIFRydXN0ZWQgYnlcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50cnVzdGVkQnl9PiAxMDArIEluc3RpdHV0aW9uczwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgey8qIDxkaXYgY2xhc3NOYW1lPXtzLnRydXN0ZWRCeV90ZXh0fT5cbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWxcbiAgICAgICAgICAgIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGVcbiAgICAgICAgICAgIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbVxuICAgICAgICAgICAgc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWRcbiAgICAgICAgICAgIGVnZXQgcGVsbGVudGVzcXVlLlxuICAgICAgICAgIDwvcD5cbiAgICAgICAgPC9kaXY+ICovfVxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJfYm94X2NvbnRhaW5lcn0+XG4gICAgICAgIHtDVVNUT01FUlNfU1RBVFMubWFwKGl0ZW0gPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJveH0+XG4gICAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29ufSBhbHQ9e2l0ZW0ubGFiZWx9IGhlaWdodD1cIjY0cHhcIiB3aWR0aD1cIjY0cHhcIiAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm94X2NvbnRlbnRzfT57aXRlbS5udW1iZXJ9PC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3hfY29udGVudHNfdGFnfT57aXRlbS50ZXh0fTwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlOZXR3b3JrID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLm5ld3R3b3JrQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvdW50cnlpbWd9PlxuICAgICAgICA8aW1nIHNyYz1cImltYWdlcy9jdXN0b21lcnMvR3JvdXAgMTEyOC5qcGdcIiBhbHQ9XCJcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5uZXR3b3JrfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubmV0d29ya1RpdGxlfT5cbiAgICAgICAgICBBIGxhcmdlIG5ldHdvcmsgc2VydmljaW5nIDxiciAvPlxuICAgICAgICAgIHRoZSB3aG9sZSBjb3VudHJ5XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9e3MubmV0d29ya19jb250ZW50fT5cbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWxcbiAgICAgICAgICAgIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGVcbiAgICAgICAgICAgIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCxcbiAgICAgICAgICAgIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaFxuICAgICAgICAgICAgdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS5cbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PiAqL31cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDdXN0b21lcnNSZXZpZXcgPSBkYXRhID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5jdXN0b21lcl9yZXZpZXd9PlxuICAgICAgPGRpdj5cbiAgICAgICAgey8qIDxpbWcgc3JjPXtkYXRhLmxvZ299IGFsdD17ZGF0YS5sYWJlbH0gY2xhc3NOYW1lPXtzLmN1c3RvbWVyTG9nb30gLz4gKi99XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNyaUNoYWl0YW55YVRleHR9PntkYXRhLnRleHR9PC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF1dGhvcn0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByb2ZpbGVib3h9PlxuICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9pY29ucy9wZXJzb25fYmx1ZS5zdmdcIiBhbHQ9XCJwcm9maWxlXCIgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXV0aG9yX3RpdGxlfT57ZGF0YS5uYW1lfTwvZGl2PlxuICAgICAgICAgIDxkaXY+e2RhdGEuZGVzaWduYXRpb259PC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUN1c3RvbWVyc0dyaWQgPSBkYXRhID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5jdXN0b21lcnN9PlxuICAgICAge2RhdGEubWFwKGl0ZW0gPT4gKFxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jbGllbnR9PlxuICAgICAgICAgIDxpbWcgc3JjPXtpdGVtLmljb259IGFsdD17aXRlbS5sYWJlbH0gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApKX1cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Q3VzdG9tZXJzMSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5jdXN0b21lcnNfY29udGFpbmVyfT5cbiAgICAgIHt0aGlzLmRpc3BsYXlDdXN0b21lcnNSZXZpZXcoQ1VTVE9NRVJfUkVWSUVXWzBdKX1cbiAgICAgIHt0aGlzLmRpc3BsYXlDdXN0b21lcnNHcmlkKENMSUVOVFNfU0VUMSl9XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUN1c3RvbWVyczIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJzX2NvbnRhaW5lcjJ9PlxuICAgICAge3RoaXMuZGlzcGxheUN1c3RvbWVyc0dyaWQoQ0xJRU5UU19TRVQyKX1cbiAgICAgIHt0aGlzLmRpc3BsYXlDdXN0b21lcnNSZXZpZXcoQ1VTVE9NRVJfUkVWSUVXWzFdKX1cbiAgICA8L2Rpdj5cbiAgKTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheVRydXN0ZWRCeSgpfTwvZGl2PlxuICAgICAgICA8ZGl2Pnt0aGlzLmRpc3BsYXlOZXR3b3JrKCl9PC9kaXY+XG4gICAgICAgIDxkaXY+e3RoaXMuZGlzcGxheUN1c3RvbWVyczEoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5kaXNwbGF5Q3VzdG9tZXJzMigpfTwvZGl2PlxuICAgICAgICB7dGhpcy5kaXNwbGF5U2Nyb2xsVG9Ub3AoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShDdXN0b21lcnMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DdXN0b21lcnMuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9DdXN0b21lcnMuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0N1c3RvbWVycy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgQ3VzdG9tZXJzIGZyb20gJy4vQ3VzdG9tZXJzJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiBcIkN1c3RvbWVycyAtIEVnbmlmeSdzIHRydXN0ZWQgb25saW5lIGluc3RpdHV0ZXMgYW5kIHN0dWRlbnRzXCIsXG4gICAgY29udGVudDpcbiAgICAgICdPdXIgb25saW5lIGVkdWNhdGlvbiBwbGF0Zm9ybSBpcyBlbmFibGluZyB0dXRvcnMgYW5kIGNvYWNoaW5nIGluc3RpdHV0ZXMgdG8gZ28gb25saW5lIGluc3RhbnRseS4nLFxuICAgIGNodW5rczogWydDdXN0b21lcnMnXSxcbiAgICBrZXl3b3JkczogJ3N0dWRlbnRzLCB0ZWFjaGVycycsXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0IGZvb3RlckFzaD5cbiAgICAgICAgPEN1c3RvbWVycyAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBZUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQXpCQTtBQTJCQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQW5DQTtBQW9DQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBcERBO0FBcURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQXJCQTtBQUNBO0FBdERBO0FBcUZBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFDQTtBQXRGQTtBQTRHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWkE7QUFDQTtBQTdHQTtBQThIQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUZBO0FBQ0E7QUEvSEE7QUF3SUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQ0E7QUF6SUE7QUErSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQ0E7QUE5SUE7QUFGQTtBQUdBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQTBJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUFqS0E7QUFDQTtBQW1LQTs7Ozs7OztBQzlLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7OztBQWVBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=