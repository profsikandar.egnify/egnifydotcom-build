(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Doubts"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Doubts/Doubts.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "* {\n  margin: 0;\n  padding: 0;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n.Doubts-root-1zjON {\n  overflow: hidden;\n}\n\n.Doubts-teachContainer-1fJ2d {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #d6ffed;\n  position: relative;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-heading-pM5GB {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n/* .teachContainer .heading .learningText {\n  width: 316px;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n} */\n\n.Doubts-headerbackgroundmobile-2QqpM img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n/* .teachContainer .heading .learningText img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n} */\n\n.Doubts-teachContainer-1fJ2d .Doubts-contentText-1I74_ {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-requestDemo-saVek {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM .Doubts-whatsapp-uVHvS {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM img {\n  width: 32px;\n  height: 32px;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-downloadApp-1Dxtv {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-downloadApp-1Dxtv .Doubts-downloadText-25SfB {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-downloadApp-1Dxtv .Doubts-playStoreIcon-8n7JB {\n  width: 132px;\n  height: 40px;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-breadcrum-2R3nS {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.Doubts-displayClients-3vuci span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-breadcrum-2R3nS span {\n  font-weight: 600;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S .Doubts-teachSection-1Odnm {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* .teachContainer .topSection .teachSection .teachImgBox {\n  width: 40px;\n  height: 40px;\n} */\n\n/* .teachContainer .topSection .teachSection .teachImgBox img {\n  width: 100%;\n  height: 100%;\n  object-fit: contain;\n} */\n\n.Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S .Doubts-teachSection-1Odnm .Doubts-teachSectionName-1rm2I {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #095;\n  font-weight: bold;\n}\n\n.Doubts-headerbackgroundmobile-2QqpM {\n  position: absolute;\n  width: 584px;\n  height: 508px;\n  right: 52px;\n  bottom: 0;\n}\n\n.Doubts-displayClients-3vuci {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.Doubts-displayClients-3vuci h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Doubts-displayClients-3vuci .Doubts-clientsWrapper-yp9wC {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Doubts-displayClients-3vuci .Doubts-clientsWrapper-yp9wC .Doubts-client-3oHCb {\n  width: 172px;\n  height: 112px;\n}\n\n.Doubts-displayClients-3vuci span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Doubts-displayClients-3vuci span a:hover {\n  text-decoration: underline;\n}\n\n.Doubts-achievedContainer-2bD7W {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedHeading-3cYzM {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-achievedProfile-23nuJ {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-achievedProfile-23nuJ.Doubts-clients-36OmD {\n  background-color: #fff3eb;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-achievedProfile-23nuJ.Doubts-students-2tHVN {\n  background-color: #f7effe;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-achievedProfile-23nuJ.Doubts-tests-2pFLY {\n  background-color: #ffebf0;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-achievedProfile-23nuJ.Doubts-questions-24jyE {\n  background-color: #ebffef;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-highlight-r5CRW {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-highlight-r5CRW.Doubts-questionsHighlight-3xuB6 {\n  color: #00ac26;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-highlight-r5CRW.Doubts-testsHighlight-37I5Y {\n  color: #f36;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-highlight-r5CRW.Doubts-studentsHighlight-GxwHP {\n  color: #8c00fe;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-highlight-r5CRW.Doubts-clientsHighlight-3GUTh {\n  color: #f60;\n}\n\n.Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa .Doubts-subText-1Px6P {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Doubts-rolesContainer-1L5aT {\n  width: 100%;\n  height: 560px;\n  padding: 145px 64px;\n  background-color: #f7f7f7;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 {\n    width: 60%;\n  }\n\n.Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN {\n      width: 100%;\n      max-width: 580px;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN h2 {\n        margin: 0;\n        font-size: 40px;\n        letter-spacing: -1.2px;\n        line-height: 48px;\n        color: #25282b;\n      }\n\n.Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN p {\n        margin: 12px 0 0 0;\n        font-size: 20px;\n        line-height: 32px;\n        color: #25282b;\n      }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ {\n    width: 40%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n  }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N {\n      width: 480px;\n      height: 270px;\n      -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n              box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n      background-color: #fff;\n      position: relative;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-topCard-3jgiO {\n        position: absolute;\n        top: 0;\n        left: 0;\n        width: 100%;\n        height: 100%;\n        z-index: 1;\n        background-color: #fff;\n      }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-topCard-3jgiO img {\n          width: 100%;\n          height: 100%;\n        }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-topCircle-3ec90 {\n        position: absolute;\n        width: 64px;\n        height: 64px;\n        border-radius: 50%;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -32px;\n        left: 341px;\n      }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-bottomCircle-2qapv {\n        position: absolute;\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        border-radius: 50%;\n        opacity: 0.2;\n        bottom: -24px;\n        left: -24px;\n      }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-untilresolved-3YrBq .Doubts-topCircle-3ec90 {\n      width: 56px;\n      height: 56px;\n      background-color: #f36;\n      opacity: 0.1;\n      top: -28px;\n      left: -28px;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-untilresolved-3YrBq .Doubts-bottomCircle-2qapv {\n      width: 72px;\n      height: 72px;\n      background-color: #3fc;\n      opacity: 0.2;\n      bottom: -36px;\n      left: 348px;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-askanytime-2MjIR .Doubts-topCircle-3ec90 {\n      width: 64px;\n      height: 64px;\n      background-color: #3fc;\n      opacity: 0.3;\n      top: -32px;\n      left: 98px;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-askanytime-2MjIR .Doubts-bottomCircle-2qapv {\n      width: 56px;\n      height: 56px;\n      background-color: #f2e5fe;\n      bottom: -24px;\n      left: 407px;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-accessandanalyse-3apu8 .Doubts-topCircle-3ec90 {\n      width: 56px;\n      height: 56px;\n      background-color: #feb546;\n      opacity: 0.2;\n      top: -28px;\n      left: -28px;\n    }\n\n.Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-accessandanalyse-3apu8 .Doubts-bottomCircle-2qapv {\n      width: 72px;\n      height: 72px;\n      background-color: #0076ff;\n      opacity: 0.2;\n      bottom: -36px;\n      left: 348px;\n    }\n\n.Doubts-availableContainer-25Qda {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-desktopImage-3A1WM {\n      width: 696px;\n      height: 371px;\n    }\n\n.Doubts-availableTitle-1oPoo {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Doubts-available-2ypZi {\n  color: #f36;\n}\n\n.Doubts-availableContentSection-3jhve {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Doubts-platformContainer-1uSP3 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.Doubts-platform-XIF0Q {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.Doubts-platformOs-10uUp {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.Doubts-row-hkMoO {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.Doubts-store-3-5Xh {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.Doubts-store-3-5Xh .Doubts-playstore-11M66 {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.Doubts-store-3-5Xh .Doubts-appstore-3YK5j {\n    width: 136px;\n    height: 40px;\n  }\n\n.Doubts-joinCommunity-704Kz {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Doubts-joinFbText-2M2jI {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.Doubts-joinFbLink-3eSQO {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.Doubts-rolesContainer-1L5aT.Doubts-reverse-1mvh5 {\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  background-color: #fff;\n}\n\n.Doubts-rolesContainer-1L5aT.Doubts-reverse-1mvh5 .Doubts-contentPart-Flyb4 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n  }\n\n.Doubts-rolesContainer-1L5aT.Doubts-reverse-1mvh5 .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN {\n      max-width: 594px;\n    }\n\n@media only screen and (max-width: 990px) {\n  .Doubts-teachContainer-1fJ2d .Doubts-maxContainer-2XbtV {\n    position: static;\n    max-width: 450px;\n  }\n\n  .Doubts-teachContainer-1fJ2d .Doubts-breadcrum-2R3nS {\n    display: none;\n    text-align: center;\n  }\n\n  .Doubts-headerbackgroundmobile-2QqpM {\n    width: 319px;\n    height: 277px;\n    right: 0;\n    left: 0;\n    margin: auto;\n  }\n\n  .Doubts-teachContainer-1fJ2d .Doubts-downloadApp-1Dxtv {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .Doubts-displayClients-3vuci {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .Doubts-displayClients-3vuci h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Doubts-displayClients-3vuci .Doubts-clientsWrapper-yp9wC {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Doubts-displayClients-3vuci .Doubts-clientsWrapper-yp9wC .Doubts-client-3oHCb {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .Doubts-displayClients-3vuci span {\n    max-width: 307px;\n  }\n\n  .Doubts-achievedContainer-2bD7W {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .Doubts-achievedContainer-2bD7W .Doubts-achievedHeading-3cYzM {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .Doubts-achievedContainer-2bD7W .Doubts-achievedRow-1pjy_ .Doubts-card-19Zwa:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .Doubts-teachContainer-1fJ2d {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .Doubts-teachContainer-1fJ2d .Doubts-heading-pM5GB {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n\n      /* .learningText {\n        width: 210px;\n\n        img {\n          left: 0;\n          min-width: 140px;\n        }\n      } */\n    }\n\n    .Doubts-teachContainer-1fJ2d .Doubts-contentText-1I74_ {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-requestDemo-saVek {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM .Doubts-whatsapp-uVHvS {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .Doubts-teachContainer-1fJ2d .Doubts-buttonwrapper-oZ33z .Doubts-whatsappwrapper-3MBLM img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S .Doubts-teachSection-1Odnm .Doubts-teachImgBox-3uYfB {\n          width: 32px;\n          height: 32px;\n        }\n\n        .Doubts-teachContainer-1fJ2d .Doubts-topSection-31d4S .Doubts-teachSection-1Odnm .Doubts-teachSectionName-1rm2I {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  /* .headerContainer {\n    height: 480px;\n    padding: 32px 64px;\n\n    .topSection {\n      .teachSection {\n        flex-direction: column;\n        align-items: center;\n\n        .teachimgbox {\n          width: 48px;\n          height: 48px;\n\n          img {\n            width: 27px;\n            height: 27px;\n          }\n        }\n\n        .teachSectionName {\n          font-size: 32px;\n          line-height: 48px;\n          text-align: center;\n          margin: 8px 0;\n        }\n      }\n    }\n  } */\n\n  .Doubts-rolesContainer-1L5aT {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 40px 16px 56px 16px;\n    height: 457px;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n  }\n\n    .Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 {\n      width: 100%;\n    }\n\n      .Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN {\n        margin: auto;\n      }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN h2 {\n          font-size: 24px;\n          text-align: center;\n          line-height: normal;\n        }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-contentPart-Flyb4 .Doubts-content-L1WoN p {\n          font-size: 14px;\n          line-height: 24px;\n          text-align: center;\n          margin-top: 16px;\n          margin-bottom: 40px;\n        }\n\n    .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ {\n      width: 100%;\n      -ms-flex-pack: center;\n          justify-content: center;\n    }\n\n      .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N {\n        width: 320px;\n        height: 180px;\n      }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-topCircle-3ec90 {\n          width: 48px;\n          height: 48px;\n        }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ .Doubts-emptyCard-11L4N .Doubts-bottomCircle-2qapv {\n          width: 48px;\n          height: 48px;\n        }\n      .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-doubtsturnup-1vSBI .Doubts-topCircle-3ec90 {\n        background-color: #feb546;\n        opacity: 0.2;\n        width: 32px;\n        height: 32px;\n        left: 24px;\n        top: -16px;\n      }\n\n      .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-doubtsturnup-1vSBI .Doubts-bottomCircle-2qapv {\n        background-color: #0076ff;\n        opacity: 0.2;\n        left: 248px;\n        bottom: -24px;\n      }\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-untilresolved-3YrBq .Doubts-emptyCard-11L4N .Doubts-topCircle-3ec90 {\n          background-color: #f36;\n          opacity: 0.1;\n          width: 32px;\n          height: 32px;\n          top: -16px;\n          left: 264px;\n        }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-untilresolved-3YrBq .Doubts-emptyCard-11L4N .Doubts-bottomCircle-2qapv {\n          background-color: #3fc;\n          opacity: 0.2;\n          left: 24px;\n          bottom: -24px;\n        }\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-askanytime-2MjIR .Doubts-emptyCard-11L4N .Doubts-topCircle-3ec90 {\n          background-color: #f36;\n          opacity: 0.1;\n          width: 32px;\n          height: 32px;\n          left: 24px;\n          top: -16px;\n        }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-askanytime-2MjIR .Doubts-emptyCard-11L4N .Doubts-bottomCircle-2qapv {\n          background-color: #f2e5fe;\n          opacity: 1;\n          bottom: -24px;\n          left: 248px;\n        }\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-accessandanalyse-3apu8 .Doubts-emptyCard-11L4N .Doubts-topCircle-3ec90 {\n          background-color: #feb546;\n          opacity: 0.2;\n          width: 32px;\n          height: 32px;\n          left: 268px;\n          top: -16px;\n        }\n\n        .Doubts-rolesContainer-1L5aT .Doubts-imagePart-20GcQ.Doubts-accessandanalyse-3apu8 .Doubts-emptyCard-11L4N .Doubts-bottomCircle-2qapv {\n          background-color: #0076ff;\n          opacity: 0.2;\n          left: 24px;\n          bottom: -24px;\n        }\n\n  .Doubts-availableContainer-25Qda {\n    padding: 32px 32px 0 32px;\n  }\n\n    .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ {\n      grid-template-columns: 1fr;\n    }\n\n      .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-availableTitle-1oPoo {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-row-hkMoO {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-row-hkMoO .Doubts-platformContainer-1uSP3 {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-row-hkMoO .Doubts-platformContainer-1uSP3 .Doubts-platform-XIF0Q {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-row-hkMoO .Doubts-platformContainer-1uSP3 .Doubts-platformOs-10uUp {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-row-hkMoO .Doubts-platformContainer-1uSP3:last-child {\n          margin-right: 0;\n        }\n\n      .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-desktopImage-3A1WM {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-store-3-5Xh {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-store-3-5Xh .Doubts-playstore-11M66 {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .Doubts-availableContainer-25Qda .Doubts-availableRow-3TUG_ .Doubts-store-3-5Xh .Doubts-appstore-3YK5j {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .Doubts-joinCommunity-704Kz {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .Doubts-joinFbText-2M2jI {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .Doubts-joinFbLink-3eSQO {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Doubts-rolesContainer-1L5aT.Doubts-reverse-1mvh5 {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Doubts/Doubts.scss"],"names":[],"mappings":"AAAA;EACE,UAAU;EACV,WAAW;EACX,+BAA+B;UACvB,uBAAuB;CAChC;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,kBAAkB;EAClB,wBAAwB;EACxB,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;CACnB;;AAED;;;;;IAKI;;AAEJ;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;;;;;;;IAOI;;AAEJ;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,gBAAgB;EAChB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,0BAA0B;EAC1B,qBAAqB;EACrB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,aAAa;EACb,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;;;IAGI;;AAEJ;;;;IAII;;AAEJ;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;CACnB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,cAAc;EACd,YAAY;EACZ,UAAU;CACX;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;;IAEI;;AAEJ;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,sCAAsC;EACtC,UAAU;EACV,UAAU;EACV,aAAa;CACd;;AAED;EACE,aAAa;EACb,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,6BAA6B;EAC7B,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,sBAAsB;EACtB,+DAA+D;UACvD,uDAAuD;EAC/D,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,cAAc;EACd,oBAAoB;EACpB,0BAA0B;EAC1B,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;IACI,WAAW;GACZ;;AAEH;MACM,YAAY;MACZ,iBAAiB;MACjB,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;KAC5B;;AAEL;QACQ,UAAU;QACV,gBAAgB;QAChB,uBAAuB;QACvB,kBAAkB;QAClB,eAAe;OAChB;;AAEP;QACQ,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;QAClB,eAAe;OAChB;;AAEP;IACI,WAAW;IACX,qBAAqB;IACrB,cAAc;IACd,mBAAmB;QACf,0BAA0B;GAC/B;;AAEH;MACM,aAAa;MACb,cAAc;MACd,mDAAmD;cAC3C,2CAA2C;MACnD,uBAAuB;MACvB,mBAAmB;KACpB;;AAEL;QACQ,mBAAmB;QACnB,OAAO;QACP,QAAQ;QACR,YAAY;QACZ,aAAa;QACb,WAAW;QACX,uBAAuB;OACxB;;AAEP;UACU,YAAY;UACZ,aAAa;SACd;;AAET;QACQ,mBAAmB;QACnB,YAAY;QACZ,aAAa;QACb,mBAAmB;QACnB,0BAA0B;QAC1B,aAAa;QACb,WAAW;QACX,YAAY;OACb;;AAEP;QACQ,mBAAmB;QACnB,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,mBAAmB;QACnB,aAAa;QACb,cAAc;QACd,YAAY;OACb;;AAEP;MACM,YAAY;MACZ,aAAa;MACb,uBAAuB;MACvB,aAAa;MACb,WAAW;MACX,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,uBAAuB;MACvB,aAAa;MACb,cAAc;MACd,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,uBAAuB;MACvB,aAAa;MACb,WAAW;MACX,WAAW;KACZ;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,cAAc;MACd,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,aAAa;MACb,WAAW;MACX,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,aAAa;MACb,cAAc;MACd,YAAY;KACb;;AAEL;EACE,+BAA+B;EAC/B,+BAA+B;EAC/B,iBAAiB;EACjB,0BAA0B;CAC3B;;AAED;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;KACf;;AAEL;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,sBAAsB;EACtB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,qBAAqB;MACjB,4BAA4B;EAChC,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,aAAa;IACb,aAAa;IACb,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,aAAa;GACd;;AAEH;EACE,YAAY;EACZ,cAAc;EACd,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;CACb;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,gCAAgC;MAC5B,4BAA4B;EAChC,uBAAuB;CACxB;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,mBAAmB;QACf,0BAA0B;GAC/B;;AAEH;MACM,iBAAiB;KAClB;;AAEL;EACE;IACE,iBAAiB;IACjB,iBAAiB;GAClB;;EAED;IACE,cAAc;IACd,mBAAmB;GACpB;;EAED;IACE,aAAa;IACb,cAAc;IACd,SAAS;IACT,QAAQ;IACR,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;GACd;;EAED;;MAEI;;EAEJ;IACE,iBAAiB;GAClB;;EAED;IACE,mBAAmB;IACnB,2DAA2D;GAC5D;;IAEC;MACE,gBAAgB;MAChB,oBAAoB;MACpB,yBAAyB;MACzB,iBAAiB;KAClB;;IAED;MACE,2BAA2B;MAC3B,UAAU;KACX;;MAEC;QACE,YAAY;QACZ,iBAAiB;QACjB,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;QACnB,kBAAkB;QAClB,cAAc;;QAEd;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;;QAEb;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;OACd;;EAEL;IACE,cAAc;IACd,mBAAmB;IACnB,mBAAmB;GACpB;;IAEC;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,gBAAgB;MAChB,yBAAyB;;MAEzB;;;;;;;UAOI;KACL;;IAED;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,iBAAiB;MACjB,yBAAyB;KAC1B;;IAED;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,iBAAiB;KAClB;;MAEC;QACE,kBAAkB;QAClB,gBAAgB;QAChB,kBAAkB;QAClB,gBAAgB;OACjB;;MAED;QACE,iBAAiB;QACjB,oBAAoB;OACrB;;QAEC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,WAAW;UACX,eAAe;SAChB;;QAED;UACE,YAAY;UACZ,aAAa;SACd;;IAEL;MACE,sBAAsB;UAClB,wBAAwB;MAC5B,cAAc;;MAEd;;UAEI;KACL;QACG;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,gBAAgB;UAChB,kBAAkB;SACnB;;EAEP;;;;;;;;;;;;;;;;;;;;;;;;;;;MA2BI;;EAEJ;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,6BAA6B;IAC7B,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,0BAA0B;GAC3B;;IAEC;MACE,YAAY;KACb;;MAEC;QACE,aAAa;OACd;;QAEC;UACE,gBAAgB;UAChB,mBAAmB;UACnB,oBAAoB;SACrB;;QAED;UACE,gBAAgB;UAChB,kBAAkB;UAClB,mBAAmB;UACnB,iBAAiB;UACjB,oBAAoB;SACrB;;IAEL;MACE,YAAY;MACZ,sBAAsB;UAClB,wBAAwB;KAC7B;;MAEC;QACE,aAAa;QACb,cAAc;OACf;;QAEC;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,YAAY;UACZ,aAAa;SACd;MACH;QACE,0BAA0B;QAC1B,aAAa;QACb,YAAY;QACZ,aAAa;QACb,WAAW;QACX,WAAW;OACZ;;MAED;QACE,0BAA0B;QAC1B,aAAa;QACb,YAAY;QACZ,cAAc;OACf;QACC;UACE,uBAAuB;UACvB,aAAa;UACb,YAAY;UACZ,aAAa;UACb,WAAW;UACX,YAAY;SACb;;QAED;UACE,uBAAuB;UACvB,aAAa;UACb,WAAW;UACX,cAAc;SACf;QACD;UACE,uBAAuB;UACvB,aAAa;UACb,YAAY;UACZ,aAAa;UACb,WAAW;UACX,WAAW;SACZ;;QAED;UACE,0BAA0B;UAC1B,WAAW;UACX,cAAc;UACd,YAAY;SACb;QACD;UACE,0BAA0B;UAC1B,aAAa;UACb,YAAY;UACZ,aAAa;UACb,YAAY;UACZ,WAAW;SACZ;;QAED;UACE,0BAA0B;UAC1B,aAAa;UACb,WAAW;UACX,cAAc;SACf;;EAEP;IACE,0BAA0B;GAC3B;;IAEC;MACE,2BAA2B;KAC5B;;MAEC;QACE,gBAAgB;QAChB,mBAAmB;QACnB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,oBAAoB;QACpB,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,YAAY;UACZ,qBAAqB;cACjB,4BAA4B;UAChC,mBAAmB;UACnB,iBAAiB;SAClB;;UAEC;YACE,kBAAkB;YAClB,kBAAkB;WACnB;;UAED;YACE,gBAAgB;YAChB,kBAAkB;WACnB;;QAEH;UACE,gBAAgB;SACjB;;MAEH;QACE,aAAa;QACb,cAAc;QACd,aAAa;OACd;;MAED;QACE,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,aAAa;UACb,aAAa;UACb,mBAAmB;SACpB;;QAED;UACE,aAAa;UACb,aAAa;UACb,UAAU;SACX;;EAEP;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;GACzB;CACF","file":"Doubts.scss","sourcesContent":["* {\n  margin: 0;\n  padding: 0;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n.root {\n  overflow: hidden;\n}\n\n.teachContainer {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #d6ffed;\n  position: relative;\n}\n\n.teachContainer .heading {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n/* .teachContainer .heading .learningText {\n  width: 316px;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n} */\n\n.headerbackgroundmobile img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n/* .teachContainer .heading .learningText img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n} */\n\n.teachContainer .contentText {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.teachContainer .buttonwrapper {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .requestDemo {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper img {\n  width: 32px;\n  height: 32px;\n}\n\n.teachContainer .downloadApp {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.teachContainer .downloadApp .downloadText {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.teachContainer .downloadApp .playStoreIcon {\n  width: 132px;\n  height: 40px;\n}\n\n.teachContainer .breadcrum {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.teachContainer .breadcrum span {\n  font-weight: 600;\n}\n\n.teachContainer .topSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.teachContainer .topSection .teachSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n/* .teachContainer .topSection .teachSection .teachImgBox {\n  width: 40px;\n  height: 40px;\n} */\n\n/* .teachContainer .topSection .teachSection .teachImgBox img {\n  width: 100%;\n  height: 100%;\n  object-fit: contain;\n} */\n\n.teachContainer .topSection .teachSection .teachSectionName {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #095;\n  font-weight: bold;\n}\n\n.headerbackgroundmobile {\n  position: absolute;\n  width: 584px;\n  height: 508px;\n  right: 52px;\n  bottom: 0;\n}\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients .clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.displayClients .clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.achievedContainer .achievedRow {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.achievedContainer .achievedRow .card {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n  background-color: #fff3eb;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n  background-color: #f7effe;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n  background-color: #ffebf0;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n  background-color: #ebffef;\n}\n\n.achievedContainer .achievedRow .card .highlight {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n  color: #00ac26;\n}\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n  color: #f36;\n}\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n  color: #8c00fe;\n}\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n  color: #f60;\n}\n\n.achievedContainer .achievedRow .card .subText {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n.rolesContainer {\n  width: 100%;\n  height: 560px;\n  padding: 145px 64px;\n  background-color: #f7f7f7;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.rolesContainer .contentPart {\n    width: 60%;\n  }\n\n.rolesContainer .contentPart .content {\n      width: 100%;\n      max-width: 580px;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.rolesContainer .contentPart .content h2 {\n        margin: 0;\n        font-size: 40px;\n        letter-spacing: -1.2px;\n        line-height: 48px;\n        color: #25282b;\n      }\n\n.rolesContainer .contentPart .content p {\n        margin: 12px 0 0 0;\n        font-size: 20px;\n        line-height: 32px;\n        color: #25282b;\n      }\n\n.rolesContainer .imagePart {\n    width: 40%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n  }\n\n.rolesContainer .imagePart .emptyCard {\n      width: 480px;\n      height: 270px;\n      -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n              box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n      background-color: #fff;\n      position: relative;\n    }\n\n.rolesContainer .imagePart .emptyCard .topCard {\n        position: absolute;\n        top: 0;\n        left: 0;\n        width: 100%;\n        height: 100%;\n        z-index: 1;\n        background-color: #fff;\n      }\n\n.rolesContainer .imagePart .emptyCard .topCard img {\n          width: 100%;\n          height: 100%;\n        }\n\n.rolesContainer .imagePart .emptyCard .topCircle {\n        position: absolute;\n        width: 64px;\n        height: 64px;\n        border-radius: 50%;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -32px;\n        left: 341px;\n      }\n\n.rolesContainer .imagePart .emptyCard .bottomCircle {\n        position: absolute;\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        border-radius: 50%;\n        opacity: 0.2;\n        bottom: -24px;\n        left: -24px;\n      }\n\n.rolesContainer .imagePart.untilresolved .topCircle {\n      width: 56px;\n      height: 56px;\n      background-color: #f36;\n      opacity: 0.1;\n      top: -28px;\n      left: -28px;\n    }\n\n.rolesContainer .imagePart.untilresolved .bottomCircle {\n      width: 72px;\n      height: 72px;\n      background-color: #3fc;\n      opacity: 0.2;\n      bottom: -36px;\n      left: 348px;\n    }\n\n.rolesContainer .imagePart.askanytime .topCircle {\n      width: 64px;\n      height: 64px;\n      background-color: #3fc;\n      opacity: 0.3;\n      top: -32px;\n      left: 98px;\n    }\n\n.rolesContainer .imagePart.askanytime .bottomCircle {\n      width: 56px;\n      height: 56px;\n      background-color: #f2e5fe;\n      bottom: -24px;\n      left: 407px;\n    }\n\n.rolesContainer .imagePart.accessandanalyse .topCircle {\n      width: 56px;\n      height: 56px;\n      background-color: #feb546;\n      opacity: 0.2;\n      top: -28px;\n      left: -28px;\n    }\n\n.rolesContainer .imagePart.accessandanalyse .bottomCircle {\n      width: 72px;\n      height: 72px;\n      background-color: #0076ff;\n      opacity: 0.2;\n      bottom: -36px;\n      left: 348px;\n    }\n\n.availableContainer {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.availableContainer .availableRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.availableContainer .availableRow .desktopImage {\n      width: 696px;\n      height: 371px;\n    }\n\n.availableTitle {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.available {\n  color: #f36;\n}\n\n.availableContentSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.platformContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.platform {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.platformOs {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.row {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.store {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.store .playstore {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.store .appstore {\n    width: 136px;\n    height: 40px;\n  }\n\n.joinCommunity {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.joinFbText {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.joinFbLink {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.rolesContainer.reverse {\n  -ms-flex-direction: row-reverse;\n      flex-direction: row-reverse;\n  background-color: #fff;\n}\n\n.rolesContainer.reverse .contentPart {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: end;\n        justify-content: flex-end;\n  }\n\n.rolesContainer.reverse .contentPart .content {\n      max-width: 594px;\n    }\n\n@media only screen and (max-width: 990px) {\n  .teachContainer .maxContainer {\n    position: static;\n    max-width: 450px;\n  }\n\n  .teachContainer .breadcrum {\n    display: none;\n    text-align: center;\n  }\n\n  .headerbackgroundmobile {\n    width: 319px;\n    height: 277px;\n    right: 0;\n    left: 0;\n    margin: auto;\n  }\n\n  .teachContainer .downloadApp {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .displayClients span {\n    max-width: 307px;\n  }\n\n  .achievedContainer {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .achievedContainer .achievedHeading {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .achievedContainer .achievedRow .card {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .teachContainer {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .teachContainer .heading {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n\n      /* .learningText {\n        width: 210px;\n\n        img {\n          left: 0;\n          min-width: 140px;\n        }\n      } */\n    }\n\n    .teachContainer .contentText {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .teachContainer .buttonwrapper {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .teachContainer .buttonwrapper .requestDemo {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .teachContainer .buttonwrapper .whatsappwrapper {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .teachContainer .buttonwrapper .whatsappwrapper img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .teachContainer .topSection {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .teachContainer .topSection .teachSection .teachImgBox {\n          width: 32px;\n          height: 32px;\n        }\n\n        .teachContainer .topSection .teachSection .teachSectionName {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  /* .headerContainer {\n    height: 480px;\n    padding: 32px 64px;\n\n    .topSection {\n      .teachSection {\n        flex-direction: column;\n        align-items: center;\n\n        .teachimgbox {\n          width: 48px;\n          height: 48px;\n\n          img {\n            width: 27px;\n            height: 27px;\n          }\n        }\n\n        .teachSectionName {\n          font-size: 32px;\n          line-height: 48px;\n          text-align: center;\n          margin: 8px 0;\n        }\n      }\n    }\n  } */\n\n  .rolesContainer {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 40px 16px 56px 16px;\n    height: 457px;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n  }\n\n    .rolesContainer .contentPart {\n      width: 100%;\n    }\n\n      .rolesContainer .contentPart .content {\n        margin: auto;\n      }\n\n        .rolesContainer .contentPart .content h2 {\n          font-size: 24px;\n          text-align: center;\n          line-height: normal;\n        }\n\n        .rolesContainer .contentPart .content p {\n          font-size: 14px;\n          line-height: 24px;\n          text-align: center;\n          margin-top: 16px;\n          margin-bottom: 40px;\n        }\n\n    .rolesContainer .imagePart {\n      width: 100%;\n      -ms-flex-pack: center;\n          justify-content: center;\n    }\n\n      .rolesContainer .imagePart .emptyCard {\n        width: 320px;\n        height: 180px;\n      }\n\n        .rolesContainer .imagePart .emptyCard .topCircle {\n          width: 48px;\n          height: 48px;\n        }\n\n        .rolesContainer .imagePart .emptyCard .bottomCircle {\n          width: 48px;\n          height: 48px;\n        }\n      .rolesContainer .imagePart.doubtsturnup .topCircle {\n        background-color: #feb546;\n        opacity: 0.2;\n        width: 32px;\n        height: 32px;\n        left: 24px;\n        top: -16px;\n      }\n\n      .rolesContainer .imagePart.doubtsturnup .bottomCircle {\n        background-color: #0076ff;\n        opacity: 0.2;\n        left: 248px;\n        bottom: -24px;\n      }\n        .rolesContainer .imagePart.untilresolved .emptyCard .topCircle {\n          background-color: #f36;\n          opacity: 0.1;\n          width: 32px;\n          height: 32px;\n          top: -16px;\n          left: 264px;\n        }\n\n        .rolesContainer .imagePart.untilresolved .emptyCard .bottomCircle {\n          background-color: #3fc;\n          opacity: 0.2;\n          left: 24px;\n          bottom: -24px;\n        }\n        .rolesContainer .imagePart.askanytime .emptyCard .topCircle {\n          background-color: #f36;\n          opacity: 0.1;\n          width: 32px;\n          height: 32px;\n          left: 24px;\n          top: -16px;\n        }\n\n        .rolesContainer .imagePart.askanytime .emptyCard .bottomCircle {\n          background-color: #f2e5fe;\n          opacity: 1;\n          bottom: -24px;\n          left: 248px;\n        }\n        .rolesContainer .imagePart.accessandanalyse .emptyCard .topCircle {\n          background-color: #feb546;\n          opacity: 0.2;\n          width: 32px;\n          height: 32px;\n          left: 268px;\n          top: -16px;\n        }\n\n        .rolesContainer .imagePart.accessandanalyse .emptyCard .bottomCircle {\n          background-color: #0076ff;\n          opacity: 0.2;\n          left: 24px;\n          bottom: -24px;\n        }\n\n  .availableContainer {\n    padding: 32px 32px 0 32px;\n  }\n\n    .availableContainer .availableRow {\n      grid-template-columns: 1fr;\n    }\n\n      .availableContainer .availableRow .availableTitle {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .availableContainer .availableRow .row {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .row .platformContainer {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .availableContainer .availableRow .row .platformContainer .platform {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .availableContainer .availableRow .row .platformContainer .platformOs {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .availableContainer .availableRow .row .platformContainer:last-child {\n          margin-right: 0;\n        }\n\n      .availableContainer .availableRow .desktopImage {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .availableContainer .availableRow .store {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .store .playstore {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .availableContainer .availableRow .store .appstore {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .joinCommunity {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .joinFbText {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .joinFbLink {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .rolesContainer.reverse {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Doubts-root-1zjON",
	"teachContainer": "Doubts-teachContainer-1fJ2d",
	"heading": "Doubts-heading-pM5GB",
	"headerbackgroundmobile": "Doubts-headerbackgroundmobile-2QqpM",
	"contentText": "Doubts-contentText-1I74_",
	"buttonwrapper": "Doubts-buttonwrapper-oZ33z",
	"requestDemo": "Doubts-requestDemo-saVek",
	"whatsappwrapper": "Doubts-whatsappwrapper-3MBLM",
	"whatsapp": "Doubts-whatsapp-uVHvS",
	"downloadApp": "Doubts-downloadApp-1Dxtv",
	"downloadText": "Doubts-downloadText-25SfB",
	"playStoreIcon": "Doubts-playStoreIcon-8n7JB",
	"breadcrum": "Doubts-breadcrum-2R3nS",
	"displayClients": "Doubts-displayClients-3vuci",
	"topSection": "Doubts-topSection-31d4S",
	"teachSection": "Doubts-teachSection-1Odnm",
	"teachSectionName": "Doubts-teachSectionName-1rm2I",
	"clientsWrapper": "Doubts-clientsWrapper-yp9wC",
	"client": "Doubts-client-3oHCb",
	"achievedContainer": "Doubts-achievedContainer-2bD7W",
	"achievedHeading": "Doubts-achievedHeading-3cYzM",
	"achievedRow": "Doubts-achievedRow-1pjy_",
	"card": "Doubts-card-19Zwa",
	"achievedProfile": "Doubts-achievedProfile-23nuJ",
	"clients": "Doubts-clients-36OmD",
	"students": "Doubts-students-2tHVN",
	"tests": "Doubts-tests-2pFLY",
	"questions": "Doubts-questions-24jyE",
	"highlight": "Doubts-highlight-r5CRW",
	"questionsHighlight": "Doubts-questionsHighlight-3xuB6",
	"testsHighlight": "Doubts-testsHighlight-37I5Y",
	"studentsHighlight": "Doubts-studentsHighlight-GxwHP",
	"clientsHighlight": "Doubts-clientsHighlight-3GUTh",
	"subText": "Doubts-subText-1Px6P",
	"rolesContainer": "Doubts-rolesContainer-1L5aT",
	"contentPart": "Doubts-contentPart-Flyb4",
	"content": "Doubts-content-L1WoN",
	"imagePart": "Doubts-imagePart-20GcQ",
	"emptyCard": "Doubts-emptyCard-11L4N",
	"topCard": "Doubts-topCard-3jgiO",
	"topCircle": "Doubts-topCircle-3ec90",
	"bottomCircle": "Doubts-bottomCircle-2qapv",
	"untilresolved": "Doubts-untilresolved-3YrBq",
	"askanytime": "Doubts-askanytime-2MjIR",
	"accessandanalyse": "Doubts-accessandanalyse-3apu8",
	"availableContainer": "Doubts-availableContainer-25Qda",
	"availableRow": "Doubts-availableRow-3TUG_",
	"desktopImage": "Doubts-desktopImage-3A1WM",
	"availableTitle": "Doubts-availableTitle-1oPoo",
	"available": "Doubts-available-2ypZi",
	"availableContentSection": "Doubts-availableContentSection-3jhve",
	"platformContainer": "Doubts-platformContainer-1uSP3",
	"platform": "Doubts-platform-XIF0Q",
	"platformOs": "Doubts-platformOs-10uUp",
	"row": "Doubts-row-hkMoO",
	"store": "Doubts-store-3-5Xh",
	"playstore": "Doubts-playstore-11M66",
	"appstore": "Doubts-appstore-3YK5j",
	"joinCommunity": "Doubts-joinCommunity-704Kz",
	"joinFbText": "Doubts-joinFbText-2M2jI",
	"joinFbLink": "Doubts-joinFbLink-3eSQO",
	"reverse": "Doubts-reverse-1mvh5",
	"maxContainer": "Doubts-maxContainer-2XbtV",
	"teachImgBox": "Doubts-teachImgBox-3uYfB",
	"doubtsturnup": "Doubts-doubtsturnup-1vSBI"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/Doubts/Doubts.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Doubts_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/Doubts/Doubts.scss");
/* harmony import */ var _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Doubts/Doubts.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var Doubts =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Doubts, _React$Component);

  function Doubts() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Doubts);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Doubts)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "displayTeachSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 9
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maxContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 10
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.breadcrum,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 11
        },
        __self: this
      }, "Home / ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        __self: this
      }, "Doubts")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 14
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 15
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachImgBox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/Doubts_Header.svg",
        alt: "Assignments",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 17
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSectionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        },
        __self: this
      }, "Doubts"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      }, "Don\u2019t let your Student\u2019s doubts pile up, clear them instantly"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        },
        __self: this
      }, "Solve your student's doubts in the most interactive and personalized way. Resolve the doubts by typing text, taking a picture, recording a video or audio."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.buttonwrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.requestDemo,
        onClick: _this.handleShowTrial,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      }, "GET SUBSCRIPTION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsappwrapper,
        href: "https://api.whatsapp.com/send?phone=918800764909&text=Hi GetRanks, I would like to know more about your Online Platform.",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsapp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/whatsapp_logo.svg",
        alt: "whatsapp",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadApp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }, "Download the app"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playStoreIcon,
        src: "/images/home/platforms/playStore.png",
        alt: "play_store",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerbackgroundmobile,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/hero.webp",
        alt: "mobile_background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayClients", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.displayClients,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOME_CLIENTS_UPDATED"].map(function (client) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.client,
          key: client.id,
          src: client.icon,
          alt: "clinet",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 91
          },
          __self: this
        });
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/customers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, "click here for more...")));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAchievedSoFar", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedHeading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clients),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/clients.svg",
        alt: "profile",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.students),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/students.svg",
        alt: "students",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.studentsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/tests.svg",
        alt: "tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.testsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questions),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/questions.svg",
        alt: "questions",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questionsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }, "Questions Solved"))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayDoubtsTurnUp", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rolesContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, "Doubts can turn up anytime"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }, "With Doubts module your students learning never stops because of a Doubt. A student can upload a picture or type a doubt and submit across all subjects and topics."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtsturnup),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/turnup.webp",
        alt: "turnup-anytime",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 180
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayUntilResolved", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rolesContainer, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.reverse),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }, "Ask until it is resolved"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, "Student can ask as many doubts as he/she wants. With the \"Doubt Resolved\" option, student and teachers can keep track of unresolved doubts."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.untilresolved),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 200
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 202
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/ask_until.webp",
        alt: "ask-until-resolved",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAnswerAnyTime", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rolesContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, "Answer doubt anytime, anywhere"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }, "Teacher will have a mobile and web app to answer doubts anytime and anywhere"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.askanytime),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 222
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/ask_anywhere.webp",
        alt: "ask-anywhere",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 225
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 227
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 228
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAccessAndAnalyse", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rolesContainer, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.reverse),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 234
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 235
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 237
        },
        __self: this
      }, "Access and Analyse doubts"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      }, "Student, Teacher, Parents will have access to all doubts and a clear analysis on total doubts asked vs resolved across all subject, topics and chapters"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.accessandanalyse),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 245
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 246
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 247
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Doubts/access_analyze.webp",
        alt: "access-analyze",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 248
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAvailableOn", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 261
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 262
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContentSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 263
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 264
        },
        __self: this
      }, "We\u2019re ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.available,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266
        },
        __self: this
      }, "available"), " on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.row,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["PLATFORMS"].map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 270
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: item.icon,
          alt: "",
          height: "40px",
          width: "40px",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 271
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platform,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 272
          },
          __self: this
        }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformOs,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 273
          },
          __self: this
        }, item.available));
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.store,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 277
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/playStore.png",
        alt: "Play Store",
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 283
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/appStore.png",
        alt: "App Store",
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.appstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.desktopImage,
        src: "/images/home/platforms/platforms_v2.webp",
        alt: "platform",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 297
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayJoinFacebook", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.joinCommunity,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 307
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.joinFbText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }, "Join our Facebook Community"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.joinFbLink,
        href: "https://www.facebook.com/Egnify/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309
        },
        __self: this
      }, "Join Now"));
    });

    return _this;
  }

  _createClass(Doubts, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 317
        },
        __self: this
      }, this.displayTeachSection(), this.displayClients(), this.displayAchievedSoFar(), this.displayDoubtsTurnUp(), this.displayUntilResolved(), this.displayAnswerAnyTime(), this.displayAccessAndAnalyse(), this.displayAvailableOn(), this.displayJoinFacebook());
    }
  }]);

  return Doubts;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Doubts_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Doubts));

/***/ }),

/***/ "./src/routes/products/get-ranks/Doubts/Doubts.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Doubts/Doubts.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Doubts/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Doubts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/Doubts/Doubts.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Doubts/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'GetRanks by Egnify: Assessment & Analytics Platform',
              chunks: ['Doubts'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Doubts__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRG91YnRzLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvRG91YnRzL0RvdWJ0cy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL0RvdWJ0cy9Eb3VidHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvRG91YnRzL0RvdWJ0cy5zY3NzPzkwMWEiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvRG91YnRzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIqIHtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxufVxcblxcbi5Eb3VidHMtcm9vdC0xempPTiB7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIHtcXG4gIG1pbi1oZWlnaHQ6IDcyMHB4O1xcbiAgcGFkZGluZzogMTZweCA2NHB4IDU2cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDZmZmVkO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtaGVhZGluZy1wTTVHQiB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICBsaW5lLWhlaWdodDogNjZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWF4LXdpZHRoOiA3NDBweDtcXG4gIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLyogLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQge1xcbiAgd2lkdGg6IDMxNnB4O1xcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59ICovXFxuXFxuLkRvdWJ0cy1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTJRcXBNIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCBpbWcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAtMnB4O1xcbiAgbGVmdDogMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBtYXgtd2lkdGg6IDMwOHB4O1xcbn0gKi9cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtY29udGVudFRleHQtMUk3NF8ge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICBtYXJnaW46IDEycHggMCAwIDA7XFxuICBtYXgtd2lkdGg6IDY4N3B4O1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtYnV0dG9ud3JhcHBlci1vWjMzeiB7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1idXR0b253cmFwcGVyLW9aMzN6IC5Eb3VidHMtcmVxdWVzdERlbW8tc2FWZWsge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtYnV0dG9ud3JhcHBlci1vWjMzeiAuRG91YnRzLXdoYXRzYXBwd3JhcHBlci0zTUJMTSB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtYnV0dG9ud3JhcHBlci1vWjMzeiAuRG91YnRzLXdoYXRzYXBwd3JhcHBlci0zTUJMTSAuRG91YnRzLXdoYXRzYXBwLXVWSHZTIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjMjUyODJiICFpbXBvcnRhbnQ7XFxuICBtYXJnaW46IDAgOHB4IDAgMzJweDtcXG4gIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtYnV0dG9ud3JhcHBlci1vWjMzeiAuRG91YnRzLXdoYXRzYXBwd3JhcHBlci0zTUJMTSBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICBoZWlnaHQ6IDMycHg7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1kb3dubG9hZEFwcC0xRHh0diB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1kb3dubG9hZEFwcC0xRHh0diAuRG91YnRzLWRvd25sb2FkVGV4dC0yNVNmQiB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWRvd25sb2FkQXBwLTFEeHR2IC5Eb3VidHMtcGxheVN0b3JlSWNvbi04bjdKQiB7XFxuICB3aWR0aDogMTMycHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1icmVhZGNydW0tMlIzblMge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxufVxcblxcbi5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTE1MnB4O1xcbiAgbWFyZ2luOiAxMnB4IGF1dG8gMDtcXG59XFxuXFxuLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWJyZWFkY3J1bS0yUjNuUyBzcGFuIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy10b3BTZWN0aW9uLTMxZDRTIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDU2cHg7XFxufVxcblxcbi5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy10b3BTZWN0aW9uLTMxZDRTIC5Eb3VidHMtdGVhY2hTZWN0aW9uLTFPZG5tIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLyogLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoSW1nQm94IHtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn0gKi9cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3ggaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59ICovXFxuXFxuLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLXRvcFNlY3Rpb24tMzFkNFMgLkRvdWJ0cy10ZWFjaFNlY3Rpb24tMU9kbm0gLkRvdWJ0cy10ZWFjaFNlY3Rpb25OYW1lLTFybTJJIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGNvbG9yOiAjMDk1O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5Eb3VidHMtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0yUXFwTSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogNTg0cHg7XFxuICBoZWlnaHQ6IDUwOHB4O1xcbiAgcmlnaHQ6IDUycHg7XFxuICBib3R0b206IDA7XFxufVxcblxcbi5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiA0NjRweDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi8qIC5kaXNwbGF5Q2xpZW50cyAuZG90cyB7XFxuICBkaXNwbGF5OiBub25lO1xcbn0gKi9cXG5cXG4uRG91YnRzLWRpc3BsYXlDbGllbnRzLTN2dWNpIGgzIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luLXRvcDogMDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kgLkRvdWJ0cy1jbGllbnRzV3JhcHBlci15cDl3QyB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uRG91YnRzLWRpc3BsYXlDbGllbnRzLTN2dWNpIC5Eb3VidHMtY2xpZW50c1dyYXBwZXIteXA5d0MgLkRvdWJ0cy1jbGllbnQtM29IQ2Ige1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMTJweDtcXG59XFxuXFxuLkRvdWJ0cy1kaXNwbGF5Q2xpZW50cy0zdnVjaSBzcGFuIGEge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XFxufVxcblxcbi5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kgc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRIZWFkaW5nLTNjWXpNIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8ge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xcbiAgLy8gZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gIGdhcDogMTZweDtcXG4gIGdhcDogMXJlbTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIC5Eb3VidHMtY2FyZC0xOVp3YSB7XFxuICB3aWR0aDogMjcwcHg7XFxuICBoZWlnaHQ6IDIwMnB4O1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC1zaXplOiAxLjVyZW07XFxuICBjb2xvcjogcmdiYSgzNywgNDAsIDQzLCAwLjYpO1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICBwYWRkaW5nOiAyNHB4O1xcbiAgcGFkZGluZzogMS41cmVtO1xcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtYWNoaWV2ZWRQcm9maWxlLTIzbnVKIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIC5Eb3VidHMtY2FyZC0xOVp3YSAuRG91YnRzLWFjaGlldmVkUHJvZmlsZS0yM251Si5Eb3VidHMtY2xpZW50cy0zNk9tRCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmM2ViO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtYWNoaWV2ZWRQcm9maWxlLTIzbnVKLkRvdWJ0cy1zdHVkZW50cy0ydEhWTiB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtYWNoaWV2ZWRQcm9maWxlLTIzbnVKLkRvdWJ0cy10ZXN0cy0ycEZMWSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtYWNoaWV2ZWRQcm9maWxlLTIzbnVKLkRvdWJ0cy1xdWVzdGlvbnMtMjRqeUUge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ViZmZlZjtcXG59XFxuXFxuLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIC5Eb3VidHMtY2FyZC0xOVp3YSAuRG91YnRzLWhpZ2hsaWdodC1yNUNSVyB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtaGlnaGxpZ2h0LXI1Q1JXLkRvdWJ0cy1xdWVzdGlvbnNIaWdobGlnaHQtM3h1QjYge1xcbiAgY29sb3I6ICMwMGFjMjY7XFxufVxcblxcbi5Eb3VidHMtYWNoaWV2ZWRDb250YWluZXItMmJEN1cgLkRvdWJ0cy1hY2hpZXZlZFJvdy0xcGp5XyAuRG91YnRzLWNhcmQtMTlad2EgLkRvdWJ0cy1oaWdobGlnaHQtcjVDUlcuRG91YnRzLXRlc3RzSGlnaGxpZ2h0LTM3STVZIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIC5Eb3VidHMtaGlnaGxpZ2h0LXI1Q1JXLkRvdWJ0cy1zdHVkZW50c0hpZ2hsaWdodC1HeHdIUCB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIC5Eb3VidHMtY2FyZC0xOVp3YSAuRG91YnRzLWhpZ2hsaWdodC1yNUNSVy5Eb3VidHMtY2xpZW50c0hpZ2hsaWdodC0zR1VUaCB7XFxuICBjb2xvcjogI2Y2MDtcXG59XFxuXFxuLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIC5Eb3VidHMtY2FyZC0xOVp3YSAuRG91YnRzLXN1YlRleHQtMVB4NlAge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDU2MHB4O1xcbiAgcGFkZGluZzogMTQ1cHggNjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1jb250ZW50UGFydC1GbHliNCB7XFxuICAgIHdpZHRoOiA2MCU7XFxuICB9XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWF4LXdpZHRoOiA1ODBweDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICB9XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiBoMiB7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBmb250LXNpemU6IDQwcHg7XFxuICAgICAgICBsZXR0ZXItc3BhY2luZzogLTEuMnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICB9XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiBwIHtcXG4gICAgICAgIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgfVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1Ege1xcbiAgICB3aWR0aDogNDAlO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG4gIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRIC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIHtcXG4gICAgICB3aWR0aDogNDgwcHg7XFxuICAgICAgaGVpZ2h0OiAyNzBweDtcXG4gICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgfVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1EgLkRvdWJ0cy1lbXB0eUNhcmQtMTFMNE4gLkRvdWJ0cy10b3BDYXJkLTNqZ2lPIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIHRvcDogMDtcXG4gICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIHotaW5kZXg6IDE7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRIC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIC5Eb3VidHMtdG9wQ2FyZC0zamdpTyBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgfVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1EgLkRvdWJ0cy1lbXB0eUNhcmQtMTFMNE4gLkRvdWJ0cy10b3BDaXJjbGUtM2VjOTAge1xcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgICAgd2lkdGg6IDY0cHg7XFxuICAgICAgICBoZWlnaHQ6IDY0cHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgdG9wOiAtMzJweDtcXG4gICAgICAgIGxlZnQ6IDM0MXB4O1xcbiAgICAgIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRIC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIC5Eb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2IHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgICBsZWZ0OiAtMjRweDtcXG4gICAgICB9XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUS5Eb3VidHMtdW50aWxyZXNvbHZlZC0zWXJCcSAuRG91YnRzLXRvcENpcmNsZS0zZWM5MCB7XFxuICAgICAgd2lkdGg6IDU2cHg7XFxuICAgICAgaGVpZ2h0OiA1NnB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgb3BhY2l0eTogMC4xO1xcbiAgICAgIHRvcDogLTI4cHg7XFxuICAgICAgbGVmdDogLTI4cHg7XFxuICAgIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy11bnRpbHJlc29sdmVkLTNZckJxIC5Eb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2IHtcXG4gICAgICB3aWR0aDogNzJweDtcXG4gICAgICBoZWlnaHQ6IDcycHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgYm90dG9tOiAtMzZweDtcXG4gICAgICBsZWZ0OiAzNDhweDtcXG4gICAgfVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1EuRG91YnRzLWFza2FueXRpbWUtMk1qSVIgLkRvdWJ0cy10b3BDaXJjbGUtM2VjOTAge1xcbiAgICAgIHdpZHRoOiA2NHB4O1xcbiAgICAgIGhlaWdodDogNjRweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgIG9wYWNpdHk6IDAuMztcXG4gICAgICB0b3A6IC0zMnB4O1xcbiAgICAgIGxlZnQ6IDk4cHg7XFxuICAgIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy1hc2thbnl0aW1lLTJNaklSIC5Eb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2IHtcXG4gICAgICB3aWR0aDogNTZweDtcXG4gICAgICBoZWlnaHQ6IDU2cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG4gICAgICBib3R0b206IC0yNHB4O1xcbiAgICAgIGxlZnQ6IDQwN3B4O1xcbiAgICB9XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUS5Eb3VidHMtYWNjZXNzYW5kYW5hbHlzZS0zYXB1OCAuRG91YnRzLXRvcENpcmNsZS0zZWM5MCB7XFxuICAgICAgd2lkdGg6IDU2cHg7XFxuICAgICAgaGVpZ2h0OiA1NnB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgIHRvcDogLTI4cHg7XFxuICAgICAgbGVmdDogLTI4cHg7XFxuICAgIH1cXG5cXG4uRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy1hY2Nlc3NhbmRhbmFseXNlLTNhcHU4IC5Eb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2IHtcXG4gICAgICB3aWR0aDogNzJweDtcXG4gICAgICBoZWlnaHQ6IDcycHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgYm90dG9tOiAtMzZweDtcXG4gICAgICBsZWZ0OiAzNDhweDtcXG4gICAgfVxcblxcbi5Eb3VidHMtYXZhaWxhYmxlQ29udGFpbmVyLTI1UWRhIHtcXG4gIHBhZGRpbmc6IDgwcHggMTEzcHggNTZweCAxNjRweDtcXG4gIHBhZGRpbmc6IDVyZW0gMTEzcHggNTZweCAxNjRweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLkRvdWJ0cy1hdmFpbGFibGVDb250YWluZXItMjVRZGEgLkRvdWJ0cy1hdmFpbGFibGVSb3ctM1RVR18gLkRvdWJ0cy1kZXNrdG9wSW1hZ2UtM0ExV00ge1xcbiAgICAgIHdpZHRoOiA2OTZweDtcXG4gICAgICBoZWlnaHQ6IDM3MXB4O1xcbiAgICB9XFxuXFxuLkRvdWJ0cy1hdmFpbGFibGVUaXRsZS0xb1BvbyB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uRG91YnRzLWF2YWlsYWJsZS0yeXBaaSB7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLkRvdWJ0cy1hdmFpbGFibGVDb250ZW50U2VjdGlvbi0zamh2ZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4uRG91YnRzLXBsYXRmb3JtQ29udGFpbmVyLTF1U1AzIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICBtYXJnaW4tcmlnaHQ6IDJyZW07XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XFxufVxcblxcbi5Eb3VidHMtcGxhdGZvcm0tWElGMFEge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbjogOHB4IDAgNHB4IDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLkRvdWJ0cy1wbGF0Zm9ybU9zLTEwdVVwIHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLkRvdWJ0cy1yb3ctaGtNb08ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICBtYXJnaW46IDQ4cHggMCAzMnB4IDA7XFxuICBtYXJnaW46IDNyZW0gMCAycmVtIDA7XFxufVxcblxcbi5Eb3VidHMtc3RvcmUtMy01WGgge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBtYXJnaW4tYm90dG9tOiA2NHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNHJlbTtcXG59XFxuXFxuLkRvdWJ0cy1zdG9yZS0zLTVYaCAuRG91YnRzLXBsYXlzdG9yZS0xMU02NiB7XFxuICAgIHdpZHRoOiAxMzZweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XFxuICB9XFxuXFxuLkRvdWJ0cy1zdG9yZS0zLTVYaCAuRG91YnRzLWFwcHN0b3JlLTNZSzVqIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuLkRvdWJ0cy1qb2luQ29tbXVuaXR5LTcwNEt6IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxNjBweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IGJvdHRvbSwgbGVmdCB0b3AsIGZyb20oI2VhNGM3MCksIHRvKCNiMjQ1N2MpKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uRG91YnRzLWpvaW5GYlRleHQtMk0yakkge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLXJpZ2h0OiA2NHB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5Eb3VidHMtam9pbkZiTGluay0zZVNRTyB7XFxuICBwYWRkaW5nOiAxNnB4IDQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVC5Eb3VidHMtcmV2ZXJzZS0xbXZoNSB7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQuRG91YnRzLXJldmVyc2UtMW12aDUgLkRvdWJ0cy1jb250ZW50UGFydC1GbHliNCB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbiAgfVxcblxcbi5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQuRG91YnRzLXJldmVyc2UtMW12aDUgLkRvdWJ0cy1jb250ZW50UGFydC1GbHliNCAuRG91YnRzLWNvbnRlbnQtTDFXb04ge1xcbiAgICAgIG1heC13aWR0aDogNTk0cHg7XFxuICAgIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtbWF4Q29udGFpbmVyLTJYYnRWIHtcXG4gICAgcG9zaXRpb246IHN0YXRpYztcXG4gICAgbWF4LXdpZHRoOiA0NTBweDtcXG4gIH1cXG5cXG4gIC5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1icmVhZGNydW0tMlIzblMge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuRG91YnRzLWhlYWRlcmJhY2tncm91bmRtb2JpbGUtMlFxcE0ge1xcbiAgICB3aWR0aDogMzE5cHg7XFxuICAgIGhlaWdodDogMjc3cHg7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICBsZWZ0OiAwO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtZG93bmxvYWRBcHAtMUR4dHYge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLkRvdWJ0cy1kaXNwbGF5Q2xpZW50cy0zdnVjaSB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBtaW4taGVpZ2h0OiAyN3JlbTtcXG4gIH1cXG5cXG4gIC5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLkRvdWJ0cy1kaXNwbGF5Q2xpZW50cy0zdnVjaSAuRG91YnRzLWNsaWVudHNXcmFwcGVyLXlwOXdDIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDAgMDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxcmVtO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIH1cXG5cXG4gIC5Eb3VidHMtZGlzcGxheUNsaWVudHMtM3Z1Y2kgLkRvdWJ0cy1jbGllbnRzV3JhcHBlci15cDl3QyAuRG91YnRzLWNsaWVudC0zb0hDYiB7XFxuICAgIHdpZHRoOiA5Ljc1cmVtO1xcbiAgICBoZWlnaHQ6IDdyZW07XFxuICB9XFxuXFxuICAvKiAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQuYWN0aXZlIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICB9ICovXFxuXFxuICAuRG91YnRzLWRpc3BsYXlDbGllbnRzLTN2dWNpIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyB7XFxuICAgIHBhZGRpbmc6IDI0cHggMTZweDtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL1RlYWNoL0NvbmZldHRpX21vYmlsZS5zdmcnKTtcXG4gIH1cXG5cXG4gICAgLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkSGVhZGluZy0zY1l6TSB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMjRweCBhdXRvO1xcbiAgICAgIG1heC13aWR0aDogMzAwcHg7XFxuICAgIH1cXG5cXG4gICAgLkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3VyAuRG91YnRzLWFjaGlldmVkUm93LTFwanlfIHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgICBnYXA6IDEycHg7XFxuICAgIH1cXG5cXG4gICAgICAuRG91YnRzLWFjaGlldmVkQ29udGFpbmVyLTJiRDdXIC5Eb3VidHMtYWNoaWV2ZWRSb3ctMXBqeV8gLkRvdWJ0cy1jYXJkLTE5WndhIHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgbWF4LXdpZHRoOiAzMjhweDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEycHg7XFxuICAgICAgICBwYWRkaW5nOiAxNnB4O1xcblxcbiAgICAgICAgLyogLmhpZ2h0TGlnaHQge1xcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICB9ICovXFxuICAgICAgfVxcblxcbiAgICAgIC5Eb3VidHMtYWNoaWV2ZWRDb250YWluZXItMmJEN1cgLkRvdWJ0cy1hY2hpZXZlZFJvdy0xcGp5XyAuRG91YnRzLWNhcmQtMTlad2E6bnRoLWNoaWxkKDIpIHtcXG4gICAgICAgIC1tcy1mbGV4LW9yZGVyOiAzO1xcbiAgICAgICAgICAgIG9yZGVyOiAzO1xcblxcbiAgICAgICAgLyogLmhpZ2h0TGlnaHQ6OmFmdGVyIHtcXG4gICAgICAgICAgY29udGVudDogJyc7XFxuICAgICAgICB9ICovXFxuICAgICAgfVxcblxcbiAgICAgIC5Eb3VidHMtYWNoaWV2ZWRDb250YWluZXItMmJEN1cgLkRvdWJ0cy1hY2hpZXZlZFJvdy0xcGp5XyAuRG91YnRzLWNhcmQtMTlad2E6bnRoLWNoaWxkKDMpIHtcXG4gICAgICAgIC1tcy1mbGV4LW9yZGVyOiAyO1xcbiAgICAgICAgICAgIG9yZGVyOiAyO1xcbiAgICAgIH1cXG5cXG4gIC5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQge1xcbiAgICBoZWlnaHQ6IDY4cmVtO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIH1cXG5cXG4gICAgLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWhlYWRpbmctcE01R0Ige1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBsaW5lLWhlaWdodDogNDhweDtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICAgIG1hcmdpbjogMzJweCBhdXRvIDAgYXV0bztcXG5cXG4gICAgICAvKiAubGVhcm5pbmdUZXh0IHtcXG4gICAgICAgIHdpZHRoOiAyMTBweDtcXG5cXG4gICAgICAgIGltZyB7XFxuICAgICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICAgIG1pbi13aWR0aDogMTQwcHg7XFxuICAgICAgICB9XFxuICAgICAgfSAqL1xcbiAgICB9XFxuXFxuICAgIC5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1jb250ZW50VGV4dC0xSTc0XyB7XFxuICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IDY1MHB4O1xcbiAgICAgIG1hcmdpbjogMTZweCBhdXRvIDAgYXV0bztcXG4gICAgfVxcblxcbiAgICAuRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtYnV0dG9ud3JhcHBlci1vWjMzeiB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgfVxcblxcbiAgICAgIC5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy1idXR0b253cmFwcGVyLW9aMzN6IC5Eb3VidHMtcmVxdWVzdERlbW8tc2FWZWsge1xcbiAgICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1pbi13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWJ1dHRvbndyYXBwZXItb1ozM3ogLkRvdWJ0cy13aGF0c2FwcHdyYXBwZXItM01CTE0ge1xcbiAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWJ1dHRvbndyYXBwZXItb1ozM3ogLkRvdWJ0cy13aGF0c2FwcHdyYXBwZXItM01CTE0gLkRvdWJ0cy13aGF0c2FwcC11Vkh2UyB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG9wYWNpdHk6IDE7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLWJ1dHRvbndyYXBwZXItb1ozM3ogLkRvdWJ0cy13aGF0c2FwcHdyYXBwZXItM01CTE0gaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLkRvdWJ0cy10ZWFjaENvbnRhaW5lci0xZkoyZCAuRG91YnRzLXRvcFNlY3Rpb24tMzFkNFMge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAuRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkIC5Eb3VidHMtdG9wU2VjdGlvbi0zMWQ0UyAuRG91YnRzLXRlYWNoU2VjdGlvbi0xT2RubSAuRG91YnRzLXRlYWNoSW1nQm94LTN1WWZCIHtcXG4gICAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5Eb3VidHMtdGVhY2hDb250YWluZXItMWZKMmQgLkRvdWJ0cy10b3BTZWN0aW9uLTMxZDRTIC5Eb3VidHMtdGVhY2hTZWN0aW9uLTFPZG5tIC5Eb3VidHMtdGVhY2hTZWN0aW9uTmFtZS0xcm0ySSB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAvKiAuaGVhZGVyQ29udGFpbmVyIHtcXG4gICAgaGVpZ2h0OiA0ODBweDtcXG4gICAgcGFkZGluZzogMzJweCA2NHB4O1xcblxcbiAgICAudG9wU2VjdGlvbiB7XFxuICAgICAgLnRlYWNoU2VjdGlvbiB7XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG5cXG4gICAgICAgIC50ZWFjaGltZ2JveCB7XFxuICAgICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuXFxuICAgICAgICAgIGltZyB7XFxuICAgICAgICAgICAgd2lkdGg6IDI3cHg7XFxuICAgICAgICAgICAgaGVpZ2h0OiAyN3B4O1xcbiAgICAgICAgICB9XFxuICAgICAgICB9XFxuXFxuICAgICAgICAudGVhY2hTZWN0aW9uTmFtZSB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgbWFyZ2luOiA4cHggMDtcXG4gICAgICAgIH1cXG4gICAgICB9XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIHBhZGRpbmc6IDQwcHggMTZweCA1NnB4IDE2cHg7XFxuICAgIGhlaWdodDogNDU3cHg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtY29udGVudFBhcnQtRmx5YjQge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICB9XFxuXFxuICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiB7XFxuICAgICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiBoMiB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWNvbnRlbnRQYXJ0LUZseWI0IC5Eb3VidHMtY29udGVudC1MMVdvTiBwIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICB9XFxuXFxuICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUSAuRG91YnRzLWVtcHR5Q2FyZC0xMUw0TiB7XFxuICAgICAgICB3aWR0aDogMzIwcHg7XFxuICAgICAgICBoZWlnaHQ6IDE4MHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1EgLkRvdWJ0cy1lbXB0eUNhcmQtMTFMNE4gLkRvdWJ0cy10b3BDaXJjbGUtM2VjOTAge1xcbiAgICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUSAuRG91YnRzLWVtcHR5Q2FyZC0xMUw0TiAuRG91YnRzLWJvdHRvbUNpcmNsZS0ycWFwdiB7XFxuICAgICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICB9XFxuICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUS5Eb3VidHMtZG91YnRzdHVybnVwLTF2U0JJIC5Eb3VidHMtdG9wQ2lyY2xlLTNlYzkwIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgIGxlZnQ6IDI0cHg7XFxuICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy1kb3VidHN0dXJudXAtMXZTQkkgLkRvdWJ0cy1ib3R0b21DaXJjbGUtMnFhcHYge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIGxlZnQ6IDI0OHB4O1xcbiAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICB9XFxuICAgICAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy11bnRpbHJlc29sdmVkLTNZckJxIC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIC5Eb3VidHMtdG9wQ2lyY2xlLTNlYzkwIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgICAgb3BhY2l0eTogMC4xO1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgICBsZWZ0OiAyNjRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5Eb3VidHMtcm9sZXNDb250YWluZXItMUw1YVQgLkRvdWJ0cy1pbWFnZVBhcnQtMjBHY1EuRG91YnRzLXVudGlscmVzb2x2ZWQtM1lyQnEgLkRvdWJ0cy1lbXB0eUNhcmQtMTFMNE4gLkRvdWJ0cy1ib3R0b21DaXJjbGUtMnFhcHYge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICAgIGxlZnQ6IDI0cHg7XFxuICAgICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgICB9XFxuICAgICAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy1hc2thbnl0aW1lLTJNaklSIC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIC5Eb3VidHMtdG9wQ2lyY2xlLTNlYzkwIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgICAgb3BhY2l0eTogMC4xO1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgICBsZWZ0OiAyNHB4O1xcbiAgICAgICAgICB0b3A6IC0xNnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUS5Eb3VidHMtYXNrYW55dGltZS0yTWpJUiAuRG91YnRzLWVtcHR5Q2FyZC0xMUw0TiAuRG91YnRzLWJvdHRvbUNpcmNsZS0ycWFwdiB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICAgICAgICAgIG9wYWNpdHk6IDE7XFxuICAgICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgICAgIGxlZnQ6IDI0OHB4O1xcbiAgICAgICAgfVxcbiAgICAgICAgLkRvdWJ0cy1yb2xlc0NvbnRhaW5lci0xTDVhVCAuRG91YnRzLWltYWdlUGFydC0yMEdjUS5Eb3VidHMtYWNjZXNzYW5kYW5hbHlzZS0zYXB1OCAuRG91YnRzLWVtcHR5Q2FyZC0xMUw0TiAuRG91YnRzLXRvcENpcmNsZS0zZWM5MCB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgICAgbGVmdDogMjY4cHg7XFxuICAgICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUIC5Eb3VidHMtaW1hZ2VQYXJ0LTIwR2NRLkRvdWJ0cy1hY2Nlc3NhbmRhbmFseXNlLTNhcHU4IC5Eb3VidHMtZW1wdHlDYXJkLTExTDROIC5Eb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2IHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgICBsZWZ0OiAyNHB4O1xcbiAgICAgICAgICBib3R0b206IC0yNHB4O1xcbiAgICAgICAgfVxcblxcbiAgLkRvdWJ0cy1hdmFpbGFibGVDb250YWluZXItMjVRZGEge1xcbiAgICBwYWRkaW5nOiAzMnB4IDMycHggMCAzMnB4O1xcbiAgfVxcblxcbiAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIH1cXG5cXG4gICAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyAuRG91YnRzLWF2YWlsYWJsZVRpdGxlLTFvUG9vIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5Eb3VidHMtYXZhaWxhYmxlQ29udGFpbmVyLTI1UWRhIC5Eb3VidHMtYXZhaWxhYmxlUm93LTNUVUdfIC5Eb3VidHMtcm93LWhrTW9PIHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDM2cHg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLkRvdWJ0cy1hdmFpbGFibGVDb250YWluZXItMjVRZGEgLkRvdWJ0cy1hdmFpbGFibGVSb3ctM1RVR18gLkRvdWJ0cy1yb3ctaGtNb08gLkRvdWJ0cy1wbGF0Zm9ybUNvbnRhaW5lci0xdVNQMyB7XFxuICAgICAgICAgIHdpZHRoOiA2OHB4O1xcbiAgICAgICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyAuRG91YnRzLXJvdy1oa01vTyAuRG91YnRzLXBsYXRmb3JtQ29udGFpbmVyLTF1U1AzIC5Eb3VidHMtcGxhdGZvcm0tWElGMFEge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuM3B4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5Eb3VidHMtYXZhaWxhYmxlQ29udGFpbmVyLTI1UWRhIC5Eb3VidHMtYXZhaWxhYmxlUm93LTNUVUdfIC5Eb3VidHMtcm93LWhrTW9PIC5Eb3VidHMtcGxhdGZvcm1Db250YWluZXItMXVTUDMgLkRvdWJ0cy1wbGF0Zm9ybU9zLTEwdVVwIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5Eb3VidHMtYXZhaWxhYmxlQ29udGFpbmVyLTI1UWRhIC5Eb3VidHMtYXZhaWxhYmxlUm93LTNUVUdfIC5Eb3VidHMtcm93LWhrTW9PIC5Eb3VidHMtcGxhdGZvcm1Db250YWluZXItMXVTUDM6bGFzdC1jaGlsZCB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyAuRG91YnRzLWRlc2t0b3BJbWFnZS0zQTFXTSB7XFxuICAgICAgICB3aWR0aDogMjk2cHg7XFxuICAgICAgICBoZWlnaHQ6IDE0MHB4O1xcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIH1cXG5cXG4gICAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyAuRG91YnRzLXN0b3JlLTMtNVhoIHtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYSAuRG91YnRzLWF2YWlsYWJsZVJvdy0zVFVHXyAuRG91YnRzLXN0b3JlLTMtNVhoIC5Eb3VidHMtcGxheXN0b3JlLTExTTY2IHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMCAxNnB4IDAgMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5Eb3VidHMtYXZhaWxhYmxlQ29udGFpbmVyLTI1UWRhIC5Eb3VidHMtYXZhaWxhYmxlUm93LTNUVUdfIC5Eb3VidHMtc3RvcmUtMy01WGggLkRvdWJ0cy1hcHBzdG9yZS0zWUs1aiB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuXFxuICAuRG91YnRzLWpvaW5Db21tdW5pdHktNzA0S3oge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICBoZWlnaHQ6IDE0NHB4O1xcbiAgfVxcblxcbiAgLkRvdWJ0cy1qb2luRmJUZXh0LTJNMmpJIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuRG91YnRzLWpvaW5GYkxpbmstM2VTUU8ge1xcbiAgICBwYWRkaW5nOiAxMnB4IDMycHg7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFULkRvdWJ0cy1yZXZlcnNlLTFtdmg1IHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Eb3VidHMvRG91YnRzLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxVQUFVO0VBQ1YsV0FBVztFQUNYLCtCQUErQjtVQUN2Qix1QkFBdUI7Q0FDaEM7O0FBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLDBCQUEwQjtFQUMxQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7Q0FDbkI7O0FBRUQ7Ozs7O0lBS0k7O0FBRUo7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7Ozs7Ozs7SUFPSTs7QUFFSjtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsWUFBWTtFQUNaLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQiwwQkFBMEI7RUFDMUIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEOzs7SUFHSTs7QUFFSjs7OztJQUlJOztBQUVKO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFlBQVk7RUFDWixVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQywwQkFBMEI7Q0FDM0I7O0FBRUQ7O0lBRUk7O0FBRUo7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7RUFDZCxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixZQUFZO0VBQ1osZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix1REFBdUQ7RUFDdkQseUJBQXlCO0VBQ3pCLDRCQUE0QjtFQUM1Qix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLDBCQUEwQjtNQUN0Qiw4QkFBOEI7Q0FDbkM7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixVQUFVO0VBQ1YsYUFBYTtDQUNkOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsK0RBQStEO1VBQ3ZELHVEQUF1RDtFQUMvRCx1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGNBQWM7RUFDZCxvQkFBb0I7RUFDcEIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtJQUNJLFdBQVc7R0FDWjs7QUFFSDtNQUNNLFlBQVk7TUFDWixpQkFBaUI7TUFDakIscUJBQXFCO01BQ3JCLGNBQWM7TUFDZCwyQkFBMkI7VUFDdkIsdUJBQXVCO0tBQzVCOztBQUVMO1FBQ1EsVUFBVTtRQUNWLGdCQUFnQjtRQUNoQix1QkFBdUI7UUFDdkIsa0JBQWtCO1FBQ2xCLGVBQWU7T0FDaEI7O0FBRVA7UUFDUSxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixlQUFlO09BQ2hCOztBQUVQO0lBQ0ksV0FBVztJQUNYLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsbUJBQW1CO1FBQ2YsMEJBQTBCO0dBQy9COztBQUVIO01BQ00sYUFBYTtNQUNiLGNBQWM7TUFDZCxtREFBbUQ7Y0FDM0MsMkNBQTJDO01BQ25ELHVCQUF1QjtNQUN2QixtQkFBbUI7S0FDcEI7O0FBRUw7UUFDUSxtQkFBbUI7UUFDbkIsT0FBTztRQUNQLFFBQVE7UUFDUixZQUFZO1FBQ1osYUFBYTtRQUNiLFdBQVc7UUFDWCx1QkFBdUI7T0FDeEI7O0FBRVA7VUFDVSxZQUFZO1VBQ1osYUFBYTtTQUNkOztBQUVUO1FBQ1EsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDBCQUEwQjtRQUMxQixhQUFhO1FBQ2IsV0FBVztRQUNYLFlBQVk7T0FDYjs7QUFFUDtRQUNRLG1CQUFtQjtRQUNuQixZQUFZO1FBQ1osYUFBYTtRQUNiLDBCQUEwQjtRQUMxQixtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLGNBQWM7UUFDZCxZQUFZO09BQ2I7O0FBRVA7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLHVCQUF1QjtNQUN2QixhQUFhO01BQ2IsV0FBVztNQUNYLFlBQVk7S0FDYjs7QUFFTDtNQUNNLFlBQVk7TUFDWixhQUFhO01BQ2IsdUJBQXVCO01BQ3ZCLGFBQWE7TUFDYixjQUFjO01BQ2QsWUFBWTtLQUNiOztBQUVMO01BQ00sWUFBWTtNQUNaLGFBQWE7TUFDYix1QkFBdUI7TUFDdkIsYUFBYTtNQUNiLFdBQVc7TUFDWCxXQUFXO0tBQ1o7O0FBRUw7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLDBCQUEwQjtNQUMxQixjQUFjO01BQ2QsWUFBWTtLQUNiOztBQUVMO01BQ00sWUFBWTtNQUNaLGFBQWE7TUFDYiwwQkFBMEI7TUFDMUIsYUFBYTtNQUNiLFdBQVc7TUFDWCxZQUFZO0tBQ2I7O0FBRUw7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLDBCQUEwQjtNQUMxQixhQUFhO01BQ2IsY0FBYztNQUNkLFlBQVk7S0FDYjs7QUFFTDtFQUNFLCtCQUErQjtFQUMvQiwrQkFBK0I7RUFDL0IsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7QUFFRDtJQUNJLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMsYUFBYTtHQUNkOztBQUVIO01BQ00sYUFBYTtNQUNiLGNBQWM7S0FDZjs7QUFFTDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQixzQkFBc0I7RUFDdEIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxvQkFBb0I7TUFDaEIsZ0JBQWdCO0VBQ3BCLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsb0JBQW9CO0VBQ3BCLG9CQUFvQjtDQUNyQjs7QUFFRDtJQUNJLGFBQWE7SUFDYixhQUFhO0lBQ2IsbUJBQW1CO0dBQ3BCOztBQUVIO0lBQ0ksYUFBYTtJQUNiLGFBQWE7R0FDZDs7QUFFSDtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QsOEZBQThGO0VBQzlGLG9FQUFvRTtFQUNwRSwrREFBK0Q7RUFDL0QsNERBQTREO0VBQzVELHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdDQUFnQztNQUM1Qiw0QkFBNEI7RUFDaEMsdUJBQXVCO0NBQ3hCOztBQUVEO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxtQkFBbUI7UUFDZiwwQkFBMEI7R0FDL0I7O0FBRUg7TUFDTSxpQkFBaUI7S0FDbEI7O0FBRUw7RUFDRTtJQUNFLGlCQUFpQjtJQUNqQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsYUFBYTtJQUNiLGNBQWM7SUFDZCxTQUFTO0lBQ1QsUUFBUTtJQUNSLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQix1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIscUJBQXFCO0dBQ3RCOztFQUVEO0lBQ0Usb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0NBQXNDO0lBQ3RDLG1DQUFtQztJQUNuQyxVQUFVO0lBQ1YsMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsYUFBYTtHQUNkOztFQUVEOztNQUVJOztFQUVKO0lBQ0UsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLDJEQUEyRDtHQUM1RDs7SUFFQztNQUNFLGdCQUFnQjtNQUNoQixvQkFBb0I7TUFDcEIseUJBQXlCO01BQ3pCLGlCQUFpQjtLQUNsQjs7SUFFRDtNQUNFLDJCQUEyQjtNQUMzQixVQUFVO0tBQ1g7O01BRUM7UUFDRSxZQUFZO1FBQ1osaUJBQWlCO1FBQ2pCLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLGtCQUFrQjtRQUNsQixjQUFjOztRQUVkOztZQUVJO09BQ0w7O01BRUQ7UUFDRSxrQkFBa0I7WUFDZCxTQUFTOztRQUViOztZQUVJO09BQ0w7O01BRUQ7UUFDRSxrQkFBa0I7WUFDZCxTQUFTO09BQ2Q7O0VBRUw7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7SUFFQztNQUNFLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLGdCQUFnQjtNQUNoQix5QkFBeUI7O01BRXpCOzs7Ozs7O1VBT0k7S0FDTDs7SUFFRDtNQUNFLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLGlCQUFpQjtNQUNqQix5QkFBeUI7S0FDMUI7O0lBRUQ7TUFDRSwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLGlCQUFpQjtLQUNsQjs7TUFFQztRQUNFLGtCQUFrQjtRQUNsQixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGdCQUFnQjtPQUNqQjs7TUFFRDtRQUNFLGlCQUFpQjtRQUNqQixvQkFBb0I7T0FDckI7O1FBRUM7VUFDRSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLFdBQVc7VUFDWCxlQUFlO1NBQ2hCOztRQUVEO1VBQ0UsWUFBWTtVQUNaLGFBQWE7U0FDZDs7SUFFTDtNQUNFLHNCQUFzQjtVQUNsQix3QkFBd0I7TUFDNUIsY0FBYzs7TUFFZDs7VUFFSTtLQUNMO1FBQ0c7VUFDRSxZQUFZO1VBQ1osYUFBYTtTQUNkOztRQUVEO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtTQUNuQjs7RUFFUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BMkJJOztFQUVKO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQiw2QkFBNkI7SUFDN0IsY0FBYztJQUNkLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsMEJBQTBCO0dBQzNCOztJQUVDO01BQ0UsWUFBWTtLQUNiOztNQUVDO1FBQ0UsYUFBYTtPQUNkOztRQUVDO1VBQ0UsZ0JBQWdCO1VBQ2hCLG1CQUFtQjtVQUNuQixvQkFBb0I7U0FDckI7O1FBRUQ7VUFDRSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLG1CQUFtQjtVQUNuQixpQkFBaUI7VUFDakIsb0JBQW9CO1NBQ3JCOztJQUVMO01BQ0UsWUFBWTtNQUNaLHNCQUFzQjtVQUNsQix3QkFBd0I7S0FDN0I7O01BRUM7UUFDRSxhQUFhO1FBQ2IsY0FBYztPQUNmOztRQUVDO1VBQ0UsWUFBWTtVQUNaLGFBQWE7U0FDZDs7UUFFRDtVQUNFLFlBQVk7VUFDWixhQUFhO1NBQ2Q7TUFDSDtRQUNFLDBCQUEwQjtRQUMxQixhQUFhO1FBQ2IsWUFBWTtRQUNaLGFBQWE7UUFDYixXQUFXO1FBQ1gsV0FBVztPQUNaOztNQUVEO1FBQ0UsMEJBQTBCO1FBQzFCLGFBQWE7UUFDYixZQUFZO1FBQ1osY0FBYztPQUNmO1FBQ0M7VUFDRSx1QkFBdUI7VUFDdkIsYUFBYTtVQUNiLFlBQVk7VUFDWixhQUFhO1VBQ2IsV0FBVztVQUNYLFlBQVk7U0FDYjs7UUFFRDtVQUNFLHVCQUF1QjtVQUN2QixhQUFhO1VBQ2IsV0FBVztVQUNYLGNBQWM7U0FDZjtRQUNEO1VBQ0UsdUJBQXVCO1VBQ3ZCLGFBQWE7VUFDYixZQUFZO1VBQ1osYUFBYTtVQUNiLFdBQVc7VUFDWCxXQUFXO1NBQ1o7O1FBRUQ7VUFDRSwwQkFBMEI7VUFDMUIsV0FBVztVQUNYLGNBQWM7VUFDZCxZQUFZO1NBQ2I7UUFDRDtVQUNFLDBCQUEwQjtVQUMxQixhQUFhO1VBQ2IsWUFBWTtVQUNaLGFBQWE7VUFDYixZQUFZO1VBQ1osV0FBVztTQUNaOztRQUVEO1VBQ0UsMEJBQTBCO1VBQzFCLGFBQWE7VUFDYixXQUFXO1VBQ1gsY0FBYztTQUNmOztFQUVQO0lBQ0UsMEJBQTBCO0dBQzNCOztJQUVDO01BQ0UsMkJBQTJCO0tBQzVCOztNQUVDO1FBQ0UsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixvQkFBb0I7T0FDckI7O01BRUQ7UUFDRSxVQUFVO1FBQ1Ysb0JBQW9CO1FBQ3BCLHNCQUFzQjtZQUNsQix3QkFBd0I7T0FDN0I7O1FBRUM7VUFDRSxZQUFZO1VBQ1oscUJBQXFCO2NBQ2pCLDRCQUE0QjtVQUNoQyxtQkFBbUI7VUFDbkIsaUJBQWlCO1NBQ2xCOztVQUVDO1lBQ0Usa0JBQWtCO1lBQ2xCLGtCQUFrQjtXQUNuQjs7VUFFRDtZQUNFLGdCQUFnQjtZQUNoQixrQkFBa0I7V0FDbkI7O1FBRUg7VUFDRSxnQkFBZ0I7U0FDakI7O01BRUg7UUFDRSxhQUFhO1FBQ2IsY0FBYztRQUNkLGFBQWE7T0FDZDs7TUFFRDtRQUNFLHNCQUFzQjtZQUNsQix3QkFBd0I7T0FDN0I7O1FBRUM7VUFDRSxhQUFhO1VBQ2IsYUFBYTtVQUNiLG1CQUFtQjtTQUNwQjs7UUFFRDtVQUNFLGFBQWE7VUFDYixhQUFhO1VBQ2IsVUFBVTtTQUNYOztFQUVQO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLGNBQWM7R0FDZjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsdUJBQXVCO1FBQ25CLG9CQUFvQjtHQUN6QjtDQUNGXCIsXCJmaWxlXCI6XCJEb3VidHMuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIqIHtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxufVxcblxcbi5yb290IHtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciB7XFxuICBtaW4taGVpZ2h0OiA3MjBweDtcXG4gIHBhZGRpbmc6IDE2cHggNjRweCA1NnB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q2ZmZlZDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiA2NnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXgtd2lkdGg6IDc0MHB4O1xcbiAgbWFyZ2luOiAxMnB4IDAgMCAwO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCB7XFxuICB3aWR0aDogMzE2cHg7XFxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn0gKi9cXG5cXG4uaGVhZGVyYmFja2dyb3VuZG1vYmlsZSBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLyogLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQgaW1nIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogLTJweDtcXG4gIGxlZnQ6IDA7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogOHB4O1xcbiAgbWF4LXdpZHRoOiAzMDhweDtcXG59ICovXFxuXFxuLnRlYWNoQ29udGFpbmVyIC5jb250ZW50VGV4dCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIG1heC13aWR0aDogNjg3cHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciB7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAucmVxdWVzdERlbW8ge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciAud2hhdHNhcHAge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgcGFkZGluZy1ib3R0b206IDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIGltZyB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMzJweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5kb3dubG9hZEFwcCB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAgLmRvd25sb2FkVGV4dCB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5kb3dubG9hZEFwcCAucGxheVN0b3JlSWNvbiB7XFxuICB3aWR0aDogMTMycHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnJlYWRjcnVtIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogMTE1MnB4O1xcbiAgbWFyZ2luOiAxMnB4IGF1dG8gMDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5icmVhZGNydW0gc3BhbiB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufSAqL1xcblxcbi8qIC50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaEltZ0JveCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBvYmplY3QtZml0OiBjb250YWluO1xcbn0gKi9cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hTZWN0aW9uTmFtZSB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzA5NTtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4uaGVhZGVyYmFja2dyb3VuZG1vYmlsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogNTg0cHg7XFxuICBoZWlnaHQ6IDUwOHB4O1xcbiAgcmlnaHQ6IDUycHg7XFxuICBib3R0b206IDA7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDQ2NHB4O1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDQwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLyogLmRpc3BsYXlDbGllbnRzIC5kb3RzIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufSAqL1xcblxcbi5kaXNwbGF5Q2xpZW50cyBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9uZXdfY29uZmV0dGkuc3ZnJyk7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICBnYXA6IDE2cHg7XFxuICBnYXA6IDFyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQge1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiAyMDJweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHBhZGRpbmc6IDEuNXJlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLmNsaWVudHMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnN0dWRlbnRzIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2VmZmU7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS50ZXN0cyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUucXVlc3Rpb25zIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodCB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQucXVlc3Rpb25zSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjMDBhYzI2O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQudGVzdHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5zdHVkZW50c0hpZ2hsaWdodCB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LmNsaWVudHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmNjA7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLnN1YlRleHQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5yb2xlc0NvbnRhaW5lciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNTYwcHg7XFxuICBwYWRkaW5nOiAxNDVweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5jb250ZW50UGFydCB7XFxuICAgIHdpZHRoOiA2MCU7XFxuICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5jb250ZW50UGFydCAuY29udGVudCB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgbWF4LXdpZHRoOiA1ODBweDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5jb250ZW50UGFydCAuY29udGVudCBoMiB7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBmb250LXNpemU6IDQwcHg7XFxuICAgICAgICBsZXR0ZXItc3BhY2luZzogLTEuMnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5jb250ZW50UGFydCAuY29udGVudCBwIHtcXG4gICAgICAgIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgfVxcblxcbi5yb2xlc0NvbnRhaW5lciAuaW1hZ2VQYXJ0IHtcXG4gICAgd2lkdGg6IDQwJTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XFxuICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCB7XFxuICAgICAgd2lkdGg6IDQ4MHB4O1xcbiAgICAgIGhlaWdodDogMjcwcHg7XFxuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMzJweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMzJweCAwIHJnYmEoMCwgMCwgMCwgMC4wOCk7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIH1cXG5cXG4ucm9sZXNDb250YWluZXIgLmltYWdlUGFydCAuZW1wdHlDYXJkIC50b3BDYXJkIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIHRvcDogMDtcXG4gICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIHotaW5kZXg6IDE7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgIH1cXG5cXG4ucm9sZXNDb250YWluZXIgLmltYWdlUGFydCAuZW1wdHlDYXJkIC50b3BDYXJkIGltZyB7XFxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCAudG9wQ2lyY2xlIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIHdpZHRoOiA2NHB4O1xcbiAgICAgICAgaGVpZ2h0OiA2NHB4O1xcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIHRvcDogLTMycHg7XFxuICAgICAgICBsZWZ0OiAzNDFweDtcXG4gICAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgICBsZWZ0OiAtMjRweDtcXG4gICAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQudW50aWxyZXNvbHZlZCAudG9wQ2lyY2xlIHtcXG4gICAgICB3aWR0aDogNTZweDtcXG4gICAgICBoZWlnaHQ6IDU2cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICBvcGFjaXR5OiAwLjE7XFxuICAgICAgdG9wOiAtMjhweDtcXG4gICAgICBsZWZ0OiAtMjhweDtcXG4gICAgfVxcblxcbi5yb2xlc0NvbnRhaW5lciAuaW1hZ2VQYXJ0LnVudGlscmVzb2x2ZWQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgd2lkdGg6IDcycHg7XFxuICAgICAgaGVpZ2h0OiA3MnB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgIGJvdHRvbTogLTM2cHg7XFxuICAgICAgbGVmdDogMzQ4cHg7XFxuICAgIH1cXG5cXG4ucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hc2thbnl0aW1lIC50b3BDaXJjbGUge1xcbiAgICAgIHdpZHRoOiA2NHB4O1xcbiAgICAgIGhlaWdodDogNjRweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgIG9wYWNpdHk6IDAuMztcXG4gICAgICB0b3A6IC0zMnB4O1xcbiAgICAgIGxlZnQ6IDk4cHg7XFxuICAgIH1cXG5cXG4ucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hc2thbnl0aW1lIC5ib3R0b21DaXJjbGUge1xcbiAgICAgIHdpZHRoOiA1NnB4O1xcbiAgICAgIGhlaWdodDogNTZweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbiAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgbGVmdDogNDA3cHg7XFxuICAgIH1cXG5cXG4ucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hY2Nlc3NhbmRhbmFseXNlIC50b3BDaXJjbGUge1xcbiAgICAgIHdpZHRoOiA1NnB4O1xcbiAgICAgIGhlaWdodDogNTZweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICB0b3A6IC0yOHB4O1xcbiAgICAgIGxlZnQ6IC0yOHB4O1xcbiAgICB9XFxuXFxuLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQuYWNjZXNzYW5kYW5hbHlzZSAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICB3aWR0aDogNzJweDtcXG4gICAgICBoZWlnaHQ6IDcycHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgYm90dG9tOiAtMzZweDtcXG4gICAgICBsZWZ0OiAzNDhweDtcXG4gICAgfVxcblxcbi5hdmFpbGFibGVDb250YWluZXIge1xcbiAgcGFkZGluZzogODBweCAxMTNweCA1NnB4IDE2NHB4O1xcbiAgcGFkZGluZzogNXJlbSAxMTNweCA1NnB4IDE2NHB4O1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5kZXNrdG9wSW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA2OTZweDtcXG4gICAgICBoZWlnaHQ6IDM3MXB4O1xcbiAgICB9XFxuXFxuLmF2YWlsYWJsZVRpdGxlIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5hdmFpbGFibGUge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5hdmFpbGFibGVDb250ZW50U2VjdGlvbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4ucGxhdGZvcm1Db250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIG1hcmdpbi1yaWdodDogMnJlbTtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG59XFxuXFxuLnBsYXRmb3JtIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW46IDhweCAwIDRweCAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5wbGF0Zm9ybU9zIHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLnJvdyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIG1hcmdpbjogNDhweCAwIDMycHggMDtcXG4gIG1hcmdpbjogM3JlbSAwIDJyZW0gMDtcXG59XFxuXFxuLnN0b3JlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDRyZW07XFxufVxcblxcbi5zdG9yZSAucGxheXN0b3JlIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIH1cXG5cXG4uc3RvcmUgLmFwcHN0b3JlIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuLmpvaW5Db21tdW5pdHkge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDE2MHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgYm90dG9tLCBsZWZ0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5qb2luRmJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1yaWdodDogNjRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uam9pbkZiTGluayB7XFxuICBwYWRkaW5nOiAxNnB4IDQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLnJvbGVzQ29udGFpbmVyLnJldmVyc2Uge1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4ucm9sZXNDb250YWluZXIucmV2ZXJzZSAuY29udGVudFBhcnQge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZW5kO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG4gIH1cXG5cXG4ucm9sZXNDb250YWluZXIucmV2ZXJzZSAuY29udGVudFBhcnQgLmNvbnRlbnQge1xcbiAgICAgIG1heC13aWR0aDogNTk0cHg7XFxuICAgIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAudGVhY2hDb250YWluZXIgLm1heENvbnRhaW5lciB7XFxuICAgIHBvc2l0aW9uOiBzdGF0aWM7XFxuICAgIG1heC13aWR0aDogNDUwcHg7XFxuICB9XFxuXFxuICAudGVhY2hDb250YWluZXIgLmJyZWFkY3J1bSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlIHtcXG4gICAgd2lkdGg6IDMxOXB4O1xcbiAgICBoZWlnaHQ6IDI3N3B4O1xcbiAgICByaWdodDogMDtcXG4gICAgbGVmdDogMDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLnRlYWNoQ29udGFpbmVyIC5kb3dubG9hZEFwcCB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfSAqL1xcblxcbiAgLmRpc3BsYXlDbGllbnRzIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLmFjaGlldmVkQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvVGVhY2gvQ29uZmV0dGlfbW9iaWxlLnN2ZycpO1xcbiAgfVxcblxcbiAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMjRweCBhdXRvO1xcbiAgICAgIG1heC13aWR0aDogMzAwcHg7XFxuICAgIH1cXG5cXG4gICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgICAgZ2FwOiAxMnB4O1xcbiAgICB9XFxuXFxuICAgICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogMzI4cHg7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTJweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgcGFkZGluZzogMTZweDtcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0IHtcXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkOm50aC1jaGlsZCgyKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMztcXG4gICAgICAgICAgICBvcmRlcjogMztcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0OjphZnRlciB7XFxuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkOm50aC1jaGlsZCgzKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgICAgICBvcmRlcjogMjtcXG4gICAgICB9XFxuXFxuICAudGVhY2hDb250YWluZXIge1xcbiAgICBoZWlnaHQ6IDY4cmVtO1xcbiAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICBtYXJnaW46IDMycHggYXV0byAwIGF1dG87XFxuXFxuICAgICAgLyogLmxlYXJuaW5nVGV4dCB7XFxuICAgICAgICB3aWR0aDogMjEwcHg7XFxuXFxuICAgICAgICBpbWcge1xcbiAgICAgICAgICBsZWZ0OiAwO1xcbiAgICAgICAgICBtaW4td2lkdGg6IDE0MHB4O1xcbiAgICAgICAgfVxcbiAgICAgIH0gKi9cXG4gICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLmNvbnRlbnRUZXh0IHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogNjUwcHg7XFxuICAgICAgbWFyZ2luOiAxNnB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgfVxcblxcbiAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAucmVxdWVzdERlbW8ge1xcbiAgICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1pbi13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIge1xcbiAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMjRweDtcXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24ge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoU2VjdGlvbk5hbWUge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgLyogLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIGhlaWdodDogNDgwcHg7XFxuICAgIHBhZGRpbmc6IDMycHggNjRweDtcXG5cXG4gICAgLnRvcFNlY3Rpb24ge1xcbiAgICAgIC50ZWFjaFNlY3Rpb24ge1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgICAgICAudGVhY2hpbWdib3gge1xcbiAgICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0OHB4O1xcblxcbiAgICAgICAgICBpbWcge1xcbiAgICAgICAgICAgIHdpZHRoOiAyN3B4O1xcbiAgICAgICAgICAgIGhlaWdodDogMjdweDtcXG4gICAgICAgICAgfVxcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoU2VjdGlvbk5hbWUge1xcbiAgICAgICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIG1hcmdpbjogOHB4IDA7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuICAucm9sZXNDb250YWluZXIge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIHBhZGRpbmc6IDQwcHggMTZweCA1NnB4IDE2cHg7XFxuICAgIGhlaWdodDogNDU3cHg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAucm9sZXNDb250YWluZXIgLmNvbnRlbnRQYXJ0IHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbiAgICAgIC5yb2xlc0NvbnRhaW5lciAuY29udGVudFBhcnQgLmNvbnRlbnQge1xcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5yb2xlc0NvbnRhaW5lciAuY29udGVudFBhcnQgLmNvbnRlbnQgaDIge1xcbiAgICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5yb2xlc0NvbnRhaW5lciAuY29udGVudFBhcnQgLmNvbnRlbnQgcCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgbWFyZ2luLXRvcDogMTZweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIH1cXG5cXG4gICAgICAucm9sZXNDb250YWluZXIgLmltYWdlUGFydCAuZW1wdHlDYXJkIHtcXG4gICAgICAgIHdpZHRoOiAzMjBweDtcXG4gICAgICAgIGhlaWdodDogMTgwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQgLmVtcHR5Q2FyZCAudG9wQ2lyY2xlIHtcXG4gICAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5yb2xlc0NvbnRhaW5lciAuaW1hZ2VQYXJ0IC5lbXB0eUNhcmQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICB9XFxuICAgICAgLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQuZG91YnRzdHVybnVwIC50b3BDaXJjbGUge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgbGVmdDogMjRweDtcXG4gICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5yb2xlc0NvbnRhaW5lciAuaW1hZ2VQYXJ0LmRvdWJ0c3R1cm51cCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDc2ZmY7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICBsZWZ0OiAyNDhweDtcXG4gICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgfVxcbiAgICAgICAgLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQudW50aWxyZXNvbHZlZCAuZW1wdHlDYXJkIC50b3BDaXJjbGUge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgICAgICAgICBvcGFjaXR5OiAwLjE7XFxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDMycHg7XFxuICAgICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICAgIGxlZnQ6IDI2NHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnJvbGVzQ29udGFpbmVyIC5pbWFnZVBhcnQudW50aWxyZXNvbHZlZCAuZW1wdHlDYXJkIC5ib3R0b21DaXJjbGUge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICAgIGxlZnQ6IDI0cHg7XFxuICAgICAgICAgIGJvdHRvbTogLTI0cHg7XFxuICAgICAgICB9XFxuICAgICAgICAucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hc2thbnl0aW1lIC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgICAgbGVmdDogMjRweDtcXG4gICAgICAgICAgdG9wOiAtMTZweDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5yb2xlc0NvbnRhaW5lciAuaW1hZ2VQYXJ0LmFza2FueXRpbWUgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG4gICAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICAgICAgbGVmdDogMjQ4cHg7XFxuICAgICAgICB9XFxuICAgICAgICAucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hY2Nlc3NhbmRhbmFseXNlIC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgICAgd2lkdGg6IDMycHg7XFxuICAgICAgICAgIGhlaWdodDogMzJweDtcXG4gICAgICAgICAgbGVmdDogMjY4cHg7XFxuICAgICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAucm9sZXNDb250YWluZXIgLmltYWdlUGFydC5hY2Nlc3NhbmRhbmFseXNlIC5lbXB0eUNhcmQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDc2ZmY7XFxuICAgICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgICAgbGVmdDogMjRweDtcXG4gICAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICAgIH1cXG5cXG4gIC5hdmFpbGFibGVDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAzMnB4IDMycHggMCAzMnB4O1xcbiAgfVxcblxcbiAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5hdmFpbGFibGVUaXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyB7XFxuICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzNnB4O1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lciB7XFxuICAgICAgICAgIHdpZHRoOiA2OHB4O1xcbiAgICAgICAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXIgLnBsYXRmb3JtIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEzLjNweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXIgLnBsYXRmb3JtT3Mge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyOmxhc3QtY2hpbGQge1xcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5kZXNrdG9wSW1hZ2Uge1xcbiAgICAgICAgd2lkdGg6IDI5NnB4O1xcbiAgICAgICAgaGVpZ2h0OiAxNDBweDtcXG4gICAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5zdG9yZSB7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5zdG9yZSAucGxheXN0b3JlIHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMCAxNnB4IDAgMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuc3RvcmUgLmFwcHN0b3JlIHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIH1cXG5cXG4gIC5qb2luQ29tbXVuaXR5IHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgaGVpZ2h0OiAxNDRweDtcXG4gIH1cXG5cXG4gIC5qb2luRmJUZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuam9pbkZiTGluayB7XFxuICAgIHBhZGRpbmc6IDEycHggMzJweDtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5yb2xlc0NvbnRhaW5lci5yZXZlcnNlIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcInJvb3RcIjogXCJEb3VidHMtcm9vdC0xempPTlwiLFxuXHRcInRlYWNoQ29udGFpbmVyXCI6IFwiRG91YnRzLXRlYWNoQ29udGFpbmVyLTFmSjJkXCIsXG5cdFwiaGVhZGluZ1wiOiBcIkRvdWJ0cy1oZWFkaW5nLXBNNUdCXCIsXG5cdFwiaGVhZGVyYmFja2dyb3VuZG1vYmlsZVwiOiBcIkRvdWJ0cy1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTJRcXBNXCIsXG5cdFwiY29udGVudFRleHRcIjogXCJEb3VidHMtY29udGVudFRleHQtMUk3NF9cIixcblx0XCJidXR0b253cmFwcGVyXCI6IFwiRG91YnRzLWJ1dHRvbndyYXBwZXItb1ozM3pcIixcblx0XCJyZXF1ZXN0RGVtb1wiOiBcIkRvdWJ0cy1yZXF1ZXN0RGVtby1zYVZla1wiLFxuXHRcIndoYXRzYXBwd3JhcHBlclwiOiBcIkRvdWJ0cy13aGF0c2FwcHdyYXBwZXItM01CTE1cIixcblx0XCJ3aGF0c2FwcFwiOiBcIkRvdWJ0cy13aGF0c2FwcC11Vkh2U1wiLFxuXHRcImRvd25sb2FkQXBwXCI6IFwiRG91YnRzLWRvd25sb2FkQXBwLTFEeHR2XCIsXG5cdFwiZG93bmxvYWRUZXh0XCI6IFwiRG91YnRzLWRvd25sb2FkVGV4dC0yNVNmQlwiLFxuXHRcInBsYXlTdG9yZUljb25cIjogXCJEb3VidHMtcGxheVN0b3JlSWNvbi04bjdKQlwiLFxuXHRcImJyZWFkY3J1bVwiOiBcIkRvdWJ0cy1icmVhZGNydW0tMlIzblNcIixcblx0XCJkaXNwbGF5Q2xpZW50c1wiOiBcIkRvdWJ0cy1kaXNwbGF5Q2xpZW50cy0zdnVjaVwiLFxuXHRcInRvcFNlY3Rpb25cIjogXCJEb3VidHMtdG9wU2VjdGlvbi0zMWQ0U1wiLFxuXHRcInRlYWNoU2VjdGlvblwiOiBcIkRvdWJ0cy10ZWFjaFNlY3Rpb24tMU9kbm1cIixcblx0XCJ0ZWFjaFNlY3Rpb25OYW1lXCI6IFwiRG91YnRzLXRlYWNoU2VjdGlvbk5hbWUtMXJtMklcIixcblx0XCJjbGllbnRzV3JhcHBlclwiOiBcIkRvdWJ0cy1jbGllbnRzV3JhcHBlci15cDl3Q1wiLFxuXHRcImNsaWVudFwiOiBcIkRvdWJ0cy1jbGllbnQtM29IQ2JcIixcblx0XCJhY2hpZXZlZENvbnRhaW5lclwiOiBcIkRvdWJ0cy1hY2hpZXZlZENvbnRhaW5lci0yYkQ3V1wiLFxuXHRcImFjaGlldmVkSGVhZGluZ1wiOiBcIkRvdWJ0cy1hY2hpZXZlZEhlYWRpbmctM2NZek1cIixcblx0XCJhY2hpZXZlZFJvd1wiOiBcIkRvdWJ0cy1hY2hpZXZlZFJvdy0xcGp5X1wiLFxuXHRcImNhcmRcIjogXCJEb3VidHMtY2FyZC0xOVp3YVwiLFxuXHRcImFjaGlldmVkUHJvZmlsZVwiOiBcIkRvdWJ0cy1hY2hpZXZlZFByb2ZpbGUtMjNudUpcIixcblx0XCJjbGllbnRzXCI6IFwiRG91YnRzLWNsaWVudHMtMzZPbURcIixcblx0XCJzdHVkZW50c1wiOiBcIkRvdWJ0cy1zdHVkZW50cy0ydEhWTlwiLFxuXHRcInRlc3RzXCI6IFwiRG91YnRzLXRlc3RzLTJwRkxZXCIsXG5cdFwicXVlc3Rpb25zXCI6IFwiRG91YnRzLXF1ZXN0aW9ucy0yNGp5RVwiLFxuXHRcImhpZ2hsaWdodFwiOiBcIkRvdWJ0cy1oaWdobGlnaHQtcjVDUldcIixcblx0XCJxdWVzdGlvbnNIaWdobGlnaHRcIjogXCJEb3VidHMtcXVlc3Rpb25zSGlnaGxpZ2h0LTN4dUI2XCIsXG5cdFwidGVzdHNIaWdobGlnaHRcIjogXCJEb3VidHMtdGVzdHNIaWdobGlnaHQtMzdJNVlcIixcblx0XCJzdHVkZW50c0hpZ2hsaWdodFwiOiBcIkRvdWJ0cy1zdHVkZW50c0hpZ2hsaWdodC1HeHdIUFwiLFxuXHRcImNsaWVudHNIaWdobGlnaHRcIjogXCJEb3VidHMtY2xpZW50c0hpZ2hsaWdodC0zR1VUaFwiLFxuXHRcInN1YlRleHRcIjogXCJEb3VidHMtc3ViVGV4dC0xUHg2UFwiLFxuXHRcInJvbGVzQ29udGFpbmVyXCI6IFwiRG91YnRzLXJvbGVzQ29udGFpbmVyLTFMNWFUXCIsXG5cdFwiY29udGVudFBhcnRcIjogXCJEb3VidHMtY29udGVudFBhcnQtRmx5YjRcIixcblx0XCJjb250ZW50XCI6IFwiRG91YnRzLWNvbnRlbnQtTDFXb05cIixcblx0XCJpbWFnZVBhcnRcIjogXCJEb3VidHMtaW1hZ2VQYXJ0LTIwR2NRXCIsXG5cdFwiZW1wdHlDYXJkXCI6IFwiRG91YnRzLWVtcHR5Q2FyZC0xMUw0TlwiLFxuXHRcInRvcENhcmRcIjogXCJEb3VidHMtdG9wQ2FyZC0zamdpT1wiLFxuXHRcInRvcENpcmNsZVwiOiBcIkRvdWJ0cy10b3BDaXJjbGUtM2VjOTBcIixcblx0XCJib3R0b21DaXJjbGVcIjogXCJEb3VidHMtYm90dG9tQ2lyY2xlLTJxYXB2XCIsXG5cdFwidW50aWxyZXNvbHZlZFwiOiBcIkRvdWJ0cy11bnRpbHJlc29sdmVkLTNZckJxXCIsXG5cdFwiYXNrYW55dGltZVwiOiBcIkRvdWJ0cy1hc2thbnl0aW1lLTJNaklSXCIsXG5cdFwiYWNjZXNzYW5kYW5hbHlzZVwiOiBcIkRvdWJ0cy1hY2Nlc3NhbmRhbmFseXNlLTNhcHU4XCIsXG5cdFwiYXZhaWxhYmxlQ29udGFpbmVyXCI6IFwiRG91YnRzLWF2YWlsYWJsZUNvbnRhaW5lci0yNVFkYVwiLFxuXHRcImF2YWlsYWJsZVJvd1wiOiBcIkRvdWJ0cy1hdmFpbGFibGVSb3ctM1RVR19cIixcblx0XCJkZXNrdG9wSW1hZ2VcIjogXCJEb3VidHMtZGVza3RvcEltYWdlLTNBMVdNXCIsXG5cdFwiYXZhaWxhYmxlVGl0bGVcIjogXCJEb3VidHMtYXZhaWxhYmxlVGl0bGUtMW9Qb29cIixcblx0XCJhdmFpbGFibGVcIjogXCJEb3VidHMtYXZhaWxhYmxlLTJ5cFppXCIsXG5cdFwiYXZhaWxhYmxlQ29udGVudFNlY3Rpb25cIjogXCJEb3VidHMtYXZhaWxhYmxlQ29udGVudFNlY3Rpb24tM2podmVcIixcblx0XCJwbGF0Zm9ybUNvbnRhaW5lclwiOiBcIkRvdWJ0cy1wbGF0Zm9ybUNvbnRhaW5lci0xdVNQM1wiLFxuXHRcInBsYXRmb3JtXCI6IFwiRG91YnRzLXBsYXRmb3JtLVhJRjBRXCIsXG5cdFwicGxhdGZvcm1Pc1wiOiBcIkRvdWJ0cy1wbGF0Zm9ybU9zLTEwdVVwXCIsXG5cdFwicm93XCI6IFwiRG91YnRzLXJvdy1oa01vT1wiLFxuXHRcInN0b3JlXCI6IFwiRG91YnRzLXN0b3JlLTMtNVhoXCIsXG5cdFwicGxheXN0b3JlXCI6IFwiRG91YnRzLXBsYXlzdG9yZS0xMU02NlwiLFxuXHRcImFwcHN0b3JlXCI6IFwiRG91YnRzLWFwcHN0b3JlLTNZSzVqXCIsXG5cdFwiam9pbkNvbW11bml0eVwiOiBcIkRvdWJ0cy1qb2luQ29tbXVuaXR5LTcwNEt6XCIsXG5cdFwiam9pbkZiVGV4dFwiOiBcIkRvdWJ0cy1qb2luRmJUZXh0LTJNMmpJXCIsXG5cdFwiam9pbkZiTGlua1wiOiBcIkRvdWJ0cy1qb2luRmJMaW5rLTNlU1FPXCIsXG5cdFwicmV2ZXJzZVwiOiBcIkRvdWJ0cy1yZXZlcnNlLTFtdmg1XCIsXG5cdFwibWF4Q29udGFpbmVyXCI6IFwiRG91YnRzLW1heENvbnRhaW5lci0yWGJ0VlwiLFxuXHRcInRlYWNoSW1nQm94XCI6IFwiRG91YnRzLXRlYWNoSW1nQm94LTN1WWZCXCIsXG5cdFwiZG91YnRzdHVybnVwXCI6IFwiRG91YnRzLWRvdWJ0c3R1cm51cC0xdlNCSVwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgTGluayBmcm9tICdjb21wb25lbnRzL0xpbmsnO1xuaW1wb3J0IHsgSE9NRV9DTElFTlRTX1VQREFURUQsIFBMQVRGT1JNUyB9IGZyb20gJy4uL0dldFJhbmtzQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vRG91YnRzLnNjc3MnO1xuXG5jbGFzcyBEb3VidHMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBkaXNwbGF5VGVhY2hTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1heENvbnRhaW5lcn0+XG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5icmVhZGNydW19PlxuICAgICAgICAgIEhvbWUgLyA8c3Bhbj5Eb3VidHM8L3NwYW4+XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wU2VjdGlvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hTZWN0aW9ufT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoSW1nQm94fT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL0RvdWJ0cy9Eb3VidHNfSGVhZGVyLnN2Z1wiIGFsdD1cIkFzc2lnbm1lbnRzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRlYWNoU2VjdGlvbk5hbWV9PkRvdWJ0czwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxoMiBjbGFzc05hbWU9e3MuaGVhZGluZ30+XG4gICAgICAgICAgRG9u4oCZdCBsZXQgeW91ciBTdHVkZW504oCZcyBkb3VidHMgcGlsZSB1cCwgY2xlYXIgdGhlbSBpbnN0YW50bHlcbiAgICAgICAgPC9oMj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPXtzLmNvbnRlbnRUZXh0fT5cbiAgICAgICAgICBTb2x2ZSB5b3VyIHN0dWRlbnQmYXBvcztzIGRvdWJ0cyBpbiB0aGUgbW9zdCBpbnRlcmFjdGl2ZSBhbmRcbiAgICAgICAgICBwZXJzb25hbGl6ZWQgd2F5LiBSZXNvbHZlIHRoZSBkb3VidHMgYnkgdHlwaW5nIHRleHQsIHRha2luZyBhIHBpY3R1cmUsXG4gICAgICAgICAgcmVjb3JkaW5nIGEgdmlkZW8gb3IgYXVkaW8uXG4gICAgICAgIDwvcD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYnV0dG9ud3JhcHBlcn0+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnJlcXVlc3REZW1vfVxuICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVTaG93VHJpYWx9XG4gICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICBHRVQgU1VCU0NSSVBUSU9OXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGNsYXNzTmFtZT17cy53aGF0c2FwcHdyYXBwZXJ9XG4gICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9hcGkud2hhdHNhcHAuY29tL3NlbmQ/cGhvbmU9OTE4ODAwNzY0OTA5JnRleHQ9SGkgR2V0UmFua3MsIEkgd291bGQgbGlrZSB0byBrbm93IG1vcmUgYWJvdXQgeW91ciBPbmxpbmUgUGxhdGZvcm0uXCJcbiAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy53aGF0c2FwcH0+Q2hhdCBvbjwvZGl2PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdHNhcHBfbG9nby5zdmdcIiBhbHQ9XCJ3aGF0c2FwcFwiIC8+XG4gICAgICAgICAgPC9hPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgey8qIDxkaXYgY2xhc3NOYW1lPXtzLmFjdGlvbnNXcmFwcGVyfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY3Rpb259PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWN0aW9uaW1nYm94fT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvcGxheV9wdXJwbGUuc3ZnXCIgYWx0PVwid2F0Y2ggdmlkZW9cIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8c3Bhbj5XYXRjaCB2aWRlbzwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY3Rpb259PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWN0aW9uaW1nYm94fT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvYnJvY2h1cmVfcHVycGxlLnN2Z1wiIGFsdD1cImJyb2NodXJlXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHNwYW4+RG93bmxvYWQgYnJvY2h1cmU8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PiAqL31cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG93bmxvYWRBcHB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvd25sb2FkVGV4dH0+RG93bmxvYWQgdGhlIGFwcDwvZGl2PlxuICAgICAgICAgIDxhXG4gICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9wbGF5Lmdvb2dsZS5jb20vc3RvcmUvYXBwcy9kZXRhaWxzP2lkPWNvbS5lZ25pZnkuZ2V0cmFua3MmaGw9ZW5fSU4mZ2w9VVNcIlxuICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnBsYXlTdG9yZUljb259XG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxheVN0b3JlLnBuZ1wiXG4gICAgICAgICAgICAgIGFsdD1cInBsYXlfc3RvcmVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlfT5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvRG91YnRzL2hlcm8ud2VicFwiIGFsdD1cIm1vYmlsZV9iYWNrZ3JvdW5kXCIgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Q2xpZW50cyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5kaXNwbGF5Q2xpZW50c30+XG4gICAgICA8aDM+XG4gICAgICAgIFRydXN0ZWQgYnkgPHNwYW4gLz5cbiAgICAgICAgbGVhZGluZyBFZHVjYXRpb25hbCBJbnN0aXR1dGlvbnNcbiAgICAgIDwvaDM+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jbGllbnRzV3JhcHBlcn0+XG4gICAgICAgIHtIT01FX0NMSUVOVFNfVVBEQVRFRC5tYXAoY2xpZW50ID0+IChcbiAgICAgICAgICA8aW1nXG4gICAgICAgICAgICBjbGFzc05hbWU9e3MuY2xpZW50fVxuICAgICAgICAgICAga2V5PXtjbGllbnQuaWR9XG4gICAgICAgICAgICBzcmM9e2NsaWVudC5pY29ufVxuICAgICAgICAgICAgYWx0PVwiY2xpbmV0XCJcbiAgICAgICAgICAvPlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPHNwYW4+XG4gICAgICAgIDxMaW5rIHRvPVwiL2N1c3RvbWVyc1wiPmNsaWNrIGhlcmUgZm9yIG1vcmUuLi48L0xpbms+XG4gICAgICA8L3NwYW4+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUFjaGlldmVkU29GYXIgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRIZWFkaW5nfT5cbiAgICAgICAgV2hhdCB3ZSBoYXZlIGFjaGlldmVkIDxzcGFuIC8+XG4gICAgICAgIHNvIGZhclxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZFJvd30+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgyNTUsIDEwMiwgMCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLmNsaWVudHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL2NsaWVudHMuc3ZnXCIgYWx0PVwicHJvZmlsZVwiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLmNsaWVudHNIaWdobGlnaHR9YH0+MTUwKzwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5DbGllbnRzPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3Muc3R1ZGVudHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3N0dWRlbnRzLnN2Z1wiIGFsdD1cInN0dWRlbnRzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3Muc3R1ZGVudHNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAzIExha2grXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPlN0dWRlbnRzPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDAsIDExNSwgMjU1LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MudGVzdHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3Rlc3RzLnN2Z1wiIGFsdD1cInRlc3RzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MudGVzdHNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAzIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPlRlc3RzIEF0dGVtcHRlZDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgwLCAxNzIsIDM4LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MucXVlc3Rpb25zfWB9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9xdWVzdGlvbnMuc3ZnXCJcbiAgICAgICAgICAgICAgYWx0PVwicXVlc3Rpb25zXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnF1ZXN0aW9uc0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDIwIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zdWJUZXh0fT5RdWVzdGlvbnMgU29sdmVkPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlEb3VidHNUdXJuVXAgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Mucm9sZXNDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICA8aDI+RG91YnRzIGNhbiB0dXJuIHVwIGFueXRpbWU8L2gyPlxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgV2l0aCBEb3VidHMgbW9kdWxlIHlvdXIgc3R1ZGVudHMgbGVhcm5pbmcgbmV2ZXIgc3RvcHMgYmVjYXVzZSBvZiBhXG4gICAgICAgICAgICBEb3VidC4gQSBzdHVkZW50IGNhbiB1cGxvYWQgYSBwaWN0dXJlIG9yIHR5cGUgYSBkb3VidCBhbmQgc3VibWl0XG4gICAgICAgICAgICBhY3Jvc3MgYWxsIHN1YmplY3RzIGFuZCB0b3BpY3MuXG4gICAgICAgICAgPC9wPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuZG91YnRzdHVybnVwfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL0RvdWJ0cy90dXJudXAud2VicFwiIGFsdD1cInR1cm51cC1hbnl0aW1lXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5VW50aWxSZXNvbHZlZCA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb2xlc0NvbnRhaW5lcn0gJHtzLnJldmVyc2V9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgIDxoMj5Bc2sgdW50aWwgaXQgaXMgcmVzb2x2ZWQ8L2gyPlxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgU3R1ZGVudCBjYW4gYXNrIGFzIG1hbnkgZG91YnRzIGFzIGhlL3NoZSB3YW50cy4gV2l0aCB0aGUgJnF1b3Q7RG91YnRcbiAgICAgICAgICAgIFJlc29sdmVkJnF1b3Q7IG9wdGlvbiwgc3R1ZGVudCBhbmQgdGVhY2hlcnMgY2FuIGtlZXAgdHJhY2sgb2ZcbiAgICAgICAgICAgIHVucmVzb2x2ZWQgZG91YnRzLlxuICAgICAgICAgIDwvcD5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLnVudGlscmVzb2x2ZWR9YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvRG91YnRzL2Fza191bnRpbC53ZWJwXCIgYWx0PVwiYXNrLXVudGlsLXJlc29sdmVkXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICBkaXNwbGF5QW5zd2VyQW55VGltZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5yb2xlc0NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnR9PlxuICAgICAgICAgIDxoMj5BbnN3ZXIgZG91YnQgYW55dGltZSwgYW55d2hlcmU8L2gyPlxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgVGVhY2hlciB3aWxsIGhhdmUgYSBtb2JpbGUgYW5kIHdlYiBhcHAgdG8gYW5zd2VyIGRvdWJ0cyBhbnl0aW1lIGFuZFxuICAgICAgICAgICAgYW55d2hlcmVcbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5hc2thbnl0aW1lfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL0RvdWJ0cy9hc2tfYW55d2hlcmUud2VicFwiIGFsdD1cImFzay1hbnl3aGVyZVwiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbiAgZGlzcGxheUFjY2Vzc0FuZEFuYWx5c2UgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm9sZXNDb250YWluZXJ9ICR7cy5yZXZlcnNlfWB9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICA8aDI+QWNjZXNzIGFuZCBBbmFseXNlIGRvdWJ0czwvaDI+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICBTdHVkZW50LCBUZWFjaGVyLCBQYXJlbnRzIHdpbGwgaGF2ZSBhY2Nlc3MgdG8gYWxsIGRvdWJ0cyBhbmQgYSBjbGVhclxuICAgICAgICAgICAgYW5hbHlzaXMgb24gdG90YWwgZG91YnRzIGFza2VkIHZzIHJlc29sdmVkIGFjcm9zcyBhbGwgc3ViamVjdCxcbiAgICAgICAgICAgIHRvcGljcyBhbmQgY2hhcHRlcnNcbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5hY2Nlc3NhbmRhbmFseXNlfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL0RvdWJ0cy9hY2Nlc3NfYW5hbHl6ZS53ZWJwXCJcbiAgICAgICAgICAgICAgYWx0PVwiYWNjZXNzLWFuYWx5emVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlBdmFpbGFibGVPbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdmFpbGFibGVDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlUm93fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlQ29udGVudFNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZVRpdGxlfT5cbiAgICAgICAgICAgIFdl4oCZcmUgPGJyIC8+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlfT5hdmFpbGFibGU8L3NwYW4+IG9uXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mucm93fT5cbiAgICAgICAgICAgIHtQTEFURk9STVMubWFwKGl0ZW0gPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGF0Zm9ybUNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PVwiXCIgaGVpZ2h0PVwiNDBweFwiIHdpZHRoPVwiNDBweFwiIC8+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhdGZvcm19PntpdGVtLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYXRmb3JtT3N9PntpdGVtLmF2YWlsYWJsZX08L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdG9yZX0+XG4gICAgICAgICAgICA8YVxuICAgICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9wbGF5Lmdvb2dsZS5jb20vc3RvcmUvYXBwcy9kZXRhaWxzP2lkPWNvbS5lZ25pZnkuZ2V0cmFua3MmaGw9ZW5fSU4mZ2w9VVNcIlxuICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxheVN0b3JlLnBuZ1wiXG4gICAgICAgICAgICAgICAgYWx0PVwiUGxheSBTdG9yZVwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnBsYXlzdG9yZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL3BsYXRmb3Jtcy9hcHBTdG9yZS5wbmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJBcHAgU3RvcmVcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuYXBwc3RvcmV9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8aW1nXG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmRlc2t0b3BJbWFnZX1cbiAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXRmb3Jtc192Mi53ZWJwXCJcbiAgICAgICAgICBhbHQ9XCJwbGF0Zm9ybVwiXG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5Sm9pbkZhY2Vib29rID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmpvaW5Db21tdW5pdHl9PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmpvaW5GYlRleHR9PkpvaW4gb3VyIEZhY2Vib29rIENvbW11bml0eTwvc3Bhbj5cbiAgICAgIDxhIGNsYXNzTmFtZT17cy5qb2luRmJMaW5rfSBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL0VnbmlmeS9cIj5cbiAgICAgICAgSm9pbiBOb3dcbiAgICAgIDwvYT5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJvb3R9PlxuICAgICAgICB7dGhpcy5kaXNwbGF5VGVhY2hTZWN0aW9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlDbGllbnRzKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlBY2hpZXZlZFNvRmFyKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlEb3VidHNUdXJuVXAoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheVVudGlsUmVzb2x2ZWQoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUFuc3dlckFueVRpbWUoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUFjY2Vzc0FuZEFuYWx5c2UoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUF2YWlsYWJsZU9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlKb2luRmFjZWJvb2soKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShEb3VidHMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Eb3VidHMuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Eb3VidHMuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0RvdWJ0cy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0JztcbmltcG9ydCBEb3VidHMgZnJvbSAnLi9Eb3VidHMnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdHZXRSYW5rcyBieSBFZ25pZnk6IEFzc2Vzc21lbnQgJiBBbmFseXRpY3MgUGxhdGZvcm0nLFxuICAgIGNodW5rczogWydEb3VidHMnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxEb3VidHMgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFyRUE7QUFDQTtBQTBFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBakJBO0FBQ0E7QUFxQkE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXREQTtBQUNBO0FBMkRBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWxCQTtBQUNBO0FBc0JBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWxCQTtBQUNBO0FBc0JBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWpCQTtBQUNBO0FBcUJBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJCQTtBQUNBO0FBMEJBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJDQTtBQUNBO0FBNkNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBQ0E7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBOzs7O0FBbFVBO0FBQ0E7QUFvVUE7Ozs7Ozs7QUMzVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFZQTs7OztBIiwic291cmNlUm9vdCI6IiJ9