(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["HowWorks"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/HowWorks/HowWorks.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".HowWorks-root-2OdTt {\n  color: #25282b;\n}\n\n.HowWorks-maxContainer-3apfi {\n  max-width: 1700px;\n}\n\n.HowWorks-headerContainer-2anVa {\n  padding: 96px 64px;\n  padding: 6rem 4rem;\n  position: relative;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-maxContainer-3apfi {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: auto;\n}\n\n.HowWorks-triangle-1wWJT {\n  position: absolute;\n  top: 14.7%;\n  left: 7.6%;\n  width: 60px;\n  height: 70px;\n}\n\n.HowWorks-chemistry-Wj8pU {\n  position: absolute;\n  bottom: 23.5%;\n  left: 7.6%;\n  width: 74px;\n  height: 94px;\n}\n\n.HowWorks-maths-Smakv {\n  position: absolute;\n  bottom: 16.8%;\n  right: 15.7%;\n  width: 90px;\n  height: 62px;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-header_title-1pe4e {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: center;\n  margin-bottom: 24px;\n  font-weight: bold;\n}\n\n.HowWorks-headerContainer-2anVa .HowWorks-header_content-6V6mN {\n  font-size: 20px;\n  line-height: 32px;\n  margin-bottom: 40px;\n  text-align: center;\n  max-width: 670px;\n}\n\n.HowWorks-scrollTop-219ko {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.HowWorks-scrollTop-219ko img {\n    width: 100%;\n    height: 100%;\n  }\n\n.HowWorks-buttonWrapper-1y5jj {\n  margin: 0 auto;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  max-width: 600px;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-requestDemo-2m23S {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  min-width: 240px;\n  min-width: 15rem;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  margin: 0 0.5rem 0 2rem;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU img {\n  width: 32px;\n  width: 2rem;\n  height: 32px;\n  height: 2rem;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-numberWrapper-R4yQ1 {\n  width: 24px;\n  height: 24px;\n  border-radius: 50%;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-right: 20px;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- span {\n  margin: 16px 0;\n  margin: 1rem 0;\n  font-size: 16px;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-numberWrapper-R4yQ1 span {\n  font-size: 14px;\n  color: #fff;\n}\n\n.HowWorks-navigator-1fc00 {\n  width: 100%;\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  position: relative;\n  margin: 0 auto;\n  border-top: 1px solid #d9d9d9;\n  padding: 80px 0;\n  padding: 5rem 0;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- {\n  width: 20%;\n  height: 50vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  position: -webkit-sticky;\n  position: sticky;\n  top: 164px;\n  border-right: 1px solid #d9d9d9;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 32px;\n  color: #25282b;\n  -webkit-transition: all 50ms ease-in-out;\n  -o-transition: all 50ms ease-in-out;\n  transition: all 50ms ease-in-out;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl.HowWorks-active-2Kxvn {\n  font-weight: bold;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1 {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background-color: #e5e5e5;\n  margin-right: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1.HowWorks-activeNumber-2c4oL {\n  background-color: #f36;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1 span {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- .HowWorks-navItem-zChdl .HowWorks-numberWrapper-R4yQ1.HowWorks-activeNumber-2c4oL span {\n  color: #fff;\n}\n\n.HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-scrollContainer-1JJ86 {\n  width: 80%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n/* .box {\n  width: 400px;\n  height: 400px;\n  margin: 32px auto;\n  border: 1px solid #f36;\n} */\n\n.HowWorks-section-3falQ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  width: 100%;\n  max-width: 800px;\n  margin: 0 auto;\n  padding: 80px 0;\n  border-bottom: 1px solid #d9d9d9;\n}\n\n.HowWorks-section-3falQ:first-child {\n  padding-top: 0;\n}\n\n.HowWorks-section-3falQ:last-child {\n  border-bottom: none;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O {\n  display: none;\n  width: 100%;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-left-1AuFu .HowWorks-title-2A7hc {\n  font-size: 14px;\n  line-height: 24px;\n}\n\n.HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O .HowWorks-arrowWrapper-2aqeX {\n  width: 24px;\n  height: 24px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 56px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-profile-2s6Ie {\n  width: 10%;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-grayBox-3y0if {\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n  background-color: #f0f0f0;\n  margin-right: 24px;\n  margin-top: 6px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-title-2A7hc {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-bottom: 12px;\n}\n\n.HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-text-18Q3A {\n  max-width: 500px;\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.HowWorks-section-3falQ .HowWorks-playerSection-1vIFi {\n  width: 100%;\n  height: 350px;\n  -webkit-box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n}\n\n@media only screen and (max-width: 990px) {\n  .HowWorks-headerContainer-2anVa {\n    padding: 40px 16px 80px;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-maxContainer-3apfi {\n    max-width: 550px;\n    margin: auto;\n  }\n\n  .HowWorks-triangle-1wWJT {\n    width: 34px;\n    height: 28px;\n    left: 6.1%;\n    top: 22.6%;\n  }\n\n  .HowWorks-chemistry-Wj8pU {\n    width: 36px;\n    height: 44px;\n    left: 2.5%;\n    top: 47.5%;\n  }\n\n  .HowWorks-maths-Smakv {\n    width: 44px;\n    height: 20px;\n    bottom: 17.6%;\n    right: 9%;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-header_title-1pe4e {\n    font-size: 32px;\n    line-height: 48px;\n    margin-bottom: 12px;\n  }\n\n  .HowWorks-headerContainer-2anVa .HowWorks-header_content-6V6mN {\n    font-size: 16px;\n    line-height: 24px;\n    margin-bottom: 48px;\n    max-width: 300px;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj .HowWorks-requestDemo-2m23S {\n    margin-bottom: 40px;\n  }\n\n  .HowWorks-buttonWrapper-1y5jj .HowWorks-whatsappwrapper-2B9Bl .HowWorks-whatsapp-2hvxU {\n    margin-left: 0;\n  }\n\n  .HowWorks-navigator-1fc00 {\n    padding: 0 16px;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi {\n    border-top: none;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-scrollContainer-1JJ86 {\n    width: 100%;\n  }\n\n  .HowWorks-navigator-1fc00 .HowWorks-maxContainer-3apfi .HowWorks-sidenav-1EI1- {\n    display: none;\n  }\n\n  .HowWorks-section-3falQ {\n    -ms-flex-align: start;\n        align-items: flex-start;\n    max-width: 600px;\n    border-bottom: none;\n    padding: 0 0 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-mobile_section-2Du5O {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n    -ms-flex-align: center;\n        align-items: center;\n    padding: 16px 0;\n    margin-bottom: 0;\n    border-bottom: 1px solid #d9d9d9;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    margin-bottom: 32px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd.HowWorks-hide-JGQHT {\n    display: none;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-profile-2s6Ie .HowWorks-grayBox-3y0if {\n    width: 40px;\n    height: 40px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-title-2A7hc {\n    font-size: 24px;\n    line-height: 32px;\n    margin: 12px 0 8px 0;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-section_content-TbMpd .HowWorks-content_box-1vQ-h .HowWorks-text-18Q3A {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-playerSection-1vIFi {\n    margin-bottom: 24px;\n  }\n\n  .HowWorks-section-3falQ .HowWorks-playerSection-1vIFi.HowWorks-hide-JGQHT {\n    display: none;\n  }\n\n  .HowWorks-scrollTop-219ko {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/HowWorks.scss"],"names":[],"mappings":"AAAA;EACE,eAAe;CAChB;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,WAAW;EACX,YAAY;EACZ,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,WAAW;EACX,YAAY;EACZ,aAAa;CACd;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,aAAa;EACb,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;EACnB,oBAAoB;EACpB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,oBAAoB;EACpB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,iBAAiB;EACjB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,gBAAgB;EAChB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,0BAA0B;EAC1B,qBAAqB;EACrB,wBAAwB;EACxB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;CACpB;;AAED;EACE,eAAe;EACf,eAAe;EACf,gBAAgB;CACjB;;AAED;EACE,gBAAgB;EAChB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,gBAAgB;CACjB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,eAAe;EACf,8BAA8B;EAC9B,gBAAgB;EAChB,gBAAgB;CACjB;;AAED;EACE,WAAW;EACX,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,yBAAyB;EACzB,iBAAiB;EACjB,WAAW;EACX,gCAAgC;CACjC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;EACpB,eAAe;EACf,yCAAyC;EACzC,oCAAoC;EACpC,iCAAiC;CAClC;;AAED;EACE,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;CACb;;AAED;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,YAAY;EACZ,iBAAiB;EACjB,eAAe;EACf,gBAAgB;EAChB,iCAAiC;CAClC;;AAED;EACE,eAAe;CAChB;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,cAAc;EACd,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,+BAA+B;EACnC,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;CACrB;;AAED;EACE,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,gBAAgB;CACjB;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,cAAc;EACd,qDAAqD;UAC7C,6CAA6C;CACtD;;AAED;EACE;IACE,wBAAwB;GACzB;;EAED;IACE,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;IACb,WAAW;IACX,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,WAAW;IACX,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,cAAc;IACd,UAAU;GACX;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;IACpB,iBAAiB;GAClB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,eAAe;GAChB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,YAAY;GACb;;EAED;IACE,cAAc;GACf;;EAED;IACE,sBAAsB;QAClB,wBAAwB;IAC5B,iBAAiB;IACjB,oBAAoB;IACpB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,+BAA+B;IACnC,uBAAuB;QACnB,oBAAoB;IACxB,gBAAgB;IAChB,iBAAiB;IACjB,iCAAiC;GAClC;;EAED;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,oBAAoB;GACrB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;CACF","file":"HowWorks.scss","sourcesContent":[".root {\n  color: #25282b;\n}\n\n.maxContainer {\n  max-width: 1700px;\n}\n\n.headerContainer {\n  padding: 96px 64px;\n  padding: 6rem 4rem;\n  position: relative;\n}\n\n.headerContainer .maxContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: auto;\n}\n\n.triangle {\n  position: absolute;\n  top: 14.7%;\n  left: 7.6%;\n  width: 60px;\n  height: 70px;\n}\n\n.chemistry {\n  position: absolute;\n  bottom: 23.5%;\n  left: 7.6%;\n  width: 74px;\n  height: 94px;\n}\n\n.maths {\n  position: absolute;\n  bottom: 16.8%;\n  right: 15.7%;\n  width: 90px;\n  height: 62px;\n}\n\n.headerContainer .header_title {\n  font-size: 40px;\n  line-height: 60px;\n  text-align: center;\n  margin-bottom: 24px;\n  font-weight: bold;\n}\n\n.headerContainer .header_content {\n  font-size: 20px;\n  line-height: 32px;\n  margin-bottom: 40px;\n  text-align: center;\n  max-width: 670px;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.buttonWrapper {\n  margin: 0 auto;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  max-width: 600px;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.buttonWrapper .requestDemo {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  min-width: 240px;\n  min-width: 15rem;\n}\n\n.buttonWrapper .whatsappwrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.buttonWrapper .whatsappwrapper .whatsapp {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  margin: 0 0.5rem 0 2rem;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.buttonWrapper .whatsappwrapper .whatsapp img {\n  width: 32px;\n  width: 2rem;\n  height: 32px;\n  height: 2rem;\n}\n\n.section .mobile_section .left .numberWrapper {\n  width: 24px;\n  height: 24px;\n  border-radius: 50%;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-right: 20px;\n}\n\n.navigator .maxContainer .sidenav span {\n  margin: 16px 0;\n  margin: 1rem 0;\n  font-size: 16px;\n}\n\n.section .mobile_section .left .numberWrapper span {\n  font-size: 14px;\n  color: #fff;\n}\n\n.navigator {\n  width: 100%;\n  padding: 0 64px;\n  padding: 0 4rem;\n}\n\n.navigator .maxContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  position: relative;\n  margin: 0 auto;\n  border-top: 1px solid #d9d9d9;\n  padding: 80px 0;\n  padding: 5rem 0;\n}\n\n.navigator .maxContainer .sidenav {\n  width: 20%;\n  height: 50vh;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  position: -webkit-sticky;\n  position: sticky;\n  top: 164px;\n  border-right: 1px solid #d9d9d9;\n}\n\n.navigator .maxContainer .sidenav .navItem {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 32px;\n  color: #25282b;\n  -webkit-transition: all 50ms ease-in-out;\n  -o-transition: all 50ms ease-in-out;\n  transition: all 50ms ease-in-out;\n}\n\n.navigator .maxContainer .sidenav .navItem.active {\n  font-weight: bold;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background-color: #e5e5e5;\n  margin-right: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper.activeNumber {\n  background-color: #f36;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper span {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.navigator .maxContainer .sidenav .navItem .numberWrapper.activeNumber span {\n  color: #fff;\n}\n\n.navigator .maxContainer .scrollContainer {\n  width: 80%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n/* .box {\n  width: 400px;\n  height: 400px;\n  margin: 32px auto;\n  border: 1px solid #f36;\n} */\n\n.section {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  width: 100%;\n  max-width: 800px;\n  margin: 0 auto;\n  padding: 80px 0;\n  border-bottom: 1px solid #d9d9d9;\n}\n\n.section:first-child {\n  padding-top: 0;\n}\n\n.section:last-child {\n  border-bottom: none;\n}\n\n.section .mobile_section {\n  display: none;\n  width: 100%;\n}\n\n.section .mobile_section .left {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.section .mobile_section .left .title {\n  font-size: 14px;\n  line-height: 24px;\n}\n\n.section .mobile_section .arrowWrapper {\n  width: 24px;\n  height: 24px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.section .section_content {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  margin-bottom: 56px;\n}\n\n.section .section_content .profile {\n  width: 10%;\n}\n\n.section .section_content .grayBox {\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n  background-color: #f0f0f0;\n  margin-right: 24px;\n  margin-top: 6px;\n}\n\n.section .section_content .content_box {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.section .section_content .content_box .title {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-bottom: 12px;\n}\n\n.section .section_content .content_box .text {\n  max-width: 500px;\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.section .playerSection {\n  width: 100%;\n  height: 350px;\n  -webkit-box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 4px 12px 0 rgba(0, 0, 0, 0.16);\n}\n\n@media only screen and (max-width: 990px) {\n  .headerContainer {\n    padding: 40px 16px 80px;\n  }\n\n  .headerContainer .maxContainer {\n    max-width: 550px;\n    margin: auto;\n  }\n\n  .triangle {\n    width: 34px;\n    height: 28px;\n    left: 6.1%;\n    top: 22.6%;\n  }\n\n  .chemistry {\n    width: 36px;\n    height: 44px;\n    left: 2.5%;\n    top: 47.5%;\n  }\n\n  .maths {\n    width: 44px;\n    height: 20px;\n    bottom: 17.6%;\n    right: 9%;\n  }\n\n  .headerContainer .header_title {\n    font-size: 32px;\n    line-height: 48px;\n    margin-bottom: 12px;\n  }\n\n  .headerContainer .header_content {\n    font-size: 16px;\n    line-height: 24px;\n    margin-bottom: 48px;\n    max-width: 300px;\n  }\n\n  .buttonWrapper {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .buttonWrapper .requestDemo {\n    margin-bottom: 40px;\n  }\n\n  .buttonWrapper .whatsappwrapper .whatsapp {\n    margin-left: 0;\n  }\n\n  .navigator {\n    padding: 0 16px;\n  }\n\n  .navigator .maxContainer {\n    border-top: none;\n  }\n\n  .navigator .maxContainer .scrollContainer {\n    width: 100%;\n  }\n\n  .navigator .maxContainer .sidenav {\n    display: none;\n  }\n\n  .section {\n    -ms-flex-align: start;\n        align-items: flex-start;\n    max-width: 600px;\n    border-bottom: none;\n    padding: 0 0 24px;\n  }\n\n  .section .mobile_section {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n    -ms-flex-align: center;\n        align-items: center;\n    padding: 16px 0;\n    margin-bottom: 0;\n    border-bottom: 1px solid #d9d9d9;\n  }\n\n  .section .section_content {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    margin-bottom: 32px;\n  }\n\n  .section .section_content.hide {\n    display: none;\n  }\n\n  .section .section_content .profile .grayBox {\n    width: 40px;\n    height: 40px;\n  }\n\n  .section .section_content .content_box .title {\n    font-size: 24px;\n    line-height: 32px;\n    margin: 12px 0 8px 0;\n  }\n\n  .section .section_content .content_box .text {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .section .playerSection {\n    margin-bottom: 24px;\n  }\n\n  .section .playerSection.hide {\n    display: none;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "HowWorks-root-2OdTt",
	"maxContainer": "HowWorks-maxContainer-3apfi",
	"headerContainer": "HowWorks-headerContainer-2anVa",
	"triangle": "HowWorks-triangle-1wWJT",
	"chemistry": "HowWorks-chemistry-Wj8pU",
	"maths": "HowWorks-maths-Smakv",
	"header_title": "HowWorks-header_title-1pe4e",
	"header_content": "HowWorks-header_content-6V6mN",
	"scrollTop": "HowWorks-scrollTop-219ko",
	"buttonWrapper": "HowWorks-buttonWrapper-1y5jj",
	"requestDemo": "HowWorks-requestDemo-2m23S",
	"whatsappwrapper": "HowWorks-whatsappwrapper-2B9Bl",
	"whatsapp": "HowWorks-whatsapp-2hvxU",
	"section": "HowWorks-section-3falQ",
	"mobile_section": "HowWorks-mobile_section-2Du5O",
	"left": "HowWorks-left-1AuFu",
	"numberWrapper": "HowWorks-numberWrapper-R4yQ1",
	"navigator": "HowWorks-navigator-1fc00",
	"sidenav": "HowWorks-sidenav-1EI1-",
	"navItem": "HowWorks-navItem-zChdl",
	"active": "HowWorks-active-2Kxvn",
	"activeNumber": "HowWorks-activeNumber-2c4oL",
	"scrollContainer": "HowWorks-scrollContainer-1JJ86",
	"title": "HowWorks-title-2A7hc",
	"arrowWrapper": "HowWorks-arrowWrapper-2aqeX",
	"section_content": "HowWorks-section_content-TbMpd",
	"profile": "HowWorks-profile-2s6Ie",
	"grayBox": "HowWorks-grayBox-3y0if",
	"content_box": "HowWorks-content_box-1vQ-h",
	"text": "HowWorks-text-18Q3A",
	"playerSection": "HowWorks-playerSection-1vIFi",
	"hide": "HowWorks-hide-JGQHT"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/HowWorks.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/HowWorks/HowWorks.scss");
/* harmony import */ var _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/HowWorks.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var HowWorks =
/*#__PURE__*/
function (_Component) {
  _inherits(HowWorks, _Component);

  function HowWorks(props) {
    var _this;

    _classCallCheck(this, HowWorks);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HowWorks).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleScroll", function () {
      if (window.scrollY > 500) {
        _this.setState({
          showScroll: true
        });
      } else {
        _this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleScrollTop", function () {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });

      _this.setState({
        showScroll: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleResize", function () {
      var isMobile = _this.state.isMobile;

      if (window.innerWidth > 990) {
        isMobile = false;
      } else {
        isMobile = true;
      }

      _this.setState({
        isMobile: isMobile
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleStepToggle", function (title, status) {
      var openList = _this.state.openList;

      if (status) {
        var ind = openList.findIndex(function (ele) {
          return ele === title;
        });

        if (ind > -1) {
          openList.splice(ind, 1);
        }

        _this.setState({
          openList: openList
        });
      } else {
        openList.push(title);

        _this.setState({
          openList: openList
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "observeElelements", function () {
      var observer = new IntersectionObserver(function (entries) {
        entries.forEach(function (entry) {
          var nodeId = entry.target.getAttribute('id');
          var link = document.querySelector("div.".concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sidenav, " a[href=\"#").concat(nodeId, "\"]"));

          if (entry.isIntersecting) {
            link.classList.add(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active);
            link.childNodes[0].classList.add(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.activeNumber);
          } else {
            link.classList.remove(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active);
            link.childNodes[0].classList.remove(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.activeNumber);
          }
        });
      }, {
        threshold: 0.55
      });
      var ele = document.getElementById("list").querySelectorAll("div[id]");
      ele.forEach(function (element) {
        observer.observe(element);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "displayHeader", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maxContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.header_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, "How egnify works"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.header_content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.buttonWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/request-demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.requestDemo,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, "START FREE TRIAL")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsappwrapper,
        href: "https://api.whatsapp.com/send?phone=919949523849&text=Hi GetRanks, I would like to know more about your Online Platform.",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsapp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/whatsapp_logo.svg",
        alt: "whatsapp",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.triangle,
        src: "/images/icons/subject_icons/triangle.svg",
        alt: "triangle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.chemistry,
        src: "/images/icons/subject_icons/chemistry.svg",
        alt: "triangle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maths,
        src: "/images/icons/subject_icons/scale.svg",
        alt: "maths",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displaySteps", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.navigator,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.maxContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sidenav,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS"].map(function (item, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: "#".concat(item.title),
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.navItem,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 156
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.numberWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 157
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 158
          },
          __self: this
        }, index + 1)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 160
          },
          __self: this
        }, item.title));
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: "list",
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.scrollContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS"].map(function (box, index) {
        var _this$state = _this.state,
            openList = _this$state.openList,
            isMobile = _this$state.isMobile;
        var isOpen = openList.includes(box.title);
        var contentClass = isOpen ? "".concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_content) : "".concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_content, " ").concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.hide);
        var playerClass = isOpen ? "".concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playerSection) : "".concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playerSection, " ").concat(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.hide);
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          id: box.title,
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 175
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobile_section,
          role: "presentation",
          onClick: function onClick() {
            if (isMobile) {
              _this.handleStepToggle(box.title, isOpen);
            }
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 176
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.left,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 185
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.numberWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 186
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 187
          },
          __self: this
        }, index + 1)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 189
          },
          __self: this
        }, box.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.arrowWrapper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 191
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: isOpen ? "/images/icons/chevron-up.svg" : "/images/icons/chevron-down.svg",
          alt: "arrow",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 192
          },
          __self: this
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: contentClass,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 202
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.profile,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 203
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.grayBox,
          src: box.img,
          alt: box.title,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 204
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content_box,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 206
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 207
          },
          __self: this
        }, box.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.text,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 208
          },
          __self: this
        }, box.content))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: playerClass,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 211
          },
          __self: this
        }));
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayScrollToTop", function () {
      var showScroll = _this.state.showScroll;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.scrollTop,
        role: "presentation",
        onClick: function onClick() {
          _this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }));
    });

    _this.state = {
      openList: _toConsumableArray(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOW_WORKS_TITLES"]),
      isMobile: false,
      showScroll: false
    };
    return _this;
  }

  _createClass(HowWorks, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleResize();
      this.observeElelements();
      this.handleScroll();
      window.addEventListener('scroll', this.handleScroll);
      document.addEventListener('resize', this.handleResize);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
      document.removeEventListener('resize', this.handleResize);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: "root",
        className: _HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, this.displayHeader(), this.displaySteps(), this.displayScrollToTop());
    }
  }]);

  return HowWorks;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_HowWorks_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(HowWorks));

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/HowWorks.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/HowWorks/HowWorks.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/HowWorks/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _HowWorks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/HowWorks/HowWorks.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/HowWorks/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify makes teaching easy',
              chunks: ['HowWorks'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HowWorks__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSG93V29ya3MuY2h1bmsuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Ib3dXb3Jrcy9Ib3dXb3Jrcy5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL0hvd1dvcmtzL0hvd1dvcmtzLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL0hvd1dvcmtzL0hvd1dvcmtzLnNjc3M/ODVlOSIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Ib3dXb3Jrcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLkhvd1dvcmtzLXJvb3QtMk9kVHQge1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkge1xcbiAgbWF4LXdpZHRoOiAxNzAwcHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmEge1xcbiAgcGFkZGluZzogOTZweCA2NHB4O1xcbiAgcGFkZGluZzogNnJlbSA0cmVtO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLkhvd1dvcmtzLXRyaWFuZ2xlLTF3V0pUIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogMTQuNyU7XFxuICBsZWZ0OiA3LjYlO1xcbiAgd2lkdGg6IDYwcHg7XFxuICBoZWlnaHQ6IDcwcHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1jaGVtaXN0cnktV2o4cFUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAyMy41JTtcXG4gIGxlZnQ6IDcuNiU7XFxuICB3aWR0aDogNzRweDtcXG4gIGhlaWdodDogOTRweDtcXG59XFxuXFxuLkhvd1dvcmtzLW1hdGhzLVNtYWt2IHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogMTYuOCU7XFxuICByaWdodDogMTUuNyU7XFxuICB3aWR0aDogOTBweDtcXG4gIGhlaWdodDogNjJweDtcXG59XFxuXFxuLkhvd1dvcmtzLWhlYWRlckNvbnRhaW5lci0yYW5WYSAuSG93V29ya3MtaGVhZGVyX3RpdGxlLTFwZTRlIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiA2MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4uSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIC5Ib3dXb3Jrcy1oZWFkZXJfY29udGVudC02VjZtTiB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXgtd2lkdGg6IDY3MHB4O1xcbn1cXG5cXG4uSG93V29ya3Mtc2Nyb2xsVG9wLTIxOWtvIHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIHdpZHRoOiA0MHB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbiAgcmlnaHQ6IDY0cHg7XFxuICBib3R0b206IDY0cHg7XFxuICB6LWluZGV4OiAxO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbn1cXG5cXG4uSG93V29ya3Mtc2Nyb2xsVG9wLTIxOWtvIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amoge1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogNjAwcHg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5Ib3dXb3Jrcy1idXR0b25XcmFwcGVyLTF5NWpqIC5Ib3dXb3Jrcy1yZXF1ZXN0RGVtby0ybTIzUyB7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2ZjO1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBjb2xvcjogIzAwMDtcXG4gIHBhZGRpbmc6IDE2cHggMjRweDtcXG4gIHdpZHRoOiAtd2Via2l0LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XFxuICBtaW4td2lkdGg6IDI0MHB4O1xcbiAgbWluLXdpZHRoOiAxNXJlbTtcXG59XFxuXFxuLkhvd1dvcmtzLWJ1dHRvbldyYXBwZXItMXk1amogLkhvd1dvcmtzLXdoYXRzYXBwd3JhcHBlci0yQjlCbCB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiAuSG93V29ya3Mtd2hhdHNhcHB3cmFwcGVyLTJCOUJsIC5Ib3dXb3Jrcy13aGF0c2FwcC0yaHZ4VSB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogIzI1MjgyYiAhaW1wb3J0YW50O1xcbiAgbWFyZ2luOiAwIDhweCAwIDMycHg7XFxuICBtYXJnaW46IDAgMC41cmVtIDAgMnJlbTtcXG4gIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgb3BhY2l0eTogMC42O1xcbn1cXG5cXG4uSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiAuSG93V29ya3Mtd2hhdHNhcHB3cmFwcGVyLTJCOUJsIC5Ib3dXb3Jrcy13aGF0c2FwcC0yaHZ4VSBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICB3aWR0aDogMnJlbTtcXG4gIGhlaWdodDogMzJweDtcXG4gIGhlaWdodDogMnJlbTtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIC5Ib3dXb3Jrcy1sZWZ0LTFBdUZ1IC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMjBweDtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIHNwYW4ge1xcbiAgbWFyZ2luOiAxNnB4IDA7XFxuICBtYXJnaW46IDFyZW0gMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIC5Ib3dXb3Jrcy1sZWZ0LTFBdUZ1IC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiAwIDY0cHg7XFxuICBwYWRkaW5nOiAwIDRyZW07XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbiAgcGFkZGluZzogODBweCAwO1xcbiAgcGFkZGluZzogNXJlbSAwO1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkgLkhvd1dvcmtzLXNpZGVuYXYtMUVJMS0ge1xcbiAgd2lkdGg6IDIwJTtcXG4gIGhlaWdodDogNTB2aDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBwb3NpdGlvbjogLXdlYmtpdC1zdGlja3k7XFxuICBwb3NpdGlvbjogc3RpY2t5O1xcbiAgdG9wOiAxNjRweDtcXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNkOWQ5ZDk7XFxufVxcblxcbi5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSAuSG93V29ya3Mtc2lkZW5hdi0xRUkxLSAuSG93V29ya3MtbmF2SXRlbS16Q2hkbCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCA1MG1zIGVhc2UtaW4tb3V0O1xcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDUwbXMgZWFzZS1pbi1vdXQ7XFxuICB0cmFuc2l0aW9uOiBhbGwgNTBtcyBlYXNlLWluLW91dDtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIC5Ib3dXb3Jrcy1uYXZJdGVtLXpDaGRsLkhvd1dvcmtzLWFjdGl2ZS0yS3h2biB7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIC5Ib3dXb3Jrcy1uYXZJdGVtLXpDaGRsIC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExIHtcXG4gIHdpZHRoOiAzMHB4O1xcbiAgaGVpZ2h0OiAzMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U1ZTVlNTtcXG4gIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIC5Ib3dXb3Jrcy1uYXZJdGVtLXpDaGRsIC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExLkhvd1dvcmtzLWFjdGl2ZU51bWJlci0yYzRvTCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkgLkhvd1dvcmtzLXNpZGVuYXYtMUVJMS0gLkhvd1dvcmtzLW5hdkl0ZW0tekNoZGwgLkhvd1dvcmtzLW51bWJlcldyYXBwZXItUjR5UTEgc3BhbiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG59XFxuXFxuLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIC5Ib3dXb3Jrcy1uYXZJdGVtLXpDaGRsIC5Ib3dXb3Jrcy1udW1iZXJXcmFwcGVyLVI0eVExLkhvd1dvcmtzLWFjdGl2ZU51bWJlci0yYzRvTCBzcGFuIHtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkgLkhvd1dvcmtzLXNjcm9sbENvbnRhaW5lci0xSko4NiB7XFxuICB3aWR0aDogODAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLyogLmJveCB7XFxuICB3aWR0aDogNDAwcHg7XFxuICBoZWlnaHQ6IDQwMHB4O1xcbiAgbWFyZ2luOiAzMnB4IGF1dG87XFxuICBib3JkZXI6IDFweCBzb2xpZCAjZjM2O1xcbn0gKi9cXG5cXG4uSG93V29ya3Mtc2VjdGlvbi0zZmFsUSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDgwMHB4O1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwYWRkaW5nOiA4MHB4IDA7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFE6Zmlyc3QtY2hpbGQge1xcbiAgcGFkZGluZy10b3A6IDA7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxROmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIC5Ib3dXb3Jrcy1sZWZ0LTFBdUZ1IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIC5Ib3dXb3Jrcy1sZWZ0LTFBdUZ1IC5Ib3dXb3Jrcy10aXRsZS0yQTdoYyB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIC5Ib3dXb3Jrcy1hcnJvd1dyYXBwZXItMmFxZVgge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIG1hcmdpbi1ib3R0b206IDU2cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLXByb2ZpbGUtMnM2SWUge1xcbiAgd2lkdGg6IDEwJTtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZCAuSG93V29ya3MtZ3JheUJveC0zeTBpZiB7XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogNDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XFxuICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICBtYXJnaW4tdG9wOiA2cHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZCAuSG93V29ya3MtY29udGVudF9ib3gtMXZRLWggLkhvd1dvcmtzLXRpdGxlLTJBN2hjIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxufVxcblxcbi5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oIC5Ib3dXb3Jrcy10ZXh0LTE4UTNBIHtcXG4gIG1heC13aWR0aDogNTAwcHg7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG59XFxuXFxuLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXBsYXllclNlY3Rpb24tMXZJRmkge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDM1MHB4O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5Ib3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmEge1xcbiAgICBwYWRkaW5nOiA0MHB4IDE2cHggODBweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmEgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSB7XFxuICAgIG1heC13aWR0aDogNTUwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy10cmlhbmdsZS0xd1dKVCB7XFxuICAgIHdpZHRoOiAzNHB4O1xcbiAgICBoZWlnaHQ6IDI4cHg7XFxuICAgIGxlZnQ6IDYuMSU7XFxuICAgIHRvcDogMjIuNiU7XFxuICB9XFxuXFxuICAuSG93V29ya3MtY2hlbWlzdHJ5LVdqOHBVIHtcXG4gICAgd2lkdGg6IDM2cHg7XFxuICAgIGhlaWdodDogNDRweDtcXG4gICAgbGVmdDogMi41JTtcXG4gICAgdG9wOiA0Ny41JTtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1tYXRocy1TbWFrdiB7XFxuICAgIHdpZHRoOiA0NHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICAgIGJvdHRvbTogMTcuNiU7XFxuICAgIHJpZ2h0OiA5JTtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmEgLkhvd1dvcmtzLWhlYWRlcl90aXRsZS0xcGU0ZSB7XFxuICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICB9XFxuXFxuICAuSG93V29ya3MtaGVhZGVyQ29udGFpbmVyLTJhblZhIC5Ib3dXb3Jrcy1oZWFkZXJfY29udGVudC02VjZtTiB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDQ4cHg7XFxuICAgIG1heC13aWR0aDogMzAwcHg7XFxuICB9XFxuXFxuICAuSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1idXR0b25XcmFwcGVyLTF5NWpqIC5Ib3dXb3Jrcy1yZXF1ZXN0RGVtby0ybTIzUyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuICAuSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqaiAuSG93V29ya3Mtd2hhdHNhcHB3cmFwcGVyLTJCOUJsIC5Ib3dXb3Jrcy13aGF0c2FwcC0yaHZ4VSB7XFxuICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCB7XFxuICAgIHBhZGRpbmc6IDAgMTZweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1uYXZpZ2F0b3ItMWZjMDAgLkhvd1dvcmtzLW1heENvbnRhaW5lci0zYXBmaSB7XFxuICAgIGJvcmRlci10b3A6IG5vbmU7XFxuICB9XFxuXFxuICAuSG93V29ya3MtbmF2aWdhdG9yLTFmYzAwIC5Ib3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmkgLkhvd1dvcmtzLXNjcm9sbENvbnRhaW5lci0xSko4NiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMCAuSG93V29ya3MtbWF4Q29udGFpbmVyLTNhcGZpIC5Ib3dXb3Jrcy1zaWRlbmF2LTFFSTEtIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIHtcXG4gICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgIHBhZGRpbmc6IDAgMCAyNHB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBwYWRkaW5nOiAxNnB4IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDlkOWQ5O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZCB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQuSG93V29ya3MtaGlkZS1KR1FIVCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuSG93V29ya3Mtc2VjdGlvbi0zZmFsUSAuSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkIC5Ib3dXb3Jrcy1wcm9maWxlLTJzNkllIC5Ib3dXb3Jrcy1ncmF5Qm94LTN5MGlmIHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDBweDtcXG4gIH1cXG5cXG4gIC5Ib3dXb3Jrcy1zZWN0aW9uLTNmYWxRIC5Ib3dXb3Jrcy1zZWN0aW9uX2NvbnRlbnQtVGJNcGQgLkhvd1dvcmtzLWNvbnRlbnRfYm94LTF2US1oIC5Ib3dXb3Jrcy10aXRsZS0yQTdoYyB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgIG1hcmdpbjogMTJweCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXNlY3Rpb25fY29udGVudC1UYk1wZCAuSG93V29ya3MtY29udGVudF9ib3gtMXZRLWggLkhvd1dvcmtzLXRleHQtMThRM0Ege1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXBsYXllclNlY3Rpb24tMXZJRmkge1xcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgfVxcblxcbiAgLkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFEgLkhvd1dvcmtzLXBsYXllclNlY3Rpb24tMXZJRmkuSG93V29ya3MtaGlkZS1KR1FIVCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuSG93V29ya3Mtc2Nyb2xsVG9wLTIxOWtvIHtcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgcmlnaHQ6IDE2cHg7XFxuICAgIGJvdHRvbTogMTZweDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9Ib3dXb3Jrcy9Ib3dXb3Jrcy5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQiwwQkFBMEI7RUFDMUIscUJBQXFCO0VBQ3JCLHdCQUF3QjtFQUN4QixrQkFBa0I7RUFDbEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGVBQWU7RUFDZixlQUFlO0VBQ2YsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsOEJBQThCO0VBQzlCLGdCQUFnQjtFQUNoQixnQkFBZ0I7Q0FDakI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxnQ0FBZ0M7Q0FDakM7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZix5Q0FBeUM7RUFDekMsb0NBQW9DO0VBQ3BDLGlDQUFpQztDQUNsQzs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7Q0FDN0I7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtDQUM1Qjs7QUFFRDs7Ozs7SUFLSTs7QUFFSjtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQ0FBaUM7Q0FDbEM7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsY0FBYztFQUNkLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7Q0FDNUI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QscURBQXFEO1VBQzdDLDZDQUE2QztDQUN0RDs7QUFFRDtFQUNFO0lBQ0Usd0JBQXdCO0dBQ3pCOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsY0FBYztJQUNkLFVBQVU7R0FDWDs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtHQUM1Qjs7RUFFRDtJQUNFLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGVBQWU7R0FDaEI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLGlCQUFpQjtJQUNqQixvQkFBb0I7SUFDcEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx1QkFBdUI7UUFDbkIsK0JBQStCO0lBQ25DLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixpQ0FBaUM7R0FDbEM7O0VBRUQ7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixhQUFhO0dBQ2Q7Q0FDRlwiLFwiZmlsZVwiOlwiSG93V29ya3Muc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucm9vdCB7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLm1heENvbnRhaW5lciB7XFxuICBtYXgtd2lkdGg6IDE3MDBweDtcXG59XFxuXFxuLmhlYWRlckNvbnRhaW5lciB7XFxuICBwYWRkaW5nOiA5NnB4IDY0cHg7XFxuICBwYWRkaW5nOiA2cmVtIDRyZW07XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5oZWFkZXJDb250YWluZXIgLm1heENvbnRhaW5lciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4udHJpYW5nbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAxNC43JTtcXG4gIGxlZnQ6IDcuNiU7XFxuICB3aWR0aDogNjBweDtcXG4gIGhlaWdodDogNzBweDtcXG59XFxuXFxuLmNoZW1pc3RyeSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IDIzLjUlO1xcbiAgbGVmdDogNy42JTtcXG4gIHdpZHRoOiA3NHB4O1xcbiAgaGVpZ2h0OiA5NHB4O1xcbn1cXG5cXG4ubWF0aHMge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAxNi44JTtcXG4gIHJpZ2h0OiAxNS43JTtcXG4gIHdpZHRoOiA5MHB4O1xcbiAgaGVpZ2h0OiA2MnB4O1xcbn1cXG5cXG4uaGVhZGVyQ29udGFpbmVyIC5oZWFkZXJfdGl0bGUge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDYwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5oZWFkZXJDb250YWluZXIgLmhlYWRlcl9jb250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1heC13aWR0aDogNjcwcHg7XFxufVxcblxcbi5zY3JvbGxUb3Age1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICByaWdodDogNjRweDtcXG4gIGJvdHRvbTogNjRweDtcXG4gIHotaW5kZXg6IDE7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5zY3JvbGxUb3AgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gIH1cXG5cXG4uYnV0dG9uV3JhcHBlciB7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiA2MDBweDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmJ1dHRvbldyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGNvbG9yOiAjMDAwO1xcbiAgcGFkZGluZzogMTZweCAyNHB4O1xcbiAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1tYXgtY29udGVudDtcXG4gIHdpZHRoOiBtYXgtY29udGVudDtcXG4gIG1pbi13aWR0aDogMjQwcHg7XFxuICBtaW4td2lkdGg6IDE1cmVtO1xcbn1cXG5cXG4uYnV0dG9uV3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5idXR0b25XcmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjMjUyODJiICFpbXBvcnRhbnQ7XFxuICBtYXJnaW46IDAgOHB4IDAgMzJweDtcXG4gIG1hcmdpbjogMCAwLjVyZW0gMCAycmVtO1xcbiAgcGFkZGluZy1ib3R0b206IDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5idXR0b25XcmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIGltZyB7XFxuICB3aWR0aDogMzJweDtcXG4gIHdpZHRoOiAycmVtO1xcbiAgaGVpZ2h0OiAzMnB4O1xcbiAgaGVpZ2h0OiAycmVtO1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24gLmxlZnQgLm51bWJlcldyYXBwZXIge1xcbiAgd2lkdGg6IDI0cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgc3BhbiB7XFxuICBtYXJnaW46IDE2cHggMDtcXG4gIG1hcmdpbjogMXJlbSAwO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24gLmxlZnQgLm51bWJlcldyYXBwZXIgc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLm5hdmlnYXRvciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDAgNjRweDtcXG4gIHBhZGRpbmc6IDAgNHJlbTtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOWQ5ZDk7XFxuICBwYWRkaW5nOiA4MHB4IDA7XFxuICBwYWRkaW5nOiA1cmVtIDA7XFxufVxcblxcbi5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciAuc2lkZW5hdiB7XFxuICB3aWR0aDogMjAlO1xcbiAgaGVpZ2h0OiA1MHZoO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHBvc2l0aW9uOiAtd2Via2l0LXN0aWNreTtcXG4gIHBvc2l0aW9uOiBzdGlja3k7XFxuICB0b3A6IDE2NHB4O1xcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIC5zaWRlbmF2IC5uYXZJdGVtIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDUwbXMgZWFzZS1pbi1vdXQ7XFxuICAtby10cmFuc2l0aW9uOiBhbGwgNTBtcyBlYXNlLWluLW91dDtcXG4gIHRyYW5zaXRpb246IGFsbCA1MG1zIGVhc2UtaW4tb3V0O1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgLm5hdkl0ZW0uYWN0aXZlIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgLm5hdkl0ZW0gLm51bWJlcldyYXBwZXIge1xcbiAgd2lkdGg6IDMwcHg7XFxuICBoZWlnaHQ6IDMwcHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTVlNWU1O1xcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgLm5hdkl0ZW0gLm51bWJlcldyYXBwZXIuYWN0aXZlTnVtYmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciAuc2lkZW5hdiAubmF2SXRlbSAubnVtYmVyV3JhcHBlciBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbn1cXG5cXG4ubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYgLm5hdkl0ZW0gLm51bWJlcldyYXBwZXIuYWN0aXZlTnVtYmVyIHNwYW4ge1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciAuc2Nyb2xsQ29udGFpbmVyIHtcXG4gIHdpZHRoOiA4MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG4vKiAuYm94IHtcXG4gIHdpZHRoOiA0MDBweDtcXG4gIGhlaWdodDogNDAwcHg7XFxuICBtYXJnaW46IDMycHggYXV0bztcXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmMzY7XFxufSAqL1xcblxcbi5zZWN0aW9uIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogODAwcHg7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHBhZGRpbmc6IDgwcHggMDtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uc2VjdGlvbjpmaXJzdC1jaGlsZCB7XFxuICBwYWRkaW5nLXRvcDogMDtcXG59XFxuXFxuLnNlY3Rpb246bGFzdC1jaGlsZCB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24ge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24gLmxlZnQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24gLmxlZnQgLnRpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbn1cXG5cXG4uc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24gLmFycm93V3JhcHBlciB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNTZweDtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAucHJvZmlsZSB7XFxuICB3aWR0aDogMTAlO1xcbn1cXG5cXG4uc2VjdGlvbiAuc2VjdGlvbl9jb250ZW50IC5ncmF5Qm94IHtcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiA0OHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG4gIG1hcmdpbi10b3A6IDZweDtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuY29udGVudF9ib3gge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG4uc2VjdGlvbiAuc2VjdGlvbl9jb250ZW50IC5jb250ZW50X2JveCAudGl0bGUge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuY29udGVudF9ib3ggLnRleHQge1xcbiAgbWF4LXdpZHRoOiA1MDBweDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbn1cXG5cXG4uc2VjdGlvbiAucGxheWVyU2VjdGlvbiB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMzUwcHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDEycHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDQwcHggMTZweCA4MHB4O1xcbiAgfVxcblxcbiAgLmhlYWRlckNvbnRhaW5lciAubWF4Q29udGFpbmVyIHtcXG4gICAgbWF4LXdpZHRoOiA1NTBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLnRyaWFuZ2xlIHtcXG4gICAgd2lkdGg6IDM0cHg7XFxuICAgIGhlaWdodDogMjhweDtcXG4gICAgbGVmdDogNi4xJTtcXG4gICAgdG9wOiAyMi42JTtcXG4gIH1cXG5cXG4gIC5jaGVtaXN0cnkge1xcbiAgICB3aWR0aDogMzZweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBsZWZ0OiAyLjUlO1xcbiAgICB0b3A6IDQ3LjUlO1xcbiAgfVxcblxcbiAgLm1hdGhzIHtcXG4gICAgd2lkdGg6IDQ0cHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gICAgYm90dG9tOiAxNy42JTtcXG4gICAgcmlnaHQ6IDklO1xcbiAgfVxcblxcbiAgLmhlYWRlckNvbnRhaW5lciAuaGVhZGVyX3RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICBsaW5lLWhlaWdodDogNDhweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJDb250YWluZXIgLmhlYWRlcl9jb250ZW50IHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogNDhweDtcXG4gICAgbWF4LXdpZHRoOiAzMDBweDtcXG4gIH1cXG5cXG4gIC5idXR0b25XcmFwcGVyIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLmJ1dHRvbldyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4gIC5idXR0b25XcmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7XFxuICB9XFxuXFxuICAubmF2aWdhdG9yIHtcXG4gICAgcGFkZGluZzogMCAxNnB4O1xcbiAgfVxcblxcbiAgLm5hdmlnYXRvciAubWF4Q29udGFpbmVyIHtcXG4gICAgYm9yZGVyLXRvcDogbm9uZTtcXG4gIH1cXG5cXG4gIC5uYXZpZ2F0b3IgLm1heENvbnRhaW5lciAuc2Nyb2xsQ29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAubmF2aWdhdG9yIC5tYXhDb250YWluZXIgLnNpZGVuYXYge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnNlY3Rpb24ge1xcbiAgICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcXG4gICAgcGFkZGluZzogMCAwIDI0cHg7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAubW9iaWxlX3NlY3Rpb24ge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHBhZGRpbmc6IDE2cHggMDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOWQ5ZDk7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAuc2VjdGlvbl9jb250ZW50IHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudC5oaWRlIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uIC5zZWN0aW9uX2NvbnRlbnQgLnByb2ZpbGUgLmdyYXlCb3gge1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgfVxcblxcbiAgLnNlY3Rpb24gLnNlY3Rpb25fY29udGVudCAuY29udGVudF9ib3ggLnRpdGxlIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgbWFyZ2luOiAxMnB4IDAgOHB4IDA7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAuc2VjdGlvbl9jb250ZW50IC5jb250ZW50X2JveCAudGV4dCB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAucGxheWVyU2VjdGlvbiB7XFxuICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICB9XFxuXFxuICAuc2VjdGlvbiAucGxheWVyU2VjdGlvbi5oaWRlIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zY3JvbGxUb3Age1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicm9vdFwiOiBcIkhvd1dvcmtzLXJvb3QtMk9kVHRcIixcblx0XCJtYXhDb250YWluZXJcIjogXCJIb3dXb3Jrcy1tYXhDb250YWluZXItM2FwZmlcIixcblx0XCJoZWFkZXJDb250YWluZXJcIjogXCJIb3dXb3Jrcy1oZWFkZXJDb250YWluZXItMmFuVmFcIixcblx0XCJ0cmlhbmdsZVwiOiBcIkhvd1dvcmtzLXRyaWFuZ2xlLTF3V0pUXCIsXG5cdFwiY2hlbWlzdHJ5XCI6IFwiSG93V29ya3MtY2hlbWlzdHJ5LVdqOHBVXCIsXG5cdFwibWF0aHNcIjogXCJIb3dXb3Jrcy1tYXRocy1TbWFrdlwiLFxuXHRcImhlYWRlcl90aXRsZVwiOiBcIkhvd1dvcmtzLWhlYWRlcl90aXRsZS0xcGU0ZVwiLFxuXHRcImhlYWRlcl9jb250ZW50XCI6IFwiSG93V29ya3MtaGVhZGVyX2NvbnRlbnQtNlY2bU5cIixcblx0XCJzY3JvbGxUb3BcIjogXCJIb3dXb3Jrcy1zY3JvbGxUb3AtMjE5a29cIixcblx0XCJidXR0b25XcmFwcGVyXCI6IFwiSG93V29ya3MtYnV0dG9uV3JhcHBlci0xeTVqalwiLFxuXHRcInJlcXVlc3REZW1vXCI6IFwiSG93V29ya3MtcmVxdWVzdERlbW8tMm0yM1NcIixcblx0XCJ3aGF0c2FwcHdyYXBwZXJcIjogXCJIb3dXb3Jrcy13aGF0c2FwcHdyYXBwZXItMkI5QmxcIixcblx0XCJ3aGF0c2FwcFwiOiBcIkhvd1dvcmtzLXdoYXRzYXBwLTJodnhVXCIsXG5cdFwic2VjdGlvblwiOiBcIkhvd1dvcmtzLXNlY3Rpb24tM2ZhbFFcIixcblx0XCJtb2JpbGVfc2VjdGlvblwiOiBcIkhvd1dvcmtzLW1vYmlsZV9zZWN0aW9uLTJEdTVPXCIsXG5cdFwibGVmdFwiOiBcIkhvd1dvcmtzLWxlZnQtMUF1RnVcIixcblx0XCJudW1iZXJXcmFwcGVyXCI6IFwiSG93V29ya3MtbnVtYmVyV3JhcHBlci1SNHlRMVwiLFxuXHRcIm5hdmlnYXRvclwiOiBcIkhvd1dvcmtzLW5hdmlnYXRvci0xZmMwMFwiLFxuXHRcInNpZGVuYXZcIjogXCJIb3dXb3Jrcy1zaWRlbmF2LTFFSTEtXCIsXG5cdFwibmF2SXRlbVwiOiBcIkhvd1dvcmtzLW5hdkl0ZW0tekNoZGxcIixcblx0XCJhY3RpdmVcIjogXCJIb3dXb3Jrcy1hY3RpdmUtMkt4dm5cIixcblx0XCJhY3RpdmVOdW1iZXJcIjogXCJIb3dXb3Jrcy1hY3RpdmVOdW1iZXItMmM0b0xcIixcblx0XCJzY3JvbGxDb250YWluZXJcIjogXCJIb3dXb3Jrcy1zY3JvbGxDb250YWluZXItMUpKODZcIixcblx0XCJ0aXRsZVwiOiBcIkhvd1dvcmtzLXRpdGxlLTJBN2hjXCIsXG5cdFwiYXJyb3dXcmFwcGVyXCI6IFwiSG93V29ya3MtYXJyb3dXcmFwcGVyLTJhcWVYXCIsXG5cdFwic2VjdGlvbl9jb250ZW50XCI6IFwiSG93V29ya3Mtc2VjdGlvbl9jb250ZW50LVRiTXBkXCIsXG5cdFwicHJvZmlsZVwiOiBcIkhvd1dvcmtzLXByb2ZpbGUtMnM2SWVcIixcblx0XCJncmF5Qm94XCI6IFwiSG93V29ya3MtZ3JheUJveC0zeTBpZlwiLFxuXHRcImNvbnRlbnRfYm94XCI6IFwiSG93V29ya3MtY29udGVudF9ib3gtMXZRLWhcIixcblx0XCJ0ZXh0XCI6IFwiSG93V29ya3MtdGV4dC0xOFEzQVwiLFxuXHRcInBsYXllclNlY3Rpb25cIjogXCJIb3dXb3Jrcy1wbGF5ZXJTZWN0aW9uLTF2SUZpXCIsXG5cdFwiaGlkZVwiOiBcIkhvd1dvcmtzLWhpZGUtSkdRSFRcIlxufTsiLCJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExpbmsgZnJvbSAnY29tcG9uZW50cy9MaW5rL0xpbmsnO1xuaW1wb3J0IHsgSE9XX1dPUktTLCBIT1dfV09SS1NfVElUTEVTIH0gZnJvbSAnLi4vR2V0UmFua3NDb25zdGFudHMnO1xuaW1wb3J0IHMgZnJvbSAnLi9Ib3dXb3Jrcy5zY3NzJztcblxuY2xhc3MgSG93V29ya3MgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgb3Blbkxpc3Q6IFsuLi5IT1dfV09SS1NfVElUTEVTXSxcbiAgICAgIGlzTW9iaWxlOiBmYWxzZSxcbiAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgIH07XG4gIH1cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5oYW5kbGVSZXNpemUoKTtcbiAgICB0aGlzLm9ic2VydmVFbGVsZW1lbnRzKCk7XG4gICAgdGhpcy5oYW5kbGVTY3JvbGwoKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5oYW5kbGVSZXNpemUpO1xuICB9XG5cbiAgaGFuZGxlU2Nyb2xsID0gKCkgPT4ge1xuICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+IDUwMCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IHRydWUsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZVNjcm9sbFRvcCA9ICgpID0+IHtcbiAgICB3aW5kb3cuc2Nyb2xsVG8oe1xuICAgICAgdG9wOiAwLFxuICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnLFxuICAgIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd1Njcm9sbDogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlUmVzaXplID0gKCkgPT4ge1xuICAgIGxldCB7IGlzTW9iaWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDk5MCkge1xuICAgICAgaXNNb2JpbGUgPSBmYWxzZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaXNNb2JpbGUgPSB0cnVlO1xuICAgIH1cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGlzTW9iaWxlLFxuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZVN0ZXBUb2dnbGUgPSAodGl0bGUsIHN0YXR1cykgPT4ge1xuICAgIGNvbnN0IHsgb3Blbkxpc3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKHN0YXR1cykge1xuICAgICAgY29uc3QgaW5kID0gb3Blbkxpc3QuZmluZEluZGV4KGVsZSA9PiBlbGUgPT09IHRpdGxlKTtcbiAgICAgIGlmIChpbmQgPiAtMSkge1xuICAgICAgICBvcGVuTGlzdC5zcGxpY2UoaW5kLCAxKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBvcGVuTGlzdCxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBvcGVuTGlzdC5wdXNoKHRpdGxlKTtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBvcGVuTGlzdCxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBvYnNlcnZlRWxlbGVtZW50cyA9ICgpID0+IHtcbiAgICBjb25zdCBvYnNlcnZlciA9IG5ldyBJbnRlcnNlY3Rpb25PYnNlcnZlcihcbiAgICAgIGVudHJpZXMgPT4ge1xuICAgICAgICBlbnRyaWVzLmZvckVhY2goZW50cnkgPT4ge1xuICAgICAgICAgIGNvbnN0IG5vZGVJZCA9IGVudHJ5LnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2lkJyk7XG4gICAgICAgICAgY29uc3QgbGluayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXG4gICAgICAgICAgICBgZGl2LiR7cy5zaWRlbmF2fSBhW2hyZWY9XCIjJHtub2RlSWR9XCJdYCxcbiAgICAgICAgICApO1xuICAgICAgICAgIGlmIChlbnRyeS5pc0ludGVyc2VjdGluZykge1xuICAgICAgICAgICAgbGluay5jbGFzc0xpc3QuYWRkKHMuYWN0aXZlKTtcbiAgICAgICAgICAgIGxpbmsuY2hpbGROb2Rlc1swXS5jbGFzc0xpc3QuYWRkKHMuYWN0aXZlTnVtYmVyKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGluay5jbGFzc0xpc3QucmVtb3ZlKHMuYWN0aXZlKTtcbiAgICAgICAgICAgIGxpbmsuY2hpbGROb2Rlc1swXS5jbGFzc0xpc3QucmVtb3ZlKHMuYWN0aXZlTnVtYmVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGhyZXNob2xkOiAwLjU1LFxuICAgICAgfSxcbiAgICApO1xuICAgIGNvbnN0IGVsZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGBsaXN0YCkucXVlcnlTZWxlY3RvckFsbChgZGl2W2lkXWApO1xuICAgIGVsZS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgb2JzZXJ2ZXIub2JzZXJ2ZShlbGVtZW50KTtcbiAgICB9KTtcbiAgfTtcblxuICBkaXNwbGF5SGVhZGVyID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlckNvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tYXhDb250YWluZXJ9PlxuICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuaGVhZGVyX3RpdGxlfT5Ib3cgZWduaWZ5IHdvcmtzPC9zcGFuPlxuICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuaGVhZGVyX2NvbnRlbnR9PlxuICAgICAgICAgIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuTG9yZW0gaXBzdW1cbiAgICAgICAgICBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LlxuICAgICAgICA8L3NwYW4+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJ1dHRvbldyYXBwZXJ9PlxuICAgICAgICAgIDxMaW5rIHRvPVwiL3JlcXVlc3QtZGVtb1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVxdWVzdERlbW99IHJvbGU9XCJwcmVzZW50YXRpb25cIj5cbiAgICAgICAgICAgICAgU1RBUlQgRlJFRSBUUklBTFxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDxhXG4gICAgICAgICAgICBjbGFzc05hbWU9e3Mud2hhdHNhcHB3cmFwcGVyfVxuICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vYXBpLndoYXRzYXBwLmNvbS9zZW5kP3Bob25lPTkxOTk0OTUyMzg0OSZ0ZXh0PUhpIEdldFJhbmtzLCBJIHdvdWxkIGxpa2UgdG8ga25vdyBtb3JlIGFib3V0IHlvdXIgT25saW5lIFBsYXRmb3JtLlwiXG4gICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mud2hhdHNhcHB9PkNoYXQgb248L2Rpdj5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXRzYXBwX2xvZ28uc3ZnXCIgYWx0PVwid2hhdHNhcHBcIiAvPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLnRyaWFuZ2xlfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvdHJpYW5nbGUuc3ZnXCJcbiAgICAgICAgYWx0PVwidHJpYW5nbGVcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLmNoZW1pc3RyeX1cbiAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9zdWJqZWN0X2ljb25zL2NoZW1pc3RyeS5zdmdcIlxuICAgICAgICBhbHQ9XCJ0cmlhbmdsZVwiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MubWF0aHN9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy9zY2FsZS5zdmdcIlxuICAgICAgICBhbHQ9XCJtYXRoc1wiXG4gICAgICAvPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlTdGVwcyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5uYXZpZ2F0b3J9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWF4Q29udGFpbmVyfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2lkZW5hdn0+XG4gICAgICAgICAge0hPV19XT1JLUy5tYXAoKGl0ZW0sIGluZGV4KSA9PiAoXG4gICAgICAgICAgICA8YSBocmVmPXtgIyR7aXRlbS50aXRsZX1gfSBjbGFzc05hbWU9e3MubmF2SXRlbX0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm51bWJlcldyYXBwZXJ9PlxuICAgICAgICAgICAgICAgIDxzcGFuPntpbmRleCArIDF9PC9zcGFuPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPHNwYW4+e2l0ZW0udGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBpZD1cImxpc3RcIiBjbGFzc05hbWU9e3Muc2Nyb2xsQ29udGFpbmVyfT5cbiAgICAgICAgICB7SE9XX1dPUktTLm1hcCgoYm94LCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvcGVuTGlzdCwgaXNNb2JpbGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCBpc09wZW4gPSBvcGVuTGlzdC5pbmNsdWRlcyhib3gudGl0bGUpO1xuICAgICAgICAgICAgY29uc3QgY29udGVudENsYXNzID0gaXNPcGVuXG4gICAgICAgICAgICAgID8gYCR7cy5zZWN0aW9uX2NvbnRlbnR9YFxuICAgICAgICAgICAgICA6IGAke3Muc2VjdGlvbl9jb250ZW50fSAke3MuaGlkZX1gO1xuICAgICAgICAgICAgY29uc3QgcGxheWVyQ2xhc3MgPSBpc09wZW5cbiAgICAgICAgICAgICAgPyBgJHtzLnBsYXllclNlY3Rpb259YFxuICAgICAgICAgICAgICA6IGAke3MucGxheWVyU2VjdGlvbn0gJHtzLmhpZGV9YDtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgIDxkaXYgaWQ9e2JveC50aXRsZX0gY2xhc3NOYW1lPXtzLnNlY3Rpb259PlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5tb2JpbGVfc2VjdGlvbn1cbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaXNNb2JpbGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZVN0ZXBUb2dnbGUoYm94LnRpdGxlLCBpc09wZW4pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlZnR9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5udW1iZXJXcmFwcGVyfT5cbiAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57aW5kZXggKyAxfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50aXRsZX0+e2JveC50aXRsZX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFycm93V3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICAgICAgaXNPcGVuXG4gICAgICAgICAgICAgICAgICAgICAgICAgID8gYC9pbWFnZXMvaWNvbnMvY2hldnJvbi11cC5zdmdgXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogYC9pbWFnZXMvaWNvbnMvY2hldnJvbi1kb3duLnN2Z2BcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiYXJyb3dcIlxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NvbnRlbnRDbGFzc30+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcm9maWxlfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9e3MuZ3JheUJveH0gc3JjPXtib3guaW1nfSBhbHQ9e2JveC50aXRsZX0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF9ib3h9PlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGl0bGV9Pntib3gudGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dH0+e2JveC5jb250ZW50fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtwbGF5ZXJDbGFzc30gLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApO1xuICAgICAgICAgIH0pfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlTY3JvbGxUb1RvcCA9ICgpID0+IHtcbiAgICBjb25zdCB7IHNob3dTY3JvbGwgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIHNob3dTY3JvbGwgJiYgKFxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLnNjcm9sbFRvcH1cbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbFRvcCgpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS9zY3JvbGxUb3Auc3ZnXCIgYWx0PVwic2Nyb2xsVG9wXCIgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgaWQ9XCJyb290XCIgY2xhc3NOYW1lPXtzLnJvb3R9PlxuICAgICAgICB7dGhpcy5kaXNwbGF5SGVhZGVyKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlTdGVwcygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U2Nyb2xsVG9Ub3AoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoSG93V29ya3MpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Ib3dXb3Jrcy5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0hvd1dvcmtzLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9Ib3dXb3Jrcy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgSG93V29ya3MgZnJvbSAnLi9Ib3dXb3Jrcyc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0VnbmlmeSBtYWtlcyB0ZWFjaGluZyBlYXN5JyxcbiAgICBjaHVua3M6IFsnSG93V29ya3MnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxIb3dXb3JrcyAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQXNCQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBaENBO0FBa0NBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBMUNBO0FBMkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBdERBO0FBdURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUF4RUE7QUEwRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuR0E7QUFvR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFuQ0E7QUFDQTtBQXJHQTtBQStJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUdBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBS0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQS9EQTtBQUNBO0FBaEpBO0FBb05BO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFsT0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBO0FBT0E7QUFDQTs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFrTkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7OztBQTlPQTtBQUNBO0FBK09BOzs7Ozs7O0FDdFBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBWUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==