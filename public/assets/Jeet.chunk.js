(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Jeet"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/jeet/Jeet.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Jeet-root-2mgrQ {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Jeet-intro-3npEu {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Jeet-introLeftPane-35bDx {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Jeet-jeet-39Jtx {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Jeet-coachingInstitutes-1JOdO {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Jeet-introContent-Rjwmh {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Jeet-introRightPane-2PY7z {\n  padding-top: 5% !important;\n}\n\n.Jeet-introRightPane-2PY7z img {\n    width: 100%;\n  }\n\n.Jeet-content-3Aeuf {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Jeet-contentImageDiv-1Ghrs {\n  height: 136px;\n  width: 136px;\n}\n\n.Jeet-screenImage-1lKDc {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Jeet-headingContent-2nvVl {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Jeet-mainContent-1aDM3 {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Jeet-testimonialsTitle-3TLf_ {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Jeet-testimonialsTitle-3TLf_::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Jeet-contentSubdiv-14PcU {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Jeet-jeeDetailsContainer-2bXgG {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.Jeet-jeeDetailsContainer-2bXgG .Jeet-subSection-3d8aX {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.Jeet-jeeTitle-12EqG {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.Jeet-jeeDescription-CN83U {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.Jeet-title2-1XLYU {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.Jeet-underline-2e2oc {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .Jeet-root-2mgrQ {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Jeet-contentForSmall-OkyDd {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Jeet-contentImageDivSmall-59O9V {\n    width: 100%;\n  }\n\n    .Jeet-contentImageDivSmall-59O9V img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Jeet-screenImageSmall-3SS8g {\n    text-align: center;\n  }\n\n  .Jeet-headingContentSmall-3oslf {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Jeet-mainContentSmall-F5bvm {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .Jeet-testimonialsTitle-3TLf_ {\n    margin-bottom: 18%;\n  }\n\n  .Jeet-testimonialsTitle-3TLf_::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Jeet-introLeftPane-35bDx {\n    width: 100%;\n  }\n\n  .Jeet-jeet-39Jtx {\n    text-align: center;\n  }\n\n  .Jeet-coachingInstitutes-1JOdO {\n    text-align: center;\n  }\n\n  .Jeet-introContent-Rjwmh {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Jeet-link-CvyVA {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .Jeet-introContent-Rjwmh {\n    width: 300px;\n  }\n\n  .Jeet-jeeDetailsContainer-2bXgG {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .Jeet-jeeDetailsContainer-2bXgG .Jeet-subSection-3d8aX {\n      width: 100%;\n    }\n\n  .Jeet-jeeTitle-12EqG {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .Jeet-jeeDescription-CN83U {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Jeet-title2-1XLYU {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Jeet-underline-2e2oc {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Jeet-introContent-Rjwmh {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .Jeet-jeeTitle-12EqG {\n    font-size: 30px;\n  }\n\n  .Jeet-jeeDescription-CN83U {\n    font-size: 18px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/Jeet.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,6BAA6B;EAC7B,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,0BAA0B;CAC3B;;AAED;IACI,WAAW;IACX,4BAA4B;IAC5B,aAAa;IACb,qBAAqB;GACtB;;AAEH;EACE,gBAAgB;EAChB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,UAAU;EACV,0BAA0B;CAC3B;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;CACF;;AAED;EACE;IACE,aAAa;GACd;;EAED;IACE,aAAa;IACb,cAAc;GACf;;IAEC;MACE,YAAY;KACb;;EAEH;IACE,8BAA8B;IAC9B,gBAAgB;GACjB;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"Jeet.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.jeeDetailsContainer {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.jeeDetailsContainer .subSection {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.jeeTitle {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.jeeDescription {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.title2 {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.underline {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .introContent {\n    width: 300px;\n  }\n\n  .jeeDetailsContainer {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .jeeDetailsContainer .subSection {\n      width: 100%;\n    }\n\n  .jeeTitle {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .jeeTitle {\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    font-size: 18px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Jeet-root-2mgrQ",
	"intro": "Jeet-intro-3npEu",
	"introLeftPane": "Jeet-introLeftPane-35bDx",
	"jeet": "Jeet-jeet-39Jtx",
	"coachingInstitutes": "Jeet-coachingInstitutes-1JOdO",
	"introContent": "Jeet-introContent-Rjwmh",
	"introRightPane": "Jeet-introRightPane-2PY7z",
	"content": "Jeet-content-3Aeuf",
	"contentImageDiv": "Jeet-contentImageDiv-1Ghrs",
	"screenImage": "Jeet-screenImage-1lKDc",
	"headingContent": "Jeet-headingContent-2nvVl",
	"mainContent": "Jeet-mainContent-1aDM3",
	"testimonialsTitle": "Jeet-testimonialsTitle-3TLf_",
	"contentSubdiv": "Jeet-contentSubdiv-14PcU",
	"jeeDetailsContainer": "Jeet-jeeDetailsContainer-2bXgG",
	"subSection": "Jeet-subSection-3d8aX",
	"jeeTitle": "Jeet-jeeTitle-12EqG",
	"jeeDescription": "Jeet-jeeDescription-CN83U",
	"title2": "Jeet-title2-1XLYU",
	"underline": "Jeet-underline-2e2oc",
	"contentForSmall": "Jeet-contentForSmall-OkyDd",
	"contentImageDivSmall": "Jeet-contentImageDivSmall-59O9V",
	"screenImageSmall": "Jeet-screenImageSmall-3SS8g",
	"headingContentSmall": "Jeet-headingContentSmall-3oslf",
	"mainContentSmall": "Jeet-mainContentSmall-F5bvm",
	"link": "Jeet-link-CvyVA"
};

/***/ }),

/***/ "./src/routes/products/jeet/Jeet.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Jeet_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/jeet/Jeet.scss");
/* harmony import */ var _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/Jeet.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







var STATS = [{
  title: '80k+',
  description: 'Students attempted jee mains in 2018'
}, {
  title: '17.5%',
  description: 'cut-off clear rate'
}, {
  title: '13k',
  description: 'Students qualified jee mains in 2018'
}, {
  title: '40%',
  description: 'Students of ap and telangana'
}];
var CONTENT = [{
  headerContent: 'Understand where do your students stand',
  mainContent: 'Marks Analysis helps you understand how your students are performing in a snapshot and identify who needs help to clear the cut-off.',
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/jeet/screen1.png'
}, {
  headerContent: 'Identify student’s strengths & areas of improvement',
  mainContent: 'Concept-based Profile helps your student identify his/her strong & weak topics and forms a basis for his/her areas of focus to improve his performance.',
  icon: '/images/products/jeet/blub.svg',
  screenImage: '/images/products/jeet/screen2.png'
}, {
  headerContent: 'Compare performance data across your institute hierarchy',
  mainContent: 'Comparison Analysis helps you compare students’ performance between states, campuses, batches, sections, tests & take apt actions on red areas.',
  icon: '/images/products/jeet/weights.svg',
  screenImage: '/images/products/jeet/screen3.png'
}, {
  headerContent: 'Channelize performance data using relevant filters',
  mainContent: 'Contextualize your students’ performance data using pre-set filters based on your institute’s hierarchy and look at analysis filtered to the selection.',
  icon: '/images/products/jeet/group.svg',
  screenImage: '/images/products/jeet/screen4.png'
}, {
  headerContent: 'Downloadable reports to understand students’ performance',
  mainContent: 'Extensive excel reports of students’ performance data which can be sorted or filtered in the way you want to look at them.',
  icon: '/images/products/jeet/download.svg',
  screenImage: '/images/products/jeet/screen5.png'
}, {
  headerContent: 'Identify the questions most of your students have gone wrong',
  mainContent: 'Error Analysis helps you determine the questions in a test which majority of your students have not attempted or attempted wrongly to enable error practice.',
  icon: '/images/products/jeet/idle.svg',
  screenImage: '/images/products/jeet/screen6.png'
}];
var SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

var Jeet =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Jeet, _React$Component);

  function Jeet() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Jeet);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Jeet)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "makeAlignment", function (contentIndex, key) {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });

    return _this;
  }

  _createClass(Jeet, [{
    key: "componentDidMount",
    // static propTypes = {
    //
    // };
    value: function componentDidMount() {
      react_ga__WEBPACK_IMPORTED_MODULE_3__["default"].initialize(window.App.googleTrackingId, {
        debug: false
      });
      react_ga__WEBPACK_IMPORTED_MODULE_3__["default"].pageview(window.location.href);
    }
    /**
     * @description Returns what alignment should the content take eg. flex-start flex end center
     * @param contentIndex,key
     * @author Sushruth
     * */

  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane, " col m4 s12"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, "GetRanks"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, "For Coaching Institues"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, "GetRanks is a learning Analytics ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }), "platform which empowers", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }), "coaching institutes to", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }), "increase number of their", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }), "students clearing cut-off in", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }), "competitive exams."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/request-demo",
        className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane, " col m8 s12"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/products/jeet/JEET.png",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDetailsContainer, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.title2),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }, "Our Stats", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.underline),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      })), STATS.map(function (details, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subSection),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 154
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeTitle),
          style: {
            borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 155
          },
          __self: this
        }, details.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDescription),
          style: {
            borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 166
          },
          __self: this
        }, details.description.toUpperCase()));
      })), CONTENT.map(function (content, contentIndex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "row",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 181
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content, " row hide-on-xs"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 182
          },
          __self: this
        }, [0, 1].map(function (key) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: contentIndex % 2 === key ? "col m5 l5 ".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv) : "col m7 l7 ".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv),
            style: {
              justifyContent: _this2.makeAlignment(contentIndex, key)
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 184
            },
            __self: this
          }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 195
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 196
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.icon,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 197
            },
            __self: this
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 199
            },
            __self: this
          }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 202
            },
            __self: this
          }, content.mainContent)) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 205
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.screenImage,
            width: "87.6%",
            height: "54.45%",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 206
            },
            __self: this
          })));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall, " row hide-on-m-and-up"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 217
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 218
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 219
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 221
          },
          __self: this
        }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 224
          },
          __self: this
        }, content.mainContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 225
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.screenImage,
          width: "87.6%",
          height: "54.45%",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 226
          },
          __self: this
        }))));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        style: {
          paddingTop: '64px',
          marginBottom: '5%'
        },
        className: "row hide",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }, "Why our clients love Egnify"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_4__["default"], {
        data: SLIDESHOW,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 241
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }));
      return view;
    }
  }]);

  return Jeet;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Jeet_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Jeet));

/***/ }),

/***/ "./src/routes/products/jeet/Jeet.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/jeet/Jeet.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/jeet/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Jeet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/jeet/Jeet.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/jeet/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify',
              chunks: ['Jeet'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Jeet__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSmVldC5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvamVldC9KZWV0LnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9qZWV0L0plZXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9qZWV0L0plZXQuc2Nzcz83MmE0IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvamVldC9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLkplZXQtcm9vdC0ybWdyUSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5KZWV0LWludHJvLTNucEV1IHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiAzJTtcXG4gIG1hcmdpbi1ib3R0b206IDUlO1xcbn1cXG5cXG4uSmVldC1pbnRyb0xlZnRQYW5lLTM1YkR4IHtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgcGFkZGluZy10b3A6IDUlICFpbXBvcnRhbnQ7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5KZWV0LWplZXQtMzlKdHgge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLkplZXQtY29hY2hpbmdJbnN0aXR1dGVzLTFKT2RPIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjhweDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5KZWV0LWludHJvQ29udGVudC1SandtaCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICB3aWR0aDogNDIwcHg7XFxufVxcblxcbi5KZWV0LWludHJvUmlnaHRQYW5lLTJQWTd6IHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uSmVldC1pbnRyb1JpZ2h0UGFuZS0yUFk3eiBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4uSmVldC1jb250ZW50LTNBZXVmIHtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbn1cXG5cXG4uSmVldC1jb250ZW50SW1hZ2VEaXYtMUdocnMge1xcbiAgaGVpZ2h0OiAxMzZweDtcXG4gIHdpZHRoOiAxMzZweDtcXG59XFxuXFxuLkplZXQtc2NyZWVuSW1hZ2UtMWxLRGMge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy10b3A6IDY4cHg7XFxufVxcblxcbi5KZWV0LWhlYWRpbmdDb250ZW50LTJudlZsIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xcbn1cXG5cXG4uSmVldC1tYWluQ29udGVudC0xYURNMyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLkplZXQtdGVzdGltb25pYWxzVGl0bGUtM1RMZl8ge1xcbiAgaGVpZ2h0OiAzOHB4O1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxuICBwYWRkaW5nOiAyJTtcXG59XFxuXFxuLkplZXQtdGVzdGltb25pYWxzVGl0bGUtM1RMZl86OmFmdGVyIHtcXG4gIGNvbnRlbnQ6ICcnO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiAycHg7XFxuICBib3JkZXItdG9wOiBzb2xpZCAzcHggI2VjNGM2ZjtcXG4gIHBhZGRpbmctdG9wOiAxMnB4O1xcbn1cXG5cXG4uSmVldC1jb250ZW50U3ViZGl2LTE0UGNVIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLkplZXQtamVlRGV0YWlsc0NvbnRhaW5lci0yYlhnRyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiAzMDBweCAhaW1wb3J0YW50O1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1ib3R0b206IDUlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNlM2U1ZjtcXG59XFxuXFxuLkplZXQtamVlRGV0YWlsc0NvbnRhaW5lci0yYlhnRyAuSmVldC1zdWJTZWN0aW9uLTNkOGFYIHtcXG4gICAgd2lkdGg6IDI1JTtcXG4gICAgbWluLWhlaWdodDogODBweCAhaW1wb3J0YW50O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ncmlkO1xcbiAgfVxcblxcbi5KZWV0LWplZVRpdGxlLTEyRXFHIHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZhYjAwO1xcbn1cXG5cXG4uSmVldC1qZWVEZXNjcmlwdGlvbi1DTjgzVSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcGFkZGluZzogNSU7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGhlaWdodDogNzBweDtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uSmVldC10aXRsZTItMVhMWVUge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMyU7XFxufVxcblxcbi5KZWV0LXVuZGVybGluZS0yZTJvYyB7XFxuICBtYXJnaW4tdG9wOiA4cHg7XFxuICBtYXJnaW4tbGVmdDogNDglO1xcbiAgd2lkdGg6IDQlO1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2VjNGM2ZjtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NjBweCkge1xcbiAgLkplZXQtcm9vdC0ybWdyUSB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4gIC5KZWV0LWNvbnRlbnRGb3JTbWFsbC1Pa3lEZCB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5KZWV0LWNvbnRlbnRJbWFnZURpdlNtYWxsLTU5TzlWIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAgIC5KZWV0LWNvbnRlbnRJbWFnZURpdlNtYWxsLTU5TzlWIGltZyB7XFxuICAgICAgaGVpZ2h0OiAxMDBweDtcXG4gICAgICB3aWR0aDogMTAwcHg7XFxuICAgIH1cXG5cXG4gIC5KZWV0LXNjcmVlbkltYWdlU21hbGwtM1NTOGcge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuSmVldC1oZWFkaW5nQ29udGVudFNtYWxsLTNvc2xmIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAuSmVldC1tYWluQ29udGVudFNtYWxsLUY1YnZtIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS42O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XFxuICB9XFxuXFxuICAuSmVldC10ZXN0aW1vbmlhbHNUaXRsZS0zVExmXyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC5KZWV0LXRlc3RpbW9uaWFsc1RpdGxlLTNUTGZfOjphZnRlciB7XFxuICAgIG1hcmdpbi10b3A6IDRweDtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XFxuICAuSmVldC1pbnRyb0xlZnRQYW5lLTM1YkR4IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuSmVldC1qZWV0LTM5SnR4IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkplZXQtY29hY2hpbmdJbnN0aXR1dGVzLTFKT2RPIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLkplZXQtaW50cm9Db250ZW50LVJqd21oIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmctbGVmdDogMTUlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNSU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIGJ1dHRvbiB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gIH1cXG5cXG4gIC5KZWV0LWxpbmstQ3Z5VkEge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XFxuICAuSmVldC1pbnRyb0NvbnRlbnQtUmp3bWgge1xcbiAgICB3aWR0aDogMzAwcHg7XFxuICB9XFxuXFxuICAuSmVldC1qZWVEZXRhaWxzQ29udGFpbmVyLTJiWGdHIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfVxcblxcbiAgICAuSmVldC1qZWVEZXRhaWxzQ29udGFpbmVyLTJiWGdHIC5KZWV0LXN1YlNlY3Rpb24tM2Q4YVgge1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICB9XFxuXFxuICAuSmVldC1qZWVUaXRsZS0xMkVxRyB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuSmVldC1qZWVEZXNjcmlwdGlvbi1DTjgzVSB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xcbiAgICBwYWRkaW5nLWxlZnQ6IDIwJTtcXG4gICAgcGFkZGluZy1yaWdodDogMjAlO1xcbiAgfVxcblxcbiAgLkplZXQtdGl0bGUyLTFYTFlVIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XFxuICB9XFxuXFxuICAuSmVldC11bmRlcmxpbmUtMmUyb2Mge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XFxuICAuSmVldC1pbnRyb0NvbnRlbnQtUmp3bWgge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICB9XFxuXFxuICAuSmVldC1qZWVUaXRsZS0xMkVxRyB7XFxuICAgIGZvbnQtc2l6ZTogMzBweDtcXG4gIH1cXG5cXG4gIC5KZWV0LWplZURlc2NyaXB0aW9uLUNOODNVIHtcXG4gICAgZm9udC1zaXplOiAxOHB4O1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvamVldC9KZWV0LnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWiw0REFBNEQ7VUFDcEQsb0RBQW9EO0NBQzdEOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsZUFBZTtFQUNmLDJCQUEyQjtFQUMzQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7SUFDSSxZQUFZO0dBQ2I7O0FBRUg7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixlQUFlO0VBQ2YsWUFBWTtFQUNaLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYiw2QkFBNkI7RUFDN0Isa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsMEJBQTBCO0NBQzNCOztBQUVEO0lBQ0ksV0FBVztJQUNYLDRCQUE0QjtJQUM1QixhQUFhO0lBQ2IscUJBQXFCO0dBQ3RCOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFO0lBQ0UsV0FBVztJQUNYLFVBQVU7R0FDWDs7RUFFRDtJQUNFLGFBQWE7SUFDYixZQUFZO0lBQ1osbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztJQUVDO01BQ0UsY0FBYztNQUNkLGFBQWE7S0FDZDs7RUFFSDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3QjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxhQUFhO0lBQ2IsY0FBYztHQUNmOztJQUVDO01BQ0UsWUFBWTtLQUNiOztFQUVIO0lBQ0UsOEJBQThCO0lBQzlCLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLGlCQUFpQjtJQUNqQixXQUFXO0dBQ1o7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCO0NBQ0ZcIixcImZpbGVcIjpcIkplZXQuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucm9vdCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5pbnRybyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogMyU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTtcXG59XFxuXFxuLmludHJvTGVmdFBhbmUge1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLmplZXQge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uaW50cm9Db250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcXG4gIHdpZHRoOiA0MjBweDtcXG59XFxuXFxuLmludHJvUmlnaHRQYW5lIHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uaW50cm9SaWdodFBhbmUgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuLmNvbnRlbnQge1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxufVxcblxcbi5jb250ZW50SW1hZ2VEaXYge1xcbiAgaGVpZ2h0OiAxMzZweDtcXG4gIHdpZHRoOiAxMzZweDtcXG59XFxuXFxuLnNjcmVlbkltYWdlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctdG9wOiA2OHB4O1xcbn1cXG5cXG4uaGVhZGluZ0NvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5tYWluQ29udGVudCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLnRlc3RpbW9uaWFsc1RpdGxlIHtcXG4gIGhlaWdodDogMzhweDtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbiAgcGFkZGluZzogMiU7XFxufVxcblxcbi50ZXN0aW1vbmlhbHNUaXRsZTo6YWZ0ZXIge1xcbiAgY29udGVudDogJyc7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgd2lkdGg6IDQ4cHg7XFxuICBoZWlnaHQ6IDJweDtcXG4gIGJvcmRlci10b3A6IHNvbGlkIDNweCAjZWM0YzZmO1xcbiAgcGFkZGluZy10b3A6IDEycHg7XFxufVxcblxcbi5jb250ZW50U3ViZGl2IHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmplZURldGFpbHNDb250YWluZXIge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWluLWhlaWdodDogMzAwcHggIWltcG9ydGFudDtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA1JTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZTNlNWY7XFxufVxcblxcbi5qZWVEZXRhaWxzQ29udGFpbmVyIC5zdWJTZWN0aW9uIHtcXG4gICAgd2lkdGg6IDI1JTtcXG4gICAgbWluLWhlaWdodDogODBweCAhaW1wb3J0YW50O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ncmlkO1xcbiAgfVxcblxcbi5qZWVUaXRsZSB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmYWIwMDtcXG59XFxuXFxuLmplZURlc2NyaXB0aW9uIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA1JTtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgaGVpZ2h0OiA3MHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi50aXRsZTIge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMyU7XFxufVxcblxcbi51bmRlcmxpbmUge1xcbiAgbWFyZ2luLXRvcDogOHB4O1xcbiAgbWFyZ2luLWxlZnQ6IDQ4JTtcXG4gIHdpZHRoOiA0JTtcXG4gIGJvcmRlcjogc29saWQgMXB4ICNlYzRjNmY7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5yb290IHtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmNvbnRlbnRGb3JTbWFsbCB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jb250ZW50SW1hZ2VEaXZTbWFsbCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgICAuY29udGVudEltYWdlRGl2U21hbGwgaW1nIHtcXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xcbiAgICAgIHdpZHRoOiAxMDBweDtcXG4gICAgfVxcblxcbiAgLnNjcmVlbkltYWdlU21hbGwge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuaGVhZGluZ0NvbnRlbnRTbWFsbCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLm1haW5Db250ZW50U21hbGwge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZTo6YWZ0ZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0cHg7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLmludHJvTGVmdFBhbmUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5qZWV0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgfVxcblxcbiAgLmxpbmsge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDMwMHB4O1xcbiAgfVxcblxcbiAgLmplZURldGFpbHNDb250YWluZXIge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1hcmdpbi10b3A6IDA7XFxuICB9XFxuXFxuICAgIC5qZWVEZXRhaWxzQ29udGFpbmVyIC5zdWJTZWN0aW9uIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbiAgLmplZVRpdGxlIHtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIGZvbnQtc2l6ZTogMzBweDtcXG4gIH1cXG5cXG4gIC5qZWVEZXNjcmlwdGlvbiB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xcbiAgICBwYWRkaW5nLWxlZnQ6IDIwJTtcXG4gICAgcGFkZGluZy1yaWdodDogMjAlO1xcbiAgfVxcblxcbiAgLnRpdGxlMiB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgcGFkZGluZy10b3A6IDQwcHg7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLnVuZGVybGluZSB7XFxuICAgIG1hcmdpbi1sZWZ0OiA0MyU7XFxuICAgIHdpZHRoOiAxNSU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICB9XFxuXFxuICAuamVlVGl0bGUge1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuamVlRGVzY3JpcHRpb24ge1xcbiAgICBmb250LXNpemU6IDE4cHg7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJyb290XCI6IFwiSmVldC1yb290LTJtZ3JRXCIsXG5cdFwiaW50cm9cIjogXCJKZWV0LWludHJvLTNucEV1XCIsXG5cdFwiaW50cm9MZWZ0UGFuZVwiOiBcIkplZXQtaW50cm9MZWZ0UGFuZS0zNWJEeFwiLFxuXHRcImplZXRcIjogXCJKZWV0LWplZXQtMzlKdHhcIixcblx0XCJjb2FjaGluZ0luc3RpdHV0ZXNcIjogXCJKZWV0LWNvYWNoaW5nSW5zdGl0dXRlcy0xSk9kT1wiLFxuXHRcImludHJvQ29udGVudFwiOiBcIkplZXQtaW50cm9Db250ZW50LVJqd21oXCIsXG5cdFwiaW50cm9SaWdodFBhbmVcIjogXCJKZWV0LWludHJvUmlnaHRQYW5lLTJQWTd6XCIsXG5cdFwiY29udGVudFwiOiBcIkplZXQtY29udGVudC0zQWV1ZlwiLFxuXHRcImNvbnRlbnRJbWFnZURpdlwiOiBcIkplZXQtY29udGVudEltYWdlRGl2LTFHaHJzXCIsXG5cdFwic2NyZWVuSW1hZ2VcIjogXCJKZWV0LXNjcmVlbkltYWdlLTFsS0RjXCIsXG5cdFwiaGVhZGluZ0NvbnRlbnRcIjogXCJKZWV0LWhlYWRpbmdDb250ZW50LTJudlZsXCIsXG5cdFwibWFpbkNvbnRlbnRcIjogXCJKZWV0LW1haW5Db250ZW50LTFhRE0zXCIsXG5cdFwidGVzdGltb25pYWxzVGl0bGVcIjogXCJKZWV0LXRlc3RpbW9uaWFsc1RpdGxlLTNUTGZfXCIsXG5cdFwiY29udGVudFN1YmRpdlwiOiBcIkplZXQtY29udGVudFN1YmRpdi0xNFBjVVwiLFxuXHRcImplZURldGFpbHNDb250YWluZXJcIjogXCJKZWV0LWplZURldGFpbHNDb250YWluZXItMmJYZ0dcIixcblx0XCJzdWJTZWN0aW9uXCI6IFwiSmVldC1zdWJTZWN0aW9uLTNkOGFYXCIsXG5cdFwiamVlVGl0bGVcIjogXCJKZWV0LWplZVRpdGxlLTEyRXFHXCIsXG5cdFwiamVlRGVzY3JpcHRpb25cIjogXCJKZWV0LWplZURlc2NyaXB0aW9uLUNOODNVXCIsXG5cdFwidGl0bGUyXCI6IFwiSmVldC10aXRsZTItMVhMWVVcIixcblx0XCJ1bmRlcmxpbmVcIjogXCJKZWV0LXVuZGVybGluZS0yZTJvY1wiLFxuXHRcImNvbnRlbnRGb3JTbWFsbFwiOiBcIkplZXQtY29udGVudEZvclNtYWxsLU9reURkXCIsXG5cdFwiY29udGVudEltYWdlRGl2U21hbGxcIjogXCJKZWV0LWNvbnRlbnRJbWFnZURpdlNtYWxsLTU5TzlWXCIsXG5cdFwic2NyZWVuSW1hZ2VTbWFsbFwiOiBcIkplZXQtc2NyZWVuSW1hZ2VTbWFsbC0zU1M4Z1wiLFxuXHRcImhlYWRpbmdDb250ZW50U21hbGxcIjogXCJKZWV0LWhlYWRpbmdDb250ZW50U21hbGwtM29zbGZcIixcblx0XCJtYWluQ29udGVudFNtYWxsXCI6IFwiSmVldC1tYWluQ29udGVudFNtYWxsLUY1YnZtXCIsXG5cdFwibGlua1wiOiBcIkplZXQtbGluay1DdnlWQVwiXG59OyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG4vLyBpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExpbmsgZnJvbSAnY29tcG9uZW50cy9MaW5rJztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCBTbGlkZXNob3cgZnJvbSAnY29tcG9uZW50cy9TbGlkZXNob3cnO1xuaW1wb3J0IFJlcXVlc3REZW1vIGZyb20gJ2NvbXBvbmVudHMvUmVxdWVzdERlbW8nO1xuaW1wb3J0IHMgZnJvbSAnLi9KZWV0LnNjc3MnO1xuXG5jb25zdCBTVEFUUyA9IFtcbiAgeyB0aXRsZTogJzgwaysnLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIGF0dGVtcHRlZCBqZWUgbWFpbnMgaW4gMjAxOCcgfSxcbiAgeyB0aXRsZTogJzE3LjUlJywgZGVzY3JpcHRpb246ICdjdXQtb2ZmIGNsZWFyIHJhdGUnIH0sXG4gIHsgdGl0bGU6ICcxM2snLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIHF1YWxpZmllZCBqZWUgbWFpbnMgaW4gMjAxOCcgfSxcbiAgeyB0aXRsZTogJzQwJScsIGRlc2NyaXB0aW9uOiAnU3R1ZGVudHMgb2YgYXAgYW5kIHRlbGFuZ2FuYScgfSxcbl07XG5cbmNvbnN0IENPTlRFTlQgPSBbXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnVW5kZXJzdGFuZCB3aGVyZSBkbyB5b3VyIHN0dWRlbnRzIHN0YW5kJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdNYXJrcyBBbmFseXNpcyBoZWxwcyB5b3UgdW5kZXJzdGFuZCBob3cgeW91ciBzdHVkZW50cyBhcmUgcGVyZm9ybWluZyBpbiBhIHNuYXBzaG90IGFuZCBpZGVudGlmeSB3aG8gbmVlZHMgaGVscCB0byBjbGVhciB0aGUgY3V0LW9mZi4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvcGVyY2VudC5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3NjcmVlbjEucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdJZGVudGlmeSBzdHVkZW504oCZcyBzdHJlbmd0aHMgJiBhcmVhcyBvZiBpbXByb3ZlbWVudCcsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnQ29uY2VwdC1iYXNlZCBQcm9maWxlIGhlbHBzIHlvdXIgc3R1ZGVudCBpZGVudGlmeSBoaXMvaGVyIHN0cm9uZyAmIHdlYWsgdG9waWNzIGFuZCBmb3JtcyBhIGJhc2lzIGZvciBoaXMvaGVyIGFyZWFzIG9mIGZvY3VzIHRvIGltcHJvdmUgaGlzIHBlcmZvcm1hbmNlLicsXG4gICAgaWNvbjogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9ibHViLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuMi5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0NvbXBhcmUgcGVyZm9ybWFuY2UgZGF0YSBhY3Jvc3MgeW91ciBpbnN0aXR1dGUgaGllcmFyY2h5JyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdDb21wYXJpc29uIEFuYWx5c2lzIGhlbHBzIHlvdSBjb21wYXJlIHN0dWRlbnRz4oCZIHBlcmZvcm1hbmNlIGJldHdlZW4gc3RhdGVzLCBjYW1wdXNlcywgYmF0Y2hlcywgc2VjdGlvbnMsIHRlc3RzICYgdGFrZSBhcHQgYWN0aW9ucyBvbiByZWQgYXJlYXMuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3dlaWdodHMuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW4zLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnQ2hhbm5lbGl6ZSBwZXJmb3JtYW5jZSBkYXRhIHVzaW5nIHJlbGV2YW50IGZpbHRlcnMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0NvbnRleHR1YWxpemUgeW91ciBzdHVkZW50c+KAmSBwZXJmb3JtYW5jZSBkYXRhIHVzaW5nIHByZS1zZXQgZmlsdGVycyBiYXNlZCBvbiB5b3VyIGluc3RpdHV0ZeKAmXMgaGllcmFyY2h5IGFuZCBsb29rIGF0IGFuYWx5c2lzIGZpbHRlcmVkIHRvIHRoZSBzZWxlY3Rpb24uJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2dyb3VwLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvc2NyZWVuNC5wbmcnLFxuICB9LFxuICB7XG4gICAgaGVhZGVyQ29udGVudDogJ0Rvd25sb2FkYWJsZSByZXBvcnRzIHRvIHVuZGVyc3RhbmQgc3R1ZGVudHPigJkgcGVyZm9ybWFuY2UnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ0V4dGVuc2l2ZSBleGNlbCByZXBvcnRzIG9mIHN0dWRlbnRz4oCZIHBlcmZvcm1hbmNlIGRhdGEgd2hpY2ggY2FuIGJlIHNvcnRlZCBvciBmaWx0ZXJlZCBpbiB0aGUgd2F5IHlvdSB3YW50IHRvIGxvb2sgYXQgdGhlbS4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvZG93bmxvYWQuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW41LnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OlxuICAgICAgJ0lkZW50aWZ5IHRoZSBxdWVzdGlvbnMgbW9zdCBvZiB5b3VyIHN0dWRlbnRzIGhhdmUgZ29uZSB3cm9uZycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnRXJyb3IgQW5hbHlzaXMgaGVscHMgeW91IGRldGVybWluZSB0aGUgcXVlc3Rpb25zIGluIGEgdGVzdCB3aGljaCBtYWpvcml0eSBvZiB5b3VyIHN0dWRlbnRzIGhhdmUgbm90IGF0dGVtcHRlZCBvciBhdHRlbXB0ZWQgd3JvbmdseSB0byBlbmFibGUgZXJyb3IgcHJhY3RpY2UuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2lkbGUuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW42LnBuZycsXG4gIH0sXG5dO1xuXG5jb25zdCBTTElERVNIT1cgPSBbXG4gIHtcbiAgICBpbWFnZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL3RlbGFuZ2FuYS5wbmcnLFxuICAgIG5hbWU6ICdNci4gU3Jpbml2YXMgS2FubmFuJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIFNyaW5pdmFzIE11cnRoeScsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBBbmRyZXcgVGFnJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIE11c3NvbGluaScsXG4gICAgcm9sZTogJ1RlYWNoZXIsIFNyaSBDaGFpdGFueWEnLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnSSBoYXRlIHRvIHRhbGsgYWJvdXQgdGhlIGdyYWRpbmcgd29ya2xvYWQsIGJ1dCBncmFkaW5nIHRoaXMgY2xhc3PigJlzIHVuaXQgdGVzdCDigJMganVzdCB0aGlzIG9uZSBjbGFzcyDigJMgdG9vayBtZSBhbG1vc3QgZm91ciBob3Vycy4gU28sIHRoYXTigJlzIGEgbG90IG9mIHRpbWUgb3V0c2lkZSBvZiBjbGFzcycsXG4gIH0sXG5dO1xuXG5jbGFzcyBKZWV0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgLy8gc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgLy9cbiAgLy8gfTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBSZWFjdEdBLmluaXRpYWxpemUod2luZG93LkFwcC5nb29nbGVUcmFja2luZ0lkLCB7XG4gICAgICBkZWJ1ZzogZmFsc2UsXG4gICAgfSk7XG4gICAgUmVhY3RHQS5wYWdldmlldyh3aW5kb3cubG9jYXRpb24uaHJlZik7XG4gIH1cblxuICAvKipcbiAgICogQGRlc2NyaXB0aW9uIFJldHVybnMgd2hhdCBhbGlnbm1lbnQgc2hvdWxkIHRoZSBjb250ZW50IHRha2UgZWcuIGZsZXgtc3RhcnQgZmxleCBlbmQgY2VudGVyXG4gICAqIEBwYXJhbSBjb250ZW50SW5kZXgsa2V5XG4gICAqIEBhdXRob3IgU3VzaHJ1dGhcbiAgICogKi9cbiAgbWFrZUFsaWdubWVudCA9IChjb250ZW50SW5kZXgsIGtleSkgPT4ge1xuICAgIGlmIChjb250ZW50SW5kZXggJSAyID09PSBrZXkpIHtcbiAgICAgIGlmIChrZXkgPT09IDApIHtcbiAgICAgICAgcmV0dXJuICdmbGV4LXN0YXJ0JztcbiAgICAgIH1cbiAgICAgIHJldHVybiAnZmxleC1lbmQnO1xuICAgIH1cbiAgICByZXR1cm4gJ2NlbnRlcic7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHZpZXcgPSAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb290fSByb3dgfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW50cm99IHJvd2B9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvTGVmdFBhbmV9IGNvbCBtNCBzMTJgfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmplZXR9PkdldFJhbmtzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb2FjaGluZ0luc3RpdHV0ZXN9PkZvciBDb2FjaGluZyBJbnN0aXR1ZXM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmludHJvQ29udGVudH0+XG4gICAgICAgICAgICAgIEdldFJhbmtzIGlzIGEgbGVhcm5pbmcgQW5hbHl0aWNzIDxiciAvPlxuICAgICAgICAgICAgICBwbGF0Zm9ybSB3aGljaCBlbXBvd2Vyc1xuICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgY29hY2hpbmcgaW5zdGl0dXRlcyB0b1xuICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgaW5jcmVhc2UgbnVtYmVyIG9mIHRoZWlyXG4gICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICBzdHVkZW50cyBjbGVhcmluZyBjdXQtb2ZmIGluXG4gICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICBjb21wZXRpdGl2ZSBleGFtcy5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPExpbmsgdG89XCIvcmVxdWVzdC1kZW1vXCIgY2xhc3NOYW1lPXtzLmxpbmt9PlxuICAgICAgICAgICAgICA8YnV0dG9uPlJFUVVFU1QgQSBERU1PPC9idXR0b24+XG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW50cm9SaWdodFBhbmV9IGNvbCBtOCBzMTJgfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9wcm9kdWN0cy9qZWV0L0pFRVQucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmplZURldGFpbHNDb250YWluZXJ9IHJvd2B9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnRpdGxlMn1gfT5cbiAgICAgICAgICAgIE91ciBTdGF0c1xuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MudW5kZXJsaW5lfWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAge1NUQVRTLm1hcCgoZGV0YWlscywgaW5kZXgpID0+IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnN1YlNlY3Rpb259YH0+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuamVlVGl0bGV9YH1cbiAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6XG4gICAgICAgICAgICAgICAgICAgIGluZGV4IDwgU1RBVFMubGVuZ3RoIC0gMVxuICAgICAgICAgICAgICAgICAgICAgID8gJ3NvbGlkIDFweCByZ2JhKDEzOSwgMTM5LCAyMjMsIDAuMyknXG4gICAgICAgICAgICAgICAgICAgICAgOiAnJyxcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2RldGFpbHMudGl0bGV9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmplZURlc2NyaXB0aW9ufWB9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlclJpZ2h0OlxuICAgICAgICAgICAgICAgICAgICBpbmRleCA8IFNUQVRTLmxlbmd0aCAtIDFcbiAgICAgICAgICAgICAgICAgICAgICA/ICdzb2xpZCAxcHggcmdiYSgxMzksIDEzOSwgMjIzLCAwLjMpJ1xuICAgICAgICAgICAgICAgICAgICAgIDogJycsXG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtkZXRhaWxzLmRlc2NyaXB0aW9uLnRvVXBwZXJDYXNlKCl9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7Q09OVEVOVC5tYXAoKGNvbnRlbnQsIGNvbnRlbnRJbmRleCkgPT4gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5jb250ZW50fSByb3cgaGlkZS1vbi14c2B9PlxuICAgICAgICAgICAgICB7WzAsIDFdLm1hcChrZXkgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRJbmRleCAlIDIgPT09IGtleVxuICAgICAgICAgICAgICAgICAgICAgID8gYGNvbCBtNSBsNSAke3MuY29udGVudFN1YmRpdn1gXG4gICAgICAgICAgICAgICAgICAgICAgOiBgY29sIG03IGw3ICR7cy5jb250ZW50U3ViZGl2fWBcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiB0aGlzLm1ha2VBbGlnbm1lbnQoY29udGVudEluZGV4LCBrZXkpLFxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICB7Y29udGVudEluZGV4ICUgMiA9PT0ga2V5ID8gKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRJbWFnZURpdn0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17Y29udGVudC5pY29ufSAvPlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmdDb250ZW50fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtjb250ZW50LmhlYWRlckNvbnRlbnR9XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbkNvbnRlbnR9Pntjb250ZW50Lm1haW5Db250ZW50fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNjcmVlbkltYWdlfT5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3JjPXtjb250ZW50LnNjcmVlbkltYWdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCI4Ny42JVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI1NC40NSVcIlxuICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmNvbnRlbnRGb3JTbWFsbH0gcm93IGhpZGUtb24tbS1hbmQtdXBgfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudEltYWdlRGl2U21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50Lmljb259IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nQ29udGVudFNtYWxsfT5cbiAgICAgICAgICAgICAgICB7Y29udGVudC5oZWFkZXJDb250ZW50fVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbkNvbnRlbnRTbWFsbH0+e2NvbnRlbnQubWFpbkNvbnRlbnR9PC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNjcmVlbkltYWdlU21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICBzcmM9e2NvbnRlbnQuc2NyZWVuSW1hZ2V9XG4gICAgICAgICAgICAgICAgICB3aWR0aD1cIjg3LjYlXCJcbiAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjU0LjQ1JVwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSl9XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nVG9wOiAnNjRweCcsIG1hcmdpbkJvdHRvbTogJzUlJyB9fVxuICAgICAgICAgIGNsYXNzTmFtZT1cInJvdyBoaWRlXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RpbW9uaWFsc1RpdGxlfT5XaHkgb3VyIGNsaWVudHMgbG92ZSBFZ25pZnk8L2Rpdj5cbiAgICAgICAgICA8U2xpZGVzaG93IGRhdGE9e1NMSURFU0hPV30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxSZXF1ZXN0RGVtbyAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgICByZXR1cm4gdmlldztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEplZXQpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9KZWV0LnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSmVldC5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vSmVldC5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgSmVldCBmcm9tICcuL0plZXQnO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0VnbmlmeScsXG4gICAgY2h1bmtzOiBbJ0plZXQnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQ+XG4gICAgICAgIDxKZWV0IC8+XG4gICAgICA8L0xheW91dD5cbiAgICApLFxuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBhY3Rpb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFFQTtBQUVBO0FBQ0E7QUFOQTtBQVVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBUUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7Ozs7OztBQXpCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFlQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWJBO0FBMkJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFLQTtBQUNBO0FBREE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXZCQTtBQWtDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBOUNBO0FBeURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTs7OztBQXpKQTtBQUNBO0FBMkpBOzs7Ozs7O0FDelBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBWUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==