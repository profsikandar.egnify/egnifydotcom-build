(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Live Classes"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Teach/Teach.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Teach-breadcrum-11xX5 {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.Teach-breadcrum-11xX5 span {\n  font-weight: 600;\n}\n\n.Teach-toggle_outer-VIsVE {\n  width: 32px;\n  height: 17.8px;\n  border-radius: 24px;\n  background-color: #f36;\n  padding: 2px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: -10px;\n}\n\n.Teach-toggle_on-2JbJd {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-toggle_inner-3gUru {\n  width: 14px;\n  height: 14px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n/*   position: absolute;\n  width: 24px;\n  height: 36px;\n  bottom: 87px;\n  right: 560px;\n\n  img {\n    width: 100%;\n    height: 100%;\n    object-fit: contain;\n  }\n} */\n\n.Teach-headerbackgroundmobile-2Z0QY {\n  position: absolute;\n  bottom: 0;\n  right: 64px;\n  width: 445px;\n  height: 584px;\n}\n\n.Teach-headerbackgroundmobile-2Z0QY img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.Teach-authorimgbox-28-2r {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-authorimgbox-28-2r img {\n    width: 100%;\n    height: 100%;\n    max-width: 640px;\n    max-height: 560px;\n    justify-self: flex-end;\n  }\n\n/* .viewmore.mobile {\n  font-size: 14px;\n  line-height: 24px;\n  width: 100%;\n  color: #0076ff;\n  display: none;\n  justify-content: center;\n  align-items: center;\n  margin-top: 30px;\n\n  img {\n    width: 20px;\n    height: 20px;\n    margin-left: 5px;\n    margin-top: 1px;\n  }\n} */\n\n.Teach-contentPart-1lSgK {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 60%;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Teach-contentPart-1lSgK .Teach-topicImage-1CM7j {\n    width: 72px;\n    height: 72px;\n    padding: 16px;\n    margin-right: 24px;\n    background: #fff;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n  }\n\n.Teach-contentPart-1lSgK .Teach-topicImage-1CM7j img {\n      width: 42px;\n      height: 42px;\n    }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n\n    /* .viewmore {\n      font-size: 20px;\n      line-height: 32px;\n      width: 100%;\n      color: #0076ff;\n      display: flex;\n      align-items: center;\n      margin-top: 24px;\n\n      img {\n        width: 24px;\n        height: 24px;\n        margin-left: 5px;\n      }\n    } */\n  }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-section_title-2nyBd {\n      display: block;\n      margin: 12px 0;\n      font-size: 40px;\n      line-height: 48px;\n      letter-spacing: -1.2px;\n      color: #25282b;\n      font-weight: 600;\n    }\n\n.Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-textcontent-3s5Dz {\n      font-size: 20px;\n      line-height: 32px;\n      color: #25282b;\n      max-width: 370px;\n    }\n\n.Teach-customers_container-3dTy8 {\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  height: 560px;\n}\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR {\n    -ms-flex-order: 0;\n        order: 0;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: start;\n        align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n  }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-sriChaitanyaText-1HRt2 {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-author_title-3HNCy {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-about_author-3sn5h {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-allcustomers-2b8NO {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: row;\n          flex-direction: row;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-allcustomers-2b8NO img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.Teach-availableContainer-2e4mP {\n  background-color: #fff;\n  padding: 80px 113px 0 164px;\n  padding: 5rem 113px 0 164px;\n  // max-width: 1300px;\n}\n\n.Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-desktopImage-n9mcs {\n      width: 648px;\n      height: 328px;\n      margin-top: 35px;\n    }\n\n.Teach-availableTitle-28MiM {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Teach-available-k7Ani {\n  color: #f36;\n}\n\n.Teach-availableContentSection-JZlaF {\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.Teach-platformContainer-1IANc {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.Teach-platform-1GcJZ {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.Teach-platformOs-2DF6E {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.Teach-row-1VSyr {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.Teach-store-IZcDQ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.Teach-store-IZcDQ .Teach-playstore-PXfhG {\n    width: 140px;\n    height: 42px;\n    margin-right: 16px;\n  }\n\n.Teach-store-IZcDQ .Teach-appstore-2HMPf {\n    width: 140px;\n    height: 42px;\n  }\n\n.Teach-displayClients-1bg_Q span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ .Teach-topCard-2ZuD4 img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Teach-teachContainer-2Q-YB {\n  height: 680px;\n  padding: 24px 64px;\n  background-color: #ffece0;\n  position: relative;\n  cursor: alias;\n}\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as {\n    font-size: 48px;\n    line-height: 66px;\n    color: #25282b;\n    max-width: 542px;\n    margin: 12px 0 0 0;\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N {\n      width: 185px;\n      display: inline;\n      position: relative;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N img {\n        position: absolute;\n        bottom: -8px;\n        left: -8px;\n        width: 100%;\n        height: 8px;\n        max-width: 225px;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi {\n    margin-top: 64px;\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-requestDemo-AboHC {\n      border-radius: 4px;\n      background-color: #3fc;\n      font-size: 20px;\n      font-weight: 600;\n      line-height: 1.5;\n      cursor: pointer;\n      color: #000;\n      padding: 16px 24px;\n      width: -webkit-max-content;\n      width: -moz-max-content;\n      width: max-content;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 .Teach-whatsapp-34spS {\n        width: -webkit-fit-content;\n        width: -moz-fit-content;\n        width: fit-content;\n        font-size: 20px;\n        cursor: pointer;\n        font-weight: 600;\n        line-height: 1.5;\n        color: #25282b !important;\n        margin: 0 8px 0 32px;\n        padding-bottom: 0;\n        opacity: 0.6;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 img {\n        width: 32px;\n        height: 32px;\n      }\n\n/* .actionsWrapper {\n    position: absolute;\n    bottom: 26px;\n    width: 100%;\n    display: flex;\n\n    .action {\n      width: fit-content;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin-right: 24px;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        opacity: 0.7;\n        text-align: left;\n        margin-left: 8px;\n      }\n\n      .actionimgbox {\n        width: 24px;\n        height: 24px;\n        background-color: #fff;\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n\n        img {\n          width: 10px;\n          height: 9px;\n          background-color: #fff;\n        }\n      }\n    }\n  } */\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n\n    /* .featuresSection {\n      display: flex;\n      align-items: center;\n      margin-left: 30%;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        margin-right: 32px;\n        font-weight: 500;\n      }\n    } */\n  }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 {\n        width: 40px;\n        height: 40px;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachSectionName-1AdS4 {\n        margin-left: 8px;\n        font-size: 20px;\n        line-height: 30px;\n        color: #ff6400;\n        font-weight: bold;\n      }\n\n.Teach-teachContainer-2Q-YB .Teach-contentText-3eX56 {\n    font-size: 20px;\n    line-height: 40px;\n    color: #25282b;\n    font-weight: normal;\n    margin: 16px 0 0 0;\n    max-width: 687px;\n  }\n\n.Teach-downloadApp-3tWrv {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.Teach-downloadApp-3tWrv .Teach-downloadText-2YOcN {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.Teach-downloadApp-3tWrv .Teach-playStoreIcon-3YdD8 {\n  width: 132px;\n  height: 40px;\n}\n\n.Teach-displayClients-1bg_Q {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.Teach-displayClients-1bg_Q h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq .Teach-client-Ii9JF {\n  width: 172px;\n  height: 112px;\n}\n\n.Teach-displayClients-1bg_Q span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Teach-displayClients-1bg_Q span a:hover {\n  text-decoration: underline;\n}\n\n.Teach-achievedContainer-Aa5ks {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedHeading-2yxR4 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-clients-1hUFi {\n  background-color: #fff3eb;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-students-PAFZG {\n  background-color: #f7effe;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-tests-27DEs {\n  background-color: #ffebf0;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-achievedProfile-3w5Nj.Teach-questions-WBnlD {\n  background-color: #ebffef;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-questionsHighlight-37La9 {\n  color: #00ac26;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-testsHighlight-2Hb9O {\n  color: #f36;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-studentsHighlight-2H5F_ {\n  color: #8c00fe;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-highlight-3llRS.Teach-clientsHighlight-1ankD {\n  color: #f60;\n}\n\n.Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 .Teach-subText-3IRHK {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.Teach-section_container_reverse-1yEDY {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    padding: 0 64px;\n    margin: auto;\n    max-width: 1700px;\n  }\n\n.Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr .Teach-contentPart-1lSgK {\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n    }\n\n.Teach-section_container-cyyTN {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.Teach-section_container-cyyTN .Teach-section_contents-1NRDr {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    padding: 0 64px;\n    max-width: 1700px;\n    margin: auto;\n  }\n\n.Teach-teachContent-19QTk {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  max-width: 337px;\n  vertical-align: top;\n  margin-bottom: 24px;\n}\n\n.Teach-teachContent-19QTk .Teach-getranks-3JcXa {\n    width: 89px;\n    height: 26px;\n    margin-bottom: -5px;\n  }\n\n.Teach-teachContent-19QTk .Teach-zoom-3RXu- {\n    width: 64px;\n    height: 18px;\n  }\n\n.Teach-imagePart-3NAi0 {\n  width: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n  position: relative;\n}\n\n.Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ .Teach-topCard-2ZuD4 {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.Teach-imagePart-3NAi0 .Teach-bottomCircle-1QU8r {\n  position: absolute;\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n}\n\n.Teach-imagePart-3NAi0 .Teach-topCircle-2waHP {\n  position: absolute;\n  width: 64px;\n  height: 64px;\n  border-radius: 50%;\n}\n\n.Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      background-color: #ffe0e8;\n      width: 88px;\n      height: 88px;\n      bottom: -44px;\n      left: 79px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      background-color: #f2e5fe;\n      width: 88px;\n      height: 88px;\n      top: -44px;\n      right: 50px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      width: 88px;\n      height: 88px;\n      background-color: #ffe0e8;\n      bottom: -44px;\n      left: 48px;\n    }\n\n.Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      width: 88px;\n      height: 88px;\n      background-color: #3fc;\n      top: -44px;\n      right: 67px;\n      opacity: 0.3;\n    }\n\n.Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n      width: 88px;\n      height: 88px;\n      background-color: #feb546;\n      bottom: -44px;\n      left: 68px;\n      opacity: 0.2;\n    }\n\n.Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n      width: 88px;\n      height: 88px;\n      background-color: #0076ff;\n      top: -44px;\n      right: 36px;\n      opacity: 0.2;\n    }\n\n.Teach-allcustomers-2b8NO p {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Teach-content-s_zCd p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.Teach-tableContainer-32ppr {\n  padding: 56px 0;\n  background-color: #f7f7f7;\n}\n\n.Teach-tableContainer-32ppr h1 {\n    text-align: center;\n    margin: 0 0 24px 0;\n  }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU {\n    width: 956px;\n    display: grid;\n    grid-template-columns: 1fr 1fr;\n    margin: auto;\n  }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk {\n      padding: 24px 32px;\n      font-size: 18px;\n      display: -ms-flexbox;\n      display: flex;\n      border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz {\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-align: center;\n            align-items: center;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-comparisionText-3FmhP {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD {\n          width: 20px;\n          height: 20px;\n          margin-left: 12px;\n          border-radius: 4px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-positive-2nQ7l {\n          background-color: #cfc;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-negative-3D6w2 {\n          background-color: #ffd6d6;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 {\n        width: 5%;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct {\n          width: 8px;\n          height: 8px;\n          border-radius: 50%;\n          background-color: #25282b;\n          margin-top: 12px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct.Teach-red-zeFql {\n          background-color: #0c0;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ {\n        width: 95%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-direction: column;\n            flex-direction: column;\n      }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n          margin-bottom: 16px;\n        }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(odd) {\n      background-color: #ffebeb;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(even) {\n      background-color: #ebffeb;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(1),\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(2) {\n      background-color: #f7f7f7;\n      padding: 8px 16px;\n    }\n\n.Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(3),\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(4) {\n      background-color: rgba(37, 40, 43, 0.1);\n      font-size: 24px;\n      line-height: 32px;\n      color: #25282b;\n      text-align: left;\n      padding: 8px 16px;\n      font-weight: 600;\n    }\n\n@media only screen and (max-width: 1280px) {\n  .Teach-section_contents-1NRDr {\n    padding: 0 40px;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .Teach-availableContainer-2e4mP {\n    padding: 5rem 64px 0 64px;\n  }\n    .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n      grid-template-columns: 1fr 1fr;\n    }\n}\n\n@media only screen and (max-width: 990px) {\n  .Teach-breadcrum-11xX5 {\n    display: none;\n  }\n\n  /* .toggle_outer {\n    width: 32px;\n    height: 18px;\n    margin-top: -8px;\n  }\n\n  .toggle_inner {\n    width: 14px;\n    height: 14px;\n  }\n\n  .toggleAtRight {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n  }\n\n  .toggleAtLeft {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n  } */\n\n  /* .mouseicon {\n    display: none;\n  } */\n\n  .Teach-section_container-cyyTN {\n    padding: 32px 0;\n  }\n\n    .Teach-section_container-cyyTN .Teach-section_contents-1NRDr {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .Teach-section_container-cyyTN .Teach-section_contents-1NRDr .Teach-section_title-2nyBd {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .Teach-section_container_reverse-1yEDY {\n    padding: 32px 0;\n  }\n\n    .Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .Teach-section_container_reverse-1yEDY .Teach-section_contents-1NRDr .Teach-section_title-2nyBd {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .Teach-contentPart-1lSgK {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n    .Teach-contentPart-1lSgK .Teach-topicImage-1CM7j {\n      width: 48px;\n      height: 48px;\n      margin: auto;\n      padding: 10px;\n    }\n\n      .Teach-contentPart-1lSgK .Teach-topicImage-1CM7j img {\n        width: 28px;\n        height: 28px;\n      }\n\n    .Teach-contentPart-1lSgK .Teach-content-s_zCd {\n      max-width: none;\n\n      /* .viewmore {\n        display: none;\n        justify-content: center;\n      } */\n    }\n\n      .Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-section_title-2nyBd {\n        text-align: center;\n        font-size: 24px;\n      }\n\n      .Teach-contentPart-1lSgK .Teach-content-s_zCd .Teach-textcontent-3s5Dz {\n        display: block;\n        width: 100%;\n        max-width: 600px;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n        margin: auto;\n      }\n\n  .Teach-teachContent-19QTk {\n    font-size: 14px;\n    line-height: 24px;\n    vertical-align: top;\n    margin-bottom: 12px;\n    max-width: 320px;\n    text-align: center;\n  }\n\n    .Teach-teachContent-19QTk .Teach-getranks-3JcXa {\n      width: 60px;\n      height: 18px;\n      margin-bottom: 0;\n    }\n\n    .Teach-teachContent-19QTk .Teach-zoom-3RXu- {\n      width: 43px;\n      height: 11px;\n      margin-bottom: 2px;\n    }\n\n  .Teach-imagePart-3NAi0 {\n    width: 100%;\n    margin-top: 40px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n\n    /* .viewmore.mobile {\n      margin-bottom: 32px;\n      display: flex;\n    } */\n  }\n\n    .Teach-imagePart-3NAi0 .Teach-emptyCard-v-GO_ {\n      width: 320px;\n      height: 180px;\n    }\n      .Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -24px;\n        left: 254px;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-livepart-3TKC8 .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        opacity: 0.2;\n        bottom: -16px;\n        left: 61px;\n      }\n      .Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        top: -16px;\n        left: 28px;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-assignmentpart-20DkI .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #3fc;\n        opacity: 0.2;\n        bottom: -24px;\n        left: 236px;\n      }\n      .Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-topCircle-2waHP {\n        width: 48px;\n        height: 48px;\n        background-color: #f2e5fe;\n        left: 243px;\n        top: -16px;\n        opacity: 1;\n      }\n\n      .Teach-imagePart-3NAi0.Teach-doubtpart-2NkyQ .Teach-emptyCard-v-GO_ .Teach-bottomCircle-1QU8r {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        right: 227px;\n        bottom: -24px;\n      }\n\n  .Teach-headerbackgroundmobile-2Z0QY {\n    width: 237px;\n    height: 340px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .Teach-customers_container-3dTy8 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n    .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR {\n      -ms-flex-order: 2;\n          order: 2;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n    }\n\n      .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-customerLogo-xFAq9 {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-sriChaitanyaText-1HRt2 {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n        .Teach-customers_container-3dTy8 .Teach-customer_review-3Q1HR .Teach-authorWrapper-2FTu7 .Teach-about_author-3sn5h {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n\n    .Teach-customers_container-3dTy8 .Teach-authorimgbox-28-2r {\n      height: 50%;\n      width: 100%;\n      -ms-flex-order: 1;\n          order: 1;\n    }\n\n      .Teach-customers_container-3dTy8 .Teach-authorimgbox-28-2r img {\n        max-width: none;\n        max-height: none;\n      }\n\n  .Teach-teachContainer-2Q-YB .Teach-downloadApp-3tWrv {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .Teach-displayClients-1bg_Q span {\n    max-width: 307px;\n  }\n\n  .Teach-teachContainer-2Q-YB {\n    height: 1012px;\n    padding: 32px 16px;\n  }\n\n    .Teach-teachContainer-2Q-YB .Teach-heading-1K5as {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 24px auto 0 auto;\n    }\n\n      .Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N {\n        width: 120px;\n      }\n\n        .Teach-teachContainer-2Q-YB .Teach-heading-1K5as .Teach-learningText-3aV7N img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .Teach-teachContainer-2Q-YB .Teach-contentText-3eX56 {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-requestDemo-AboHC {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 .Teach-whatsapp-34spS {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .Teach-teachContainer-2Q-YB .Teach-buttonwrapper-i_jdi .Teach-whatsappwrapper-3jpz4 img {\n          width: 24px;\n          height: 24px;\n        }\n\n    /* .actionsWrapper {\n      right: 0;\n      width: fit-content;\n      margin-right: 16px;\n\n      .action {\n        margin-left: 12px;\n        margin-right: 0;\n\n        span {\n          display: none;\n        }\n\n        .actionimgbox {\n          margin: 0;\n        }\n      }\n    } */\n\n    .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachImgBox-mkz67 {\n          width: 32px;\n          height: 32px;\n        }\n\n        .Teach-teachContainer-2Q-YB .Teach-topSection-1ZZr- .Teach-teachSection-33snH .Teach-teachSectionName-1AdS4 {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .Teach-displayClients-1bg_Q {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .Teach-displayClients-1bg_Q h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Teach-displayClients-1bg_Q .Teach-clientsWrapper-3fJYq .Teach-client-Ii9JF {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .Teach-achievedContainer-Aa5ks {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .Teach-achievedContainer-Aa5ks .Teach-achievedHeading-2yxR4 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17 {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .Teach-achievedContainer-Aa5ks .Teach-achievedRow-AXwBV .Teach-card-6zL17:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .Teach-availableContainer-2e4mP {\n    padding: 32px 32px 0 32px;\n  }\n\n    .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ {\n      grid-template-columns: 1fr;\n    }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-availableTitle-28MiM {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc .Teach-platform-1GcJZ {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc .Teach-platformOs-2DF6E {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-row-1VSyr .Teach-platformContainer-1IANc:last-child {\n          margin-right: 0;\n        }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-desktopImage-n9mcs {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ .Teach-playstore-PXfhG {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .Teach-availableContainer-2e4mP .Teach-availableRow-3SeEQ .Teach-store-IZcDQ .Teach-appstore-2HMPf {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .Teach-tableContainer-32ppr {\n    padding: 40px 0;\n    background-color: #f7f7f7;\n  }\n\n    .Teach-tableContainer-32ppr h1 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 16px 32px 16px;\n    }\n\n    .Teach-tableContainer-32ppr .Teach-table-y9qtU {\n      width: 328px;\n      display: grid;\n      grid-template-columns: 1fr;\n      margin: auto;\n    }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk {\n        padding: 16px 20px 16px 16px;\n        font-size: 16px;\n        display: -ms-flexbox;\n        display: flex;\n        border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n      }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz {\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-align: center;\n              align-items: center;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-comparisionText-3FmhP {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD {\n            width: 16px;\n            height: 16px;\n            margin-left: 8px;\n            border-radius: 4px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-positive-2nQ7l {\n            background-color: #cfc;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-comparisionWrapper-MJ1Qz .Teach-box-287sD.Teach-negative-3D6w2 {\n            background-color: #ffd6d6;\n          }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ {\n          width: 92%;\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-direction: column;\n              flex-direction: column;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n            margin-bottom: 12px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-tableData-3nntZ span:last-child {\n            margin-bottom: 0;\n          }\n\n        .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 {\n          width: 8%;\n        }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 .Teach-dot-fu9Ct {\n            width: 8px;\n            height: 8px;\n            border-radius: 50%;\n            background-color: #25282b;\n            margin-top: 8px;\n          }\n\n          .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk .Teach-dotWrapper-ANQj6 span:last-child {\n            margin-bottom: 0;\n          }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(1) {\n        background: none;\n        margin-top: 16px;\n        padding: 8px 0;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(2) {\n        grid-row: 1/2;\n        background: none;\n        padding: 8px 0;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(3) {\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(4) {\n        grid-row: 2/3;\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(6) {\n        grid-row: 3/4;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(8) {\n        grid-row: 4/5;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(10) {\n        grid-row: 5/6;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(12) {\n        grid-row: 6/7;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(13) {\n        border-bottom: none;\n      }\n\n      .Teach-tableContainer-32ppr .Teach-table-y9qtU .Teach-comparision-WqBQk:nth-child(14) {\n        grid-row: 7/8;\n        border-bottom: none;\n      }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/Teach.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,YAAY;EACZ,eAAe;EACf,oBAAoB;EACpB,uBAAuB;EACvB,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,uBAAuB;CACxB;;AAED;;;;;;;;;;;IAWI;;AAEJ;EACE,mBAAmB;EACnB,UAAU;EACV,YAAY;EACZ,aAAa;EACb,cAAc;CACf;;AAED;IACI,YAAY;IACZ,aAAa;IACb,uBAAuB;OACpB,oBAAoB;GACxB;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;IACI,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,kBAAkB;IAClB,uBAAuB;GACxB;;AAEH;;;;;;;;;;;;;;;;IAgBI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,WAAW;EACX,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;IACI,YAAY;IACZ,aAAa;IACb,cAAc;IACd,mBAAmB;IACnB,iBAAiB;IACjB,mBAAmB;IACnB,mDAAmD;YAC3C,2CAA2C;GACpD;;AAEH;MACM,YAAY;MACZ,aAAa;KACd;;AAEL;IACI,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;;IAEjB;;;;;;;;;;;;;;QAcI;GACL;;AAEH;MACM,eAAe;MACf,eAAe;MACf,gBAAgB;MAChB,kBAAkB;MAClB,uBAAuB;MACvB,eAAe;MACf,iBAAiB;KAClB;;AAEL;MACM,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;MACf,iBAAiB;KAClB;;AAEL;EACE,uBAAuB;EACvB,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,cAAc;CACf;;AAED;IACI,kBAAkB;QACd,SAAS;IACb,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,mBAAmB;IACnB,WAAW;GACZ;;AAEH;MACM,aAAa;MACb,aAAa;MACb,mBAAmB;MACnB,oBAAoB;KACrB;;AAEL;QACQ,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;AAEP;MACM,gBAAgB;MAChB,kBAAkB;MAClB,iBAAiB;MACjB,oBAAoB;MACpB,iBAAiB;KAClB;;AAEL;MACM,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;KAC5B;;AAEL;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,iBAAiB;QACjB,YAAY;QACZ,mBAAmB;OACpB;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;AAEP;MACM,gBAAgB;MAChB,kBAAkB;MAClB,iBAAiB;MACjB,YAAY;MACZ,qBAAqB;MACrB,cAAc;MACd,wBAAwB;UACpB,oBAAoB;MACxB,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,gBAAgB;QAChB,iBAAiB;QACjB,YAAY;QACZ,aAAa;QACb,uBAAuB;WACpB,oBAAoB;OACxB;;AAEP;EACE,uBAAuB;EACvB,4BAA4B;EAC5B,4BAA4B;EAC5B,qBAAqB;CACtB;;AAED;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;MACd,iBAAiB;KAClB;;AAEL;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,YAAY;CACb;;AAED;EACE,qBAAqB;MACjB,4BAA4B;CACjC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,sBAAsB;EACtB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,qBAAqB;MACjB,4BAA4B;EAChC,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,aAAa;IACb,aAAa;IACb,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,cAAc;EACd,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,cAAc;CACf;;AAED;IACI,gBAAgB;IAChB,kBAAkB;IAClB,eAAe;IACf,iBAAiB;IACjB,mBAAmB;GACpB;;AAEH;MACM,aAAa;MACb,gBAAgB;MAChB,mBAAmB;KACpB;;AAEL;QACQ,mBAAmB;QACnB,aAAa;QACb,WAAW;QACX,YAAY;QACZ,YAAY;QACZ,iBAAiB;OAClB;;AAEP;IACI,iBAAiB;IACjB,qBAAqB;IACrB,cAAc;IACd,YAAY;IACZ,qBAAqB;QACjB,4BAA4B;IAChC,uBAAuB;QACnB,oBAAoB;GACzB;;AAEH;MACM,mBAAmB;MACnB,uBAAuB;MACvB,gBAAgB;MAChB,iBAAiB;MACjB,iBAAiB;MACjB,gBAAgB;MAChB,YAAY;MACZ,mBAAmB;MACnB,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;KACpB;;AAEL;MACM,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,qBAAqB;MACrB,cAAc;MACd,qBAAqB;UACjB,4BAA4B;MAChC,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,2BAA2B;QAC3B,wBAAwB;QACxB,mBAAmB;QACnB,gBAAgB;QAChB,gBAAgB;QAChB,iBAAiB;QACjB,iBAAiB;QACjB,0BAA0B;QAC1B,qBAAqB;QACrB,kBAAkB;QAClB,aAAa;OACd;;AAEP;QACQ,YAAY;QACZ,aAAa;OACd;;AAEP;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAsCM;;AAEN;IACI,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;;IAEjB;;;;;;;;;;;;QAYI;GACL;;AAEH;MACM,qBAAqB;MACrB,cAAc;MACd,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,YAAY;QACZ,aAAa;OACd;;AAEP;UACU,YAAY;UACZ,aAAa;UACb,uBAAuB;aACpB,oBAAoB;SACxB;;AAET;QACQ,iBAAiB;QACjB,gBAAgB;QAChB,kBAAkB;QAClB,eAAe;QACf,kBAAkB;OACnB;;AAEP;IACI,gBAAgB;IAChB,kBAAkB;IAClB,eAAe;IACf,oBAAoB;IACpB,mBAAmB;IACnB,iBAAiB;GAClB;;AAEH;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,aAAa;EACb,aAAa;CACd;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,sCAAsC;EACtC,UAAU;EACV,UAAU;EACV,aAAa;CACd;;AAED;EACE,aAAa;EACb,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,6BAA6B;EAC7B,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,sBAAsB;EACtB,+DAA+D;UACvD,uDAAuD;EAC/D,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;;;;;;;;;;;;;;IAcI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;EAC1B,iBAAiB;EACjB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,wBAAwB;QACpB,oBAAoB;IACxB,YAAY;IACZ,gBAAgB;IAChB,aAAa;IACb,kBAAkB;GACnB;;AAEH;MACM,qBAAqB;UACjB,4BAA4B;KACjC;;AAEL;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;EACvB,iBAAiB;EACjB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,gCAAgC;QAC5B,4BAA4B;IAChC,YAAY;IACZ,gBAAgB;IAChB,kBAAkB;IAClB,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,YAAY;IACZ,aAAa;IACb,oBAAoB;GACrB;;AAEH;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,WAAW;EACX,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,cAAc;EACd,mBAAmB;EACnB,mDAAmD;UAC3C,2CAA2C;EACnD,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;MACM,0BAA0B;MAC1B,YAAY;MACZ,aAAa;MACb,cAAc;MACd,WAAW;KACZ;;AAEL;MACM,0BAA0B;MAC1B,YAAY;MACZ,aAAa;MACb,WAAW;MACX,YAAY;KACb;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,cAAc;MACd,WAAW;KACZ;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,uBAAuB;MACvB,WAAW;MACX,YAAY;MACZ,aAAa;KACd;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,cAAc;MACd,WAAW;MACX,aAAa;KACd;;AAEL;MACM,YAAY;MACZ,aAAa;MACb,0BAA0B;MAC1B,WAAW;MACX,YAAY;MACZ,aAAa;KACd;;AAEL;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,uBAAuB;EACvB,eAAe;EACf,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,0BAA0B;CAC3B;;AAED;IACI,mBAAmB;IACnB,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,cAAc;IACd,+BAA+B;IAC/B,aAAa;GACd;;AAEH;MACM,mBAAmB;MACnB,gBAAgB;MAChB,qBAAqB;MACrB,cAAc;MACd,2CAA2C;KAC5C;;AAEL;QACQ,qBAAqB;QACrB,cAAc;QACd,uBAAuB;YACnB,oBAAoB;OACzB;;AAEP;UACU,gBAAgB;UAChB,kBAAkB;UAClB,eAAe;SAChB;;AAET;UACU,YAAY;UACZ,aAAa;UACb,kBAAkB;UAClB,mBAAmB;SACpB;;AAET;UACU,uBAAuB;SACxB;;AAET;UACU,0BAA0B;SAC3B;;AAET;QACQ,UAAU;OACX;;AAEP;UACU,WAAW;UACX,YAAY;UACZ,mBAAmB;UACnB,0BAA0B;UAC1B,iBAAiB;SAClB;;AAET;UACU,uBAAuB;SACxB;;AAET;QACQ,WAAW;QACX,qBAAqB;QACrB,cAAc;QACd,2BAA2B;YACvB,uBAAuB;OAC5B;;AAEP;UACU,gBAAgB;UAChB,kBAAkB;UAClB,eAAe;UACf,oBAAoB;SACrB;;AAET;MACM,0BAA0B;KAC3B;;AAEL;MACM,0BAA0B;KAC3B;;AAEL;;MAEM,0BAA0B;MAC1B,kBAAkB;KACnB;;AAEL;;MAEM,wCAAwC;MACxC,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;MACf,iBAAiB;MACjB,kBAAkB;MAClB,iBAAiB;KAClB;;AAEL;EACE;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,0BAA0B;GAC3B;IACC;MACE,+BAA+B;KAChC;CACJ;;AAED;EACE;IACE,cAAc;GACf;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;MAyBI;;EAEJ;;MAEI;;EAEJ;IACE,gBAAgB;GACjB;;IAEC;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,gBAAgB;KACjB;;MAEC;QACE,oBAAoB;QACpB,eAAe;OAChB;;EAEL;IACE,gBAAgB;GACjB;;IAEC;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,gBAAgB;KACjB;;MAEC;QACE,oBAAoB;QACpB,eAAe;OAChB;;EAEL;IACE,YAAY;IACZ,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,aAAa;MACb,cAAc;KACf;;MAEC;QACE,YAAY;QACZ,aAAa;OACd;;IAEH;MACE,gBAAgB;;MAEhB;;;UAGI;KACL;;MAEC;QACE,mBAAmB;QACnB,gBAAgB;OACjB;;MAED;QACE,eAAe;QACf,YAAY;QACZ,iBAAiB;QACjB,mBAAmB;QACnB,gBAAgB;QAChB,kBAAkB;QAClB,aAAa;OACd;;EAEL;IACE,gBAAgB;IAChB,kBAAkB;IAClB,oBAAoB;IACpB,oBAAoB;IACpB,iBAAiB;IACjB,mBAAmB;GACpB;;IAEC;MACE,YAAY;MACZ,aAAa;MACb,iBAAiB;KAClB;;IAED;MACE,YAAY;MACZ,aAAa;MACb,mBAAmB;KACpB;;EAEH;IACE,YAAY;IACZ,iBAAiB;IACjB,sBAAsB;QAClB,wBAAwB;IAC5B,2BAA2B;QACvB,uBAAuB;;IAE3B;;;QAGI;GACL;;IAEC;MACE,aAAa;MACb,cAAc;KACf;MACC;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,aAAa;QACb,WAAW;QACX,YAAY;OACb;;MAED;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,aAAa;QACb,cAAc;QACd,WAAW;OACZ;MACD;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,WAAW;QACX,WAAW;OACZ;;MAED;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,cAAc;QACd,YAAY;OACb;MACD;QACE,YAAY;QACZ,aAAa;QACb,0BAA0B;QAC1B,YAAY;QACZ,WAAW;QACX,WAAW;OACZ;;MAED;QACE,YAAY;QACZ,aAAa;QACb,uBAAuB;QACvB,aAAa;QACb,aAAa;QACb,cAAc;OACf;;EAEL;IACE,aAAa;IACb,cAAc;IACd,SAAS;IACT,SAAS;IACT,cAAc;IACd,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,aAAa;IACb,YAAY;IACZ,uBAAuB;QACnB,oBAAoB;GACzB;;IAEC;MACE,kBAAkB;UACd,SAAS;MACb,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,uBAAuB;UACnB,oBAAoB;MACxB,mBAAmB;MACnB,YAAY;MACZ,YAAY;MACZ,mBAAmB;KACpB;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,mBAAmB;QACnB,iBAAiB;QACjB,oBAAoB;OACrB;;MAED;QACE,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;QACnB,gBAAgB;OACjB;QACC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,oBAAoB;SACrB;;IAEL;MACE,YAAY;MACZ,YAAY;MACZ,kBAAkB;UACd,SAAS;KACd;;MAEC;QACE,gBAAgB;QAChB,iBAAiB;OAClB;;EAEL;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,aAAa;GACd;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,eAAe;IACf,mBAAmB;GACpB;;IAEC;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,gBAAgB;MAChB,yBAAyB;KAC1B;;MAEC;QACE,aAAa;OACd;;QAEC;UACE,QAAQ;UACR,iBAAiB;SAClB;;IAEL;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,iBAAiB;MACjB,yBAAyB;KAC1B;;IAED;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,iBAAiB;KAClB;;MAEC;QACE,kBAAkB;QAClB,gBAAgB;QAChB,kBAAkB;QAClB,gBAAgB;OACjB;;MAED;QACE,iBAAiB;QACjB,oBAAoB;OACrB;;QAEC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,WAAW;UACX,eAAe;SAChB;;QAED;UACE,YAAY;UACZ,aAAa;SACd;;IAEL;;;;;;;;;;;;;;;;;QAiBI;;IAEJ;MACE,sBAAsB;UAClB,wBAAwB;MAC5B,cAAc;;MAEd;;UAEI;KACL;QACG;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,gBAAgB;UAChB,kBAAkB;SACnB;;EAEP;IACE,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;GACd;;EAED;;MAEI;;EAEJ;IACE,mBAAmB;IACnB,2DAA2D;GAC5D;;IAEC;MACE,gBAAgB;MAChB,oBAAoB;MACpB,yBAAyB;MACzB,iBAAiB;KAClB;;IAED;MACE,2BAA2B;MAC3B,UAAU;KACX;;MAEC;QACE,YAAY;QACZ,iBAAiB;QACjB,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;QACnB,kBAAkB;QAClB,cAAc;;QAEd;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;;QAEb;;YAEI;OACL;;MAED;QACE,kBAAkB;YACd,SAAS;OACd;;EAEL;IACE,0BAA0B;GAC3B;;IAEC;MACE,2BAA2B;KAC5B;;MAEC;QACE,gBAAgB;QAChB,mBAAmB;QACnB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,oBAAoB;QACpB,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,YAAY;UACZ,qBAAqB;cACjB,4BAA4B;UAChC,mBAAmB;UACnB,iBAAiB;SAClB;;UAEC;YACE,kBAAkB;YAClB,kBAAkB;WACnB;;UAED;YACE,gBAAgB;YAChB,kBAAkB;WACnB;;QAEH;UACE,gBAAgB;SACjB;;MAEH;QACE,aAAa;QACb,cAAc;QACd,aAAa;OACd;;MAED;QACE,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,aAAa;UACb,aAAa;UACb,mBAAmB;SACpB;;QAED;UACE,aAAa;UACb,aAAa;UACb,UAAU;SACX;;EAEP;IACE,gBAAgB;IAChB,0BAA0B;GAC3B;;IAEC;MACE,gBAAgB;MAChB,oBAAoB;MACpB,yBAAyB;KAC1B;;IAED;MACE,aAAa;MACb,cAAc;MACd,2BAA2B;MAC3B,aAAa;KACd;;MAEC;QACE,6BAA6B;QAC7B,gBAAgB;QAChB,qBAAqB;QACrB,cAAc;QACd,2CAA2C;OAC5C;;QAEC;UACE,qBAAqB;UACrB,cAAc;UACd,uBAAuB;cACnB,oBAAoB;SACzB;;UAEC;YACE,gBAAgB;YAChB,kBAAkB;YAClB,eAAe;WAChB;;UAED;YACE,YAAY;YACZ,aAAa;YACb,iBAAiB;YACjB,mBAAmB;WACpB;;UAED;YACE,uBAAuB;WACxB;;UAED;YACE,0BAA0B;WAC3B;;QAEH;UACE,WAAW;UACX,qBAAqB;UACrB,cAAc;UACd,2BAA2B;cACvB,uBAAuB;SAC5B;;UAEC;YACE,gBAAgB;YAChB,kBAAkB;YAClB,eAAe;YACf,oBAAoB;WACrB;;UAED;YACE,iBAAiB;WAClB;;QAEH;UACE,UAAU;SACX;;UAEC;YACE,WAAW;YACX,YAAY;YACZ,mBAAmB;YACnB,0BAA0B;YAC1B,gBAAgB;WACjB;;UAED;YACE,iBAAiB;WAClB;;MAEL;QACE,iBAAiB;QACjB,iBAAiB;QACjB,eAAe;OAChB;;MAED;QACE,cAAc;QACd,iBAAiB;QACjB,eAAe;OAChB;;MAED;QACE,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;MAED;QACE,cAAc;QACd,gBAAgB;QAChB,kBAAkB;QAClB,oBAAoB;OACrB;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,cAAc;OACf;;MAED;QACE,oBAAoB;OACrB;;MAED;QACE,cAAc;QACd,oBAAoB;OACrB;CACN","file":"Teach.scss","sourcesContent":[".breadcrum {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.breadcrum span {\n  font-weight: 600;\n}\n\n.toggle_outer {\n  width: 32px;\n  height: 17.8px;\n  border-radius: 24px;\n  background-color: #f36;\n  padding: 2px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: -10px;\n}\n\n.toggle_on {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.toggle_inner {\n  width: 14px;\n  height: 14px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n/*   position: absolute;\n  width: 24px;\n  height: 36px;\n  bottom: 87px;\n  right: 560px;\n\n  img {\n    width: 100%;\n    height: 100%;\n    object-fit: contain;\n  }\n} */\n\n.headerbackgroundmobile {\n  position: absolute;\n  bottom: 0;\n  right: 64px;\n  width: 445px;\n  height: 584px;\n}\n\n.headerbackgroundmobile img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.authorimgbox {\n  width: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.authorimgbox img {\n    width: 100%;\n    height: 100%;\n    max-width: 640px;\n    max-height: 560px;\n    justify-self: flex-end;\n  }\n\n/* .viewmore.mobile {\n  font-size: 14px;\n  line-height: 24px;\n  width: 100%;\n  color: #0076ff;\n  display: none;\n  justify-content: center;\n  align-items: center;\n  margin-top: 30px;\n\n  img {\n    width: 20px;\n    height: 20px;\n    margin-left: 5px;\n    margin-top: 1px;\n  }\n} */\n\n.contentPart {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 60%;\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.contentPart .topicImage {\n    width: 72px;\n    height: 72px;\n    padding: 16px;\n    margin-right: 24px;\n    background: #fff;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 0 16px 0 rgba(0, 0, 0, 0.12);\n  }\n\n.contentPart .topicImage img {\n      width: 42px;\n      height: 42px;\n    }\n\n.contentPart .content {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n\n    /* .viewmore {\n      font-size: 20px;\n      line-height: 32px;\n      width: 100%;\n      color: #0076ff;\n      display: flex;\n      align-items: center;\n      margin-top: 24px;\n\n      img {\n        width: 24px;\n        height: 24px;\n        margin-left: 5px;\n      }\n    } */\n  }\n\n.contentPart .content .section_title {\n      display: block;\n      margin: 12px 0;\n      font-size: 40px;\n      line-height: 48px;\n      letter-spacing: -1.2px;\n      color: #25282b;\n      font-weight: 600;\n    }\n\n.contentPart .content .textcontent {\n      font-size: 20px;\n      line-height: 32px;\n      color: #25282b;\n      max-width: 370px;\n    }\n\n.customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  height: 560px;\n}\n\n.customers_container .customer_review {\n    -ms-flex-order: 0;\n        order: 0;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: start;\n        align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n  }\n\n.customers_container .customer_review .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n    }\n\n.customers_container .customer_review .customerLogo img {\n        width: 100%;\n        height: 100%;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.customers_container .customer_review .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n.customers_container .customer_review .authorWrapper {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n    }\n\n.customers_container .customer_review .authorWrapper .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n.customers_container .customer_review .authorWrapper .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n\n.customers_container .customer_review .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: row;\n          flex-direction: row;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.customers_container .customer_review .allcustomers img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        -o-object-fit: contain;\n           object-fit: contain;\n      }\n\n.availableContainer {\n  background-color: #fff;\n  padding: 80px 113px 0 164px;\n  padding: 5rem 113px 0 164px;\n  // max-width: 1300px;\n}\n\n.availableContainer .availableRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.availableContainer .availableRow .desktopImage {\n      width: 648px;\n      height: 328px;\n      margin-top: 35px;\n    }\n\n.availableTitle {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.available {\n  color: #f36;\n}\n\n.availableContentSection {\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.platformContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.platform {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.platformOs {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.row {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.store {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.store .playstore {\n    width: 140px;\n    height: 42px;\n    margin-right: 16px;\n  }\n\n.store .appstore {\n    width: 140px;\n    height: 42px;\n  }\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.imagePart .emptyCard .topCard img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer {\n  height: 680px;\n  padding: 24px 64px;\n  background-color: #ffece0;\n  position: relative;\n  cursor: alias;\n}\n\n.teachContainer .heading {\n    font-size: 48px;\n    line-height: 66px;\n    color: #25282b;\n    max-width: 542px;\n    margin: 12px 0 0 0;\n  }\n\n.teachContainer .heading .learningText {\n      width: 185px;\n      display: inline;\n      position: relative;\n    }\n\n.teachContainer .heading .learningText img {\n        position: absolute;\n        bottom: -8px;\n        left: -8px;\n        width: 100%;\n        height: 8px;\n        max-width: 225px;\n      }\n\n.teachContainer .buttonwrapper {\n    margin-top: 64px;\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n.teachContainer .buttonwrapper .requestDemo {\n      border-radius: 4px;\n      background-color: #3fc;\n      font-size: 20px;\n      font-weight: 600;\n      line-height: 1.5;\n      cursor: pointer;\n      color: #000;\n      padding: 16px 24px;\n      width: -webkit-max-content;\n      width: -moz-max-content;\n      width: max-content;\n    }\n\n.teachContainer .buttonwrapper .whatsappwrapper {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n        width: -webkit-fit-content;\n        width: -moz-fit-content;\n        width: fit-content;\n        font-size: 20px;\n        cursor: pointer;\n        font-weight: 600;\n        line-height: 1.5;\n        color: #25282b !important;\n        margin: 0 8px 0 32px;\n        padding-bottom: 0;\n        opacity: 0.6;\n      }\n\n.teachContainer .buttonwrapper .whatsappwrapper img {\n        width: 32px;\n        height: 32px;\n      }\n\n/* .actionsWrapper {\n    position: absolute;\n    bottom: 26px;\n    width: 100%;\n    display: flex;\n\n    .action {\n      width: fit-content;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin-right: 24px;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        opacity: 0.7;\n        text-align: left;\n        margin-left: 8px;\n      }\n\n      .actionimgbox {\n        width: 24px;\n        height: 24px;\n        background-color: #fff;\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n\n        img {\n          width: 10px;\n          height: 9px;\n          background-color: #fff;\n        }\n      }\n    }\n  } */\n\n.teachContainer .topSection {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n\n    /* .featuresSection {\n      display: flex;\n      align-items: center;\n      margin-left: 30%;\n\n      span {\n        font-size: 14px;\n        line-height: 20px;\n        color: #25282b;\n        margin-right: 32px;\n        font-weight: 500;\n      }\n    } */\n  }\n\n.teachContainer .topSection .teachSection {\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.teachContainer .topSection .teachSection .teachImgBox {\n        width: 40px;\n        height: 40px;\n      }\n\n.teachContainer .topSection .teachSection .teachImgBox img {\n          width: 100%;\n          height: 100%;\n          -o-object-fit: contain;\n             object-fit: contain;\n        }\n\n.teachContainer .topSection .teachSection .teachSectionName {\n        margin-left: 8px;\n        font-size: 20px;\n        line-height: 30px;\n        color: #ff6400;\n        font-weight: bold;\n      }\n\n.teachContainer .contentText {\n    font-size: 20px;\n    line-height: 40px;\n    color: #25282b;\n    font-weight: normal;\n    margin: 16px 0 0 0;\n    max-width: 687px;\n  }\n\n.downloadApp {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.downloadApp .downloadText {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.downloadApp .playStoreIcon {\n  width: 132px;\n  height: 40px;\n}\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients .clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.displayClients .clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.achievedContainer .achievedRow {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.achievedContainer .achievedRow .card {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n  background-color: #fff3eb;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n  background-color: #f7effe;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n  background-color: #ffebf0;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n  background-color: #ebffef;\n}\n\n.achievedContainer .achievedRow .card .highlight {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n  color: #00ac26;\n}\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n  color: #f36;\n}\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n  color: #8c00fe;\n}\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n  color: #f60;\n}\n\n.achievedContainer .achievedRow .card .subText {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.section_container_reverse {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.section_container_reverse .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    padding: 0 64px;\n    margin: auto;\n    max-width: 1700px;\n  }\n\n.section_container_reverse .section_contents .contentPart {\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n    }\n\n.section_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 184px 0;\n  width: 100%;\n  margin: auto;\n}\n\n.section_container .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    padding: 0 64px;\n    max-width: 1700px;\n    margin: auto;\n  }\n\n.teachContent {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  max-width: 337px;\n  vertical-align: top;\n  margin-bottom: 24px;\n}\n\n.teachContent .getranks {\n    width: 89px;\n    height: 26px;\n    margin-bottom: -5px;\n  }\n\n.teachContent .zoom {\n    width: 64px;\n    height: 18px;\n  }\n\n.imagePart {\n  width: 40%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.imagePart .emptyCard {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  -webkit-box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n          box-shadow: 0 0 32px 0 rgba(0, 0, 0, 0.08);\n  position: relative;\n}\n\n.imagePart .emptyCard .topCard {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.imagePart .bottomCircle {\n  position: absolute;\n  width: 48px;\n  height: 48px;\n  border-radius: 50%;\n}\n\n.imagePart .topCircle {\n  position: absolute;\n  width: 64px;\n  height: 64px;\n  border-radius: 50%;\n}\n\n.imagePart.livepart .emptyCard .bottomCircle {\n      background-color: #ffe0e8;\n      width: 88px;\n      height: 88px;\n      bottom: -44px;\n      left: 79px;\n    }\n\n.imagePart.livepart .emptyCard .topCircle {\n      background-color: #f2e5fe;\n      width: 88px;\n      height: 88px;\n      top: -44px;\n      right: 50px;\n    }\n\n.imagePart.assignmentpart .emptyCard .bottomCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #ffe0e8;\n      bottom: -44px;\n      left: 48px;\n    }\n\n.imagePart.assignmentpart .emptyCard .topCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #3fc;\n      top: -44px;\n      right: 67px;\n      opacity: 0.3;\n    }\n\n.imagePart.doubtpart .emptyCard .bottomCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #feb546;\n      bottom: -44px;\n      left: 68px;\n      opacity: 0.2;\n    }\n\n.imagePart.doubtpart .emptyCard .topCircle {\n      width: 88px;\n      height: 88px;\n      background-color: #0076ff;\n      top: -44px;\n      right: 36px;\n      opacity: 0.2;\n    }\n\n.allcustomers p {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.content p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.tableContainer {\n  padding: 56px 0;\n  background-color: #f7f7f7;\n}\n\n.tableContainer h1 {\n    text-align: center;\n    margin: 0 0 24px 0;\n  }\n\n.tableContainer .table {\n    width: 956px;\n    display: grid;\n    grid-template-columns: 1fr 1fr;\n    margin: auto;\n  }\n\n.tableContainer .table .comparision {\n      padding: 24px 32px;\n      font-size: 18px;\n      display: -ms-flexbox;\n      display: flex;\n      border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    }\n\n.tableContainer .table .comparision .comparisionWrapper {\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-align: center;\n            align-items: center;\n      }\n\n.tableContainer .table .comparision .comparisionWrapper .comparisionText {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box {\n          width: 20px;\n          height: 20px;\n          margin-left: 12px;\n          border-radius: 4px;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box.positive {\n          background-color: #cfc;\n        }\n\n.tableContainer .table .comparision .comparisionWrapper .box.negative {\n          background-color: #ffd6d6;\n        }\n\n.tableContainer .table .comparision .dotWrapper {\n        width: 5%;\n      }\n\n.tableContainer .table .comparision .dotWrapper .dot {\n          width: 8px;\n          height: 8px;\n          border-radius: 50%;\n          background-color: #25282b;\n          margin-top: 12px;\n        }\n\n.tableContainer .table .comparision .dotWrapper .dot.red {\n          background-color: #0c0;\n        }\n\n.tableContainer .table .comparision .tableData {\n        width: 95%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-direction: column;\n            flex-direction: column;\n      }\n\n.tableContainer .table .comparision .tableData span {\n          font-size: 20px;\n          line-height: 32px;\n          color: #25282b;\n          margin-bottom: 16px;\n        }\n\n.tableContainer .table .comparision:nth-child(odd) {\n      background-color: #ffebeb;\n    }\n\n.tableContainer .table .comparision:nth-child(even) {\n      background-color: #ebffeb;\n    }\n\n.tableContainer .table .comparision:nth-child(1),\n    .tableContainer .table .comparision:nth-child(2) {\n      background-color: #f7f7f7;\n      padding: 8px 16px;\n    }\n\n.tableContainer .table .comparision:nth-child(3),\n    .tableContainer .table .comparision:nth-child(4) {\n      background-color: rgba(37, 40, 43, 0.1);\n      font-size: 24px;\n      line-height: 32px;\n      color: #25282b;\n      text-align: left;\n      padding: 8px 16px;\n      font-weight: 600;\n    }\n\n@media only screen and (max-width: 1280px) {\n  .section_contents {\n    padding: 0 40px;\n  }\n}\n\n@media only screen and (max-width: 1200px) {\n  .availableContainer {\n    padding: 5rem 64px 0 64px;\n  }\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr 1fr;\n    }\n}\n\n@media only screen and (max-width: 990px) {\n  .breadcrum {\n    display: none;\n  }\n\n  /* .toggle_outer {\n    width: 32px;\n    height: 18px;\n    margin-top: -8px;\n  }\n\n  .toggle_inner {\n    width: 14px;\n    height: 14px;\n  }\n\n  .toggleAtRight {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n  }\n\n  .toggleAtLeft {\n    padding: 3% 5% 0 5%;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n  } */\n\n  /* .mouseicon {\n    display: none;\n  } */\n\n  .section_container {\n    padding: 32px 0;\n  }\n\n    .section_container .section_contents {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .section_container .section_contents .section_title {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .section_container_reverse {\n    padding: 32px 0;\n  }\n\n    .section_container_reverse .section_contents {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      padding: 0 16px;\n    }\n\n      .section_container_reverse .section_contents .section_title {\n        line-height: normal;\n        margin: 16px 0;\n      }\n\n  .contentPart {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n    .contentPart .topicImage {\n      width: 48px;\n      height: 48px;\n      margin: auto;\n      padding: 10px;\n    }\n\n      .contentPart .topicImage img {\n        width: 28px;\n        height: 28px;\n      }\n\n    .contentPart .content {\n      max-width: none;\n\n      /* .viewmore {\n        display: none;\n        justify-content: center;\n      } */\n    }\n\n      .contentPart .content .section_title {\n        text-align: center;\n        font-size: 24px;\n      }\n\n      .contentPart .content .textcontent {\n        display: block;\n        width: 100%;\n        max-width: 600px;\n        text-align: center;\n        font-size: 14px;\n        line-height: 24px;\n        margin: auto;\n      }\n\n  .teachContent {\n    font-size: 14px;\n    line-height: 24px;\n    vertical-align: top;\n    margin-bottom: 12px;\n    max-width: 320px;\n    text-align: center;\n  }\n\n    .teachContent .getranks {\n      width: 60px;\n      height: 18px;\n      margin-bottom: 0;\n    }\n\n    .teachContent .zoom {\n      width: 43px;\n      height: 11px;\n      margin-bottom: 2px;\n    }\n\n  .imagePart {\n    width: 100%;\n    margin-top: 40px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n\n    /* .viewmore.mobile {\n      margin-bottom: 32px;\n      display: flex;\n    } */\n  }\n\n    .imagePart .emptyCard {\n      width: 320px;\n      height: 180px;\n    }\n      .imagePart.livepart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #feb546;\n        opacity: 0.2;\n        top: -24px;\n        left: 254px;\n      }\n\n      .imagePart.livepart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #0076ff;\n        opacity: 0.2;\n        bottom: -16px;\n        left: 61px;\n      }\n      .imagePart.assignmentpart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        top: -16px;\n        left: 28px;\n      }\n\n      .imagePart.assignmentpart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #3fc;\n        opacity: 0.2;\n        bottom: -24px;\n        left: 236px;\n      }\n      .imagePart.doubtpart .emptyCard .topCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f2e5fe;\n        left: 243px;\n        top: -16px;\n        opacity: 1;\n      }\n\n      .imagePart.doubtpart .emptyCard .bottomCircle {\n        width: 48px;\n        height: 48px;\n        background-color: #f36;\n        opacity: 0.1;\n        right: 227px;\n        bottom: -24px;\n      }\n\n  .headerbackgroundmobile {\n    width: 237px;\n    height: 340px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .customers_container {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n    width: 100%;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n    .customers_container .customer_review {\n      -ms-flex-order: 2;\n          order: 2;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n    }\n\n      .customers_container .customer_review .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .customers_container .customer_review .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n        .customers_container .customer_review .authorWrapper .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n\n    .customers_container .authorimgbox {\n      height: 50%;\n      width: 100%;\n      -ms-flex-order: 1;\n          order: 1;\n    }\n\n      .customers_container .authorimgbox img {\n        max-width: none;\n        max-height: none;\n      }\n\n  .teachContainer .downloadApp {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .displayClients span {\n    max-width: 307px;\n  }\n\n  .teachContainer {\n    height: 1012px;\n    padding: 32px 16px;\n  }\n\n    .teachContainer .heading {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 24px auto 0 auto;\n    }\n\n      .teachContainer .heading .learningText {\n        width: 120px;\n      }\n\n        .teachContainer .heading .learningText img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .teachContainer .contentText {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .teachContainer .buttonwrapper {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .teachContainer .buttonwrapper .requestDemo {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .teachContainer .buttonwrapper .whatsappwrapper {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .teachContainer .buttonwrapper .whatsappwrapper img {\n          width: 24px;\n          height: 24px;\n        }\n\n    /* .actionsWrapper {\n      right: 0;\n      width: fit-content;\n      margin-right: 16px;\n\n      .action {\n        margin-left: 12px;\n        margin-right: 0;\n\n        span {\n          display: none;\n        }\n\n        .actionimgbox {\n          margin: 0;\n        }\n      }\n    } */\n\n    .teachContainer .topSection {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .teachContainer .topSection .teachSection .teachImgBox {\n          width: 32px;\n          height: 32px;\n        }\n\n        .teachContainer .topSection .teachSection .teachSectionName {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .achievedContainer {\n    padding: 24px 16px;\n    background-image: url('/images/Teach/Confetti_mobile.svg');\n  }\n\n    .achievedContainer .achievedHeading {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 auto 24px auto;\n      max-width: 300px;\n    }\n\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr;\n      gap: 12px;\n    }\n\n      .achievedContainer .achievedRow .card {\n        width: 100%;\n        max-width: 328px;\n        font-size: 14px;\n        line-height: 24px;\n        margin-right: 12px;\n        margin-left: 12px;\n        padding: 16px;\n\n        /* .hightLight {\n          font-size: 20px;\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(2) {\n        -ms-flex-order: 3;\n            order: 3;\n\n        /* .hightLight::after {\n          content: '';\n        } */\n      }\n\n      .achievedContainer .achievedRow .card:nth-child(3) {\n        -ms-flex-order: 2;\n            order: 2;\n      }\n\n  .availableContainer {\n    padding: 32px 32px 0 32px;\n  }\n\n    .availableContainer .availableRow {\n      grid-template-columns: 1fr;\n    }\n\n      .availableContainer .availableRow .availableTitle {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .availableContainer .availableRow .row {\n        margin: 0;\n        margin-bottom: 36px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .row .platformContainer {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .availableContainer .availableRow .row .platformContainer .platform {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .availableContainer .availableRow .row .platformContainer .platformOs {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .availableContainer .availableRow .row .platformContainer:last-child {\n          margin-right: 0;\n        }\n\n      .availableContainer .availableRow .desktopImage {\n        width: 296px;\n        height: 140px;\n        margin: auto;\n      }\n\n      .availableContainer .availableRow .store {\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .store .playstore {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .availableContainer .availableRow .store .appstore {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .tableContainer {\n    padding: 40px 0;\n    background-color: #f7f7f7;\n  }\n\n    .tableContainer h1 {\n      font-size: 24px;\n      line-height: normal;\n      margin: 0 16px 32px 16px;\n    }\n\n    .tableContainer .table {\n      width: 328px;\n      display: grid;\n      grid-template-columns: 1fr;\n      margin: auto;\n    }\n\n      .tableContainer .table .comparision {\n        padding: 16px 20px 16px 16px;\n        font-size: 16px;\n        display: -ms-flexbox;\n        display: flex;\n        border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n      }\n\n        .tableContainer .table .comparision .comparisionWrapper {\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-align: center;\n              align-items: center;\n        }\n\n          .tableContainer .table .comparision .comparisionWrapper .comparisionText {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box {\n            width: 16px;\n            height: 16px;\n            margin-left: 8px;\n            border-radius: 4px;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box.positive {\n            background-color: #cfc;\n          }\n\n          .tableContainer .table .comparision .comparisionWrapper .box.negative {\n            background-color: #ffd6d6;\n          }\n\n        .tableContainer .table .comparision .tableData {\n          width: 92%;\n          display: -ms-flexbox;\n          display: flex;\n          -ms-flex-direction: column;\n              flex-direction: column;\n        }\n\n          .tableContainer .table .comparision .tableData span {\n            font-size: 14px;\n            line-height: 24px;\n            color: #25282b;\n            margin-bottom: 12px;\n          }\n\n          .tableContainer .table .comparision .tableData span:last-child {\n            margin-bottom: 0;\n          }\n\n        .tableContainer .table .comparision .dotWrapper {\n          width: 8%;\n        }\n\n          .tableContainer .table .comparision .dotWrapper .dot {\n            width: 8px;\n            height: 8px;\n            border-radius: 50%;\n            background-color: #25282b;\n            margin-top: 8px;\n          }\n\n          .tableContainer .table .comparision .dotWrapper span:last-child {\n            margin-bottom: 0;\n          }\n\n      .tableContainer .table .comparision:nth-child(1) {\n        background: none;\n        margin-top: 16px;\n        padding: 8px 0;\n      }\n\n      .tableContainer .table .comparision:nth-child(2) {\n        grid-row: 1/2;\n        background: none;\n        padding: 8px 0;\n      }\n\n      .tableContainer .table .comparision:nth-child(3) {\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .tableContainer .table .comparision:nth-child(4) {\n        grid-row: 2/3;\n        font-size: 16px;\n        padding: 8px 12px;\n        line-height: normal;\n      }\n\n      .tableContainer .table .comparision:nth-child(6) {\n        grid-row: 3/4;\n      }\n\n      .tableContainer .table .comparision:nth-child(8) {\n        grid-row: 4/5;\n      }\n\n      .tableContainer .table .comparision:nth-child(10) {\n        grid-row: 5/6;\n      }\n\n      .tableContainer .table .comparision:nth-child(12) {\n        grid-row: 6/7;\n      }\n\n      .tableContainer .table .comparision:nth-child(13) {\n        border-bottom: none;\n      }\n\n      .tableContainer .table .comparision:nth-child(14) {\n        grid-row: 7/8;\n        border-bottom: none;\n      }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"breadcrum": "Teach-breadcrum-11xX5",
	"toggle_outer": "Teach-toggle_outer-VIsVE",
	"toggle_on": "Teach-toggle_on-2JbJd",
	"toggle_inner": "Teach-toggle_inner-3gUru",
	"headerbackgroundmobile": "Teach-headerbackgroundmobile-2Z0QY",
	"authorimgbox": "Teach-authorimgbox-28-2r",
	"contentPart": "Teach-contentPart-1lSgK",
	"topicImage": "Teach-topicImage-1CM7j",
	"content": "Teach-content-s_zCd",
	"section_title": "Teach-section_title-2nyBd",
	"textcontent": "Teach-textcontent-3s5Dz",
	"customers_container": "Teach-customers_container-3dTy8",
	"customer_review": "Teach-customer_review-3Q1HR",
	"customerLogo": "Teach-customerLogo-xFAq9",
	"sriChaitanyaText": "Teach-sriChaitanyaText-1HRt2",
	"authorWrapper": "Teach-authorWrapper-2FTu7",
	"author_title": "Teach-author_title-3HNCy",
	"about_author": "Teach-about_author-3sn5h",
	"allcustomers": "Teach-allcustomers-2b8NO",
	"availableContainer": "Teach-availableContainer-2e4mP",
	"availableRow": "Teach-availableRow-3SeEQ",
	"desktopImage": "Teach-desktopImage-n9mcs",
	"availableTitle": "Teach-availableTitle-28MiM",
	"available": "Teach-available-k7Ani",
	"availableContentSection": "Teach-availableContentSection-JZlaF",
	"platformContainer": "Teach-platformContainer-1IANc",
	"platform": "Teach-platform-1GcJZ",
	"platformOs": "Teach-platformOs-2DF6E",
	"row": "Teach-row-1VSyr",
	"store": "Teach-store-IZcDQ",
	"playstore": "Teach-playstore-PXfhG",
	"appstore": "Teach-appstore-2HMPf",
	"displayClients": "Teach-displayClients-1bg_Q",
	"imagePart": "Teach-imagePart-3NAi0",
	"emptyCard": "Teach-emptyCard-v-GO_",
	"topCard": "Teach-topCard-2ZuD4",
	"teachContainer": "Teach-teachContainer-2Q-YB",
	"heading": "Teach-heading-1K5as",
	"learningText": "Teach-learningText-3aV7N",
	"buttonwrapper": "Teach-buttonwrapper-i_jdi",
	"requestDemo": "Teach-requestDemo-AboHC",
	"whatsappwrapper": "Teach-whatsappwrapper-3jpz4",
	"whatsapp": "Teach-whatsapp-34spS",
	"topSection": "Teach-topSection-1ZZr-",
	"teachSection": "Teach-teachSection-33snH",
	"teachImgBox": "Teach-teachImgBox-mkz67",
	"teachSectionName": "Teach-teachSectionName-1AdS4",
	"contentText": "Teach-contentText-3eX56",
	"downloadApp": "Teach-downloadApp-3tWrv",
	"downloadText": "Teach-downloadText-2YOcN",
	"playStoreIcon": "Teach-playStoreIcon-3YdD8",
	"clientsWrapper": "Teach-clientsWrapper-3fJYq",
	"client": "Teach-client-Ii9JF",
	"achievedContainer": "Teach-achievedContainer-Aa5ks",
	"achievedHeading": "Teach-achievedHeading-2yxR4",
	"achievedRow": "Teach-achievedRow-AXwBV",
	"card": "Teach-card-6zL17",
	"achievedProfile": "Teach-achievedProfile-3w5Nj",
	"clients": "Teach-clients-1hUFi",
	"students": "Teach-students-PAFZG",
	"tests": "Teach-tests-27DEs",
	"questions": "Teach-questions-WBnlD",
	"highlight": "Teach-highlight-3llRS",
	"questionsHighlight": "Teach-questionsHighlight-37La9",
	"testsHighlight": "Teach-testsHighlight-2Hb9O",
	"studentsHighlight": "Teach-studentsHighlight-2H5F_",
	"clientsHighlight": "Teach-clientsHighlight-1ankD",
	"subText": "Teach-subText-3IRHK",
	"section_container_reverse": "Teach-section_container_reverse-1yEDY",
	"section_contents": "Teach-section_contents-1NRDr",
	"section_container": "Teach-section_container-cyyTN",
	"teachContent": "Teach-teachContent-19QTk",
	"getranks": "Teach-getranks-3JcXa",
	"zoom": "Teach-zoom-3RXu-",
	"bottomCircle": "Teach-bottomCircle-1QU8r",
	"topCircle": "Teach-topCircle-2waHP",
	"livepart": "Teach-livepart-3TKC8",
	"assignmentpart": "Teach-assignmentpart-20DkI",
	"doubtpart": "Teach-doubtpart-2NkyQ",
	"tableContainer": "Teach-tableContainer-32ppr",
	"table": "Teach-table-y9qtU",
	"comparision": "Teach-comparision-WqBQk",
	"comparisionWrapper": "Teach-comparisionWrapper-MJ1Qz",
	"comparisionText": "Teach-comparisionText-3FmhP",
	"box": "Teach-box-287sD",
	"positive": "Teach-positive-2nQ7l",
	"negative": "Teach-negative-3D6w2",
	"dotWrapper": "Teach-dotWrapper-ANQj6",
	"dot": "Teach-dot-fu9Ct",
	"red": "Teach-red-zeFql",
	"tableData": "Teach-tableData-3nntZ"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/Teach.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Teach_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/Teach/Teach.scss");
/* harmony import */ var _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Teach_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/Teach.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var Teach =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Teach, _React$Component);

  function Teach(props) {
    var _this;

    _classCallCheck(this, Teach);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Teach).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "displayToggle", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        role: "presentation",
        className: _this.state.liveToggle ? "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_outer, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_on) : _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_outer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.toggle_inner,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayTeachSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.breadcrum,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }, "Home / ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        },
        __self: this
      }, "Live Classes")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachImgBox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/live_colored.svg",
        alt: "live-classes",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachSectionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      }, "Live Classes"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, "Maximize the\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.learningText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, "learning\xA0"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/live classes/underscore_for_title.png",
        alt: "underscore",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      })), "of your students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }, "A powerful teaching module that facilitates distance learning, promote positive student behaviour, engage parents in the learning process and reduce teacher workload all from one easy-to-use platform."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.buttonwrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.requestDemo,
        onClick: _this.handleShowTrial,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, "GET SUBSCRIPTION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsappwrapper,
        href: "https://api.whatsapp.com/send?phone=918800764909&text=Hi GetRanks, I would like to know more about your Online Platform.",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whatsapp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/whatsapp_logo.svg",
        alt: "whatsapp",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadApp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.downloadText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }, "Download the app"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playStoreIcon,
        src: "/images/home/platforms/playStore.png",
        alt: "play_store",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerbackgroundmobile,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/teach_header_human.webp",
        alt: "mobile_background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayClients", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.displayClients,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["HOME_CLIENTS_UPDATED"].map(function (client) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.client,
          key: client.id,
          src: client.icon,
          alt: "clinet",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 114
          },
          __self: this
        });
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/customers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, "click here for more...")));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAchievedSoFar", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedHeading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clients),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/clients.svg",
        alt: "profile",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.clientsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.students),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/students.svg",
        alt: "students",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.studentsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 161
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/tests.svg",
        alt: "tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.testsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167
        },
        __self: this
      }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.achievedProfile, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questions),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/questions.svg",
        alt: "questions",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.highlight, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.questionsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }, "Questions Solved"))));
    });

    _defineProperty(_assertThisInitialized(_this), "liveToggleHandler", function () {
      _this.setState({
        liveToggle: !_this.state.liveToggle
      });
    });

    _defineProperty(_assertThisInitialized(_this), "lectureToggleHandler", function () {
      _this.setState({
        lectureToggle: !_this.state.lectureToggle
      });
    });

    _defineProperty(_assertThisInitialized(_this), "assignmentToggleHandler", function () {
      _this.setState({
        assignmentToggle: !_this.state.assignmentToggle
      });
    });

    _defineProperty(_assertThisInitialized(_this), "doubtToggleHandler", function () {
      _this.setState({
        doubtToggle: !_this.state.doubtToggle
      });
    });

    _defineProperty(_assertThisInitialized(_this), "displayLiveSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topicImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Teach/old_Live.svg",
        alt: "live",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, "Live"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teachContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.getranks,
        src: "/images/icons/getranks copy.svg",
        alt: "getranks",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }), "\xA0+\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.zoom,
        src: "/images/icons/Zoom-Logo.png",
        alt: "zoom-logo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 222
        },
        __self: this
      }), "\xA0Integration Zoom is integrated with GetRanks. All Benefits-In, All Limitations-out"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }, "No matter where education takes place, \"Egnify Live with Zoom \" can help engage students,faculty and staff for learning, collaboration and administration."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.livepart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 237
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/live.webp",
        alt: "live",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayIndependence", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 256
        },
        __self: this
      }, "Independence:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      }, "Your Zoom account will be used. All the features and provisions of your Zoom account shall be available with an added advantage of login restrictions only for selected students."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 264
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/independence.svg",
        alt: "independence",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 267
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayFlexibility", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 279
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 280
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 282
        },
        __self: this
      }, "Flexibility:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 283
        },
        __self: this
      }, "Any number of Zoom accounts can be used. Different Zoom accounts can be mapped with different Class Sessions thus allowing multiple teachers to use their personal accounts."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 290
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 291
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 292
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/flexibility.webp",
        alt: "flexibility",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 293
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 296
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displaySecureAccess", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 304
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 305
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 307
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }, "Secure Access:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309
        },
        __self: this
      }, "Only selected students who have access to the App can now access Zoom class, no need to share Meeting ID everytime, No random access."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 316
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 317
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 318
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/live.webp",
        alt: "secureAccess",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 319
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 321
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 322
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayMultipleUses", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 330
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 331
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 332
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 333
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 334
        },
        __self: this
      }, "Multiple uses of Zoom:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 335
        },
        __self: this
      }, "Zoom facility available for live Class and Doubts modules. Soon Zoom is being integrated to the online Exam module for online Exam proctoring."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 342
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 343
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 344
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/multiple_uses.webp",
        alt: "multi-uses",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 345
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayOrganisedContent", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 356
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 357
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 358
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 359
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 360
        },
        __self: this
      }, "Organised Content:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 361
        },
        __self: this
      }, "Now you dont have to invite students by sending separate meeting notifications and reminders to parents. This will allow all the Zoom classes to be listed and seen on the students dashboard scheduled as a part of curriculum."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 369
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 371
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/organised_content.webp",
        alt: "secureAccess",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 372
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 377
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 378
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAllowReplay", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 386
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 387
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 388
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 389
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 390
        },
        __self: this
      }, "Allow Replay:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 391
        },
        __self: this
      }, "Zoom meeting recordings can be uploaded for later replay by students."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.doubtpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 397
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 398
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 399
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/allow_replay.webp",
        alt: "multi-uses",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 400
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 402
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 403
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAttendanceRecord", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 411
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 412
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 413
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 414
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 415
        },
        __self: this
      }, "Attendance Record:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 416
        },
        __self: this
      }, "Now its possible to maintain organized record of attendance in Zoom Class along with other attendance and digital learning activity record of the student."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imagePart, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.assignmentpart),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 423
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 424
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 425
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Teach/flexibility.webp",
        alt: "secureAccess",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 426
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 428
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 429
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayDifference", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 437
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 438
        },
        __self: this
      }, "GetRanks + Zoom is", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 440
        },
        __self: this
      }), " more powerful than Zoom alone"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.table,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 442
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 443
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 444
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 445
        },
        __self: this
      }, "Negative Points"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.box, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.negative),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 446
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 449
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 450
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparisionText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 451
        },
        __self: this
      }, "Positive Points"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.box, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.positive),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 452
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 455
        },
        __self: this
      }, "Zoom only"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 456
        },
        __self: this
      }, "GeRanks+Zoom"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 457
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 458
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 459
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 461
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 462
        },
        __self: this
      }, "Nothing Personalized. You become a part of a portal and popularize it. Your Brand identity is lost."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 466
        },
        __self: this
      }, "Student data & Content is shared on the portal"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 469
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 470
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 471
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 473
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, "Personalized APP with Institute Name and Logo strengthens your Brand"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 478
        },
        __self: this
      }, "Personalized System that you can also buy and install on your server"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 482
        },
        __self: this
      }, "Secure Content can be viewed by Users only inside the APP"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 487
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 488
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 489
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 491
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 492
        },
        __self: this
      }, "Does not allow replay. This saves bandwidth and reduce cost by compromising the essential need of Self Learning"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }, "If internet is lost during session, that much session is missed. Student is never able to view it back. No provision to go back during live session"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 501
        },
        __self: this
      }, "Nothing is stored online. Live session video is Downloadable and freely distributable as link or file. Manage your files locally after downloading"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 508
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 509
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 510
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 512
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 513
        },
        __self: this
      }, "Lectures are auto recorded during live session and available for replay multiple times"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 517
        },
        __self: this
      }, "Student will not miss anything if internet is lost, he can start again and watch where he left. Possible to go back in live lecture to review a topic and then return to live moment"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 522
        },
        __self: this
      }, "Videos are stored in Safe Google Servers in your full access, no hassle of maintaining locally"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 528
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 529
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 530
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 532
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 533
        },
        __self: this
      }, "No attendance and participation Record"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 536
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 537
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 538
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 540
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 541
        },
        __self: this
      }, "Attendance record of every activity is maintained."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 544
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 545
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 546
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 548
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 549
        },
        __self: this
      }, "There is cost per broadcaster"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 552
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 553
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 554
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 556
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 557
        },
        __self: this
      }, "Multiple teachers can cast using single license"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 560
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 561
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 562
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 564
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 565
        },
        __self: this
      }, "No control over user identity. Users can enter with random names like \u2018rocky123\u2019 and create nuisance"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.comparision,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 571
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dotWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 572
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dot, " ").concat(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.red),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 573
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tableData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 575
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 576
        },
        __self: this
      }, "Restricted Login and Access Control: User cannot send any indecent message because registered name and number of sender is visible")))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayCustomers", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customers_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 587
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customer_review,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 588
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.customerLogo,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 589
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "images/customers/Frame 1135.jpg",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 590
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sriChaitanyaText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 592
        },
        __self: this
      }, "\u201CLorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis\u201D"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.authorWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 598
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.author_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 599
        },
        __self: this
      }, "Srujan Sagar"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.about_author,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 600
        },
        __self: this
      }, "Acadamic head,", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 601
        },
        __self: this
      }, "Sri Chaitanya"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.allcustomers,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 604
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 605
        },
        __self: this
      }, "see all customers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "images/icons/arrowRight.png",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 606
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.authorimgbox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 609
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "images/icons/srujanSagar.png",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 610
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAvailableOn", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 615
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 616
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableContentSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 617
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.availableTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 618
        },
        __self: this
      }, "We\u2019re ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 619
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.available,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 620
        },
        __self: this
      }, "available"), " on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.row,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 622
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_3__["PLATFORMS"].map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 624
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: item.icon,
          alt: "",
          height: "40px",
          width: "40px",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 625
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platform,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 626
          },
          __self: this
        }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.platformOs,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 627
          },
          __self: this
        }, item.available));
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.store,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 631
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/playStore.webp",
        alt: "Play Store",
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.playstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 632
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/appStore.webp",
        alt: "App Store",
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.appstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 637
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a.desktopImage,
        src: "/images/home/platforms/platforms.png",
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 645
        },
        __self: this
      })));
    });

    _this.state = {
      liveToggle: false,
      lectureToggle: false,
      assignmentToggle: false,
      doubtToggle: false
    };
    return _this;
  } // Toggle Button Code


  _createClass(Teach, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 655
        },
        __self: this
      }, this.displayTeachSection(), this.displayClients(), this.displayAchievedSoFar(), this.displayLiveSection(), this.displayIndependence(), this.displayFlexibility(), this.displaySecureAccess(), this.displayMultipleUses(), this.displayOrganisedContent(), this.displayAllowReplay(), this.displayAttendanceRecord(), this.displayDifference(), this.displayAvailableOn());
    }
  }]);

  return Teach;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Teach_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Teach));

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/Teach.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Teach/Teach.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Teach/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Teach__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/Teach/Teach.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Teach/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'GetRanks by Egnify: Assessment & Analytics Platform',
              chunk: ['Live Classes'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Teach__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGl2ZSBDbGFzc2VzLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVhY2gvVGVhY2guc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZWFjaC9UZWFjaC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZWFjaC9UZWFjaC5zY3NzPzY1MDkiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVhY2gvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5UZWFjaC1icmVhZGNydW0tMTF4WDUge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxufVxcblxcbi5UZWFjaC1icmVhZGNydW0tMTF4WDUgc3BhbiB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uVGVhY2gtdG9nZ2xlX291dGVyLVZJc1ZFIHtcXG4gIHdpZHRoOiAzMnB4O1xcbiAgaGVpZ2h0OiAxNy44cHg7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIHBhZGRpbmc6IDJweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiAtMTBweDtcXG59XFxuXFxuLlRlYWNoLXRvZ2dsZV9vbi0ySmJKZCB7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLlRlYWNoLXRvZ2dsZV9pbm5lci0zZ1VydSB7XFxuICB3aWR0aDogMTRweDtcXG4gIGhlaWdodDogMTRweDtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4vKiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAzNnB4O1xcbiAgYm90dG9tOiA4N3B4O1xcbiAgcmlnaHQ6IDU2MHB4O1xcblxcbiAgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIH1cXG59ICovXFxuXFxuLlRlYWNoLWhlYWRlcmJhY2tncm91bmRtb2JpbGUtMlowUVkge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAwO1xcbiAgcmlnaHQ6IDY0cHg7XFxuICB3aWR0aDogNDQ1cHg7XFxuICBoZWlnaHQ6IDU4NHB4O1xcbn1cXG5cXG4uVGVhY2gtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0yWjBRWSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbi5UZWFjaC1hdXRob3JpbWdib3gtMjgtMnIge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4uVGVhY2gtYXV0aG9yaW1nYm94LTI4LTJyIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNjQwcHg7XFxuICAgIG1heC1oZWlnaHQ6IDU2MHB4O1xcbiAgICBqdXN0aWZ5LXNlbGY6IGZsZXgtZW5kO1xcbiAgfVxcblxcbi8qIC52aWV3bW9yZS5tb2JpbGUge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDMwcHg7XFxuXFxuICBpbWcge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICBtYXJnaW4tdG9wOiAxcHg7XFxuICB9XFxufSAqL1xcblxcbi5UZWFjaC1jb250ZW50UGFydC0xbFNnSyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgd2lkdGg6IDYwJTtcXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4uVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLXRvcGljSW1hZ2UtMUNNN2oge1xcbiAgICB3aWR0aDogNzJweDtcXG4gICAgaGVpZ2h0OiA3MnB4O1xcbiAgICBwYWRkaW5nOiAxNnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgIGJhY2tncm91bmQ6ICNmZmY7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgfVxcblxcbi5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtdG9waWNJbWFnZS0xQ003aiBpbWcge1xcbiAgICAgIHdpZHRoOiA0MnB4O1xcbiAgICAgIGhlaWdodDogNDJweDtcXG4gICAgfVxcblxcbi5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtY29udGVudC1zX3pDZCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtaW4td2lkdGg6IDI5MHB4O1xcbiAgICBtYXgtd2lkdGg6IDU0MHB4O1xcblxcbiAgICAvKiAudmlld21vcmUge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBjb2xvcjogIzAwNzZmZjtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG5cXG4gICAgICBpbWcge1xcbiAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICBoZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgIH1cXG4gICAgfSAqL1xcbiAgfVxcblxcbi5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtY29udGVudC1zX3pDZCAuVGVhY2gtc2VjdGlvbl90aXRsZS0ybnlCZCB7XFxuICAgICAgZGlzcGxheTogYmxvY2s7XFxuICAgICAgbWFyZ2luOiAxMnB4IDA7XFxuICAgICAgZm9udC1zaXplOiA0MHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgIGxldHRlci1zcGFjaW5nOiAtMS4ycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgfVxcblxcbi5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtY29udGVudC1zX3pDZCAuVGVhY2gtdGV4dGNvbnRlbnQtM3M1RHoge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICBtYXgtd2lkdGg6IDM3MHB4O1xcbiAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTgge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGhlaWdodDogNTYwcHg7XFxufVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIge1xcbiAgICAtbXMtZmxleC1vcmRlcjogMDtcXG4gICAgICAgIG9yZGVyOiAwO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZzogNDhweCA2NHB4O1xcbiAgICB3aWR0aDogNTAlO1xcbiAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWN1c3RvbWVyTG9nby14RkFxOSB7XFxuICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIGhlaWdodDogODBweDtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWN1c3RvbWVyTG9nby14RkFxOSBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtc3JpQ2hhaXRhbnlhVGV4dC0xSFJ0MiB7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtYXV0aG9yV3JhcHBlci0yRlR1NyB7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgfVxcblxcbi5UZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4IC5UZWFjaC1jdXN0b21lcl9yZXZpZXctM1ExSFIgLlRlYWNoLWF1dGhvcldyYXBwZXItMkZUdTcgLlRlYWNoLWF1dGhvcl90aXRsZS0zSE5DeSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtYXV0aG9yV3JhcHBlci0yRlR1NyAuVGVhY2gtYWJvdXRfYXV0aG9yLTNzbjVoIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gICAgICB9XFxuXFxuLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtYWxsY3VzdG9tZXJzLTJiOE5PIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1hbGxjdXN0b21lcnMtMmI4Tk8gaW1nIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDNweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICAgICAgICB3aWR0aDogMjBweDtcXG4gICAgICAgIGhlaWdodDogMjBweDtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBwYWRkaW5nOiA4MHB4IDExM3B4IDAgMTY0cHg7XFxuICBwYWRkaW5nOiA1cmVtIDExM3B4IDAgMTY0cHg7XFxuICAvLyBtYXgtd2lkdGg6IDEzMDBweDtcXG59XFxuXFxuLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIHtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLWRlc2t0b3BJbWFnZS1uOW1jcyB7XFxuICAgICAgd2lkdGg6IDY0OHB4O1xcbiAgICAgIGhlaWdodDogMzI4cHg7XFxuICAgICAgbWFyZ2luLXRvcDogMzVweDtcXG4gICAgfVxcblxcbi5UZWFjaC1hdmFpbGFibGVUaXRsZS0yOE1pTSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uVGVhY2gtYXZhaWxhYmxlLWs3QW5pIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uVGVhY2gtYXZhaWxhYmxlQ29udGVudFNlY3Rpb24tSlpsYUYge1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbn1cXG5cXG4uVGVhY2gtcGxhdGZvcm1Db250YWluZXItMUlBTmMge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIG1hcmdpbi1yaWdodDogMnJlbTtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG59XFxuXFxuLlRlYWNoLXBsYXRmb3JtLTFHY0paIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW46IDhweCAwIDRweCAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5UZWFjaC1wbGF0Zm9ybU9zLTJERjZFIHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLlRlYWNoLXJvdy0xVlN5ciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIG1hcmdpbjogNDhweCAwIDMycHggMDtcXG4gIG1hcmdpbjogM3JlbSAwIDJyZW0gMDtcXG59XFxuXFxuLlRlYWNoLXN0b3JlLUlaY0RRIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDRyZW07XFxufVxcblxcbi5UZWFjaC1zdG9yZS1JWmNEUSAuVGVhY2gtcGxheXN0b3JlLVBYZmhHIHtcXG4gICAgd2lkdGg6IDE0MHB4O1xcbiAgICBoZWlnaHQ6IDQycHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIH1cXG5cXG4uVGVhY2gtc3RvcmUtSVpjRFEgLlRlYWNoLWFwcHN0b3JlLTJITVBmIHtcXG4gICAgd2lkdGg6IDE0MHB4O1xcbiAgICBoZWlnaHQ6IDQycHg7XFxuICB9XFxuXFxuLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDExNTJweDtcXG4gIG1hcmdpbjogMTJweCBhdXRvIDA7XFxufVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtdG9wQ2FyZC0yWnVENCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIHtcXG4gIGhlaWdodDogNjgwcHg7XFxuICBwYWRkaW5nOiAyNHB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlY2UwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgY3Vyc29yOiBhbGlhcztcXG59XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIHtcXG4gICAgZm9udC1zaXplOiA0OHB4O1xcbiAgICBsaW5lLWhlaWdodDogNjZweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIG1heC13aWR0aDogNTQycHg7XFxuICAgIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWhlYWRpbmctMUs1YXMgLlRlYWNoLWxlYXJuaW5nVGV4dC0zYVY3TiB7XFxuICAgICAgd2lkdGg6IDE4NXB4O1xcbiAgICAgIGRpc3BsYXk6IGlubGluZTtcXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWhlYWRpbmctMUs1YXMgLlRlYWNoLWxlYXJuaW5nVGV4dC0zYVY3TiBpbWcge1xcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgICAgYm90dG9tOiAtOHB4O1xcbiAgICAgICAgbGVmdDogLThweDtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiA4cHg7XFxuICAgICAgICBtYXgtd2lkdGg6IDIyNXB4O1xcbiAgICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkge1xcbiAgICBtYXJnaW4tdG9wOiA2NHB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkgLlRlYWNoLXJlcXVlc3REZW1vLUFib0hDIHtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgICBjb2xvcjogIzAwMDtcXG4gICAgICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IG1heC1jb250ZW50O1xcbiAgICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIC5UZWFjaC13aGF0c2FwcHdyYXBwZXItM2pwejQge1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IC5UZWFjaC13aGF0c2FwcC0zNHNwUyB7XFxuICAgICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYiAhaW1wb3J0YW50O1xcbiAgICAgICAgbWFyZ2luOiAwIDhweCAwIDMycHg7XFxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gICAgICAgIG9wYWNpdHk6IDAuNjtcXG4gICAgICB9XFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIC5UZWFjaC13aGF0c2FwcHdyYXBwZXItM2pwejQgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgIH1cXG5cXG4vKiAuYWN0aW9uc1dyYXBwZXIge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJvdHRvbTogMjZweDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuXFxuICAgIC5hY3Rpb24ge1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcblxcbiAgICAgIHNwYW4ge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgIG9wYWNpdHk6IDAuNztcXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWN0aW9uaW1nYm94IHtcXG4gICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgICAgICBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA5cHg7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC10b3BTZWN0aW9uLTFaWnItIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiA1NnB4O1xcblxcbiAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLWxlZnQ6IDMwJTtcXG5cXG4gICAgICBzcGFuIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xcbiAgICAgIH1cXG4gICAgfSAqL1xcbiAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSAuVGVhY2gtdGVhY2hTZWN0aW9uLTMzc25IIHtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCAuVGVhY2gtdGVhY2hJbWdCb3gtbWt6Njcge1xcbiAgICAgICAgd2lkdGg6IDQwcHg7XFxuICAgICAgICBoZWlnaHQ6IDQwcHg7XFxuICAgICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSAuVGVhY2gtdGVhY2hTZWN0aW9uLTMzc25IIC5UZWFjaC10ZWFjaEltZ0JveC1ta3o2NyBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSAuVGVhY2gtdGVhY2hTZWN0aW9uLTMzc25IIC5UZWFjaC10ZWFjaFNlY3Rpb25OYW1lLTFBZFM0IHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMzBweDtcXG4gICAgICAgIGNvbG9yOiAjZmY2NDAwO1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICAgICAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtY29udGVudFRleHQtM2VYNTYge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMCAwO1xcbiAgICBtYXgtd2lkdGg6IDY4N3B4O1xcbiAgfVxcblxcbi5UZWFjaC1kb3dubG9hZEFwcC0zdFdydiB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi5UZWFjaC1kb3dubG9hZEFwcC0zdFdydiAuVGVhY2gtZG93bmxvYWRUZXh0LTJZT2NOIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgb3BhY2l0eTogMC43O1xcbn1cXG5cXG4uVGVhY2gtZG93bmxvYWRBcHAtM3RXcnYgLlRlYWNoLXBsYXlTdG9yZUljb24tM1lkRDgge1xcbiAgd2lkdGg6IDEzMnB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1Ege1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiA0NjRweDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5UZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUSBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1EgLlRlYWNoLWNsaWVudHNXcmFwcGVyLTNmSllxIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5UZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUSAuVGVhY2gtY2xpZW50c1dyYXBwZXItM2ZKWXEgLlRlYWNoLWNsaWVudC1JaTlKRiB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1Egc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1Egc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3Mge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9uZXdfY29uZmV0dGkuc3ZnJyk7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRIZWFkaW5nLTJ5eFI0IHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICBnYXA6IDE2cHg7XFxuICBnYXA6IDFyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcge1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiAyMDJweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHBhZGRpbmc6IDEuNXJlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtYWNoaWV2ZWRQcm9maWxlLTN3NU5qIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtYWNoaWV2ZWRQcm9maWxlLTN3NU5qLlRlYWNoLWNsaWVudHMtMWhVRmkge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtYWNoaWV2ZWRQcm9maWxlLTN3NU5qLlRlYWNoLXN0dWRlbnRzLVBBRlpHIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2VmZmU7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWFjaGlldmVkUHJvZmlsZS0zdzVOai5UZWFjaC10ZXN0cy0yN0RFcyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1hY2hpZXZlZFByb2ZpbGUtM3c1TmouVGVhY2gtcXVlc3Rpb25zLVdCbmxEIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWhpZ2hsaWdodC0zbGxSUyB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1oaWdobGlnaHQtM2xsUlMuVGVhY2gtcXVlc3Rpb25zSGlnaGxpZ2h0LTM3TGE5IHtcXG4gIGNvbG9yOiAjMDBhYzI2O1xcbn1cXG5cXG4uVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3IC5UZWFjaC1oaWdobGlnaHQtM2xsUlMuVGVhY2gtdGVzdHNIaWdobGlnaHQtMkhiOU8ge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLWhpZ2hsaWdodC0zbGxSUy5UZWFjaC1zdHVkZW50c0hpZ2hsaWdodC0ySDVGXyB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyAuVGVhY2gtaGlnaGxpZ2h0LTNsbFJTLlRlYWNoLWNsaWVudHNIaWdobGlnaHQtMWFua0Qge1xcbiAgY29sb3I6ICNmNjA7XFxufVxcblxcbi5UZWFjaC1hY2hpZXZlZENvbnRhaW5lci1BYTVrcyAuVGVhY2gtYWNoaWV2ZWRSb3ctQVh3QlYgLlRlYWNoLWNhcmQtNnpMMTcgLlRlYWNoLXN1YlRleHQtM0lSSEsge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi8qIC50b2dnbGVBdFJpZ2h0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDAgNjRweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZUF0TGVmdCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59ICovXFxuXFxuLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMXlFRFkge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwYWRkaW5nOiAxODRweCAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTF5RURZIC5UZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDAgNjRweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gIH1cXG5cXG4uVGVhY2gtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xeUVEWSAuVGVhY2gtc2VjdGlvbl9jb250ZW50cy0xTlJEciAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIH1cXG5cXG4uVGVhY2gtc2VjdGlvbl9jb250YWluZXItY3l5VE4ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBwYWRkaW5nOiAxODRweCAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lci1jeXlUTiAuVGVhY2gtc2VjdGlvbl9jb250ZW50cy0xTlJEciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZzogMCA2NHB4O1xcbiAgICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRlbnQtMTlRVGsge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1heC13aWR0aDogMzM3cHg7XFxuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLlRlYWNoLXRlYWNoQ29udGVudC0xOVFUayAuVGVhY2gtZ2V0cmFua3MtM0pjWGEge1xcbiAgICB3aWR0aDogODlweDtcXG4gICAgaGVpZ2h0OiAyNnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAtNXB4O1xcbiAgfVxcblxcbi5UZWFjaC10ZWFjaENvbnRlbnQtMTlRVGsgLlRlYWNoLXpvb20tM1JYdS0ge1xcbiAgICB3aWR0aDogNjRweDtcXG4gICAgaGVpZ2h0OiAxOHB4O1xcbiAgfVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAge1xcbiAgd2lkdGg6IDQwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMCAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIHtcXG4gIHdpZHRoOiA2MDZweDtcXG4gIGhlaWdodDogMzQxcHg7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDMycHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENhcmQtMlp1RDQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgei1pbmRleDogMTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG59XFxuXFxuLlRlYWNoLWltYWdlUGFydC0zTkFpMCAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiA0OHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwIC5UZWFjaC10b3BDaXJjbGUtMndhSFAge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDY0cHg7XFxuICBoZWlnaHQ6IDY0cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxufVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtbGl2ZXBhcnQtM1RLQzggLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDc5cHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWxpdmVwYXJ0LTNUS0M4IC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENpcmNsZS0yd2FIUCB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgdG9wOiAtNDRweDtcXG4gICAgICByaWdodDogNTBweDtcXG4gICAgfVxcblxcbi5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtYXNzaWdubWVudHBhcnQtMjBEa0kgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDQ4cHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWFzc2lnbm1lbnRwYXJ0LTIwRGtJIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENpcmNsZS0yd2FIUCB7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgdG9wOiAtNDRweDtcXG4gICAgICByaWdodDogNjdweDtcXG4gICAgICBvcGFjaXR5OiAwLjM7XFxuICAgIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWRvdWJ0cGFydC0yTmt5USAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC1ib3R0b21DaXJjbGUtMVFVOHIge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICAgIGJvdHRvbTogLTQ0cHg7XFxuICAgICAgbGVmdDogNjhweDtcXG4gICAgICBvcGFjaXR5OiAwLjI7XFxuICAgIH1cXG5cXG4uVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWRvdWJ0cGFydC0yTmt5USAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC10b3BDaXJjbGUtMndhSFAge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDM2cHg7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICB9XFxuXFxuLlRlYWNoLWFsbGN1c3RvbWVycy0yYjhOTyBwIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZWFjaC1jb250ZW50LXNfekNkIHAgcCB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIHtcXG4gIHBhZGRpbmc6IDU2cHggMDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciBoMSB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUge1xcbiAgICB3aWR0aDogOTU2cHg7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIHtcXG4gICAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgICAgZm9udC1zaXplOiAxOHB4O1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigwLCAwLCAwLCAwLjEpO1xcbiAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiB7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWNvbXBhcmlzaW9uVGV4dC0zRm1oUCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWJveC0yODdzRCB7XFxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiAuVGVhY2gtYm94LTI4N3NELlRlYWNoLXBvc2l0aXZlLTJuUTdsIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NmYztcXG4gICAgICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtY29tcGFyaXNpb25XcmFwcGVyLU1KMVF6IC5UZWFjaC1ib3gtMjg3c0QuVGVhY2gtbmVnYXRpdmUtM0Q2dzIge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkNmQ2O1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1kb3RXcmFwcGVyLUFOUWo2IHtcXG4gICAgICAgIHdpZHRoOiA1JTtcXG4gICAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWRvdFdyYXBwZXItQU5RajYgLlRlYWNoLWRvdC1mdTlDdCB7XFxuICAgICAgICAgIHdpZHRoOiA4cHg7XFxuICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICAgIG1hcmdpbi10b3A6IDEycHg7XFxuICAgICAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWRvdFdyYXBwZXItQU5RajYgLlRlYWNoLWRvdC1mdTlDdC5UZWFjaC1yZWQtemVGcWwge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMGMwO1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC10YWJsZURhdGEtM25udFoge1xcbiAgICAgICAgd2lkdGg6IDk1JTtcXG4gICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC10YWJsZURhdGEtM25udFogc3BhbiB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICAgICAgfVxcblxcbi5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZChvZGQpIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmViO1xcbiAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKGV2ZW4pIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmViO1xcbiAgICB9XFxuXFxuLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDEpLFxcbiAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMikge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIH1cXG5cXG4uVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMyksXFxuICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCg0KSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgzNywgNDAsIDQzLCAwLjEpO1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xcbiAgLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIge1xcbiAgICBwYWRkaW5nOiAwIDQwcHg7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XFxuICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIHtcXG4gICAgcGFkZGluZzogNXJlbSA2NHB4IDAgNjRweDtcXG4gIH1cXG4gICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5UZWFjaC1icmVhZGNydW0tMTF4WDUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLyogLnRvZ2dsZV9vdXRlciB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDE4cHg7XFxuICAgIG1hcmdpbi10b3A6IC04cHg7XFxuICB9XFxuXFxuICAudG9nZ2xlX2lubmVyIHtcXG4gICAgd2lkdGg6IDE0cHg7XFxuICAgIGhlaWdodDogMTRweDtcXG4gIH1cXG5cXG4gIC50b2dnbGVBdFJpZ2h0IHtcXG4gICAgcGFkZGluZzogMyUgNSUgMCA1JTtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAudG9nZ2xlQXRMZWZ0IHtcXG4gICAgcGFkZGluZzogMyUgNSUgMCA1JTtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9ICovXFxuXFxuICAvKiAubW91c2VpY29uIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH0gKi9cXG5cXG4gIC5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lci1jeXlUTiB7XFxuICAgIHBhZGRpbmc6IDMycHggMDtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyLWN5eVROIC5UZWFjaC1zZWN0aW9uX2NvbnRlbnRzLTFOUkRyIHtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBwYWRkaW5nOiAwIDE2cHg7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtc2VjdGlvbl9jb250YWluZXItY3l5VE4gLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIgLlRlYWNoLXNlY3Rpb25fdGl0bGUtMm55QmQge1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICAgIG1hcmdpbjogMTZweCAwO1xcbiAgICAgIH1cXG5cXG4gIC5UZWFjaC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTF5RURZIHtcXG4gICAgcGFkZGluZzogMzJweCAwO1xcbiAgfVxcblxcbiAgICAuVGVhY2gtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xeUVEWSAuVGVhY2gtc2VjdGlvbl9jb250ZW50cy0xTlJEciB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgcGFkZGluZzogMCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMXlFRFkgLlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHIgLlRlYWNoLXNlY3Rpb25fdGl0bGUtMm55QmQge1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICAgIG1hcmdpbjogMTZweCAwO1xcbiAgICAgIH1cXG5cXG4gIC5UZWFjaC1jb250ZW50UGFydC0xbFNnSyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLXRvcGljSW1hZ2UtMUNNN2oge1xcbiAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgcGFkZGluZzogMTBweDtcXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtdG9waWNJbWFnZS0xQ003aiBpbWcge1xcbiAgICAgICAgd2lkdGg6IDI4cHg7XFxuICAgICAgICBoZWlnaHQ6IDI4cHg7XFxuICAgICAgfVxcblxcbiAgICAuVGVhY2gtY29udGVudFBhcnQtMWxTZ0sgLlRlYWNoLWNvbnRlbnQtc196Q2Qge1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG5cXG4gICAgICAvKiAudmlld21vcmUge1xcbiAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH0gKi9cXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtY29udGVudC1zX3pDZCAuVGVhY2gtc2VjdGlvbl90aXRsZS0ybnlCZCB7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1jb250ZW50UGFydC0xbFNnSyAuVGVhY2gtY29udGVudC1zX3pDZCAuVGVhY2gtdGV4dGNvbnRlbnQtM3M1RHoge1xcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtdGVhY2hDb250ZW50LTE5UVRrIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gICAgbWF4LXdpZHRoOiAzMjBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgICAuVGVhY2gtdGVhY2hDb250ZW50LTE5UVRrIC5UZWFjaC1nZXRyYW5rcy0zSmNYYSB7XFxuICAgICAgd2lkdGg6IDYwcHg7XFxuICAgICAgaGVpZ2h0OiAxOHB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG5cXG4gICAgLlRlYWNoLXRlYWNoQ29udGVudC0xOVFUayAuVGVhY2gtem9vbS0zUlh1LSB7XFxuICAgICAgd2lkdGg6IDQzcHg7XFxuICAgICAgaGVpZ2h0OiAxMXB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcXG4gICAgfVxcblxcbiAgLlRlYWNoLWltYWdlUGFydC0zTkFpMCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcblxcbiAgICAvKiAudmlld21vcmUubW9iaWxlIHtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4gICAgLlRlYWNoLWltYWdlUGFydC0zTkFpMCAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIHtcXG4gICAgICB3aWR0aDogMzIwcHg7XFxuICAgICAgaGVpZ2h0OiAxODBweDtcXG4gICAgfVxcbiAgICAgIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtbGl2ZXBhcnQtM1RLQzggLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtdG9wQ2lyY2xlLTJ3YUhQIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIHRvcDogLTI0cHg7XFxuICAgICAgICBsZWZ0OiAyNTRweDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWltYWdlUGFydC0zTkFpMC5UZWFjaC1saXZlcGFydC0zVEtDOCAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC1ib3R0b21DaXJjbGUtMVFVOHIge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgYm90dG9tOiAtMTZweDtcXG4gICAgICAgIGxlZnQ6IDYxcHg7XFxuICAgICAgfVxcbiAgICAgIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtYXNzaWdubWVudHBhcnQtMjBEa0kgLlRlYWNoLWVtcHR5Q2FyZC12LUdPXyAuVGVhY2gtdG9wQ2lyY2xlLTJ3YUhQIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICBsZWZ0OiAyOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWFzc2lnbm1lbnRwYXJ0LTIwRGtJIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLWJvdHRvbUNpcmNsZS0xUVU4ciB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICBib3R0b206IC0yNHB4O1xcbiAgICAgICAgbGVmdDogMjM2cHg7XFxuICAgICAgfVxcbiAgICAgIC5UZWFjaC1pbWFnZVBhcnQtM05BaTAuVGVhY2gtZG91YnRwYXJ0LTJOa3lRIC5UZWFjaC1lbXB0eUNhcmQtdi1HT18gLlRlYWNoLXRvcENpcmNsZS0yd2FIUCB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICAgICAgICBsZWZ0OiAyNDNweDtcXG4gICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICBvcGFjaXR5OiAxO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtaW1hZ2VQYXJ0LTNOQWkwLlRlYWNoLWRvdWJ0cGFydC0yTmt5USAuVGVhY2gtZW1wdHlDYXJkLXYtR09fIC5UZWFjaC1ib3R0b21DaXJjbGUtMVFVOHIge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgICAgICAgb3BhY2l0eTogMC4xO1xcbiAgICAgICAgcmlnaHQ6IDIyN3B4O1xcbiAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0yWjBRWSB7XFxuICAgIHdpZHRoOiAyMzdweDtcXG4gICAgaGVpZ2h0OiAzNDBweDtcXG4gICAgcmlnaHQ6IDA7XFxuICAgIGxlZnQ6IDAlO1xcbiAgICBib3R0b206IC0xcmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIHtcXG4gICAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgICAgb3JkZXI6IDI7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1jdXN0b21lckxvZ28teEZBcTkge1xcbiAgICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgICBoZWlnaHQ6IDU2cHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWN1c3RvbWVyc19jb250YWluZXItM2RUeTggLlRlYWNoLWN1c3RvbWVyX3Jldmlldy0zUTFIUiAuVGVhY2gtc3JpQ2hhaXRhbnlhVGV4dC0xSFJ0MiB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuICAgICAgICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSIC5UZWFjaC1hdXRob3JXcmFwcGVyLTJGVHU3IC5UZWFjaC1hYm91dF9hdXRob3ItM3NuNWgge1xcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAzNHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtYXV0aG9yaW1nYm94LTI4LTJyIHtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICAgICAgb3JkZXI6IDE7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtY3VzdG9tZXJzX2NvbnRhaW5lci0zZFR5OCAuVGVhY2gtYXV0aG9yaW1nYm94LTI4LTJyIGltZyB7XFxuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgICBtYXgtaGVpZ2h0OiBub25lO1xcbiAgICAgIH1cXG5cXG4gIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtZG93bmxvYWRBcHAtM3RXcnYge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIHtcXG4gICAgaGVpZ2h0OiAxMDEycHg7XFxuICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICBtYXJnaW46IDI0cHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWhlYWRpbmctMUs1YXMgLlRlYWNoLWxlYXJuaW5nVGV4dC0zYVY3TiB7XFxuICAgICAgICB3aWR0aDogMTIwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1oZWFkaW5nLTFLNWFzIC5UZWFjaC1sZWFybmluZ1RleHQtM2FWN04gaW1nIHtcXG4gICAgICAgICAgbGVmdDogMDtcXG4gICAgICAgICAgbWluLXdpZHRoOiAxNDBweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1jb250ZW50VGV4dC0zZVg1NiB7XFxuICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IDY1MHB4O1xcbiAgICAgIG1hcmdpbjogMTZweCBhdXRvIDAgYXV0bztcXG4gICAgfVxcblxcbiAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGkgLlRlYWNoLXJlcXVlc3REZW1vLUFib0hDIHtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtaW4td2lkdGg6IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtYnV0dG9ud3JhcHBlci1pX2pkaSAuVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0IC5UZWFjaC13aGF0c2FwcC0zNHNwUyB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG9wYWNpdHk6IDE7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLXRlYWNoQ29udGFpbmVyLTJRLVlCIC5UZWFjaC1idXR0b253cmFwcGVyLWlfamRpIC5UZWFjaC13aGF0c2FwcHdyYXBwZXItM2pwejQgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLyogLmFjdGlvbnNXcmFwcGVyIHtcXG4gICAgICByaWdodDogMDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xcblxcbiAgICAgIC5hY3Rpb24ge1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEycHg7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuXFxuICAgICAgICBzcGFuIHtcXG4gICAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5hY3Rpb25pbWdib3gge1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9ICovXFxuXFxuICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSB7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICBtYXJnaW4tdG9wOiAwO1xcblxcbiAgICAgIC8qIC5mZWF0dXJlc1NlY3Rpb24ge1xcbiAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICB9ICovXFxuICAgIH1cXG4gICAgICAgIC5UZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQiAuVGVhY2gtdG9wU2VjdGlvbi0xWlpyLSAuVGVhY2gtdGVhY2hTZWN0aW9uLTMzc25IIC5UZWFjaC10ZWFjaEltZ0JveC1ta3o2NyB7XFxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDMycHg7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtdGVhY2hDb250YWluZXItMlEtWUIgLlRlYWNoLXRvcFNlY3Rpb24tMVpaci0gLlRlYWNoLXRlYWNoU2VjdGlvbi0zM3NuSCAuVGVhY2gtdGVhY2hTZWN0aW9uTmFtZS0xQWRTNCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAuVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1Ege1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICB9XFxuXFxuICAuVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1EgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLlRlYWNoLWRpc3BsYXlDbGllbnRzLTFiZ19RIC5UZWFjaC1jbGllbnRzV3JhcHBlci0zZkpZcSB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuVGVhY2gtZGlzcGxheUNsaWVudHMtMWJnX1EgLlRlYWNoLWNsaWVudHNXcmFwcGVyLTNmSllxIC5UZWFjaC1jbGllbnQtSWk5SkYge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfSAqL1xcblxcbiAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIHtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvVGVhY2gvQ29uZmV0dGlfbW9iaWxlLnN2ZycpO1xcbiAgfVxcblxcbiAgICAuVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkSGVhZGluZy0yeXhSNCB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMjRweCBhdXRvO1xcbiAgICAgIG1heC13aWR0aDogMzAwcHg7XFxuICAgIH1cXG5cXG4gICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgICAgZ2FwOiAxMnB4O1xcbiAgICB9XFxuXFxuICAgICAgLlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzIC5UZWFjaC1hY2hpZXZlZFJvdy1BWHdCViAuVGVhY2gtY2FyZC02ekwxNyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogMzI4cHg7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTJweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgcGFkZGluZzogMTZweDtcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0IHtcXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3Om50aC1jaGlsZCgyKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMztcXG4gICAgICAgICAgICBvcmRlcjogMztcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0OjphZnRlciB7XFxuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtYWNoaWV2ZWRDb250YWluZXItQWE1a3MgLlRlYWNoLWFjaGlldmVkUm93LUFYd0JWIC5UZWFjaC1jYXJkLTZ6TDE3Om50aC1jaGlsZCgzKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgICAgICBvcmRlcjogMjtcXG4gICAgICB9XFxuXFxuICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIHtcXG4gICAgcGFkZGluZzogMzJweCAzMnB4IDAgMzJweDtcXG4gIH1cXG5cXG4gICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtYXZhaWxhYmxlVGl0bGUtMjhNaU0ge1xcbiAgICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1yb3ctMVZTeXIge1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzZweDtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLXJvdy0xVlN5ciAuVGVhY2gtcGxhdGZvcm1Db250YWluZXItMUlBTmMge1xcbiAgICAgICAgICB3aWR0aDogNjhweDtcXG4gICAgICAgICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1yb3ctMVZTeXIgLlRlYWNoLXBsYXRmb3JtQ29udGFpbmVyLTFJQU5jIC5UZWFjaC1wbGF0Zm9ybS0xR2NKWiB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMy4zcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUCAuVGVhY2gtYXZhaWxhYmxlUm93LTNTZUVRIC5UZWFjaC1yb3ctMVZTeXIgLlRlYWNoLXBsYXRmb3JtQ29udGFpbmVyLTFJQU5jIC5UZWFjaC1wbGF0Zm9ybU9zLTJERjZFIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtcm93LTFWU3lyIC5UZWFjaC1wbGF0Zm9ybUNvbnRhaW5lci0xSUFOYzpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtZGVza3RvcEltYWdlLW45bWNzIHtcXG4gICAgICAgIHdpZHRoOiAyOTZweDtcXG4gICAgICAgIGhlaWdodDogMTQwcHg7XFxuICAgICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtc3RvcmUtSVpjRFEge1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC1hdmFpbGFibGVDb250YWluZXItMmU0bVAgLlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUSAuVGVhY2gtc3RvcmUtSVpjRFEgLlRlYWNoLXBsYXlzdG9yZS1QWGZoRyB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDAgMTZweCAwIDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuVGVhY2gtYXZhaWxhYmxlQ29udGFpbmVyLTJlNG1QIC5UZWFjaC1hdmFpbGFibGVSb3ctM1NlRVEgLlRlYWNoLXN0b3JlLUlaY0RRIC5UZWFjaC1hcHBzdG9yZS0ySE1QZiB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuXFxuICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIge1xcbiAgICBwYWRkaW5nOiA0MHB4IDA7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICB9XFxuXFxuICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciBoMSB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIDE2cHggMzJweCAxNnB4O1xcbiAgICB9XFxuXFxuICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUge1xcbiAgICAgIHdpZHRoOiAzMjhweDtcXG4gICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIHtcXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMjBweCAxNnB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiB7XFxuICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiAuVGVhY2gtY29tcGFyaXNpb25UZXh0LTNGbWhQIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiAuVGVhY2gtYm94LTI4N3NEIHtcXG4gICAgICAgICAgICB3aWR0aDogMTZweDtcXG4gICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XFxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLWNvbXBhcmlzaW9uV3JhcHBlci1NSjFReiAuVGVhY2gtYm94LTI4N3NELlRlYWNoLXBvc2l0aXZlLTJuUTdsIHtcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZjO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXogLlRlYWNoLWJveC0yODdzRC5UZWFjaC1uZWdhdGl2ZS0zRDZ3MiB7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDZkNjtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLXRhYmxlRGF0YS0zbm50WiB7XFxuICAgICAgICAgIHdpZHRoOiA5MiU7XFxuICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC10YWJsZURhdGEtM25udFogc3BhbiB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWsgLlRlYWNoLXRhYmxlRGF0YS0zbm50WiBzcGFuOmxhc3QtY2hpbGQge1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrIC5UZWFjaC1kb3RXcmFwcGVyLUFOUWo2IHtcXG4gICAgICAgICAgd2lkdGg6IDglO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtZG90V3JhcHBlci1BTlFqNiAuVGVhY2gtZG90LWZ1OUN0IHtcXG4gICAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRayAuVGVhY2gtZG90V3JhcHBlci1BTlFqNiBzcGFuOmxhc3QtY2hpbGQge1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMSkge1xcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgICAgIG1hcmdpbi10b3A6IDE2cHg7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDIpIHtcXG4gICAgICAgIGdyaWQtcm93OiAxLzI7XFxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgzKSB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMTJweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCg0KSB7XFxuICAgICAgICBncmlkLXJvdzogMi8zO1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDEycHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoNikge1xcbiAgICAgICAgZ3JpZC1yb3c6IDMvNDtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDgpIHtcXG4gICAgICAgIGdyaWQtcm93OiA0LzU7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgxMCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDUvNjtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByIC5UZWFjaC10YWJsZS15OXF0VSAuVGVhY2gtY29tcGFyaXNpb24tV3FCUWs6bnRoLWNoaWxkKDEyKSB7XFxuICAgICAgICBncmlkLXJvdzogNi83O1xcbiAgICAgIH1cXG5cXG4gICAgICAuVGVhY2gtdGFibGVDb250YWluZXItMzJwcHIgLlRlYWNoLXRhYmxlLXk5cXRVIC5UZWFjaC1jb21wYXJpc2lvbi1XcUJRazpudGgtY2hpbGQoMTMpIHtcXG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZWFjaC10YWJsZUNvbnRhaW5lci0zMnBwciAuVGVhY2gtdGFibGUteTlxdFUgLlRlYWNoLWNvbXBhcmlzaW9uLVdxQlFrOm50aC1jaGlsZCgxNCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDcvODtcXG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgICAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1RlYWNoL1RlYWNoLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxtQkFBbUI7TUFDZiwwQkFBMEI7Q0FDL0I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQix1QkFBdUI7Q0FDeEI7O0FBRUQ7Ozs7Ozs7Ozs7O0lBV0k7O0FBRUo7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsY0FBYztDQUNmOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYix1QkFBdUI7T0FDcEIsb0JBQW9CO0dBQ3hCOztBQUVIO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsbUJBQW1CO01BQ2YsMEJBQTBCO0NBQy9COztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHVCQUF1QjtHQUN4Qjs7QUFFSDs7Ozs7Ozs7Ozs7Ozs7OztJQWdCSTs7QUFFSjtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixXQUFXO0VBQ1gsbUJBQW1CO01BQ2YsMEJBQTBCO0NBQy9COztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsbURBQW1EO1lBQzNDLDJDQUEyQztHQUNwRDs7QUFFSDtNQUNNLFlBQVk7TUFDWixhQUFhO0tBQ2Q7O0FBRUw7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjs7SUFFakI7Ozs7Ozs7Ozs7Ozs7O1FBY0k7R0FDTDs7QUFFSDtNQUNNLGVBQWU7TUFDZixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQix1QkFBdUI7TUFDdkIsZUFBZTtNQUNmLGlCQUFpQjtLQUNsQjs7QUFFTDtNQUNNLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsZUFBZTtNQUNmLGlCQUFpQjtLQUNsQjs7QUFFTDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsY0FBYztDQUNmOztBQUVEO0lBQ0ksa0JBQWtCO1FBQ2QsU0FBUztJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLG1CQUFtQjtJQUNuQixXQUFXO0dBQ1o7O0FBRUg7TUFDTSxhQUFhO01BQ2IsYUFBYTtNQUNiLG1CQUFtQjtNQUNuQixvQkFBb0I7S0FDckI7O0FBRUw7UUFDUSxZQUFZO1FBQ1osYUFBYTtRQUNiLHVCQUF1QjtXQUNwQixvQkFBb0I7T0FDeEI7O0FBRVA7TUFDTSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLGlCQUFpQjtNQUNqQixvQkFBb0I7TUFDcEIsaUJBQWlCO0tBQ2xCOztBQUVMO01BQ00scUJBQXFCO01BQ3JCLGNBQWM7TUFDZCwyQkFBMkI7VUFDdkIsdUJBQXVCO0tBQzVCOztBQUVMO1FBQ1EsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixpQkFBaUI7UUFDakIsWUFBWTtRQUNaLG1CQUFtQjtPQUNwQjs7QUFFUDtRQUNRLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsb0JBQW9CO09BQ3JCOztBQUVQO01BQ00sZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixpQkFBaUI7TUFDakIsWUFBWTtNQUNaLHFCQUFxQjtNQUNyQixjQUFjO01BQ2Qsd0JBQXdCO1VBQ3BCLG9CQUFvQjtNQUN4Qix1QkFBdUI7VUFDbkIsb0JBQW9CO0tBQ3pCOztBQUVMO1FBQ1EsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtRQUNqQixZQUFZO1FBQ1osYUFBYTtRQUNiLHVCQUF1QjtXQUNwQixvQkFBb0I7T0FDeEI7O0FBRVA7RUFDRSx1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLDRCQUE0QjtFQUM1QixxQkFBcUI7Q0FDdEI7O0FBRUQ7SUFDSSwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLGFBQWE7R0FDZDs7QUFFSDtNQUNNLGFBQWE7TUFDYixjQUFjO01BQ2QsaUJBQWlCO0tBQ2xCOztBQUVMO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UscUJBQXFCO01BQ2pCLDRCQUE0QjtDQUNqQzs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLG9CQUFvQjtNQUNoQixnQkFBZ0I7RUFDcEIsc0JBQXNCO0VBQ3RCLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQixxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLG9CQUFvQjtFQUNwQixvQkFBb0I7Q0FDckI7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsYUFBYTtJQUNiLG1CQUFtQjtHQUNwQjs7QUFFSDtJQUNJLGFBQWE7SUFDYixhQUFhO0dBQ2Q7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsY0FBYztDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLG1CQUFtQjtHQUNwQjs7QUFFSDtNQUNNLGFBQWE7TUFDYixnQkFBZ0I7TUFDaEIsbUJBQW1CO0tBQ3BCOztBQUVMO1FBQ1EsbUJBQW1CO1FBQ25CLGFBQWE7UUFDYixXQUFXO1FBQ1gsWUFBWTtRQUNaLFlBQVk7UUFDWixpQkFBaUI7T0FDbEI7O0FBRVA7SUFDSSxpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxZQUFZO0lBQ1oscUJBQXFCO1FBQ2pCLDRCQUE0QjtJQUNoQyx1QkFBdUI7UUFDbkIsb0JBQW9CO0dBQ3pCOztBQUVIO01BQ00sbUJBQW1CO01BQ25CLHVCQUF1QjtNQUN2QixnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLGlCQUFpQjtNQUNqQixnQkFBZ0I7TUFDaEIsWUFBWTtNQUNaLG1CQUFtQjtNQUNuQiwyQkFBMkI7TUFDM0Isd0JBQXdCO01BQ3hCLG1CQUFtQjtLQUNwQjs7QUFFTDtNQUNNLDJCQUEyQjtNQUMzQix3QkFBd0I7TUFDeEIsbUJBQW1CO01BQ25CLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QscUJBQXFCO1VBQ2pCLDRCQUE0QjtNQUNoQyx1QkFBdUI7VUFDbkIsb0JBQW9CO0tBQ3pCOztBQUVMO1FBQ1EsMkJBQTJCO1FBQzNCLHdCQUF3QjtRQUN4QixtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGdCQUFnQjtRQUNoQixpQkFBaUI7UUFDakIsaUJBQWlCO1FBQ2pCLDBCQUEwQjtRQUMxQixxQkFBcUI7UUFDckIsa0JBQWtCO1FBQ2xCLGFBQWE7T0FDZDs7QUFFUDtRQUNRLFlBQVk7UUFDWixhQUFhO09BQ2Q7O0FBRVA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01Bc0NNOztBQUVOO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLGlCQUFpQjs7SUFFakI7Ozs7Ozs7Ozs7OztRQVlJO0dBQ0w7O0FBRUg7TUFDTSxxQkFBcUI7TUFDckIsY0FBYztNQUNkLHVCQUF1QjtVQUNuQixvQkFBb0I7S0FDekI7O0FBRUw7UUFDUSxZQUFZO1FBQ1osYUFBYTtPQUNkOztBQUVQO1VBQ1UsWUFBWTtVQUNaLGFBQWE7VUFDYix1QkFBdUI7YUFDcEIsb0JBQW9CO1NBQ3hCOztBQUVUO1FBQ1EsaUJBQWlCO1FBQ2pCLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsZUFBZTtRQUNmLGtCQUFrQjtPQUNuQjs7QUFFUDtJQUNJLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsaUJBQWlCO0dBQ2xCOztBQUVIO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsY0FBYztFQUNkLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsVUFBVTtFQUNWLFlBQVk7RUFDWixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLHNCQUFzQjtFQUN0QixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHVEQUF1RDtFQUN2RCx5QkFBeUI7RUFDekIsNEJBQTRCO0VBQzVCLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsMEJBQTBCO01BQ3RCLDhCQUE4QjtDQUNuQzs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLHNDQUFzQztFQUN0QyxzQ0FBc0M7RUFDdEMsVUFBVTtFQUNWLFVBQVU7RUFDVixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QiwrREFBK0Q7VUFDdkQsdURBQXVEO0VBQy9ELHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7Q0FDcEI7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7O0lBY0k7O0FBRUo7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx3QkFBd0I7UUFDcEIsb0JBQW9CO0lBQ3hCLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLGtCQUFrQjtHQUNuQjs7QUFFSDtNQUNNLHFCQUFxQjtVQUNqQiw0QkFBNEI7S0FDakM7O0FBRUw7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxnQ0FBZ0M7UUFDNUIsNEJBQTRCO0lBQ2hDLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsb0JBQW9CO0NBQ3JCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixvQkFBb0I7R0FDckI7O0FBRUg7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtDQUN6Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLG1EQUFtRDtVQUMzQywyQ0FBMkM7RUFDbkQsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLFdBQVc7RUFDWCxtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7Q0FDcEI7O0FBRUQ7TUFDTSwwQkFBMEI7TUFDMUIsWUFBWTtNQUNaLGFBQWE7TUFDYixjQUFjO01BQ2QsV0FBVztLQUNaOztBQUVMO01BQ00sMEJBQTBCO01BQzFCLFlBQVk7TUFDWixhQUFhO01BQ2IsV0FBVztNQUNYLFlBQVk7S0FDYjs7QUFFTDtNQUNNLFlBQVk7TUFDWixhQUFhO01BQ2IsMEJBQTBCO01BQzFCLGNBQWM7TUFDZCxXQUFXO0tBQ1o7O0FBRUw7TUFDTSxZQUFZO01BQ1osYUFBYTtNQUNiLHVCQUF1QjtNQUN2QixXQUFXO01BQ1gsWUFBWTtNQUNaLGFBQWE7S0FDZDs7QUFFTDtNQUNNLFlBQVk7TUFDWixhQUFhO01BQ2IsMEJBQTBCO01BQzFCLGNBQWM7TUFDZCxXQUFXO01BQ1gsYUFBYTtLQUNkOztBQUVMO01BQ00sWUFBWTtNQUNaLGFBQWE7TUFDYiwwQkFBMEI7TUFDMUIsV0FBVztNQUNYLFlBQVk7TUFDWixhQUFhO0tBQ2Q7O0FBRUw7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHdCQUF3QjtNQUNwQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLGFBQWE7RUFDYixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsMEJBQTBCO0NBQzNCOztBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7QUFFSDtJQUNJLGFBQWE7SUFDYixjQUFjO0lBQ2QsK0JBQStCO0lBQy9CLGFBQWE7R0FDZDs7QUFFSDtNQUNNLG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIscUJBQXFCO01BQ3JCLGNBQWM7TUFDZCwyQ0FBMkM7S0FDNUM7O0FBRUw7UUFDUSxxQkFBcUI7UUFDckIsY0FBYztRQUNkLHVCQUF1QjtZQUNuQixvQkFBb0I7T0FDekI7O0FBRVA7VUFDVSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLGVBQWU7U0FDaEI7O0FBRVQ7VUFDVSxZQUFZO1VBQ1osYUFBYTtVQUNiLGtCQUFrQjtVQUNsQixtQkFBbUI7U0FDcEI7O0FBRVQ7VUFDVSx1QkFBdUI7U0FDeEI7O0FBRVQ7VUFDVSwwQkFBMEI7U0FDM0I7O0FBRVQ7UUFDUSxVQUFVO09BQ1g7O0FBRVA7VUFDVSxXQUFXO1VBQ1gsWUFBWTtVQUNaLG1CQUFtQjtVQUNuQiwwQkFBMEI7VUFDMUIsaUJBQWlCO1NBQ2xCOztBQUVUO1VBQ1UsdUJBQXVCO1NBQ3hCOztBQUVUO1FBQ1EsV0FBVztRQUNYLHFCQUFxQjtRQUNyQixjQUFjO1FBQ2QsMkJBQTJCO1lBQ3ZCLHVCQUF1QjtPQUM1Qjs7QUFFUDtVQUNVLGdCQUFnQjtVQUNoQixrQkFBa0I7VUFDbEIsZUFBZTtVQUNmLG9CQUFvQjtTQUNyQjs7QUFFVDtNQUNNLDBCQUEwQjtLQUMzQjs7QUFFTDtNQUNNLDBCQUEwQjtLQUMzQjs7QUFFTDs7TUFFTSwwQkFBMEI7TUFDMUIsa0JBQWtCO0tBQ25COztBQUVMOztNQUVNLHdDQUF3QztNQUN4QyxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLGVBQWU7TUFDZixpQkFBaUI7TUFDakIsa0JBQWtCO01BQ2xCLGlCQUFpQjtLQUNsQjs7QUFFTDtFQUNFO0lBQ0UsZ0JBQWdCO0dBQ2pCO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLDBCQUEwQjtHQUMzQjtJQUNDO01BQ0UsK0JBQStCO0tBQ2hDO0NBQ0o7O0FBRUQ7RUFDRTtJQUNFLGNBQWM7R0FDZjs7RUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQXlCSTs7RUFFSjs7TUFFSTs7RUFFSjtJQUNFLGdCQUFnQjtHQUNqQjs7SUFFQztNQUNFLDJCQUEyQjtVQUN2Qix1QkFBdUI7TUFDM0IsZ0JBQWdCO0tBQ2pCOztNQUVDO1FBQ0Usb0JBQW9CO1FBQ3BCLGVBQWU7T0FDaEI7O0VBRUw7SUFDRSxnQkFBZ0I7R0FDakI7O0lBRUM7TUFDRSwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLGdCQUFnQjtLQUNqQjs7TUFFQztRQUNFLG9CQUFvQjtRQUNwQixlQUFlO09BQ2hCOztFQUVMO0lBQ0UsWUFBWTtJQUNaLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3Qjs7SUFFQztNQUNFLFlBQVk7TUFDWixhQUFhO01BQ2IsYUFBYTtNQUNiLGNBQWM7S0FDZjs7TUFFQztRQUNFLFlBQVk7UUFDWixhQUFhO09BQ2Q7O0lBRUg7TUFDRSxnQkFBZ0I7O01BRWhCOzs7VUFHSTtLQUNMOztNQUVDO1FBQ0UsbUJBQW1CO1FBQ25CLGdCQUFnQjtPQUNqQjs7TUFFRDtRQUNFLGVBQWU7UUFDZixZQUFZO1FBQ1osaUJBQWlCO1FBQ2pCLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGFBQWE7T0FDZDs7RUFFTDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0UsWUFBWTtNQUNaLGFBQWE7TUFDYixpQkFBaUI7S0FDbEI7O0lBRUQ7TUFDRSxZQUFZO01BQ1osYUFBYTtNQUNiLG1CQUFtQjtLQUNwQjs7RUFFSDtJQUNFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QiwyQkFBMkI7UUFDdkIsdUJBQXVCOztJQUUzQjs7O1FBR0k7R0FDTDs7SUFFQztNQUNFLGFBQWE7TUFDYixjQUFjO0tBQ2Y7TUFDQztRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsMEJBQTBCO1FBQzFCLGFBQWE7UUFDYixXQUFXO1FBQ1gsWUFBWTtPQUNiOztNQUVEO1FBQ0UsWUFBWTtRQUNaLGFBQWE7UUFDYiwwQkFBMEI7UUFDMUIsYUFBYTtRQUNiLGNBQWM7UUFDZCxXQUFXO09BQ1o7TUFDRDtRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCLGFBQWE7UUFDYixXQUFXO1FBQ1gsV0FBVztPQUNaOztNQUVEO1FBQ0UsWUFBWTtRQUNaLGFBQWE7UUFDYix1QkFBdUI7UUFDdkIsYUFBYTtRQUNiLGNBQWM7UUFDZCxZQUFZO09BQ2I7TUFDRDtRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsMEJBQTBCO1FBQzFCLFlBQVk7UUFDWixXQUFXO1FBQ1gsV0FBVztPQUNaOztNQUVEO1FBQ0UsWUFBWTtRQUNaLGFBQWE7UUFDYix1QkFBdUI7UUFDdkIsYUFBYTtRQUNiLGFBQWE7UUFDYixjQUFjO09BQ2Y7O0VBRUw7SUFDRSxhQUFhO0lBQ2IsY0FBYztJQUNkLFNBQVM7SUFDVCxTQUFTO0lBQ1QsY0FBYztJQUNkLGFBQWE7R0FDZDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixhQUFhO0lBQ2IsWUFBWTtJQUNaLHVCQUF1QjtRQUNuQixvQkFBb0I7R0FDekI7O0lBRUM7TUFDRSxrQkFBa0I7VUFDZCxTQUFTO01BQ2IscUJBQXFCO01BQ3JCLGNBQWM7TUFDZCwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLHVCQUF1QjtVQUNuQixvQkFBb0I7TUFDeEIsbUJBQW1CO01BQ25CLFlBQVk7TUFDWixZQUFZO01BQ1osbUJBQW1CO0tBQ3BCOztNQUVDO1FBQ0UsWUFBWTtRQUNaLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLGdCQUFnQjtPQUNqQjtRQUNDO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixvQkFBb0I7U0FDckI7O0lBRUw7TUFDRSxZQUFZO01BQ1osWUFBWTtNQUNaLGtCQUFrQjtVQUNkLFNBQVM7S0FDZDs7TUFFQztRQUNFLGdCQUFnQjtRQUNoQixpQkFBaUI7T0FDbEI7O0VBRUw7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxlQUFlO0lBQ2YsbUJBQW1CO0dBQ3BCOztJQUVDO01BQ0UsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsZ0JBQWdCO01BQ2hCLHlCQUF5QjtLQUMxQjs7TUFFQztRQUNFLGFBQWE7T0FDZDs7UUFFQztVQUNFLFFBQVE7VUFDUixpQkFBaUI7U0FDbEI7O0lBRUw7TUFDRSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixpQkFBaUI7TUFDakIseUJBQXlCO0tBQzFCOztJQUVEO01BQ0UsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQixpQkFBaUI7S0FDbEI7O01BRUM7UUFDRSxrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixnQkFBZ0I7T0FDakI7O01BRUQ7UUFDRSxpQkFBaUI7UUFDakIsb0JBQW9CO09BQ3JCOztRQUVDO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixXQUFXO1VBQ1gsZUFBZTtTQUNoQjs7UUFFRDtVQUNFLFlBQVk7VUFDWixhQUFhO1NBQ2Q7O0lBRUw7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBaUJJOztJQUVKO01BQ0Usc0JBQXNCO1VBQ2xCLHdCQUF3QjtNQUM1QixjQUFjOztNQUVkOztVQUVJO0tBQ0w7UUFDRztVQUNFLFlBQVk7VUFDWixhQUFhO1NBQ2Q7O1FBRUQ7VUFDRSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1NBQ25COztFQUVQO0lBQ0UscUJBQXFCO0lBQ3JCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQ0FBc0M7SUFDdEMsbUNBQW1DO0lBQ25DLFVBQVU7SUFDViwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGVBQWU7SUFDZixhQUFhO0dBQ2Q7O0VBRUQ7O01BRUk7O0VBRUo7SUFDRSxtQkFBbUI7SUFDbkIsMkRBQTJEO0dBQzVEOztJQUVDO01BQ0UsZ0JBQWdCO01BQ2hCLG9CQUFvQjtNQUNwQix5QkFBeUI7TUFDekIsaUJBQWlCO0tBQ2xCOztJQUVEO01BQ0UsMkJBQTJCO01BQzNCLFVBQVU7S0FDWDs7TUFFQztRQUNFLFlBQVk7UUFDWixpQkFBaUI7UUFDakIsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsa0JBQWtCO1FBQ2xCLGNBQWM7O1FBRWQ7O1lBRUk7T0FDTDs7TUFFRDtRQUNFLGtCQUFrQjtZQUNkLFNBQVM7O1FBRWI7O1lBRUk7T0FDTDs7TUFFRDtRQUNFLGtCQUFrQjtZQUNkLFNBQVM7T0FDZDs7RUFFTDtJQUNFLDBCQUEwQjtHQUMzQjs7SUFFQztNQUNFLDJCQUEyQjtLQUM1Qjs7TUFFQztRQUNFLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsVUFBVTtRQUNWLG9CQUFvQjtRQUNwQixzQkFBc0I7WUFDbEIsd0JBQXdCO09BQzdCOztRQUVDO1VBQ0UsWUFBWTtVQUNaLHFCQUFxQjtjQUNqQiw0QkFBNEI7VUFDaEMsbUJBQW1CO1VBQ25CLGlCQUFpQjtTQUNsQjs7VUFFQztZQUNFLGtCQUFrQjtZQUNsQixrQkFBa0I7V0FDbkI7O1VBRUQ7WUFDRSxnQkFBZ0I7WUFDaEIsa0JBQWtCO1dBQ25COztRQUVIO1VBQ0UsZ0JBQWdCO1NBQ2pCOztNQUVIO1FBQ0UsYUFBYTtRQUNiLGNBQWM7UUFDZCxhQUFhO09BQ2Q7O01BRUQ7UUFDRSxzQkFBc0I7WUFDbEIsd0JBQXdCO09BQzdCOztRQUVDO1VBQ0UsYUFBYTtVQUNiLGFBQWE7VUFDYixtQkFBbUI7U0FDcEI7O1FBRUQ7VUFDRSxhQUFhO1VBQ2IsYUFBYTtVQUNiLFVBQVU7U0FDWDs7RUFFUDtJQUNFLGdCQUFnQjtJQUNoQiwwQkFBMEI7R0FDM0I7O0lBRUM7TUFDRSxnQkFBZ0I7TUFDaEIsb0JBQW9CO01BQ3BCLHlCQUF5QjtLQUMxQjs7SUFFRDtNQUNFLGFBQWE7TUFDYixjQUFjO01BQ2QsMkJBQTJCO01BQzNCLGFBQWE7S0FDZDs7TUFFQztRQUNFLDZCQUE2QjtRQUM3QixnQkFBZ0I7UUFDaEIscUJBQXFCO1FBQ3JCLGNBQWM7UUFDZCwyQ0FBMkM7T0FDNUM7O1FBRUM7VUFDRSxxQkFBcUI7VUFDckIsY0FBYztVQUNkLHVCQUF1QjtjQUNuQixvQkFBb0I7U0FDekI7O1VBRUM7WUFDRSxnQkFBZ0I7WUFDaEIsa0JBQWtCO1lBQ2xCLGVBQWU7V0FDaEI7O1VBRUQ7WUFDRSxZQUFZO1lBQ1osYUFBYTtZQUNiLGlCQUFpQjtZQUNqQixtQkFBbUI7V0FDcEI7O1VBRUQ7WUFDRSx1QkFBdUI7V0FDeEI7O1VBRUQ7WUFDRSwwQkFBMEI7V0FDM0I7O1FBRUg7VUFDRSxXQUFXO1VBQ1gscUJBQXFCO1VBQ3JCLGNBQWM7VUFDZCwyQkFBMkI7Y0FDdkIsdUJBQXVCO1NBQzVCOztVQUVDO1lBQ0UsZ0JBQWdCO1lBQ2hCLGtCQUFrQjtZQUNsQixlQUFlO1lBQ2Ysb0JBQW9CO1dBQ3JCOztVQUVEO1lBQ0UsaUJBQWlCO1dBQ2xCOztRQUVIO1VBQ0UsVUFBVTtTQUNYOztVQUVDO1lBQ0UsV0FBVztZQUNYLFlBQVk7WUFDWixtQkFBbUI7WUFDbkIsMEJBQTBCO1lBQzFCLGdCQUFnQjtXQUNqQjs7VUFFRDtZQUNFLGlCQUFpQjtXQUNsQjs7TUFFTDtRQUNFLGlCQUFpQjtRQUNqQixpQkFBaUI7UUFDakIsZUFBZTtPQUNoQjs7TUFFRDtRQUNFLGNBQWM7UUFDZCxpQkFBaUI7UUFDakIsZUFBZTtPQUNoQjs7TUFFRDtRQUNFLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsY0FBYztRQUNkLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsY0FBYztPQUNmOztNQUVEO1FBQ0UsY0FBYztPQUNmOztNQUVEO1FBQ0UsY0FBYztPQUNmOztNQUVEO1FBQ0UsY0FBYztPQUNmOztNQUVEO1FBQ0Usb0JBQW9CO09BQ3JCOztNQUVEO1FBQ0UsY0FBYztRQUNkLG9CQUFvQjtPQUNyQjtDQUNOXCIsXCJmaWxlXCI6XCJUZWFjaC5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5icmVhZGNydW0ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxufVxcblxcbi5icmVhZGNydW0gc3BhbiB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4udG9nZ2xlX291dGVyIHtcXG4gIHdpZHRoOiAzMnB4O1xcbiAgaGVpZ2h0OiAxNy44cHg7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIHBhZGRpbmc6IDJweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiAtMTBweDtcXG59XFxuXFxuLnRvZ2dsZV9vbiB7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZV9pbm5lciB7XFxuICB3aWR0aDogMTRweDtcXG4gIGhlaWdodDogMTRweDtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4vKiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAzNnB4O1xcbiAgYm90dG9tOiA4N3B4O1xcbiAgcmlnaHQ6IDU2MHB4O1xcblxcbiAgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gIH1cXG59ICovXFxuXFxuLmhlYWRlcmJhY2tncm91bmRtb2JpbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAwO1xcbiAgcmlnaHQ6IDY0cHg7XFxuICB3aWR0aDogNDQ1cHg7XFxuICBoZWlnaHQ6IDU4NHB4O1xcbn1cXG5cXG4uaGVhZGVyYmFja2dyb3VuZG1vYmlsZSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbi5hdXRob3JpbWdib3gge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4uYXV0aG9yaW1nYm94IGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNjQwcHg7XFxuICAgIG1heC1oZWlnaHQ6IDU2MHB4O1xcbiAgICBqdXN0aWZ5LXNlbGY6IGZsZXgtZW5kO1xcbiAgfVxcblxcbi8qIC52aWV3bW9yZS5tb2JpbGUge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGNvbG9yOiAjMDA3NmZmO1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDMwcHg7XFxuXFxuICBpbWcge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICBtYXJnaW4tdG9wOiAxcHg7XFxuICB9XFxufSAqL1xcblxcbi5jb250ZW50UGFydCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgd2lkdGg6IDYwJTtcXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4uY29udGVudFBhcnQgLnRvcGljSW1hZ2Uge1xcbiAgICB3aWR0aDogNzJweDtcXG4gICAgaGVpZ2h0OiA3MnB4O1xcbiAgICBwYWRkaW5nOiAxNnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XFxuICAgIGJhY2tncm91bmQ6ICNmZmY7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgfVxcblxcbi5jb250ZW50UGFydCAudG9waWNJbWFnZSBpbWcge1xcbiAgICAgIHdpZHRoOiA0MnB4O1xcbiAgICAgIGhlaWdodDogNDJweDtcXG4gICAgfVxcblxcbi5jb250ZW50UGFydCAuY29udGVudCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtaW4td2lkdGg6IDI5MHB4O1xcbiAgICBtYXgtd2lkdGg6IDU0MHB4O1xcblxcbiAgICAvKiAudmlld21vcmUge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBjb2xvcjogIzAwNzZmZjtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG5cXG4gICAgICBpbWcge1xcbiAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICBoZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgIH1cXG4gICAgfSAqL1xcbiAgfVxcblxcbi5jb250ZW50UGFydCAuY29udGVudCAuc2VjdGlvbl90aXRsZSB7XFxuICAgICAgZGlzcGxheTogYmxvY2s7XFxuICAgICAgbWFyZ2luOiAxMnB4IDA7XFxuICAgICAgZm9udC1zaXplOiA0MHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgICAgIGxldHRlci1zcGFjaW5nOiAtMS4ycHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgfVxcblxcbi5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQge1xcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICBtYXgtd2lkdGg6IDM3MHB4O1xcbiAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGhlaWdodDogNTYwcHg7XFxufVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcge1xcbiAgICAtbXMtZmxleC1vcmRlcjogMDtcXG4gICAgICAgIG9yZGVyOiAwO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZzogNDhweCA2NHB4O1xcbiAgICB3aWR0aDogNTAlO1xcbiAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmN1c3RvbWVyTG9nbyB7XFxuICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIGhlaWdodDogODBweDtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmN1c3RvbWVyTG9nbyBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuYXV0aG9yV3JhcHBlciB7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgfVxcblxcbi5jdXN0b21lcnNfY29udGFpbmVyIC5jdXN0b21lcl9yZXZpZXcgLmF1dGhvcldyYXBwZXIgLmF1dGhvcl90aXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuYXV0aG9yV3JhcHBlciAuYWJvdXRfYXV0aG9yIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gICAgICB9XFxuXFxuLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuYWxsY3VzdG9tZXJzIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5hbGxjdXN0b21lcnMgaW1nIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDNweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICAgICAgICB3aWR0aDogMjBweDtcXG4gICAgICAgIGhlaWdodDogMjBweDtcXG4gICAgICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG5cXG4uYXZhaWxhYmxlQ29udGFpbmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBwYWRkaW5nOiA4MHB4IDExM3B4IDAgMTY0cHg7XFxuICBwYWRkaW5nOiA1cmVtIDExM3B4IDAgMTY0cHg7XFxuICAvLyBtYXgtd2lkdGg6IDEzMDBweDtcXG59XFxuXFxuLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IHtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLmRlc2t0b3BJbWFnZSB7XFxuICAgICAgd2lkdGg6IDY0OHB4O1xcbiAgICAgIGhlaWdodDogMzI4cHg7XFxuICAgICAgbWFyZ2luLXRvcDogMzVweDtcXG4gICAgfVxcblxcbi5hdmFpbGFibGVUaXRsZSB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogMS4yO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uYXZhaWxhYmxlIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uYXZhaWxhYmxlQ29udGVudFNlY3Rpb24ge1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbn1cXG5cXG4ucGxhdGZvcm1Db250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIG1hcmdpbi1yaWdodDogMnJlbTtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG59XFxuXFxuLnBsYXRmb3JtIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW46IDhweCAwIDRweCAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5wbGF0Zm9ybU9zIHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLnJvdyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIG1hcmdpbjogNDhweCAwIDMycHggMDtcXG4gIG1hcmdpbjogM3JlbSAwIDJyZW0gMDtcXG59XFxuXFxuLnN0b3JlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDRyZW07XFxufVxcblxcbi5zdG9yZSAucGxheXN0b3JlIHtcXG4gICAgd2lkdGg6IDE0MHB4O1xcbiAgICBoZWlnaHQ6IDQycHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIH1cXG5cXG4uc3RvcmUgLmFwcHN0b3JlIHtcXG4gICAgd2lkdGg6IDE0MHB4O1xcbiAgICBoZWlnaHQ6IDQycHg7XFxuICB9XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDExNTJweDtcXG4gIG1hcmdpbjogMTJweCBhdXRvIDA7XFxufVxcblxcbi5pbWFnZVBhcnQgLmVtcHR5Q2FyZCAudG9wQ2FyZCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIHtcXG4gIGhlaWdodDogNjgwcHg7XFxuICBwYWRkaW5nOiAyNHB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlY2UwO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgY3Vyc29yOiBhbGlhcztcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiA0OHB4O1xcbiAgICBsaW5lLWhlaWdodDogNjZweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIG1heC13aWR0aDogNTQycHg7XFxuICAgIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCB7XFxuICAgICAgd2lkdGg6IDE4NXB4O1xcbiAgICAgIGRpc3BsYXk6IGlubGluZTtcXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCBpbWcge1xcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgICAgYm90dG9tOiAtOHB4O1xcbiAgICAgICAgbGVmdDogLThweDtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgaGVpZ2h0OiA4cHg7XFxuICAgICAgICBtYXgtd2lkdGg6IDIyNXB4O1xcbiAgICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIge1xcbiAgICBtYXJnaW4tdG9wOiA2NHB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIH1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgICBjb2xvcjogIzAwMDtcXG4gICAgICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICAgICAgd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IG1heC1jb250ZW50O1xcbiAgICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIge1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCB7XFxuICAgICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYiAhaW1wb3J0YW50O1xcbiAgICAgICAgbWFyZ2luOiAwIDhweCAwIDMycHg7XFxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gICAgICAgIG9wYWNpdHk6IDAuNjtcXG4gICAgICB9XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgaW1nIHtcXG4gICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgIH1cXG5cXG4vKiAuYWN0aW9uc1dyYXBwZXIge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGJvdHRvbTogMjZweDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuXFxuICAgIC5hY3Rpb24ge1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcblxcbiAgICAgIHNwYW4ge1xcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICAgIG9wYWNpdHk6IDAuNztcXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWN0aW9uaW1nYm94IHtcXG4gICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgICAgICBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA5cHg7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiA1NnB4O1xcblxcbiAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLWxlZnQ6IDMwJTtcXG5cXG4gICAgICBzcGFuIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDMycHg7XFxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xcbiAgICAgIH1cXG4gICAgfSAqL1xcbiAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIHtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgICAgICAgd2lkdGg6IDQwcHg7XFxuICAgICAgICBoZWlnaHQ6IDQwcHg7XFxuICAgICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaEltZ0JveCBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcbiAgICAgICAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaFNlY3Rpb25OYW1lIHtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMzBweDtcXG4gICAgICAgIGNvbG9yOiAjZmY2NDAwO1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICAgICAgfVxcblxcbi50ZWFjaENvbnRhaW5lciAuY29udGVudFRleHQge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMCAwO1xcbiAgICBtYXgtd2lkdGg6IDY4N3B4O1xcbiAgfVxcblxcbi5kb3dubG9hZEFwcCB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi5kb3dubG9hZEFwcCAuZG93bmxvYWRUZXh0IHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgb3BhY2l0eTogMC43O1xcbn1cXG5cXG4uZG93bmxvYWRBcHAgLnBsYXlTdG9yZUljb24ge1xcbiAgd2lkdGg6IDEzMnB4O1xcbiAgaGVpZ2h0OiA0MHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiA0NjRweDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9uZXdfY29uZmV0dGkuc3ZnJyk7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICBnYXA6IDE2cHg7XFxuICBnYXA6IDFyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQge1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiAyMDJweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHBhZGRpbmc6IDEuNXJlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLmNsaWVudHMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnN0dWRlbnRzIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2VmZmU7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS50ZXN0cyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUucXVlc3Rpb25zIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodCB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQucXVlc3Rpb25zSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjMDBhYzI2O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQudGVzdHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5zdHVkZW50c0hpZ2hsaWdodCB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LmNsaWVudHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmNjA7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLnN1YlRleHQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi8qIC50b2dnbGVBdFJpZ2h0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDAgNjRweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZUF0TGVmdCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59ICovXFxuXFxuLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2Uge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwYWRkaW5nOiAxODRweCAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlIC5zZWN0aW9uX2NvbnRlbnRzIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDAgNjRweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gIH1cXG5cXG4uc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSAuc2VjdGlvbl9jb250ZW50cyAuY29udGVudFBhcnQge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIH1cXG5cXG4uc2VjdGlvbl9jb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBwYWRkaW5nOiAxODRweCAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5zZWN0aW9uX2NvbnRhaW5lciAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZzogMCA2NHB4O1xcbiAgICBtYXgtd2lkdGg6IDE3MDBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi50ZWFjaENvbnRlbnQge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1heC13aWR0aDogMzM3cHg7XFxuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG59XFxuXFxuLnRlYWNoQ29udGVudCAuZ2V0cmFua3Mge1xcbiAgICB3aWR0aDogODlweDtcXG4gICAgaGVpZ2h0OiAyNnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAtNXB4O1xcbiAgfVxcblxcbi50ZWFjaENvbnRlbnQgLnpvb20ge1xcbiAgICB3aWR0aDogNjRweDtcXG4gICAgaGVpZ2h0OiAxOHB4O1xcbiAgfVxcblxcbi5pbWFnZVBhcnQge1xcbiAgd2lkdGg6IDQwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmltYWdlUGFydCAuZW1wdHlDYXJkIHtcXG4gIHdpZHRoOiA2MDZweDtcXG4gIGhlaWdodDogMzQxcHg7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAzMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDMycHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC5lbXB0eUNhcmQgLnRvcENhcmQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgei1pbmRleDogMTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG59XFxuXFxuLmltYWdlUGFydCAuYm90dG9tQ2lyY2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA0OHB4O1xcbiAgaGVpZ2h0OiA0OHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC50b3BDaXJjbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDY0cHg7XFxuICBoZWlnaHQ6IDY0cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxufVxcblxcbi5pbWFnZVBhcnQubGl2ZXBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDc5cHg7XFxuICAgIH1cXG5cXG4uaW1hZ2VQYXJ0LmxpdmVwYXJ0IC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgdG9wOiAtNDRweDtcXG4gICAgICByaWdodDogNTBweDtcXG4gICAgfVxcblxcbi5pbWFnZVBhcnQuYXNzaWdubWVudHBhcnQgLmVtcHR5Q2FyZCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgICB3aWR0aDogODhweDtcXG4gICAgICBoZWlnaHQ6IDg4cHg7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgICBib3R0b206IC00NHB4O1xcbiAgICAgIGxlZnQ6IDQ4cHg7XFxuICAgIH1cXG5cXG4uaW1hZ2VQYXJ0LmFzc2lnbm1lbnRwYXJ0IC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgaGVpZ2h0OiA4OHB4O1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgdG9wOiAtNDRweDtcXG4gICAgICByaWdodDogNjdweDtcXG4gICAgICBvcGFjaXR5OiAwLjM7XFxuICAgIH1cXG5cXG4uaW1hZ2VQYXJ0LmRvdWJ0cGFydCAuZW1wdHlDYXJkIC5ib3R0b21DaXJjbGUge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgICAgIGJvdHRvbTogLTQ0cHg7XFxuICAgICAgbGVmdDogNjhweDtcXG4gICAgICBvcGFjaXR5OiAwLjI7XFxuICAgIH1cXG5cXG4uaW1hZ2VQYXJ0LmRvdWJ0cGFydCAuZW1wdHlDYXJkIC50b3BDaXJjbGUge1xcbiAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgIGhlaWdodDogODhweDtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICAgIHRvcDogLTQ0cHg7XFxuICAgICAgcmlnaHQ6IDM2cHg7XFxuICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICB9XFxuXFxuLmFsbGN1c3RvbWVycyBwIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5jb250ZW50IHAgcCB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLnRhYmxlQ29udGFpbmVyIHtcXG4gIHBhZGRpbmc6IDU2cHggMDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi50YWJsZUNvbnRhaW5lciBoMSB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUge1xcbiAgICB3aWR0aDogOTU2cHg7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIHtcXG4gICAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgICAgZm9udC1zaXplOiAxOHB4O1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigwLCAwLCAwLCAwLjEpO1xcbiAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciB7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmNvbXBhcmlzaW9uVGV4dCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmJveCB7XFxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciAuYm94LnBvc2l0aXZlIHtcXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NmYztcXG4gICAgICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuY29tcGFyaXNpb25XcmFwcGVyIC5ib3gubmVnYXRpdmUge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkNmQ2O1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5kb3RXcmFwcGVyIHtcXG4gICAgICAgIHdpZHRoOiA1JTtcXG4gICAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmRvdFdyYXBwZXIgLmRvdCB7XFxuICAgICAgICAgIHdpZHRoOiA4cHg7XFxuICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICAgIG1hcmdpbi10b3A6IDEycHg7XFxuICAgICAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmRvdFdyYXBwZXIgLmRvdC5yZWQge1xcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMGMwO1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC50YWJsZURhdGEge1xcbiAgICAgICAgd2lkdGg6IDk1JTtcXG4gICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC50YWJsZURhdGEgc3BhbiB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xcbiAgICAgICAgfVxcblxcbi50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZChvZGQpIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmViO1xcbiAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKGV2ZW4pIHtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmViO1xcbiAgICB9XFxuXFxuLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDEpLFxcbiAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMikge1xcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgIH1cXG5cXG4udGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMyksXFxuICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCg0KSB7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgzNywgNDAsIDQzLCAwLjEpO1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMzJweDtcXG4gICAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xcbiAgLnNlY3Rpb25fY29udGVudHMge1xcbiAgICBwYWRkaW5nOiAwIDQwcHg7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XFxuICAuYXZhaWxhYmxlQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogNXJlbSA2NHB4IDAgNjRweDtcXG4gIH1cXG4gICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5icmVhZGNydW0ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLyogLnRvZ2dsZV9vdXRlciB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDE4cHg7XFxuICAgIG1hcmdpbi10b3A6IC04cHg7XFxuICB9XFxuXFxuICAudG9nZ2xlX2lubmVyIHtcXG4gICAgd2lkdGg6IDE0cHg7XFxuICAgIGhlaWdodDogMTRweDtcXG4gIH1cXG5cXG4gIC50b2dnbGVBdFJpZ2h0IHtcXG4gICAgcGFkZGluZzogMyUgNSUgMCA1JTtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAudG9nZ2xlQXRMZWZ0IHtcXG4gICAgcGFkZGluZzogMyUgNSUgMCA1JTtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9ICovXFxuXFxuICAvKiAubW91c2VpY29uIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH0gKi9cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDMycHggMDtcXG4gIH1cXG5cXG4gICAgLnNlY3Rpb25fY29udGFpbmVyIC5zZWN0aW9uX2NvbnRlbnRzIHtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBwYWRkaW5nOiAwIDE2cHg7XFxuICAgIH1cXG5cXG4gICAgICAuc2VjdGlvbl9jb250YWluZXIgLnNlY3Rpb25fY29udGVudHMgLnNlY3Rpb25fdGl0bGUge1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICAgIG1hcmdpbjogMTZweCAwO1xcbiAgICAgIH1cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlIHtcXG4gICAgcGFkZGluZzogMzJweCAwO1xcbiAgfVxcblxcbiAgICAuc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgcGFkZGluZzogMCAxNnB4O1xcbiAgICB9XFxuXFxuICAgICAgLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UgLnNlY3Rpb25fY29udGVudHMgLnNlY3Rpb25fdGl0bGUge1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICAgIG1hcmdpbjogMTZweCAwO1xcbiAgICAgIH1cXG5cXG4gIC5jb250ZW50UGFydCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgICAuY29udGVudFBhcnQgLnRvcGljSW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgcGFkZGluZzogMTBweDtcXG4gICAgfVxcblxcbiAgICAgIC5jb250ZW50UGFydCAudG9waWNJbWFnZSBpbWcge1xcbiAgICAgICAgd2lkdGg6IDI4cHg7XFxuICAgICAgICBoZWlnaHQ6IDI4cHg7XFxuICAgICAgfVxcblxcbiAgICAuY29udGVudFBhcnQgLmNvbnRlbnQge1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG5cXG4gICAgICAvKiAudmlld21vcmUge1xcbiAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH0gKi9cXG4gICAgfVxcblxcbiAgICAgIC5jb250ZW50UGFydCAuY29udGVudCAuc2VjdGlvbl90aXRsZSB7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQge1xcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgICB9XFxuXFxuICAudGVhY2hDb250ZW50IHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG4gICAgbWF4LXdpZHRoOiAzMjBweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgICAudGVhY2hDb250ZW50IC5nZXRyYW5rcyB7XFxuICAgICAgd2lkdGg6IDYwcHg7XFxuICAgICAgaGVpZ2h0OiAxOHB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgIH1cXG5cXG4gICAgLnRlYWNoQ29udGVudCAuem9vbSB7XFxuICAgICAgd2lkdGg6IDQzcHg7XFxuICAgICAgaGVpZ2h0OiAxMXB4O1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcXG4gICAgfVxcblxcbiAgLmltYWdlUGFydCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcblxcbiAgICAvKiAudmlld21vcmUubW9iaWxlIHtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIH0gKi9cXG4gIH1cXG5cXG4gICAgLmltYWdlUGFydCAuZW1wdHlDYXJkIHtcXG4gICAgICB3aWR0aDogMzIwcHg7XFxuICAgICAgaGVpZ2h0OiAxODBweDtcXG4gICAgfVxcbiAgICAgIC5pbWFnZVBhcnQubGl2ZXBhcnQgLmVtcHR5Q2FyZCAudG9wQ2lyY2xlIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMjtcXG4gICAgICAgIHRvcDogLTI0cHg7XFxuICAgICAgICBsZWZ0OiAyNTRweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmltYWdlUGFydC5saXZlcGFydCAuZW1wdHlDYXJkIC5ib3R0b21DaXJjbGUge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICAgICAgb3BhY2l0eTogMC4yO1xcbiAgICAgICAgYm90dG9tOiAtMTZweDtcXG4gICAgICAgIGxlZnQ6IDYxcHg7XFxuICAgICAgfVxcbiAgICAgIC5pbWFnZVBhcnQuYXNzaWdubWVudHBhcnQgLmVtcHR5Q2FyZCAudG9wQ2lyY2xlIHtcXG4gICAgICAgIHdpZHRoOiA0OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA0OHB4O1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgIG9wYWNpdHk6IDAuMTtcXG4gICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICBsZWZ0OiAyOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuaW1hZ2VQYXJ0LmFzc2lnbm1lbnRwYXJ0IC5lbXB0eUNhcmQgLmJvdHRvbUNpcmNsZSB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgICAgICBvcGFjaXR5OiAwLjI7XFxuICAgICAgICBib3R0b206IC0yNHB4O1xcbiAgICAgICAgbGVmdDogMjM2cHg7XFxuICAgICAgfVxcbiAgICAgIC5pbWFnZVBhcnQuZG91YnRwYXJ0IC5lbXB0eUNhcmQgLnRvcENpcmNsZSB7XFxuICAgICAgICB3aWR0aDogNDhweDtcXG4gICAgICAgIGhlaWdodDogNDhweDtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICAgICAgICBsZWZ0OiAyNDNweDtcXG4gICAgICAgIHRvcDogLTE2cHg7XFxuICAgICAgICBvcGFjaXR5OiAxO1xcbiAgICAgIH1cXG5cXG4gICAgICAuaW1hZ2VQYXJ0LmRvdWJ0cGFydCAuZW1wdHlDYXJkIC5ib3R0b21DaXJjbGUge1xcbiAgICAgICAgd2lkdGg6IDQ4cHg7XFxuICAgICAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgICAgICAgb3BhY2l0eTogMC4xO1xcbiAgICAgICAgcmlnaHQ6IDIyN3B4O1xcbiAgICAgICAgYm90dG9tOiAtMjRweDtcXG4gICAgICB9XFxuXFxuICAuaGVhZGVyYmFja2dyb3VuZG1vYmlsZSB7XFxuICAgIHdpZHRoOiAyMzdweDtcXG4gICAgaGVpZ2h0OiAzNDBweDtcXG4gICAgcmlnaHQ6IDA7XFxuICAgIGxlZnQ6IDAlO1xcbiAgICBib3R0b206IC0xcmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgICAuY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IHtcXG4gICAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgICAgb3JkZXI6IDI7XFxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuICAgIH1cXG5cXG4gICAgICAuY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5jdXN0b21lckxvZ28ge1xcbiAgICAgICAgd2lkdGg6IDg4cHg7XFxuICAgICAgICBoZWlnaHQ6IDU2cHg7XFxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmN1c3RvbWVyc19jb250YWluZXIgLmN1c3RvbWVyX3JldmlldyAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuICAgICAgICAuY3VzdG9tZXJzX2NvbnRhaW5lciAuY3VzdG9tZXJfcmV2aWV3IC5hdXRob3JXcmFwcGVyIC5hYm91dF9hdXRob3Ige1xcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAzNHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAuY3VzdG9tZXJzX2NvbnRhaW5lciAuYXV0aG9yaW1nYm94IHtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAtbXMtZmxleC1vcmRlcjogMTtcXG4gICAgICAgICAgb3JkZXI6IDE7XFxuICAgIH1cXG5cXG4gICAgICAuY3VzdG9tZXJzX2NvbnRhaW5lciAuYXV0aG9yaW1nYm94IGltZyB7XFxuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XFxuICAgICAgICBtYXgtaGVpZ2h0OiBub25lO1xcbiAgICAgIH1cXG5cXG4gIC50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDMwN3B4O1xcbiAgfVxcblxcbiAgLnRlYWNoQ29udGFpbmVyIHtcXG4gICAgaGVpZ2h0OiAxMDEycHg7XFxuICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG4gIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICBtYXJnaW46IDI0cHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAudGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCB7XFxuICAgICAgICB3aWR0aDogMTIwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQgaW1nIHtcXG4gICAgICAgICAgbGVmdDogMDtcXG4gICAgICAgICAgbWluLXdpZHRoOiAxNDBweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLnRlYWNoQ29udGFpbmVyIC5jb250ZW50VGV4dCB7XFxuICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBtYXgtd2lkdGg6IDY1MHB4O1xcbiAgICAgIG1hcmdpbjogMTZweCBhdXRvIDAgYXV0bztcXG4gICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIge1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIG1hcmdpbi10b3A6IDQ4cHg7XFxuICAgIH1cXG5cXG4gICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLnJlcXVlc3REZW1vIHtcXG4gICAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICBtaW4td2lkdGg6IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHg7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIC53aGF0c2FwcCB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG9wYWNpdHk6IDE7XFxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgaW1nIHtcXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XFxuICAgICAgICAgIGhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLyogLmFjdGlvbnNXcmFwcGVyIHtcXG4gICAgICByaWdodDogMDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xcblxcbiAgICAgIC5hY3Rpb24ge1xcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEycHg7XFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuXFxuICAgICAgICBzcGFuIHtcXG4gICAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5hY3Rpb25pbWdib3gge1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9ICovXFxuXFxuICAgIC50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiB7XFxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICBtYXJnaW4tdG9wOiAwO1xcblxcbiAgICAgIC8qIC5mZWF0dXJlc1NlY3Rpb24ge1xcbiAgICAgICAgZGlzcGxheTogbm9uZTtcXG4gICAgICB9ICovXFxuICAgIH1cXG4gICAgICAgIC50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaEltZ0JveCB7XFxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDMycHg7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hTZWN0aW9uTmFtZSB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgaDMge1xcbiAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIG1heC13aWR0aDogMTQuODc1cmVtO1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfSAqL1xcblxcbiAgLmFjaGlldmVkQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMjRweCAxNnB4O1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvVGVhY2gvQ29uZmV0dGlfbW9iaWxlLnN2ZycpO1xcbiAgfVxcblxcbiAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMjRweCBhdXRvO1xcbiAgICAgIG1heC13aWR0aDogMzAwcHg7XFxuICAgIH1cXG5cXG4gICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgICAgZ2FwOiAxMnB4O1xcbiAgICB9XFxuXFxuICAgICAgLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogMzI4cHg7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTJweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xcbiAgICAgICAgcGFkZGluZzogMTZweDtcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0IHtcXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkOm50aC1jaGlsZCgyKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMztcXG4gICAgICAgICAgICBvcmRlcjogMztcXG5cXG4gICAgICAgIC8qIC5oaWdodExpZ2h0OjphZnRlciB7XFxuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xcbiAgICAgICAgfSAqL1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkOm50aC1jaGlsZCgzKSB7XFxuICAgICAgICAtbXMtZmxleC1vcmRlcjogMjtcXG4gICAgICAgICAgICBvcmRlcjogMjtcXG4gICAgICB9XFxuXFxuICAuYXZhaWxhYmxlQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMzJweCAzMnB4IDAgMzJweDtcXG4gIH1cXG5cXG4gICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuYXZhaWxhYmxlVGl0bGUge1xcbiAgICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cge1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzZweDtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXIge1xcbiAgICAgICAgICB3aWR0aDogNjhweDtcXG4gICAgICAgICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIC5wbGF0Zm9ybSB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMy4zcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIC5wbGF0Zm9ybU9zIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lcjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuZGVza3RvcEltYWdlIHtcXG4gICAgICAgIHdpZHRoOiAyOTZweDtcXG4gICAgICAgIGhlaWdodDogMTQwcHg7XFxuICAgICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuc3RvcmUge1xcbiAgICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuc3RvcmUgLnBsYXlzdG9yZSB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDAgMTZweCAwIDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnN0b3JlIC5hcHBzdG9yZSB7XFxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcXG4gICAgICAgICAgaGVpZ2h0OiA0MnB4O1xcbiAgICAgICAgICBtYXJnaW46IDA7XFxuICAgICAgICB9XFxuXFxuICAudGFibGVDb250YWluZXIge1xcbiAgICBwYWRkaW5nOiA0MHB4IDA7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICB9XFxuXFxuICAgIC50YWJsZUNvbnRhaW5lciBoMSB7XFxuICAgICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgbWFyZ2luOiAwIDE2cHggMzJweCAxNnB4O1xcbiAgICB9XFxuXFxuICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUge1xcbiAgICAgIHdpZHRoOiAzMjhweDtcXG4gICAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIHtcXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMjBweCAxNnB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDAsIDAsIDAsIDAuMSk7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciB7XFxuICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciAuY29tcGFyaXNpb25UZXh0IHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgICAgY29sb3I6ICMyNTI4MmI7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciAuYm94IHtcXG4gICAgICAgICAgICB3aWR0aDogMTZweDtcXG4gICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XFxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLmNvbXBhcmlzaW9uV3JhcHBlciAuYm94LnBvc2l0aXZlIHtcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZjO1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5jb21wYXJpc2lvbldyYXBwZXIgLmJveC5uZWdhdGl2ZSB7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDZkNjtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLnRhYmxlRGF0YSB7XFxuICAgICAgICAgIHdpZHRoOiA5MiU7XFxuICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICB9XFxuXFxuICAgICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC50YWJsZURhdGEgc3BhbiB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb24gLnRhYmxlRGF0YSBzcGFuOmxhc3QtY2hpbGQge1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uIC5kb3RXcmFwcGVyIHtcXG4gICAgICAgICAgd2lkdGg6IDglO1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuZG90V3JhcHBlciAuZG90IHtcXG4gICAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyODJiO1xcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbiAuZG90V3JhcHBlciBzcGFuOmxhc3QtY2hpbGQge1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMSkge1xcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcXG4gICAgICAgIG1hcmdpbi10b3A6IDE2cHg7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMDtcXG4gICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDIpIHtcXG4gICAgICAgIGdyaWQtcm93OiAxLzI7XFxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgzKSB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBwYWRkaW5nOiA4cHggMTJweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCg0KSB7XFxuICAgICAgICBncmlkLXJvdzogMi8zO1xcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgcGFkZGluZzogOHB4IDEycHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoNikge1xcbiAgICAgICAgZ3JpZC1yb3c6IDMvNDtcXG4gICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDgpIHtcXG4gICAgICAgIGdyaWQtcm93OiA0LzU7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgxMCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDUvNjtcXG4gICAgICB9XFxuXFxuICAgICAgLnRhYmxlQ29udGFpbmVyIC50YWJsZSAuY29tcGFyaXNpb246bnRoLWNoaWxkKDEyKSB7XFxuICAgICAgICBncmlkLXJvdzogNi83O1xcbiAgICAgIH1cXG5cXG4gICAgICAudGFibGVDb250YWluZXIgLnRhYmxlIC5jb21wYXJpc2lvbjpudGgtY2hpbGQoMTMpIHtcXG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgICAgfVxcblxcbiAgICAgIC50YWJsZUNvbnRhaW5lciAudGFibGUgLmNvbXBhcmlzaW9uOm50aC1jaGlsZCgxNCkge1xcbiAgICAgICAgZ3JpZC1yb3c6IDcvODtcXG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XFxuICAgICAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwiYnJlYWRjcnVtXCI6IFwiVGVhY2gtYnJlYWRjcnVtLTExeFg1XCIsXG5cdFwidG9nZ2xlX291dGVyXCI6IFwiVGVhY2gtdG9nZ2xlX291dGVyLVZJc1ZFXCIsXG5cdFwidG9nZ2xlX29uXCI6IFwiVGVhY2gtdG9nZ2xlX29uLTJKYkpkXCIsXG5cdFwidG9nZ2xlX2lubmVyXCI6IFwiVGVhY2gtdG9nZ2xlX2lubmVyLTNnVXJ1XCIsXG5cdFwiaGVhZGVyYmFja2dyb3VuZG1vYmlsZVwiOiBcIlRlYWNoLWhlYWRlcmJhY2tncm91bmRtb2JpbGUtMlowUVlcIixcblx0XCJhdXRob3JpbWdib3hcIjogXCJUZWFjaC1hdXRob3JpbWdib3gtMjgtMnJcIixcblx0XCJjb250ZW50UGFydFwiOiBcIlRlYWNoLWNvbnRlbnRQYXJ0LTFsU2dLXCIsXG5cdFwidG9waWNJbWFnZVwiOiBcIlRlYWNoLXRvcGljSW1hZ2UtMUNNN2pcIixcblx0XCJjb250ZW50XCI6IFwiVGVhY2gtY29udGVudC1zX3pDZFwiLFxuXHRcInNlY3Rpb25fdGl0bGVcIjogXCJUZWFjaC1zZWN0aW9uX3RpdGxlLTJueUJkXCIsXG5cdFwidGV4dGNvbnRlbnRcIjogXCJUZWFjaC10ZXh0Y29udGVudC0zczVEelwiLFxuXHRcImN1c3RvbWVyc19jb250YWluZXJcIjogXCJUZWFjaC1jdXN0b21lcnNfY29udGFpbmVyLTNkVHk4XCIsXG5cdFwiY3VzdG9tZXJfcmV2aWV3XCI6IFwiVGVhY2gtY3VzdG9tZXJfcmV2aWV3LTNRMUhSXCIsXG5cdFwiY3VzdG9tZXJMb2dvXCI6IFwiVGVhY2gtY3VzdG9tZXJMb2dvLXhGQXE5XCIsXG5cdFwic3JpQ2hhaXRhbnlhVGV4dFwiOiBcIlRlYWNoLXNyaUNoYWl0YW55YVRleHQtMUhSdDJcIixcblx0XCJhdXRob3JXcmFwcGVyXCI6IFwiVGVhY2gtYXV0aG9yV3JhcHBlci0yRlR1N1wiLFxuXHRcImF1dGhvcl90aXRsZVwiOiBcIlRlYWNoLWF1dGhvcl90aXRsZS0zSE5DeVwiLFxuXHRcImFib3V0X2F1dGhvclwiOiBcIlRlYWNoLWFib3V0X2F1dGhvci0zc241aFwiLFxuXHRcImFsbGN1c3RvbWVyc1wiOiBcIlRlYWNoLWFsbGN1c3RvbWVycy0yYjhOT1wiLFxuXHRcImF2YWlsYWJsZUNvbnRhaW5lclwiOiBcIlRlYWNoLWF2YWlsYWJsZUNvbnRhaW5lci0yZTRtUFwiLFxuXHRcImF2YWlsYWJsZVJvd1wiOiBcIlRlYWNoLWF2YWlsYWJsZVJvdy0zU2VFUVwiLFxuXHRcImRlc2t0b3BJbWFnZVwiOiBcIlRlYWNoLWRlc2t0b3BJbWFnZS1uOW1jc1wiLFxuXHRcImF2YWlsYWJsZVRpdGxlXCI6IFwiVGVhY2gtYXZhaWxhYmxlVGl0bGUtMjhNaU1cIixcblx0XCJhdmFpbGFibGVcIjogXCJUZWFjaC1hdmFpbGFibGUtazdBbmlcIixcblx0XCJhdmFpbGFibGVDb250ZW50U2VjdGlvblwiOiBcIlRlYWNoLWF2YWlsYWJsZUNvbnRlbnRTZWN0aW9uLUpabGFGXCIsXG5cdFwicGxhdGZvcm1Db250YWluZXJcIjogXCJUZWFjaC1wbGF0Zm9ybUNvbnRhaW5lci0xSUFOY1wiLFxuXHRcInBsYXRmb3JtXCI6IFwiVGVhY2gtcGxhdGZvcm0tMUdjSlpcIixcblx0XCJwbGF0Zm9ybU9zXCI6IFwiVGVhY2gtcGxhdGZvcm1Pcy0yREY2RVwiLFxuXHRcInJvd1wiOiBcIlRlYWNoLXJvdy0xVlN5clwiLFxuXHRcInN0b3JlXCI6IFwiVGVhY2gtc3RvcmUtSVpjRFFcIixcblx0XCJwbGF5c3RvcmVcIjogXCJUZWFjaC1wbGF5c3RvcmUtUFhmaEdcIixcblx0XCJhcHBzdG9yZVwiOiBcIlRlYWNoLWFwcHN0b3JlLTJITVBmXCIsXG5cdFwiZGlzcGxheUNsaWVudHNcIjogXCJUZWFjaC1kaXNwbGF5Q2xpZW50cy0xYmdfUVwiLFxuXHRcImltYWdlUGFydFwiOiBcIlRlYWNoLWltYWdlUGFydC0zTkFpMFwiLFxuXHRcImVtcHR5Q2FyZFwiOiBcIlRlYWNoLWVtcHR5Q2FyZC12LUdPX1wiLFxuXHRcInRvcENhcmRcIjogXCJUZWFjaC10b3BDYXJkLTJadUQ0XCIsXG5cdFwidGVhY2hDb250YWluZXJcIjogXCJUZWFjaC10ZWFjaENvbnRhaW5lci0yUS1ZQlwiLFxuXHRcImhlYWRpbmdcIjogXCJUZWFjaC1oZWFkaW5nLTFLNWFzXCIsXG5cdFwibGVhcm5pbmdUZXh0XCI6IFwiVGVhY2gtbGVhcm5pbmdUZXh0LTNhVjdOXCIsXG5cdFwiYnV0dG9ud3JhcHBlclwiOiBcIlRlYWNoLWJ1dHRvbndyYXBwZXItaV9qZGlcIixcblx0XCJyZXF1ZXN0RGVtb1wiOiBcIlRlYWNoLXJlcXVlc3REZW1vLUFib0hDXCIsXG5cdFwid2hhdHNhcHB3cmFwcGVyXCI6IFwiVGVhY2gtd2hhdHNhcHB3cmFwcGVyLTNqcHo0XCIsXG5cdFwid2hhdHNhcHBcIjogXCJUZWFjaC13aGF0c2FwcC0zNHNwU1wiLFxuXHRcInRvcFNlY3Rpb25cIjogXCJUZWFjaC10b3BTZWN0aW9uLTFaWnItXCIsXG5cdFwidGVhY2hTZWN0aW9uXCI6IFwiVGVhY2gtdGVhY2hTZWN0aW9uLTMzc25IXCIsXG5cdFwidGVhY2hJbWdCb3hcIjogXCJUZWFjaC10ZWFjaEltZ0JveC1ta3o2N1wiLFxuXHRcInRlYWNoU2VjdGlvbk5hbWVcIjogXCJUZWFjaC10ZWFjaFNlY3Rpb25OYW1lLTFBZFM0XCIsXG5cdFwiY29udGVudFRleHRcIjogXCJUZWFjaC1jb250ZW50VGV4dC0zZVg1NlwiLFxuXHRcImRvd25sb2FkQXBwXCI6IFwiVGVhY2gtZG93bmxvYWRBcHAtM3RXcnZcIixcblx0XCJkb3dubG9hZFRleHRcIjogXCJUZWFjaC1kb3dubG9hZFRleHQtMllPY05cIixcblx0XCJwbGF5U3RvcmVJY29uXCI6IFwiVGVhY2gtcGxheVN0b3JlSWNvbi0zWWREOFwiLFxuXHRcImNsaWVudHNXcmFwcGVyXCI6IFwiVGVhY2gtY2xpZW50c1dyYXBwZXItM2ZKWXFcIixcblx0XCJjbGllbnRcIjogXCJUZWFjaC1jbGllbnQtSWk5SkZcIixcblx0XCJhY2hpZXZlZENvbnRhaW5lclwiOiBcIlRlYWNoLWFjaGlldmVkQ29udGFpbmVyLUFhNWtzXCIsXG5cdFwiYWNoaWV2ZWRIZWFkaW5nXCI6IFwiVGVhY2gtYWNoaWV2ZWRIZWFkaW5nLTJ5eFI0XCIsXG5cdFwiYWNoaWV2ZWRSb3dcIjogXCJUZWFjaC1hY2hpZXZlZFJvdy1BWHdCVlwiLFxuXHRcImNhcmRcIjogXCJUZWFjaC1jYXJkLTZ6TDE3XCIsXG5cdFwiYWNoaWV2ZWRQcm9maWxlXCI6IFwiVGVhY2gtYWNoaWV2ZWRQcm9maWxlLTN3NU5qXCIsXG5cdFwiY2xpZW50c1wiOiBcIlRlYWNoLWNsaWVudHMtMWhVRmlcIixcblx0XCJzdHVkZW50c1wiOiBcIlRlYWNoLXN0dWRlbnRzLVBBRlpHXCIsXG5cdFwidGVzdHNcIjogXCJUZWFjaC10ZXN0cy0yN0RFc1wiLFxuXHRcInF1ZXN0aW9uc1wiOiBcIlRlYWNoLXF1ZXN0aW9ucy1XQm5sRFwiLFxuXHRcImhpZ2hsaWdodFwiOiBcIlRlYWNoLWhpZ2hsaWdodC0zbGxSU1wiLFxuXHRcInF1ZXN0aW9uc0hpZ2hsaWdodFwiOiBcIlRlYWNoLXF1ZXN0aW9uc0hpZ2hsaWdodC0zN0xhOVwiLFxuXHRcInRlc3RzSGlnaGxpZ2h0XCI6IFwiVGVhY2gtdGVzdHNIaWdobGlnaHQtMkhiOU9cIixcblx0XCJzdHVkZW50c0hpZ2hsaWdodFwiOiBcIlRlYWNoLXN0dWRlbnRzSGlnaGxpZ2h0LTJINUZfXCIsXG5cdFwiY2xpZW50c0hpZ2hsaWdodFwiOiBcIlRlYWNoLWNsaWVudHNIaWdobGlnaHQtMWFua0RcIixcblx0XCJzdWJUZXh0XCI6IFwiVGVhY2gtc3ViVGV4dC0zSVJIS1wiLFxuXHRcInNlY3Rpb25fY29udGFpbmVyX3JldmVyc2VcIjogXCJUZWFjaC1zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlLTF5RURZXCIsXG5cdFwic2VjdGlvbl9jb250ZW50c1wiOiBcIlRlYWNoLXNlY3Rpb25fY29udGVudHMtMU5SRHJcIixcblx0XCJzZWN0aW9uX2NvbnRhaW5lclwiOiBcIlRlYWNoLXNlY3Rpb25fY29udGFpbmVyLWN5eVROXCIsXG5cdFwidGVhY2hDb250ZW50XCI6IFwiVGVhY2gtdGVhY2hDb250ZW50LTE5UVRrXCIsXG5cdFwiZ2V0cmFua3NcIjogXCJUZWFjaC1nZXRyYW5rcy0zSmNYYVwiLFxuXHRcInpvb21cIjogXCJUZWFjaC16b29tLTNSWHUtXCIsXG5cdFwiYm90dG9tQ2lyY2xlXCI6IFwiVGVhY2gtYm90dG9tQ2lyY2xlLTFRVThyXCIsXG5cdFwidG9wQ2lyY2xlXCI6IFwiVGVhY2gtdG9wQ2lyY2xlLTJ3YUhQXCIsXG5cdFwibGl2ZXBhcnRcIjogXCJUZWFjaC1saXZlcGFydC0zVEtDOFwiLFxuXHRcImFzc2lnbm1lbnRwYXJ0XCI6IFwiVGVhY2gtYXNzaWdubWVudHBhcnQtMjBEa0lcIixcblx0XCJkb3VidHBhcnRcIjogXCJUZWFjaC1kb3VidHBhcnQtMk5reVFcIixcblx0XCJ0YWJsZUNvbnRhaW5lclwiOiBcIlRlYWNoLXRhYmxlQ29udGFpbmVyLTMycHByXCIsXG5cdFwidGFibGVcIjogXCJUZWFjaC10YWJsZS15OXF0VVwiLFxuXHRcImNvbXBhcmlzaW9uXCI6IFwiVGVhY2gtY29tcGFyaXNpb24tV3FCUWtcIixcblx0XCJjb21wYXJpc2lvbldyYXBwZXJcIjogXCJUZWFjaC1jb21wYXJpc2lvbldyYXBwZXItTUoxUXpcIixcblx0XCJjb21wYXJpc2lvblRleHRcIjogXCJUZWFjaC1jb21wYXJpc2lvblRleHQtM0ZtaFBcIixcblx0XCJib3hcIjogXCJUZWFjaC1ib3gtMjg3c0RcIixcblx0XCJwb3NpdGl2ZVwiOiBcIlRlYWNoLXBvc2l0aXZlLTJuUTdsXCIsXG5cdFwibmVnYXRpdmVcIjogXCJUZWFjaC1uZWdhdGl2ZS0zRDZ3MlwiLFxuXHRcImRvdFdyYXBwZXJcIjogXCJUZWFjaC1kb3RXcmFwcGVyLUFOUWo2XCIsXG5cdFwiZG90XCI6IFwiVGVhY2gtZG90LWZ1OUN0XCIsXG5cdFwicmVkXCI6IFwiVGVhY2gtcmVkLXplRnFsXCIsXG5cdFwidGFibGVEYXRhXCI6IFwiVGVhY2gtdGFibGVEYXRhLTNubnRaXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgeyBQTEFURk9STVMsIEhPTUVfQ0xJRU5UU19VUERBVEVEIH0gZnJvbSAnLi4vR2V0UmFua3NDb25zdGFudHMnO1xuaW1wb3J0IHMgZnJvbSAnLi9UZWFjaC5zY3NzJztcblxuY2xhc3MgVGVhY2ggZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgbGl2ZVRvZ2dsZTogZmFsc2UsXG4gICAgICBsZWN0dXJlVG9nZ2xlOiBmYWxzZSxcbiAgICAgIGFzc2lnbm1lbnRUb2dnbGU6IGZhbHNlLFxuICAgICAgZG91YnRUb2dnbGU6IGZhbHNlLFxuICAgIH07XG4gIH1cblxuICAvLyBUb2dnbGUgQnV0dG9uIENvZGVcbiAgZGlzcGxheVRvZ2dsZSA9ICgpID0+IChcbiAgICA8ZGl2XG4gICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgIHRoaXMuc3RhdGUubGl2ZVRvZ2dsZVxuICAgICAgICAgID8gYCR7cy50b2dnbGVfb3V0ZXJ9ICR7cy50b2dnbGVfb259YFxuICAgICAgICAgIDogcy50b2dnbGVfb3V0ZXJcbiAgICAgIH1cbiAgICA+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b2dnbGVfaW5uZXJ9IC8+XG4gICAgPC9kaXY+XG4gICk7XG4gIC8vIFRlYWNoIFNlY3Rpb25cbiAgZGlzcGxheVRlYWNoU2VjdGlvbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZWFjaENvbnRhaW5lcn0+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYnJlYWRjcnVtfT5cbiAgICAgICAgSG9tZSAvIDxzcGFuPkxpdmUgQ2xhc3Nlczwvc3Bhbj5cbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcFNlY3Rpb259PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZWFjaFNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoSW1nQm94fT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZWFjaC9saXZlX2NvbG9yZWQuc3ZnXCIgYWx0PVwibGl2ZS1jbGFzc2VzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGVhY2hTZWN0aW9uTmFtZX0+TGl2ZSBDbGFzc2VzPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGgyIGNsYXNzTmFtZT17cy5oZWFkaW5nfT5cbiAgICAgICAgTWF4aW1pemUgdGhlJm5ic3A7XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmxlYXJuaW5nVGV4dH0+XG4gICAgICAgICAgPHNwYW4+bGVhcm5pbmcmbmJzcDs8L3NwYW4+XG4gICAgICAgICAgPGltZ1xuICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL2xpdmUgY2xhc3Nlcy91bmRlcnNjb3JlX2Zvcl90aXRsZS5wbmdcIlxuICAgICAgICAgICAgYWx0PVwidW5kZXJzY29yZVwiXG4gICAgICAgICAgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIG9mIHlvdXIgc3R1ZGVudHNcbiAgICAgIDwvaDI+XG4gICAgICA8cCBjbGFzc05hbWU9e3MuY29udGVudFRleHR9PlxuICAgICAgICBBIHBvd2VyZnVsIHRlYWNoaW5nIG1vZHVsZSB0aGF0IGZhY2lsaXRhdGVzIGRpc3RhbmNlIGxlYXJuaW5nLCBwcm9tb3RlXG4gICAgICAgIHBvc2l0aXZlIHN0dWRlbnQgYmVoYXZpb3VyLCBlbmdhZ2UgcGFyZW50cyBpbiB0aGUgbGVhcm5pbmcgcHJvY2VzcyBhbmRcbiAgICAgICAgcmVkdWNlIHRlYWNoZXIgd29ya2xvYWQgYWxsIGZyb20gb25lIGVhc3ktdG8tdXNlIHBsYXRmb3JtLlxuICAgICAgPC9wPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYnV0dG9ud3JhcHBlcn0+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MucmVxdWVzdERlbW99XG4gICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVTaG93VHJpYWx9XG4gICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgID5cbiAgICAgICAgICBHRVQgU1VCU0NSSVBUSU9OXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8YVxuICAgICAgICAgIGNsYXNzTmFtZT17cy53aGF0c2FwcHdyYXBwZXJ9XG4gICAgICAgICAgaHJlZj1cImh0dHBzOi8vYXBpLndoYXRzYXBwLmNvbS9zZW5kP3Bob25lPTkxODgwMDc2NDkwOSZ0ZXh0PUhpIEdldFJhbmtzLCBJIHdvdWxkIGxpa2UgdG8ga25vdyBtb3JlIGFib3V0IHlvdXIgT25saW5lIFBsYXRmb3JtLlwiXG4gICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLndoYXRzYXBwfT5DaGF0IG9uPC9kaXY+XG4gICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdHNhcHBfbG9nby5zdmdcIiBhbHQ9XCJ3aGF0c2FwcFwiIC8+XG4gICAgICAgIDwvYT5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG93bmxvYWRBcHB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3dubG9hZFRleHR9PkRvd25sb2FkIHRoZSBhcHA8L2Rpdj5cbiAgICAgICAgPGFcbiAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9wbGF5Lmdvb2dsZS5jb20vc3RvcmUvYXBwcy9kZXRhaWxzP2lkPWNvbS5lZ25pZnkuZ2V0cmFua3MmaGw9ZW5fSU4mZ2w9VVNcIlxuICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgID5cbiAgICAgICAgICA8aW1nXG4gICAgICAgICAgICBjbGFzc05hbWU9e3MucGxheVN0b3JlSWNvbn1cbiAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxheVN0b3JlLnBuZ1wiXG4gICAgICAgICAgICBhbHQ9XCJwbGF5X3N0b3JlXCJcbiAgICAgICAgICAvPlxuICAgICAgICA8L2E+XG4gICAgICA8L2Rpdj5cbiAgICAgIHsvKlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW91c2VpY29ufT5cbiAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL21vdXNlX2ljb24uc3ZnXCIgYWx0PVwibW91c2VfaWNvblwiIC8+XG4gICAgICA8L2Rpdj4gKi99XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlfT5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVhY2gvdGVhY2hfaGVhZGVyX2h1bWFuLndlYnBcIlxuICAgICAgICAgIGFsdD1cIm1vYmlsZV9iYWNrZ3JvdW5kXCJcbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDbGllbnRzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRpc3BsYXlDbGllbnRzfT5cbiAgICAgIDxoMz5cbiAgICAgICAgVHJ1c3RlZCBieSA8c3BhbiAvPlxuICAgICAgICBsZWFkaW5nIEVkdWNhdGlvbmFsIEluc3RpdHV0aW9uc1xuICAgICAgPC9oMz5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNsaWVudHNXcmFwcGVyfT5cbiAgICAgICAge0hPTUVfQ0xJRU5UU19VUERBVEVELm1hcChjbGllbnQgPT4gKFxuICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgIGNsYXNzTmFtZT17cy5jbGllbnR9XG4gICAgICAgICAgICBrZXk9e2NsaWVudC5pZH1cbiAgICAgICAgICAgIHNyYz17Y2xpZW50Lmljb259XG4gICAgICAgICAgICBhbHQ9XCJjbGluZXRcIlxuICAgICAgICAgIC8+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgICA8c3Bhbj5cbiAgICAgICAgPExpbmsgdG89XCIvY3VzdG9tZXJzXCI+Y2xpY2sgaGVyZSBmb3IgbW9yZS4uLjwvTGluaz5cbiAgICAgIDwvc3Bhbj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5QWNoaWV2ZWRTb0ZhciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZENvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hY2hpZXZlZEhlYWRpbmd9PlxuICAgICAgICBXaGF0IHdlIGhhdmUgYWNoaWV2ZWQgPHNwYW4gLz5cbiAgICAgICAgc28gZmFyXG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkUm93fT5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDI1NSwgMTAyLCAwLCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MuY2xpZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvY2xpZW50cy5zdmdcIiBhbHQ9XCJwcm9maWxlXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MuY2xpZW50c0hpZ2hsaWdodH1gfT4xNTArPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPkNsaWVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5zdHVkZW50c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvc3R1ZGVudHMuc3ZnXCIgYWx0PVwic3R1ZGVudHNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5zdHVkZW50c0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDMgTGFraCtcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+U3R1ZGVudHM8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMCwgMTE1LCAyNTUsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy50ZXN0c31gfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvdGVzdHMuc3ZnXCIgYWx0PVwidGVzdHNcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy50ZXN0c0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDMgTWlsbGlvbitcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+VGVzdHMgQXR0ZW1wdGVkPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT17cy5jYXJkfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDAsIDE3MiwgMzgsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5xdWVzdGlvbnN9YH0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3F1ZXN0aW9ucy5zdmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJxdWVzdGlvbnNcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MucXVlc3Rpb25zSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMjAgTWlsbGlvbitcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnN1YlRleHR9PlF1ZXN0aW9ucyBTb2x2ZWQ8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgLy8gTGl2ZSBPbkNsaWNrIEhhbmRsZXJcbiAgbGl2ZVRvZ2dsZUhhbmRsZXIgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxpdmVUb2dnbGU6ICF0aGlzLnN0YXRlLmxpdmVUb2dnbGUgfSk7XG4gIH07XG4gIGxlY3R1cmVUb2dnbGVIYW5kbGVyID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBsZWN0dXJlVG9nZ2xlOiAhdGhpcy5zdGF0ZS5sZWN0dXJlVG9nZ2xlIH0pO1xuICB9O1xuICBhc3NpZ25tZW50VG9nZ2xlSGFuZGxlciA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgYXNzaWdubWVudFRvZ2dsZTogIXRoaXMuc3RhdGUuYXNzaWdubWVudFRvZ2dsZSB9KTtcbiAgfTtcbiAgZG91YnRUb2dnbGVIYW5kbGVyID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkb3VidFRvZ2dsZTogIXRoaXMuc3RhdGUuZG91YnRUb2dnbGUgfSk7XG4gIH07XG5cbiAgLy8gTGl2ZSBTZWN0aW9uXG4gIGRpc3BsYXlMaXZlU2VjdGlvbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9waWNJbWFnZX0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZWFjaC9vbGRfTGl2ZS5zdmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJsaXZlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+TGl2ZTwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlYWNoQ29udGVudH0+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuZ2V0cmFua3N9XG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9nZXRyYW5rcyBjb3B5LnN2Z1wiXG4gICAgICAgICAgICAgICAgYWx0PVwiZ2V0cmFua3NcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAmbmJzcDsrJm5ic3A7XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Muem9vbX1cbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL1pvb20tTG9nby5wbmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cInpvb20tbG9nb1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICZuYnNwO0ludGVncmF0aW9uIFpvb20gaXMgaW50ZWdyYXRlZCB3aXRoIEdldFJhbmtzLiBBbGxcbiAgICAgICAgICAgICAgQmVuZWZpdHMtSW4sIEFsbCBMaW1pdGF0aW9ucy1vdXRcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBObyBtYXR0ZXIgd2hlcmUgZWR1Y2F0aW9uIHRha2VzIHBsYWNlLCAmcXVvdDtFZ25pZnkgTGl2ZSB3aXRoIFpvb21cbiAgICAgICAgICAgICAgJnF1b3Q7IGNhbiBoZWxwIGVuZ2FnZSBzdHVkZW50cyxmYWN1bHR5IGFuZCBzdGFmZiBmb3IgbGVhcm5pbmcsXG4gICAgICAgICAgICAgIGNvbGxhYm9yYXRpb24gYW5kIGFkbWluaXN0cmF0aW9uLlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5saXZlcGFydH1gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZWFjaC9saXZlLndlYnBcIiBhbHQ9XCJsaXZlXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIC8vIExlY3R1cmUgc2VjdGlvblxuICBkaXNwbGF5SW5kZXBlbmRlbmNlID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+SW5kZXBlbmRlbmNlOjwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIFlvdXIgWm9vbSBhY2NvdW50IHdpbGwgYmUgdXNlZC4gQWxsIHRoZSBmZWF0dXJlcyBhbmQgcHJvdmlzaW9ucyBvZlxuICAgICAgICAgICAgICB5b3VyIFpvb20gYWNjb3VudCBzaGFsbCBiZSBhdmFpbGFibGUgd2l0aCBhbiBhZGRlZCBhZHZhbnRhZ2Ugb2ZcbiAgICAgICAgICAgICAgbG9naW4gcmVzdHJpY3Rpb25zIG9ubHkgZm9yIHNlbGVjdGVkIHN0dWRlbnRzLlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuYXNzaWdubWVudHBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvaW5kZXBlbmRlbmNlLnN2Z1wiIGFsdD1cImluZGVwZW5kZW5jZVwiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbiAgLy8gRG91YnRzIFNlY3Rpb25cbiAgZGlzcGxheUZsZXhpYmlsaXR5ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2V9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5GbGV4aWJpbGl0eTo8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBBbnkgbnVtYmVyIG9mIFpvb20gYWNjb3VudHMgY2FuIGJlIHVzZWQuIERpZmZlcmVudCBab29tIGFjY291bnRzXG4gICAgICAgICAgICAgIGNhbiBiZSBtYXBwZWQgd2l0aCBkaWZmZXJlbnQgQ2xhc3MgU2Vzc2lvbnMgdGh1cyBhbGxvd2luZyBtdWx0aXBsZVxuICAgICAgICAgICAgICB0ZWFjaGVycyB0byB1c2UgdGhlaXIgcGVyc29uYWwgYWNjb3VudHMuXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5kb3VidHBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvZmxleGliaWxpdHkud2VicFwiIGFsdD1cImZsZXhpYmlsaXR5XCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlTZWN1cmVBY2Nlc3MgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5TZWN1cmUgQWNjZXNzOjwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy50ZXh0Y29udGVudH0+XG4gICAgICAgICAgICAgIE9ubHkgc2VsZWN0ZWQgc3R1ZGVudHMgd2hvIGhhdmUgYWNjZXNzIHRvIHRoZSBBcHAgY2FuIG5vdyBhY2Nlc3NcbiAgICAgICAgICAgICAgWm9vbSBjbGFzcywgbm8gbmVlZCB0byBzaGFyZSBNZWV0aW5nIElEIGV2ZXJ5dGltZSwgTm8gcmFuZG9tXG4gICAgICAgICAgICAgIGFjY2Vzcy5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLmFzc2lnbm1lbnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL2xpdmUud2VicFwiIGFsdD1cInNlY3VyZUFjY2Vzc1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5TXVsdGlwbGVVc2VzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2V9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5NdWx0aXBsZSB1c2VzIG9mIFpvb206PC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgWm9vbSBmYWNpbGl0eSBhdmFpbGFibGUgZm9yIGxpdmUgQ2xhc3MgYW5kIERvdWJ0cyBtb2R1bGVzLiBTb29uXG4gICAgICAgICAgICAgIFpvb20gaXMgYmVpbmcgaW50ZWdyYXRlZCB0byB0aGUgb25saW5lIEV4YW0gbW9kdWxlIGZvciBvbmxpbmUgRXhhbVxuICAgICAgICAgICAgICBwcm9jdG9yaW5nLlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuZG91YnRwYXJ0fWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1RlYWNoL211bHRpcGxlX3VzZXMud2VicFwiIGFsdD1cIm11bHRpLXVzZXNcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheU9yZ2FuaXNlZENvbnRlbnQgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5PcmdhbmlzZWQgQ29udGVudDo8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBOb3cgeW91IGRvbnQgaGF2ZSB0byBpbnZpdGUgc3R1ZGVudHMgYnkgc2VuZGluZyBzZXBhcmF0ZSBtZWV0aW5nXG4gICAgICAgICAgICAgIG5vdGlmaWNhdGlvbnMgYW5kIHJlbWluZGVycyB0byBwYXJlbnRzLiBUaGlzIHdpbGwgYWxsb3cgYWxsIHRoZVxuICAgICAgICAgICAgICBab29tIGNsYXNzZXMgdG8gYmUgbGlzdGVkIGFuZCBzZWVuIG9uIHRoZSBzdHVkZW50cyBkYXNoYm9hcmRcbiAgICAgICAgICAgICAgc2NoZWR1bGVkIGFzIGEgcGFydCBvZiBjdXJyaWN1bHVtLlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuYXNzaWdubWVudHBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZWFjaC9vcmdhbmlzZWRfY29udGVudC53ZWJwXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJzZWN1cmVBY2Nlc3NcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUFsbG93UmVwbGF5ID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyX3JldmVyc2V9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5BbGxvdyBSZXBsYXk6PC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgWm9vbSBtZWV0aW5nIHJlY29yZGluZ3MgY2FuIGJlIHVwbG9hZGVkIGZvciBsYXRlciByZXBsYXkgYnlcbiAgICAgICAgICAgICAgc3R1ZGVudHMuXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5kb3VidHBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvYWxsb3dfcmVwbGF5LndlYnBcIiBhbHQ9XCJtdWx0aS11c2VzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlBdHRlbmRhbmNlUmVjb3JkID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+QXR0ZW5kYW5jZSBSZWNvcmQ6PC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgTm93IGl0cyBwb3NzaWJsZSB0byBtYWludGFpbiBvcmdhbml6ZWQgcmVjb3JkIG9mIGF0dGVuZGFuY2UgaW5cbiAgICAgICAgICAgICAgWm9vbSBDbGFzcyBhbG9uZyB3aXRoIG90aGVyIGF0dGVuZGFuY2UgYW5kIGRpZ2l0YWwgbGVhcm5pbmdcbiAgICAgICAgICAgICAgYWN0aXZpdHkgcmVjb3JkIG9mIHRoZSBzdHVkZW50LlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MuYXNzaWdubWVudHBhcnR9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVhY2gvZmxleGliaWxpdHkud2VicFwiIGFsdD1cInNlY3VyZUFjY2Vzc1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5RGlmZmVyZW5jZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZUNvbnRhaW5lcn0+XG4gICAgICA8aDE+XG4gICAgICAgIEdldFJhbmtzICsgWm9vbSBpc1xuICAgICAgICA8YnIgLz4gbW9yZSBwb3dlcmZ1bCB0aGFuIFpvb20gYWxvbmVcbiAgICAgIDwvaDE+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbldyYXBwZXJ9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9uVGV4dH0+TmVnYXRpdmUgUG9pbnRzPC9zcGFuPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYm94fSAke3MubmVnYXRpdmV9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbldyYXBwZXJ9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9uVGV4dH0+UG9zaXRpdmUgUG9pbnRzPC9zcGFuPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYm94fSAke3MucG9zaXRpdmV9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5ab29tIG9ubHk8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb259PkdlUmFua3MrWm9vbTwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21wYXJpc2lvbn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90V3JhcHBlcn0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5kb3R9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWJsZURhdGF9PlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIE5vdGhpbmcgUGVyc29uYWxpemVkLiBZb3UgYmVjb21lIGEgcGFydCBvZiBhIHBvcnRhbCBhbmQgcG9wdWxhcml6ZVxuICAgICAgICAgICAgICBpdC4gWW91ciBCcmFuZCBpZGVudGl0eSBpcyBsb3N0LlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4+U3R1ZGVudCBkYXRhICYgQ29udGVudCBpcyBzaGFyZWQgb24gdGhlIHBvcnRhbDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH0gJHtzLnJlZH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgUGVyc29uYWxpemVkIEFQUCB3aXRoIEluc3RpdHV0ZSBOYW1lIGFuZCBMb2dvIHN0cmVuZ3RoZW5zIHlvdXJcbiAgICAgICAgICAgICAgQnJhbmRcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBQZXJzb25hbGl6ZWQgU3lzdGVtIHRoYXQgeW91IGNhbiBhbHNvIGJ1eSBhbmQgaW5zdGFsbCBvbiB5b3VyXG4gICAgICAgICAgICAgIHNlcnZlclxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIFNlY3VyZSBDb250ZW50IGNhbiBiZSB2aWV3ZWQgYnkgVXNlcnMgb25seSBpbnNpZGUgdGhlIEFQUFxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdFdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZG90fWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFibGVEYXRhfT5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBEb2VzIG5vdCBhbGxvdyByZXBsYXkuIFRoaXMgc2F2ZXMgYmFuZHdpZHRoIGFuZCByZWR1Y2UgY29zdCBieVxuICAgICAgICAgICAgICBjb21wcm9taXNpbmcgdGhlIGVzc2VudGlhbCBuZWVkIG9mIFNlbGYgTGVhcm5pbmdcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBJZiBpbnRlcm5ldCBpcyBsb3N0IGR1cmluZyBzZXNzaW9uLCB0aGF0IG11Y2ggc2Vzc2lvbiBpcyBtaXNzZWQuXG4gICAgICAgICAgICAgIFN0dWRlbnQgaXMgbmV2ZXIgYWJsZSB0byB2aWV3IGl0IGJhY2suIE5vIHByb3Zpc2lvbiB0byBnbyBiYWNrXG4gICAgICAgICAgICAgIGR1cmluZyBsaXZlIHNlc3Npb25cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBOb3RoaW5nIGlzIHN0b3JlZCBvbmxpbmUuIExpdmUgc2Vzc2lvbiB2aWRlbyBpcyBEb3dubG9hZGFibGUgYW5kXG4gICAgICAgICAgICAgIGZyZWVseSBkaXN0cmlidXRhYmxlIGFzIGxpbmsgb3IgZmlsZS4gTWFuYWdlIHlvdXIgZmlsZXMgbG9jYWxseVxuICAgICAgICAgICAgICBhZnRlciBkb3dubG9hZGluZ1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tcGFyaXNpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdFdyYXBwZXJ9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZG90fSAke3MucmVkfWB9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFibGVEYXRhfT5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICBMZWN0dXJlcyBhcmUgYXV0byByZWNvcmRlZCBkdXJpbmcgbGl2ZSBzZXNzaW9uIGFuZCBhdmFpbGFibGUgZm9yXG4gICAgICAgICAgICAgIHJlcGxheSBtdWx0aXBsZSB0aW1lc1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIFN0dWRlbnQgd2lsbCBub3QgbWlzcyBhbnl0aGluZyBpZiBpbnRlcm5ldCBpcyBsb3N0LCBoZSBjYW4gc3RhcnRcbiAgICAgICAgICAgICAgYWdhaW4gYW5kIHdhdGNoIHdoZXJlIGhlIGxlZnQuIFBvc3NpYmxlIHRvIGdvIGJhY2sgaW4gbGl2ZSBsZWN0dXJlXG4gICAgICAgICAgICAgIHRvIHJldmlldyBhIHRvcGljIGFuZCB0aGVuIHJldHVybiB0byBsaXZlIG1vbWVudFxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgIFZpZGVvcyBhcmUgc3RvcmVkIGluIFNhZmUgR29vZ2xlIFNlcnZlcnMgaW4geW91ciBmdWxsIGFjY2Vzcywgbm9cbiAgICAgICAgICAgICAgaGFzc2xlIG9mIG1haW50YWluaW5nIGxvY2FsbHlcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5ObyBhdHRlbmRhbmNlIGFuZCBwYXJ0aWNpcGF0aW9uIFJlY29yZDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH0gJHtzLnJlZH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5BdHRlbmRhbmNlIHJlY29yZCBvZiBldmVyeSBhY3Rpdml0eSBpcyBtYWludGFpbmVkLjwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5UaGVyZSBpcyBjb3N0IHBlciBicm9hZGNhc3Rlcjwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH0gJHtzLnJlZH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5NdWx0aXBsZSB0ZWFjaGVycyBjYW4gY2FzdCB1c2luZyBzaW5nbGUgbGljZW5zZTwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgTm8gY29udHJvbCBvdmVyIHVzZXIgaWRlbnRpdHkuIFVzZXJzIGNhbiBlbnRlciB3aXRoIHJhbmRvbSBuYW1lc1xuICAgICAgICAgICAgICBsaWtlIOKAmHJvY2t5MTIz4oCZIGFuZCBjcmVhdGUgbnVpc2FuY2VcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbXBhcmlzaW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3RXcmFwcGVyfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmRvdH0gJHtzLnJlZH1gfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhYmxlRGF0YX0+XG4gICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgUmVzdHJpY3RlZCBMb2dpbiBhbmQgQWNjZXNzIENvbnRyb2w6IFVzZXIgY2Fubm90IHNlbmQgYW55IGluZGVjZW50XG4gICAgICAgICAgICAgIG1lc3NhZ2UgYmVjYXVzZSByZWdpc3RlcmVkIG5hbWUgYW5kIG51bWJlciBvZiBzZW5kZXIgaXMgdmlzaWJsZVxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlDdXN0b21lcnMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuY3VzdG9tZXJzX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jdXN0b21lcl9yZXZpZXd9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jdXN0b21lckxvZ299PlxuICAgICAgICAgIDxpbWcgc3JjPVwiaW1hZ2VzL2N1c3RvbWVycy9GcmFtZSAxMTM1LmpwZ1wiIGFsdD1cIlwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zcmlDaGFpdGFueWFUZXh0fT5cbiAgICAgICAgICDigJxMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsXG4gICAgICAgICAgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1c1xuICAgICAgICAgIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXJcbiAgICAgICAgICBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlz4oCdXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdXRob3JXcmFwcGVyfT5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYXV0aG9yX3RpdGxlfT5TcnVqYW4gU2FnYXI8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmFib3V0X2F1dGhvcn0+XG4gICAgICAgICAgICBBY2FkYW1pYyBoZWFkLDxzcGFuPlNyaSBDaGFpdGFueWE8L3NwYW4+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWxsY3VzdG9tZXJzfT5cbiAgICAgICAgICA8c3Bhbj5zZWUgYWxsIGN1c3RvbWVyczwvc3Bhbj5cbiAgICAgICAgICA8aW1nIHNyYz1cImltYWdlcy9pY29ucy9hcnJvd1JpZ2h0LnBuZ1wiIGFsdD1cIlwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdXRob3JpbWdib3h9PlxuICAgICAgICA8aW1nIHNyYz1cImltYWdlcy9pY29ucy9zcnVqYW5TYWdhci5wbmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG4gIGRpc3BsYXlBdmFpbGFibGVPbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdmFpbGFibGVDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlUm93fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlQ29udGVudFNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZVRpdGxlfT5cbiAgICAgICAgICAgIFdl4oCZcmUgPGJyIC8+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlfT5hdmFpbGFibGU8L3NwYW4+IG9uXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mucm93fT5cbiAgICAgICAgICAgIHtQTEFURk9STVMubWFwKGl0ZW0gPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGF0Zm9ybUNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PVwiXCIgaGVpZ2h0PVwiNDBweFwiIHdpZHRoPVwiNDBweFwiIC8+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhdGZvcm19PntpdGVtLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYXRmb3JtT3N9PntpdGVtLmF2YWlsYWJsZX08L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdG9yZX0+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxheVN0b3JlLndlYnBcIlxuICAgICAgICAgICAgICBhbHQ9XCJQbGF5IFN0b3JlXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnBsYXlzdG9yZX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvYXBwU3RvcmUud2VicFwiXG4gICAgICAgICAgICAgIGFsdD1cIkFwcCBTdG9yZVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5hcHBzdG9yZX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxpbWdcbiAgICAgICAgICBjbGFzc05hbWU9e3MuZGVza3RvcEltYWdlfVxuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxhdGZvcm1zLnBuZ1wiXG4gICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIHt0aGlzLmRpc3BsYXlUZWFjaFNlY3Rpb24oKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUNsaWVudHMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUFjaGlldmVkU29GYXIoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUxpdmVTZWN0aW9uKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlJbmRlcGVuZGVuY2UoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUZsZXhpYmlsaXR5KCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlTZWN1cmVBY2Nlc3MoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheU11bHRpcGxlVXNlcygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5T3JnYW5pc2VkQ29udGVudCgpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5QWxsb3dSZXBsYXkoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUF0dGVuZGFuY2VSZWNvcmQoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheURpZmZlcmVuY2UoKX1cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheUN1c3RvbWVycygpICovfVxuICAgICAgICB7dGhpcy5kaXNwbGF5QXZhaWxhYmxlT24oKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShUZWFjaCk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1RlYWNoLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vVGVhY2guc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1RlYWNoLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnY29tcG9uZW50cy9MYXlvdXQnO1xuaW1wb3J0IFRlYWNoIGZyb20gJy4vVGVhY2gnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdHZXRSYW5rcyBieSBFZ25pZnk6IEFzc2Vzc21lbnQgJiBBbmFseXRpY3MgUGxhdGZvcm0nLFxuICAgIGNodW5rOiBbJ0xpdmUgQ2xhc3NlcyddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dCBmb290ZXJBc2g+XG4gICAgICAgIDxUZWFjaCAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBV0E7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFUQTtBQUNBO0FBWkE7QUF3QkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbEVBO0FBQ0E7QUF6QkE7QUFrR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFqQkE7QUFDQTtBQW5HQTtBQXdIQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXREQTtBQUNBO0FBekhBO0FBc0xBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUF4TEE7QUF5TEE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTNMQTtBQTRMQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBOUxBO0FBK0xBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFqTUE7QUFtTUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBeENBO0FBQ0E7QUFwTUE7QUFtUEE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW5CQTtBQUNBO0FBcFBBO0FBNlFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFuQkE7QUFDQTtBQTlRQTtBQXVTQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbkJBO0FBQ0E7QUF4U0E7QUFpVUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW5CQTtBQUNBO0FBbFVBO0FBMlZBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF2QkE7QUFDQTtBQTVWQTtBQXlYQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbEJBO0FBQ0E7QUExWEE7QUFrWkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW5CQTtBQUNBO0FBblpBO0FBNGFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE1SUE7QUFDQTtBQTdhQTtBQWtrQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF4QkE7QUFDQTtBQW5rQkE7QUE4bEJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEvQkE7QUFDQTtBQTdsQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBRkE7QUFRQTtBQUNBO0FBQ0E7OztBQTJuQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTs7OztBQXpwQkE7QUFDQTtBQTJwQkE7Ozs7Ozs7QUNscUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFZQTs7OztBIiwic291cmNlUm9vdCI6IiJ9