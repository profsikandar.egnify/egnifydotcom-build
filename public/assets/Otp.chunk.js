(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Otp"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/otp/Otp.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Otp-root-3_HHi {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.Otp-intro-29Jt_ {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.Otp-introLeftPane-3f1ds {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.Otp-jeet-3ssDG {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.Otp-coachingInstitutes-2hftY {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.Otp-introContent-nSCzx {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.Otp-introRightPane-3w8NH {\n  padding-top: 5% !important;\n}\n\n.Otp-introRightPane-3w8NH img {\n    width: 100%;\n  }\n\n.Otp-content-_6A1r {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.Otp-contentImageDiv-eFW5W {\n  height: 136px;\n  width: 136px;\n}\n\n.Otp-screenImage-27Z1f {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.Otp-headingContent-3qFSG {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.Otp-mainContent-2ViNK {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.Otp-testimonialsTitle-1OuV3 {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.Otp-testimonialsTitle-1OuV3::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.Otp-contentSubdiv-yvn5L {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Otp-jeeDetailsContainer-1kBqS {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.Otp-jeeDetailsContainer-1kBqS .Otp-subSection-1insS {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.Otp-jeeTitle-y-j2t {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.Otp-jeeDescription-27sHd {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.Otp-title2-164qD {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.Otp-underline-ffzz8 {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .Otp-root-3_HHi {\n    padding: 0;\n    margin: 0;\n  }\n\n  .Otp-contentForSmall-3bhwV {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .Otp-contentImageDivSmall-1OrJN {\n    width: 100%;\n  }\n\n    .Otp-contentImageDivSmall-1OrJN img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .Otp-screenImageSmall-2S7YZ {\n    text-align: center;\n  }\n\n  .Otp-headingContentSmall-1TYj4 {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .Otp-mainContentSmall-1pfgV {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .Otp-testimonialsTitle-1OuV3 {\n    margin-bottom: 18%;\n  }\n\n  .Otp-testimonialsTitle-1OuV3::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .Otp-introLeftPane-3f1ds {\n    width: 100%;\n  }\n\n  .Otp-jeet-3ssDG {\n    text-align: center;\n  }\n\n  .Otp-coachingInstitutes-2hftY {\n    text-align: center;\n  }\n\n  .Otp-introContent-nSCzx {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .Otp-link-2uVcd {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Otp-title2-164qD {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Otp-underline-ffzz8 {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .Otp-jeeDetailsContainer-1kBqS {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .Otp-jeeDetailsContainer-1kBqS .Otp-subSection-1insS {\n      width: 100%;\n    }\n\n  .Otp-jeeTitle-y-j2t {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .Otp-title2-164qD {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .Otp-underline-ffzz8 {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .Otp-introContent-nSCzx {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .Otp-introContent-nSCzx {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .Otp-jeeTitle-y-j2t {\n    font-size: 30px;\n  }\n\n  .Otp-jeeDescription-27sHd {\n    font-size: 18px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/Otp.scss"],"names":[],"mappings":"AAAA;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,mBAAmB;EACnB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;EACZ,4DAA4D;UACpD,oDAAoD;CAC7D;;AAED;EACE,aAAa;EACb,iBAAiB;EACjB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,eAAe;EACf,2BAA2B;EAC3B,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,eAAe;EACf,iBAAiB;EACjB,sBAAsB;EACtB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;EACf,oBAAoB;EACpB,oBAAoB;EACpB,aAAa;CACd;;AAED;EACE,2BAA2B;CAC5B;;AAED;IACI,YAAY;GACb;;AAEH;EACE,iBAAiB;EACjB,kBAAkB;EAClB,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,kBAAkB;CACnB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,kBAAkB;CACnB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,mBAAmB;EACnB,kBAAkB;EAClB,YAAY;CACb;;AAED;EACE,YAAY;EACZ,eAAe;EACf,eAAe;EACf,YAAY;EACZ,YAAY;EACZ,8BAA8B;EAC9B,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,6BAA6B;EAC7B,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,0BAA0B;CAC3B;;AAED;IACI,WAAW;IACX,4BAA4B;IAC5B,aAAa;IACb,qBAAqB;GACtB;;AAEH;EACE,gBAAgB;EAChB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;EACZ,aAAa;EACb,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,UAAU;EACV,0BAA0B;CAC3B;;AAED;EACE;IACE,WAAW;IACX,UAAU;GACX;;EAED;IACE,aAAa;IACb,YAAY;IACZ,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;IAEC;MACE,cAAc;MACd,aAAa;KACd;;EAEH;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,kBAAkB;GACnB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,kBAAkB;IAClB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,aAAa;IACb,cAAc;GACf;;IAEC;MACE,YAAY;KACb;;EAEH;IACE,8BAA8B;IAC9B,gBAAgB;GACjB;;EAED;IACE,8BAA8B;IAC9B,mBAAmB;IACnB,2BAA2B;IAC3B,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;IAClB,qBAAqB;GACtB;;EAED;IACE,iBAAiB;IACjB,WAAW;GACZ;CACF;;AAED;EACE;IACE,YAAY;GACb;CACF;;AAED;EACE;IACE,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;GAClB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;GACjB;CACF","file":"Otp.scss","sourcesContent":[".root {\n  background-color: #fff;\n}\n\nbutton {\n  height: 56px;\n  border-radius: 4px;\n  background-color: #ec4c6f;\n  font-size: 14px;\n  font-weight: 600;\n  color: #fff;\n  -webkit-box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n          box-shadow: 2px 4px 12px 0 rgba(236, 76, 111, 0.24);\n}\n\n.intro {\n  height: 100%;\n  padding-left: 5%;\n  padding-right: 3%;\n  margin-bottom: 5%;\n}\n\n.introLeftPane {\n  color: #3e3e5f;\n  padding-top: 5% !important;\n  height: 100%;\n}\n\n.jeet {\n  font-size: 40px;\n  font-weight: 300;\n  margin-bottom: 8px;\n}\n\n.coachingInstitutes {\n  font-size: 20px;\n  color: #5f6368;\n  font-weight: 300;\n  letter-spacing: 0.8px;\n  margin-bottom: 40px;\n}\n\n.introContent {\n  font-size: 24px;\n  line-height: 1.5;\n  letter-spacing: 1px;\n  color: #5f6368;\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  width: 420px;\n}\n\n.introRightPane {\n  padding-top: 5% !important;\n}\n\n.introRightPane img {\n    width: 100%;\n  }\n\n.content {\n  padding-left: 5%;\n  padding-right: 5%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 4%;\n}\n\n.contentImageDiv {\n  height: 136px;\n  width: 136px;\n}\n\n.screenImage {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-top: 68px;\n}\n\n.headingContent {\n  width: 100%;\n  font-size: 32px;\n  color: #3e3e5f;\n  margin-bottom: 24px;\n  padding-left: 20px;\n}\n\n.mainContent {\n  width: 100%;\n  font-size: 20px;\n  line-height: 1.6;\n  color: #5f6368;\n  padding-left: 20px;\n}\n\n.testimonialsTitle {\n  height: 38px;\n  font-size: 32px;\n  font-weight: 500;\n  color: #3e3e5f;\n  text-align: center;\n  margin-bottom: 4%;\n  padding: 2%;\n}\n\n.testimonialsTitle::after {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 48px;\n  height: 2px;\n  border-top: solid 3px #ec4c6f;\n  padding-top: 12px;\n}\n\n.contentSubdiv {\n  height: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.jeeDetailsContainer {\n  height: 100%;\n  min-height: 300px !important;\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 5%;\n  background-color: #3e3e5f;\n}\n\n.jeeDetailsContainer .subSection {\n    width: 25%;\n    min-height: 80px !important;\n    height: 100%;\n    display: inline-grid;\n  }\n\n.jeeTitle {\n  font-size: 48px;\n  text-align: center;\n  color: #ffab00;\n}\n\n.jeeDescription {\n  font-size: 16px;\n  font-weight: 600;\n  text-align: center;\n  padding: 5%;\n  color: #fff;\n  height: 70px;\n  width: 100%;\n}\n\n.title2 {\n  font-size: 32px;\n  font-weight: 500;\n  text-align: center;\n  color: #fff;\n  padding: 3%;\n}\n\n.underline {\n  margin-top: 8px;\n  margin-left: 48%;\n  width: 4%;\n  border: solid 1px #ec4c6f;\n}\n\n@media only screen and (max-width: 960px) {\n  .root {\n    padding: 0;\n    margin: 0;\n  }\n\n  .contentForSmall {\n    height: 100%;\n    padding: 6%;\n    text-align: center;\n  }\n\n  .contentImageDivSmall {\n    width: 100%;\n  }\n\n    .contentImageDivSmall img {\n      height: 100px;\n      width: 100px;\n    }\n\n  .screenImageSmall {\n    text-align: center;\n  }\n\n  .headingContentSmall {\n    font-size: 24px;\n    color: #3e3e5f;\n    margin-bottom: 8%;\n  }\n\n  .mainContentSmall {\n    font-size: 16px;\n    line-height: 1.6;\n    color: #5f6368;\n    margin-bottom: 8%;\n  }\n\n  .testimonialsTitle {\n    margin-bottom: 18%;\n  }\n\n  .testimonialsTitle::after {\n    margin-top: 4px;\n  }\n\n  button {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 800px) {\n  .introLeftPane {\n    width: 100%;\n  }\n\n  .jeet {\n    text-align: center;\n  }\n\n  .coachingInstitutes {\n    text-align: center;\n  }\n\n  .introContent {\n    width: 100%;\n    padding-left: 15%;\n    padding-right: 15%;\n    text-align: center;\n  }\n\n  button {\n    width: 200px;\n  }\n\n  .link {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (max-width: 1024px) {\n  .jeeDetailsContainer {\n    height: 100%;\n    margin-top: 0;\n  }\n\n    .jeeDetailsContainer .subSection {\n      width: 100%;\n    }\n\n  .jeeTitle {\n    border-right: none !important;\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    border-right: none !important;\n    margin-bottom: 10%;\n    font-size: 18px !important;\n    padding-left: 20%;\n    padding-right: 20%;\n  }\n\n  .title2 {\n    font-size: 24px;\n    padding-top: 40px;\n    padding-bottom: 20px;\n  }\n\n  .underline {\n    margin-left: 43%;\n    width: 15%;\n  }\n}\n\n@media only screen and (device-width: 1024px) {\n  .introContent {\n    width: 100%;\n  }\n}\n\n@media only screen and (max-width: 500px) {\n  .introContent {\n    width: 100%;\n    text-align: center;\n    padding-left: 0;\n    padding-right: 0;\n  }\n\n  .jeeTitle {\n    font-size: 30px;\n  }\n\n  .jeeDescription {\n    font-size: 18px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "Otp-root-3_HHi",
	"intro": "Otp-intro-29Jt_",
	"introLeftPane": "Otp-introLeftPane-3f1ds",
	"jeet": "Otp-jeet-3ssDG",
	"coachingInstitutes": "Otp-coachingInstitutes-2hftY",
	"introContent": "Otp-introContent-nSCzx",
	"introRightPane": "Otp-introRightPane-3w8NH",
	"content": "Otp-content-_6A1r",
	"contentImageDiv": "Otp-contentImageDiv-eFW5W",
	"screenImage": "Otp-screenImage-27Z1f",
	"headingContent": "Otp-headingContent-3qFSG",
	"mainContent": "Otp-mainContent-2ViNK",
	"testimonialsTitle": "Otp-testimonialsTitle-1OuV3",
	"contentSubdiv": "Otp-contentSubdiv-yvn5L",
	"jeeDetailsContainer": "Otp-jeeDetailsContainer-1kBqS",
	"subSection": "Otp-subSection-1insS",
	"jeeTitle": "Otp-jeeTitle-y-j2t",
	"jeeDescription": "Otp-jeeDescription-27sHd",
	"title2": "Otp-title2-164qD",
	"underline": "Otp-underline-ffzz8",
	"contentForSmall": "Otp-contentForSmall-3bhwV",
	"contentImageDivSmall": "Otp-contentImageDivSmall-1OrJN",
	"screenImageSmall": "Otp-screenImageSmall-2S7YZ",
	"headingContentSmall": "Otp-headingContentSmall-1TYj4",
	"mainContentSmall": "Otp-mainContentSmall-1pfgV",
	"link": "Otp-link-2uVcd"
};

/***/ }),

/***/ "./src/routes/products/otp/Otp.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Slideshow__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Slideshow/Slideshow.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Otp_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/otp/Otp.scss");
/* harmony import */ var _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Otp_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/Otp.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import PropTypes from 'prop-types';







var STATS = [{
  title: '80k+',
  description: 'Students attempted jee mains in 2018'
}, {
  title: '17.5%',
  description: 'cut-off clear rate'
}, {
  title: '13k',
  description: 'Students qualified jee mains in 2018'
}, {
  title: '40%',
  description: 'Students of ap and telangana'
}];
var CONTENT = [{
  headerContent: 'Flexible Assessments',
  mainContent: 'Simulating the JEE experience, one can conduct an assessment in online and offline modes simultaneously. Provide students with the best in online assessment experience and do away with the hassle of maintaining expensive hardware.',
  icon: '/images/products/otp/icon-1.svg',
  screenImage: '/images/products/otp/screen1.png'
}, {
  headerContent: 'Own Question Papers',
  mainContent: 'The quality of assessments itself is the most important criteria to understand the true potential of Students. As such we provide you with the capability to upload your own question paper to maintain optimum quality standards throughout your assessments.',
  icon: '/images/products/otp/icon-2.svg',
  screenImage: '/images/products/otp/screen2.png'
}, {
  headerContent: 'Student profile',
  mainContent: 'Students get access to their test analysis, across marks, errors, concepts and behavior, and customized tests for practice and learning material. A one-stop-shop to help your students prepare and perform better.',
  icon: '/images/products/otp/icon-3.svg',
  screenImage: '/images/products/otp/screen3.png'
}, {
  headerContent: 'Mark Analysis',
  mainContent: 'Marks Analysis helps you understand how your students are performing in a snapshot and identify who needs help to clear the cut-off.',
  icon: '/images/products/jeet/percent.svg',
  screenImage: '/images/products/jeet/screen1.png'
}, {
  headerContent: 'Error Analysis',
  mainContent: 'Error Analysis helps you determine the questions in a test which majority of your students have not attempted or attempted wrongly to enable error practice.',
  icon: '/images/products/jeet/idle.svg',
  screenImage: '/images/products/jeet/screen6.png'
}, {
  headerContent: 'Personalised Learning/Teaching Plans',
  mainContent: 'No two students are same and each of them has a different path to success. Our personalized learning plans target individual students’ weak areas and help them score better, every time. Our Teaching Plans enables you to teach the topics that matter more to your students and help create a better learning experience. ',
  icon: '/images/products/otp/icon-6.svg',
  screenImage: '/images/products/otp/screen6.png'
}];
var SLIDESHOW = [{
  image: '/images/home/clients/telangana.png',
  name: 'Mr. Srinivas Kannan',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class.'
}, {
  image: '',
  name: 'Mr. Srinivas Murthy',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class.'
}, {
  image: '',
  name: 'Mr. Andrew Tag',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}, {
  image: '',
  name: 'Mr. Mussolini',
  role: 'Teacher, Sri Chaitanya',
  content: 'I hate to talk about the grading workload, but grading this class’s unit test – just this one class – took me almost four hours. So, that’s a lot of time outside of class'
}];

var Otp =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Otp, _React$Component);

  function Otp() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Otp);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Otp)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "makeAlignment", function (contentIndex, key) {
      if (contentIndex % 2 === key) {
        if (key === 0) {
          return 'flex-start';
        }

        return 'flex-end';
      }

      return 'center';
    });

    return _this;
  }

  _createClass(Otp, [{
    key: "componentDidMount",
    // static propTypes = {
    //
    // };
    value: function componentDidMount() {
      react_ga__WEBPACK_IMPORTED_MODULE_4__["default"].initialize(window.App.googleTrackingId, {
        debug: false
      });
      react_ga__WEBPACK_IMPORTED_MODULE_4__["default"].pageview(window.location.href);
    }
    /**
     * @description Returns what alignment should the content take eg. flex-start flex end center
     * @param contentIndex,key
     * @author Sushruth
     * */

  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.root, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.intro, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introLeftPane, " col m4 s12"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeet,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, "PREP"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.coachingInstitutes,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, "For Online Assessments"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, "Empowers Institutes to create, schedule and conduct online assessments and access in-depth competence improve student exam readiness."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/request-demo",
        className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.link,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }, "REQUEST A DEMO"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.introRightPane, " col m8 s12"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/products/otp/otp-hero-2x.png",
        alt: "",
        style: {
          width: '90%'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDetailsContainer, " row"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.title2),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }, "Our Stats", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.underline),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      })), STATS.map(function (details, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subSection),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 150
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeTitle),
          style: {
            borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 151
          },
          __self: this
        }, details.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.jeeDescription),
          style: {
            borderRight: index < STATS.length - 1 ? 'solid 1px rgba(139, 139, 223, 0.3)' : ''
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 162
          },
          __self: this
        }, details.description.toUpperCase()));
      })), CONTENT.map(function (content, contentIndex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "row",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 177
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content, " row hide-on-xs"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 178
          },
          __self: this
        }, [0, 1].map(function (key) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: contentIndex % 2 === key ? "col m5 l5 ".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv) : "col m7 l7 ".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentSubdiv),
            style: {
              justifyContent: _this2.makeAlignment(contentIndex, key)
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 180
            },
            __self: this
          }, contentIndex % 2 === key ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 191
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDiv,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 192
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.icon,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 193
            },
            __self: this
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 195
            },
            __self: this
          }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContent,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 198
            },
            __self: this
          }, content.mainContent)) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImage,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 201
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            alt: "",
            src: content.screenImage,
            width: "87.6%",
            height: "54.45%",
            style: {
              width: contentIndex === 5 ? '70%' : ''
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 202
            },
            __self: this
          })));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentForSmall, " row hide-on-m-and-up"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 214
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentImageDivSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 215
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 216
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headingContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 218
          },
          __self: this
        }, content.headerContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mainContentSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 221
          },
          __self: this
        }, content.mainContent), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.screenImageSmall,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 222
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          alt: "",
          src: content.screenImage,
          width: "87.6%",
          height: "54.45%",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 223
          },
          __self: this
        }))));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        style: {
          paddingTop: '64px',
          marginBottom: '5%'
        },
        className: "row hide",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 233
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testimonialsTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 237
        },
        __self: this
      }, "Why our clients love Egnify"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slideshow__WEBPACK_IMPORTED_MODULE_3__["default"], {
        data: SLIDESHOW,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_5__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }));
      return view;
    }
  }]);

  return Otp;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Otp_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Otp));

/***/ }),

/***/ "./src/routes/products/otp/Otp.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/otp/Otp.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/otp/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Otp__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/products/otp/Otp.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/otp/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify',
              chunks: ['Otp'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Otp__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3RwLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9vdHAvT3RwLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9vdHAvT3RwLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvb3RwL090cC5zY3NzPzRlNDAiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9vdHAvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5PdHAtcm9vdC0zX0hIaSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5PdHAtaW50cm8tMjlKdF8ge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xcbiAgbWFyZ2luLWJvdHRvbTogNSU7XFxufVxcblxcbi5PdHAtaW50cm9MZWZ0UGFuZS0zZjFkcyB7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbiAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG4uT3RwLWplZXQtM3NzREcge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLk90cC1jb2FjaGluZ0luc3RpdHV0ZXMtMmhmdFkge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuOHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLk90cC1pbnRyb0NvbnRlbnQtblNDengge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xcbiAgd2lkdGg6IDQyMHB4O1xcbn1cXG5cXG4uT3RwLWludHJvUmlnaHRQYW5lLTN3OE5IIHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uT3RwLWludHJvUmlnaHRQYW5lLTN3OE5IIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbi5PdHAtY29udGVudC1fNkExciB7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogNSU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG59XFxuXFxuLk90cC1jb250ZW50SW1hZ2VEaXYtZUZXNVcge1xcbiAgaGVpZ2h0OiAxMzZweDtcXG4gIHdpZHRoOiAxMzZweDtcXG59XFxuXFxuLk90cC1zY3JlZW5JbWFnZS0yN1oxZiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNjhweDtcXG59XFxuXFxuLk90cC1oZWFkaW5nQ29udGVudC0zcUZTRyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLk90cC1tYWluQ29udGVudC0yVmlOSyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLk90cC10ZXN0aW1vbmlhbHNUaXRsZS0xT3VWMyB7XFxuICBoZWlnaHQ6IDM4cHg7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBmb250LXdlaWdodDogNTAwO1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA0JTtcXG4gIHBhZGRpbmc6IDIlO1xcbn1cXG5cXG4uT3RwLXRlc3RpbW9uaWFsc1RpdGxlLTFPdVYzOjphZnRlciB7XFxuICBjb250ZW50OiAnJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICB3aWR0aDogNDhweDtcXG4gIGhlaWdodDogMnB4O1xcbiAgYm9yZGVyLXRvcDogc29saWQgM3B4ICNlYzRjNmY7XFxuICBwYWRkaW5nLXRvcDogMTJweDtcXG59XFxuXFxuLk90cC1jb250ZW50U3ViZGl2LXl2bjVMIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLk90cC1qZWVEZXRhaWxzQ29udGFpbmVyLTFrQnFTIHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDMwMHB4ICFpbXBvcnRhbnQ7XFxuICBwYWRkaW5nLXJpZ2h0OiA1JTtcXG4gIHBhZGRpbmctbGVmdDogNSU7XFxuICBwYWRkaW5nLWJvdHRvbTogNSU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2UzZTVmO1xcbn1cXG5cXG4uT3RwLWplZURldGFpbHNDb250YWluZXItMWtCcVMgLk90cC1zdWJTZWN0aW9uLTFpbnNTIHtcXG4gICAgd2lkdGg6IDI1JTtcXG4gICAgbWluLWhlaWdodDogODBweCAhaW1wb3J0YW50O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ncmlkO1xcbiAgfVxcblxcbi5PdHAtamVlVGl0bGUteS1qMnQge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICNmZmFiMDA7XFxufVxcblxcbi5PdHAtamVlRGVzY3JpcHRpb24tMjdzSGQge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDUlO1xcbiAgY29sb3I6ICNmZmY7XFxuICBoZWlnaHQ6IDcwcHg7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLk90cC10aXRsZTItMTY0cUQge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMyU7XFxufVxcblxcbi5PdHAtdW5kZXJsaW5lLWZmeno4IHtcXG4gIG1hcmdpbi10b3A6IDhweDtcXG4gIG1hcmdpbi1sZWZ0OiA0OCU7XFxuICB3aWR0aDogNCU7XFxuICBib3JkZXI6IHNvbGlkIDFweCAjZWM0YzZmO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk2MHB4KSB7XFxuICAuT3RwLXJvb3QtM19ISGkge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuT3RwLWNvbnRlbnRGb3JTbWFsbC0zYmh3ViB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5PdHAtY29udGVudEltYWdlRGl2U21hbGwtMU9ySk4ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gICAgLk90cC1jb250ZW50SW1hZ2VEaXZTbWFsbC0xT3JKTiBpbWcge1xcbiAgICAgIGhlaWdodDogMTAwcHg7XFxuICAgICAgd2lkdGg6IDEwMHB4O1xcbiAgICB9XFxuXFxuICAuT3RwLXNjcmVlbkltYWdlU21hbGwtMlM3WVoge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuT3RwLWhlYWRpbmdDb250ZW50U21hbGwtMVRZajQge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC5PdHAtbWFpbkNvbnRlbnRTbWFsbC0xcGZnViB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNjtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLk90cC10ZXN0aW1vbmlhbHNUaXRsZS0xT3VWMyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC5PdHAtdGVzdGltb25pYWxzVGl0bGUtMU91VjM6OmFmdGVyIHtcXG4gICAgbWFyZ2luLXRvcDogNHB4O1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5PdHAtaW50cm9MZWZ0UGFuZS0zZjFkcyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLk90cC1qZWV0LTNzc0RHIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLk90cC1jb2FjaGluZ0luc3RpdHV0ZXMtMmhmdFkge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuT3RwLWludHJvQ29udGVudC1uU0N6eCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gICAgcGFkZGluZy1yaWdodDogMTUlO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICB9XFxuXFxuICAuT3RwLWxpbmstMnVWY2Qge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuT3RwLWplZURlc2NyaXB0aW9uLTI3c0hkIHtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcXG4gICAgZm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XFxuICAgIHBhZGRpbmctbGVmdDogMjAlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XFxuICB9XFxuXFxuICAuT3RwLXRpdGxlMi0xNjRxRCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgcGFkZGluZy10b3A6IDQwcHg7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLk90cC11bmRlcmxpbmUtZmZ6ejgge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xcbiAgLk90cC1qZWVEZXRhaWxzQ29udGFpbmVyLTFrQnFTIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXJnaW4tdG9wOiAwO1xcbiAgfVxcblxcbiAgICAuT3RwLWplZURldGFpbHNDb250YWluZXItMWtCcVMgLk90cC1zdWJTZWN0aW9uLTFpbnNTIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbiAgLk90cC1qZWVUaXRsZS15LWoydCB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuT3RwLWplZURlc2NyaXB0aW9uLTI3c0hkIHtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcXG4gICAgZm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XFxuICAgIHBhZGRpbmctbGVmdDogMjAlO1xcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XFxuICB9XFxuXFxuICAuT3RwLXRpdGxlMi0xNjRxRCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgcGFkZGluZy10b3A6IDQwcHg7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLk90cC11bmRlcmxpbmUtZmZ6ejgge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDEwMjRweCkge1xcbiAgLk90cC1pbnRyb0NvbnRlbnQtblNDengge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xcbiAgLk90cC1pbnRyb0NvbnRlbnQtblNDengge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICB9XFxuXFxuICAuT3RwLWplZVRpdGxlLXktajJ0IHtcXG4gICAgZm9udC1zaXplOiAzMHB4O1xcbiAgfVxcblxcbiAgLk90cC1qZWVEZXNjcmlwdGlvbi0yN3NIZCB7XFxuICAgIGZvbnQtc2l6ZTogMThweDtcXG4gIH1cXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL290cC9PdHAuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFBQTtFQUNFLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLDREQUE0RDtVQUNwRCxvREFBb0Q7Q0FDN0Q7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsMkJBQTJCO0VBQzNCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7QUFFRDtJQUNJLFlBQVk7R0FDYjs7QUFFSDtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGNBQWM7RUFDZCxhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLGVBQWU7RUFDZixZQUFZO0VBQ1osWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQiwwQkFBMEI7Q0FDM0I7O0FBRUQ7SUFDSSxXQUFXO0lBQ1gsNEJBQTRCO0lBQzVCLGFBQWE7SUFDYixxQkFBcUI7R0FDdEI7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0U7SUFDRSxXQUFXO0lBQ1gsVUFBVTtHQUNYOztFQUVEO0lBQ0UsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0lBRUM7TUFDRSxjQUFjO01BQ2QsYUFBYTtLQUNkOztFQUVIO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7R0FDakI7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxzQkFBc0I7UUFDbEIsd0JBQXdCO0dBQzdCOztFQUVEO0lBQ0UsOEJBQThCO0lBQzlCLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0Isa0JBQWtCO0lBQ2xCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIscUJBQXFCO0dBQ3RCOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLFdBQVc7R0FDWjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxhQUFhO0lBQ2IsY0FBYztHQUNmOztJQUVDO01BQ0UsWUFBWTtLQUNiOztFQUVIO0lBQ0UsOEJBQThCO0lBQzlCLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLGlCQUFpQjtJQUNqQixXQUFXO0dBQ1o7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjtDQUNGXCIsXCJmaWxlXCI6XCJPdHAuc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIucm9vdCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG5idXR0b24ge1xcbiAgaGVpZ2h0OiA1NnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjNGM2ZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDRweCAxMnB4IDAgcmdiYSgyMzYsIDc2LCAxMTEsIDAuMjQpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAycHggNHB4IDEycHggMCByZ2JhKDIzNiwgNzYsIDExMSwgMC4yNCk7XFxufVxcblxcbi5pbnRybyB7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwYWRkaW5nLWxlZnQ6IDUlO1xcbiAgcGFkZGluZy1yaWdodDogMyU7XFxuICBtYXJnaW4tYm90dG9tOiA1JTtcXG59XFxuXFxuLmludHJvTGVmdFBhbmUge1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLmplZXQge1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uaW50cm9Db250ZW50IHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcXG4gIHdpZHRoOiA0MjBweDtcXG59XFxuXFxuLmludHJvUmlnaHRQYW5lIHtcXG4gIHBhZGRpbmctdG9wOiA1JSAhaW1wb3J0YW50O1xcbn1cXG5cXG4uaW50cm9SaWdodFBhbmUgaW1nIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuLmNvbnRlbnQge1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNCU7XFxufVxcblxcbi5jb250ZW50SW1hZ2VEaXYge1xcbiAgaGVpZ2h0OiAxMzZweDtcXG4gIHdpZHRoOiAxMzZweDtcXG59XFxuXFxuLnNjcmVlbkltYWdlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmctdG9wOiA2OHB4O1xcbn1cXG5cXG4uaGVhZGluZ0NvbnRlbnQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDMycHg7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XFxufVxcblxcbi5tYWluQ29udGVudCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICBjb2xvcjogIzVmNjM2ODtcXG4gIHBhZGRpbmctbGVmdDogMjBweDtcXG59XFxuXFxuLnRlc3RpbW9uaWFsc1RpdGxlIHtcXG4gIGhlaWdodDogMzhweDtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogIzNlM2U1ZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQlO1xcbiAgcGFkZGluZzogMiU7XFxufVxcblxcbi50ZXN0aW1vbmlhbHNUaXRsZTo6YWZ0ZXIge1xcbiAgY29udGVudDogJyc7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgd2lkdGg6IDQ4cHg7XFxuICBoZWlnaHQ6IDJweDtcXG4gIGJvcmRlci10b3A6IHNvbGlkIDNweCAjZWM0YzZmO1xcbiAgcGFkZGluZy10b3A6IDEycHg7XFxufVxcblxcbi5jb250ZW50U3ViZGl2IHtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmplZURldGFpbHNDb250YWluZXIge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWluLWhlaWdodDogMzAwcHggIWltcG9ydGFudDtcXG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xcbiAgcGFkZGluZy1sZWZ0OiA1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA1JTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZTNlNWY7XFxufVxcblxcbi5qZWVEZXRhaWxzQ29udGFpbmVyIC5zdWJTZWN0aW9uIHtcXG4gICAgd2lkdGg6IDI1JTtcXG4gICAgbWluLWhlaWdodDogODBweCAhaW1wb3J0YW50O1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ncmlkO1xcbiAgfVxcblxcbi5qZWVUaXRsZSB7XFxuICBmb250LXNpemU6IDQ4cHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogI2ZmYWIwMDtcXG59XFxuXFxuLmplZURlc2NyaXB0aW9uIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA1JTtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgaGVpZ2h0OiA3MHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcblxcbi50aXRsZTIge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMyU7XFxufVxcblxcbi51bmRlcmxpbmUge1xcbiAgbWFyZ2luLXRvcDogOHB4O1xcbiAgbWFyZ2luLWxlZnQ6IDQ4JTtcXG4gIHdpZHRoOiA0JTtcXG4gIGJvcmRlcjogc29saWQgMXB4ICNlYzRjNmY7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcXG4gIC5yb290IHtcXG4gICAgcGFkZGluZzogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLmNvbnRlbnRGb3JTbWFsbCB7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgcGFkZGluZzogNiU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jb250ZW50SW1hZ2VEaXZTbWFsbCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgICAuY29udGVudEltYWdlRGl2U21hbGwgaW1nIHtcXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xcbiAgICAgIHdpZHRoOiAxMDBweDtcXG4gICAgfVxcblxcbiAgLnNjcmVlbkltYWdlU21hbGwge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuaGVhZGluZ0NvbnRlbnRTbWFsbCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICAgIG1hcmdpbi1ib3R0b206IDglO1xcbiAgfVxcblxcbiAgLm1haW5Db250ZW50U21hbGwge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tYm90dG9tOiA4JTtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZSB7XFxuICAgIG1hcmdpbi1ib3R0b206IDE4JTtcXG4gIH1cXG5cXG4gIC50ZXN0aW1vbmlhbHNUaXRsZTo6YWZ0ZXIge1xcbiAgICBtYXJnaW4tdG9wOiA0cHg7XFxuICB9XFxuXFxuICBidXR0b24ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLmludHJvTGVmdFBhbmUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5qZWV0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmNvYWNoaW5nSW5zdGl0dXRlcyB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgcGFkZGluZy1sZWZ0OiAxNSU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgYnV0dG9uIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgfVxcblxcbiAgLmxpbmsge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuamVlRGVzY3JpcHRpb24ge1xcbiAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTAlO1xcbiAgICBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDtcXG4gICAgcGFkZGluZy1sZWZ0OiAyMCU7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcXG4gIH1cXG5cXG4gIC50aXRsZTIge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gIH1cXG5cXG4gIC51bmRlcmxpbmUge1xcbiAgICBtYXJnaW4tbGVmdDogNDMlO1xcbiAgICB3aWR0aDogMTUlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xcbiAgLmplZURldGFpbHNDb250YWluZXIge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1hcmdpbi10b3A6IDA7XFxuICB9XFxuXFxuICAgIC5qZWVEZXRhaWxzQ29udGFpbmVyIC5zdWJTZWN0aW9uIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgfVxcblxcbiAgLmplZVRpdGxlIHtcXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XFxuICAgIGZvbnQtc2l6ZTogMzBweDtcXG4gIH1cXG5cXG4gIC5qZWVEZXNjcmlwdGlvbiB7XFxuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XFxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xcbiAgICBwYWRkaW5nLWxlZnQ6IDIwJTtcXG4gICAgcGFkZGluZy1yaWdodDogMjAlO1xcbiAgfVxcblxcbiAgLnRpdGxlMiB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgcGFkZGluZy10b3A6IDQwcHg7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLnVuZGVybGluZSB7XFxuICAgIG1hcmdpbi1sZWZ0OiA0MyU7XFxuICAgIHdpZHRoOiAxNSU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKGRldmljZS13aWR0aDogMTAyNHB4KSB7XFxuICAuaW50cm9Db250ZW50IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcXG4gIC5pbnRyb0NvbnRlbnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XFxuICB9XFxuXFxuICAuamVlVGl0bGUge1xcbiAgICBmb250LXNpemU6IDMwcHg7XFxuICB9XFxuXFxuICAuamVlRGVzY3JpcHRpb24ge1xcbiAgICBmb250LXNpemU6IDE4cHg7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJyb290XCI6IFwiT3RwLXJvb3QtM19ISGlcIixcblx0XCJpbnRyb1wiOiBcIk90cC1pbnRyby0yOUp0X1wiLFxuXHRcImludHJvTGVmdFBhbmVcIjogXCJPdHAtaW50cm9MZWZ0UGFuZS0zZjFkc1wiLFxuXHRcImplZXRcIjogXCJPdHAtamVldC0zc3NER1wiLFxuXHRcImNvYWNoaW5nSW5zdGl0dXRlc1wiOiBcIk90cC1jb2FjaGluZ0luc3RpdHV0ZXMtMmhmdFlcIixcblx0XCJpbnRyb0NvbnRlbnRcIjogXCJPdHAtaW50cm9Db250ZW50LW5TQ3p4XCIsXG5cdFwiaW50cm9SaWdodFBhbmVcIjogXCJPdHAtaW50cm9SaWdodFBhbmUtM3c4TkhcIixcblx0XCJjb250ZW50XCI6IFwiT3RwLWNvbnRlbnQtXzZBMXJcIixcblx0XCJjb250ZW50SW1hZ2VEaXZcIjogXCJPdHAtY29udGVudEltYWdlRGl2LWVGVzVXXCIsXG5cdFwic2NyZWVuSW1hZ2VcIjogXCJPdHAtc2NyZWVuSW1hZ2UtMjdaMWZcIixcblx0XCJoZWFkaW5nQ29udGVudFwiOiBcIk90cC1oZWFkaW5nQ29udGVudC0zcUZTR1wiLFxuXHRcIm1haW5Db250ZW50XCI6IFwiT3RwLW1haW5Db250ZW50LTJWaU5LXCIsXG5cdFwidGVzdGltb25pYWxzVGl0bGVcIjogXCJPdHAtdGVzdGltb25pYWxzVGl0bGUtMU91VjNcIixcblx0XCJjb250ZW50U3ViZGl2XCI6IFwiT3RwLWNvbnRlbnRTdWJkaXYteXZuNUxcIixcblx0XCJqZWVEZXRhaWxzQ29udGFpbmVyXCI6IFwiT3RwLWplZURldGFpbHNDb250YWluZXItMWtCcVNcIixcblx0XCJzdWJTZWN0aW9uXCI6IFwiT3RwLXN1YlNlY3Rpb24tMWluc1NcIixcblx0XCJqZWVUaXRsZVwiOiBcIk90cC1qZWVUaXRsZS15LWoydFwiLFxuXHRcImplZURlc2NyaXB0aW9uXCI6IFwiT3RwLWplZURlc2NyaXB0aW9uLTI3c0hkXCIsXG5cdFwidGl0bGUyXCI6IFwiT3RwLXRpdGxlMi0xNjRxRFwiLFxuXHRcInVuZGVybGluZVwiOiBcIk90cC11bmRlcmxpbmUtZmZ6ejhcIixcblx0XCJjb250ZW50Rm9yU21hbGxcIjogXCJPdHAtY29udGVudEZvclNtYWxsLTNiaHdWXCIsXG5cdFwiY29udGVudEltYWdlRGl2U21hbGxcIjogXCJPdHAtY29udGVudEltYWdlRGl2U21hbGwtMU9ySk5cIixcblx0XCJzY3JlZW5JbWFnZVNtYWxsXCI6IFwiT3RwLXNjcmVlbkltYWdlU21hbGwtMlM3WVpcIixcblx0XCJoZWFkaW5nQ29udGVudFNtYWxsXCI6IFwiT3RwLWhlYWRpbmdDb250ZW50U21hbGwtMVRZajRcIixcblx0XCJtYWluQ29udGVudFNtYWxsXCI6IFwiT3RwLW1haW5Db250ZW50U21hbGwtMXBmZ1ZcIixcblx0XCJsaW5rXCI6IFwiT3RwLWxpbmstMnVWY2RcIlxufTsiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuLy8gaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgU2xpZGVzaG93IGZyb20gJ2NvbXBvbmVudHMvU2xpZGVzaG93JztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCBSZXF1ZXN0RGVtbyBmcm9tICdjb21wb25lbnRzL1JlcXVlc3REZW1vJztcbmltcG9ydCBzIGZyb20gJy4vT3RwLnNjc3MnO1xuXG5jb25zdCBTVEFUUyA9IFtcbiAgeyB0aXRsZTogJzgwaysnLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIGF0dGVtcHRlZCBqZWUgbWFpbnMgaW4gMjAxOCcgfSxcbiAgeyB0aXRsZTogJzE3LjUlJywgZGVzY3JpcHRpb246ICdjdXQtb2ZmIGNsZWFyIHJhdGUnIH0sXG4gIHsgdGl0bGU6ICcxM2snLCBkZXNjcmlwdGlvbjogJ1N0dWRlbnRzIHF1YWxpZmllZCBqZWUgbWFpbnMgaW4gMjAxOCcgfSxcbiAgeyB0aXRsZTogJzQwJScsIGRlc2NyaXB0aW9uOiAnU3R1ZGVudHMgb2YgYXAgYW5kIHRlbGFuZ2FuYScgfSxcbl07XG5cbmNvbnN0IENPTlRFTlQgPSBbXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnRmxleGlibGUgQXNzZXNzbWVudHMnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1NpbXVsYXRpbmcgdGhlIEpFRSBleHBlcmllbmNlLCBvbmUgY2FuIGNvbmR1Y3QgYW4gYXNzZXNzbWVudCBpbiBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgc2ltdWx0YW5lb3VzbHkuIFByb3ZpZGUgc3R1ZGVudHMgd2l0aCB0aGUgYmVzdCBpbiBvbmxpbmUgYXNzZXNzbWVudCBleHBlcmllbmNlIGFuZCBkbyBhd2F5IHdpdGggdGhlIGhhc3NsZSBvZiBtYWludGFpbmluZyBleHBlbnNpdmUgaGFyZHdhcmUuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvaWNvbi0xLnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9zY3JlZW4xLnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnT3duIFF1ZXN0aW9uIFBhcGVycycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnVGhlIHF1YWxpdHkgb2YgYXNzZXNzbWVudHMgaXRzZWxmIGlzIHRoZSBtb3N0IGltcG9ydGFudCBjcml0ZXJpYSB0byB1bmRlcnN0YW5kIHRoZSB0cnVlIHBvdGVudGlhbCBvZiBTdHVkZW50cy4gQXMgc3VjaCB3ZSBwcm92aWRlIHlvdSB3aXRoIHRoZSBjYXBhYmlsaXR5IHRvIHVwbG9hZCB5b3VyIG93biBxdWVzdGlvbiBwYXBlciB0byBtYWludGFpbiBvcHRpbXVtIHF1YWxpdHkgc3RhbmRhcmRzIHRocm91Z2hvdXQgeW91ciBhc3Nlc3NtZW50cy4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9pY29uLTIuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvb3RwL3NjcmVlbjIucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdTdHVkZW50IHByb2ZpbGUnLFxuICAgIG1haW5Db250ZW50OlxuICAgICAgJ1N0dWRlbnRzIGdldCBhY2Nlc3MgdG8gdGhlaXIgdGVzdCBhbmFseXNpcywgYWNyb3NzIG1hcmtzLCBlcnJvcnMsIGNvbmNlcHRzIGFuZCBiZWhhdmlvciwgYW5kIGN1c3RvbWl6ZWQgdGVzdHMgZm9yIHByYWN0aWNlIGFuZCBsZWFybmluZyBtYXRlcmlhbC4gQSBvbmUtc3RvcC1zaG9wIHRvIGhlbHAgeW91ciBzdHVkZW50cyBwcmVwYXJlIGFuZCBwZXJmb3JtIGJldHRlci4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9pY29uLTMuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvb3RwL3NjcmVlbjMucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdNYXJrIEFuYWx5c2lzJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdNYXJrcyBBbmFseXNpcyBoZWxwcyB5b3UgdW5kZXJzdGFuZCBob3cgeW91ciBzdHVkZW50cyBhcmUgcGVyZm9ybWluZyBpbiBhIHNuYXBzaG90IGFuZCBpZGVudGlmeSB3aG8gbmVlZHMgaGVscCB0byBjbGVhciB0aGUgY3V0LW9mZi4nLFxuICAgIGljb246ICcvaW1hZ2VzL3Byb2R1Y3RzL2plZXQvcGVyY2VudC5zdmcnLFxuICAgIHNjcmVlbkltYWdlOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L3NjcmVlbjEucG5nJyxcbiAgfSxcbiAge1xuICAgIGhlYWRlckNvbnRlbnQ6ICdFcnJvciBBbmFseXNpcycsXG4gICAgbWFpbkNvbnRlbnQ6XG4gICAgICAnRXJyb3IgQW5hbHlzaXMgaGVscHMgeW91IGRldGVybWluZSB0aGUgcXVlc3Rpb25zIGluIGEgdGVzdCB3aGljaCBtYWpvcml0eSBvZiB5b3VyIHN0dWRlbnRzIGhhdmUgbm90IGF0dGVtcHRlZCBvciBhdHRlbXB0ZWQgd3JvbmdseSB0byBlbmFibGUgZXJyb3IgcHJhY3RpY2UuJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9qZWV0L2lkbGUuc3ZnJyxcbiAgICBzY3JlZW5JbWFnZTogJy9pbWFnZXMvcHJvZHVjdHMvamVldC9zY3JlZW42LnBuZycsXG4gIH0sXG4gIHtcbiAgICBoZWFkZXJDb250ZW50OiAnUGVyc29uYWxpc2VkIExlYXJuaW5nL1RlYWNoaW5nIFBsYW5zJyxcbiAgICBtYWluQ29udGVudDpcbiAgICAgICdObyB0d28gc3R1ZGVudHMgYXJlIHNhbWUgYW5kIGVhY2ggb2YgdGhlbSBoYXMgYSBkaWZmZXJlbnQgcGF0aCB0byBzdWNjZXNzLiBPdXIgcGVyc29uYWxpemVkIGxlYXJuaW5nIHBsYW5zIHRhcmdldCBpbmRpdmlkdWFsIHN0dWRlbnRz4oCZIHdlYWsgYXJlYXMgYW5kIGhlbHAgdGhlbSBzY29yZSBiZXR0ZXIsIGV2ZXJ5IHRpbWUuIE91ciBUZWFjaGluZyBQbGFucyBlbmFibGVzIHlvdSB0byB0ZWFjaCB0aGUgdG9waWNzIHRoYXQgbWF0dGVyIG1vcmUgdG8geW91ciBzdHVkZW50cyBhbmQgaGVscCBjcmVhdGUgYSBiZXR0ZXIgbGVhcm5pbmcgZXhwZXJpZW5jZS4gJyxcbiAgICBpY29uOiAnL2ltYWdlcy9wcm9kdWN0cy9vdHAvaWNvbi02LnN2ZycsXG4gICAgc2NyZWVuSW1hZ2U6ICcvaW1hZ2VzL3Byb2R1Y3RzL290cC9zY3JlZW42LnBuZycsXG4gIH0sXG5dO1xuXG5jb25zdCBTTElERVNIT1cgPSBbXG4gIHtcbiAgICBpbWFnZTogJy9pbWFnZXMvaG9tZS9jbGllbnRzL3RlbGFuZ2FuYS5wbmcnLFxuICAgIG5hbWU6ICdNci4gU3Jpbml2YXMgS2FubmFuJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzLicsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJycsXG4gICAgbmFtZTogJ01yLiBTcmluaXZhcyBNdXJ0aHknLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MuJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnJyxcbiAgICBuYW1lOiAnTXIuIEFuZHJldyBUYWcnLFxuICAgIHJvbGU6ICdUZWFjaGVyLCBTcmkgQ2hhaXRhbnlhJyxcbiAgICBjb250ZW50OlxuICAgICAgJ0kgaGF0ZSB0byB0YWxrIGFib3V0IHRoZSBncmFkaW5nIHdvcmtsb2FkLCBidXQgZ3JhZGluZyB0aGlzIGNsYXNz4oCZcyB1bml0IHRlc3Qg4oCTIGp1c3QgdGhpcyBvbmUgY2xhc3Mg4oCTIHRvb2sgbWUgYWxtb3N0IGZvdXIgaG91cnMuIFNvLCB0aGF04oCZcyBhIGxvdCBvZiB0aW1lIG91dHNpZGUgb2YgY2xhc3MnLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICcnLFxuICAgIG5hbWU6ICdNci4gTXVzc29saW5pJyxcbiAgICByb2xlOiAnVGVhY2hlciwgU3JpIENoYWl0YW55YScsXG4gICAgY29udGVudDpcbiAgICAgICdJIGhhdGUgdG8gdGFsayBhYm91dCB0aGUgZ3JhZGluZyB3b3JrbG9hZCwgYnV0IGdyYWRpbmcgdGhpcyBjbGFzc+KAmXMgdW5pdCB0ZXN0IOKAkyBqdXN0IHRoaXMgb25lIGNsYXNzIOKAkyB0b29rIG1lIGFsbW9zdCBmb3VyIGhvdXJzLiBTbywgdGhhdOKAmXMgYSBsb3Qgb2YgdGltZSBvdXRzaWRlIG9mIGNsYXNzJyxcbiAgfSxcbl07XG5cbmNsYXNzIE90cCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIC8vIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gIC8vXG4gIC8vIH07XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgUmVhY3RHQS5pbml0aWFsaXplKHdpbmRvdy5BcHAuZ29vZ2xlVHJhY2tpbmdJZCwge1xuICAgICAgZGVidWc6IGZhbHNlLFxuICAgIH0pO1xuICAgIFJlYWN0R0EucGFnZXZpZXcod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBkZXNjcmlwdGlvbiBSZXR1cm5zIHdoYXQgYWxpZ25tZW50IHNob3VsZCB0aGUgY29udGVudCB0YWtlIGVnLiBmbGV4LXN0YXJ0IGZsZXggZW5kIGNlbnRlclxuICAgKiBAcGFyYW0gY29udGVudEluZGV4LGtleVxuICAgKiBAYXV0aG9yIFN1c2hydXRoXG4gICAqICovXG4gIG1ha2VBbGlnbm1lbnQgPSAoY29udGVudEluZGV4LCBrZXkpID0+IHtcbiAgICBpZiAoY29udGVudEluZGV4ICUgMiA9PT0ga2V5KSB7XG4gICAgICBpZiAoa2V5ID09PSAwKSB7XG4gICAgICAgIHJldHVybiAnZmxleC1zdGFydCc7XG4gICAgICB9XG4gICAgICByZXR1cm4gJ2ZsZXgtZW5kJztcbiAgICB9XG4gICAgcmV0dXJuICdjZW50ZXInO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB2aWV3ID0gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm9vdH0gcm93YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmludHJvfSByb3dgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbnRyb0xlZnRQYW5lfSBjb2wgbTQgczEyYH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5qZWV0fT5QUkVQPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb2FjaGluZ0luc3RpdHV0ZXN9PkZvciBPbmxpbmUgQXNzZXNzbWVudHM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmludHJvQ29udGVudH0+XG4gICAgICAgICAgICAgIEVtcG93ZXJzIEluc3RpdHV0ZXMgdG8gY3JlYXRlLCBzY2hlZHVsZSBhbmQgY29uZHVjdCBvbmxpbmVcbiAgICAgICAgICAgICAgYXNzZXNzbWVudHMgYW5kIGFjY2VzcyBpbi1kZXB0aCBjb21wZXRlbmNlIGltcHJvdmUgc3R1ZGVudCBleGFtXG4gICAgICAgICAgICAgIHJlYWRpbmVzcy5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPExpbmsgdG89XCIvcmVxdWVzdC1kZW1vXCIgY2xhc3NOYW1lPXtzLmxpbmt9PlxuICAgICAgICAgICAgICA8YnV0dG9uPlJFUVVFU1QgQSBERU1PPC9idXR0b24+XG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW50cm9SaWdodFBhbmV9IGNvbCBtOCBzMTJgfT5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9wcm9kdWN0cy9vdHAvb3RwLWhlcm8tMngucG5nXCJcbiAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6ICc5MCUnIH19XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuamVlRGV0YWlsc0NvbnRhaW5lcn0gcm93YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MudGl0bGUyfWB9PlxuICAgICAgICAgICAgT3VyIFN0YXRzXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy51bmRlcmxpbmV9YH0gLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICB7U1RBVFMubWFwKChkZXRhaWxzLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Muc3ViU2VjdGlvbn1gfT5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5qZWVUaXRsZX1gfVxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBib3JkZXJSaWdodDpcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPCBTVEFUUy5sZW5ndGggLSAxXG4gICAgICAgICAgICAgICAgICAgICAgPyAnc29saWQgMXB4IHJnYmEoMTM5LCAxMzksIDIyMywgMC4zKSdcbiAgICAgICAgICAgICAgICAgICAgICA6ICcnLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7ZGV0YWlscy50aXRsZX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuamVlRGVzY3JpcHRpb259YH1cbiAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6XG4gICAgICAgICAgICAgICAgICAgIGluZGV4IDwgU1RBVFMubGVuZ3RoIC0gMVxuICAgICAgICAgICAgICAgICAgICAgID8gJ3NvbGlkIDFweCByZ2JhKDEzOSwgMTM5LCAyMjMsIDAuMyknXG4gICAgICAgICAgICAgICAgICAgICAgOiAnJyxcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2RldGFpbHMuZGVzY3JpcHRpb24udG9VcHBlckNhc2UoKX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHtDT05URU5ULm1hcCgoY29udGVudCwgY29udGVudEluZGV4KSA9PiAoXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmNvbnRlbnR9IHJvdyBoaWRlLW9uLXhzYH0+XG4gICAgICAgICAgICAgIHtbMCwgMV0ubWFwKGtleSA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgICAgY29udGVudEluZGV4ICUgMiA9PT0ga2V5XG4gICAgICAgICAgICAgICAgICAgICAgPyBgY29sIG01IGw1ICR7cy5jb250ZW50U3ViZGl2fWBcbiAgICAgICAgICAgICAgICAgICAgICA6IGBjb2wgbTcgbDcgJHtzLmNvbnRlbnRTdWJkaXZ9YFxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeUNvbnRlbnQ6IHRoaXMubWFrZUFsaWdubWVudChjb250ZW50SW5kZXgsIGtleSksXG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIHtjb250ZW50SW5kZXggJSAyID09PSBrZXkgPyAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudEltYWdlRGl2fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50Lmljb259IC8+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ0NvbnRlbnR9PlxuICAgICAgICAgICAgICAgICAgICAgICAge2NvbnRlbnQuaGVhZGVyQ29udGVudH1cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tYWluQ29udGVudH0+e2NvbnRlbnQubWFpbkNvbnRlbnR9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2NyZWVuSW1hZ2V9PlxuICAgICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzcmM9e2NvbnRlbnQuc2NyZWVuSW1hZ2V9XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjg3LjYlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjU0LjQ1JVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogY29udGVudEluZGV4ID09PSA1ID8gJzcwJScgOiAnJyB9fVxuICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmNvbnRlbnRGb3JTbWFsbH0gcm93IGhpZGUtb24tbS1hbmQtdXBgfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudEltYWdlRGl2U21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtjb250ZW50Lmljb259IC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nQ29udGVudFNtYWxsfT5cbiAgICAgICAgICAgICAgICB7Y29udGVudC5oZWFkZXJDb250ZW50fVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWFpbkNvbnRlbnRTbWFsbH0+e2NvbnRlbnQubWFpbkNvbnRlbnR9PC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNjcmVlbkltYWdlU21hbGx9PlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICBzcmM9e2NvbnRlbnQuc2NyZWVuSW1hZ2V9XG4gICAgICAgICAgICAgICAgICB3aWR0aD1cIjg3LjYlXCJcbiAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjU0LjQ1JVwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSl9XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nVG9wOiAnNjRweCcsIG1hcmdpbkJvdHRvbTogJzUlJyB9fVxuICAgICAgICAgIGNsYXNzTmFtZT1cInJvdyBoaWRlXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RpbW9uaWFsc1RpdGxlfT5XaHkgb3VyIGNsaWVudHMgbG92ZSBFZ25pZnk8L2Rpdj5cbiAgICAgICAgICA8U2xpZGVzaG93IGRhdGE9e1NMSURFU0hPV30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxSZXF1ZXN0RGVtbyAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgICByZXR1cm4gdmlldztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKE90cCk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL090cC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL090cC5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vT3RwLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBPdHAgZnJvbSAnLi9PdHAnO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0VnbmlmeScsXG4gICAgY2h1bmtzOiBbJ090cCddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPE90cCAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBTEE7QUFTQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQVFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7QUF6QkE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7Ozs7Ozs7O0FBZUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBREE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFiQTtBQTJCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBS0E7QUFDQTtBQURBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF2QkE7QUFtQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQS9DQTtBQTBEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7Ozs7QUF2SkE7QUFDQTtBQXlKQTs7Ozs7OztBQ3RQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7OztBQVlBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=