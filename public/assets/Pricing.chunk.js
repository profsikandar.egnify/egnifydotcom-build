(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pricing"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Modal-bodyOverlay-3fSN4 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.Modal-modal-9QpxU {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,+BAA+B;EAC/B,iBAAiB;CAClB;;AAED;EACE,eAAe;EACf,qCAAqC;EACrC,cAAc;EACd,mBAAmB;CACpB;;AAED;;;;;;;EAOE","file":"Modal.scss","sourcesContent":[".bodyOverlay {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: hidden;\n}\n\n.modal {\n  overflow: auto;\n  background-color: rgb(255, 255, 255);\n  outline: none;\n  position: relative;\n}\n\n/*\n@media only screen and (min-width: 1361px) {\n  .marksModal {\n    width: 600px;\n    margin: -133px 0 0 -300px;\n  }\n}\n*/\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bodyOverlay": "Modal-bodyOverlay-3fSN4",
	"modal": "Modal-modal-9QpxU"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/pricing/Pricing.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Pricing-pricingHeader-2MLoH {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.Pricing-darkBackground-17ag8 {\n  background-color: #25282b !important;\n}\n\n.Pricing-price_title-3Yqn8 {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.Pricing-price_content-5qlsh {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.Pricing-modulesContainer-9h2Yz {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 72px;\n}\n\n.Pricing-modulesHeader-zfiUY {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.Pricing-modulesList-3_DuR {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(5, 1fr);\n  gap: 44px;\n  margin-top: 24px;\n}\n\n.Pricing-module-dy7g3 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Pricing-imageWrapper-2cu95 {\n  width: 52px;\n  height: 52px;\n  border-radius: 26px;\n  -webkit-box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-bottom: 12px;\n}\n\n.Pricing-imageWrapper-2cu95 img {\n  width: 32px;\n  height: 32px;\n}\n\n.Pricing-scrollTop-Aag07 {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Pricing-scrollTop-Aag07 img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Pricing-price_content-5qlsh p {\n  font-size: 16px;\n  opacity: 0.7;\n  line-height: 2;\n  line-height: 32px;\n}\n\n.Pricing-annual-30ie_ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  margin-right: 50px;\n  height: auto;\n}\n\n.Pricing-toggle_btn-3t9M8 {\n  margin-right: 50px;\n}\n\n.Pricing-annual_text-c6h-G {\n  font-size: 14px;\n  padding-bottom: 4px;\n  line-height: normal;\n  border-bottom: 2px solid #fff;\n}\n\n.Pricing-annual_offer-2VBwG {\n  opacity: 0.8;\n  color: #f36;\n  margin-bottom: 4px;\n  display: inline-block;\n}\n\n.Pricing-underline-VNfR5 {\n  border-bottom: 2px solid black;\n}\n\n.Pricing-pricing_container-2DZ9K {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.Pricing-plans-3oyRG {\n  padding: 0 48px 32px 48px;\n  padding: 0 3rem 2rem 3rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Pricing-plan-cbyB7 {\n  width: 270px;\n  height: 477px;\n  margin: 0 6px;\n  border-radius: 8px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  position: relative;\n}\n\n.Pricing-plan-cbyB7 .Pricing-innerCard-XD-XR {\n    background-color: #fff;\n    position: absolute;\n    padding: 32px 0;\n    padding: 2rem 0;\n    width: 100%;\n    height: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    z-index: 1;\n  }\n\n.Pricing-symbol-1PZl7 {\n  border-radius: 100%;\n}\n\n.Pricing-pricing_title-18Vxm {\n  font-size: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  width: 100%;\n  color: #25282b;\n  margin-bottom: 50px;\n}\n\n.Pricing-toggle_outer-TDyU0 {\n  width: 56px;\n  height: 31px;\n  border-radius: 24px;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 3px;\n}\n\n.Pricing-toggle_on-KyTo6 {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.Pricing-toggle_inner-dxNua {\n  width: 24.9px;\n  height: 24.9px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n.Pricing-plan_title_students-ixHtF {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.Pricing-plan_title-2z_k0 p {\n    margin: 0;\n  }\n\n.Pricing-plan_title_price-2Zx49 {\n  font-weight: 600;\n  color: #25282b;\n  font-size: 24px;\n  opacity: 1;\n  display: inline-block;\n  margin-bottom: 8px;\n}\n\n.Pricing-price-rctgP {\n  color: #25282b;\n  font-size: 32px;\n  font-weight: 600;\n}\n\n.Pricing-price-rctgP p {\n    margin: 32px 0;\n  }\n\n.Pricing-price_desc-4xVbA {\n  color: #000;\n  opacity: 0.6;\n  line-height: 1.5;\n}\n\n.Pricing-price_desc-4xVbA p {\n    margin: 0 0 24px 0;\n  }\n\n.Pricing-plan-cbyB7 button {\n  margin-top: 40px;\n  padding: 0% 15%;\n  text-transform: capitalize;\n  font-weight: normal;\n  border-radius: 24px;\n}\n\n.Pricing-price_contact-7uNig a {\n  text-decoration: underline;\n  // margin-bottom: 500px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.Pricing-pricePerStudentHighlight-qAWcu {\n  font-size: 24px;\n  font-weight: 600;\n  background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n  -webkit-text-fill-color: transparent;\n  -webkit-background-clip: text;\n  color: #25282b;\n}\n\n.Pricing-buyButton-3g0NI {\n  background-color: #f36;\n}\n\n.Pricing-enterprise-1O-mF {\n  width: 91.5%;\n  margin: auto;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 8px;\n  padding: 42px 200px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 56px;\n}\n\n.Pricing-enterpriseHeading-2Xm68 {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.Pricing-enterprisecontext-3AD9J {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.Pricing-contactus-XfL9G {\n  padding: 8px 20px;\n  border-radius: 24px;\n  background-color: #f36;\n  color: #fff;\n  font-size: 14px;\n  line-height: 20px;\n}\n\n/* .faq_container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  color: #25282b;\n  background-color: #f7f7f7;\n}\n\n.faq_title p {\n  font-size: 40px;\n  font-weight: 600;\n}\n\n.expand_all {\n  margin: 0% 10%;\n  align-self: flex-end;\n  font-size: 16px;\n  color: #25282b;\n  cursor: pointer;\n  border-bottom: 1px solid black;\n  width: max-content;\n}\n\n.accordian {\n  width: 80%;\n} */\n\n/* .border {\n  cursor: pointer;\n  border-bottom: 1px solid #c7c7c7;\n  padding: 44px 0 40px 0;\n}\n\n.border:last-child {\n  border-bottom: none;\n} */\n\n/* .question {\n  width: 100%;\n  font-size: 16px;\n  color: #25282b;\n  font-weight: bold;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.answer {\n  display: none;\n  font-size: 14px;\n  line-height: 25.4px;\n  width: 85%;\n} */\n\n/* .show .answer {\n  display: block;\n} */\n\n/* .up {\n  display: none;\n} */\n\n/* .show .up {\n  display: block;\n  transform: rotate(180deg);\n}\n\n.show .down {\n  display: none;\n} */\n\n.Pricing-leftArrow-3BcpG,\n.Pricing-rightArrow-1CGiq {\n  display: none;\n}\n\n.Pricing-circle_bg-vbvtk {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.Pricing-square_bg-1fWOk {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n  -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n.Pricing-line_vector_bg-2Zkss {\n  -webkit-transform: rotate(180deg);\n      -ms-transform: rotate(180deg);\n          transform: rotate(180deg);\n  opacity: 0.5;\n  position: absolute;\n  left: 10%;\n}\n\n.Pricing-triangle_bg-1X7I4 {\n  position: absolute;\n  right: 7%;\n}\n\n.Pricing-displayClients-SxgNR {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.Pricing-displayClients-SxgNR h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Pricing-displayClients-SxgNR span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Pricing-displayClients-SxgNR span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q a {\n  width: 259px;\n  height: 127px;\n}\n\n.Pricing-displayClients-SxgNR span a:hover {\n  text-decoration: underline;\n}\n\n.Pricing-clientsWrapper-65G_i {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Pricing-clientsWrapper-65G_i .Pricing-client-14kGt {\n  width: 172px;\n  height: 112px;\n}\n\n.Pricing-achievedContainer-3NBMf {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu {\n    text-align: center;\n    font-size: 40px;\n    line-height: 1.2;\n    color: #25282b;\n    font-weight: 600;\n    margin-bottom: 40px;\n  }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(4, 1fr);\n    // grid-template-rows: repeat(2, 1fr);\n    gap: 16px;\n    gap: 1rem;\n    margin: auto;\n  }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd {\n      width: 270px;\n      height: 202px;\n      font-size: 24px;\n      font-size: 1.5rem;\n      color: rgba(37, 40, 43, 0.6);\n      line-height: 40px;\n      padding: 24px;\n      padding: 1.5rem;\n      border-radius: 0.5rem;\n      -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n              box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n      background-color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI {\n        width: 52px;\n        height: 52px;\n        border-radius: 50%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-pack: center;\n            justify-content: center;\n        -ms-flex-align: center;\n            align-items: center;\n        margin-bottom: 20px;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-clients-2mnCX {\n        background-color: #fff3eb;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-students-1YHpU {\n        background-color: #f7effe;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-tests-3nDUz {\n        background-color: #ffebf0;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-achievedProfile-1jgdI.Pricing-questions-3qJ8D {\n        background-color: #ebffef;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm {\n        font-size: 36px;\n        font-weight: bold;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-questionsHighlight-1HJlR {\n        color: #00ac26;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-testsHighlight-vkapq {\n        color: #f36;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-studentsHighlight-2UuO9 {\n        color: #8c00fe;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-highlight-3-Tsm.Pricing-clientsHighlight-2hUQE {\n        color: #f60;\n      }\n\n.Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL .Pricing-card-3-jMd .Pricing-subText-3PFWz {\n        font-size: 24px;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.Pricing-featured-3vf8L {\n  background-color: #f7f7f7;\n  padding: 56px 64px 40px;\n  padding: 3.5rem 4rem 2.5rem;\n}\n\n.Pricing-featured-3vf8L h2 {\n  text-align: center;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n  margin-top: 0;\n  font-size: 40px;\n  font-size: 2.5rem;\n  line-height: 48px;\n  line-height: 3rem;\n}\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 40px;\n  gap: 2.5rem;\n  margin: auto;\n}\n\n.Pricing-titlebox-1mBcy {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.Pricing-titlebox-1mBcy h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.Pricing-titlebox-1mBcy img {\n    cursor: pointer;\n  }\n\n.Pricing-featured-3vf8L .Pricing-showMedia-6U76q a img {\n  background-color: #fff;\n  width: 100%;\n  height: 100%;\n}\n\n.Pricing-on_news-2bgDh {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-max-content;\n  height: -moz-max-content;\n  height: max-content;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  gap: 12px;\n  margin: 0 auto 56px auto;\n}\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9 {\n    width: 368px;\n    height: 250px;\n    cursor: pointer;\n  }\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9:nth-child(1) {\n    margin-left: 0;\n  }\n\n.Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9:nth-child(3) {\n    margin-right: 0;\n  }\n\n.Pricing-awardsContainer-2PyIg {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 56px 192px;\n  padding: 3.5rem 12rem;\n  background-color: #fff;\n}\n\n.Pricing-awardsContainer-2PyIg .Pricing-awardsTitle-2F_p8 {\n    font-size: 40px;\n    font-size: 2.5rem;\n    font-weight: 600;\n    color: #25282b;\n    margin-bottom: 48px;\n    margin-bottom: 3rem;\n  }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL {\n    display: grid;\n    grid-template-columns: repeat(3, 1fr);\n    gap: 88px;\n    gap: 5.5rem;\n  }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP {\n      width: 240px;\n      width: 15rem;\n      height: 208px;\n      height: 13rem;\n    }\n\n.Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP:nth-child(3n + 2) {\n      margin-right: 0;\n    }\n\n.Pricing-awardsContainer-2PyIg .Pricing-dots-2Jt23 {\n    display: none;\n  }\n\n.Pricing-popupoverlay-32yez {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.Pricing-presspopup-1UCPJ {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.Pricing-player-1FCsc {\n  width: 100%;\n  height: 302px;\n}\n\n@media only screen and (max-width: 990px) {\n  .Pricing-mobilePricingContainer-3w6Qk {\n    width: 100%;\n  }\n\n  .Pricing-modulesContainer-9h2Yz {\n    margin: 64px 0;\n  }\n\n  .Pricing-tabsContainer-1-5am {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    background-color: #f7f7f7;\n  }\n\n  .Pricing-annualtab-dyIQZ {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-right-radius: 8px;\n  }\n\n  .Pricing-monthtab-3Jy_s {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-left-radius: 8px;\n  }\n\n  .Pricing-activeTab-3vi1R {\n    // cursor: pointer;\n    background-color: #f7f7f7;\n    border: 0;\n  }\n\n  .Pricing-mobilePlans-vbHxh {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n    padding-bottom: 12px;\n  }\n\n  .Pricing-mobilePriceCard-32uBZ {\n    padding: 24px 52px 24px 53px;\n    margin: 12px 0;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    border-radius: 8px;\n    max-width: 328px;\n    background-color: #fff;\n  }\n\n  .Pricing-presspopup-1UCPJ {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .Pricing-titlebox-1mBcy {\n    margin-bottom: 16px;\n  }\n\n  .Pricing-titlebox-1mBcy h2 {\n    font-size: 11.5px;\n  }\n\n  .Pricing-modulesList-3_DuR {\n    grid-template-columns: repeat(3, 1fr);\n    grid-column-gap: 40px;\n    grid-row-gap: 20px;\n  }\n\n  .Pricing-imageWrapper-2cu95 {\n    width: 40px;\n    height: 40px;\n  }\n\n  .Pricing-imageWrapper-2cu95 img {\n    width: 24px;\n    height: 24px;\n  }\n\n  .Pricing-pricePerStudentHighlightMobile-36iXF {\n    font-size: 20px;\n    font-weight: 600;\n    background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n    -webkit-text-fill-color: transparent;\n    -webkit-background-clip: text;\n    color: #25282b;\n    opacity: 1 !important;\n  }\n\n  .Pricing-displayClients-SxgNR {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n    background-color: #fff;\n  }\n\n  .Pricing-displayClients-SxgNR h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-client-14kGt {\n    width: 9.75rem;\n    height: 7rem;\n    border: 1px solid rgba(37, 40, 43, 0.16);\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-client-14kGt.Pricing-active-1fA3n {\n    display: block;\n  }\n\n  .Pricing-leftArrow-3BcpG {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    left: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .Pricing-leftArrow-3BcpG span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .Pricing-rightArrow-1CGiq {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    right: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .Pricing-rightArrow-1CGiq span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i span {\n    max-width: 400px;\n  }\n\n  .Pricing-dots-2Jt23 {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n    margin-bottom: 24px;\n  }\n\n  .Pricing-displayClients-SxgNR .Pricing-clientsWrapper-65G_i .Pricing-dots-2Jt23 {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .Pricing-achievedContainer-3NBMf {\n    padding: 1.5rem 1rem;\n    background-image: url('/images/home/Confetti.svg');\n    background-color: #f7f7f7;\n  }\n\n    .Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu {\n      font-size: 1.5rem;\n      padding: 0;\n      margin-bottom: 1.5rem;\n    }\n\n      .Pricing-achievedContainer-3NBMf .Pricing-achievedHeading-hxnwu span::after {\n        content: '\\A';\n        white-space: pre;\n      }\n\n    .Pricing-achievedContainer-3NBMf .Pricing-achievedRow-29TsL {\n      grid-template-columns: 1fr;\n      gap: 0.75rem;\n      max-width: 20.5rem;\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: auto;\n    }\n\n  .Pricing-awardsContainer-2PyIg {\n    padding: 1.5rem;\n  }\n\n    .Pricing-awardsContainer-2PyIg .Pricing-awardsTitle-2F_p8 {\n      font-size: 1.5rem;\n      padding-bottom: 0;\n      margin-bottom: 2rem;\n    }\n\n    .Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      grid-template-columns: 1fr 1fr;\n      gap: 1.5rem 1rem;\n      margin: auto;\n      position: relative;\n    }\n\n      .Pricing-awardsContainer-2PyIg .Pricing-achievedRow-29TsL .Pricing-awardLogo-2pqvP {\n        width: 100%;\n        height: 100%;\n        max-width: 15rem;\n        max-height: 13rem;\n      }\n    .Pricing-price_contact-7uNig a {\n      font-size: 14px;\n      line-height: 21px;\n    }\n\n  .Pricing-featured-3vf8L {\n    padding: 1.5rem 1rem 1.25rem;\n  }\n\n    .Pricing-featured-3vf8L h2 {\n      font-size: 1.5rem;\n      line-height: normal;\n      margin: 0 0 1.5rem 0;\n    }\n\n    .Pricing-featured-3vf8L .Pricing-showMedia-6U76q {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: 0 auto 0 auto;\n      grid-template-columns: 1fr 1fr;\n      gap: 1rem;\n      position: relative;\n    }\n\n      .Pricing-featured-3vf8L .Pricing-showMedia-6U76q a {\n        width: 140px;\n        height: 72px;\n      }\n\n  .Pricing-on_news-2bgDh {\n    margin-bottom: 1.5rem;\n    grid-template-columns: repeat(1, 1fr);\n  }\n\n    .Pricing-on_news-2bgDh .Pricing-featuredImage-Joc-9 {\n      width: 100%;\n      max-width: 340px;\n    }\n\n  .Pricing-pricingHeader-2MLoH {\n    padding: 24px 0 0;\n  }\n\n  .Pricing-scrollTop-Aag07 {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .Pricing-price_title-3Yqn8 {\n    font-size: 32px;\n  }\n\n  .Pricing-price_content-5qlsh {\n    width: 100%;\n    margin: 16px 0 0;\n    max-width: 90%;\n  }\n\n  .Pricing-toggle_btn-3t9M8 {\n    margin-left: -20px;\n    margin-right: 20px;\n  }\n\n  .Pricing-toggle_outer-TDyU0 {\n    width: 40px;\n    height: 22px;\n  }\n\n  .Pricing-toggle_inner-dxNua {\n    width: 17.8px;\n    height: 17.6px;\n  }\n\n  .Pricing-plan-cbyB7 {\n    width: 270px;\n    display: none;\n    margin: 0;\n  }\n\n  .Pricing-active-1fA3n {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .Pricing-dot-2Wd1n {\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background-color: #e5e5e5;\n    margin: 0 5px;\n  }\n\n  .Pricing-dotactive-31__- {\n    background-color: #f36;\n  }\n\n  .Pricing-plan_title_students-ixHtF {\n    font-size: 16px;\n    line-height: 32px;\n  }\n\n  .Pricing-plan_title-2z_k0 p {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  /* .question {\n    font-size: 14px;\n    font-weight: 600;\n  } */\n    .Pricing-price-rctgP p {\n      font-size: 32px;\n      letter-spacing: -0.96px;\n    }\n\n  .Pricing-price_desc-4xVbA {\n    width: 101%;\n    font-size: 14px;\n    line-height: 21px;\n    margin-top: 24px;\n    opacity: 0.8;\n  }\n\n  .Pricing-buyButton-3g0NI {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    width: 174px;\n    border-radius: 24px;\n    color: #fff;\n    padding: 10px 16px;\n    font-size: 16px;\n    font-weight: 600;\n    margin-top: 28px;\n  }\n\n  /* .faq_container {\n    padding: 24px;\n\n    .faq_title p {\n      font-size: 32px;\n      margin-top: 0;\n      margin-bottom: 24px;\n    }\n\n    .expand_all {\n      font-size: 14px;\n      margin: 0 auto 4px auto;\n    }\n\n    .accordian {\n      width: 100%;\n\n      .border {\n        padding: 16px 0;\n\n        .answer {\n          width: 100%;\n\n          p {\n            margin-top: 8px;\n            margin-bottom: 0;\n            line-height: 18px;\n            opacity: 0.7;\n            font-size: 12px;\n          }\n        }\n      }\n    }\n  } */\n\n  .Pricing-circle_bg-vbvtk {\n    display: none;\n    position: absolute;\n    width: 12%;\n    top: 25%;\n    left: 95%;\n  }\n\n  .Pricing-square_bg-1fWOk {\n    display: none;\n    position: absolute;\n    top: 120%;\n    left: -3%;\n    -webkit-transform: rotate(45deg);\n        -ms-transform: rotate(45deg);\n            transform: rotate(45deg);\n    width: 10%;\n  }\n\n  .Pricing-line_vector_bg-2Zkss {\n    display: none;\n  }\n\n  .Pricing-triangle_bg-1X7I4 {\n    display: none;\n    position: absolute;\n    left: -10%;\n    width: 20%;\n    top: 554px;\n  }\n}\n\n.Pricing-moduleLabel-28Van {\n  color: #25282b;\n}\n\n.Pricing-viewLink-1-bHw {\n  width: 195px;\n  height: 32px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 64.6px 0 112px 0;\n  padding: 0 !important;\n  font-size: 24px;\n  line-height: 1.33;\n  color: #f36;\n  text-align: center;\n  text-decoration: underline;\n}\n\n@media only screen and (max-width: 990px) {\n  .Pricing-viewLink-1-bHw {\n    font-size: 14px;\n    margin: 0 0 48px 0;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/Pricing.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,qCAAqC;CACtC;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,oDAAoD;UAC5C,4CAA4C;EACpD,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,aAAa;EACb,eAAe;EACf,kBAAkB;CACnB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,mBAAmB;EACnB,aAAa;CACd;;AAED;EACE,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,oBAAoB;EACpB,oBAAoB;EACpB,8BAA8B;CAC/B;;AAED;EACE,aAAa;EACb,YAAY;EACZ,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,+BAA+B;CAChC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE,0BAA0B;EAC1B,0BAA0B;EAC1B,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,aAAa;EACb,cAAc;EACd,cAAc;EACd,mBAAmB;EACnB,uBAAuB;EACvB,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;CACpB;;AAED;IACI,uBAAuB;IACvB,mBAAmB;IACnB,gBAAgB;IAChB,gBAAgB;IAChB,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,mBAAmB;IACnB,WAAW;GACZ;;AAEH;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,YAAY;EACZ,eAAe;EACf,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,oBAAoB;EACpB,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,uBAAuB;MACnB,oBAAoB;EACxB,aAAa;CACd;;AAED;EACE,mBAAmB;MACf,0BAA0B;CAC/B;;AAED;EACE,cAAc;EACd,eAAe;EACf,oBAAoB;EACpB,uBAAuB;CACxB;;AAED;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;CACd;;AAED;IACI,UAAU;GACX;;AAEH;EACE,iBAAiB;EACjB,eAAe;EACf,gBAAgB;EAChB,WAAW;EACX,sBAAsB;EACtB,mBAAmB;CACpB;;AAED;EACE,eAAe;EACf,gBAAgB;EAChB,iBAAiB;CAClB;;AAED;IACI,eAAe;GAChB;;AAEH;EACE,YAAY;EACZ,aAAa;EACb,iBAAiB;CAClB;;AAED;IACI,mBAAmB;GACpB;;AAEH;EACE,iBAAiB;EACjB,gBAAgB;EAChB,2BAA2B;EAC3B,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,YAAY;EACZ,aAAa;CACd;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,wEAAwE;EACxE,mEAAmE;EACnE,gEAAgE;EAChE,qCAAqC;EACrC,8BAA8B;EAC9B,eAAe;CAChB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,aAAa;EACb,aAAa;EACb,qDAAqD;UAC7C,6CAA6C;EACrD,mBAAmB;EACnB,oBAAoB;EACpB,qBAAqB;EACrB,cAAc;EACd,0BAA0B;MACtB,8BAA8B;EAClC,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,kBAAkB;EAClB,oBAAoB;EACpB,uBAAuB;EACvB,YAAY;EACZ,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;;;;;;;;;;;;;;;;;;;;;;;;;IAyBI;;AAEJ;;;;;;;;IAQI;;AAEJ;;;;;;;;;;;;;;;IAeI;;AAEJ;;IAEI;;AAEJ;;IAEI;;AAEJ;;;;;;;IAOI;;AAEJ;;EAEE,cAAc;CACf;;AAED;EACE,mBAAmB;EACnB,UAAU;EACV,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,QAAQ;EACR,iCAAiC;MAC7B,6BAA6B;UACzB,yBAAyB;CAClC;;AAED;EACE,kCAAkC;MAC9B,8BAA8B;UAC1B,0BAA0B;EAClC,aAAa;EACb,mBAAmB;EACnB,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,UAAU;CACX;;AAED;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;IACI,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,iBAAiB;IACjB,oBAAoB;GACrB;;AAEH;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,sCAAsC;IACtC,UAAU;IACV,UAAU;IACV,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;MACd,gBAAgB;MAChB,kBAAkB;MAClB,6BAA6B;MAC7B,kBAAkB;MAClB,cAAc;MACd,gBAAgB;MAChB,sBAAsB;MACtB,+DAA+D;cACvD,uDAAuD;MAC/D,uBAAuB;MACvB,qBAAqB;MACrB,cAAc;MACd,2BAA2B;UACvB,uBAAuB;MAC3B,uBAAuB;UACnB,oBAAoB;KACzB;;AAEL;QACQ,YAAY;QACZ,aAAa;QACb,mBAAmB;QACnB,qBAAqB;QACrB,cAAc;QACd,sBAAsB;YAClB,wBAAwB;QAC5B,uBAAuB;YACnB,oBAAoB;QACxB,oBAAoB;OACrB;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,0BAA0B;OAC3B;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,kBAAkB;QAClB,mBAAmB;OACpB;;AAEP;QACQ,eAAe;OAChB;;AAEP;QACQ,YAAY;OACb;;AAEP;QACQ,eAAe;OAChB;;AAEP;QACQ,YAAY;OACb;;AAEP;QACQ,gBAAgB;QAChB,kBAAkB;QAClB,mBAAmB;OACpB;;AAEP;EACE,0BAA0B;EAC1B,wBAAwB;EACxB,4BAA4B;CAC7B;;AAED;EACE,mBAAmB;EACnB,oBAAoB;EACpB,sBAAsB;EACtB,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,aAAa;CACd;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,uBAAuB;MACnB,+BAA+B;EACnC,oBAAoB;CACrB;;AAED;IACI,gBAAgB;IAChB,UAAU;IACV,WAAW;IACX,oBAAoB;IACpB,2BAA2B;OACxB,wBAAwB;IAC3B,iBAAiB;GAClB;;AAEH;IACI,gBAAgB;GACjB;;AAEH;EACE,uBAAuB;EACvB,YAAY;EACZ,aAAa;CACd;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,yBAAyB;CAC1B;;AAED;IACI,aAAa;IACb,cAAc;IACd,gBAAgB;GACjB;;AAEH;IACI,eAAe;GAChB;;AAEH;IACI,gBAAgB;GACjB;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;EACpB,sBAAsB;EACtB,uBAAuB;CACxB;;AAED;IACI,gBAAgB;IAChB,kBAAkB;IAClB,iBAAiB;IACjB,eAAe;IACf,oBAAoB;IACpB,oBAAoB;GACrB;;AAEH;IACI,cAAc;IACd,sCAAsC;IACtC,UAAU;IACV,YAAY;GACb;;AAEH;MACM,aAAa;MACb,aAAa;MACb,cAAc;MACd,cAAc;KACf;;AAEL;MACM,gBAAgB;KACjB;;AAEL;IACI,cAAc;GACf;;AAEH;EACE,gBAAgB;EAChB,OAAO;EACP,QAAQ;EACR,YAAY;EACZ,cAAc;EACd,qCAAqC;EACrC,WAAW;CACZ;;AAED;EACE,cAAc;EACd,YAAY;EACZ,iBAAiB;EACjB,cAAc;EACd,uBAAuB;EACvB,mBAAmB;EACnB,WAAW;CACZ;;AAED;EACE,YAAY;EACZ,cAAc;CACf;;AAED;EACE;IACE,YAAY;GACb;;EAED;IACE,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,qBAAqB;IACrB,cAAc;IACd,0BAA0B;GAC3B;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,0BAA0B;IAC1B,mBAAmB;IACnB,gCAAgC;GACjC;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,0BAA0B;IAC1B,mBAAmB;IACnB,+BAA+B;GAChC;;EAED;IACE,mBAAmB;IACnB,0BAA0B;IAC1B,UAAU;GACX;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,0BAA0B;IAC1B,qBAAqB;GACtB;;EAED;IACE,6BAA6B;IAC7B,eAAe;IACf,qDAAqD;YAC7C,6CAA6C;IACrD,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,2BAA2B;QACvB,uBAAuB;IAC3B,mBAAmB;IACnB,iBAAiB;IACjB,uBAAuB;GACxB;;EAED;IACE,cAAc;IACd,aAAa;IACb,cAAc;GACf;;EAED;IACE,oBAAoB;GACrB;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,sCAAsC;IACtC,sBAAsB;IACtB,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,iBAAiB;IACjB,wEAAwE;IACxE,mEAAmE;IACnE,gEAAgE;IAChE,qCAAqC;IACrC,8BAA8B;IAC9B,eAAe;IACf,sBAAsB;GACvB;;EAED;IACE,qBAAqB;IACrB,kBAAkB;IAClB,uBAAuB;GACxB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;IACb,yCAAyC;GAC1C;;EAED;IACE,eAAe;GAChB;;EAED;IACE,eAAe;IACf,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,mBAAmB;IACnB,YAAY;IACZ,SAAS;IACT,mBAAmB;GACpB;;IAEC;MACE,kBAAkB;MAClB,4BAA4B;MAC5B,mBAAmB;MACnB,UAAU;MACV,UAAU;MACV,gBAAgB;KACjB;;EAEH;IACE,eAAe;IACf,YAAY;IACZ,aAAa;IACb,4BAA4B;IAC5B,mBAAmB;IACnB,aAAa;IACb,SAAS;IACT,mBAAmB;GACpB;;IAEC;MACE,kBAAkB;MAClB,4BAA4B;MAC5B,mBAAmB;MACnB,UAAU;MACV,UAAU;MACV,gBAAgB;KACjB;;EAEH;IACE,iBAAiB;GAClB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;IACxB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,qBAAqB;IACrB,cAAc;GACf;;EAED;IACE,qBAAqB;IACrB,mDAAmD;IACnD,0BAA0B;GAC3B;;IAEC;MACE,kBAAkB;MAClB,WAAW;MACX,sBAAsB;KACvB;;MAEC;QACE,cAAc;QACd,iBAAiB;OAClB;;IAEH;MACE,2BAA2B;MAC3B,aAAa;MACb,mBAAmB;MACnB,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,aAAa;KACd;;EAEH;IACE,gBAAgB;GACjB;;IAEC;MACE,kBAAkB;MAClB,kBAAkB;MAClB,oBAAoB;KACrB;;IAED;MACE,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,+BAA+B;MAC/B,iBAAiB;MACjB,aAAa;MACb,mBAAmB;KACpB;;MAEC;QACE,YAAY;QACZ,aAAa;QACb,iBAAiB;QACjB,kBAAkB;OACnB;IACH;MACE,gBAAgB;MAChB,kBAAkB;KACnB;;EAEH;IACE,6BAA6B;GAC9B;;IAEC;MACE,kBAAkB;MAClB,oBAAoB;MACpB,qBAAqB;KACtB;;IAED;MACE,2BAA2B;MAC3B,wBAAwB;MACxB,mBAAmB;MACnB,sBAAsB;MACtB,+BAA+B;MAC/B,UAAU;MACV,mBAAmB;KACpB;;MAEC;QACE,aAAa;QACb,aAAa;OACd;;EAEL;IACE,sBAAsB;IACtB,sCAAsC;GACvC;;IAEC;MACE,YAAY;MACZ,iBAAiB;KAClB;;EAEH;IACE,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,eAAe;GAChB;;EAED;IACE,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;IACd,eAAe;GAChB;;EAED;IACE,aAAa;IACb,cAAc;IACd,UAAU;GACX;;EAED;IACE,qBAAqB;IACrB,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;IACb,mBAAmB;IACnB,0BAA0B;IAC1B,cAAc;GACf;;EAED;IACE,uBAAuB;GACxB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;;;MAGI;IACF;MACE,gBAAgB;MAChB,wBAAwB;KACzB;;EAEH;IACE,YAAY;IACZ,gBAAgB;IAChB,kBAAkB;IAClB,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,aAAa;IACb,oBAAoB;IACpB,YAAY;IACZ,mBAAmB;IACnB,gBAAgB;IAChB,iBAAiB;IACjB,iBAAiB;GAClB;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAiCI;;EAEJ;IACE,cAAc;IACd,mBAAmB;IACnB,WAAW;IACX,SAAS;IACT,UAAU;GACX;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,UAAU;IACV,UAAU;IACV,iCAAiC;QAC7B,6BAA6B;YACzB,yBAAyB;IACjC,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,WAAW;IACX,WAAW;IACX,WAAW;GACZ;CACF;;AAED;EACE,eAAe;CAChB;;AAED;EACE,aAAa;EACb,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,yBAAyB;EACzB,sBAAsB;EACtB,gBAAgB;EAChB,kBAAkB;EAClB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;CAC5B;;AAED;EACE;IACE,gBAAgB;IAChB,mBAAmB;GACpB;CACF","file":"Pricing.scss","sourcesContent":[".pricingHeader {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.darkBackground {\n  background-color: #25282b !important;\n}\n\n.price_title {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.price_content {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.modulesContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 72px;\n}\n\n.modulesHeader {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.modulesList {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(5, 1fr);\n  gap: 44px;\n  margin-top: 24px;\n}\n\n.module {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.imageWrapper {\n  width: 52px;\n  height: 52px;\n  border-radius: 26px;\n  -webkit-box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.12);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-bottom: 12px;\n}\n\n.imageWrapper img {\n  width: 32px;\n  height: 32px;\n}\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.price_content p {\n  font-size: 16px;\n  opacity: 0.7;\n  line-height: 2;\n  line-height: 32px;\n}\n\n.annual {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  margin-right: 50px;\n  height: auto;\n}\n\n.toggle_btn {\n  margin-right: 50px;\n}\n\n.annual_text {\n  font-size: 14px;\n  padding-bottom: 4px;\n  line-height: normal;\n  border-bottom: 2px solid #fff;\n}\n\n.annual_offer {\n  opacity: 0.8;\n  color: #f36;\n  margin-bottom: 4px;\n  display: inline-block;\n}\n\n.underline {\n  border-bottom: 2px solid black;\n}\n\n.pricing_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n.plans {\n  padding: 0 48px 32px 48px;\n  padding: 0 3rem 2rem 3rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.plan {\n  width: 270px;\n  height: 477px;\n  margin: 0 6px;\n  border-radius: 8px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  position: relative;\n}\n\n.plan .innerCard {\n    background-color: #fff;\n    position: absolute;\n    padding: 32px 0;\n    padding: 2rem 0;\n    width: 100%;\n    height: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    text-align: center;\n    z-index: 1;\n  }\n\n.symbol {\n  border-radius: 100%;\n}\n\n.pricing_title {\n  font-size: 16px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  width: 100%;\n  color: #25282b;\n  margin-bottom: 50px;\n}\n\n.toggle_outer {\n  width: 56px;\n  height: 31px;\n  border-radius: 24px;\n  background-color: #f36;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 3px;\n}\n\n.toggle_on {\n  -ms-flex-pack: end;\n      justify-content: flex-end;\n}\n\n.toggle_inner {\n  width: 24.9px;\n  height: 24.9px;\n  border-radius: 100%;\n  background-color: #fff;\n}\n\n.plan_title_students {\n  font-size: 14px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.plan_title p {\n    margin: 0;\n  }\n\n.plan_title_price {\n  font-weight: 600;\n  color: #25282b;\n  font-size: 24px;\n  opacity: 1;\n  display: inline-block;\n  margin-bottom: 8px;\n}\n\n.price {\n  color: #25282b;\n  font-size: 32px;\n  font-weight: 600;\n}\n\n.price p {\n    margin: 32px 0;\n  }\n\n.price_desc {\n  color: #000;\n  opacity: 0.6;\n  line-height: 1.5;\n}\n\n.price_desc p {\n    margin: 0 0 24px 0;\n  }\n\n.plan button {\n  margin-top: 40px;\n  padding: 0% 15%;\n  text-transform: capitalize;\n  font-weight: normal;\n  border-radius: 24px;\n}\n\n.price_contact a {\n  text-decoration: underline;\n  // margin-bottom: 500px;\n  color: #000;\n  opacity: 0.6;\n}\n\n.pricePerStudentHighlight {\n  font-size: 24px;\n  font-weight: 600;\n  background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n  background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n  -webkit-text-fill-color: transparent;\n  -webkit-background-clip: text;\n  color: #25282b;\n}\n\n.buyButton {\n  background-color: #f36;\n}\n\n.enterprise {\n  width: 91.5%;\n  margin: auto;\n  -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 8px;\n  padding: 42px 200px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 56px;\n}\n\n.enterpriseHeading {\n  font-size: 24px;\n  font-weight: 600;\n  color: #25282b;\n}\n\n.enterprisecontext {\n  font-size: 16px;\n  line-height: 24px;\n}\n\n.contactus {\n  padding: 8px 20px;\n  border-radius: 24px;\n  background-color: #f36;\n  color: #fff;\n  font-size: 14px;\n  line-height: 20px;\n}\n\n/* .faq_container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  color: #25282b;\n  background-color: #f7f7f7;\n}\n\n.faq_title p {\n  font-size: 40px;\n  font-weight: 600;\n}\n\n.expand_all {\n  margin: 0% 10%;\n  align-self: flex-end;\n  font-size: 16px;\n  color: #25282b;\n  cursor: pointer;\n  border-bottom: 1px solid black;\n  width: max-content;\n}\n\n.accordian {\n  width: 80%;\n} */\n\n/* .border {\n  cursor: pointer;\n  border-bottom: 1px solid #c7c7c7;\n  padding: 44px 0 40px 0;\n}\n\n.border:last-child {\n  border-bottom: none;\n} */\n\n/* .question {\n  width: 100%;\n  font-size: 16px;\n  color: #25282b;\n  font-weight: bold;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.answer {\n  display: none;\n  font-size: 14px;\n  line-height: 25.4px;\n  width: 85%;\n} */\n\n/* .show .answer {\n  display: block;\n} */\n\n/* .up {\n  display: none;\n} */\n\n/* .show .up {\n  display: block;\n  transform: rotate(180deg);\n}\n\n.show .down {\n  display: none;\n} */\n\n.leftArrow,\n.rightArrow {\n  display: none;\n}\n\n.circle_bg {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.square_bg {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n  -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n.line_vector_bg {\n  -webkit-transform: rotate(180deg);\n      -ms-transform: rotate(180deg);\n          transform: rotate(180deg);\n  opacity: 0.5;\n  position: absolute;\n  left: 10%;\n}\n\n.triangle_bg {\n  position: absolute;\n  right: 7%;\n}\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.featured .showMedia a {\n  width: 259px;\n  height: 127px;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n    text-align: center;\n    font-size: 40px;\n    line-height: 1.2;\n    color: #25282b;\n    font-weight: 600;\n    margin-bottom: 40px;\n  }\n\n.achievedContainer .achievedRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(4, 1fr);\n    // grid-template-rows: repeat(2, 1fr);\n    gap: 16px;\n    gap: 1rem;\n    margin: auto;\n  }\n\n.achievedContainer .achievedRow .card {\n      width: 270px;\n      height: 202px;\n      font-size: 24px;\n      font-size: 1.5rem;\n      color: rgba(37, 40, 43, 0.6);\n      line-height: 40px;\n      padding: 24px;\n      padding: 1.5rem;\n      border-radius: 0.5rem;\n      -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n              box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n      background-color: #fff;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-direction: column;\n          flex-direction: column;\n      -ms-flex-align: center;\n          align-items: center;\n    }\n\n.achievedContainer .achievedRow .card .achievedProfile {\n        width: 52px;\n        height: 52px;\n        border-radius: 50%;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-pack: center;\n            justify-content: center;\n        -ms-flex-align: center;\n            align-items: center;\n        margin-bottom: 20px;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n        background-color: #fff3eb;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n        background-color: #f7effe;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n        background-color: #ffebf0;\n      }\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n        background-color: #ebffef;\n      }\n\n.achievedContainer .achievedRow .card .highlight {\n        font-size: 36px;\n        font-weight: bold;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n        color: #00ac26;\n      }\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n        color: #f36;\n      }\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n        color: #8c00fe;\n      }\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n        color: #f60;\n      }\n\n.achievedContainer .achievedRow .card .subText {\n        font-size: 24px;\n        line-height: 40px;\n        text-align: center;\n      }\n\n.featured {\n  background-color: #f7f7f7;\n  padding: 56px 64px 40px;\n  padding: 3.5rem 4rem 2.5rem;\n}\n\n.featured h2 {\n  text-align: center;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n  margin-top: 0;\n  font-size: 40px;\n  font-size: 2.5rem;\n  line-height: 48px;\n  line-height: 3rem;\n}\n\n.featured .showMedia {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 40px;\n  gap: 2.5rem;\n  margin: auto;\n}\n\n.titlebox {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n  margin-bottom: 28px;\n}\n\n.titlebox h2 {\n    font-size: 20px;\n    margin: 0;\n    width: 90%;\n    white-space: nowrap;\n    -o-text-overflow: ellipsis;\n       text-overflow: ellipsis;\n    overflow: hidden;\n  }\n\n.titlebox img {\n    cursor: pointer;\n  }\n\n.featured .showMedia a img {\n  background-color: #fff;\n  width: 100%;\n  height: 100%;\n}\n\n.on_news {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-max-content;\n  height: -moz-max-content;\n  height: max-content;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  gap: 12px;\n  margin: 0 auto 56px auto;\n}\n\n.on_news .featuredImage {\n    width: 368px;\n    height: 250px;\n    cursor: pointer;\n  }\n\n.on_news .featuredImage:nth-child(1) {\n    margin-left: 0;\n  }\n\n.on_news .featuredImage:nth-child(3) {\n    margin-right: 0;\n  }\n\n.awardsContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 56px 192px;\n  padding: 3.5rem 12rem;\n  background-color: #fff;\n}\n\n.awardsContainer .awardsTitle {\n    font-size: 40px;\n    font-size: 2.5rem;\n    font-weight: 600;\n    color: #25282b;\n    margin-bottom: 48px;\n    margin-bottom: 3rem;\n  }\n\n.awardsContainer .achievedRow {\n    display: grid;\n    grid-template-columns: repeat(3, 1fr);\n    gap: 88px;\n    gap: 5.5rem;\n  }\n\n.awardsContainer .achievedRow .awardLogo {\n      width: 240px;\n      width: 15rem;\n      height: 208px;\n      height: 13rem;\n    }\n\n.awardsContainer .achievedRow .awardLogo:nth-child(3n + 2) {\n      margin-right: 0;\n    }\n\n.awardsContainer .dots {\n    display: none;\n  }\n\n.popupoverlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 3;\n}\n\n.presspopup {\n  padding: 24px;\n  width: 100%;\n  max-width: 585px;\n  height: 407px;\n  background-color: #fff;\n  border-radius: 5px;\n  z-index: 4;\n}\n\n.player {\n  width: 100%;\n  height: 302px;\n}\n\n@media only screen and (max-width: 990px) {\n  .mobilePricingContainer {\n    width: 100%;\n  }\n\n  .modulesContainer {\n    margin: 64px 0;\n  }\n\n  .tabsContainer {\n    width: 100%;\n    display: -ms-flexbox;\n    display: flex;\n    background-color: #f7f7f7;\n  }\n\n  .annualtab {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-right-radius: 8px;\n  }\n\n  .monthtab {\n    cursor: pointer;\n    width: 50%;\n    background-color: #e8e8e8;\n    padding: 24px 32px;\n    border-bottom-left-radius: 8px;\n  }\n\n  .activeTab {\n    // cursor: pointer;\n    background-color: #f7f7f7;\n    border: 0;\n  }\n\n  .mobilePlans {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    background-color: #f7f7f7;\n    padding-bottom: 12px;\n  }\n\n  .mobilePriceCard {\n    padding: 24px 52px 24px 53px;\n    margin: 12px 0;\n    -webkit-box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n            box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.12);\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    border-radius: 8px;\n    max-width: 328px;\n    background-color: #fff;\n  }\n\n  .presspopup {\n    padding: 12px;\n    width: 328px;\n    height: 217px;\n  }\n\n  .titlebox {\n    margin-bottom: 16px;\n  }\n\n  .titlebox h2 {\n    font-size: 11.5px;\n  }\n\n  .modulesList {\n    grid-template-columns: repeat(3, 1fr);\n    grid-column-gap: 40px;\n    grid-row-gap: 20px;\n  }\n\n  .imageWrapper {\n    width: 40px;\n    height: 40px;\n  }\n\n  .imageWrapper img {\n    width: 24px;\n    height: 24px;\n  }\n\n  .pricePerStudentHighlightMobile {\n    font-size: 20px;\n    font-weight: 600;\n    background-image: -webkit-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: -o-linear-gradient(31deg, #f36 29%, #ff601d 70%);\n    background-image: linear-gradient(59deg, #f36 29%, #ff601d 70%);\n    -webkit-text-fill-color: transparent;\n    -webkit-background-clip: text;\n    color: #25282b;\n    opacity: 1 !important;\n  }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n    background-color: #fff;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n    border: 1px solid rgba(37, 40, 43, 0.16);\n  }\n\n  .displayClients .clientsWrapper .client.active {\n    display: block;\n  }\n\n  .leftArrow {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    left: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .leftArrow span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .rightArrow {\n    display: block;\n    width: 25px;\n    height: 25px;\n    background-color: lightgray;\n    position: absolute;\n    right: -40px;\n    top: 50%;\n    border-radius: 50%;\n  }\n\n    .rightArrow span {\n      font-size: 1.5rem;\n      color: rgb(37, 40, 43, 0.5);\n      position: absolute;\n      left: 7px;\n      top: -2px;\n      cursor: pointer;\n    }\n\n  .displayClients .clientsWrapper span {\n    max-width: 400px;\n  }\n\n  .dots {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n    margin-top: 56px;\n    margin-bottom: 24px;\n  }\n\n  .displayClients .clientsWrapper .dots {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .achievedContainer {\n    padding: 1.5rem 1rem;\n    background-image: url('/images/home/Confetti.svg');\n    background-color: #f7f7f7;\n  }\n\n    .achievedContainer .achievedHeading {\n      font-size: 1.5rem;\n      padding: 0;\n      margin-bottom: 1.5rem;\n    }\n\n      .achievedContainer .achievedHeading span::after {\n        content: '\\a';\n        white-space: pre;\n      }\n\n    .achievedContainer .achievedRow {\n      grid-template-columns: 1fr;\n      gap: 0.75rem;\n      max-width: 20.5rem;\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: auto;\n    }\n\n  .awardsContainer {\n    padding: 1.5rem;\n  }\n\n    .awardsContainer .awardsTitle {\n      font-size: 1.5rem;\n      padding-bottom: 0;\n      margin-bottom: 2rem;\n    }\n\n    .awardsContainer .achievedRow {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      grid-template-columns: 1fr 1fr;\n      gap: 1.5rem 1rem;\n      margin: auto;\n      position: relative;\n    }\n\n      .awardsContainer .achievedRow .awardLogo {\n        width: 100%;\n        height: 100%;\n        max-width: 15rem;\n        max-height: 13rem;\n      }\n    .price_contact a {\n      font-size: 14px;\n      line-height: 21px;\n    }\n\n  .featured {\n    padding: 1.5rem 1rem 1.25rem;\n  }\n\n    .featured h2 {\n      font-size: 1.5rem;\n      line-height: normal;\n      margin: 0 0 1.5rem 0;\n    }\n\n    .featured .showMedia {\n      width: -webkit-fit-content;\n      width: -moz-fit-content;\n      width: fit-content;\n      margin: 0 auto 0 auto;\n      grid-template-columns: 1fr 1fr;\n      gap: 1rem;\n      position: relative;\n    }\n\n      .featured .showMedia a {\n        width: 140px;\n        height: 72px;\n      }\n\n  .on_news {\n    margin-bottom: 1.5rem;\n    grid-template-columns: repeat(1, 1fr);\n  }\n\n    .on_news .featuredImage {\n      width: 100%;\n      max-width: 340px;\n    }\n\n  .pricingHeader {\n    padding: 24px 0 0;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n\n  .price_title {\n    font-size: 32px;\n  }\n\n  .price_content {\n    width: 100%;\n    margin: 16px 0 0;\n    max-width: 90%;\n  }\n\n  .toggle_btn {\n    margin-left: -20px;\n    margin-right: 20px;\n  }\n\n  .toggle_outer {\n    width: 40px;\n    height: 22px;\n  }\n\n  .toggle_inner {\n    width: 17.8px;\n    height: 17.6px;\n  }\n\n  .plan {\n    width: 270px;\n    display: none;\n    margin: 0;\n  }\n\n  .active {\n    display: -ms-flexbox;\n    display: flex;\n  }\n\n  .dot {\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background-color: #e5e5e5;\n    margin: 0 5px;\n  }\n\n  .dotactive {\n    background-color: #f36;\n  }\n\n  .plan_title_students {\n    font-size: 16px;\n    line-height: 32px;\n  }\n\n  .plan_title p {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  /* .question {\n    font-size: 14px;\n    font-weight: 600;\n  } */\n    .price p {\n      font-size: 32px;\n      letter-spacing: -0.96px;\n    }\n\n  .price_desc {\n    width: 101%;\n    font-size: 14px;\n    line-height: 21px;\n    margin-top: 24px;\n    opacity: 0.8;\n  }\n\n  .buyButton {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    width: 174px;\n    border-radius: 24px;\n    color: #fff;\n    padding: 10px 16px;\n    font-size: 16px;\n    font-weight: 600;\n    margin-top: 28px;\n  }\n\n  /* .faq_container {\n    padding: 24px;\n\n    .faq_title p {\n      font-size: 32px;\n      margin-top: 0;\n      margin-bottom: 24px;\n    }\n\n    .expand_all {\n      font-size: 14px;\n      margin: 0 auto 4px auto;\n    }\n\n    .accordian {\n      width: 100%;\n\n      .border {\n        padding: 16px 0;\n\n        .answer {\n          width: 100%;\n\n          p {\n            margin-top: 8px;\n            margin-bottom: 0;\n            line-height: 18px;\n            opacity: 0.7;\n            font-size: 12px;\n          }\n        }\n      }\n    }\n  } */\n\n  .circle_bg {\n    display: none;\n    position: absolute;\n    width: 12%;\n    top: 25%;\n    left: 95%;\n  }\n\n  .square_bg {\n    display: none;\n    position: absolute;\n    top: 120%;\n    left: -3%;\n    -webkit-transform: rotate(45deg);\n        -ms-transform: rotate(45deg);\n            transform: rotate(45deg);\n    width: 10%;\n  }\n\n  .line_vector_bg {\n    display: none;\n  }\n\n  .triangle_bg {\n    display: none;\n    position: absolute;\n    left: -10%;\n    width: 20%;\n    top: 554px;\n  }\n}\n\n.moduleLabel {\n  color: #25282b;\n}\n\n.viewLink {\n  width: 195px;\n  height: 32px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin: 64.6px 0 112px 0;\n  padding: 0 !important;\n  font-size: 24px;\n  line-height: 1.33;\n  color: #f36;\n  text-align: center;\n  text-decoration: underline;\n}\n\n@media only screen and (max-width: 990px) {\n  .viewLink {\n    font-size: 14px;\n    margin: 0 0 48px 0;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"pricingHeader": "Pricing-pricingHeader-2MLoH",
	"darkBackground": "Pricing-darkBackground-17ag8",
	"price_title": "Pricing-price_title-3Yqn8",
	"price_content": "Pricing-price_content-5qlsh",
	"modulesContainer": "Pricing-modulesContainer-9h2Yz",
	"modulesHeader": "Pricing-modulesHeader-zfiUY",
	"modulesList": "Pricing-modulesList-3_DuR",
	"module": "Pricing-module-dy7g3",
	"imageWrapper": "Pricing-imageWrapper-2cu95",
	"scrollTop": "Pricing-scrollTop-Aag07",
	"annual": "Pricing-annual-30ie_",
	"toggle_btn": "Pricing-toggle_btn-3t9M8",
	"annual_text": "Pricing-annual_text-c6h-G",
	"annual_offer": "Pricing-annual_offer-2VBwG",
	"underline": "Pricing-underline-VNfR5",
	"pricing_container": "Pricing-pricing_container-2DZ9K",
	"plans": "Pricing-plans-3oyRG",
	"plan": "Pricing-plan-cbyB7",
	"innerCard": "Pricing-innerCard-XD-XR",
	"symbol": "Pricing-symbol-1PZl7",
	"pricing_title": "Pricing-pricing_title-18Vxm",
	"toggle_outer": "Pricing-toggle_outer-TDyU0",
	"toggle_on": "Pricing-toggle_on-KyTo6",
	"toggle_inner": "Pricing-toggle_inner-dxNua",
	"plan_title_students": "Pricing-plan_title_students-ixHtF",
	"plan_title": "Pricing-plan_title-2z_k0",
	"plan_title_price": "Pricing-plan_title_price-2Zx49",
	"price": "Pricing-price-rctgP",
	"price_desc": "Pricing-price_desc-4xVbA",
	"price_contact": "Pricing-price_contact-7uNig",
	"pricePerStudentHighlight": "Pricing-pricePerStudentHighlight-qAWcu",
	"buyButton": "Pricing-buyButton-3g0NI",
	"enterprise": "Pricing-enterprise-1O-mF",
	"enterpriseHeading": "Pricing-enterpriseHeading-2Xm68",
	"enterprisecontext": "Pricing-enterprisecontext-3AD9J",
	"contactus": "Pricing-contactus-XfL9G",
	"leftArrow": "Pricing-leftArrow-3BcpG",
	"rightArrow": "Pricing-rightArrow-1CGiq",
	"circle_bg": "Pricing-circle_bg-vbvtk",
	"square_bg": "Pricing-square_bg-1fWOk",
	"line_vector_bg": "Pricing-line_vector_bg-2Zkss",
	"triangle_bg": "Pricing-triangle_bg-1X7I4",
	"displayClients": "Pricing-displayClients-SxgNR",
	"featured": "Pricing-featured-3vf8L",
	"showMedia": "Pricing-showMedia-6U76q",
	"clientsWrapper": "Pricing-clientsWrapper-65G_i",
	"client": "Pricing-client-14kGt",
	"achievedContainer": "Pricing-achievedContainer-3NBMf",
	"achievedHeading": "Pricing-achievedHeading-hxnwu",
	"achievedRow": "Pricing-achievedRow-29TsL",
	"card": "Pricing-card-3-jMd",
	"achievedProfile": "Pricing-achievedProfile-1jgdI",
	"clients": "Pricing-clients-2mnCX",
	"students": "Pricing-students-1YHpU",
	"tests": "Pricing-tests-3nDUz",
	"questions": "Pricing-questions-3qJ8D",
	"highlight": "Pricing-highlight-3-Tsm",
	"questionsHighlight": "Pricing-questionsHighlight-1HJlR",
	"testsHighlight": "Pricing-testsHighlight-vkapq",
	"studentsHighlight": "Pricing-studentsHighlight-2UuO9",
	"clientsHighlight": "Pricing-clientsHighlight-2hUQE",
	"subText": "Pricing-subText-3PFWz",
	"titlebox": "Pricing-titlebox-1mBcy",
	"on_news": "Pricing-on_news-2bgDh",
	"featuredImage": "Pricing-featuredImage-Joc-9",
	"awardsContainer": "Pricing-awardsContainer-2PyIg",
	"awardsTitle": "Pricing-awardsTitle-2F_p8",
	"awardLogo": "Pricing-awardLogo-2pqvP",
	"dots": "Pricing-dots-2Jt23",
	"popupoverlay": "Pricing-popupoverlay-32yez",
	"presspopup": "Pricing-presspopup-1UCPJ",
	"player": "Pricing-player-1FCsc",
	"mobilePricingContainer": "Pricing-mobilePricingContainer-3w6Qk",
	"tabsContainer": "Pricing-tabsContainer-1-5am",
	"annualtab": "Pricing-annualtab-dyIQZ",
	"monthtab": "Pricing-monthtab-3Jy_s",
	"activeTab": "Pricing-activeTab-3vi1R",
	"mobilePlans": "Pricing-mobilePlans-vbHxh",
	"mobilePriceCard": "Pricing-mobilePriceCard-32uBZ",
	"pricePerStudentHighlightMobile": "Pricing-pricePerStudentHighlightMobile-36iXF",
	"active": "Pricing-active-1fA3n",
	"dot": "Pricing-dot-2Wd1n",
	"dotactive": "Pricing-dotactive-31__-",
	"moduleLabel": "Pricing-moduleLabel-28Van",
	"viewLink": "Pricing-viewLink-1-bHw"
};

/***/ }),

/***/ "./src/components/Modal/Modal.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Modal/Modal.scss");
/* harmony import */ var _Modal_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Modal_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Modal/Modal.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var Modal =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Modal, _React$Component);

  function Modal() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Modal);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Modal)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleKeyDownEvent", function (e) {
      if (e.which === 27) {
        _this.props.toggleModal();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleOutsideClick", function (e) {
      // ignore clicks on the component itself
      if (_this.modalContainer && _this.modalContainer.contains(e.target)) {
        return;
      }

      if (_this.modalContainer) {
        document.removeEventListener('click', _this.handleOutsideClick, false);
        document.removeEventListener('touchstart', _this.handleOutsideClick, false);

        _this.props.toggleModal();
      }
    });

    return _this;
  }

  _createClass(Modal, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.modalContainer) {
        this.modalContainer.focus();
      }

      document.addEventListener('click', this.handleOutsideClick, false);
      document.addEventListener('touchstart', this.handleOutsideClick, false);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "body-overlay ".concat(_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bodyOverlay, " ").concat(this.props.overlayClassName),
        role: "presentation",
        onKeyDown: this.handleKeyDownEvent,
        ref: function ref(_ref2) {
          _this2.modalOverlay = _ref2;
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a.modal, " ").concat(this.props.modalClassName),
        ref: function ref(_ref) {
          _this2.modalContainer = _ref;
        },
        tabIndex: "-1",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }, this.props.children));
    }
  }]);

  return Modal;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(Modal, "propTypes", {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired,
  modalClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  overlayClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  // eslint-disable-line
  toggleModal: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
});

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Modal_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(Modal));

/***/ }),

/***/ "./src/components/Modal/Modal.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Modal/Modal.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/Pricing.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/react-player/lib/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Modal/Modal.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/get-ranks/pricing/constants.js");
/* harmony import */ var _Pricing_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./src/routes/products/get-ranks/pricing/Pricing.scss");
/* harmony import */ var _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/Pricing.js";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










function Pricing() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      activeIndex = _useState2[0],
      setActiveIndex = _useState2[1]; // const [expandAll, setExpandAll] = useState(false);


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggle = _useState4[0],
      setToggle = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      showScroll = _useState6[0],
      setShowScroll = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      showModal = _useState8[0],
      setShowModal = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState10 = _slicedToArray(_useState9, 2),
      activePress = _useState10[0],
      setActivePress = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      isMobile = _useState12[0],
      setIsMobile = _useState12[1];

  var handleScroll = function handleScroll() {
    if (window.scrollY > 500) {
      setShowScroll(true);
    } else {
      setShowScroll(false);
    }
  };

  var handleResize = function handleResize() {
    if (window.innerWidth >= 990) {
      setIsMobile(false);
    } else {
      setIsMobile(true);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    handleScroll();
    handleResize();
    window.addEventListener('scroll', handleScroll);
    window.addEventListener('resize', handleResize);
    return function () {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleResize);
    };
  });

  var handleScrollTop = function handleScrollTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
    setShowScroll(false);
  };

  var displayScrollToTop = function displayScrollToTop() {
    return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.scrollTop,
      role: "presentation",
      onClick: function onClick() {
        handleScrollTop();
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/scrollTop.svg",
      alt: "scrollTop",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }));
  };

  var toggleHandler = function toggleHandler() {
    setToggle(!toggle);
  };

  var displayModules = function displayModules() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesHeader,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Every plan includes"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.modulesList,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, _constants__WEBPACK_IMPORTED_MODULE_6__["ModulesList"].map(function (module) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
        to: "/modules/".concat(module.value),
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.module,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.imageWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: module.path,
        alt: module.label,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.moduleLabel,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, module.label));
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.line_vector_bg,
      src: "images/icons/vector_line.png",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.triangle_bg,
      src: "images/icons/triangle-background.svg",
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }));
  };

  var gotoPrevSlide = function gotoPrevSlide() {
    if (activeIndex === 0) {
      setActiveIndex(3);
    } else setActiveIndex(activeIndex - 1);
  };

  var gotoNextSlide = function gotoNextSlide() {
    if (activeIndex === 3) setActiveIndex(0);else setActiveIndex(activeIndex + 1);
  };

  var displayMobilePricings = function displayMobilePricings() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePricingContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.tabsContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      role: "presentation",
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annualtab, " ").concat(!toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.activeTab : null),
      onClick: function onClick() {
        setToggle(false);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, "Annual Plans"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      role: "presentation",
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.monthtab, " ").concat(toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.activeTab : null),
      onClick: function onClick() {
        setToggle(true);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }, "Monthly Plans")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePlans,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138
      },
      __self: this
    }, (toggle ? _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"] : _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerAnnumDetails"]).map(function (detail) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePriceCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, detail.pack), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_students,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, "upto ", detail.noOfStudents, " students"), toggle ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      }, "switch to annual pack to save 15%") : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, "Per student\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricePerStudentHighlightMobile,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }, "\u20B945/-"), "\xA0per month"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 160
        },
        __self: this
      }, "\u20B9", detail.pricePerMonth));
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        width: 'fit-content'
      },
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.mobilePriceCard,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165
      },
      __self: this
    }, "Enterprise"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166
      },
      __self: this
    }, "For institutes with more than 1000 students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
      to: "/request-demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 170
      },
      __self: this
    }, "Contact us")))));
  };

  var displayPricings = function displayPricings() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricing_container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 178
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricing_title,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 190
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_text, " ").concat(toggle ? '' : _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.underline),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 191
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_offer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 192
      },
      __self: this
    }, "(save 15%)"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 193
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 194
      },
      __self: this
    }, "Annual Plans"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_btn,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 197
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      role: "presentation",
      onClick: function onClick() {
        return toggleHandler();
      },
      className: toggle ? "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_outer, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_on, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.darkBackground) : _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_outer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 198
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.toggle_inner,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 207
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.annual_text, " ").concat(toggle ? _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.underline : ''),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 210
      },
      __self: this
    }, "Monthly Plans")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plans,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214
      },
      __self: this
    }, (toggle ? _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"] : _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerAnnumDetails"]).map(function (plan, index) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        key: plan.pack,
        className: index === activeIndex ? "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.active) : "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 217
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.innerCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.leftArrow,
        onClick: gotoPrevSlide,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 229
        },
        __self: this
      }, "\u2039")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.rightArrow,
        onClick: gotoNextSlide,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, "\u203A")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.symbol,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "images/icons/".concat(plan.image),
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 241
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_price,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }, plan.pack), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 244
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.plan_title_students,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 245
        },
        __self: this
      }, "Up to ", plan.noOfStudents, " students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 251
        },
        __self: this
      }, "\u20B9", plan.pricePerMonth)), !toggle ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255
        },
        __self: this
      }, "Per Student"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricePerStudentHighlight,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 256
        },
        __self: this
      }, "\u20B945/-"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 259
        },
        __self: this
      }, "per month")) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_desc,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 263
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 264
        },
        __self: this
      }, "switch to annual pack to save 15%")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_contact),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "/contact-us",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 267
        },
        __self: this
      }, "Contact Us for a Quote"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.buyButton,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      }, "Buy"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dots,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 273
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_6__["pricingPerMonthDetails"].map(function (dot, clientindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          key: dot.pack,
          className: clientindex === activeIndex ? "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dot, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dotactive) : "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.dot),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 275
          },
          __self: this
        });
      }))));
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterprise,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 290
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterpriseHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 291
      },
      __self: this
    }, "Enterprise"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.enterprisecontext,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 292
      },
      __self: this
    }, "For institutes with more than 1000 students"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
      to: "/request-demo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 295
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.contactus,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 296
      },
      __self: this
    }, "Contact Us"))));
  };

  var displayPricingHeader = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.pricingHeader,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 303
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.circle_bg,
    src: "images/icons/circle-background.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 304
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.square_bg,
    src: "images/icons/square-background.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 309
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 314
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 315
    },
    __self: this
  }, "Choose your edition."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 316
    },
    __self: this
  }, "Try it free for 7 days.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.price_content,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 318
    },
    __self: this
  }, "Egnify plans start as low as Rs.45/- per student per month."), displayModules(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
    to: "/pricing/viewDetails",
    className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.viewLink,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 322
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 323
    },
    __self: this
  }, "View Plan Details")), isMobile ? displayMobilePricings() : displayPricings()); // Pricing
  // const displayPricing = (
  //   <div className={s.pricing_container}>
  //     <div className={s.pricing_title}>
  //       <img
  //         className={s.line_vector_bg}
  //         src="images/icons/vector_line.png"
  //         alt=""
  //       />
  //       <img
  //         className={s.triangle_bg}
  //         src="images/icons/triangle-background.svg"
  //         alt=""
  //       />
  //       <div className={s.annual}>
  //         <div className={`${s.annual_text} ${toggle ? '' : s.underline}`}>
  //           <span className={s.annual_offer}>(save 15%)</span>
  //           <br />
  //           <span>Annual Plans</span>
  //         </div>
  //       </div>
  //       <div className={s.toggle_btn}>
  //         <div
  //           role="presentation"
  //           onClick={() => toggleHandler()}
  //           className={
  //             toggle ? `${s.toggle_outer} ${s.toggle_on}` : s.toggle_outer
  //           }
  //         >
  //           <div className={s.toggle_inner} />
  //         </div>
  //       </div>
  //       <div className={`${s.annual_text} ${toggle ? s.underline : ''}`}>
  //         Monthly Plans
  //       </div>
  //     </div>
  //     <div className={s.plans}>
  //       {(toggle ? pricingPerMonthDetails : pricingPerAnnumDetails).map(
  //         (plan, index) => (
  //           <div
  //             key={plan.pack}
  //             className={
  //               index === activeIndex ? `${s.plan} ${s.active}` : `${s.plan}`
  //             }
  //           >
  //             <div className={s.innerCard}>
  //               <div
  //                 className={s.leftArrow}
  //                 onClick={gotoPrevSlide}
  //                 role="presentation"
  //               >
  //                 <span>&#8249;</span>
  //               </div>
  //               <div
  //                 className={s.rightArrow}
  //                 onClick={gotoNextSlide}
  //                 role="presentation"
  //               >
  //                 <span>&#8250;</span>
  //               </div>
  //               <div className={s.symbol}>
  //                 <img src={`images/icons/${plan.image}`} alt="" />
  //               </div>
  //               <div className={s.plan_title}>
  //                 <p>
  //                   <span className={s.plan_title_price}>{plan.pack}</span>
  //                   <br />
  //                   <span className={s.plan_title_students}>
  //                     Up to {plan.noOfStudents} students
  //                   </span>
  //                 </p>
  //               </div>
  //               <div className={s.price}>
  //                 <p>{plan.pricePerMonth}</p>
  //               </div>
  //               <div className={s.price_desc}>
  //                 <p>
  //                   Per {toggle ? 'month' : 'year'}, billed
  //                   <br /> {toggle ? 'month' : 'year'} in Rupees
  //                 </p>
  //               </div>
  //               <div className={`${s.price_contact}`}>
  //                 <a href="/contact-us">Contact Us for a Quote</a>
  //               </div>
  //               <button>Buy</button>
  //
  //               <div className={s.dots}>
  //                 {pricingPerMonthDetails.map((dot, clientindex) => (
  //                   <div
  //                     key={dot.pack}
  //                     className={
  //                       clientindex === activeIndex
  //                         ? `${s.dot} ${s.dotactive}`
  //                         : `${s.dot}`
  //                     }
  //                   />
  //                 ))}
  //               </div>
  //             </div>
  //           </div>
  //         ),
  //       )}
  //     </div>
  //   </div>
  // );
  // const onClickHandler = e => {
  //   const parent = e.currentTarget.parentElement;
  //
  //   if (parent.classList.contains(`${s.show}`)) {
  //     parent.classList.remove(`${s.show}`);
  //   } else {
  //     parent.classList.add(`${s.show}`);
  //   }
  // };
  // const expandAppHandler = () => {
  //   setExpandAll(!expandAll);
  // };
  // const pricingFAQ = (
  //   <div className={s.faq_container}>
  //     <div className={s.faq_title}>
  //       <p>Pricing FAQs</p>
  //     </div>
  //     <div
  //       className={s.expand_all}
  //       role="presentation"
  //       onClick={() => {
  //         expandAppHandler();
  //       }}
  //     >
  //       {expandAll ? 'Minimize All' : 'Expand All'}
  //     </div>
  //     <div className={s.accordian}>
  //       {FAQ.map(item => (
  //         <div
  //           key={item.label}
  //           className={`${s.border} ${expandAll ? s.show : ''}`}
  //         >
  //           <div
  //             role="presentation"
  //             onClick={e => {
  //               onClickHandler(e);
  //             }}
  //             className={s.question}
  //           >
  //             <span>{item.label}</span>
  //             <img
  //               className={s.down}
  //               src="images/icons/chevron-down.svg"
  //               alt=""
  //             />
  //             <img
  //               className={s.up}
  //               src="images/icons/chevron-down.svg"
  //               alt=""
  //             />
  //           </div>
  //           <div className={s.answer}>
  //             <p>{item.content}</p>
  //           </div>
  //         </div>
  //       ))}
  //     </div>
  //   </div>
  // );
  // const displayViewLink = () => (
  //  <span className={s.view}>
  //  <Link to="/pricing/viewDetails" className={s.viewLink}>View Plan Details</Link>
  // </span>
  // )

  var displayTrustedBy = function displayTrustedBy() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.displayClients,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 504
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 505
      },
      __self: this
    }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 506
      },
      __self: this
    }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clientsWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 509
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_CLIENTS_UPDATED"].map(function (client) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.client,
        key: client.id,
        src: client.icon,
        alt: "clinet",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 511
        },
        __self: this
      });
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 519
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_3__["default"], {
      to: "/customers",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 520
      },
      __self: this
    }, "click here for more...")));
  };

  var displayAchieved = function displayAchieved() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 526
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedHeading,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 527
      },
      __self: this
    }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 528
      },
      __self: this
    }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedRow,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 531
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 532
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clients),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 536
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/clients.svg",
      alt: "profile",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 537
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.clientsHighlight),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 539
      },
      __self: this
    }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 540
      },
      __self: this
    }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 542
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.students),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 546
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/students.svg",
      alt: "students",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 547
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.studentsHighlight),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 549
      },
      __self: this
    }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 552
      },
      __self: this
    }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card),
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 554
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.tests),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 558
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/tests.svg",
      alt: "tests",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 559
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.testsHighlight),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 561
      },
      __self: this
    }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 564
      },
      __self: this
    }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.card,
      style: {
        boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 566
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedProfile, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.questions),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 570
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/home/what-achieved/questions.svg",
      alt: "questions",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 571
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.highlight, " ").concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.questionsHighlight),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 576
      },
      __self: this
    }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.subText,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 579
      },
      __self: this
    }, "Questions Solved"))));
  };

  var openModal = function openModal(index) {
    setShowModal(true);
    setActivePress(_GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"][index]);
  };

  var toggleModal = function toggleModal() {
    setShowModal(false);
  };

  var displayFeaturedIn = function displayFeaturedIn() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.featured,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 595
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 596
      },
      __self: this
    }, "We've got featured in"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.on_news,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 597
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PRESS_VIDEO_COVERAGE"].map(function (item, index) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        role: "presentation",
        onClick: function onClick() {
          return openModal(index);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 599
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: item.icon,
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.featuredImage,
        alt: item.title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 600
        },
        __self: this
      }));
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.showMedia,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 604
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_MEDIA"].map(function (media) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.active,
        href: media.url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 606
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: media.icon,
        alt: media.name,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 607
        },
        __self: this
      }));
    })));
  };

  var displayAwards = function displayAwards() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardsContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 615
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardsTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 616
      },
      __self: this
    }, "Awards"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "".concat(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.achievedRow),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 617
      },
      __self: this
    }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_AWARDS"].map(function (item) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: item.icon1,
        alt: item.content,
        className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.awardLogo,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 619
        },
        __self: this
      });
    })));
  };

  var displayModal = function displayModal() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Modal__WEBPACK_IMPORTED_MODULE_4__["default"], {
      modalClassName: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.presspopup,
      overlayClassName: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.popupoverlay,
      toggleModal: toggleModal,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 626
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.titlebox,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 631
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 632
      },
      __self: this
    }, activePress.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/images/icons/close.svg",
      onClick: toggleModal,
      alt: "close",
      role: "presentation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 633
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_2___default.a, {
      playing: false,
      url: activePress.url,
      className: _Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a.player,
      width: "100%",
      height: "80%",
      controls: true,
      loop: true,
      playsinline: true,
      pip: false,
      config: {
        youtube: {
          playerVars: {
            showinfo: 1
          }
        }
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 641
      },
      __self: this
    }));
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 661
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 662
    },
    __self: this
  }, displayPricingHeader), displayTrustedBy(), displayAchieved(), displayFeaturedIn(), displayAwards(), displayScrollToTop(), showModal ? displayModal() : null);
}

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Pricing_scss__WEBPACK_IMPORTED_MODULE_7___default.a)(Pricing));

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/Pricing.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/pricing/Pricing.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAQ", function() { return FAQ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pricingPerAnnumDetails", function() { return pricingPerAnnumDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pricingPerMonthDetails", function() { return pricingPerMonthDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModulesList", function() { return ModulesList; });
var FAQ = [{
  label: 'What is GetRanks Learning Platform?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What do I get with a Rise subscription?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How much does a subscription cost?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Do you offer monthly subscriptions?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Is there a free trial period?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Do you offer discounts?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How can I get an even bigger discount?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I buy a subscription outside the India?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I buy a subscription on behalf of someone else?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'How do I purchase a tax-exempt subscription?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I cancel my subscription at any time?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What is your refund policy?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What’s the difference between user roles?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'What user roles count towards my plan’s user limit?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}, {
  label: 'Can I upgrade my plan as needed?',
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus vel quam sit turpis fames nibh tortor cursus. Sed massa vulputate faucibus id eget pellentesque.'
}];
var pricingPerAnnumDetails = [{
  image: 'triangle.svg',
  pack: 'Starter',
  noOfStudents: 100,
  pricePerMonth: '50,000/-'
}, {
  image: 'rhombus.svg',
  pack: 'Growth',
  noOfStudents: 250,
  pricePerMonth: '2,00,000/-'
}, {
  image: 'pentagon.svg',
  pack: 'Midsize',
  noOfStudents: 500,
  pricePerMonth: '3,00,000/-'
}, {
  image: 'hexagon.svg',
  pack: 'Large',
  noOfStudents: 1000,
  pricePerMonth: '4,00,000/-'
}];
var pricingPerMonthDetails = [{
  image: 'triangle.svg',
  pack: 'Starter',
  noOfStudents: 100,
  pricePerMonth: '20,000/-'
}, {
  image: 'rhombus.svg',
  pack: 'Growth',
  noOfStudents: 250,
  pricePerMonth: '50,000/-'
}, {
  image: 'pentagon.svg',
  pack: 'Midsize',
  noOfStudents: 500,
  pricePerMonth: '1,00,000/-'
}, {
  image: 'hexagon.svg',
  pack: 'Large',
  noOfStudents: 1000,
  pricePerMonth: '2,00,000/-'
}];
var ModulesList = [{
  label: 'Live Classes',
  path: '/images/home/New SubMenu Items/Teach/old_Live.svg',
  value: 'liveclasses'
}, {
  label: 'Online Tests',
  path: '/images/home/New SubMenu Items/Test/module_test.svg',
  value: 'tests'
}, {
  label: 'Assignments',
  path: '/images/home/New SubMenu Items/Teach/old_Assignments.svg',
  value: 'assignments'
}, {
  label: 'Doubts',
  path: '/images/home/New SubMenu Items/Teach/old_Doubts.svg',
  value: 'doubts'
}, {
  label: 'Connect',
  path: '/images/home/New SubMenu Items/Connect/new_connect.svg',
  value: 'connect'
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/pricing/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Pricing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/pricing/Pricing.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/pricing/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: ['GetRanks by Egnify: Assessment & Analytics Platform'],
              chunks: ['Pricing'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Pricing__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJpY2luZy5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvcHJpY2luZy9QcmljaW5nLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2NvbXBvbmVudHMvTW9kYWwvTW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvTW9kYWwvTW9kYWwuc2Nzcz9kODBlIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaWNpbmcvUHJpY2luZy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nL1ByaWNpbmcuc2Nzcz81MjMzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaWNpbmcvY29uc3RhbnRzLmpzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaWNpbmcvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5Nb2RhbC1ib2R5T3ZlcmxheS0zZlNONCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbn1cXG5cXG4uTW9kYWwtbW9kYWwtOVFweFUge1xcbiAgb3ZlcmZsb3c6IGF1dG87XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XFxuICBvdXRsaW5lOiBub25lO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4vKlxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTM2MXB4KSB7XFxuICAubWFya3NNb2RhbCB7XFxuICAgIHdpZHRoOiA2MDBweDtcXG4gICAgbWFyZ2luOiAtMTMzcHggMCAwIC0zMDBweDtcXG4gIH1cXG59XFxuKi9cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL01vZGFsL01vZGFsLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QiwrQkFBK0I7RUFDL0IsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLHFDQUFxQztFQUNyQyxjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCOztBQUVEOzs7Ozs7O0VBT0VcIixcImZpbGVcIjpcIk1vZGFsLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLmJvZHlPdmVybGF5IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi5tb2RhbCB7XFxuICBvdmVyZmxvdzogYXV0bztcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi8qXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMzYxcHgpIHtcXG4gIC5tYXJrc01vZGFsIHtcXG4gICAgd2lkdGg6IDYwMHB4O1xcbiAgICBtYXJnaW46IC0xMzNweCAwIDAgLTMwMHB4O1xcbiAgfVxcbn1cXG4qL1xcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJib2R5T3ZlcmxheVwiOiBcIk1vZGFsLWJvZHlPdmVybGF5LTNmU040XCIsXG5cdFwibW9kYWxcIjogXCJNb2RhbC1tb2RhbC05UXB4VVwiXG59OyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuUHJpY2luZy1wcmljaW5nSGVhZGVyLTJNTG9IIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNSU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5QcmljaW5nLWRhcmtCYWNrZ3JvdW5kLTE3YWc4IHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG59XFxuXFxuLlByaWNpbmctcHJpY2VfdGl0bGUtM1lxbjgge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc2l6ZTogNTZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZy1wcmljZV9jb250ZW50LTVxbHNoIHtcXG4gIHdpZHRoOiA1MCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi5QcmljaW5nLW1vZHVsZXNDb250YWluZXItOWgyWXoge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDcycHg7XFxufVxcblxcbi5QcmljaW5nLW1vZHVsZXNIZWFkZXItemZpVVkge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZy1tb2R1bGVzTGlzdC0zX0R1UiB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDUsIDFmcik7XFxuICBnYXA6IDQ0cHg7XFxuICBtYXJnaW4tdG9wOiAyNHB4O1xcbn1cXG5cXG4uUHJpY2luZy1tb2R1bGUtZHk3ZzMge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlByaWNpbmctaW1hZ2VXcmFwcGVyLTJjdTk1IHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjZweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxufVxcblxcbi5QcmljaW5nLWltYWdlV3JhcHBlci0yY3U5NSBpbWcge1xcbiAgd2lkdGg6IDMycHg7XFxuICBoZWlnaHQ6IDMycHg7XFxufVxcblxcbi5QcmljaW5nLXNjcm9sbFRvcC1BYWcwNyB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLlByaWNpbmctc2Nyb2xsVG9wLUFhZzA3IGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLlByaWNpbmctcHJpY2VfY29udGVudC01cWxzaCBwIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIG9wYWNpdHk6IDAuNztcXG4gIGxpbmUtaGVpZ2h0OiAyO1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxufVxcblxcbi5QcmljaW5nLWFubnVhbC0zMGllXyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xcbiAgaGVpZ2h0OiBhdXRvO1xcbn1cXG5cXG4uUHJpY2luZy10b2dnbGVfYnRuLTN0OU04IHtcXG4gIG1hcmdpbi1yaWdodDogNTBweDtcXG59XFxuXFxuLlByaWNpbmctYW5udWFsX3RleHQtYzZoLUcge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDRweDtcXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2ZmZjtcXG59XFxuXFxuLlByaWNpbmctYW5udWFsX29mZmVyLTJWQndHIHtcXG4gIG9wYWNpdHk6IDAuODtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgbWFyZ2luLWJvdHRvbTogNHB4O1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbn1cXG5cXG4uUHJpY2luZy11bmRlcmxpbmUtVk5mUjUge1xcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIGJsYWNrO1xcbn1cXG5cXG4uUHJpY2luZy1wcmljaW5nX2NvbnRhaW5lci0yRFo5SyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG4uUHJpY2luZy1wbGFucy0zb3lSRyB7XFxuICBwYWRkaW5nOiAwIDQ4cHggMzJweCA0OHB4O1xcbiAgcGFkZGluZzogMCAzcmVtIDJyZW0gM3JlbTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5QcmljaW5nLXBsYW4tY2J5Qjcge1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiA0NzdweDtcXG4gIG1hcmdpbjogMCA2cHg7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5QcmljaW5nLXBsYW4tY2J5QjcgLlByaWNpbmctaW5uZXJDYXJkLVhELVhSIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBwYWRkaW5nOiAzMnB4IDA7XFxuICAgIHBhZGRpbmc6IDJyZW0gMDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgei1pbmRleDogMTtcXG4gIH1cXG5cXG4uUHJpY2luZy1zeW1ib2wtMVBabDcge1xcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcXG59XFxuXFxuLlByaWNpbmctcHJpY2luZ190aXRsZS0xOFZ4bSB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XFxufVxcblxcbi5QcmljaW5nLXRvZ2dsZV9vdXRlci1URHlVMCB7XFxuICB3aWR0aDogNTZweDtcXG4gIGhlaWdodDogMzFweDtcXG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDNweDtcXG59XFxuXFxuLlByaWNpbmctdG9nZ2xlX29uLUt5VG82IHtcXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4uUHJpY2luZy10b2dnbGVfaW5uZXItZHhOdWEge1xcbiAgd2lkdGg6IDI0LjlweDtcXG4gIGhlaWdodDogMjQuOXB4O1xcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5QcmljaW5nLXBsYW5fdGl0bGVfc3R1ZGVudHMtaXhIdEYge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5QcmljaW5nLXBsYW5fdGl0bGUtMnpfazAgcCB7XFxuICAgIG1hcmdpbjogMDtcXG4gIH1cXG5cXG4uUHJpY2luZy1wbGFuX3RpdGxlX3ByaWNlLTJaeDQ5IHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIG9wYWNpdHk6IDE7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5QcmljaW5nLXByaWNlLXJjdGdQIHtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlByaWNpbmctcHJpY2UtcmN0Z1AgcCB7XFxuICAgIG1hcmdpbjogMzJweCAwO1xcbiAgfVxcblxcbi5QcmljaW5nLXByaWNlX2Rlc2MtNHhWYkEge1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbn1cXG5cXG4uUHJpY2luZy1wcmljZV9kZXNjLTR4VmJBIHAge1xcbiAgICBtYXJnaW46IDAgMCAyNHB4IDA7XFxuICB9XFxuXFxuLlByaWNpbmctcGxhbi1jYnlCNyBidXR0b24ge1xcbiAgbWFyZ2luLXRvcDogNDBweDtcXG4gIHBhZGRpbmc6IDAlIDE1JTtcXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxufVxcblxcbi5QcmljaW5nLXByaWNlX2NvbnRhY3QtN3VOaWcgYSB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG4gIC8vIG1hcmdpbi1ib3R0b206IDUwMHB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5QcmljaW5nLXByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodC1xQVdjdSB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNTlkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZy1idXlCdXR0b24tM2cwTkkge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG59XFxuXFxuLlByaWNpbmctZW50ZXJwcmlzZS0xTy1tRiB7XFxuICB3aWR0aDogOTEuNSU7XFxuICBtYXJnaW46IGF1dG87XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIHBhZGRpbmc6IDQycHggMjAwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNTZweDtcXG59XFxuXFxuLlByaWNpbmctZW50ZXJwcmlzZUhlYWRpbmctMlhtNjgge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZy1lbnRlcnByaXNlY29udGV4dC0zQUQ5SiB7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG59XFxuXFxuLlByaWNpbmctY29udGFjdHVzLVhmTDlHIHtcXG4gIHBhZGRpbmc6IDhweCAyMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4vKiAuZmFxX2NvbnRhaW5lciB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5mYXFfdGl0bGUgcCB7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uZXhwYW5kX2FsbCB7XFxuICBtYXJnaW46IDAlIDEwJTtcXG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XFxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XFxufVxcblxcbi5hY2NvcmRpYW4ge1xcbiAgd2lkdGg6IDgwJTtcXG59ICovXFxuXFxuLyogLmJvcmRlciB7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2M3YzdjNztcXG4gIHBhZGRpbmc6IDQ0cHggMCA0MHB4IDA7XFxufVxcblxcbi5ib3JkZXI6bGFzdC1jaGlsZCB7XFxuICBib3JkZXItYm90dG9tOiBub25lO1xcbn0gKi9cXG5cXG4vKiAucXVlc3Rpb24ge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5hbnN3ZXIge1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNS40cHg7XFxuICB3aWR0aDogODUlO1xcbn0gKi9cXG5cXG4vKiAuc2hvdyAuYW5zd2VyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbn0gKi9cXG5cXG4vKiAudXAge1xcbiAgZGlzcGxheTogbm9uZTtcXG59ICovXFxuXFxuLyogLnNob3cgLnVwIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG59XFxuXFxuLnNob3cgLmRvd24ge1xcbiAgZGlzcGxheTogbm9uZTtcXG59ICovXFxuXFxuLlByaWNpbmctbGVmdEFycm93LTNCY3BHLFxcbi5QcmljaW5nLXJpZ2h0QXJyb3ctMUNHaXEge1xcbiAgZGlzcGxheTogbm9uZTtcXG59XFxuXFxuLlByaWNpbmctY2lyY2xlX2JnLXZidnRrIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGxlZnQ6IDEwJTtcXG4gIHRvcDogNSU7XFxufVxcblxcbi5QcmljaW5nLXNxdWFyZV9iZy0xZldPayB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICByaWdodDogMTAlO1xcbiAgdG9wOiA1JTtcXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG59XFxuXFxuLlByaWNpbmctbGluZV92ZWN0b3JfYmctMlprc3Mge1xcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xcbiAgICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xcbiAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xcbiAgb3BhY2l0eTogMC41O1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMTAlO1xcbn1cXG5cXG4uUHJpY2luZy10cmlhbmdsZV9iZy0xWDdJNCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICByaWdodDogNyU7XFxufVxcblxcbi5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWluLWhlaWdodDogNDY0cHg7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4uUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiBzcGFuIGEge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XFxufVxcblxcbi5QcmljaW5nLWZlYXR1cmVkLTN2ZjhMIC5QcmljaW5nLXNob3dNZWRpYS02VTc2cSBhIHtcXG4gIHdpZHRoOiAyNTlweDtcXG4gIGhlaWdodDogMTI3cHg7XFxufVxcblxcbi5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIHNwYW4gYTpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLlByaWNpbmctY2xpZW50c1dyYXBwZXItNjVHX2kge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg2LCAxZnIpO1xcbiAgZ2FwOiAyNHB4O1xcbiAgZ2FwOiAxLjVyZW07XFxuICBtYXJnaW46IDAgYXV0bztcXG59XFxuXFxuLlByaWNpbmctY2xpZW50c1dyYXBwZXItNjVHX2kgLlByaWNpbmctY2xpZW50LTE0a0d0IHtcXG4gIHdpZHRoOiAxNzJweDtcXG4gIGhlaWdodDogMTEycHg7XFxufVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZEhlYWRpbmctaHhud3Uge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gICAgLy8gZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxNnB4O1xcbiAgICBnYXA6IDFyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIHtcXG4gICAgICB3aWR0aDogMjcwcHg7XFxuICAgICAgaGVpZ2h0OiAyMDJweDtcXG4gICAgICBmb250LXNpemU6IDI0cHg7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICBwYWRkaW5nOiAyNHB4O1xcbiAgICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gICAgICBib3JkZXItcmFkaXVzOiAwLjVyZW07XFxuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICAgICAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWFjaGlldmVkUHJvZmlsZS0xamdkSSB7XFxuICAgICAgICB3aWR0aDogNTJweDtcXG4gICAgICAgIGhlaWdodDogNTJweDtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctYWNoaWV2ZWRQcm9maWxlLTFqZ2RJLlByaWNpbmctY2xpZW50cy0ybW5DWCB7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmM2ViO1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1jYXJkLTMtak1kIC5QcmljaW5nLWFjaGlldmVkUHJvZmlsZS0xamdkSS5QcmljaW5nLXN0dWRlbnRzLTFZSHBVIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2VmZmU7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctYWNoaWV2ZWRQcm9maWxlLTFqZ2RJLlByaWNpbmctdGVzdHMtM25EVXoge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1hY2hpZXZlZFByb2ZpbGUtMWpnZEkuUHJpY2luZy1xdWVzdGlvbnMtM3FKOEQge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ViZmZlZjtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1oaWdobGlnaHQtMy1Uc20ge1xcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1oaWdobGlnaHQtMy1Uc20uUHJpY2luZy1xdWVzdGlvbnNIaWdobGlnaHQtMUhKbFIge1xcbiAgICAgICAgY29sb3I6ICMwMGFjMjY7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctaGlnaGxpZ2h0LTMtVHNtLlByaWNpbmctdGVzdHNIaWdobGlnaHQtdmthcHEge1xcbiAgICAgICAgY29sb3I6ICNmMzY7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctaGlnaGxpZ2h0LTMtVHNtLlByaWNpbmctc3R1ZGVudHNIaWdobGlnaHQtMlV1Tzkge1xcbiAgICAgICAgY29sb3I6ICM4YzAwZmU7XFxuICAgICAgfVxcblxcbi5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIC5QcmljaW5nLWNhcmQtMy1qTWQgLlByaWNpbmctaGlnaGxpZ2h0LTMtVHNtLlByaWNpbmctY2xpZW50c0hpZ2hsaWdodC0yaFVRRSB7XFxuICAgICAgICBjb2xvcjogI2Y2MDtcXG4gICAgICB9XFxuXFxuLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctY2FyZC0zLWpNZCAuUHJpY2luZy1zdWJUZXh0LTNQRld6IHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4uUHJpY2luZy1mZWF0dXJlZC0zdmY4TCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDQwcHg7XFxuICBwYWRkaW5nOiAzLjVyZW0gNHJlbSAyLjVyZW07XFxufVxcblxcbi5QcmljaW5nLWZlYXR1cmVkLTN2ZjhMIGgyIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICBtYXJnaW4tYm90dG9tOiAyLjVyZW07XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgZm9udC1zaXplOiAyLjVyZW07XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiAzcmVtO1xcbn1cXG5cXG4uUHJpY2luZy1mZWF0dXJlZC0zdmY4TCAuUHJpY2luZy1zaG93TWVkaWEtNlU3NnEge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xcbiAgZ2FwOiA0MHB4O1xcbiAgZ2FwOiAyLjVyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5QcmljaW5nLXRpdGxlYm94LTFtQmN5IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICBtYXJnaW4tYm90dG9tOiAyOHB4O1xcbn1cXG5cXG4uUHJpY2luZy10aXRsZWJveC0xbUJjeSBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xcbiAgICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuLlByaWNpbmctdGl0bGVib3gtMW1CY3kgaW1nIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgfVxcblxcbi5QcmljaW5nLWZlYXR1cmVkLTN2ZjhMIC5QcmljaW5nLXNob3dNZWRpYS02VTc2cSBhIGltZyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5QcmljaW5nLW9uX25ld3MtMmJnRGgge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGhlaWdodDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIGhlaWdodDogLW1vei1tYXgtY29udGVudDtcXG4gIGhlaWdodDogbWF4LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gIGdhcDogMTJweDtcXG4gIG1hcmdpbjogMCBhdXRvIDU2cHggYXV0bztcXG59XFxuXFxuLlByaWNpbmctb25fbmV3cy0yYmdEaCAuUHJpY2luZy1mZWF0dXJlZEltYWdlLUpvYy05IHtcXG4gICAgd2lkdGg6IDM2OHB4O1xcbiAgICBoZWlnaHQ6IDI1MHB4O1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICB9XFxuXFxuLlByaWNpbmctb25fbmV3cy0yYmdEaCAuUHJpY2luZy1mZWF0dXJlZEltYWdlLUpvYy05Om50aC1jaGlsZCgxKSB7XFxuICAgIG1hcmdpbi1sZWZ0OiAwO1xcbiAgfVxcblxcbi5QcmljaW5nLW9uX25ld3MtMmJnRGggLlByaWNpbmctZmVhdHVyZWRJbWFnZS1Kb2MtOTpudGgtY2hpbGQoMykge1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICB9XFxuXFxuLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiA1NnB4IDE5MnB4O1xcbiAgcGFkZGluZzogMy41cmVtIDEycmVtO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIC5QcmljaW5nLWF3YXJkc1RpdGxlLTJGX3A4IHtcXG4gICAgZm9udC1zaXplOiA0MHB4O1xcbiAgICBmb250LXNpemU6IDIuNXJlbTtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIG1hcmdpbi1ib3R0b206IDQ4cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDNyZW07XFxuICB9XFxuXFxuLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIHtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gICAgZ2FwOiA4OHB4O1xcbiAgICBnYXA6IDUuNXJlbTtcXG4gIH1cXG5cXG4uUHJpY2luZy1hd2FyZHNDb250YWluZXItMlB5SWcgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wgLlByaWNpbmctYXdhcmRMb2dvLTJwcXZQIHtcXG4gICAgICB3aWR0aDogMjQwcHg7XFxuICAgICAgd2lkdGg6IDE1cmVtO1xcbiAgICAgIGhlaWdodDogMjA4cHg7XFxuICAgICAgaGVpZ2h0OiAxM3JlbTtcXG4gICAgfVxcblxcbi5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1hd2FyZExvZ28tMnBxdlA6bnRoLWNoaWxkKDNuICsgMikge1xcbiAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgfVxcblxcbi5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1kb3RzLTJKdDIzIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4uUHJpY2luZy1wb3B1cG92ZXJsYXktMzJ5ZXoge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgdG9wOiAwO1xcbiAgbGVmdDogMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcXG4gIHotaW5kZXg6IDM7XFxufVxcblxcbi5QcmljaW5nLXByZXNzcG9wdXAtMVVDUEoge1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiA1ODVweDtcXG4gIGhlaWdodDogNDA3cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xcbiAgei1pbmRleDogNDtcXG59XFxuXFxuLlByaWNpbmctcGxheWVyLTFGQ3NjIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAzMDJweDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLlByaWNpbmctbW9iaWxlUHJpY2luZ0NvbnRhaW5lci0zdzZRayB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLlByaWNpbmctbW9kdWxlc0NvbnRhaW5lci05aDJZeiB7XFxuICAgIG1hcmdpbjogNjRweCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmctdGFic0NvbnRhaW5lci0xLTVhbSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWFubnVhbHRhYi1keUlRWiB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcXG4gICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogOHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctbW9udGh0YWItM0p5X3Mge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIHdpZHRoOiA1MCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogOHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctYWN0aXZlVGFiLTN2aTFSIHtcXG4gICAgLy8gY3Vyc29yOiBwb2ludGVyO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgICBib3JkZXI6IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZy1tb2JpbGVQbGFucy12Ykh4aCB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gICAgcGFkZGluZy1ib3R0b206IDEycHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1tb2JpbGVQcmljZUNhcmQtMzJ1Qloge1xcbiAgICBwYWRkaW5nOiAyNHB4IDUycHggMjRweCA1M3B4O1xcbiAgICBtYXJnaW46IDEycHggMDtcXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICBtYXgtd2lkdGg6IDMyOHB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgfVxcblxcbiAgLlByaWNpbmctcHJlc3Nwb3B1cC0xVUNQSiB7XFxuICAgIHBhZGRpbmc6IDEycHg7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiAyMTdweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRpdGxlYm94LTFtQmN5IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRpdGxlYm94LTFtQmN5IGgyIHtcXG4gICAgZm9udC1zaXplOiAxMS41cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1tb2R1bGVzTGlzdC0zX0R1UiB7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICAgIGdyaWQtY29sdW1uLWdhcDogNDBweDtcXG4gICAgZ3JpZC1yb3ctZ2FwOiAyMHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctaW1hZ2VXcmFwcGVyLTJjdTk1IHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDBweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWltYWdlV3JhcHBlci0yY3U5NSBpbWcge1xcbiAgICB3aWR0aDogMjRweDtcXG4gICAgaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0TW9iaWxlLTM2aVhGIHtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgzMWRlZywgI2YzNiAyOSUsICNmZjYwMWQgNzAlKTtcXG4gICAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNTlkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBvcGFjaXR5OiAxICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBtaW4taGVpZ2h0OiAyN3JlbTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIGgzIHtcXG4gICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICBtYXgtd2lkdGg6IDE0Ljg3NXJlbTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIC5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDAgMDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxcmVtO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIC5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIC5QcmljaW5nLWNsaWVudC0xNGtHdCB7XFxuICAgIHdpZHRoOiA5Ljc1cmVtO1xcbiAgICBoZWlnaHQ6IDdyZW07XFxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMzcsIDQwLCA0MywgMC4xNik7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kaXNwbGF5Q2xpZW50cy1TeGdOUiAuUHJpY2luZy1jbGllbnRzV3JhcHBlci02NUdfaSAuUHJpY2luZy1jbGllbnQtMTRrR3QuUHJpY2luZy1hY3RpdmUtMWZBM24ge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWxlZnRBcnJvdy0zQmNwRyB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB3aWR0aDogMjVweDtcXG4gICAgaGVpZ2h0OiAyNXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgbGVmdDogLTQwcHg7XFxuICAgIHRvcDogNTAlO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB9XFxuXFxuICAgIC5QcmljaW5nLWxlZnRBcnJvdy0zQmNwRyBzcGFuIHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBjb2xvcjogcmdiKDM3LCA0MCwgNDMsIDAuNSk7XFxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgIGxlZnQ6IDdweDtcXG4gICAgICB0b3A6IC0ycHg7XFxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB9XFxuXFxuICAuUHJpY2luZy1yaWdodEFycm93LTFDR2lxIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHdpZHRoOiAyNXB4O1xcbiAgICBoZWlnaHQ6IDI1cHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JheTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICByaWdodDogLTQwcHg7XFxuICAgIHRvcDogNTAlO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB9XFxuXFxuICAgIC5QcmljaW5nLXJpZ2h0QXJyb3ctMUNHaXEgc3BhbiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgY29sb3I6IHJnYigzNywgNDAsIDQzLCAwLjUpO1xcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICBsZWZ0OiA3cHg7XFxuICAgICAgdG9wOiAtMnB4O1xcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgfVxcblxcbiAgLlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlIgLlByaWNpbmctY2xpZW50c1dyYXBwZXItNjVHX2kgc3BhbiB7XFxuICAgIG1heC13aWR0aDogNDAwcHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kb3RzLTJKdDIzIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogNTZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWRpc3BsYXlDbGllbnRzLVN4Z05SIC5QcmljaW5nLWNsaWVudHNXcmFwcGVyLTY1R19pIC5QcmljaW5nLWRvdHMtMkp0MjMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2ltYWdlcy9ob21lL0NvbmZldHRpLnN2ZycpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAuUHJpY2luZy1hY2hpZXZlZENvbnRhaW5lci0zTkJNZiAuUHJpY2luZy1hY2hpZXZlZEhlYWRpbmctaHhud3Uge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIHBhZGRpbmc6IDA7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xcbiAgICB9XFxuXFxuICAgICAgLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRIZWFkaW5nLWh4bnd1IHNwYW46OmFmdGVyIHtcXG4gICAgICAgIGNvbnRlbnQ6ICdcXFxcQSc7XFxuICAgICAgICB3aGl0ZS1zcGFjZTogcHJlO1xcbiAgICAgIH1cXG5cXG4gICAgLlByaWNpbmctYWNoaWV2ZWRDb250YWluZXItM05CTWYgLlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0wge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIGdhcDogMC43NXJlbTtcXG4gICAgICBtYXgtd2lkdGg6IDIwLjVyZW07XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgfVxcblxcbiAgLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIHtcXG4gICAgcGFkZGluZzogMS41cmVtO1xcbiAgfVxcblxcbiAgICAuUHJpY2luZy1hd2FyZHNDb250YWluZXItMlB5SWcgLlByaWNpbmctYXdhcmRzVGl0bGUtMkZfcDgge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICAgIH1cXG5cXG4gICAgLlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnIC5QcmljaW5nLWFjaGlldmVkUm93LTI5VHNMIHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgICAgIGdhcDogMS41cmVtIDFyZW07XFxuICAgICAgbWFyZ2luOiBhdXRvO1xcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgfVxcblxcbiAgICAgIC5QcmljaW5nLWF3YXJkc0NvbnRhaW5lci0yUHlJZyAuUHJpY2luZy1hY2hpZXZlZFJvdy0yOVRzTCAuUHJpY2luZy1hd2FyZExvZ28tMnBxdlAge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICBtYXgtd2lkdGg6IDE1cmVtO1xcbiAgICAgICAgbWF4LWhlaWdodDogMTNyZW07XFxuICAgICAgfVxcbiAgICAuUHJpY2luZy1wcmljZV9jb250YWN0LTd1TmlnIGEge1xcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjFweDtcXG4gICAgfVxcblxcbiAgLlByaWNpbmctZmVhdHVyZWQtM3ZmOEwge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbSAxLjI1cmVtO1xcbiAgfVxcblxcbiAgICAuUHJpY2luZy1mZWF0dXJlZC0zdmY4TCBoMiB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgICBtYXJnaW46IDAgMCAxLjVyZW0gMDtcXG4gICAgfVxcblxcbiAgICAuUHJpY2luZy1mZWF0dXJlZC0zdmY4TCAuUHJpY2luZy1zaG93TWVkaWEtNlU3NnEge1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBtYXJnaW46IDAgYXV0byAwIGF1dG87XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMWZyO1xcbiAgICAgIGdhcDogMXJlbTtcXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIH1cXG5cXG4gICAgICAuUHJpY2luZy1mZWF0dXJlZC0zdmY4TCAuUHJpY2luZy1zaG93TWVkaWEtNlU3NnEgYSB7XFxuICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICBoZWlnaHQ6IDcycHg7XFxuICAgICAgfVxcblxcbiAgLlByaWNpbmctb25fbmV3cy0yYmdEaCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMSwgMWZyKTtcXG4gIH1cXG5cXG4gICAgLlByaWNpbmctb25fbmV3cy0yYmdEaCAuUHJpY2luZy1mZWF0dXJlZEltYWdlLUpvYy05IHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBtYXgtd2lkdGg6IDM0MHB4O1xcbiAgICB9XFxuXFxuICAuUHJpY2luZy1wcmljaW5nSGVhZGVyLTJNTG9IIHtcXG4gICAgcGFkZGluZzogMjRweCAwIDA7XFxuICB9XFxuXFxuICAuUHJpY2luZy1zY3JvbGxUb3AtQWFnMDcge1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICByaWdodDogMTZweDtcXG4gICAgYm90dG9tOiAxNnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcHJpY2VfdGl0bGUtM1lxbjgge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1wcmljZV9jb250ZW50LTVxbHNoIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbjogMTZweCAwIDA7XFxuICAgIG1heC13aWR0aDogOTAlO1xcbiAgfVxcblxcbiAgLlByaWNpbmctdG9nZ2xlX2J0bi0zdDlNOCB7XFxuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctdG9nZ2xlX291dGVyLVREeVUwIHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogMjJweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRvZ2dsZV9pbm5lci1keE51YSB7XFxuICAgIHdpZHRoOiAxNy44cHg7XFxuICAgIGhlaWdodDogMTcuNnB4O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcGxhbi1jYnlCNyB7XFxuICAgIHdpZHRoOiAyNzBweDtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgbWFyZ2luOiAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmctYWN0aXZlLTFmQTNuIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kb3QtMldkMW4ge1xcbiAgICB3aWR0aDogMTBweDtcXG4gICAgaGVpZ2h0OiAxMHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlNWU1ZTU7XFxuICAgIG1hcmdpbjogMCA1cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1kb3RhY3RpdmUtMzFfXy0ge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgfVxcblxcbiAgLlByaWNpbmctcGxhbl90aXRsZV9zdHVkZW50cy1peEh0RiB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICB9XFxuXFxuICAuUHJpY2luZy1wbGFuX3RpdGxlLTJ6X2swIHAge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLyogLnF1ZXN0aW9uIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgfSAqL1xcbiAgICAuUHJpY2luZy1wcmljZS1yY3RnUCBwIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGV0dGVyLXNwYWNpbmc6IC0wLjk2cHg7XFxuICAgIH1cXG5cXG4gIC5QcmljaW5nLXByaWNlX2Rlc2MtNHhWYkEge1xcbiAgICB3aWR0aDogMTAxJTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjFweDtcXG4gICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgb3BhY2l0eTogMC44O1xcbiAgfVxcblxcbiAgLlByaWNpbmctYnV5QnV0dG9uLTNnME5JIHtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAxNzRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gICAgY29sb3I6ICNmZmY7XFxuICAgIHBhZGRpbmc6IDEwcHggMTZweDtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBtYXJnaW4tdG9wOiAyOHB4O1xcbiAgfVxcblxcbiAgLyogLmZhcV9jb250YWluZXIge1xcbiAgICBwYWRkaW5nOiAyNHB4O1xcblxcbiAgICAuZmFxX3RpdGxlIHAge1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBtYXJnaW4tdG9wOiAwO1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XFxuICAgIH1cXG5cXG4gICAgLmV4cGFuZF9hbGwge1xcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICBtYXJnaW46IDAgYXV0byA0cHggYXV0bztcXG4gICAgfVxcblxcbiAgICAuYWNjb3JkaWFuIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG5cXG4gICAgICAuYm9yZGVyIHtcXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMDtcXG5cXG4gICAgICAgIC5hbnN3ZXIge1xcbiAgICAgICAgICB3aWR0aDogMTAwJTtcXG5cXG4gICAgICAgICAgcCB7XFxuICAgICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XFxuICAgICAgICAgICAgb3BhY2l0eTogMC43O1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gICAgICAgICAgfVxcbiAgICAgICAgfVxcbiAgICAgIH1cXG4gICAgfVxcbiAgfSAqL1xcblxcbiAgLlByaWNpbmctY2lyY2xlX2JnLXZidnRrIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogMTIlO1xcbiAgICB0b3A6IDI1JTtcXG4gICAgbGVmdDogOTUlO1xcbiAgfVxcblxcbiAgLlByaWNpbmctc3F1YXJlX2JnLTFmV09rIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDEyMCU7XFxuICAgIGxlZnQ6IC0zJTtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgd2lkdGg6IDEwJTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLWxpbmVfdmVjdG9yX2JnLTJaa3NzIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nLXRyaWFuZ2xlX2JnLTFYN0k0IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAtMTAlO1xcbiAgICB3aWR0aDogMjAlO1xcbiAgICB0b3A6IDU1NHB4O1xcbiAgfVxcbn1cXG5cXG4uUHJpY2luZy1tb2R1bGVMYWJlbC0yOFZhbiB7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmctdmlld0xpbmstMS1iSHcge1xcbiAgd2lkdGg6IDE5NXB4O1xcbiAgaGVpZ2h0OiAzMnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luOiA2NC42cHggMCAxMTJweCAwO1xcbiAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuMzM7XFxuICBjb2xvcjogI2YzNjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAuUHJpY2luZy12aWV3TGluay0xLWJIdyB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbWFyZ2luOiAwIDAgNDhweCAwO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL3ByaWNpbmcvUHJpY2luZy5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxxQ0FBcUM7Q0FDdEM7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsVUFBVTtFQUNWLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QiwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQixvREFBb0Q7VUFDNUMsNENBQTRDO0VBQ3BELHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osYUFBYTtHQUNkOztBQUVIO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLG1CQUFtQjtFQUNuQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQiw4QkFBOEI7Q0FDL0I7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSwrQkFBK0I7Q0FDaEM7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7Q0FDNUI7O0FBRUQ7RUFDRSwwQkFBMEI7RUFDMUIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtDQUNwQjs7QUFFRDtJQUNJLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQix1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLG1CQUFtQjtJQUNuQixXQUFXO0dBQ1o7O0FBRUg7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGVBQWU7RUFDZixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFvQjtFQUNwQix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsbUJBQW1CO01BQ2YsMEJBQTBCO0NBQy9COztBQUVEO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7SUFDSSxVQUFVO0dBQ1g7O0FBRUg7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCO0NBQ2xCOztBQUVEO0lBQ0ksZUFBZTtHQUNoQjs7QUFFSDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCOztBQUVEO0lBQ0ksbUJBQW1CO0dBQ3BCOztBQUVIO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQiwyQkFBMkI7RUFDM0Isb0JBQW9CO0VBQ3BCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsd0VBQXdFO0VBQ3hFLG1FQUFtRTtFQUNuRSxnRUFBZ0U7RUFDaEUscUNBQXFDO0VBQ3JDLDhCQUE4QjtFQUM5QixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixxREFBcUQ7VUFDN0MsNkNBQTZDO0VBQ3JELG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwwQkFBMEI7TUFDdEIsOEJBQThCO0VBQ2xDLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBeUJJOztBQUVKOzs7Ozs7OztJQVFJOztBQUVKOzs7Ozs7Ozs7Ozs7Ozs7SUFlSTs7QUFFSjs7SUFFSTs7QUFFSjs7SUFFSTs7QUFFSjs7Ozs7OztJQU9JOztBQUVKOztFQUVFLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsUUFBUTtDQUNUOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxRQUFRO0VBQ1IsaUNBQWlDO01BQzdCLDZCQUE2QjtVQUN6Qix5QkFBeUI7Q0FDbEM7O0FBRUQ7RUFDRSxrQ0FBa0M7TUFDOUIsOEJBQThCO1VBQzFCLDBCQUEwQjtFQUNsQyxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFVBQVU7Q0FDWDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQywwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7RUFDZCxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsVUFBVTtFQUNWLFlBQVk7RUFDWixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsdURBQXVEO0VBQ3ZELHlCQUF5QjtFQUN6Qiw0QkFBNEI7RUFDNUIsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQiwwQkFBMEI7TUFDdEIsOEJBQThCO0NBQ25DOztBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7O0FBRUg7SUFDSSwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsVUFBVTtJQUNWLGFBQWE7R0FDZDs7QUFFSDtNQUNNLGFBQWE7TUFDYixjQUFjO01BQ2QsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQiw2QkFBNkI7TUFDN0Isa0JBQWtCO01BQ2xCLGNBQWM7TUFDZCxnQkFBZ0I7TUFDaEIsc0JBQXNCO01BQ3RCLCtEQUErRDtjQUN2RCx1REFBdUQ7TUFDL0QsdUJBQXVCO01BQ3ZCLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsMkJBQTJCO1VBQ3ZCLHVCQUF1QjtNQUMzQix1QkFBdUI7VUFDbkIsb0JBQW9CO0tBQ3pCOztBQUVMO1FBQ1EsWUFBWTtRQUNaLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIscUJBQXFCO1FBQ3JCLGNBQWM7UUFDZCxzQkFBc0I7WUFDbEIsd0JBQXdCO1FBQzVCLHVCQUF1QjtZQUNuQixvQkFBb0I7UUFDeEIsb0JBQW9CO09BQ3JCOztBQUVQO1FBQ1EsMEJBQTBCO09BQzNCOztBQUVQO1FBQ1EsMEJBQTBCO09BQzNCOztBQUVQO1FBQ1EsMEJBQTBCO09BQzNCOztBQUVQO1FBQ1EsMEJBQTBCO09BQzNCOztBQUVQO1FBQ1EsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsbUJBQW1CO09BQ3BCOztBQUVQO1FBQ1EsZUFBZTtPQUNoQjs7QUFFUDtRQUNRLFlBQVk7T0FDYjs7QUFFUDtRQUNRLGVBQWU7T0FDaEI7O0FBRVA7UUFDUSxZQUFZO09BQ2I7O0FBRVA7UUFDUSxnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtPQUNwQjs7QUFFUDtFQUNFLDBCQUEwQjtFQUMxQix3QkFBd0I7RUFDeEIsNEJBQTRCO0NBQzdCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixzQkFBc0I7RUFDdEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHVCQUF1QjtNQUNuQiwrQkFBK0I7RUFDbkMsb0JBQW9CO0NBQ3JCOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixXQUFXO0lBQ1gsb0JBQW9CO0lBQ3BCLDJCQUEyQjtPQUN4Qix3QkFBd0I7SUFDM0IsaUJBQWlCO0dBQ2xCOztBQUVIO0lBQ0ksZ0JBQWdCO0dBQ2pCOztBQUVIO0VBQ0UsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQiw0QkFBNEI7RUFDNUIseUJBQXlCO0VBQ3pCLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVix5QkFBeUI7Q0FDMUI7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsY0FBYztJQUNkLGdCQUFnQjtHQUNqQjs7QUFFSDtJQUNJLGVBQWU7R0FDaEI7O0FBRUg7SUFDSSxnQkFBZ0I7R0FDakI7O0FBRUg7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLG9CQUFvQjtFQUNwQixzQkFBc0I7RUFDdEIsdUJBQXVCO0NBQ3hCOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixvQkFBb0I7R0FDckI7O0FBRUg7SUFDSSxjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLFVBQVU7SUFDVixZQUFZO0dBQ2I7O0FBRUg7TUFDTSxhQUFhO01BQ2IsYUFBYTtNQUNiLGNBQWM7TUFDZCxjQUFjO0tBQ2Y7O0FBRUw7TUFDTSxnQkFBZ0I7S0FDakI7O0FBRUw7SUFDSSxjQUFjO0dBQ2Y7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLFFBQVE7RUFDUixZQUFZO0VBQ1osY0FBYztFQUNkLHFDQUFxQztFQUNyQyxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osY0FBYztDQUNmOztBQUVEO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxlQUFlO0dBQ2hCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMEJBQTBCO0dBQzNCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLGdDQUFnQztHQUNqQzs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsMEJBQTBCO0lBQzFCLG1CQUFtQjtJQUNuQiwrQkFBK0I7R0FDaEM7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFVBQVU7R0FDWDs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsMEJBQTBCO0lBQzFCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLDZCQUE2QjtJQUM3QixlQUFlO0lBQ2YscURBQXFEO1lBQzdDLDZDQUE2QztJQUNyRCwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtRQUNsQix3QkFBd0I7SUFDNUIsdUJBQXVCO1FBQ25CLG9CQUFvQjtJQUN4QiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsdUJBQXVCO0dBQ3hCOztFQUVEO0lBQ0UsY0FBYztJQUNkLGFBQWE7SUFDYixjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxzQ0FBc0M7SUFDdEMsc0JBQXNCO0lBQ3RCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQix3RUFBd0U7SUFDeEUsbUVBQW1FO0lBQ25FLGdFQUFnRTtJQUNoRSxxQ0FBcUM7SUFDckMsOEJBQThCO0lBQzlCLGVBQWU7SUFDZixzQkFBc0I7R0FDdkI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLHVCQUF1QjtHQUN4Qjs7RUFFRDtJQUNFLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixxQkFBcUI7R0FDdEI7O0VBRUQ7SUFDRSxvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQ0FBc0M7SUFDdEMsbUNBQW1DO0lBQ25DLFVBQVU7SUFDViwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGVBQWU7SUFDZixhQUFhO0lBQ2IseUNBQXlDO0dBQzFDOztFQUVEO0lBQ0UsZUFBZTtHQUNoQjs7RUFFRDtJQUNFLGVBQWU7SUFDZixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFNBQVM7SUFDVCxtQkFBbUI7R0FDcEI7O0lBRUM7TUFDRSxrQkFBa0I7TUFDbEIsNEJBQTRCO01BQzVCLG1CQUFtQjtNQUNuQixVQUFVO01BQ1YsVUFBVTtNQUNWLGdCQUFnQjtLQUNqQjs7RUFFSDtJQUNFLGVBQWU7SUFDZixZQUFZO0lBQ1osYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFNBQVM7SUFDVCxtQkFBbUI7R0FDcEI7O0lBRUM7TUFDRSxrQkFBa0I7TUFDbEIsNEJBQTRCO01BQzVCLG1CQUFtQjtNQUNuQixVQUFVO01BQ1YsVUFBVTtNQUNWLGdCQUFnQjtLQUNqQjs7RUFFSDtJQUNFLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1Qix1QkFBdUI7UUFDbkIsb0JBQW9CO0lBQ3hCLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztHQUNmOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLG1EQUFtRDtJQUNuRCwwQkFBMEI7R0FDM0I7O0lBRUM7TUFDRSxrQkFBa0I7TUFDbEIsV0FBVztNQUNYLHNCQUFzQjtLQUN2Qjs7TUFFQztRQUNFLGNBQWM7UUFDZCxpQkFBaUI7T0FDbEI7O0lBRUg7TUFDRSwyQkFBMkI7TUFDM0IsYUFBYTtNQUNiLG1CQUFtQjtNQUNuQiwyQkFBMkI7TUFDM0Isd0JBQXdCO01BQ3hCLG1CQUFtQjtNQUNuQixhQUFhO0tBQ2Q7O0VBRUg7SUFDRSxnQkFBZ0I7R0FDakI7O0lBRUM7TUFDRSxrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtLQUNyQjs7SUFFRDtNQUNFLDJCQUEyQjtNQUMzQix3QkFBd0I7TUFDeEIsbUJBQW1CO01BQ25CLCtCQUErQjtNQUMvQixpQkFBaUI7TUFDakIsYUFBYTtNQUNiLG1CQUFtQjtLQUNwQjs7TUFFQztRQUNFLFlBQVk7UUFDWixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGtCQUFrQjtPQUNuQjtJQUNIO01BQ0UsZ0JBQWdCO01BQ2hCLGtCQUFrQjtLQUNuQjs7RUFFSDtJQUNFLDZCQUE2QjtHQUM5Qjs7SUFFQztNQUNFLGtCQUFrQjtNQUNsQixvQkFBb0I7TUFDcEIscUJBQXFCO0tBQ3RCOztJQUVEO01BQ0UsMkJBQTJCO01BQzNCLHdCQUF3QjtNQUN4QixtQkFBbUI7TUFDbkIsc0JBQXNCO01BQ3RCLCtCQUErQjtNQUMvQixVQUFVO01BQ1YsbUJBQW1CO0tBQ3BCOztNQUVDO1FBQ0UsYUFBYTtRQUNiLGFBQWE7T0FDZDs7RUFFTDtJQUNFLHNCQUFzQjtJQUN0QixzQ0FBc0M7R0FDdkM7O0lBRUM7TUFDRSxZQUFZO01BQ1osaUJBQWlCO0tBQ2xCOztFQUVIO0lBQ0Usa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixlQUFlO0dBQ2hCOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsZUFBZTtHQUNoQjs7RUFFRDtJQUNFLGFBQWE7SUFDYixjQUFjO0lBQ2QsVUFBVTtHQUNYOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7R0FDZjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSx1QkFBdUI7R0FDeEI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDs7O01BR0k7SUFDRjtNQUNFLGdCQUFnQjtNQUNoQix3QkFBd0I7S0FDekI7O0VBRUg7SUFDRSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsYUFBYTtHQUNkOztFQUVEO0lBQ0UsMkJBQTJCO0lBQzNCLHdCQUF3QjtJQUN4QixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLG9CQUFvQjtJQUNwQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsaUJBQWlCO0dBQ2xCOztFQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7TUFpQ0k7O0VBRUo7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxTQUFTO0lBQ1QsVUFBVTtHQUNYOztFQUVEO0lBQ0UsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsVUFBVTtJQUNWLGlDQUFpQztRQUM3Qiw2QkFBNkI7WUFDekIseUJBQXlCO0lBQ2pDLFdBQVc7R0FDWjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXO0dBQ1o7Q0FDRjs7QUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFO0lBQ0UsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtHQUNwQjtDQUNGXCIsXCJmaWxlXCI6XCJQcmljaW5nLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnByaWNpbmdIZWFkZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIHBhZGRpbmctdG9wOiA1JTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLmRhcmtCYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG59XFxuXFxuLnByaWNlX3RpdGxlIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBmb250LXNpemU6IDU2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLnByaWNlX2NvbnRlbnQge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgbWFyZ2luLXRvcDogMTJweDtcXG59XFxuXFxuLm1vZHVsZXNDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDcycHg7XFxufVxcblxcbi5tb2R1bGVzSGVhZGVyIHtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLm1vZHVsZXNMaXN0IHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNSwgMWZyKTtcXG4gIGdhcDogNDRweDtcXG4gIG1hcmdpbi10b3A6IDI0cHg7XFxufVxcblxcbi5tb2R1bGUge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmltYWdlV3JhcHBlciB7XFxuICB3aWR0aDogNTJweDtcXG4gIGhlaWdodDogNTJweDtcXG4gIGJvcmRlci1yYWRpdXM6IDI2cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMnB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbn1cXG5cXG4uaW1hZ2VXcmFwcGVyIGltZyB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMzJweDtcXG59XFxuXFxuLnNjcm9sbFRvcCB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLnNjcm9sbFRvcCBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi5wcmljZV9jb250ZW50IHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgb3BhY2l0eTogMC43O1xcbiAgbGluZS1oZWlnaHQ6IDI7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG59XFxuXFxuLmFubnVhbCB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xcbiAgaGVpZ2h0OiBhdXRvO1xcbn1cXG5cXG4udG9nZ2xlX2J0biB7XFxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7XFxufVxcblxcbi5hbm51YWxfdGV4dCB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBwYWRkaW5nLWJvdHRvbTogNHB4O1xcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZmZmO1xcbn1cXG5cXG4uYW5udWFsX29mZmVyIHtcXG4gIG9wYWNpdHk6IDAuODtcXG4gIGNvbG9yOiAjZjM2O1xcbiAgbWFyZ2luLWJvdHRvbTogNHB4O1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbn1cXG5cXG4udW5kZXJsaW5lIHtcXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCBibGFjaztcXG59XFxuXFxuLnByaWNpbmdfY29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcblxcbi5wbGFucyB7XFxuICBwYWRkaW5nOiAwIDQ4cHggMzJweCA0OHB4O1xcbiAgcGFkZGluZzogMCAzcmVtIDJyZW0gM3JlbTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcbi5wbGFuIHtcXG4gIHdpZHRoOiAyNzBweDtcXG4gIGhlaWdodDogNDc3cHg7XFxuICBtYXJnaW46IDAgNnB4O1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4ucGxhbiAuaW5uZXJDYXJkIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBwYWRkaW5nOiAzMnB4IDA7XFxuICAgIHBhZGRpbmc6IDJyZW0gMDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgei1pbmRleDogMTtcXG4gIH1cXG5cXG4uc3ltYm9sIHtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XFxufVxcblxcbi5wcmljaW5nX3RpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB3aWR0aDogMTAwJTtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcXG59XFxuXFxuLnRvZ2dsZV9vdXRlciB7XFxuICB3aWR0aDogNTZweDtcXG4gIGhlaWdodDogMzFweDtcXG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDNweDtcXG59XFxuXFxuLnRvZ2dsZV9vbiB7XFxuICAtbXMtZmxleC1wYWNrOiBlbmQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZV9pbm5lciB7XFxuICB3aWR0aDogMjQuOXB4O1xcbiAgaGVpZ2h0OiAyNC45cHg7XFxuICBib3JkZXItcmFkaXVzOiAxMDAlO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG59XFxuXFxuLnBsYW5fdGl0bGVfc3R1ZGVudHMge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgY29sb3I6ICMwMDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5wbGFuX3RpdGxlIHAge1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuLnBsYW5fdGl0bGVfcHJpY2Uge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgb3BhY2l0eTogMTtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLnByaWNlIHtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLnByaWNlIHAge1xcbiAgICBtYXJnaW46IDMycHggMDtcXG4gIH1cXG5cXG4ucHJpY2VfZGVzYyB7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxufVxcblxcbi5wcmljZV9kZXNjIHAge1xcbiAgICBtYXJnaW46IDAgMCAyNHB4IDA7XFxuICB9XFxuXFxuLnBsYW4gYnV0dG9uIHtcXG4gIG1hcmdpbi10b3A6IDQwcHg7XFxuICBwYWRkaW5nOiAwJSAxNSU7XFxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbn1cXG5cXG4ucHJpY2VfY29udGFjdCBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgLy8gbWFyZ2luLWJvdHRvbTogNTAwcHg7XFxuICBjb2xvcjogIzAwMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLnByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodCB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNTlkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uYnV5QnV0dG9uIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5lbnRlcnByaXNlIHtcXG4gIHdpZHRoOiA5MS41JTtcXG4gIG1hcmdpbjogYXV0bztcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgcGFkZGluZzogNDJweCAyMDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA1NnB4O1xcbn1cXG5cXG4uZW50ZXJwcmlzZUhlYWRpbmcge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uZW50ZXJwcmlzZWNvbnRleHQge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxufVxcblxcbi5jb250YWN0dXMge1xcbiAgcGFkZGluZzogOHB4IDIwcHg7XFxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxufVxcblxcbi8qIC5mYXFfY29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLmZhcV90aXRsZSBwIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5leHBhbmRfYWxsIHtcXG4gIG1hcmdpbjogMCUgMTAlO1xcbiAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBibGFjaztcXG4gIHdpZHRoOiBtYXgtY29udGVudDtcXG59XFxuXFxuLmFjY29yZGlhbiB7XFxuICB3aWR0aDogODAlO1xcbn0gKi9cXG5cXG4vKiAuYm9yZGVyIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYzdjN2M3O1xcbiAgcGFkZGluZzogNDRweCAwIDQwcHggMDtcXG59XFxuXFxuLmJvcmRlcjpsYXN0LWNoaWxkIHtcXG4gIGJvcmRlci1ib3R0b206IG5vbmU7XFxufSAqL1xcblxcbi8qIC5xdWVzdGlvbiB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG59XFxuXFxuLmFuc3dlciB7XFxuICBkaXNwbGF5OiBub25lO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDI1LjRweDtcXG4gIHdpZHRoOiA4NSU7XFxufSAqL1xcblxcbi8qIC5zaG93IC5hbnN3ZXIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufSAqL1xcblxcbi8qIC51cCB7XFxuICBkaXNwbGF5OiBub25lO1xcbn0gKi9cXG5cXG4vKiAuc2hvdyAudXAge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xcbn1cXG5cXG4uc2hvdyAuZG93biB7XFxuICBkaXNwbGF5OiBub25lO1xcbn0gKi9cXG5cXG4ubGVmdEFycm93LFxcbi5yaWdodEFycm93IHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufVxcblxcbi5jaXJjbGVfYmcge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMTAlO1xcbiAgdG9wOiA1JTtcXG59XFxuXFxuLnNxdWFyZV9iZyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICByaWdodDogMTAlO1xcbiAgdG9wOiA1JTtcXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG59XFxuXFxuLmxpbmVfdmVjdG9yX2JnIHtcXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gIG9wYWNpdHk6IDAuNTtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGxlZnQ6IDEwJTtcXG59XFxuXFxuLnRyaWFuZ2xlX2JnIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHJpZ2h0OiA3JTtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWluLWhlaWdodDogNDY0cHg7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggNDBweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgaDMge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXJnaW4tdG9wOiAwO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzAwNzZmZjtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtYXgtd2lkdGg6IDExNTJweDtcXG4gIG1hcmdpbjogMTJweCBhdXRvIDA7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyBzcGFuIGEge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XFxufVxcblxcbi5mZWF0dXJlZCAuc2hvd01lZGlhIGEge1xcbiAgd2lkdGg6IDI1OXB4O1xcbiAgaGVpZ2h0OiAxMjdweDtcXG59XFxuXFxuLmRpc3BsYXlDbGllbnRzIHNwYW4gYTpob3ZlciB7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuLmNsaWVudHNXcmFwcGVyIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5jbGllbnRzV3JhcHBlciAuY2xpZW50IHtcXG4gIHdpZHRoOiAxNzJweDtcXG4gIGhlaWdodDogMTEycHg7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2ltYWdlcy9ob21lL25ld19jb25mZXR0aS5zdmcnKTtcXG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZEhlYWRpbmcge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xcbiAgICAvLyBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBnYXA6IDE2cHg7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQge1xcbiAgICAgIHdpZHRoOiAyNzBweDtcXG4gICAgICBoZWlnaHQ6IDIwMnB4O1xcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBjb2xvcjogcmdiYSgzNywgNDAsIDQzLCAwLjYpO1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgICAgIHBhZGRpbmc6IDI0cHg7XFxuICAgICAgcGFkZGluZzogMS41cmVtO1xcbiAgICAgIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMC4yNXJlbSAxLjVyZW0gMCByZ2JhKDE0MCwgMCwgMjU0LCAwLjE2KTtcXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZSB7XFxuICAgICAgICB3aWR0aDogNTJweDtcXG4gICAgICAgIGhlaWdodDogNTJweDtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5jbGllbnRzIHtcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmYzZWI7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS5zdHVkZW50cyB7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUudGVzdHMge1xcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWJmMDtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnF1ZXN0aW9ucyB7XFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWJmZmVmO1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQge1xcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnF1ZXN0aW9uc0hpZ2hsaWdodCB7XFxuICAgICAgICBjb2xvcjogIzAwYWMyNjtcXG4gICAgICB9XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LnRlc3RzSGlnaGxpZ2h0IHtcXG4gICAgICAgIGNvbG9yOiAjZjM2O1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQuc3R1ZGVudHNIaWdobGlnaHQge1xcbiAgICAgICAgY29sb3I6ICM4YzAwZmU7XFxuICAgICAgfVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5jbGllbnRzSGlnaGxpZ2h0IHtcXG4gICAgICAgIGNvbG9yOiAjZjYwO1xcbiAgICAgIH1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5zdWJUZXh0IHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIH1cXG5cXG4uZmVhdHVyZWQge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgcGFkZGluZzogMy41cmVtIDRyZW0gMi41cmVtO1xcbn1cXG5cXG4uZmVhdHVyZWQgaDIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcXG4gIG1hcmdpbi1ib3R0b206IDIuNXJlbTtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBmb250LXNpemU6IDIuNXJlbTtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgbGluZS1oZWlnaHQ6IDNyZW07XFxufVxcblxcbi5mZWF0dXJlZCAuc2hvd01lZGlhIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIGdhcDogNDBweDtcXG4gIGdhcDogMi41cmVtO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4udGl0bGVib3gge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIG1hcmdpbi1ib3R0b206IDI4cHg7XFxufVxcblxcbi50aXRsZWJveCBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xcbiAgICAtby10ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcXG4gICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XFxuICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICB9XFxuXFxuLnRpdGxlYm94IGltZyB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gIH1cXG5cXG4uZmVhdHVyZWQgLnNob3dNZWRpYSBhIGltZyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5vbl9uZXdzIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otbWF4LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IG1heC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICBnYXA6IDEycHg7XFxuICBtYXJnaW46IDAgYXV0byA1NnB4IGF1dG87XFxufVxcblxcbi5vbl9uZXdzIC5mZWF0dXJlZEltYWdlIHtcXG4gICAgd2lkdGg6IDM2OHB4O1xcbiAgICBoZWlnaHQ6IDI1MHB4O1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICB9XFxuXFxuLm9uX25ld3MgLmZlYXR1cmVkSW1hZ2U6bnRoLWNoaWxkKDEpIHtcXG4gICAgbWFyZ2luLWxlZnQ6IDA7XFxuICB9XFxuXFxuLm9uX25ld3MgLmZlYXR1cmVkSW1hZ2U6bnRoLWNoaWxkKDMpIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgfVxcblxcbi5hd2FyZHNDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDU2cHggMTkycHg7XFxuICBwYWRkaW5nOiAzLjVyZW0gMTJyZW07XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIC5hd2FyZHNUaXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogNDBweDtcXG4gICAgZm9udC1zaXplOiAyLjVyZW07XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBtYXJnaW4tYm90dG9tOiA0OHB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xcbiAgfVxcblxcbi5hd2FyZHNDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gICAgZ2FwOiA4OHB4O1xcbiAgICBnYXA6IDUuNXJlbTtcXG4gIH1cXG5cXG4uYXdhcmRzQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuYXdhcmRMb2dvIHtcXG4gICAgICB3aWR0aDogMjQwcHg7XFxuICAgICAgd2lkdGg6IDE1cmVtO1xcbiAgICAgIGhlaWdodDogMjA4cHg7XFxuICAgICAgaGVpZ2h0OiAxM3JlbTtcXG4gICAgfVxcblxcbi5hd2FyZHNDb250YWluZXIgLmFjaGlldmVkUm93IC5hd2FyZExvZ286bnRoLWNoaWxkKDNuICsgMikge1xcbiAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgfVxcblxcbi5hd2FyZHNDb250YWluZXIgLmRvdHMge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbi5wb3B1cG92ZXJsYXkge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgdG9wOiAwO1xcbiAgbGVmdDogMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcXG4gIHotaW5kZXg6IDM7XFxufVxcblxcbi5wcmVzc3BvcHVwIHtcXG4gIHBhZGRpbmc6IDI0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1heC13aWR0aDogNTg1cHg7XFxuICBoZWlnaHQ6IDQwN3B4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcXG4gIHotaW5kZXg6IDQ7XFxufVxcblxcbi5wbGF5ZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDMwMnB4O1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAubW9iaWxlUHJpY2luZ0NvbnRhaW5lciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLm1vZHVsZXNDb250YWluZXIge1xcbiAgICBtYXJnaW46IDY0cHggMDtcXG4gIH1cXG5cXG4gIC50YWJzQ29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgLmFubnVhbHRhYiB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcXG4gICAgcGFkZGluZzogMjRweCAzMnB4O1xcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogOHB4O1xcbiAgfVxcblxcbiAgLm1vbnRodGFiIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB3aWR0aDogNTAlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDhweDtcXG4gIH1cXG5cXG4gIC5hY3RpdmVUYWIge1xcbiAgICAvLyBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgIGJvcmRlcjogMDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVQbGFucyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gICAgcGFkZGluZy1ib3R0b206IDEycHg7XFxuICB9XFxuXFxuICAubW9iaWxlUHJpY2VDYXJkIHtcXG4gICAgcGFkZGluZzogMjRweCA1MnB4IDI0cHggNTNweDtcXG4gICAgbWFyZ2luOiAxMnB4IDA7XFxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgbWF4LXdpZHRoOiAzMjhweDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4gIC5wcmVzc3BvcHVwIHtcXG4gICAgcGFkZGluZzogMTJweDtcXG4gICAgd2lkdGg6IDMyOHB4O1xcbiAgICBoZWlnaHQ6IDIxN3B4O1xcbiAgfVxcblxcbiAgLnRpdGxlYm94IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC50aXRsZWJveCBoMiB7XFxuICAgIGZvbnQtc2l6ZTogMTEuNXB4O1xcbiAgfVxcblxcbiAgLm1vZHVsZXNMaXN0IHtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMywgMWZyKTtcXG4gICAgZ3JpZC1jb2x1bW4tZ2FwOiA0MHB4O1xcbiAgICBncmlkLXJvdy1nYXA6IDIwcHg7XFxuICB9XFxuXFxuICAuaW1hZ2VXcmFwcGVyIHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDBweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVdyYXBwZXIgaW1nIHtcXG4gICAgd2lkdGg6IDI0cHg7XFxuICAgIGhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5wcmljZVBlclN0dWRlbnRIaWdobGlnaHRNb2JpbGUge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDMxZGVnLCAjZjM2IDI5JSwgI2ZmNjAxZCA3MCUpO1xcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMzFkZWcsICNmMzYgMjklLCAjZmY2MDFkIDcwJSk7XFxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCg1OWRlZywgI2YzNiAyOSUsICNmZjYwMWQgNzAlKTtcXG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIG9wYWNpdHk6IDEgIWltcG9ydGFudDtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBtaW4taGVpZ2h0OiAyN3JlbTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyBoMyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWF4LXdpZHRoOiAxNC44NzVyZW07XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDAgMDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxcmVtO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICAgIHdpZHRoOiA5Ljc1cmVtO1xcbiAgICBoZWlnaHQ6IDdyZW07XFxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMzcsIDQwLCA0MywgMC4xNik7XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQuYWN0aXZlIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICB9XFxuXFxuICAubGVmdEFycm93IHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHdpZHRoOiAyNXB4O1xcbiAgICBoZWlnaHQ6IDI1cHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JheTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAtNDBweDtcXG4gICAgdG9wOiA1MCU7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIH1cXG5cXG4gICAgLmxlZnRBcnJvdyBzcGFuIHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBjb2xvcjogcmdiKDM3LCA0MCwgNDMsIDAuNSk7XFxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgIGxlZnQ6IDdweDtcXG4gICAgICB0b3A6IC0ycHg7XFxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB9XFxuXFxuICAucmlnaHRBcnJvdyB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB3aWR0aDogMjVweDtcXG4gICAgaGVpZ2h0OiAyNXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcmlnaHQ6IC00MHB4O1xcbiAgICB0b3A6IDUwJTtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgfVxcblxcbiAgICAucmlnaHRBcnJvdyBzcGFuIHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBjb2xvcjogcmdiKDM3LCA0MCwgNDMsIDAuNSk7XFxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgIGxlZnQ6IDdweDtcXG4gICAgICB0b3A6IC0ycHg7XFxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHNwYW4ge1xcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xcbiAgfVxcblxcbiAgLmRvdHMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiA1NnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgfVxcblxcbiAgLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuZG90cyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgfVxcblxcbiAgLmFjaGlldmVkQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2ltYWdlcy9ob21lL0NvbmZldHRpLnN2ZycpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xcbiAgfVxcblxcbiAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyB7XFxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgICAgcGFkZGluZzogMDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XFxuICAgIH1cXG5cXG4gICAgICAuYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkSGVhZGluZyBzcGFuOjphZnRlciB7XFxuICAgICAgICBjb250ZW50OiAnXFxcXGEnO1xcbiAgICAgICAgd2hpdGUtc3BhY2U6IHByZTtcXG4gICAgICB9XFxuXFxuICAgIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICAgIGdhcDogMC43NXJlbTtcXG4gICAgICBtYXgtd2lkdGg6IDIwLjVyZW07XFxuICAgICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICAgIG1hcmdpbjogYXV0bztcXG4gICAgfVxcblxcbiAgLmF3YXJkc0NvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbTtcXG4gIH1cXG5cXG4gICAgLmF3YXJkc0NvbnRhaW5lciAuYXdhcmRzVGl0bGUge1xcbiAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICAgIH1cXG5cXG4gICAgLmF3YXJkc0NvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAxZnI7XFxuICAgICAgZ2FwOiAxLjVyZW0gMXJlbTtcXG4gICAgICBtYXJnaW46IGF1dG87XFxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB9XFxuXFxuICAgICAgLmF3YXJkc0NvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmF3YXJkTG9nbyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIG1heC13aWR0aDogMTVyZW07XFxuICAgICAgICBtYXgtaGVpZ2h0OiAxM3JlbTtcXG4gICAgICB9XFxuICAgIC5wcmljZV9jb250YWN0IGEge1xcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjFweDtcXG4gICAgfVxcblxcbiAgLmZlYXR1cmVkIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW0gMS4yNXJlbTtcXG4gIH1cXG5cXG4gICAgLmZlYXR1cmVkIGgyIHtcXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xcbiAgICAgIG1hcmdpbjogMCAwIDEuNXJlbSAwO1xcbiAgICB9XFxuXFxuICAgIC5mZWF0dXJlZCAuc2hvd01lZGlhIHtcXG4gICAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gMCBhdXRvO1xcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcXG4gICAgICBnYXA6IDFyZW07XFxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB9XFxuXFxuICAgICAgLmZlYXR1cmVkIC5zaG93TWVkaWEgYSB7XFxuICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICBoZWlnaHQ6IDcycHg7XFxuICAgICAgfVxcblxcbiAgLm9uX25ld3Mge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDEsIDFmcik7XFxuICB9XFxuXFxuICAgIC5vbl9uZXdzIC5mZWF0dXJlZEltYWdlIHtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBtYXgtd2lkdGg6IDM0MHB4O1xcbiAgICB9XFxuXFxuICAucHJpY2luZ0hlYWRlciB7XFxuICAgIHBhZGRpbmc6IDI0cHggMCAwO1xcbiAgfVxcblxcbiAgLnNjcm9sbFRvcCB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAucHJpY2VfdGl0bGUge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICB9XFxuXFxuICAucHJpY2VfY29udGVudCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW46IDE2cHggMCAwO1xcbiAgICBtYXgtd2lkdGg6IDkwJTtcXG4gIH1cXG5cXG4gIC50b2dnbGVfYnRuIHtcXG4gICAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAudG9nZ2xlX291dGVyIHtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogMjJweDtcXG4gIH1cXG5cXG4gIC50b2dnbGVfaW5uZXIge1xcbiAgICB3aWR0aDogMTcuOHB4O1xcbiAgICBoZWlnaHQ6IDE3LjZweDtcXG4gIH1cXG5cXG4gIC5wbGFuIHtcXG4gICAgd2lkdGg6IDI3MHB4O1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBtYXJnaW46IDA7XFxuICB9XFxuXFxuICAuYWN0aXZlIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICB9XFxuXFxuICAuZG90IHtcXG4gICAgd2lkdGg6IDEwcHg7XFxuICAgIGhlaWdodDogMTBweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTVlNWU1O1xcbiAgICBtYXJnaW46IDAgNXB4O1xcbiAgfVxcblxcbiAgLmRvdGFjdGl2ZSB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICB9XFxuXFxuICAucGxhbl90aXRsZV9zdHVkZW50cyB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICB9XFxuXFxuICAucGxhbl90aXRsZSBwIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC8qIC5xdWVzdGlvbiB7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIH0gKi9cXG4gICAgLnByaWNlIHAge1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBsZXR0ZXItc3BhY2luZzogLTAuOTZweDtcXG4gICAgfVxcblxcbiAgLnByaWNlX2Rlc2Mge1xcbiAgICB3aWR0aDogMTAxJTtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjFweDtcXG4gICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgb3BhY2l0eTogMC44O1xcbiAgfVxcblxcbiAgLmJ1eUJ1dHRvbiB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogMTc0cHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgbWFyZ2luLXRvcDogMjhweDtcXG4gIH1cXG5cXG4gIC8qIC5mYXFfY29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogMjRweDtcXG5cXG4gICAgLmZhcV90aXRsZSBwIHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgICB9XFxuXFxuICAgIC5leHBhbmRfYWxsIHtcXG4gICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgbWFyZ2luOiAwIGF1dG8gNHB4IGF1dG87XFxuICAgIH1cXG5cXG4gICAgLmFjY29yZGlhbiB7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuXFxuICAgICAgLmJvcmRlciB7XFxuICAgICAgICBwYWRkaW5nOiAxNnB4IDA7XFxuXFxuICAgICAgICAuYW5zd2VyIHtcXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XFxuXFxuICAgICAgICAgIHAge1xcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNztcXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XFxuICAgICAgICAgIH1cXG4gICAgICAgIH1cXG4gICAgICB9XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC5jaXJjbGVfYmcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHdpZHRoOiAxMiU7XFxuICAgIHRvcDogMjUlO1xcbiAgICBsZWZ0OiA5NSU7XFxuICB9XFxuXFxuICAuc3F1YXJlX2JnIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDEyMCU7XFxuICAgIGxlZnQ6IC0zJTtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcXG4gICAgd2lkdGg6IDEwJTtcXG4gIH1cXG5cXG4gIC5saW5lX3ZlY3Rvcl9iZyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAudHJpYW5nbGVfYmcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGxlZnQ6IC0xMCU7XFxuICAgIHdpZHRoOiAyMCU7XFxuICAgIHRvcDogNTU0cHg7XFxuICB9XFxufVxcblxcbi5tb2R1bGVMYWJlbCB7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLnZpZXdMaW5rIHtcXG4gIHdpZHRoOiAxOTVweDtcXG4gIGhlaWdodDogMzJweDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbjogNjQuNnB4IDAgMTEycHggMDtcXG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjMzO1xcbiAgY29sb3I6ICNmMzY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLnZpZXdMaW5rIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBtYXJnaW46IDAgMCA0OHB4IDA7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJwcmljaW5nSGVhZGVyXCI6IFwiUHJpY2luZy1wcmljaW5nSGVhZGVyLTJNTG9IXCIsXG5cdFwiZGFya0JhY2tncm91bmRcIjogXCJQcmljaW5nLWRhcmtCYWNrZ3JvdW5kLTE3YWc4XCIsXG5cdFwicHJpY2VfdGl0bGVcIjogXCJQcmljaW5nLXByaWNlX3RpdGxlLTNZcW44XCIsXG5cdFwicHJpY2VfY29udGVudFwiOiBcIlByaWNpbmctcHJpY2VfY29udGVudC01cWxzaFwiLFxuXHRcIm1vZHVsZXNDb250YWluZXJcIjogXCJQcmljaW5nLW1vZHVsZXNDb250YWluZXItOWgyWXpcIixcblx0XCJtb2R1bGVzSGVhZGVyXCI6IFwiUHJpY2luZy1tb2R1bGVzSGVhZGVyLXpmaVVZXCIsXG5cdFwibW9kdWxlc0xpc3RcIjogXCJQcmljaW5nLW1vZHVsZXNMaXN0LTNfRHVSXCIsXG5cdFwibW9kdWxlXCI6IFwiUHJpY2luZy1tb2R1bGUtZHk3ZzNcIixcblx0XCJpbWFnZVdyYXBwZXJcIjogXCJQcmljaW5nLWltYWdlV3JhcHBlci0yY3U5NVwiLFxuXHRcInNjcm9sbFRvcFwiOiBcIlByaWNpbmctc2Nyb2xsVG9wLUFhZzA3XCIsXG5cdFwiYW5udWFsXCI6IFwiUHJpY2luZy1hbm51YWwtMzBpZV9cIixcblx0XCJ0b2dnbGVfYnRuXCI6IFwiUHJpY2luZy10b2dnbGVfYnRuLTN0OU04XCIsXG5cdFwiYW5udWFsX3RleHRcIjogXCJQcmljaW5nLWFubnVhbF90ZXh0LWM2aC1HXCIsXG5cdFwiYW5udWFsX29mZmVyXCI6IFwiUHJpY2luZy1hbm51YWxfb2ZmZXItMlZCd0dcIixcblx0XCJ1bmRlcmxpbmVcIjogXCJQcmljaW5nLXVuZGVybGluZS1WTmZSNVwiLFxuXHRcInByaWNpbmdfY29udGFpbmVyXCI6IFwiUHJpY2luZy1wcmljaW5nX2NvbnRhaW5lci0yRFo5S1wiLFxuXHRcInBsYW5zXCI6IFwiUHJpY2luZy1wbGFucy0zb3lSR1wiLFxuXHRcInBsYW5cIjogXCJQcmljaW5nLXBsYW4tY2J5QjdcIixcblx0XCJpbm5lckNhcmRcIjogXCJQcmljaW5nLWlubmVyQ2FyZC1YRC1YUlwiLFxuXHRcInN5bWJvbFwiOiBcIlByaWNpbmctc3ltYm9sLTFQWmw3XCIsXG5cdFwicHJpY2luZ190aXRsZVwiOiBcIlByaWNpbmctcHJpY2luZ190aXRsZS0xOFZ4bVwiLFxuXHRcInRvZ2dsZV9vdXRlclwiOiBcIlByaWNpbmctdG9nZ2xlX291dGVyLVREeVUwXCIsXG5cdFwidG9nZ2xlX29uXCI6IFwiUHJpY2luZy10b2dnbGVfb24tS3lUbzZcIixcblx0XCJ0b2dnbGVfaW5uZXJcIjogXCJQcmljaW5nLXRvZ2dsZV9pbm5lci1keE51YVwiLFxuXHRcInBsYW5fdGl0bGVfc3R1ZGVudHNcIjogXCJQcmljaW5nLXBsYW5fdGl0bGVfc3R1ZGVudHMtaXhIdEZcIixcblx0XCJwbGFuX3RpdGxlXCI6IFwiUHJpY2luZy1wbGFuX3RpdGxlLTJ6X2swXCIsXG5cdFwicGxhbl90aXRsZV9wcmljZVwiOiBcIlByaWNpbmctcGxhbl90aXRsZV9wcmljZS0yWng0OVwiLFxuXHRcInByaWNlXCI6IFwiUHJpY2luZy1wcmljZS1yY3RnUFwiLFxuXHRcInByaWNlX2Rlc2NcIjogXCJQcmljaW5nLXByaWNlX2Rlc2MtNHhWYkFcIixcblx0XCJwcmljZV9jb250YWN0XCI6IFwiUHJpY2luZy1wcmljZV9jb250YWN0LTd1TmlnXCIsXG5cdFwicHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0XCI6IFwiUHJpY2luZy1wcmljZVBlclN0dWRlbnRIaWdobGlnaHQtcUFXY3VcIixcblx0XCJidXlCdXR0b25cIjogXCJQcmljaW5nLWJ1eUJ1dHRvbi0zZzBOSVwiLFxuXHRcImVudGVycHJpc2VcIjogXCJQcmljaW5nLWVudGVycHJpc2UtMU8tbUZcIixcblx0XCJlbnRlcnByaXNlSGVhZGluZ1wiOiBcIlByaWNpbmctZW50ZXJwcmlzZUhlYWRpbmctMlhtNjhcIixcblx0XCJlbnRlcnByaXNlY29udGV4dFwiOiBcIlByaWNpbmctZW50ZXJwcmlzZWNvbnRleHQtM0FEOUpcIixcblx0XCJjb250YWN0dXNcIjogXCJQcmljaW5nLWNvbnRhY3R1cy1YZkw5R1wiLFxuXHRcImxlZnRBcnJvd1wiOiBcIlByaWNpbmctbGVmdEFycm93LTNCY3BHXCIsXG5cdFwicmlnaHRBcnJvd1wiOiBcIlByaWNpbmctcmlnaHRBcnJvdy0xQ0dpcVwiLFxuXHRcImNpcmNsZV9iZ1wiOiBcIlByaWNpbmctY2lyY2xlX2JnLXZidnRrXCIsXG5cdFwic3F1YXJlX2JnXCI6IFwiUHJpY2luZy1zcXVhcmVfYmctMWZXT2tcIixcblx0XCJsaW5lX3ZlY3Rvcl9iZ1wiOiBcIlByaWNpbmctbGluZV92ZWN0b3JfYmctMlprc3NcIixcblx0XCJ0cmlhbmdsZV9iZ1wiOiBcIlByaWNpbmctdHJpYW5nbGVfYmctMVg3STRcIixcblx0XCJkaXNwbGF5Q2xpZW50c1wiOiBcIlByaWNpbmctZGlzcGxheUNsaWVudHMtU3hnTlJcIixcblx0XCJmZWF0dXJlZFwiOiBcIlByaWNpbmctZmVhdHVyZWQtM3ZmOExcIixcblx0XCJzaG93TWVkaWFcIjogXCJQcmljaW5nLXNob3dNZWRpYS02VTc2cVwiLFxuXHRcImNsaWVudHNXcmFwcGVyXCI6IFwiUHJpY2luZy1jbGllbnRzV3JhcHBlci02NUdfaVwiLFxuXHRcImNsaWVudFwiOiBcIlByaWNpbmctY2xpZW50LTE0a0d0XCIsXG5cdFwiYWNoaWV2ZWRDb250YWluZXJcIjogXCJQcmljaW5nLWFjaGlldmVkQ29udGFpbmVyLTNOQk1mXCIsXG5cdFwiYWNoaWV2ZWRIZWFkaW5nXCI6IFwiUHJpY2luZy1hY2hpZXZlZEhlYWRpbmctaHhud3VcIixcblx0XCJhY2hpZXZlZFJvd1wiOiBcIlByaWNpbmctYWNoaWV2ZWRSb3ctMjlUc0xcIixcblx0XCJjYXJkXCI6IFwiUHJpY2luZy1jYXJkLTMtak1kXCIsXG5cdFwiYWNoaWV2ZWRQcm9maWxlXCI6IFwiUHJpY2luZy1hY2hpZXZlZFByb2ZpbGUtMWpnZElcIixcblx0XCJjbGllbnRzXCI6IFwiUHJpY2luZy1jbGllbnRzLTJtbkNYXCIsXG5cdFwic3R1ZGVudHNcIjogXCJQcmljaW5nLXN0dWRlbnRzLTFZSHBVXCIsXG5cdFwidGVzdHNcIjogXCJQcmljaW5nLXRlc3RzLTNuRFV6XCIsXG5cdFwicXVlc3Rpb25zXCI6IFwiUHJpY2luZy1xdWVzdGlvbnMtM3FKOERcIixcblx0XCJoaWdobGlnaHRcIjogXCJQcmljaW5nLWhpZ2hsaWdodC0zLVRzbVwiLFxuXHRcInF1ZXN0aW9uc0hpZ2hsaWdodFwiOiBcIlByaWNpbmctcXVlc3Rpb25zSGlnaGxpZ2h0LTFISmxSXCIsXG5cdFwidGVzdHNIaWdobGlnaHRcIjogXCJQcmljaW5nLXRlc3RzSGlnaGxpZ2h0LXZrYXBxXCIsXG5cdFwic3R1ZGVudHNIaWdobGlnaHRcIjogXCJQcmljaW5nLXN0dWRlbnRzSGlnaGxpZ2h0LTJVdU85XCIsXG5cdFwiY2xpZW50c0hpZ2hsaWdodFwiOiBcIlByaWNpbmctY2xpZW50c0hpZ2hsaWdodC0yaFVRRVwiLFxuXHRcInN1YlRleHRcIjogXCJQcmljaW5nLXN1YlRleHQtM1BGV3pcIixcblx0XCJ0aXRsZWJveFwiOiBcIlByaWNpbmctdGl0bGVib3gtMW1CY3lcIixcblx0XCJvbl9uZXdzXCI6IFwiUHJpY2luZy1vbl9uZXdzLTJiZ0RoXCIsXG5cdFwiZmVhdHVyZWRJbWFnZVwiOiBcIlByaWNpbmctZmVhdHVyZWRJbWFnZS1Kb2MtOVwiLFxuXHRcImF3YXJkc0NvbnRhaW5lclwiOiBcIlByaWNpbmctYXdhcmRzQ29udGFpbmVyLTJQeUlnXCIsXG5cdFwiYXdhcmRzVGl0bGVcIjogXCJQcmljaW5nLWF3YXJkc1RpdGxlLTJGX3A4XCIsXG5cdFwiYXdhcmRMb2dvXCI6IFwiUHJpY2luZy1hd2FyZExvZ28tMnBxdlBcIixcblx0XCJkb3RzXCI6IFwiUHJpY2luZy1kb3RzLTJKdDIzXCIsXG5cdFwicG9wdXBvdmVybGF5XCI6IFwiUHJpY2luZy1wb3B1cG92ZXJsYXktMzJ5ZXpcIixcblx0XCJwcmVzc3BvcHVwXCI6IFwiUHJpY2luZy1wcmVzc3BvcHVwLTFVQ1BKXCIsXG5cdFwicGxheWVyXCI6IFwiUHJpY2luZy1wbGF5ZXItMUZDc2NcIixcblx0XCJtb2JpbGVQcmljaW5nQ29udGFpbmVyXCI6IFwiUHJpY2luZy1tb2JpbGVQcmljaW5nQ29udGFpbmVyLTN3NlFrXCIsXG5cdFwidGFic0NvbnRhaW5lclwiOiBcIlByaWNpbmctdGFic0NvbnRhaW5lci0xLTVhbVwiLFxuXHRcImFubnVhbHRhYlwiOiBcIlByaWNpbmctYW5udWFsdGFiLWR5SVFaXCIsXG5cdFwibW9udGh0YWJcIjogXCJQcmljaW5nLW1vbnRodGFiLTNKeV9zXCIsXG5cdFwiYWN0aXZlVGFiXCI6IFwiUHJpY2luZy1hY3RpdmVUYWItM3ZpMVJcIixcblx0XCJtb2JpbGVQbGFuc1wiOiBcIlByaWNpbmctbW9iaWxlUGxhbnMtdmJIeGhcIixcblx0XCJtb2JpbGVQcmljZUNhcmRcIjogXCJQcmljaW5nLW1vYmlsZVByaWNlQ2FyZC0zMnVCWlwiLFxuXHRcInByaWNlUGVyU3R1ZGVudEhpZ2hsaWdodE1vYmlsZVwiOiBcIlByaWNpbmctcHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0TW9iaWxlLTM2aVhGXCIsXG5cdFwiYWN0aXZlXCI6IFwiUHJpY2luZy1hY3RpdmUtMWZBM25cIixcblx0XCJkb3RcIjogXCJQcmljaW5nLWRvdC0yV2QxblwiLFxuXHRcImRvdGFjdGl2ZVwiOiBcIlByaWNpbmctZG90YWN0aXZlLTMxX18tXCIsXG5cdFwibW9kdWxlTGFiZWxcIjogXCJQcmljaW5nLW1vZHVsZUxhYmVsLTI4VmFuXCIsXG5cdFwidmlld0xpbmtcIjogXCJQcmljaW5nLXZpZXdMaW5rLTEtYkh3XCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgcyBmcm9tICcuL01vZGFsLnNjc3MnO1xuXG5jbGFzcyBNb2RhbCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIC8qKiBMaWJyYXJ5IFZhcmlhYmxlIGZvciB2ZXJpZnlpbmcgcHJvcHMgZnJvbSB0aGUgcGFyZW50IGNvbXBvbmVudCAqL1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZS5pc1JlcXVpcmVkLFxuICAgIG1vZGFsQ2xhc3NOYW1lOiBQcm9wVHlwZXMub2JqZWN0LCAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgb3ZlcmxheUNsYXNzTmFtZTogUHJvcFR5cGVzLm9iamVjdCwgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgIHRvZ2dsZU1vZGFsOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGlmICh0aGlzLm1vZGFsQ29udGFpbmVyKSB7XG4gICAgICB0aGlzLm1vZGFsQ29udGFpbmVyLmZvY3VzKCk7XG4gICAgfVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5oYW5kbGVPdXRzaWRlQ2xpY2ssIGZhbHNlKTtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgdGhpcy5oYW5kbGVPdXRzaWRlQ2xpY2ssIGZhbHNlKTtcbiAgfVxuXG4gIGhhbmRsZUtleURvd25FdmVudCA9IGUgPT4ge1xuICAgIGlmIChlLndoaWNoID09PSAyNykge1xuICAgICAgdGhpcy5wcm9wcy50b2dnbGVNb2RhbCgpO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVPdXRzaWRlQ2xpY2sgPSBlID0+IHtcbiAgICAvLyBpZ25vcmUgY2xpY2tzIG9uIHRoZSBjb21wb25lbnQgaXRzZWxmXG4gICAgaWYgKHRoaXMubW9kYWxDb250YWluZXIgJiYgdGhpcy5tb2RhbENvbnRhaW5lci5jb250YWlucyhlLnRhcmdldCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaWYgKHRoaXMubW9kYWxDb250YWluZXIpIHtcbiAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5oYW5kbGVPdXRzaWRlQ2xpY2ssIGZhbHNlKTtcbiAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXG4gICAgICAgICd0b3VjaHN0YXJ0JyxcbiAgICAgICAgdGhpcy5oYW5kbGVPdXRzaWRlQ2xpY2ssXG4gICAgICAgIGZhbHNlLFxuICAgICAgKTtcbiAgICAgIHRoaXMucHJvcHMudG9nZ2xlTW9kYWwoKTtcbiAgICB9XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17YGJvZHktb3ZlcmxheSAke3MuYm9keU92ZXJsYXl9ICR7dGhpcy5wcm9wcy5vdmVybGF5Q2xhc3NOYW1lfWB9XG4gICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICBvbktleURvd249e3RoaXMuaGFuZGxlS2V5RG93bkV2ZW50fVxuICAgICAgICByZWY9e3JlZiA9PiB7XG4gICAgICAgICAgdGhpcy5tb2RhbE92ZXJsYXkgPSByZWY7XG4gICAgICAgIH19XG4gICAgICA+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e2Ake3MubW9kYWx9ICR7dGhpcy5wcm9wcy5tb2RhbENsYXNzTmFtZX1gfVxuICAgICAgICAgIHJlZj17cmVmID0+IHtcbiAgICAgICAgICAgIHRoaXMubW9kYWxDb250YWluZXIgPSByZWY7XG4gICAgICAgICAgfX1cbiAgICAgICAgICB0YWJJbmRleD1cIi0xXCJcbiAgICAgICAgPlxuICAgICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShNb2RhbCk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL01vZGFsLnNjc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vTW9kYWwuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL01vZGFsLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgUmVhY3RQbGF5ZXIgZnJvbSAncmVhY3QtcGxheWVyJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgTW9kYWwgZnJvbSAnY29tcG9uZW50cy9Nb2RhbCc7XG5pbXBvcnQge1xuICBIT01FX0NMSUVOVFNfVVBEQVRFRCxcbiAgUFJFU1NfVklERU9fQ09WRVJBR0UsXG4gIEhPTUVfTUVESUEsXG4gIEhPTUVfQVdBUkRTLFxufSBmcm9tICcuLi9HZXRSYW5rc0NvbnN0YW50cyc7XG5pbXBvcnQge1xuICBwcmljaW5nUGVyQW5udW1EZXRhaWxzLFxuICBwcmljaW5nUGVyTW9udGhEZXRhaWxzLFxuICBNb2R1bGVzTGlzdCxcbn0gZnJvbSAnLi9jb25zdGFudHMnO1xuaW1wb3J0IHMgZnJvbSAnLi9QcmljaW5nLnNjc3MnO1xuXG5mdW5jdGlvbiBQcmljaW5nKCkge1xuICBjb25zdCBbYWN0aXZlSW5kZXgsIHNldEFjdGl2ZUluZGV4XSA9IHVzZVN0YXRlKDApO1xuICAvLyBjb25zdCBbZXhwYW5kQWxsLCBzZXRFeHBhbmRBbGxdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBbdG9nZ2xlLCBzZXRUb2dnbGVdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBbc2hvd1Njcm9sbCwgc2V0U2hvd1Njcm9sbF0gPSB1c2VTdGF0ZShmYWxzZSk7XG4gIGNvbnN0IFtzaG93TW9kYWwsIHNldFNob3dNb2RhbF0gPSB1c2VTdGF0ZShmYWxzZSk7XG4gIGNvbnN0IFthY3RpdmVQcmVzcywgc2V0QWN0aXZlUHJlc3NdID0gdXNlU3RhdGUobnVsbCk7XG4gIGNvbnN0IFtpc01vYmlsZSwgc2V0SXNNb2JpbGVdID0gdXNlU3RhdGUoZmFsc2UpO1xuXG4gIGNvbnN0IGhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LnNjcm9sbFkgPiA1MDApIHtcbiAgICAgIHNldFNob3dTY3JvbGwodHJ1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNldFNob3dTY3JvbGwoZmFsc2UpO1xuICAgIH1cbiAgfTtcblxuICBjb25zdCBoYW5kbGVSZXNpemUgPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDk5MCkge1xuICAgICAgc2V0SXNNb2JpbGUoZmFsc2UpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzZXRJc01vYmlsZSh0cnVlKTtcbiAgICB9XG4gIH07XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBoYW5kbGVTY3JvbGwoKTtcbiAgICBoYW5kbGVSZXNpemUoKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgaGFuZGxlU2Nyb2xsKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgaGFuZGxlUmVzaXplKTtcbiAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGhhbmRsZVNjcm9sbCk7XG4gICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgaGFuZGxlUmVzaXplKTtcbiAgICB9O1xuICB9KTtcblxuICBjb25zdCBoYW5kbGVTY3JvbGxUb3AgPSAoKSA9PiB7XG4gICAgd2luZG93LnNjcm9sbFRvKHtcbiAgICAgIHRvcDogMCxcbiAgICAgIGJlaGF2aW9yOiAnc21vb3RoJyxcbiAgICB9KTtcbiAgICBzZXRTaG93U2Nyb2xsKGZhbHNlKTtcbiAgfTtcblxuICBjb25zdCBkaXNwbGF5U2Nyb2xsVG9Ub3AgPSAoKSA9PlxuICAgIHNob3dTY3JvbGwgJiYgKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e3Muc2Nyb2xsVG9wfVxuICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIGhhbmRsZVNjcm9sbFRvcCgpO1xuICAgICAgICB9fVxuICAgICAgPlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS9zY3JvbGxUb3Auc3ZnXCIgYWx0PVwic2Nyb2xsVG9wXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgICk7XG5cbiAgY29uc3QgdG9nZ2xlSGFuZGxlciA9ICgpID0+IHtcbiAgICBzZXRUb2dnbGUoIXRvZ2dsZSk7XG4gIH07XG5cbiAgY29uc3QgZGlzcGxheU1vZHVsZXMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlc0NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2R1bGVzSGVhZGVyfT5FdmVyeSBwbGFuIGluY2x1ZGVzPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2R1bGVzTGlzdH0+XG4gICAgICAgIHtNb2R1bGVzTGlzdC5tYXAobW9kdWxlID0+IChcbiAgICAgICAgICA8TGluayB0bz17YC9tb2R1bGVzLyR7bW9kdWxlLnZhbHVlfWB9IGNsYXNzTmFtZT17cy5tb2R1bGV9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW1hZ2VXcmFwcGVyfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9e21vZHVsZS5wYXRofSBhbHQ9e21vZHVsZS5sYWJlbH0gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlTGFiZWx9Pnttb2R1bGUubGFiZWx9PC9kaXY+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MubGluZV92ZWN0b3JfYmd9XG4gICAgICAgIHNyYz1cImltYWdlcy9pY29ucy92ZWN0b3JfbGluZS5wbmdcIlxuICAgICAgICBhbHQ9XCJcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLnRyaWFuZ2xlX2JnfVxuICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvdHJpYW5nbGUtYmFja2dyb3VuZC5zdmdcIlxuICAgICAgICBhbHQ9XCJcIlxuICAgICAgLz5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBjb25zdCBnb3RvUHJldlNsaWRlID0gKCkgPT4ge1xuICAgIGlmIChhY3RpdmVJbmRleCA9PT0gMCkge1xuICAgICAgc2V0QWN0aXZlSW5kZXgoMyk7XG4gICAgfSBlbHNlIHNldEFjdGl2ZUluZGV4KGFjdGl2ZUluZGV4IC0gMSk7XG4gIH07XG4gIGNvbnN0IGdvdG9OZXh0U2xpZGUgPSAoKSA9PiB7XG4gICAgaWYgKGFjdGl2ZUluZGV4ID09PSAzKSBzZXRBY3RpdmVJbmRleCgwKTtcbiAgICBlbHNlIHNldEFjdGl2ZUluZGV4KGFjdGl2ZUluZGV4ICsgMSk7XG4gIH07XG5cbiAgY29uc3QgZGlzcGxheU1vYmlsZVByaWNpbmdzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vYmlsZVByaWNpbmdDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFic0NvbnRhaW5lcn0+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuYW5udWFsdGFifSAkeyF0b2dnbGUgPyBzLmFjdGl2ZVRhYiA6IG51bGx9YH1cbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICBzZXRUb2dnbGUoZmFsc2UpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICBBbm51YWwgUGxhbnNcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICBjbGFzc05hbWU9e2Ake3MubW9udGh0YWJ9ICR7dG9nZ2xlID8gcy5hY3RpdmVUYWIgOiBudWxsfWB9XG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgc2V0VG9nZ2xlKHRydWUpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICBNb250aGx5IFBsYW5zXG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2JpbGVQbGFuc30+XG4gICAgICAgIHsodG9nZ2xlID8gcHJpY2luZ1Blck1vbnRoRGV0YWlscyA6IHByaWNpbmdQZXJBbm51bURldGFpbHMpLm1hcChcbiAgICAgICAgICBkZXRhaWwgPT4gKFxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9iaWxlUHJpY2VDYXJkfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhbl90aXRsZV9wcmljZX0+e2RldGFpbC5wYWNrfTwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3N0dWRlbnRzfT5cbiAgICAgICAgICAgICAgICB1cHRvIHtkZXRhaWwubm9PZlN0dWRlbnRzfSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAge3RvZ2dsZSA/IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV9kZXNjfT5cbiAgICAgICAgICAgICAgICAgIHN3aXRjaCB0byBhbm51YWwgcGFjayB0byBzYXZlIDE1JVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX2Rlc2N9PlxuICAgICAgICAgICAgICAgICAgUGVyIHN0dWRlbnQmbmJzcDtcbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5wcmljZVBlclN0dWRlbnRIaWdobGlnaHRNb2JpbGV9PlxuICAgICAgICAgICAgICAgICAgICAmIzgzNzc7NDUvLVxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgJm5ic3A7cGVyIG1vbnRoXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYnV5QnV0dG9ufT4mIzgzNzc7e2RldGFpbC5wcmljZVBlck1vbnRofTwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSxcbiAgICAgICAgKX1cbiAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogJ2ZpdC1jb250ZW50JyB9fSBjbGFzc05hbWU9e3MubW9iaWxlUHJpY2VDYXJkfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3ByaWNlfT5FbnRlcnByaXNlPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfZGVzY30+XG4gICAgICAgICAgICBGb3IgaW5zdGl0dXRlcyB3aXRoIG1vcmUgdGhhbiAxMDAwIHN0dWRlbnRzXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPExpbmsgdG89XCIvcmVxdWVzdC1kZW1vXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5idXlCdXR0b259PkNvbnRhY3QgdXM8L2Rpdj5cbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZGlzcGxheVByaWNpbmdzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNpbmdfY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNpbmdfdGl0bGV9PlxuICAgICAgICB7LyogIDxpbWdcbiAgICAgICAgICBjbGFzc05hbWU9e3MubGluZV92ZWN0b3JfYmd9XG4gICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3ZlY3Rvcl9saW5lLnBuZ1wiXG4gICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgLz5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIGNsYXNzTmFtZT17cy50cmlhbmdsZV9iZ31cbiAgICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvdHJpYW5nbGUtYmFja2dyb3VuZC5zdmdcIlxuICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgIC8+ICAqL31cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYW5udWFsfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hbm51YWxfdGV4dH0gJHt0b2dnbGUgPyAnJyA6IHMudW5kZXJsaW5lfWB9PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmFubnVhbF9vZmZlcn0+KHNhdmUgMTUlKTwvc3Bhbj5cbiAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgPHNwYW4+QW5udWFsIFBsYW5zPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9nZ2xlX2J0bn0+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0b2dnbGVIYW5kbGVyKCl9XG4gICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICB0b2dnbGVcbiAgICAgICAgICAgICAgICA/IGAke3MudG9nZ2xlX291dGVyfSAke3MudG9nZ2xlX29ufSAke3MuZGFya0JhY2tncm91bmR9YFxuICAgICAgICAgICAgICAgIDogcy50b2dnbGVfb3V0ZXJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b2dnbGVfaW5uZXJ9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hbm51YWxfdGV4dH0gJHt0b2dnbGUgPyBzLnVuZGVybGluZSA6ICcnfWB9PlxuICAgICAgICAgIE1vbnRobHkgUGxhbnNcbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYW5zfT5cbiAgICAgICAgeyh0b2dnbGUgPyBwcmljaW5nUGVyTW9udGhEZXRhaWxzIDogcHJpY2luZ1BlckFubnVtRGV0YWlscykubWFwKFxuICAgICAgICAgIChwbGFuLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBrZXk9e3BsYW4ucGFja31cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICBpbmRleCA9PT0gYWN0aXZlSW5kZXggPyBgJHtzLnBsYW59ICR7cy5hY3RpdmV9YCA6IGAke3MucGxhbn1gXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaW5uZXJDYXJkfT5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3MubGVmdEFycm93fVxuICAgICAgICAgICAgICAgICAgb25DbGljaz17Z290b1ByZXZTbGlkZX1cbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPiYjODI0OTs8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnJpZ2h0QXJyb3d9XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXtnb3RvTmV4dFNsaWRlfVxuICAgICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPHNwYW4+JiM4MjUwOzwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zeW1ib2x9PlxuICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BpbWFnZXMvaWNvbnMvJHtwbGFuLmltYWdlfWB9IGFsdD1cIlwiIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhbl90aXRsZX0+XG4gICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnBsYW5fdGl0bGVfcHJpY2V9PntwbGFuLnBhY2t9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnBsYW5fdGl0bGVfc3R1ZGVudHN9PlxuICAgICAgICAgICAgICAgICAgICAgIFVwIHRvIHtwbGFuLm5vT2ZTdHVkZW50c30gc3R1ZGVudHNcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlfT5cbiAgICAgICAgICAgICAgICAgIDxwPiYjODM3Nzt7cGxhbi5wcmljZVBlck1vbnRofTwvcD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICB7IXRvZ2dsZSA/IChcbiAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXY+UGVyIFN0dWRlbnQ8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VQZXJTdHVkZW50SGlnaGxpZ2h0fT5cbiAgICAgICAgICAgICAgICAgICAgICAmIzgzNzc7NDUvLVxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdj5wZXIgbW9udGg8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV9kZXNjfT5cbiAgICAgICAgICAgICAgICAgICAgICA8cD5zd2l0Y2ggdG8gYW5udWFsIHBhY2sgdG8gc2F2ZSAxNSU8L3A+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5wcmljZV9jb250YWN0fWB9PlxuICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvY29udGFjdC11c1wiPkNvbnRhY3QgVXMgZm9yIGEgUXVvdGU8L2E+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT17cy5idXlCdXR0b259PkJ1eTwvYnV0dG9uPlxuXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZG90c30+XG4gICAgICAgICAgICAgICAgICB7cHJpY2luZ1Blck1vbnRoRGV0YWlscy5tYXAoKGRvdCwgY2xpZW50aW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICAgIGtleT17ZG90LnBhY2t9XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaWVudGluZGV4ID09PSBhY3RpdmVJbmRleFxuICAgICAgICAgICAgICAgICAgICAgICAgICA/IGAke3MuZG90fSAke3MuZG90YWN0aXZlfWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiBgJHtzLmRvdH1gXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSxcbiAgICAgICAgKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW50ZXJwcmlzZX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVudGVycHJpc2VIZWFkaW5nfT5FbnRlcnByaXNlPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVudGVycHJpc2Vjb250ZXh0fT5cbiAgICAgICAgICBGb3IgaW5zdGl0dXRlcyB3aXRoIG1vcmUgdGhhbiAxMDAwIHN0dWRlbnRzXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8TGluayB0bz1cIi9yZXF1ZXN0LWRlbW9cIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250YWN0dXN9PkNvbnRhY3QgVXM8L2Rpdj5cbiAgICAgICAgPC9MaW5rPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZGlzcGxheVByaWNpbmdIZWFkZXIgPSAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2luZ0hlYWRlcn0+XG4gICAgICA8aW1nXG4gICAgICAgIGNsYXNzTmFtZT17cy5jaXJjbGVfYmd9XG4gICAgICAgIHNyYz1cImltYWdlcy9pY29ucy9jaXJjbGUtYmFja2dyb3VuZC5zdmdcIlxuICAgICAgICBhbHQ9XCJcIlxuICAgICAgLz5cbiAgICAgIDxpbWdcbiAgICAgICAgY2xhc3NOYW1lPXtzLnNxdWFyZV9iZ31cbiAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3NxdWFyZS1iYWNrZ3JvdW5kLnN2Z1wiXG4gICAgICAgIGFsdD1cIlwiXG4gICAgICAvPlxuICAgICAgPGRpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfdGl0bGV9PkNob29zZSB5b3VyIGVkaXRpb24uPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX3RpdGxlfT5UcnkgaXQgZnJlZSBmb3IgNyBkYXlzLjwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV9jb250ZW50fT5cbiAgICAgICAgRWduaWZ5IHBsYW5zIHN0YXJ0IGFzIGxvdyBhcyBScy40NS8tIHBlciBzdHVkZW50IHBlciBtb250aC5cbiAgICAgIDwvZGl2PlxuICAgICAge2Rpc3BsYXlNb2R1bGVzKCl9XG4gICAgICA8TGluayB0bz1cIi9wcmljaW5nL3ZpZXdEZXRhaWxzXCIgY2xhc3NOYW1lPXtzLnZpZXdMaW5rfT5cbiAgICAgICAgPHA+VmlldyBQbGFuIERldGFpbHM8L3A+XG4gICAgICA8L0xpbms+XG4gICAgICB7aXNNb2JpbGUgPyBkaXNwbGF5TW9iaWxlUHJpY2luZ3MoKSA6IGRpc3BsYXlQcmljaW5ncygpfVxuICAgIDwvZGl2PlxuICApO1xuXG4gIC8vIFByaWNpbmdcbiAgLy8gY29uc3QgZGlzcGxheVByaWNpbmcgPSAoXG4gIC8vICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2luZ19jb250YWluZXJ9PlxuICAvLyAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2luZ190aXRsZX0+XG4gIC8vICAgICAgIDxpbWdcbiAgLy8gICAgICAgICBjbGFzc05hbWU9e3MubGluZV92ZWN0b3JfYmd9XG4gIC8vICAgICAgICAgc3JjPVwiaW1hZ2VzL2ljb25zL3ZlY3Rvcl9saW5lLnBuZ1wiXG4gIC8vICAgICAgICAgYWx0PVwiXCJcbiAgLy8gICAgICAgLz5cbiAgLy8gICAgICAgPGltZ1xuICAvLyAgICAgICAgIGNsYXNzTmFtZT17cy50cmlhbmdsZV9iZ31cbiAgLy8gICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvdHJpYW5nbGUtYmFja2dyb3VuZC5zdmdcIlxuICAvLyAgICAgICAgIGFsdD1cIlwiXG4gIC8vICAgICAgIC8+XG4gIC8vICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFubnVhbH0+XG4gIC8vICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYW5udWFsX3RleHR9ICR7dG9nZ2xlID8gJycgOiBzLnVuZGVybGluZX1gfT5cbiAgLy8gICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5hbm51YWxfb2ZmZXJ9PihzYXZlIDE1JSk8L3NwYW4+XG4gIC8vICAgICAgICAgICA8YnIgLz5cbiAgLy8gICAgICAgICAgIDxzcGFuPkFubnVhbCBQbGFuczwvc3Bhbj5cbiAgLy8gICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgPC9kaXY+XG4gIC8vICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvZ2dsZV9idG59PlxuICAvLyAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAvLyAgICAgICAgICAgb25DbGljaz17KCkgPT4gdG9nZ2xlSGFuZGxlcigpfVxuICAvLyAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgLy8gICAgICAgICAgICAgdG9nZ2xlID8gYCR7cy50b2dnbGVfb3V0ZXJ9ICR7cy50b2dnbGVfb259YCA6IHMudG9nZ2xlX291dGVyXG4gIC8vICAgICAgICAgICB9XG4gIC8vICAgICAgICAgPlxuICAvLyAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9nZ2xlX2lubmVyfSAvPlxuICAvLyAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYW5udWFsX3RleHR9ICR7dG9nZ2xlID8gcy51bmRlcmxpbmUgOiAnJ31gfT5cbiAgLy8gICAgICAgICBNb250aGx5IFBsYW5zXG4gIC8vICAgICAgIDwvZGl2PlxuICAvLyAgICAgPC9kaXY+XG4gIC8vICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGFuc30+XG4gIC8vICAgICAgIHsodG9nZ2xlID8gcHJpY2luZ1Blck1vbnRoRGV0YWlscyA6IHByaWNpbmdQZXJBbm51bURldGFpbHMpLm1hcChcbiAgLy8gICAgICAgICAocGxhbiwgaW5kZXgpID0+IChcbiAgLy8gICAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgICAga2V5PXtwbGFuLnBhY2t9XG4gIC8vICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gIC8vICAgICAgICAgICAgICAgaW5kZXggPT09IGFjdGl2ZUluZGV4ID8gYCR7cy5wbGFufSAke3MuYWN0aXZlfWAgOiBgJHtzLnBsYW59YFxuICAvLyAgICAgICAgICAgICB9XG4gIC8vICAgICAgICAgICA+XG4gIC8vICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmlubmVyQ2FyZH0+XG4gIC8vICAgICAgICAgICAgICAgPGRpdlxuICAvLyAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmxlZnRBcnJvd31cbiAgLy8gICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2dvdG9QcmV2U2xpZGV9XG4gIC8vICAgICAgICAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgLy8gICAgICAgICAgICAgICA+XG4gIC8vICAgICAgICAgICAgICAgICA8c3Bhbj4mIzgyNDk7PC9zcGFuPlxuICAvLyAgICAgICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5yaWdodEFycm93fVxuICAvLyAgICAgICAgICAgICAgICAgb25DbGljaz17Z290b05leHRTbGlkZX1cbiAgLy8gICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAvLyAgICAgICAgICAgICAgID5cbiAgLy8gICAgICAgICAgICAgICAgIDxzcGFuPiYjODI1MDs8L3NwYW4+XG4gIC8vICAgICAgICAgICAgICAgPC9kaXY+XG4gIC8vICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ltYm9sfT5cbiAgLy8gICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaW1hZ2VzL2ljb25zLyR7cGxhbi5pbWFnZX1gfSBhbHQ9XCJcIiAvPlxuICAvLyAgICAgICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYW5fdGl0bGV9PlxuICAvLyAgICAgICAgICAgICAgICAgPHA+XG4gIC8vICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3ByaWNlfT57cGxhbi5wYWNrfTwvc3Bhbj5cbiAgLy8gICAgICAgICAgICAgICAgICAgPGJyIC8+XG4gIC8vICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5wbGFuX3RpdGxlX3N0dWRlbnRzfT5cbiAgLy8gICAgICAgICAgICAgICAgICAgICBVcCB0byB7cGxhbi5ub09mU3R1ZGVudHN9IHN0dWRlbnRzXG4gIC8vICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgLy8gICAgICAgICAgICAgICAgIDwvcD5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZX0+XG4gIC8vICAgICAgICAgICAgICAgICA8cD57cGxhbi5wcmljZVBlck1vbnRofTwvcD5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV9kZXNjfT5cbiAgLy8gICAgICAgICAgICAgICAgIDxwPlxuICAvLyAgICAgICAgICAgICAgICAgICBQZXIge3RvZ2dsZSA/ICdtb250aCcgOiAneWVhcid9LCBiaWxsZWRcbiAgLy8gICAgICAgICAgICAgICAgICAgPGJyIC8+IHt0b2dnbGUgPyAnbW9udGgnIDogJ3llYXInfSBpbiBSdXBlZXNcbiAgLy8gICAgICAgICAgICAgICAgIDwvcD5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5wcmljZV9jb250YWN0fWB9PlxuICAvLyAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIi9jb250YWN0LXVzXCI+Q29udGFjdCBVcyBmb3IgYSBRdW90ZTwvYT5cbiAgLy8gICAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgICAgICA8YnV0dG9uPkJ1eTwvYnV0dG9uPlxuICAvL1xuICAvLyAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvdHN9PlxuICAvLyAgICAgICAgICAgICAgICAge3ByaWNpbmdQZXJNb250aERldGFpbHMubWFwKChkb3QsIGNsaWVudGluZGV4KSA9PiAoXG4gIC8vICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgICAgICAgICAgICBrZXk9e2RvdC5wYWNrfVxuICAvLyAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gIC8vICAgICAgICAgICAgICAgICAgICAgICBjbGllbnRpbmRleCA9PT0gYWN0aXZlSW5kZXhcbiAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgPyBgJHtzLmRvdH0gJHtzLmRvdGFjdGl2ZX1gXG4gIC8vICAgICAgICAgICAgICAgICAgICAgICAgIDogYCR7cy5kb3R9YFxuICAvLyAgICAgICAgICAgICAgICAgICAgIH1cbiAgLy8gICAgICAgICAgICAgICAgICAgLz5cbiAgLy8gICAgICAgICAgICAgICAgICkpfVxuICAvLyAgICAgICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICksXG4gIC8vICAgICAgICl9XG4gIC8vICAgICA8L2Rpdj5cbiAgLy8gICA8L2Rpdj5cbiAgLy8gKTtcblxuICAvLyBjb25zdCBvbkNsaWNrSGFuZGxlciA9IGUgPT4ge1xuICAvLyAgIGNvbnN0IHBhcmVudCA9IGUuY3VycmVudFRhcmdldC5wYXJlbnRFbGVtZW50O1xuICAvL1xuICAvLyAgIGlmIChwYXJlbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGAke3Muc2hvd31gKSkge1xuICAvLyAgICAgcGFyZW50LmNsYXNzTGlzdC5yZW1vdmUoYCR7cy5zaG93fWApO1xuICAvLyAgIH0gZWxzZSB7XG4gIC8vICAgICBwYXJlbnQuY2xhc3NMaXN0LmFkZChgJHtzLnNob3d9YCk7XG4gIC8vICAgfVxuICAvLyB9O1xuXG4gIC8vIGNvbnN0IGV4cGFuZEFwcEhhbmRsZXIgPSAoKSA9PiB7XG4gIC8vICAgc2V0RXhwYW5kQWxsKCFleHBhbmRBbGwpO1xuICAvLyB9O1xuXG4gIC8vIGNvbnN0IHByaWNpbmdGQVEgPSAoXG4gIC8vICAgPGRpdiBjbGFzc05hbWU9e3MuZmFxX2NvbnRhaW5lcn0+XG4gIC8vICAgICA8ZGl2IGNsYXNzTmFtZT17cy5mYXFfdGl0bGV9PlxuICAvLyAgICAgICA8cD5QcmljaW5nIEZBUXM8L3A+XG4gIC8vICAgICA8L2Rpdj5cbiAgLy8gICAgIDxkaXZcbiAgLy8gICAgICAgY2xhc3NOYW1lPXtzLmV4cGFuZF9hbGx9XG4gIC8vICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAvLyAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gIC8vICAgICAgICAgZXhwYW5kQXBwSGFuZGxlcigpO1xuICAvLyAgICAgICB9fVxuICAvLyAgICAgPlxuICAvLyAgICAgICB7ZXhwYW5kQWxsID8gJ01pbmltaXplIEFsbCcgOiAnRXhwYW5kIEFsbCd9XG4gIC8vICAgICA8L2Rpdj5cbiAgLy8gICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjY29yZGlhbn0+XG4gIC8vICAgICAgIHtGQVEubWFwKGl0ZW0gPT4gKFxuICAvLyAgICAgICAgIDxkaXZcbiAgLy8gICAgICAgICAgIGtleT17aXRlbS5sYWJlbH1cbiAgLy8gICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5ib3JkZXJ9ICR7ZXhwYW5kQWxsID8gcy5zaG93IDogJyd9YH1cbiAgLy8gICAgICAgICA+XG4gIC8vICAgICAgICAgICA8ZGl2XG4gIC8vICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAvLyAgICAgICAgICAgICBvbkNsaWNrPXtlID0+IHtcbiAgLy8gICAgICAgICAgICAgICBvbkNsaWNrSGFuZGxlcihlKTtcbiAgLy8gICAgICAgICAgICAgfX1cbiAgLy8gICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnF1ZXN0aW9ufVxuICAvLyAgICAgICAgICAgPlxuICAvLyAgICAgICAgICAgICA8c3Bhbj57aXRlbS5sYWJlbH08L3NwYW4+XG4gIC8vICAgICAgICAgICAgIDxpbWdcbiAgLy8gICAgICAgICAgICAgICBjbGFzc05hbWU9e3MuZG93bn1cbiAgLy8gICAgICAgICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvY2hldnJvbi1kb3duLnN2Z1wiXG4gIC8vICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgLy8gICAgICAgICAgICAgLz5cbiAgLy8gICAgICAgICAgICAgPGltZ1xuICAvLyAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy51cH1cbiAgLy8gICAgICAgICAgICAgICBzcmM9XCJpbWFnZXMvaWNvbnMvY2hldnJvbi1kb3duLnN2Z1wiXG4gIC8vICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgLy8gICAgICAgICAgICAgLz5cbiAgLy8gICAgICAgICAgIDwvZGl2PlxuICAvLyAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYW5zd2VyfT5cbiAgLy8gICAgICAgICAgICAgPHA+e2l0ZW0uY29udGVudH08L3A+XG4gIC8vICAgICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgICA8L2Rpdj5cbiAgLy8gICAgICAgKSl9XG4gIC8vICAgICA8L2Rpdj5cbiAgLy8gICA8L2Rpdj5cbiAgLy8gKTtcblxuICAvLyBjb25zdCBkaXNwbGF5Vmlld0xpbmsgPSAoKSA9PiAoXG4gIC8vICA8c3BhbiBjbGFzc05hbWU9e3Mudmlld30+XG4gIC8vICA8TGluayB0bz1cIi9wcmljaW5nL3ZpZXdEZXRhaWxzXCIgY2xhc3NOYW1lPXtzLnZpZXdMaW5rfT5WaWV3IFBsYW4gRGV0YWlsczwvTGluaz5cbiAgLy8gPC9zcGFuPlxuICAvLyApXG5cbiAgY29uc3QgZGlzcGxheVRydXN0ZWRCeSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5kaXNwbGF5Q2xpZW50c30+XG4gICAgICA8aDM+XG4gICAgICAgIFRydXN0ZWQgYnkgPHNwYW4gLz5cbiAgICAgICAgbGVhZGluZyBFZHVjYXRpb25hbCBJbnN0aXR1dGlvbnNcbiAgICAgIDwvaDM+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jbGllbnRzV3JhcHBlcn0+XG4gICAgICAgIHtIT01FX0NMSUVOVFNfVVBEQVRFRC5tYXAoY2xpZW50ID0+IChcbiAgICAgICAgICA8aW1nXG4gICAgICAgICAgICBjbGFzc05hbWU9e3MuY2xpZW50fVxuICAgICAgICAgICAga2V5PXtjbGllbnQuaWR9XG4gICAgICAgICAgICBzcmM9e2NsaWVudC5pY29ufVxuICAgICAgICAgICAgYWx0PVwiY2xpbmV0XCJcbiAgICAgICAgICAvPlxuICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPHNwYW4+XG4gICAgICAgIDxMaW5rIHRvPVwiL2N1c3RvbWVyc1wiPmNsaWNrIGhlcmUgZm9yIG1vcmUuLi48L0xpbms+XG4gICAgICA8L3NwYW4+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZGlzcGxheUFjaGlldmVkID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkSGVhZGluZ30+XG4gICAgICAgIFdoYXQgd2UgaGF2ZSBhY2hpZXZlZCA8c3BhbiAvPlxuICAgICAgICBzbyBmYXJcbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRSb3d9PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMjU1LCAxMDIsIDAsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5jbGllbnRzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9jbGllbnRzLnN2Z1wiIGFsdD1cInByb2ZpbGVcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5jbGllbnRzSGlnaGxpZ2h0fWB9PjE1MCs8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+Q2xpZW50czwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnN0dWRlbnRzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9zdHVkZW50cy5zdmdcIiBhbHQ9XCJzdHVkZW50c1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnN0dWRlbnRzSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMyBMYWtoK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5TdHVkZW50czwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuY2FyZH1gfVxuICAgICAgICAgIHN0eWxlPXt7IGJveFNoYWRvdzogJzAgNHB4IDMycHggMCByZ2JhKDAsIDExNSwgMjU1LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MudGVzdHN9YH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS93aGF0LWFjaGlldmVkL3Rlc3RzLnN2Z1wiIGFsdD1cInRlc3RzXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3MuaGlnaGxpZ2h0fSAke3MudGVzdHNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAzIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuPlRlc3RzIEF0dGVtcHRlZDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgwLCAxNzIsIDM4LCAwLjIpJyB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWNoaWV2ZWRQcm9maWxlfSAke3MucXVlc3Rpb25zfWB9PlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9xdWVzdGlvbnMuc3ZnXCJcbiAgICAgICAgICAgICAgYWx0PVwicXVlc3Rpb25zXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnF1ZXN0aW9uc0hpZ2hsaWdodH1gfT5cbiAgICAgICAgICAgIDIwIE1pbGxpb24rXG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zdWJUZXh0fT5RdWVzdGlvbnMgU29sdmVkPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGNvbnN0IG9wZW5Nb2RhbCA9IGluZGV4ID0+IHtcbiAgICBzZXRTaG93TW9kYWwodHJ1ZSk7XG4gICAgc2V0QWN0aXZlUHJlc3MoUFJFU1NfVklERU9fQ09WRVJBR0VbaW5kZXhdKTtcbiAgfTtcblxuICBjb25zdCB0b2dnbGVNb2RhbCA9ICgpID0+IHtcbiAgICBzZXRTaG93TW9kYWwoZmFsc2UpO1xuICB9O1xuXG4gIGNvbnN0IGRpc3BsYXlGZWF0dXJlZEluID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmZlYXR1cmVkfT5cbiAgICAgIDxoMj5XZSZhcG9zO3ZlIGdvdCBmZWF0dXJlZCBpbjwvaDI+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5vbl9uZXdzfT5cbiAgICAgICAge1BSRVNTX1ZJREVPX0NPVkVSQUdFLm1hcCgoaXRlbSwgaW5kZXgpID0+IChcbiAgICAgICAgICA8ZGl2IHJvbGU9XCJwcmVzZW50YXRpb25cIiBvbkNsaWNrPXsoKSA9PiBvcGVuTW9kYWwoaW5kZXgpfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPXtpdGVtLmljb259IGNsYXNzTmFtZT17cy5mZWF0dXJlZEltYWdlfSBhbHQ9e2l0ZW0udGl0bGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zaG93TWVkaWF9PlxuICAgICAgICB7SE9NRV9NRURJQS5tYXAobWVkaWEgPT4gKFxuICAgICAgICAgIDxhIGNsYXNzTmFtZT17cy5hY3RpdmV9IGhyZWY9e21lZGlhLnVybH0+XG4gICAgICAgICAgICA8aW1nIHNyYz17bWVkaWEuaWNvbn0gYWx0PXttZWRpYS5uYW1lfSAvPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBjb25zdCBkaXNwbGF5QXdhcmRzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF3YXJkc0NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5hd2FyZHNUaXRsZX0+QXdhcmRzPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFJvd31gfT5cbiAgICAgICAge0hPTUVfQVdBUkRTLm1hcChpdGVtID0+IChcbiAgICAgICAgICA8aW1nIHNyYz17aXRlbS5pY29uMX0gYWx0PXtpdGVtLmNvbnRlbnR9IGNsYXNzTmFtZT17cy5hd2FyZExvZ299IC8+XG4gICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgY29uc3QgZGlzcGxheU1vZGFsID0gKCkgPT4gKFxuICAgIDxNb2RhbFxuICAgICAgbW9kYWxDbGFzc05hbWU9e3MucHJlc3Nwb3B1cH1cbiAgICAgIG92ZXJsYXlDbGFzc05hbWU9e3MucG9wdXBvdmVybGF5fVxuICAgICAgdG9nZ2xlTW9kYWw9e3RvZ2dsZU1vZGFsfVxuICAgID5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRpdGxlYm94fT5cbiAgICAgICAgPGgyPnthY3RpdmVQcmVzcy50aXRsZX08L2gyPlxuICAgICAgICA8aW1nXG4gICAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9jbG9zZS5zdmdcIlxuICAgICAgICAgIG9uQ2xpY2s9e3RvZ2dsZU1vZGFsfVxuICAgICAgICAgIGFsdD1cImNsb3NlXCJcbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuXG4gICAgICA8UmVhY3RQbGF5ZXJcbiAgICAgICAgcGxheWluZz17ZmFsc2V9XG4gICAgICAgIHVybD17YWN0aXZlUHJlc3MudXJsfVxuICAgICAgICBjbGFzc05hbWU9e3MucGxheWVyfVxuICAgICAgICB3aWR0aD1cIjEwMCVcIlxuICAgICAgICBoZWlnaHQ9XCI4MCVcIlxuICAgICAgICBjb250cm9sc1xuICAgICAgICBsb29wXG4gICAgICAgIHBsYXlzaW5saW5lXG4gICAgICAgIHBpcD17ZmFsc2V9XG4gICAgICAgIGNvbmZpZz17e1xuICAgICAgICAgIHlvdXR1YmU6IHtcbiAgICAgICAgICAgIHBsYXllclZhcnM6IHsgc2hvd2luZm86IDEgfSxcbiAgICAgICAgICB9LFxuICAgICAgICB9fVxuICAgICAgLz5cbiAgICA8L01vZGFsPlxuICApO1xuXG4gIHJldHVybiAoXG4gICAgPGRpdj5cbiAgICAgIDxkaXY+e2Rpc3BsYXlQcmljaW5nSGVhZGVyfTwvZGl2PlxuICAgICAge2Rpc3BsYXlUcnVzdGVkQnkoKX1cbiAgICAgIHtkaXNwbGF5QWNoaWV2ZWQoKX1cbiAgICAgIHtkaXNwbGF5RmVhdHVyZWRJbigpfVxuICAgICAge2Rpc3BsYXlBd2FyZHMoKX1cbiAgICAgIHtkaXNwbGF5U2Nyb2xsVG9Ub3AoKX1cbiAgICAgIHtzaG93TW9kYWwgPyBkaXNwbGF5TW9kYWwoKSA6IG51bGx9XG4gICAgPC9kaXY+XG4gICk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoUHJpY2luZyk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaWNpbmcuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiZXhwb3J0IGNvbnN0IEZBUSA9IFtcbiAge1xuICAgIGxhYmVsOiAnV2hhdCBpcyBHZXRSYW5rcyBMZWFybmluZyBQbGF0Zm9ybT8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnV2hhdCBkbyBJIGdldCB3aXRoIGEgUmlzZSBzdWJzY3JpcHRpb24/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0hvdyBtdWNoIGRvZXMgYSBzdWJzY3JpcHRpb24gY29zdD8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnRG8geW91IG9mZmVyIG1vbnRobHkgc3Vic2NyaXB0aW9ucz8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnSXMgdGhlcmUgYSBmcmVlIHRyaWFsIHBlcmlvZD8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnRG8geW91IG9mZmVyIGRpc2NvdW50cz8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnSG93IGNhbiBJIGdldCBhbiBldmVuIGJpZ2dlciBkaXNjb3VudD8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnQ2FuIEkgYnV5IGEgc3Vic2NyaXB0aW9uIG91dHNpZGUgdGhlIEluZGlhPycsXG4gICAgY29udGVudDpcbiAgICAgICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4nLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdDYW4gSSBidXkgYSBzdWJzY3JpcHRpb24gb24gYmVoYWxmIG9mIHNvbWVvbmUgZWxzZT8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnSG93IGRvIEkgcHVyY2hhc2UgYSB0YXgtZXhlbXB0IHN1YnNjcmlwdGlvbj8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnQ2FuIEkgY2FuY2VsIG15IHN1YnNjcmlwdGlvbiBhdCBhbnkgdGltZT8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnV2hhdCBpcyB5b3VyIHJlZnVuZCBwb2xpY3k/JyxcbiAgICBjb250ZW50OlxuICAgICAgJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIFRlbGx1cyB2ZWwgcXVhbSBzaXQgdHVycGlzIGZhbWVzIG5pYmggdG9ydG9yIGN1cnN1cy4gU2VkIG1hc3NhIHZ1bHB1dGF0ZSBmYXVjaWJ1cyBpZCBlZ2V0IHBlbGxlbnRlc3F1ZS4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLicsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ1doYXTigJlzIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gdXNlciByb2xlcz8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnV2hhdCB1c2VyIHJvbGVzIGNvdW50IHRvd2FyZHMgbXkgcGxhbuKAmXMgdXNlciBsaW1pdD8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnQ2FuIEkgdXBncmFkZSBteSBwbGFuIGFzIG5lZWRlZD8nLFxuICAgIGNvbnRlbnQ6XG4gICAgICAnTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gVGVsbHVzIHZlbCBxdWFtIHNpdCB0dXJwaXMgZmFtZXMgbmliaCB0b3J0b3IgY3Vyc3VzLiBTZWQgbWFzc2EgdnVscHV0YXRlIGZhdWNpYnVzIGlkIGVnZXQgcGVsbGVudGVzcXVlLiBMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LiBUZWxsdXMgdmVsIHF1YW0gc2l0IHR1cnBpcyBmYW1lcyBuaWJoIHRvcnRvciBjdXJzdXMuIFNlZCBtYXNzYSB2dWxwdXRhdGUgZmF1Y2lidXMgaWQgZWdldCBwZWxsZW50ZXNxdWUuJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBwcmljaW5nUGVyQW5udW1EZXRhaWxzID0gW1xuICB7XG4gICAgaW1hZ2U6ICd0cmlhbmdsZS5zdmcnLFxuICAgIHBhY2s6ICdTdGFydGVyJyxcbiAgICBub09mU3R1ZGVudHM6IDEwMCxcbiAgICBwcmljZVBlck1vbnRoOiAnNTAsMDAwLy0nLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICdyaG9tYnVzLnN2ZycsXG4gICAgcGFjazogJ0dyb3d0aCcsXG4gICAgbm9PZlN0dWRlbnRzOiAyNTAsXG4gICAgcHJpY2VQZXJNb250aDogJzIsMDAsMDAwLy0nLFxuICB9LFxuICB7XG4gICAgaW1hZ2U6ICdwZW50YWdvbi5zdmcnLFxuICAgIHBhY2s6ICdNaWRzaXplJyxcbiAgICBub09mU3R1ZGVudHM6IDUwMCxcbiAgICBwcmljZVBlck1vbnRoOiAnMywwMCwwMDAvLScsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJ2hleGFnb24uc3ZnJyxcbiAgICBwYWNrOiAnTGFyZ2UnLFxuICAgIG5vT2ZTdHVkZW50czogMTAwMCxcbiAgICBwcmljZVBlck1vbnRoOiAnNCwwMCwwMDAvLScsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgcHJpY2luZ1Blck1vbnRoRGV0YWlscyA9IFtcbiAge1xuICAgIGltYWdlOiAndHJpYW5nbGUuc3ZnJyxcbiAgICBwYWNrOiAnU3RhcnRlcicsXG4gICAgbm9PZlN0dWRlbnRzOiAxMDAsXG4gICAgcHJpY2VQZXJNb250aDogJzIwLDAwMC8tJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAncmhvbWJ1cy5zdmcnLFxuICAgIHBhY2s6ICdHcm93dGgnLFxuICAgIG5vT2ZTdHVkZW50czogMjUwLFxuICAgIHByaWNlUGVyTW9udGg6ICc1MCwwMDAvLScsXG4gIH0sXG4gIHtcbiAgICBpbWFnZTogJ3BlbnRhZ29uLnN2ZycsXG4gICAgcGFjazogJ01pZHNpemUnLFxuICAgIG5vT2ZTdHVkZW50czogNTAwLFxuICAgIHByaWNlUGVyTW9udGg6ICcxLDAwLDAwMC8tJyxcbiAgfSxcbiAge1xuICAgIGltYWdlOiAnaGV4YWdvbi5zdmcnLFxuICAgIHBhY2s6ICdMYXJnZScsXG4gICAgbm9PZlN0dWRlbnRzOiAxMDAwLFxuICAgIHByaWNlUGVyTW9udGg6ICcyLDAwLDAwMC8tJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBNb2R1bGVzTGlzdCA9IFtcbiAge1xuICAgIGxhYmVsOiAnTGl2ZSBDbGFzc2VzJyxcbiAgICBwYXRoOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9MaXZlLnN2ZycsXG4gICAgdmFsdWU6ICdsaXZlY2xhc3NlcycsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ09ubGluZSBUZXN0cycsXG4gICAgcGF0aDogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9UZXN0L21vZHVsZV90ZXN0LnN2ZycsXG4gICAgdmFsdWU6ICd0ZXN0cycsXG4gIH0sXG4gIHtcbiAgICBsYWJlbDogJ0Fzc2lnbm1lbnRzJyxcbiAgICBwYXRoOiAnL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1RlYWNoL29sZF9Bc3NpZ25tZW50cy5zdmcnLFxuICAgIHZhbHVlOiAnYXNzaWdubWVudHMnLFxuICB9LFxuICB7XG4gICAgbGFiZWw6ICdEb3VidHMnLFxuICAgIHBhdGg6ICcvaW1hZ2VzL2hvbWUvTmV3IFN1Yk1lbnUgSXRlbXMvVGVhY2gvb2xkX0RvdWJ0cy5zdmcnLFxuICAgIHZhbHVlOiAnZG91YnRzJyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiAnQ29ubmVjdCcsXG4gICAgcGF0aDogJy9pbWFnZXMvaG9tZS9OZXcgU3ViTWVudSBJdGVtcy9Db25uZWN0L25ld19jb25uZWN0LnN2ZycsXG4gICAgdmFsdWU6ICdjb25uZWN0JyxcbiAgfSxcbl07XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExheW91dCBmcm9tICdjb21wb25lbnRzL0xheW91dC9MYXlvdXQnO1xuaW1wb3J0IFByaWNpbmcgZnJvbSAnLi9QcmljaW5nJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiBbJ0dldFJhbmtzIGJ5IEVnbmlmeTogQXNzZXNzbWVudCAmIEFuYWx5dGljcyBQbGF0Zm9ybSddLFxuICAgIGNodW5rczogWydQcmljaW5nJ10sXG4gICAgY29tcG9uZW50OiAoXG4gICAgICA8TGF5b3V0IGZvb3RlckFzaD5cbiAgICAgICAgPFByaWNpbmcgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQXdCQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTs7OztBQTVEQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFKQTtBQUNBO0FBNERBOzs7Ozs7O0FDcEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBWUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUtBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVRBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBVUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbEJBO0FBQ0E7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBcEJBO0FBd0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF0REE7QUFDQTtBQTREQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQTFEQTtBQTBFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBdkhBO0FBQ0E7QUE0SEE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFqQkE7QUFDQTtBQXFCQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBdERBO0FBQ0E7QUEyREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQVhBO0FBQ0E7QUFtQkE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFKQTtBQUNBO0FBVUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFEQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWhCQTtBQUNBO0FBa0NBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBOzs7Ozs7O0FDaHFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7O0FDN0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFZQTs7OztBIiwic291cmNlUm9vdCI6IiJ9