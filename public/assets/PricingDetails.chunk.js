(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PricingDetails"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".PricingViewDetails-pricingHeader-2bYv- {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.PricingViewDetails-price_title-2nqFt {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.PricingViewDetails-price_content-1fVtK {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.PricingViewDetails-whoAreYou-3T0mg {\n  margin: 72px 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.PricingViewDetails-adminContainer-29ElQ {\n  cursor: pointer;\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 20px 20px;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.PricingViewDetails-tabsContainer-3_Zwu {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin: 24px 0 72px 0;\n}\n\n.PricingViewDetails-adminLabelContainer-2Mroj {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.PricingViewDetails-adminLabel-23wjb {\n  // width: 119px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 19px 38px 8px 8px;\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-imgContainer-1Jj_u {\n  width: 40px;\n  height: 40px;\n  margin: 10px 8px 0 0;\n  padding: 10px;\n  background-color: rgba(196, 196, 196, 0.3);\n  border-radius: 50px;\n}\n\n.PricingViewDetails-adminImg-2mAsS {\n  width: 20px;\n  height: 20px;\n  color: #25282b;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-plane-21eaT {\n  margin: 0 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.PricingViewDetails-moduleContainer-I6SSy {\n  width: 30%;\n  cursor: pointer;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 30px;\n  border-right: 1px solid rgba(0, 0, 0, 0.2);\n  background-color: #fff;\n}\n\n.PricingViewDetails-moduleContainer-I6SSy:last-child {\n  border-right-width: 0;\n}\n\n.PricingViewDetails-moduleContainerActive-2BKQt {\n  background-color: #f36;\n}\n\n.PricingViewDetails-iconActive-1MVuw {\n  background-color: #f36;\n}\n\n.PricingViewDetails-headerContainer-3Lmkq {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  // height: 48px;\n  width: 100%;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n}\n\n.PricingViewDetails-icon-UXaHR {\n  width: 24px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-moduleName-3StFu {\n  margin: 0 0 0 8px;\n  font-size: 14px;\n  letter-spacing: -0.28px;\n  color: #25282b;\n}\n\n.PricingViewDetails-moduleNameActive-3Lzqz {\n  color: #fff;\n  font-weight: 600;\n}\n\n.PricingViewDetails-adminContainerActive-1Z0JL {\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 0 20px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n          box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n  border: solid 1px #ff99b3;\n  text-align: center;\n}\n\n.PricingViewDetails-rightIcon-2i32Z {\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  width: 16px;\n  height: 16px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.PricingViewDetails-active-2cVke {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.PricingViewDetails-subModules-1R5-x {\n  cursor: pointer;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 0 24px 0;\n  font-size: 16px;\n  line-height: 1.5;\n  color: #25282b;\n}\n\n.PricingViewDetails-subModuleActive-1hEpJ {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #f36;\n}\n\n.PricingViewDetails-iconDataContainer-i2G6T {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  // margin-left: 1px solid #d9d9d9;\n}\n\n.PricingViewDetails-iconDataContainer1-ur6rZ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  margin-left: 10px;\n  // margin-bottom: 8px;\n}\n\n.PricingViewDetails-dataContainer-3Q7OM {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 14px;\n  line-height: 1.57;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-icons-2XSLR {\n  width: 18px;\n  height: 18px;\n  margin: 6px 12px 0 0;\n}\n\n.PricingViewDetails-iconContainer-1QcNN {\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 12px;\n  // border-right: 1px solid #d9d9d9;\n}\n\n/* .dataIconContainer {\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  column-gap: 3rem;\n  row-gap: 3rem;\n}\n */\n\n/* .verticalLine {\n  height: 420px;\n  width: 1px;\n  margin: 63px 10px 0 0;\n  background-color: #d9d9d9;\n} */\n\n.PricingViewDetails-studentModule-1lFxy {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 16px;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-sectionName-1WSX5 {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin-bottom: 24px;\n  font-size: 24px;\n  font-weight: 600;\n  line-height: 1.33;\n  text-align: left;\n  color: #25282b;\n}\n\n.PricingViewDetails-subModulesFlex-3p7bL {\n  display: -ms-flexbox;\n  display: flex;\n  margin: 32px 0 32px 0;\n}\n\n/* .subsubmodules {\n  display: flex;\n}\n */\n\n.PricingViewDetails-moduleFlex-2NouI {\n  padding: 0 64px 0 64px;\n  width: 87%;\n  margin: auto;\n}\n\n/* .moduleFlex1 {\n  padding: 0 64px 0 64px;\n  width: fit-content;\n  margin: auto;\n}\n */\n\n.PricingViewDetails-modules-1dSYh {\n  padding: 0 64px 0 64px;\n  width: 100%;\n  margin: auto;\n  overflow: hidden;\n}\n\n.PricingViewDetails-arrowIcon-1I8M6 {\n  width: 10.9px;\n  height: 16px;\n}\n\n.PricingViewDetails-subModuleContainer-2F_iK {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n}\n\n.PricingViewDetails-submodulesSectionContainer-2D5Jx {\n  -ms-flex: 0.4 1;\n      flex: 0.4 1;\n  // width: 30%;\n}\n\n.PricingViewDetails-submodulesSectionContainer1-3Hz0T {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n  // width: 25%;\n  margin: 50px 0 0 0;\n  // border-left: 1px solid #d9d9d9;\n}\n\n.PricingViewDetails-imageContainer-3CA5q {\n  width: 9%;\n}\n\n/* @media screen and (min-width: 1280px) {\n  .moduleFlex{\n    width: 53.5%;\n  }\n} */\n\n@media only screen and (min-width: 991px) {\n  .PricingViewDetails-plus-25OpF {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleIcon-_NUTR {\n    display: none;\n  }\n\n  .PricingViewDetails-subModuleMobileContainer-3Xkg3 {\n    display: none;\n  }\n\n  .PricingViewDetails-plusContainer-39Pqg {\n    display: none;\n  }\n\n  /* .headerContainerMobileView {\n    display: none;\n  } */\n\n  .PricingViewDetails-headerContainers-3kGGo {\n    display: none;\n  }\n\n  .PricingViewDetails-goBackLink-mPLEA {\n    display: none;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .PricingViewDetails-modules-1dSYh {\n    padding: 0;\n  }\n\n  .PricingViewDetails-pricingHeader-2bYv- {\n    margin: 24px 0 0 0;\n    padding: 0 64px 0 64px;\n  }\n\n  .PricingViewDetails-price_title-2nqFt {\n    font-size: 32px;\n  }\n\n  .PricingViewDetails-price_content-1fVtK {\n    font-size: 14px;\n    width: 100%;\n    line-height: 1.71;\n    margin: 12px 0 48px 0;\n  }\n\n  .PricingViewDetails-whoAreYou-3T0mg {\n    font-size: 20px;\n    margin: 48px 0 24px 0;\n  }\n\n  .PricingViewDetails-goBackLink-mPLEA {\n    text-align: center;\n    font-size: 14px;\n    color: #f36;\n    text-decoration: underline;\n  }\n\n  .PricingViewDetails-adminContainerActive-1Z0JL {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    background-color: #e8e8e8;\n    border-style: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    padding: 0;\n    border-bottom-right-radius: 8px;\n    // padding: 0 49px 24px 50px;\n  }\n\n  /* .adminContainerActive:second-child {\n    border-bottom-left-radius: 8px;\n  } */\n\n  .PricingViewDetails-adminContainer-29ElQ {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    border-style: none;\n    background-color: transparent;\n    margin: 0;\n    padding: 0;\n  }\n\n  .PricingViewDetails-rightIcon-2i32Z {\n    display: none;\n  }\n\n  .PricingViewDetails-imgContainer-1Jj_u {\n    width: 10%;\n    height: 0;\n    background-color: transparent;\n    // padding: 24px 49px 24px 50px;\n    // margin-left: 40px;\n    // padding: 0;\n    // margin: 19px 0 0 0;\n  }\n\n  .PricingViewDetails-adminLabel-23wjb {\n    // width:0px;\n    font-size: 14px;\n    text-align: center;\n    margin: 19px 0 0 0;\n  }\n\n  .PricingViewDetails-moduleData-EDv-k {\n    background-color: #f7f7f7;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .PricingViewDetails-tabsContainer-3_Zwu {\n    // justify-content: flex-start;\n    margin: 0 0 20px 0;\n    width: 100%;\n    padding: 0;\n  }\n\n  .PricingViewDetails-plane-21eaT {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleFlex-2NouI {\n    padding: 0;\n  }\n\n  /* .headerContainer {\n    display: none;\n  } */\n\n  /*  .mobileMOduleContainer {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  }\n\n  .mobileMOduleContainerActive {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    border-radius: 24px;\n    width: 328px;\n    height: 44px;\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  } */\n\n  /*  .plus {\n    width: 20px;\n    height: 20px;\n  } */\n\n  /* .imageMobileUrl {\n    width: 10%;\n    height: 20px;\n  } */\n\n  /* .mobileModuleName {\n    width: 75%;\n    font-size: 14px;\n    color: #25282b;\n    text-align: left;\n  }\n\n  .mobileModuleNameActive {\n    width: 75%;\n    font-size: 14px;\n    color: #f36;\n    text-align: left;\n  } */\n\n  .PricingViewDetails-headerContainers-3kGGo {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    // justify-content: center;\n    border: none;\n    // margin: 100px 0 0 0;\n  }\n\n  .PricingViewDetails-active-2cVke {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .PricingViewDetails-adminLabelContainer-2Mroj {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .PricingViewDetails-moduleContainer-I6SSy {\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n  }\n\n  .PricingViewDetails-moduleContainerActive-2BKQt {\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    border-radius: 24px;\n  }\n\n  /* .icon {\n    width: 10%;\n    height: 20px;\n  } */\n\n  .PricingViewDetails-adminImg-2mAsS {\n    width: 16px;\n    height: 16px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .PricingViewDetails-moduleName-3StFu {\n    width: 90%;\n    font-size: 14px;\n    color: #25282b;\n  }\n\n  .PricingViewDetails-moduleNameActive-3Lzqz {\n    color: #f36;\n    width: 90%;\n  }\n\n  .PricingViewDetails-plus-25OpF {\n    width: 20px;\n    height: 20px;\n    display: -ms-flexbox;\n    display: flex;\n    justify-items: flex-end;\n    -ms-flex-align: end;\n        align-items: flex-end;\n  }\n\n  .PricingViewDetails-subModuleContainer-2F_iK {\n    display: none;\n  }\n\n  .PricingViewDetails-dataContainer-3Q7OM {\n    display: none;\n  }\n\n  .PricingViewDetails-icons-2XSLR {\n    display: none;\n  }\n\n  .PricingViewDetails-submodulesSectionContainer1-3Hz0T {\n    display: none;\n  }\n\n  .PricingViewDetails-sectionName-1WSX5 {\n    display: none;\n  }\n\n  /* .subModuleMobileContainer {\n    margin: 0 0 0 16px;\n  } */\n\n  .PricingViewDetails-subModuleName-3x1To {\n    font-size: 16px;\n    line-height: 1.5;\n    font-weight: 600;\n    color: #25282b;\n    margin: 16px 0 12px 0;\n  }\n\n  .PricingViewDetails-submoduleDataContainer-22IY- {\n    display: -ms-flexbox;\n    display: flex;\n    margin: 0 0 8px 0;\n  }\n\n  .PricingViewDetails-submoduleData-1NPL2 {\n    font-size: 12px;\n    color: #25282b;\n    text-align: left;\n    line-height: 1.67;\n    margin-left: 8px;\n  }\n\n  .PricingViewDetails-rightIcons-81roB {\n    width: 20px;\n    height: 20px;\n  }\n\n  .PricingViewDetails-iconActive-1MVuw {\n    display: none;\n  }\n\n  .PricingViewDetails-subModuleMobileContainer-3Xkg3 {\n    // height: 20%;\n    overflow-y: scroll;\n    margin: 0 0 0 16px;\n  }\n\n  .PricingViewDetails-icon-UXaHR {\n    display: none;\n  }\n\n  .PricingViewDetails-moduleIcon-_NUTR {\n    width: 20px;\n    height: 20px;\n  }\n\n  .PricingViewDetails-headerContainer-3Lmkq {\n    display: none;\n  }\n\n  .PricingViewDetails-submodulesSectionContainer-2D5Jx {\n    display: none;\n  }\n\n  .PricingViewDetails-subModulesFlex-3p7bL {\n    display: none;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,mBAAmB;CACpB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,sBAAsB;EACtB,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,mBAAmB;EACnB,aAAa;EACb,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,6BAA6B;EAC7B,qCAAqC;EACrC,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;CACpB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,0BAA0B;EAC1B,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;EACrB,cAAc;EACd,2CAA2C;EAC3C,oBAAoB;CACrB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,eAAe;EACf,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,WAAW;EACX,gBAAgB;EAChB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,2CAA2C;EAC3C,uBAAuB;CACxB;;AAED;EACE,sBAAsB;CACvB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,uBAAuB;CACxB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,gBAAgB;EAChB,YAAY;EACZ,qCAAqC;EACrC,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,kBAAkB;EAClB,gBAAgB;EAChB,wBAAwB;EACxB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;EACnB,wDAAwD;UAChD,gDAAgD;EACxD,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,WAAW;EACX,YAAY;EACZ,aAAa;EACb,qBAAqB;MACjB,aAAa;EACjB,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,gBAAgB;EAChB,qBAAqB;MACjB,aAAa;EACjB,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,qBAAqB;EACzB,kCAAkC;CACnC;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,mBAAmB;MACf,qBAAqB;EACzB,kBAAkB;EAClB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;MACjB,aAAa;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,qBAAqB;CACtB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;EACpB,mCAAmC;CACpC;;AAED;;;;;;GAMG;;AAEH;;;;;IAKI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,aAAa;EACjB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,qBAAqB;MACjB,aAAa;EACjB,oBAAoB;EACpB,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,sBAAsB;CACvB;;AAED;;;GAGG;;AAEH;EACE,uBAAuB;EACvB,WAAW;EACX,aAAa;CACd;;AAED;;;;;GAKG;;AAEH;EACE,uBAAuB;EACvB,YAAY;EACZ,aAAa;EACb,iBAAiB;CAClB;;AAED;EACE,cAAc;EACd,aAAa;CACd;;AAED;EACE,gBAAgB;MACZ,YAAY;CACjB;;AAED;EACE,gBAAgB;MACZ,YAAY;EAChB,cAAc;CACf;;AAED;EACE,gBAAgB;MACZ,YAAY;EAChB,cAAc;EACd,mBAAmB;EACnB,kCAAkC;CACnC;;AAED;EACE,UAAU;CACX;;AAED;;;;IAII;;AAEJ;EACE;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;;MAEI;;EAEJ;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;CACF;;AAED;EACE;IACE,WAAW;GACZ;;EAED;IACE,mBAAmB;IACnB,uBAAuB;GACxB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,YAAY;IACZ,kBAAkB;IAClB,sBAAsB;GACvB;;EAED;IACE,gBAAgB;IAChB,sBAAsB;GACvB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,YAAY;IACZ,2BAA2B;GAC5B;;EAED;IACE,gBAAgB;IAChB,WAAW;IACX,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,0BAA0B;IAC1B,mBAAmB;IACnB,yBAAyB;YACjB,iBAAiB;IACzB,UAAU;IACV,WAAW;IACX,gCAAgC;IAChC,6BAA6B;GAC9B;;EAED;;MAEI;;EAEJ;IACE,gBAAgB;IAChB,WAAW;IACX,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;IAClC,mBAAmB;IACnB,8BAA8B;IAC9B,UAAU;IACV,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,WAAW;IACX,UAAU;IACV,8BAA8B;IAC9B,gCAAgC;IAChC,qBAAqB;IACrB,cAAc;IACd,sBAAsB;GACvB;;EAED;IACE,aAAa;IACb,gBAAgB;IAChB,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,0BAA0B;IAC1B,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,+BAA+B;IAC/B,mBAAmB;IACnB,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,cAAc;GACf;;EAED;IACE,WAAW;GACZ;;EAED;;MAEI;;EAEJ;;;;;;;;;;;;;;;;;;;;;;;;;;;MA2BI;;EAEJ;;;MAGI;;EAEJ;;;MAGI;;EAEJ;;;;;;;;;;;;MAYI;;EAEJ;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,2BAA2B;IAC3B,aAAa;IACb,uBAAuB;GACxB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;GACnC;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,0BAA0B;QACtB,8BAA8B;GACnC;;EAED;IACE,qBAAqB;QACjB,4BAA4B;IAChC,aAAa;IACb,aAAa;IACb,oBAAoB;IACpB,qCAAqC;IACrC,uBAAuB;IACvB,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,0BAA0B;IAC1B,0BAA0B;IAC1B,oBAAoB;GACrB;;EAED;;;MAGI;;EAEJ;IACE,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,sBAAsB;QAClB,wBAAwB;IAC5B,uBAAuB;QACnB,oBAAoB;GACzB;;EAED;IACE,WAAW;IACX,gBAAgB;IAChB,eAAe;GAChB;;EAED;IACE,YAAY;IACZ,WAAW;GACZ;;EAED;IACE,YAAY;IACZ,aAAa;IACb,qBAAqB;IACrB,cAAc;IACd,wBAAwB;IACxB,oBAAoB;QAChB,sBAAsB;GAC3B;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;;MAEI;;EAEJ;IACE,gBAAgB;IAChB,iBAAiB;IACjB,iBAAiB;IACjB,eAAe;IACf,sBAAsB;GACvB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,eAAe;IACf,iBAAiB;IACjB,kBAAkB;IAClB,iBAAiB;GAClB;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;GACf;;EAED;IACE,eAAe;IACf,mBAAmB;IACnB,mBAAmB;GACpB;;EAED;IACE,cAAc;GACf;;EAED;IACE,YAAY;IACZ,aAAa;GACd;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;;EAED;IACE,cAAc;GACf;CACF","file":"PricingViewDetails.scss","sourcesContent":[".pricingHeader {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  text-align: center;\n  padding-top: 5%;\n  position: relative;\n}\n\n.price_title {\n  font-weight: 600;\n  font-size: 56px;\n  color: #25282b;\n}\n\n.price_content {\n  width: 50%;\n  font-size: 16px;\n  line-height: 32px;\n  margin-top: 12px;\n}\n\n.whoAreYou {\n  margin: 72px 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.adminContainer {\n  cursor: pointer;\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 20px 20px;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.tabsContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin: 24px 0 72px 0;\n}\n\n.adminLabelContainer {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.adminLabel {\n  // width: 119px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 19px 38px 8px 8px;\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.imgContainer {\n  width: 40px;\n  height: 40px;\n  margin: 10px 8px 0 0;\n  padding: 10px;\n  background-color: rgba(196, 196, 196, 0.3);\n  border-radius: 50px;\n}\n\n.adminImg {\n  width: 20px;\n  height: 20px;\n  color: #25282b;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.plane {\n  margin: 0 0 24px 0;\n  font-size: 28px;\n  font-weight: 600;\n  text-align: center;\n  color: #25282b;\n}\n\n.moduleContainer {\n  width: 30%;\n  cursor: pointer;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 12px 30px;\n  border-right: 1px solid rgba(0, 0, 0, 0.2);\n  background-color: #fff;\n}\n\n.moduleContainer:last-child {\n  border-right-width: 0;\n}\n\n.moduleContainerActive {\n  background-color: #f36;\n}\n\n.iconActive {\n  background-color: #f36;\n}\n\n.headerContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  // height: 48px;\n  width: 100%;\n  border: solid 1px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n}\n\n.icon {\n  width: 24px;\n  height: 24px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.moduleName {\n  margin: 0 0 0 8px;\n  font-size: 14px;\n  letter-spacing: -0.28px;\n  color: #25282b;\n}\n\n.moduleNameActive {\n  color: #fff;\n  font-weight: 600;\n}\n\n.adminContainerActive {\n  position: relative;\n  width: 226px;\n  height: 82px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 40px 0 0;\n  padding: 10px 10px 0 20px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n          box-shadow: 0 0 16px 0 rgba(255, 51, 102, 0.16);\n  border: solid 1px #ff99b3;\n  text-align: center;\n}\n\n.rightIcon {\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  width: 16px;\n  height: 16px;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.active {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.subModules {\n  cursor: pointer;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin: 0 0 24px 0;\n  font-size: 16px;\n  line-height: 1.5;\n  color: #25282b;\n}\n\n.subModuleActive {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.5;\n  text-align: left;\n  color: #f36;\n}\n\n.iconDataContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  // margin-left: 1px solid #d9d9d9;\n}\n\n.iconDataContainer1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: row;\n      justify-content: row;\n  margin-left: 10px;\n  // margin-bottom: 8px;\n}\n\n.dataContainer {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 14px;\n  line-height: 1.57;\n  text-align: left;\n  color: #25282b;\n}\n\n.icons {\n  width: 18px;\n  height: 18px;\n  margin: 6px 12px 0 0;\n}\n\n.iconContainer {\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 12px;\n  // border-right: 1px solid #d9d9d9;\n}\n\n/* .dataIconContainer {\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  column-gap: 3rem;\n  row-gap: 3rem;\n}\n */\n\n/* .verticalLine {\n  height: 420px;\n  width: 1px;\n  margin: 63px 10px 0 0;\n  background-color: #d9d9d9;\n} */\n\n.studentModule {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  font-size: 16px;\n  line-height: 1.5;\n  text-align: left;\n  color: #25282b;\n}\n\n.sectionName {\n  -ms-flex-positive: 0;\n      flex-grow: 0;\n  margin-bottom: 24px;\n  font-size: 24px;\n  font-weight: 600;\n  line-height: 1.33;\n  text-align: left;\n  color: #25282b;\n}\n\n.subModulesFlex {\n  display: -ms-flexbox;\n  display: flex;\n  margin: 32px 0 32px 0;\n}\n\n/* .subsubmodules {\n  display: flex;\n}\n */\n\n.moduleFlex {\n  padding: 0 64px 0 64px;\n  width: 87%;\n  margin: auto;\n}\n\n/* .moduleFlex1 {\n  padding: 0 64px 0 64px;\n  width: fit-content;\n  margin: auto;\n}\n */\n\n.modules {\n  padding: 0 64px 0 64px;\n  width: 100%;\n  margin: auto;\n  overflow: hidden;\n}\n\n.arrowIcon {\n  width: 10.9px;\n  height: 16px;\n}\n\n.subModuleContainer {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n}\n\n.submodulesSectionContainer {\n  -ms-flex: 0.4 1;\n      flex: 0.4 1;\n  // width: 30%;\n}\n\n.submodulesSectionContainer1 {\n  -ms-flex: 0.3 1;\n      flex: 0.3 1;\n  // width: 25%;\n  margin: 50px 0 0 0;\n  // border-left: 1px solid #d9d9d9;\n}\n\n.imageContainer {\n  width: 9%;\n}\n\n/* @media screen and (min-width: 1280px) {\n  .moduleFlex{\n    width: 53.5%;\n  }\n} */\n\n@media only screen and (min-width: 991px) {\n  .plus {\n    display: none;\n  }\n\n  .moduleIcon {\n    display: none;\n  }\n\n  .subModuleMobileContainer {\n    display: none;\n  }\n\n  .plusContainer {\n    display: none;\n  }\n\n  /* .headerContainerMobileView {\n    display: none;\n  } */\n\n  .headerContainers {\n    display: none;\n  }\n\n  .goBackLink {\n    display: none;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .modules {\n    padding: 0;\n  }\n\n  .pricingHeader {\n    margin: 24px 0 0 0;\n    padding: 0 64px 0 64px;\n  }\n\n  .price_title {\n    font-size: 32px;\n  }\n\n  .price_content {\n    font-size: 14px;\n    width: 100%;\n    line-height: 1.71;\n    margin: 12px 0 48px 0;\n  }\n\n  .whoAreYou {\n    font-size: 20px;\n    margin: 48px 0 24px 0;\n  }\n\n  .goBackLink {\n    text-align: center;\n    font-size: 14px;\n    color: #f36;\n    text-decoration: underline;\n  }\n\n  .adminContainerActive {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    background-color: #e8e8e8;\n    border-style: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    padding: 0;\n    border-bottom-right-radius: 8px;\n    // padding: 0 49px 24px 50px;\n  }\n\n  /* .adminContainerActive:second-child {\n    border-bottom-left-radius: 8px;\n  } */\n\n  .adminContainer {\n    cursor: pointer;\n    width: 50%;\n    height: 65px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    border-style: none;\n    background-color: transparent;\n    margin: 0;\n    padding: 0;\n  }\n\n  .rightIcon {\n    display: none;\n  }\n\n  .imgContainer {\n    width: 10%;\n    height: 0;\n    background-color: transparent;\n    // padding: 24px 49px 24px 50px;\n    // margin-left: 40px;\n    // padding: 0;\n    // margin: 19px 0 0 0;\n  }\n\n  .adminLabel {\n    // width:0px;\n    font-size: 14px;\n    text-align: center;\n    margin: 19px 0 0 0;\n  }\n\n  .moduleData {\n    background-color: #f7f7f7;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .tabsContainer {\n    // justify-content: flex-start;\n    margin: 0 0 20px 0;\n    width: 100%;\n    padding: 0;\n  }\n\n  .plane {\n    display: none;\n  }\n\n  .moduleFlex {\n    padding: 0;\n  }\n\n  /* .headerContainer {\n    display: none;\n  } */\n\n  /*  .mobileMOduleContainer {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  }\n\n  .mobileMOduleContainerActive {\n    display: flex;\n    flex-direction: row;\n    justify-items: flex-start;\n    align-items: center;\n    border-radius: 24px;\n    width: 328px;\n    height: 44px;\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    margin: 0 0 8px 0;\n    padding: 0 0 0 16px;\n  } */\n\n  /*  .plus {\n    width: 20px;\n    height: 20px;\n  } */\n\n  /* .imageMobileUrl {\n    width: 10%;\n    height: 20px;\n  } */\n\n  /* .mobileModuleName {\n    width: 75%;\n    font-size: 14px;\n    color: #25282b;\n    text-align: left;\n  }\n\n  .mobileModuleNameActive {\n    width: 75%;\n    font-size: 14px;\n    color: #f36;\n    text-align: left;\n  } */\n\n  .headerContainers {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    // justify-content: center;\n    border: none;\n    // margin: 100px 0 0 0;\n  }\n\n  .active {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .adminLabelContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n  }\n\n  .moduleContainer {\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    width: 328px;\n    height: 44px;\n    border-radius: 24px;\n    border: solid 1px rgba(0, 0, 0, 0.2);\n    background-color: #fff;\n    margin: 0 0 8px 0;\n  }\n\n  .moduleContainerActive {\n    color: #f36;\n    border: solid 1px #ff99b3;\n    background-color: #fff5f7;\n    border-radius: 24px;\n  }\n\n  /* .icon {\n    width: 10%;\n    height: 20px;\n  } */\n\n  .adminImg {\n    width: 16px;\n    height: 16px;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-align: center;\n        align-items: center;\n  }\n\n  .moduleName {\n    width: 90%;\n    font-size: 14px;\n    color: #25282b;\n  }\n\n  .moduleNameActive {\n    color: #f36;\n    width: 90%;\n  }\n\n  .plus {\n    width: 20px;\n    height: 20px;\n    display: -ms-flexbox;\n    display: flex;\n    justify-items: flex-end;\n    -ms-flex-align: end;\n        align-items: flex-end;\n  }\n\n  .subModuleContainer {\n    display: none;\n  }\n\n  .dataContainer {\n    display: none;\n  }\n\n  .icons {\n    display: none;\n  }\n\n  .submodulesSectionContainer1 {\n    display: none;\n  }\n\n  .sectionName {\n    display: none;\n  }\n\n  /* .subModuleMobileContainer {\n    margin: 0 0 0 16px;\n  } */\n\n  .subModuleName {\n    font-size: 16px;\n    line-height: 1.5;\n    font-weight: 600;\n    color: #25282b;\n    margin: 16px 0 12px 0;\n  }\n\n  .submoduleDataContainer {\n    display: -ms-flexbox;\n    display: flex;\n    margin: 0 0 8px 0;\n  }\n\n  .submoduleData {\n    font-size: 12px;\n    color: #25282b;\n    text-align: left;\n    line-height: 1.67;\n    margin-left: 8px;\n  }\n\n  .rightIcons {\n    width: 20px;\n    height: 20px;\n  }\n\n  .iconActive {\n    display: none;\n  }\n\n  .subModuleMobileContainer {\n    // height: 20%;\n    overflow-y: scroll;\n    margin: 0 0 0 16px;\n  }\n\n  .icon {\n    display: none;\n  }\n\n  .moduleIcon {\n    width: 20px;\n    height: 20px;\n  }\n\n  .headerContainer {\n    display: none;\n  }\n\n  .submodulesSectionContainer {\n    display: none;\n  }\n\n  .subModulesFlex {\n    display: none;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"pricingHeader": "PricingViewDetails-pricingHeader-2bYv-",
	"price_title": "PricingViewDetails-price_title-2nqFt",
	"price_content": "PricingViewDetails-price_content-1fVtK",
	"whoAreYou": "PricingViewDetails-whoAreYou-3T0mg",
	"adminContainer": "PricingViewDetails-adminContainer-29ElQ",
	"tabsContainer": "PricingViewDetails-tabsContainer-3_Zwu",
	"adminLabelContainer": "PricingViewDetails-adminLabelContainer-2Mroj",
	"adminLabel": "PricingViewDetails-adminLabel-23wjb",
	"imgContainer": "PricingViewDetails-imgContainer-1Jj_u",
	"adminImg": "PricingViewDetails-adminImg-2mAsS",
	"plane": "PricingViewDetails-plane-21eaT",
	"moduleContainer": "PricingViewDetails-moduleContainer-I6SSy",
	"moduleContainerActive": "PricingViewDetails-moduleContainerActive-2BKQt",
	"iconActive": "PricingViewDetails-iconActive-1MVuw",
	"headerContainer": "PricingViewDetails-headerContainer-3Lmkq",
	"icon": "PricingViewDetails-icon-UXaHR",
	"moduleName": "PricingViewDetails-moduleName-3StFu",
	"moduleNameActive": "PricingViewDetails-moduleNameActive-3Lzqz",
	"adminContainerActive": "PricingViewDetails-adminContainerActive-1Z0JL",
	"rightIcon": "PricingViewDetails-rightIcon-2i32Z",
	"active": "PricingViewDetails-active-2cVke",
	"subModules": "PricingViewDetails-subModules-1R5-x",
	"subModuleActive": "PricingViewDetails-subModuleActive-1hEpJ",
	"iconDataContainer": "PricingViewDetails-iconDataContainer-i2G6T",
	"iconDataContainer1": "PricingViewDetails-iconDataContainer1-ur6rZ",
	"dataContainer": "PricingViewDetails-dataContainer-3Q7OM",
	"icons": "PricingViewDetails-icons-2XSLR",
	"iconContainer": "PricingViewDetails-iconContainer-1QcNN",
	"studentModule": "PricingViewDetails-studentModule-1lFxy",
	"sectionName": "PricingViewDetails-sectionName-1WSX5",
	"subModulesFlex": "PricingViewDetails-subModulesFlex-3p7bL",
	"moduleFlex": "PricingViewDetails-moduleFlex-2NouI",
	"modules": "PricingViewDetails-modules-1dSYh",
	"arrowIcon": "PricingViewDetails-arrowIcon-1I8M6",
	"subModuleContainer": "PricingViewDetails-subModuleContainer-2F_iK",
	"submodulesSectionContainer": "PricingViewDetails-submodulesSectionContainer-2D5Jx",
	"submodulesSectionContainer1": "PricingViewDetails-submodulesSectionContainer1-3Hz0T",
	"imageContainer": "PricingViewDetails-imageContainer-3CA5q",
	"plus": "PricingViewDetails-plus-25OpF",
	"moduleIcon": "PricingViewDetails-moduleIcon-_NUTR",
	"subModuleMobileContainer": "PricingViewDetails-subModuleMobileContainer-3Xkg3",
	"plusContainer": "PricingViewDetails-plusContainer-39Pqg",
	"headerContainers": "PricingViewDetails-headerContainers-3kGGo",
	"goBackLink": "PricingViewDetails-goBackLink-mPLEA",
	"moduleData": "PricingViewDetails-moduleData-EDv-k",
	"subModuleName": "PricingViewDetails-subModuleName-3x1To",
	"submoduleDataContainer": "PricingViewDetails-submoduleDataContainer-22IY-",
	"submoduleData": "PricingViewDetails-submoduleData-1NPL2",
	"rightIcons": "PricingViewDetails-rightIcons-81roB"
};

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/Constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanDetails", function() { return PlanDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "demo", function() { return demo; });
var PlanDetails = {
  admin: {
    label: 'Admin/Teacher',
    value: 'admin',
    imgUrl: '/images/admin.svg',
    modules: {
      onlinetest: {
        label: 'Online Tests',
        value: 'onlinetest',
        imageUrl: '/images/Tests.svg',
        mobileImageUrl: '/images/PricingDetails/TestsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeTests.svg',
        subModules: {
          tests: {
            label: 'Online Testing Platform',
            value: 'tests',
            data: [{
              label: 'Test Creation',
              value: 'testcreation'
            }, {
              label: 'Edit Test',
              value: 'edittest'
            }, {
              label: 'Delete Test',
              value: 'deletetest'
            }, {
              label: 'Grace Period',
              value: 'graceperiod'
            }, {
              label: 'Assign Question Paper to Teachers',
              value: 'assignquestionpapertoteacher'
            }, {
              label: 'Upload your own Question Paper in .docx',
              value: 'uploadyourownquestionpaperindocx'
            }, {
              label: 'Upload your own question paper in 5 min',
              value: 'uploadyourownquestionpaperin5min'
            }, {
              label: 'Supports any type of questions (single answer, multiple answer, paragraph etc.,',
              value: 'supportsanytypeofqustions'
            }, {
              label: 'Partial Paper',
              value: 'partialpaper'
            }, {
              label: 'Preview like Test Platform',
              value: 'previewliketestplatform'
            }, {
              label: 'Proctoring',
              value: 'proctoring'
            }, {
              label: 'Live Attendance / Attendance',
              value: 'liveattendanceattendance'
            }, {
              label: 'Downloadable attendance sheets',
              value: 'downloadableattendancesheets'
            }, {
              label: 'Downloadable question paper, key and solutions',
              value: 'downloadquestionpaperkeysolutions'
            }, {
              label: 'Key Editing',
              value: 'keyediting'
            }, {
              label: 'Auto Reports & Analysis',
              value: 'autoreportsanalysis'
            }, {
              label: 'Results generation for Offline exam',
              value: 'resultsgenerationforofflineexams'
            }, {
              label: 'Supports Online / Offline tests',
              value: 'supportsonlineofflinetests'
            }, {
              label: 'Flexible test times',
              value: 'flexibletesttimes '
            }, {
              label: 'Best Hierarcy system',
              value: 'besthierarcysystem'
            }, {
              label: 'Parser can convert text / images / equations / graphs',
              value: 'parsercanconverttextimagesequationsgraphs'
            }]
          },
          qustionbank: {
            label: 'Question Bank',
            value: 'qustionbank',
            data: [{
              label: 'Question bank with 1,50,000+ Questions with Key and Solutions',
              value: 'questionbankwith1,50,000+questionswithkeyandsolutions'
            }, {
              label: 'Questions in 3 difficulty levels (Easy / Medium / High)',
              value: 'Questionsin3difficultylevels'
            }, {
              label: 'Question tagging',
              value: 'questiontagging'
            }]
          },
          papers: {
            label: 'Papers',
            value: 'papers',
            data: [{
              label: 'Create Question Paper',
              value: 'createquestionpaper'
            }, {
              label: 'Select questions',
              value: 'selectquestions'
            }, {
              label: 'Question paper resume feature',
              value: 'questionpaperresumefeature'
            }, {
              label: 'Partial Paper feature',
              value: 'partialpaperfeature'
            }, {
              label: 'Design personalized question paper/s with Institute Name, Logo & Watermark',
              value: 'designpersonalizedquestionpaper/swithinstitutenamelogo&watermark'
            }]
          },
          practice: {
            label: 'Practice',
            value: 'practice',
            data: [{
              label: 'Unlimited Practice tests',
              value: 'unlimitedpracticetests'
            }, {
              label: 'Questions based practice',
              value: 'questionsbasedpractice'
            }, {
              label: 'Time based Practice',
              value: 'timebasedpractice'
            }, {
              label: 'Detailed analysis of all practice tests',
              value: 'detailedanalysisofallpracticetests'
            }]
          },
          analysis: {
            label: 'Analysis',
            value: 'analysis',
            data: [{
              label: 'Marks Analyssis',
              value: 'markanalyssis'
            }, {
              label: 'Topic wise Analysis',
              value: 'topicwiseanalysis'
            }, {
              label: 'Error Analysis',
              value: 'erroranalysis'
            }]
          },
          reports: {
            label: 'Reports',
            value: 'reports',
            data: [{
              label: 'Test Results',
              value: 'testresults'
            }, {
              label: 'C_W_U Report',
              value: 'cwcreport'
            }, {
              label: 'Marks Distribution',
              value: 'marksdistribution'
            }, {
              label: 'Error Count',
              value: 'errorcount'
            }, {
              label: 'Weak Subjects',
              value: 'Weaksubject'
            }]
          }
        }
      },
      liveclasses: {
        label: 'Live classes',
        value: 'liveclasses',
        imageUrl: '/images/Live.svg',
        mobileImageUrl: '/images/PricingDetails/LiveMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeLive.svg',
        subModules: {
          live: {
            label: 'Live',
            value: 'live',
            data: [{
              label: 'Interractive Live Class Creation (integrated with zoom)',
              value: 'interractiveliveclasscreation'
            }, {
              label: 'Recording Class creation',
              value: 'recordingclasscreation'
            }, {
              label: 'Recurring classes',
              value: 'recurringclasses'
            }, {
              label: 'Attach file while class creating (image / pdf)',
              value: 'attachfilewhileclasscreating'
            }, {
              label: 'Edit Class',
              value: 'editclass'
            }, {
              label: 'Delete Class',
              value: 'deleteclass'
            }, {
              label: 'Live Attendance',
              value: 'liveattendance'
            }, {
              label: 'Watch later',
              value: 'watchlater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }, {
              label: 'Download classes list',
              value: 'downloadclasseslist'
            }, {
              label: 'Secure Notes, Downloadable links',
              value: 'securenotesdownloadablelinks'
            }, {
              label: 'Student Management System',
              value: 'studentmanagementsystem'
            }, {
              label: 'Best Hierarcy system',
              value: 'besthierarcysystem'
            }]
          },
          securecontent: {
            label: 'Restricted Login',
            value: 'securecontent',
            data: [{
              label: 'Content visible only inside App',
              value: 'contentvisibleonlyinsideapp'
            }, {
              label: 'Video Link sharing disabled',
              value: 'videolinksharingdisabled'
            }, {
              label: 'Screen Recording Disabled',
              value: 'screenrecordingdisabled'
            }, {
              label: 'Screenshot Disabled',
              value: 'screenshotdisabled'
            }, {
              label: 'eNotes Downloading Enabled/Disabled',
              value: 'enotesdownloadingenableddisabled'
            }, {
              label: 'Only Registered Students can enter with known identity, so no nuisance during class unlike Zoom',
              value: 'onlyregisteredstudentscanenterwithknownidentitysononuisanceduringclassunlikezoom'
            }]
          }
        }
      },
      assignments: {
        label: 'Assignments',
        value: 'assignments',
        imageUrl: '/images/Assignments.svg',
        mobileImageUrl: '/images/PricingDetails/AssignmentsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeAssignments.svg',
        subModules: {
          assignment: {
            label: 'Assignments',
            value: 'assignment',
            data: [{
              label: 'Easy Creation of assignment',
              value: 'easycreationofassignment'
            }, {
              label: 'Submission status',
              value: 'submissionstatus'
            }, {
              label: 'Awarding Marks',
              value: 'awardingmarks'
            }, {
              label: 'Easy to Evaluate',
              value: 'easytoevaluate'
            }, {
              label: 'Helpful Toolbox',
              value: 'helpfultoolbox'
            }, {
              label: 'Anytime, Anywhere Assignments',
              value: 'anytimeanywhereassignments'
            }, {
              label: 'Grade Faster with Easy-to-Use Tools',
              value: 'gradefasterwitheasytousetools'
            }]
          }
        }
      },
      doubts: {
        label: 'Doubts',
        value: 'doubts',
        imageUrl: '/images/Doubts.svg',
        mobileImageUrl: '/images/PricingDetails/DoubtsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeDoubts.svg',
        subModules: {
          doubt: {
            label: 'Doubts',
            value: 'doubt',
            data: [{
              label: 'Easy to clear doubts (Chat / upload image / insert link)',
              value: 'easytocleardoubts'
            }, {
              label: 'Bookmark doubts',
              value: 'bookmarkdoubts'
            }, {
              label: 'Reopen doubts',
              value: 'reopendoubts'
            }, {
              label: 'Check later',
              value: 'checklater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }]
          }
        }
      },
      connect: {
        label: 'Connect',
        value: 'connect',
        imageUrl: '/images/Connect.svg',
        mobileImageUrl: '/images/PricingDetails/ConnectMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeConnect.svg',
        subModules: {
          schedule: {
            label: 'Schedule',
            value: 'schedule'
          },
          notifications: {
            label: 'Notifications',
            value: 'notifications'
          },
          messanger: {
            label: 'Messanger',
            value: 'messanger'
          }
        }
      }
    }
  },
  student: {
    label: 'Student',
    value: 'student',
    imgUrl: '/images/student.svg',
    modules: {
      onlinetest: {
        label: 'Online Tests',
        value: 'onlinetest',
        imageUrl: '/images/Tests.svg',
        mobileImageUrl: '/images/PricingDetails/TestsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeTests.svg',
        subModules: {
          tests: {
            label: 'Tests',
            value: 'tests',
            data: [{
              label: 'Easy to check Upcoming Tests',
              value: 'easytocheckupcomingtests'
            }, {
              label: 'Easy to attempt',
              value: 'easytoattempt'
            }, {
              label: 'Answered / Not Answered / Not visited lists can be checked easily',
              value: 'Answerednotanswerednotvisitedlistscanbecheckedeasily'
            }, {
              label: 'Subject wise questions response can be checked easily by colours',
              value: 'subjectwisequestionsresponsecanbecheckedeasilybycolours'
            }, {
              label: 'Instant Results',
              value: 'instantresults'
            }]
          },
          analysis: {
            label: 'Analysis',
            value: 'analysis',
            data: [{
              label: 'Overall Test Performance',
              value: 'overalltestperformance'
            }, {
              label: 'Attempt & Accuracy rate in all tests',
              value: 'attemptaccuracyrateinalltests'
            }, {
              label: 'Tests Performance in all topics',
              value: 'testsperformanceinalltopics'
            }, {
              label: 'Overall / Subject wise Marks Trend',
              value: 'overallsubjectwisemarkstrend'
            }, {
              label: 'Overall / Subject wise Attempt and Accuracy trend',
              value: 'overallsubjectwiseattemptandaccuracytrend'
            }, {
              label: 'Easily find completed tests',
              value: 'easilyfindcompletedtests'
            }, {
              label: 'Individual and Overall Summary',
              value: 'individualandoverallsummary'
            }, {
              label: 'Subject wise marks of all / individual tests',
              value: 'subjectwisemarksofallindividualtests'
            }, {
              label: 'Attempt & Accuracy rate in all subjects',
              value: 'attemptaccuracyrateinallsubjects'
            }, {
              label: 'Questions distribution in overall & subject wise in individual / all tests',
              value: 'questionsdistributioninoverallsubjectwiseinindividualalltests'
            }, {
              label: 'Topic / sub topic / question level analysis',
              value: 'topicsubtopicquestionlevelanalysis'
            }, {
              label: 'Question wise behaviour analysis',
              value: 'questionwisebehaviouranalysis'
            }, {
              label: 'Question / subject / difficult level Time spent',
              value: 'questionsubjectdifficultleveltimespent'
            }, {
              label: 'Time spent on Correct / wrong / unattempted questions',
              value: 'timespentoncorrectwrongunattemptedquestions'
            }, {
              label: 'Can Compare wit Topper',
              value: 'cancomparewittopper'
            }]
          },
          practice: {
            label: 'Practice',
            value: 'practice',
            data: [{
              label: 'Practice available for 11th, 12th and both',
              value: 'practiceavailablefor11th12thandboth'
            }, {
              label: 'Supports - JEE Main / NEET / EAMCET',
              value: 'supportsjeemainneeteamcet'
            }, {
              label: 'Unlimited Practice tests',
              value: 'unlimitedpracticetests'
            }, {
              label: 'Full syllabus Tests',
              value: 'fullsyllabustests'
            }, {
              label: 'Subject wise practice',
              value: 'subjectwisepractice'
            }, {
              label: 'chapter wise practice',
              value: 'chapterwisepractice'
            }, {
              label: 'Questions based practice',
              value: 'questionsbasedpractice'
            }, {
              label: 'Time based Practice',
              value: 'timebasedpractice'
            }, {
              label: 'Instant Results',
              value: 'instantresults'
            }, {
              label: 'Detailed analysis of all practice tests',
              value: 'detailedanalysisofallpracticetests'
            }]
          }
        }
      },
      liveclasses: {
        label: 'Live classes',
        value: 'liveclasses',
        imageUrl: '/images/Live.svg',
        mobileImageUrl: '/images/PricingDetails/LiveMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeLive.svg',
        subModules: {
          live: {
            label: 'Live',
            value: 'live',
            data: [{
              label: 'Signle Click to Join class',
              value: 'signleclicktojoinclass'
            }, {
              label: 'No need to download any apps (zoom integrated)',
              value: 'noneedtodownloadanyapps'
            }, {
              label: 'Watch later',
              value: 'watchlater'
            }, {
              label: 'Smart Filters',
              value: 'Smartfilters'
            }]
          }
        }
      },
      assignments: {
        label: 'Assignments',
        value: 'assignments',
        imageUrl: '/images/Assignments.svg',
        mobileImageUrl: '/images/PricingDetails/AssignmentsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeAssignments.svg',
        subModules: {
          assignment: {
            label: 'Assignments',
            value: 'assignment',
            data: [{
              label: 'Easy submission',
              value: 'easysubmission'
            }, {
              label: 'Easy to check evaluation',
              value: 'easytocheckevaluation'
            }]
          }
        }
      },
      doubts: {
        label: 'Doubts',
        value: 'doubts',
        imageUrl: '/images/Doubts.svg',
        mobileImageUrl: '/images/PricingDetails/DoubtsMobile.svg',
        activeImageUrl: '/images/PricingDetails/activeDoubts.svg',
        subModules: {
          doubt: {
            label: 'Doubts',
            value: 'doubt',
            data: [{
              label: 'Easy to ask doubts (Chat / upload image / insert link)',
              value: 'easytoaskdoubts'
            }, {
              label: 'Bookmark doubts',
              value: 'bookmarkdoubts'
            }, {
              label: 'Reopen doubts',
              value: 'reopendoubts'
            }, {
              label: 'Check later',
              value: 'checklater'
            }, {
              label: 'Smart Filters',
              value: 'smartfilters'
            }]
          }
        }
      }
    }
  }
};
var demo = function demo() {};

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var _Constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/Constants.js");
/* harmony import */ var _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss");
/* harmony import */ var _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var PricingViewDetails =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PricingViewDetails, _React$Component);

  function PricingViewDetails(props) {
    var _this;

    _classCallCheck(this, PricingViewDetails);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PricingViewDetails).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleResize", function () {
      var mobile = _this.state.mobile; // console.log("function called")

      if (window.innerWidth < 990) {
        _this.setState({
          mobile: !mobile
        });
      } else {
        _this.setState({
          mobile: mobile
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "showRole", function (value) {
      _this.setState({
        show: !true
      });

      var role = _this.state.role;
      var plan = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][value].modules;
      var first = Object.values(plan || {});
      var firstMod = first[0].value;
      var subMod = plan[firstMod].subModules;
      var firstSub = Object.values(subMod || {});
      var firstOne = firstSub[0].value;

      if (value !== role) {
        _this.setState({
          role: value,
          activeModule: firstMod,
          activeSubmodule: firstOne
        });
      } else {
        _this.setState({
          role: value,
          activeModule: firstMod,
          activeSubmodule: firstOne
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "showModules", function (key) {
      var _this$state = _this.state,
          role = _this$state.role,
          activeModule = _this$state.activeModule;
      var mod = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      var sub = mod[key].subModules;
      var submodule = Object.values(sub || {});

      if (key !== activeModule) {
        _this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });
      } else {
        _this.setState({
          activeModule: key
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "showSubmodule", function (value) {
      var activeSubmodule = _this.state.activeSubmodule;

      if (value !== activeSubmodule) {
        _this.setState({
          activeSubmodule: value
        });
      }

      _this.setState({
        activeSubmodule: value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "showData", function (value) {
      var activeSection = _this.state.activeSection;

      if (value !== activeSection) {
        _this.setState({
          activeSection: value
        });
      } else {
        _this.setState({
          activeSection: value
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "displaySections", function () {
      var _this$state2 = _this.state,
          role = _this$state2.role,
          activeModule = _this$state2.activeModule,
          activeSubmodule = _this$state2.activeSubmodule;
      var activeSubModuleName = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule].label;
      var sectionData = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submodulesSectionContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.sectionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, activeSubModuleName), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, Object.values(sectionData.data || {}).map(function (d, index) {
        var below = index < 10;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconDataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 106
          },
          __self: this
        }, below && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 108
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Icons.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icons,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 114
          },
          __self: this
        }, d.label)));
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displaySection", function () {
      var _this$state3 = _this.state,
          role = _this$state3.role,
          activeModule = _this$state3.activeModule,
          activeSubmodule = _this$state3.activeSubmodule;
      var sectionData = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules[activeSubmodule];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submodulesSectionContainer1,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, Object.values(sectionData.data || {}).map(function (d, index) {
        var below = index >= 10;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconDataContainer1,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 135
          },
          __self: this
        }, below && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 137
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Icons.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icons,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 138
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.dataContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 143
          },
          __self: this
        }, d.label)));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displaySubModules", function () {
      var _this$state4 = _this.state,
          role = _this$state4.role,
          activeModule = _this$state4.activeModule,
          activeSubmodule = _this$state4.activeSubmodule;
      var filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules[activeModule].subModules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, Object.values(filter || {}).map(function (each) {
        var isActive = activeSubmodule === each.value;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.studentModule,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 161
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imageContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 162
          },
          __self: this
        }, isActive ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/PricingDetails/Arrow.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.arrowIcon,
          alt: "arrow",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 164
          },
          __self: this
        }) : null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModules, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModules,
          onClick: function onClick() {
            _this.showSubmodule(each.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 171
          },
          __self: this
        }, each.label));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "showModule", function (key) {
      var _this$state5 = _this.state,
          role = _this$state5.role,
          activeModule = _this$state5.activeModule;
      var mod = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      var sub = mod[key].subModules;
      var submodule = Object.values(sub || {});

      if (key !== activeModule) {
        _this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });
        /* this.setState(prevState => ({
          show: !prevState.show,
        }));   */


        _this.setState({
          show: !true
        });
      } else {
        _this.setState({
          activeModule: key,
          activeSubmodule: submodule[0].value
        });

        _this.setState(function (prevState) {
          return {
            show: !prevState.show
          };
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "displayModule", function () {
      var _this$state6 = _this.state,
          role = _this$state6.role,
          activeModule = _this$state6.activeModule,
          show = _this$state6.show;
      var filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainers,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }, Object.values(filter).map(function (item) {
        var isActiveModule = activeModule === item.value;
        var showAll = activeModule === item.value;
        var imageUrl = isActiveModule ? item.activeImageUrl : item.imageUrl;
        var imageMobileUrl = isActiveModule && !show ? item.mobileImageUrl : item.imageUrl;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 224
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActiveModule && !show ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainerActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer,
          onClick: function onClick() {
            _this.showModule(item.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 225
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageUrl,
          className: isActiveModule ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon,
          alt: "img",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 236
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageMobileUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleIcon,
          alt: "mobileIcon",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 243
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: isActiveModule && !show ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleNameActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 248
          },
          __self: this
        }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plusContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 257
          },
          __self: this
        }, isActiveModule && !show ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/minus.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plus,
          alt: "minus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 259
          },
          __self: this
        }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/plus.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plus,
          alt: "plus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 265
          },
          __self: this
        }))), isActiveModule && !show && showAll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 270
          },
          __self: this
        }, Object.values(item.subModules || {}).map(function (each) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleMobileContainer, " custom-scrollbar"),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 272
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModuleName,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 275
            },
            __self: this
          }, each.label), Object.values(each.data || {}).map(function (data) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
              className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submoduleDataContainer,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 277
              },
              __self: this
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
              src: "/images/Icons.svg",
              className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rightIcons,
              alt: "right",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 278
              },
              __self: this
            }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
              className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.submoduleData,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 283
              },
              __self: this
            }, data.label));
          }));
        })));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayModules", function () {
      var _this$state7 = _this.state,
          role = _this$state7.role,
          activeModule = _this$state7.activeModule;
      var filter = _Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"][role].modules;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.headerContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 301
        },
        __self: this
      }, Object.values(filter).map(function (item) {
        var isActiveModule = activeModule === item.value;
        var imageUrl = isActiveModule ? item.activeImageUrl : item.imageUrl;
        var imageMobileUrl = isActiveModule ? item.mobileImageUrl : item.imageUrl;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActiveModule ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainerActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleContainer,
          onClick: function onClick() {
            _this.showModules(item.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 309
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageUrl,
          className: isActiveModule ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.iconActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.icon,
          alt: "img",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 320
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: imageMobileUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleIcon,
          alt: "mobile",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 327
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: isActiveModule ? "".concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName, " ").concat(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleNameActive) : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 328
          },
          __self: this
        }, item.label));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayTypeOfUser", function () {
      var role = _this.state.role;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tabsContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        },
        __self: this
      }, Object.values(_Constants__WEBPACK_IMPORTED_MODULE_3__["PlanDetails"]).map(function (each) {
        var isActive = role === each.value;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminContainerActive : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminContainer,
          onClick: function onClick() {
            _this.showRole(each.value);
          },
          role: "presentation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 351
          },
          __self: this
        }, isActive ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "/images/Shape.svg",
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.rightIcon,
          alt: "right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 359
          },
          __self: this
        }) : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: isActive ? _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.active : _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminLabelContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 365
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.imgContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 366
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: each.imgUrl,
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminImg,
          alt: "student",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 367
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adminLabel,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 369
          },
          __self: this
        }, each.label)));
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayPlanes", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.plane,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 378
        },
        __self: this
      }, "Every Plan includes");
    });

    _defineProperty(_assertThisInitialized(_this), "displayPricingHeader", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.pricingHeader,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 381
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 382
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 383
        },
        __self: this
      }, "Choose your edition."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 384
        },
        __self: this
      }, "Try it free for 7 days.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.price_content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 386
        },
        __self: this
      }, "Egnify plans start as low as Rs.45/- per student per month."));
    });

    _defineProperty(_assertThisInitialized(_this), "dispalyWhoAreYou", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 393
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.whoAreYou,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 394
        },
        __self: this
      }, "Who are you?"));
    });

    _defineProperty(_assertThisInitialized(_this), "dispalyGoBackLink", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/pricing",
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.goBackLink,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 399
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 400
        },
        __self: this
      }, "back to pricing details"));
    });

    _this.state = {
      role: 'admin',
      activeModule: 'onlinetest',
      activeSubmodule: 'tests',
      mobile: false,
      show: false
    };
    return _this;
  }

  _createClass(PricingViewDetails, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleResize();
      window.addEventListener('resize', this.handleResize);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.modules,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 406
        },
        __self: this
      }, this.displayPricingHeader(), this.dispalyGoBackLink(), this.dispalyWhoAreYou(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleData,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 410
        },
        __self: this
      }, this.displayTypeOfUser(), this.displayPlanes(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.moduleFlex,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 413
        },
        __self: this
      }, this.displayModules(), this.displayModule(), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a.subModulesFlex,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 417
        },
        __self: this
      }, this.displaySubModules(), this.displaySections(), this.displaySection()))));
    }
  }]);

  return PricingViewDetails;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_PricingViewDetails_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(PricingViewDetails));

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/PricingViewDetails/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _PricingViewDetails__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/PricingViewDetails/PricingViewDetails.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/PricingViewDetails/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: ['GetRanks by Egnify: Assessment & Analytics Platform'],
              chunks: ['PricingDetails'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PricingViewDetails__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJpY2luZ0RldGFpbHMuY2h1bmsuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9QcmljaW5nVmlld0RldGFpbHMvUHJpY2luZ1ZpZXdEZXRhaWxzLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvUHJpY2luZ1ZpZXdEZXRhaWxzL0NvbnN0YW50cy5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9QcmljaW5nVmlld0RldGFpbHMvUHJpY2luZ1ZpZXdEZXRhaWxzLmpzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1ByaWNpbmdWaWV3RGV0YWlscy9QcmljaW5nVmlld0RldGFpbHMuc2Nzcz85NWI4IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1ByaWNpbmdWaWV3RGV0YWlscy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlByaWNpbmdWaWV3RGV0YWlscy1wcmljaW5nSGVhZGVyLTJiWXYtIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNSU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtcHJpY2VfdGl0bGUtMm5xRnQge1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc2l6ZTogNTZweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNlX2NvbnRlbnQtMWZWdEsge1xcbiAgd2lkdGg6IDUwJTtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgbWFyZ2luLXRvcDogMTJweDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy13aG9BcmVZb3UtM1QwbWcge1xcbiAgbWFyZ2luOiA3MnB4IDAgMjRweCAwO1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluQ29udGFpbmVyLTI5RWxRIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiAyMjZweDtcXG4gIGhlaWdodDogODJweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHggMTBweCAyMHB4IDIwcHg7XFxuICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXRhYnNDb250YWluZXItM19ad3Uge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgbWFyZ2luOiAyNHB4IDAgNzJweCAwO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluTGFiZWxDb250YWluZXItMk1yb2oge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluTGFiZWwtMjN3amIge1xcbiAgLy8gd2lkdGg6IDExOXB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luOiAxOXB4IDM4cHggOHB4IDhweDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWltZ0NvbnRhaW5lci0xSmpfdSB7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIG1hcmdpbjogMTBweCA4cHggMCAwO1xcbiAgcGFkZGluZzogMTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMTk2LCAxOTYsIDE5NiwgMC4zKTtcXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtYWRtaW5JbWctMm1Bc1Mge1xcbiAgd2lkdGg6IDIwcHg7XFxuICBoZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXBsYW5lLTIxZWFUIHtcXG4gIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVDb250YWluZXItSTZTU3kge1xcbiAgd2lkdGg6IDMwJTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDEycHggMzBweDtcXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlQ29udGFpbmVyLUk2U1N5Omxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiAwO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUNvbnRhaW5lckFjdGl2ZS0yQktRdCB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25BY3RpdmUtMU1WdXcge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1oZWFkZXJDb250YWluZXItM0xta3Ege1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLy8gaGVpZ2h0OiA0OHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtaWNvbi1VWGFIUiB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZU5hbWUtM1N0RnUge1xcbiAgbWFyZ2luOiAwIDAgMCA4cHg7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsZXR0ZXItc3BhY2luZzogLTAuMjhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZU5hbWVBY3RpdmUtM0x6cXoge1xcbiAgY29sb3I6ICNmZmY7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluQ29udGFpbmVyQWN0aXZlLTFaMEpMIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiAyMjZweDtcXG4gIGhlaWdodDogODJweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHggMTBweCAwIDIwcHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAxNnB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMTZweCAwIHJnYmEoMjU1LCA1MSwgMTAyLCAwLjE2KTtcXG4gIGJvcmRlcjogc29saWQgMXB4ICNmZjk5YjM7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtcmlnaHRJY29uLTJpMzJaIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogOHB4O1xcbiAgcmlnaHQ6IDhweDtcXG4gIHdpZHRoOiAxNnB4O1xcbiAgaGVpZ2h0OiAxNnB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtYWN0aXZlLTJjVmtlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVzLTFSNS14IHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIG1hcmdpbjogMCAwIDI0cHggMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVBY3RpdmUtMWhFcEoge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtaWNvbkRhdGFDb250YWluZXItaTJHNlQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogcm93O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogcm93O1xcbiAgLy8gbWFyZ2luLWxlZnQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25EYXRhQ29udGFpbmVyMS11cjZyWiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiByb3c7XFxuICAgICAganVzdGlmeS1jb250ZW50OiByb3c7XFxuICBtYXJnaW4tbGVmdDogMTBweDtcXG4gIC8vIG1hcmdpbi1ib3R0b206IDhweDtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1kYXRhQ29udGFpbmVyLTNRN09NIHtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU3O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25zLTJYU0xSIHtcXG4gIHdpZHRoOiAxOHB4O1xcbiAgaGVpZ2h0OiAxOHB4O1xcbiAgbWFyZ2luOiA2cHggMTJweCAwIDA7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtaWNvbkNvbnRhaW5lci0xUWNOTiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgLy8gYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2Q5ZDlkOTtcXG59XFxuXFxuLyogLmRhdGFJY29uQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xcbiAgY29sdW1uLWdhcDogM3JlbTtcXG4gIHJvdy1nYXA6IDNyZW07XFxufVxcbiAqL1xcblxcbi8qIC52ZXJ0aWNhbExpbmUge1xcbiAgaGVpZ2h0OiA0MjBweDtcXG4gIHdpZHRoOiAxcHg7XFxuICBtYXJnaW46IDYzcHggMTBweCAwIDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDlkOWQ5O1xcbn0gKi9cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXN0dWRlbnRNb2R1bGUtMWxGeHkge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zZWN0aW9uTmFtZS0xV1NYNSB7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjMzO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZXNGbGV4LTNwN2JMIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIG1hcmdpbjogMzJweCAwIDMycHggMDtcXG59XFxuXFxuLyogLnN1YnN1Ym1vZHVsZXMge1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuICovXFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVGbGV4LTJOb3VJIHtcXG4gIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB3aWR0aDogODclO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4vKiAubW9kdWxlRmxleDEge1xcbiAgcGFkZGluZzogMCA2NHB4IDAgNjRweDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuICovXFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVzLTFkU1loIHtcXG4gIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1hcmdpbjogYXV0bztcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtYXJyb3dJY29uLTFJOE02IHtcXG4gIHdpZHRoOiAxMC45cHg7XFxuICBoZWlnaHQ6IDE2cHg7XFxufVxcblxcbi5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlQ29udGFpbmVyLTJGX2lLIHtcXG4gIC1tcy1mbGV4OiAwLjMgMTtcXG4gICAgICBmbGV4OiAwLjMgMTtcXG59XFxuXFxuLlByaWNpbmdWaWV3RGV0YWlscy1zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lci0yRDVKeCB7XFxuICAtbXMtZmxleDogMC40IDE7XFxuICAgICAgZmxleDogMC40IDE7XFxuICAvLyB3aWR0aDogMzAlO1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMS0zSHowVCB7XFxuICAtbXMtZmxleDogMC4zIDE7XFxuICAgICAgZmxleDogMC4zIDE7XFxuICAvLyB3aWR0aDogMjUlO1xcbiAgbWFyZ2luOiA1MHB4IDAgMCAwO1xcbiAgLy8gYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uUHJpY2luZ1ZpZXdEZXRhaWxzLWltYWdlQ29udGFpbmVyLTNDQTVxIHtcXG4gIHdpZHRoOiA5JTtcXG59XFxuXFxuLyogQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KSB7XFxuICAubW9kdWxlRmxleHtcXG4gICAgd2lkdGg6IDUzLjUlO1xcbiAgfVxcbn0gKi9cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDk5MXB4KSB7XFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXBsdXMtMjVPcEYge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVJY29uLV9OVVRSIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlTW9iaWxlQ29udGFpbmVyLTNYa2czIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtcGx1c0NvbnRhaW5lci0zOVBxZyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAvKiAuaGVhZGVyQ29udGFpbmVyTW9iaWxlVmlldyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWhlYWRlckNvbnRhaW5lcnMtM2tHR28ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1nb0JhY2tMaW5rLW1QTEVBIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVzLTFkU1loIHtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtcHJpY2luZ0hlYWRlci0yYll2LSB7XFxuICAgIG1hcmdpbjogMjRweCAwIDAgMDtcXG4gICAgcGFkZGluZzogMCA2NHB4IDAgNjRweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtcHJpY2VfdGl0bGUtMm5xRnQge1xcbiAgICBmb250LXNpemU6IDMycHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNlX2NvbnRlbnQtMWZWdEsge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBsaW5lLWhlaWdodDogMS43MTtcXG4gICAgbWFyZ2luOiAxMnB4IDAgNDhweCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy13aG9BcmVZb3UtM1QwbWcge1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIG1hcmdpbjogNDhweCAwIDI0cHggMDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtZ29CYWNrTGluay1tUExFQSB7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWFkbWluQ29udGFpbmVyQWN0aXZlLTFaMEpMIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB3aWR0aDogNTAlO1xcbiAgICBoZWlnaHQ6IDY1cHg7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcXG4gICAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogOHB4O1xcbiAgICAvLyBwYWRkaW5nOiAwIDQ5cHggMjRweCA1MHB4O1xcbiAgfVxcblxcbiAgLyogLmFkbWluQ29udGFpbmVyQWN0aXZlOnNlY29uZC1jaGlsZCB7XFxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDhweDtcXG4gIH0gKi9cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtYWRtaW5Db250YWluZXItMjlFbFEge1xcbiAgICBjdXJzb3I6IHBvaW50ZXI7XFxuICAgIHdpZHRoOiA1MCU7XFxuICAgIGhlaWdodDogNjVweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gICAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1yaWdodEljb24tMmkzMloge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1pbWdDb250YWluZXItMUpqX3Uge1xcbiAgICB3aWR0aDogMTAlO1xcbiAgICBoZWlnaHQ6IDA7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgICAvLyBwYWRkaW5nOiAyNHB4IDQ5cHggMjRweCA1MHB4O1xcbiAgICAvLyBtYXJnaW4tbGVmdDogNDBweDtcXG4gICAgLy8gcGFkZGluZzogMDtcXG4gICAgLy8gbWFyZ2luOiAxOXB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkxhYmVsLTIzd2piIHtcXG4gICAgLy8gd2lkdGg6MHB4O1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luOiAxOXB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVEYXRhLUVEdi1rIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXRhYnNDb250YWluZXItM19ad3Uge1xcbiAgICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIG1hcmdpbjogMCAwIDIwcHggMDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXBsYW5lLTIxZWFUIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlRmxleC0yTm91SSB7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAvKiAuaGVhZGVyQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH0gKi9cXG5cXG4gIC8qICAubW9iaWxlTU9kdWxlQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAganVzdGlmeS1pdGVtczogZmxleC1zdGFydDtcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgd2lkdGg6IDMyOHB4O1xcbiAgICBoZWlnaHQ6IDQ0cHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICAgIGJvcmRlcjogc29saWQgMXB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICAgIHBhZGRpbmc6IDAgMCAwIDE2cHg7XFxuICB9XFxuXFxuICAubW9iaWxlTU9kdWxlQ29udGFpbmVyQWN0aXZlIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAganVzdGlmeS1pdGVtczogZmxleC1zdGFydDtcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gICAgd2lkdGg6IDMyOHB4O1xcbiAgICBoZWlnaHQ6IDQ0cHg7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjZmY5OWIzO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmNWY3O1xcbiAgICBtYXJnaW46IDAgMCA4cHggMDtcXG4gICAgcGFkZGluZzogMCAwIDAgMTZweDtcXG4gIH0gKi9cXG5cXG4gIC8qICAucGx1cyB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9ICovXFxuXFxuICAvKiAuaW1hZ2VNb2JpbGVVcmwge1xcbiAgICB3aWR0aDogMTAlO1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9ICovXFxuXFxuICAvKiAubW9iaWxlTW9kdWxlTmFtZSB7XFxuICAgIHdpZHRoOiA3NSU7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICB9XFxuXFxuICAubW9iaWxlTW9kdWxlTmFtZUFjdGl2ZSB7XFxuICAgIHdpZHRoOiA3NSU7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICB9ICovXFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWhlYWRlckNvbnRhaW5lcnMtM2tHR28ge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgYm9yZGVyOiBub25lO1xcbiAgICAvLyBtYXJnaW46IDEwMHB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1hY3RpdmUtMmNWa2Uge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkxhYmVsQ29udGFpbmVyLTJNcm9qIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtbW9kdWxlQ29udGFpbmVyLUk2U1N5IHtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVDb250YWluZXJBY3RpdmUtMkJLUXQge1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmOTliMztcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjVmNztcXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcXG4gIH1cXG5cXG4gIC8qIC5pY29uIHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkltZy0ybUFzUyB7XFxuICAgIHdpZHRoOiAxNnB4O1xcbiAgICBoZWlnaHQ6IDE2cHg7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZU5hbWUtM1N0RnUge1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lQWN0aXZlLTNMenF6IHtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIHdpZHRoOiA5MCU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXBsdXMtMjVPcEYge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAganVzdGlmeS1pdGVtczogZmxleC1lbmQ7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBlbmQ7XFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZUNvbnRhaW5lci0yRl9pSyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWRhdGFDb250YWluZXItM1E3T00ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1pY29ucy0yWFNMUiB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMS0zSHowVCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXNlY3Rpb25OYW1lLTFXU1g1IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC8qIC5zdWJNb2R1bGVNb2JpbGVDb250YWluZXIge1xcbiAgICBtYXJnaW46IDAgMCAwIDE2cHg7XFxuICB9ICovXFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZU5hbWUtM3gxVG8ge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICBtYXJnaW46IDE2cHggMCAxMnB4IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZURhdGFDb250YWluZXItMjJJWS0ge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZURhdGEtMU5QTDIge1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgICBsaW5lLWhlaWdodDogMS42NztcXG4gICAgbWFyZ2luLWxlZnQ6IDhweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtcmlnaHRJY29ucy04MXJvQiB7XFxuICAgIHdpZHRoOiAyMHB4O1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25BY3RpdmUtMU1WdXcge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVNb2JpbGVDb250YWluZXItM1hrZzMge1xcbiAgICAvLyBoZWlnaHQ6IDIwJTtcXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xcbiAgICBtYXJnaW46IDAgMCAwIDE2cHg7XFxuICB9XFxuXFxuICAuUHJpY2luZ1ZpZXdEZXRhaWxzLWljb24tVVhhSFIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVJY29uLV9OVVRSIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtaGVhZGVyQ29udGFpbmVyLTNMbWtxIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5QcmljaW5nVmlld0RldGFpbHMtc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXItMkQ1Sngge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVzRmxleC0zcDdiTCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvUHJpY2luZ1ZpZXdEZXRhaWxzL1ByaWNpbmdWaWV3RGV0YWlscy5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGFBQWE7RUFDYixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLHFDQUFxQztFQUNyQyxtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztDQUNmOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQiwwQkFBMEI7RUFDMUIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkNBQTJDO0VBQzNDLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsZUFBZTtFQUNmLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7TUFDbEIsd0JBQXdCO0VBQzVCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsbUJBQW1CO0VBQ25CLDJDQUEyQztFQUMzQyx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixxQ0FBcUM7RUFDckMsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQix1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQix3QkFBd0I7RUFDeEIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGFBQWE7RUFDYixxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQix3REFBd0Q7VUFDaEQsZ0RBQWdEO0VBQ3hELDBCQUEwQjtFQUMxQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtNQUNqQixhQUFhO0VBQ2pCLHVCQUF1QjtLQUNwQixvQkFBb0I7Q0FDeEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztDQUNmOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtNQUNqQixhQUFhO0VBQ2pCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsWUFBWTtDQUNiOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxtQkFBbUI7TUFDZixxQkFBcUI7RUFDekIsa0NBQWtDO0NBQ25DOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxtQkFBbUI7TUFDZixxQkFBcUI7RUFDekIsa0JBQWtCO0VBQ2xCLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLHFCQUFxQjtNQUNqQixhQUFhO0VBQ2pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLG1DQUFtQztDQUNwQzs7QUFFRDs7Ozs7O0dBTUc7O0FBRUg7Ozs7O0lBS0k7O0FBRUo7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHFCQUFxQjtNQUNqQixhQUFhO0VBQ2pCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxxQkFBcUI7TUFDakIsYUFBYTtFQUNqQixvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtDQUN2Qjs7QUFFRDs7O0dBR0c7O0FBRUg7RUFDRSx1QkFBdUI7RUFDdkIsV0FBVztFQUNYLGFBQWE7Q0FDZDs7QUFFRDs7Ozs7R0FLRzs7QUFFSDtFQUNFLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osYUFBYTtFQUNiLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGNBQWM7RUFDZCxhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxnQkFBZ0I7TUFDWixZQUFZO0NBQ2pCOztBQUVEO0VBQ0UsZ0JBQWdCO01BQ1osWUFBWTtFQUNoQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxnQkFBZ0I7TUFDWixZQUFZO0VBQ2hCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsa0NBQWtDO0NBQ25DOztBQUVEO0VBQ0UsVUFBVTtDQUNYOztBQUVEOzs7O0lBSUk7O0FBRUo7RUFDRTtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDs7TUFFSTs7RUFFSjtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsdUJBQXVCO0dBQ3hCOztFQUVEO0lBQ0UsZ0JBQWdCO0dBQ2pCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsc0JBQXNCO0dBQ3ZCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLHNCQUFzQjtHQUN2Qjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLDJCQUEyQjtHQUM1Qjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLHlCQUF5QjtZQUNqQixpQkFBaUI7SUFDekIsVUFBVTtJQUNWLFdBQVc7SUFDWCxnQ0FBZ0M7SUFDaEMsNkJBQTZCO0dBQzlCOztFQUVEOztNQUVJOztFQUVKO0lBQ0UsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxhQUFhO0lBQ2IscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwwQkFBMEI7UUFDdEIsOEJBQThCO0lBQ2xDLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLFdBQVc7SUFDWCxVQUFVO0lBQ1YsOEJBQThCO0lBQzlCLGdDQUFnQztJQUNoQyxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtHQUN2Qjs7RUFFRDtJQUNFLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLDBCQUEwQjtJQUMxQixxQkFBcUI7SUFDckIsY0FBYztJQUNkLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0Isc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3Qjs7RUFFRDtJQUNFLCtCQUErQjtJQUMvQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFdBQVc7R0FDWjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLFdBQVc7R0FDWjs7RUFFRDs7TUFFSTs7RUFFSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BMkJJOztFQUVKOzs7TUFHSTs7RUFFSjs7O01BR0k7O0VBRUo7Ozs7Ozs7Ozs7OztNQVlJOztFQUVKO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLDJCQUEyQjtJQUMzQixhQUFhO0lBQ2IsdUJBQXVCO0dBQ3hCOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwwQkFBMEI7UUFDdEIsOEJBQThCO0dBQ25DOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwwQkFBMEI7UUFDdEIsOEJBQThCO0dBQ25DOztFQUVEO0lBQ0UscUJBQXFCO1FBQ2pCLDRCQUE0QjtJQUNoQyxhQUFhO0lBQ2IsYUFBYTtJQUNiLG9CQUFvQjtJQUNwQixxQ0FBcUM7SUFDckMsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLG9CQUFvQjtHQUNyQjs7RUFFRDs7O01BR0k7O0VBRUo7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1Qix1QkFBdUI7UUFDbkIsb0JBQW9CO0dBQ3pCOztFQUVEO0lBQ0UsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0dBQ2hCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCx3QkFBd0I7SUFDeEIsb0JBQW9CO1FBQ2hCLHNCQUFzQjtHQUMzQjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDtJQUNFLGNBQWM7R0FDZjs7RUFFRDs7TUFFSTs7RUFFSjtJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixzQkFBc0I7R0FDdkI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0VBRUQ7SUFDRSxjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsY0FBYztHQUNmOztFQUVEO0lBQ0UsY0FBYztHQUNmO0NBQ0ZcIixcImZpbGVcIjpcIlByaWNpbmdWaWV3RGV0YWlscy5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5wcmljaW5nSGVhZGVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBwYWRkaW5nLXRvcDogNSU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5wcmljZV90aXRsZSB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgZm9udC1zaXplOiA1NnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5wcmljZV9jb250ZW50IHtcXG4gIHdpZHRoOiA1MCU7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi53aG9BcmVZb3Uge1xcbiAgbWFyZ2luOiA3MnB4IDAgMjRweCAwO1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uYWRtaW5Db250YWluZXIge1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDIyNnB4O1xcbiAgaGVpZ2h0OiA4MnB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xcbiAgcGFkZGluZzogMTBweCAxMHB4IDIwcHggMjBweDtcXG4gIGJvcmRlcjogc29saWQgMXB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi50YWJzQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbjogMjRweCAwIDcycHggMDtcXG59XFxuXFxuLmFkbWluTGFiZWxDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4uYWRtaW5MYWJlbCB7XFxuICAvLyB3aWR0aDogMTE5cHg7XFxuICBoZWlnaHQ6IDI0cHg7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW46IDE5cHggMzhweCA4cHggOHB4O1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5pbWdDb250YWluZXIge1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxuICBtYXJnaW46IDEwcHggOHB4IDAgMDtcXG4gIHBhZGRpbmc6IDEwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE5NiwgMTk2LCAxOTYsIDAuMyk7XFxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xcbn1cXG5cXG4uYWRtaW5JbWcge1xcbiAgd2lkdGg6IDIwcHg7XFxuICBoZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4ucGxhbmUge1xcbiAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4ubW9kdWxlQ29udGFpbmVyIHtcXG4gIHdpZHRoOiAzMCU7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBwYWRkaW5nOiAxMnB4IDMwcHg7XFxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbn1cXG5cXG4ubW9kdWxlQ29udGFpbmVyOmxhc3QtY2hpbGQge1xcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiAwO1xcbn1cXG5cXG4ubW9kdWxlQ29udGFpbmVyQWN0aXZlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5pY29uQWN0aXZlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxufVxcblxcbi5oZWFkZXJDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLy8gaGVpZ2h0OiA0OHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxufVxcblxcbi5pY29uIHtcXG4gIHdpZHRoOiAyNHB4O1xcbiAgaGVpZ2h0OiAyNHB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5tb2R1bGVOYW1lIHtcXG4gIG1hcmdpbjogMCAwIDAgOHB4O1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjI4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLm1vZHVsZU5hbWVBY3RpdmUge1xcbiAgY29sb3I6ICNmZmY7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uYWRtaW5Db250YWluZXJBY3RpdmUge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDIyNnB4O1xcbiAgaGVpZ2h0OiA4MnB4O1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xcbiAgcGFkZGluZzogMTBweCAxMHB4IDAgMjBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDE2cHggMCByZ2JhKDI1NSwgNTEsIDEwMiwgMC4xNik7XFxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAxNnB4IDAgcmdiYSgyNTUsIDUxLCAxMDIsIDAuMTYpO1xcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmOTliMztcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLnJpZ2h0SWNvbiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDhweDtcXG4gIHJpZ2h0OiA4cHg7XFxuICB3aWR0aDogMTZweDtcXG4gIGhlaWdodDogMTZweDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uYWN0aXZlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLnN1Yk1vZHVsZXMge1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgLW1zLWZsZXgtcG9zaXRpdmU6IDA7XFxuICAgICAgZmxleC1ncm93OiAwO1xcbiAgbWFyZ2luOiAwIDAgMjRweCAwO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uc3ViTW9kdWxlQWN0aXZlIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uaWNvbkRhdGFDb250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogcm93O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogcm93O1xcbiAgLy8gbWFyZ2luLWxlZnQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4uaWNvbkRhdGFDb250YWluZXIxIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IHJvdztcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHJvdztcXG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xcbiAgLy8gbWFyZ2luLWJvdHRvbTogOHB4O1xcbn1cXG5cXG4uZGF0YUNvbnRhaW5lciB7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMS41NztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLmljb25zIHtcXG4gIHdpZHRoOiAxOHB4O1xcbiAgaGVpZ2h0OiAxOHB4O1xcbiAgbWFyZ2luOiA2cHggMTJweCAwIDA7XFxufVxcblxcbi5pY29uQ29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICAvLyBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xcbn1cXG5cXG4vKiAuZGF0YUljb25Db250YWluZXIge1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XFxuICBjb2x1bW4tZ2FwOiAzcmVtO1xcbiAgcm93LWdhcDogM3JlbTtcXG59XFxuICovXFxuXFxuLyogLnZlcnRpY2FsTGluZSB7XFxuICBoZWlnaHQ6IDQyMHB4O1xcbiAgd2lkdGg6IDFweDtcXG4gIG1hcmdpbjogNjNweCAxMHB4IDAgMDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNkOWQ5ZDk7XFxufSAqL1xcblxcbi5zdHVkZW50TW9kdWxlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBvc2l0aXZlOiAwO1xcbiAgICAgIGZsZXgtZ3JvdzogMDtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5zZWN0aW9uTmFtZSB7XFxuICAtbXMtZmxleC1wb3NpdGl2ZTogMDtcXG4gICAgICBmbGV4LWdyb3c6IDA7XFxuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjMzO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uc3ViTW9kdWxlc0ZsZXgge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgbWFyZ2luOiAzMnB4IDAgMzJweCAwO1xcbn1cXG5cXG4vKiAuc3Vic3VibW9kdWxlcyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG4gKi9cXG5cXG4ubW9kdWxlRmxleCB7XFxuICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgd2lkdGg6IDg3JTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLyogLm1vZHVsZUZsZXgxIHtcXG4gIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBtYXJnaW46IGF1dG87XFxufVxcbiAqL1xcblxcbi5tb2R1bGVzIHtcXG4gIHBhZGRpbmc6IDAgNjRweCAwIDY0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1hcmdpbjogYXV0bztcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi5hcnJvd0ljb24ge1xcbiAgd2lkdGg6IDEwLjlweDtcXG4gIGhlaWdodDogMTZweDtcXG59XFxuXFxuLnN1Yk1vZHVsZUNvbnRhaW5lciB7XFxuICAtbXMtZmxleDogMC4zIDE7XFxuICAgICAgZmxleDogMC4zIDE7XFxufVxcblxcbi5zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lciB7XFxuICAtbXMtZmxleDogMC40IDE7XFxuICAgICAgZmxleDogMC40IDE7XFxuICAvLyB3aWR0aDogMzAlO1xcbn1cXG5cXG4uc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXIxIHtcXG4gIC1tcy1mbGV4OiAwLjMgMTtcXG4gICAgICBmbGV4OiAwLjMgMTtcXG4gIC8vIHdpZHRoOiAyNSU7XFxuICBtYXJnaW46IDUwcHggMCAwIDA7XFxuICAvLyBib3JkZXItbGVmdDogMXB4IHNvbGlkICNkOWQ5ZDk7XFxufVxcblxcbi5pbWFnZUNvbnRhaW5lciB7XFxuICB3aWR0aDogOSU7XFxufVxcblxcbi8qIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyODBweCkge1xcbiAgLm1vZHVsZUZsZXh7XFxuICAgIHdpZHRoOiA1My41JTtcXG4gIH1cXG59ICovXFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTFweCkge1xcbiAgLnBsdXMge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLm1vZHVsZUljb24ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAucGx1c0NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAvKiAuaGVhZGVyQ29udGFpbmVyTW9iaWxlVmlldyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAuaGVhZGVyQ29udGFpbmVycyB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuZ29CYWNrTGluayB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkwcHgpIHtcXG4gIC5tb2R1bGVzIHtcXG4gICAgcGFkZGluZzogMDtcXG4gIH1cXG5cXG4gIC5wcmljaW5nSGVhZGVyIHtcXG4gICAgbWFyZ2luOiAyNHB4IDAgMCAwO1xcbiAgICBwYWRkaW5nOiAwIDY0cHggMCA2NHB4O1xcbiAgfVxcblxcbiAgLnByaWNlX3RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAzMnB4O1xcbiAgfVxcblxcbiAgLnByaWNlX2NvbnRlbnQge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBsaW5lLWhlaWdodDogMS43MTtcXG4gICAgbWFyZ2luOiAxMnB4IDAgNDhweCAwO1xcbiAgfVxcblxcbiAgLndob0FyZVlvdSB7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbWFyZ2luOiA0OHB4IDAgMjRweCAwO1xcbiAgfVxcblxcbiAgLmdvQmFja0xpbmsge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgY29sb3I6ICNmMzY7XFxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbiAgfVxcblxcbiAgLmFkbWluQ29udGFpbmVyQWN0aXZlIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgICB3aWR0aDogNTAlO1xcbiAgICBoZWlnaHQ6IDY1cHg7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcXG4gICAgYm9yZGVyLXN0eWxlOiBub25lO1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XFxuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogOHB4O1xcbiAgICAvLyBwYWRkaW5nOiAwIDQ5cHggMjRweCA1MHB4O1xcbiAgfVxcblxcbiAgLyogLmFkbWluQ29udGFpbmVyQWN0aXZlOnNlY29uZC1jaGlsZCB7XFxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDhweDtcXG4gIH0gKi9cXG5cXG4gIC5hZG1pbkNvbnRhaW5lciB7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgd2lkdGg6IDUwJTtcXG4gICAgaGVpZ2h0OiA2NXB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogZGlzdHJpYnV0ZTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbiAgICBib3JkZXItc3R5bGU6IG5vbmU7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgICBtYXJnaW46IDA7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAucmlnaHRJY29uIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5pbWdDb250YWluZXIge1xcbiAgICB3aWR0aDogMTAlO1xcbiAgICBoZWlnaHQ6IDA7XFxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgICAvLyBwYWRkaW5nOiAyNHB4IDQ5cHggMjRweCA1MHB4O1xcbiAgICAvLyBtYXJnaW4tbGVmdDogNDBweDtcXG4gICAgLy8gcGFkZGluZzogMDtcXG4gICAgLy8gbWFyZ2luOiAxOXB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLmFkbWluTGFiZWwge1xcbiAgICAvLyB3aWR0aDowcHg7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW46IDE5cHggMCAwIDA7XFxuICB9XFxuXFxuICAubW9kdWxlRGF0YSB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgfVxcblxcbiAgLnRhYnNDb250YWluZXIge1xcbiAgICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIG1hcmdpbjogMCAwIDIwcHggMDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIHBhZGRpbmc6IDA7XFxuICB9XFxuXFxuICAucGxhbmUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLm1vZHVsZUZsZXgge1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgfVxcblxcbiAgLyogLmhlYWRlckNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9ICovXFxuXFxuICAvKiAgLm1vYmlsZU1PZHVsZUNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgICBwYWRkaW5nOiAwIDAgMCAxNnB4O1xcbiAgfVxcblxcbiAgLm1vYmlsZU1PZHVsZUNvbnRhaW5lckFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmOTliMztcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjVmNztcXG4gICAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICAgIHBhZGRpbmc6IDAgMCAwIDE2cHg7XFxuICB9ICovXFxuXFxuICAvKiAgLnBsdXMge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLyogLmltYWdlTW9iaWxlVXJsIHtcXG4gICAgd2lkdGg6IDEwJTtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfSAqL1xcblxcbiAgLyogLm1vYmlsZU1vZHVsZU5hbWUge1xcbiAgICB3aWR0aDogNzUlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfVxcblxcbiAgLm1vYmlsZU1vZHVsZU5hbWVBY3RpdmUge1xcbiAgICB3aWR0aDogNzUlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgfSAqL1xcblxcbiAgLmhlYWRlckNvbnRhaW5lcnMge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgYm9yZGVyOiBub25lO1xcbiAgICAvLyBtYXJnaW46IDEwMHB4IDAgMCAwO1xcbiAgfVxcblxcbiAgLmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICB9XFxuXFxuICAuYWRtaW5MYWJlbENvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxuICB9XFxuXFxuICAubW9kdWxlQ29udGFpbmVyIHtcXG4gICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgIHdpZHRoOiAzMjhweDtcXG4gICAgaGVpZ2h0OiA0NHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCByZ2JhKDAsIDAsIDAsIDAuMik7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbjogMCAwIDhweCAwO1xcbiAgfVxcblxcbiAgLm1vZHVsZUNvbnRhaW5lckFjdGl2ZSB7XFxuICAgIGNvbG9yOiAjZjM2O1xcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjZmY5OWIzO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmNWY3O1xcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xcbiAgfVxcblxcbiAgLyogLmljb24ge1xcbiAgICB3aWR0aDogMTAlO1xcbiAgICBoZWlnaHQ6IDIwcHg7XFxuICB9ICovXFxuXFxuICAuYWRtaW5JbWcge1xcbiAgICB3aWR0aDogMTZweDtcXG4gICAgaGVpZ2h0OiAxNnB4O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgLm1vZHVsZU5hbWUge1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGNvbG9yOiAjMjUyODJiO1xcbiAgfVxcblxcbiAgLm1vZHVsZU5hbWVBY3RpdmUge1xcbiAgICBjb2xvcjogI2YzNjtcXG4gICAgd2lkdGg6IDkwJTtcXG4gIH1cXG5cXG4gIC5wbHVzIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGp1c3RpZnktaXRlbXM6IGZsZXgtZW5kO1xcbiAgICAtbXMtZmxleC1hbGlnbjogZW5kO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xcbiAgfVxcblxcbiAgLnN1Yk1vZHVsZUNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuZGF0YUNvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuaWNvbnMge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxuXFxuICAuc2VjdGlvbk5hbWUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLyogLnN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lciB7XFxuICAgIG1hcmdpbjogMCAwIDAgMTZweDtcXG4gIH0gKi9cXG5cXG4gIC5zdWJNb2R1bGVOYW1lIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzI1MjgyYjtcXG4gICAgbWFyZ2luOiAxNnB4IDAgMTJweCAwO1xcbiAgfVxcblxcbiAgLnN1Ym1vZHVsZURhdGFDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgbWFyZ2luOiAwIDAgOHB4IDA7XFxuICB9XFxuXFxuICAuc3VibW9kdWxlRGF0YSB7XFxuICAgIGZvbnQtc2l6ZTogMTJweDtcXG4gICAgY29sb3I6ICMyNTI4MmI7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgIGxpbmUtaGVpZ2h0OiAxLjY3O1xcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgfVxcblxcbiAgLnJpZ2h0SWNvbnMge1xcbiAgICB3aWR0aDogMjBweDtcXG4gICAgaGVpZ2h0OiAyMHB4O1xcbiAgfVxcblxcbiAgLmljb25BY3RpdmUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lciB7XFxuICAgIC8vIGhlaWdodDogMjAlO1xcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XFxuICAgIG1hcmdpbjogMCAwIDAgMTZweDtcXG4gIH1cXG5cXG4gIC5pY29uIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5tb2R1bGVJY29uIHtcXG4gICAgd2lkdGg6IDIwcHg7XFxuICAgIGhlaWdodDogMjBweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJDb250YWluZXIge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLnN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5zdWJNb2R1bGVzRmxleCB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJwcmljaW5nSGVhZGVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXByaWNpbmdIZWFkZXItMmJZdi1cIixcblx0XCJwcmljZV90aXRsZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1wcmljZV90aXRsZS0ybnFGdFwiLFxuXHRcInByaWNlX2NvbnRlbnRcIjogXCJQcmljaW5nVmlld0RldGFpbHMtcHJpY2VfY29udGVudC0xZlZ0S1wiLFxuXHRcIndob0FyZVlvdVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy13aG9BcmVZb3UtM1QwbWdcIixcblx0XCJhZG1pbkNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkNvbnRhaW5lci0yOUVsUVwiLFxuXHRcInRhYnNDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtdGFic0NvbnRhaW5lci0zX1p3dVwiLFxuXHRcImFkbWluTGFiZWxDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtYWRtaW5MYWJlbENvbnRhaW5lci0yTXJvalwiLFxuXHRcImFkbWluTGFiZWxcIjogXCJQcmljaW5nVmlld0RldGFpbHMtYWRtaW5MYWJlbC0yM3dqYlwiLFxuXHRcImltZ0NvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pbWdDb250YWluZXItMUpqX3VcIixcblx0XCJhZG1pbkltZ1wiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1hZG1pbkltZy0ybUFzU1wiLFxuXHRcInBsYW5lXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXBsYW5lLTIxZWFUXCIsXG5cdFwibW9kdWxlQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUNvbnRhaW5lci1JNlNTeVwiLFxuXHRcIm1vZHVsZUNvbnRhaW5lckFjdGl2ZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVDb250YWluZXJBY3RpdmUtMkJLUXRcIixcblx0XCJpY29uQWN0aXZlXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWljb25BY3RpdmUtMU1WdXdcIixcblx0XCJoZWFkZXJDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaGVhZGVyQ29udGFpbmVyLTNMbWtxXCIsXG5cdFwiaWNvblwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pY29uLVVYYUhSXCIsXG5cdFwibW9kdWxlTmFtZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lLTNTdEZ1XCIsXG5cdFwibW9kdWxlTmFtZUFjdGl2ZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVOYW1lQWN0aXZlLTNMenF6XCIsXG5cdFwiYWRtaW5Db250YWluZXJBY3RpdmVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtYWRtaW5Db250YWluZXJBY3RpdmUtMVowSkxcIixcblx0XCJyaWdodEljb25cIjogXCJQcmljaW5nVmlld0RldGFpbHMtcmlnaHRJY29uLTJpMzJaXCIsXG5cdFwiYWN0aXZlXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWFjdGl2ZS0yY1ZrZVwiLFxuXHRcInN1Yk1vZHVsZXNcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlcy0xUjUteFwiLFxuXHRcInN1Yk1vZHVsZUFjdGl2ZVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1zdWJNb2R1bGVBY3RpdmUtMWhFcEpcIixcblx0XCJpY29uRGF0YUNvbnRhaW5lclwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pY29uRGF0YUNvbnRhaW5lci1pMkc2VFwiLFxuXHRcImljb25EYXRhQ29udGFpbmVyMVwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pY29uRGF0YUNvbnRhaW5lcjEtdXI2clpcIixcblx0XCJkYXRhQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWRhdGFDb250YWluZXItM1E3T01cIixcblx0XCJpY29uc1wiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1pY29ucy0yWFNMUlwiLFxuXHRcImljb25Db250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaWNvbkNvbnRhaW5lci0xUWNOTlwiLFxuXHRcInN0dWRlbnRNb2R1bGVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3R1ZGVudE1vZHVsZS0xbEZ4eVwiLFxuXHRcInNlY3Rpb25OYW1lXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXNlY3Rpb25OYW1lLTFXU1g1XCIsXG5cdFwic3ViTW9kdWxlc0ZsZXhcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlc0ZsZXgtM3A3YkxcIixcblx0XCJtb2R1bGVGbGV4XCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZUZsZXgtMk5vdUlcIixcblx0XCJtb2R1bGVzXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLW1vZHVsZXMtMWRTWWhcIixcblx0XCJhcnJvd0ljb25cIjogXCJQcmljaW5nVmlld0RldGFpbHMtYXJyb3dJY29uLTFJOE02XCIsXG5cdFwic3ViTW9kdWxlQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZUNvbnRhaW5lci0yRl9pS1wiLFxuXHRcInN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyLTJENUp4XCIsXG5cdFwic3VibW9kdWxlc1NlY3Rpb25Db250YWluZXIxXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Ym1vZHVsZXNTZWN0aW9uQ29udGFpbmVyMS0zSHowVFwiLFxuXHRcImltYWdlQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLWltYWdlQ29udGFpbmVyLTNDQTVxXCIsXG5cdFwicGx1c1wiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1wbHVzLTI1T3BGXCIsXG5cdFwibW9kdWxlSWNvblwiOiBcIlByaWNpbmdWaWV3RGV0YWlscy1tb2R1bGVJY29uLV9OVVRSXCIsXG5cdFwic3ViTW9kdWxlTW9iaWxlQ29udGFpbmVyXCI6IFwiUHJpY2luZ1ZpZXdEZXRhaWxzLXN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lci0zWGtnM1wiLFxuXHRcInBsdXNDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtcGx1c0NvbnRhaW5lci0zOVBxZ1wiLFxuXHRcImhlYWRlckNvbnRhaW5lcnNcIjogXCJQcmljaW5nVmlld0RldGFpbHMtaGVhZGVyQ29udGFpbmVycy0za0dHb1wiLFxuXHRcImdvQmFja0xpbmtcIjogXCJQcmljaW5nVmlld0RldGFpbHMtZ29CYWNrTGluay1tUExFQVwiLFxuXHRcIm1vZHVsZURhdGFcIjogXCJQcmljaW5nVmlld0RldGFpbHMtbW9kdWxlRGF0YS1FRHYta1wiLFxuXHRcInN1Yk1vZHVsZU5hbWVcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3ViTW9kdWxlTmFtZS0zeDFUb1wiLFxuXHRcInN1Ym1vZHVsZURhdGFDb250YWluZXJcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3VibW9kdWxlRGF0YUNvbnRhaW5lci0yMklZLVwiLFxuXHRcInN1Ym1vZHVsZURhdGFcIjogXCJQcmljaW5nVmlld0RldGFpbHMtc3VibW9kdWxlRGF0YS0xTlBMMlwiLFxuXHRcInJpZ2h0SWNvbnNcIjogXCJQcmljaW5nVmlld0RldGFpbHMtcmlnaHRJY29ucy04MXJvQlwiXG59OyIsImV4cG9ydCBjb25zdCBQbGFuRGV0YWlscyA9IHtcbiAgYWRtaW46IHtcbiAgICBsYWJlbDogJ0FkbWluL1RlYWNoZXInLFxuICAgIHZhbHVlOiAnYWRtaW4nLFxuICAgIGltZ1VybDogJy9pbWFnZXMvYWRtaW4uc3ZnJyxcbiAgICBtb2R1bGVzOiB7XG4gICAgICBvbmxpbmV0ZXN0OiB7XG4gICAgICAgIGxhYmVsOiAnT25saW5lIFRlc3RzJyxcbiAgICAgICAgdmFsdWU6ICdvbmxpbmV0ZXN0JyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL1Rlc3RzLnN2ZycsXG4gICAgICAgIG1vYmlsZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9UZXN0c01vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlVGVzdHMuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIHRlc3RzOiB7XG4gICAgICAgICAgICBsYWJlbDogJ09ubGluZSBUZXN0aW5nIFBsYXRmb3JtJyxcbiAgICAgICAgICAgIHZhbHVlOiAndGVzdHMnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7IGxhYmVsOiAnVGVzdCBDcmVhdGlvbicsIHZhbHVlOiAndGVzdGNyZWF0aW9uJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnRWRpdCBUZXN0JywgdmFsdWU6ICdlZGl0dGVzdCcgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0RlbGV0ZSBUZXN0JywgdmFsdWU6ICdkZWxldGV0ZXN0JyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnR3JhY2UgUGVyaW9kJywgdmFsdWU6ICdncmFjZXBlcmlvZCcgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQXNzaWduIFF1ZXN0aW9uIFBhcGVyIHRvIFRlYWNoZXJzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Fzc2lnbnF1ZXN0aW9ucGFwZXJ0b3RlYWNoZXInLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdVcGxvYWQgeW91ciBvd24gUXVlc3Rpb24gUGFwZXIgaW4gLmRvY3gnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndXBsb2FkeW91cm93bnF1ZXN0aW9ucGFwZXJpbmRvY3gnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdVcGxvYWQgeW91ciBvd24gcXVlc3Rpb24gcGFwZXIgaW4gNSBtaW4nLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndXBsb2FkeW91cm93bnF1ZXN0aW9ucGFwZXJpbjVtaW4nLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnU3VwcG9ydHMgYW55IHR5cGUgb2YgcXVlc3Rpb25zIChzaW5nbGUgYW5zd2VyLCBtdWx0aXBsZSBhbnN3ZXIsIHBhcmFncmFwaCBldGMuLCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzdXBwb3J0c2FueXR5cGVvZnF1c3Rpb25zJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1BhcnRpYWwgUGFwZXInLCB2YWx1ZTogJ3BhcnRpYWxwYXBlcicgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUHJldmlldyBsaWtlIFRlc3QgUGxhdGZvcm0nLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncHJldmlld2xpa2V0ZXN0cGxhdGZvcm0nLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnUHJvY3RvcmluZycsIHZhbHVlOiAncHJvY3RvcmluZycgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnTGl2ZSBBdHRlbmRhbmNlIC8gQXR0ZW5kYW5jZScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdsaXZlYXR0ZW5kYW5jZWF0dGVuZGFuY2UnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEb3dubG9hZGFibGUgYXR0ZW5kYW5jZSBzaGVldHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZG93bmxvYWRhYmxlYXR0ZW5kYW5jZXNoZWV0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Rvd25sb2FkYWJsZSBxdWVzdGlvbiBwYXBlciwga2V5IGFuZCBzb2x1dGlvbnMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZG93bmxvYWRxdWVzdGlvbnBhcGVya2V5c29sdXRpb25zJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0tleSBFZGl0aW5nJywgdmFsdWU6ICdrZXllZGl0aW5nJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBdXRvIFJlcG9ydHMgJiBBbmFseXNpcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhdXRvcmVwb3J0c2FuYWx5c2lzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVzdWx0cyBnZW5lcmF0aW9uIGZvciBPZmZsaW5lIGV4YW0nLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncmVzdWx0c2dlbmVyYXRpb25mb3JvZmZsaW5lZXhhbXMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdXBwb3J0cyBPbmxpbmUgLyBPZmZsaW5lIHRlc3RzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3N1cHBvcnRzb25saW5lb2ZmbGluZXRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0ZsZXhpYmxlIHRlc3QgdGltZXMnLCB2YWx1ZTogJ2ZsZXhpYmxldGVzdHRpbWVzICcgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Jlc3QgSGllcmFyY3kgc3lzdGVtJywgdmFsdWU6ICdiZXN0aGllcmFyY3lzeXN0ZW0nIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1BhcnNlciBjYW4gY29udmVydCB0ZXh0IC8gaW1hZ2VzIC8gZXF1YXRpb25zIC8gZ3JhcGhzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3BhcnNlcmNhbmNvbnZlcnR0ZXh0aW1hZ2VzZXF1YXRpb25zZ3JhcGhzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBxdXN0aW9uYmFuazoge1xuICAgICAgICAgICAgbGFiZWw6ICdRdWVzdGlvbiBCYW5rJyxcbiAgICAgICAgICAgIHZhbHVlOiAncXVzdGlvbmJhbmsnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnUXVlc3Rpb24gYmFuayB3aXRoIDEsNTAsMDAwKyBRdWVzdGlvbnMgd2l0aCBLZXkgYW5kIFNvbHV0aW9ucycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdxdWVzdGlvbmJhbmt3aXRoMSw1MCwwMDArcXVlc3Rpb25zd2l0aGtleWFuZHNvbHV0aW9ucycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDpcbiAgICAgICAgICAgICAgICAgICdRdWVzdGlvbnMgaW4gMyBkaWZmaWN1bHR5IGxldmVscyAoRWFzeSAvIE1lZGl1bSAvIEhpZ2gpJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ1F1ZXN0aW9uc2luM2RpZmZpY3VsdHlsZXZlbHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnUXVlc3Rpb24gdGFnZ2luZycsIHZhbHVlOiAncXVlc3Rpb250YWdnaW5nJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHBhcGVyczoge1xuICAgICAgICAgICAgbGFiZWw6ICdQYXBlcnMnLFxuICAgICAgICAgICAgdmFsdWU6ICdwYXBlcnMnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7IGxhYmVsOiAnQ3JlYXRlIFF1ZXN0aW9uIFBhcGVyJywgdmFsdWU6ICdjcmVhdGVxdWVzdGlvbnBhcGVyJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU2VsZWN0IHF1ZXN0aW9ucycsIHZhbHVlOiAnc2VsZWN0cXVlc3Rpb25zJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdRdWVzdGlvbiBwYXBlciByZXN1bWUgZmVhdHVyZScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdxdWVzdGlvbnBhcGVycmVzdW1lZmVhdHVyZScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdQYXJ0aWFsIFBhcGVyIGZlYXR1cmUnLCB2YWx1ZTogJ3BhcnRpYWxwYXBlcmZlYXR1cmUnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDpcbiAgICAgICAgICAgICAgICAgICdEZXNpZ24gcGVyc29uYWxpemVkIHF1ZXN0aW9uIHBhcGVyL3Mgd2l0aCBJbnN0aXR1dGUgTmFtZSwgTG9nbyAmIFdhdGVybWFyaycsXG4gICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAnZGVzaWducGVyc29uYWxpemVkcXVlc3Rpb25wYXBlci9zd2l0aGluc3RpdHV0ZW5hbWVsb2dvJndhdGVybWFyaycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgcHJhY3RpY2U6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUHJhY3RpY2UnLFxuICAgICAgICAgICAgdmFsdWU6ICdwcmFjdGljZScsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1VubGltaXRlZCBQcmFjdGljZSB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd1bmxpbWl0ZWRwcmFjdGljZXRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb25zIGJhc2VkIHByYWN0aWNlJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3F1ZXN0aW9uc2Jhc2VkcHJhY3RpY2UnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnVGltZSBiYXNlZCBQcmFjdGljZScsIHZhbHVlOiAndGltZWJhc2VkcHJhY3RpY2UnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RldGFpbGVkIGFuYWx5c2lzIG9mIGFsbCBwcmFjdGljZSB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdkZXRhaWxlZGFuYWx5c2lzb2ZhbGxwcmFjdGljZXRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmFseXNpczoge1xuICAgICAgICAgICAgbGFiZWw6ICdBbmFseXNpcycsXG4gICAgICAgICAgICB2YWx1ZTogJ2FuYWx5c2lzJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnTWFya3MgQW5hbHlzc2lzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ21hcmthbmFseXNzaXMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUb3BpYyB3aXNlIEFuYWx5c2lzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3RvcGljd2lzZWFuYWx5c2lzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Vycm9yIEFuYWx5c2lzJywgdmFsdWU6ICdlcnJvcmFuYWx5c2lzJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHJlcG9ydHM6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUmVwb3J0cycsXG4gICAgICAgICAgICB2YWx1ZTogJ3JlcG9ydHMnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUZXN0IFJlc3VsdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndGVzdHJlc3VsdHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDX1dfVSBSZXBvcnQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnY3djcmVwb3J0JyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnTWFya3MgRGlzdHJpYnV0aW9uJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ21hcmtzZGlzdHJpYnV0aW9uJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRXJyb3IgQ291bnQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZXJyb3Jjb3VudCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdXZWFrIFN1YmplY3RzJywgdmFsdWU6ICdXZWFrc3ViamVjdCcgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBsaXZlY2xhc3Nlczoge1xuICAgICAgICBsYWJlbDogJ0xpdmUgY2xhc3NlcycsXG4gICAgICAgIHZhbHVlOiAnbGl2ZWNsYXNzZXMnLFxuICAgICAgICBpbWFnZVVybDogJy9pbWFnZXMvTGl2ZS5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvTGl2ZU1vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlTGl2ZS5zdmcnLFxuICAgICAgICBzdWJNb2R1bGVzOiB7XG4gICAgICAgICAgbGl2ZToge1xuICAgICAgICAgICAgbGFiZWw6ICdMaXZlJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbGl2ZScsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDpcbiAgICAgICAgICAgICAgICAgICdJbnRlcnJhY3RpdmUgTGl2ZSBDbGFzcyBDcmVhdGlvbiAoaW50ZWdyYXRlZCB3aXRoIHpvb20pJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2ludGVycmFjdGl2ZWxpdmVjbGFzc2NyZWF0aW9uJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVjb3JkaW5nIENsYXNzIGNyZWF0aW9uJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3JlY29yZGluZ2NsYXNzY3JlYXRpb24nLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnUmVjdXJyaW5nIGNsYXNzZXMnLCB2YWx1ZTogJ3JlY3VycmluZ2NsYXNzZXMnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0F0dGFjaCBmaWxlIHdoaWxlIGNsYXNzIGNyZWF0aW5nIChpbWFnZSAvIHBkZiknLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnYXR0YWNoZmlsZXdoaWxlY2xhc3NjcmVhdGluZycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdFZGl0IENsYXNzJywgdmFsdWU6ICdlZGl0Y2xhc3MnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdEZWxldGUgQ2xhc3MnLCB2YWx1ZTogJ2RlbGV0ZWNsYXNzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnTGl2ZSBBdHRlbmRhbmNlJywgdmFsdWU6ICdsaXZlYXR0ZW5kYW5jZScgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1dhdGNoIGxhdGVyJywgdmFsdWU6ICd3YXRjaGxhdGVyJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU21hcnQgRmlsdGVycycsIHZhbHVlOiAnc21hcnRmaWx0ZXJzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnRG93bmxvYWQgY2xhc3NlcyBsaXN0JywgdmFsdWU6ICdkb3dubG9hZGNsYXNzZXNsaXN0JyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZWN1cmUgTm90ZXMsIERvd25sb2FkYWJsZSBsaW5rcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzZWN1cmVub3Rlc2Rvd25sb2FkYWJsZWxpbmtzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3R1ZGVudCBNYW5hZ2VtZW50IFN5c3RlbScsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdzdHVkZW50bWFuYWdlbWVudHN5c3RlbScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdCZXN0IEhpZXJhcmN5IHN5c3RlbScsIHZhbHVlOiAnYmVzdGhpZXJhcmN5c3lzdGVtJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHNlY3VyZWNvbnRlbnQ6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnUmVzdHJpY3RlZCBMb2dpbicsXG4gICAgICAgICAgICB2YWx1ZTogJ3NlY3VyZWNvbnRlbnQnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDb250ZW50IHZpc2libGUgb25seSBpbnNpZGUgQXBwJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2NvbnRlbnR2aXNpYmxlb25seWluc2lkZWFwcCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1ZpZGVvIExpbmsgc2hhcmluZyBkaXNhYmxlZCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd2aWRlb2xpbmtzaGFyaW5nZGlzYWJsZWQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTY3JlZW4gUmVjb3JkaW5nIERpc2FibGVkJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3NjcmVlbnJlY29yZGluZ2Rpc2FibGVkJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1NjcmVlbnNob3QgRGlzYWJsZWQnLCB2YWx1ZTogJ3NjcmVlbnNob3RkaXNhYmxlZCcgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnZU5vdGVzIERvd25sb2FkaW5nIEVuYWJsZWQvRGlzYWJsZWQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZW5vdGVzZG93bmxvYWRpbmdlbmFibGVkZGlzYWJsZWQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnT25seSBSZWdpc3RlcmVkIFN0dWRlbnRzIGNhbiBlbnRlciB3aXRoIGtub3duIGlkZW50aXR5LCBzbyBubyBudWlzYW5jZSBkdXJpbmcgY2xhc3MgdW5saWtlIFpvb20nLFxuICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgJ29ubHlyZWdpc3RlcmVkc3R1ZGVudHNjYW5lbnRlcndpdGhrbm93bmlkZW50aXR5c29ub251aXNhbmNlZHVyaW5nY2xhc3N1bmxpa2V6b29tJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBhc3NpZ25tZW50czoge1xuICAgICAgICBsYWJlbDogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgdmFsdWU6ICdhc3NpZ25tZW50cycsXG4gICAgICAgIGltYWdlVXJsOiAnL2ltYWdlcy9Bc3NpZ25tZW50cy5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvQXNzaWdubWVudHNNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZUFzc2lnbm1lbnRzLnN2ZycsXG4gICAgICAgIHN1Yk1vZHVsZXM6IHtcbiAgICAgICAgICBhc3NpZ25tZW50OiB7XG4gICAgICAgICAgICBsYWJlbDogJ0Fzc2lnbm1lbnRzJyxcbiAgICAgICAgICAgIHZhbHVlOiAnYXNzaWdubWVudCcsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vhc3kgQ3JlYXRpb24gb2YgYXNzaWdubWVudCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdlYXN5Y3JlYXRpb25vZmFzc2lnbm1lbnQnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU3VibWlzc2lvbiBzdGF0dXMnLCB2YWx1ZTogJ3N1Ym1pc3Npb25zdGF0dXMnIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdBd2FyZGluZyBNYXJrcycsIHZhbHVlOiAnYXdhcmRpbmdtYXJrcycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Vhc3kgdG8gRXZhbHVhdGUnLCB2YWx1ZTogJ2Vhc3l0b2V2YWx1YXRlJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnSGVscGZ1bCBUb29sYm94JywgdmFsdWU6ICdoZWxwZnVsdG9vbGJveCcgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQW55dGltZSwgQW55d2hlcmUgQXNzaWdubWVudHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnYW55dGltZWFueXdoZXJlYXNzaWdubWVudHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdHcmFkZSBGYXN0ZXIgd2l0aCBFYXN5LXRvLVVzZSBUb29scycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdncmFkZWZhc3RlcndpdGhlYXN5dG91c2V0b29scycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgZG91YnRzOiB7XG4gICAgICAgIGxhYmVsOiAnRG91YnRzJyxcbiAgICAgICAgdmFsdWU6ICdkb3VidHMnLFxuICAgICAgICBpbWFnZVVybDogJy9pbWFnZXMvRG91YnRzLnN2ZycsXG4gICAgICAgIG1vYmlsZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9Eb3VidHNNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZURvdWJ0cy5zdmcnLFxuICAgICAgICBzdWJNb2R1bGVzOiB7XG4gICAgICAgICAgZG91YnQ6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnRG91YnRzJyxcbiAgICAgICAgICAgIHZhbHVlOiAnZG91YnQnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnRWFzeSB0byBjbGVhciBkb3VidHMgKENoYXQgLyB1cGxvYWQgaW1hZ2UgLyBpbnNlcnQgbGluayknLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZWFzeXRvY2xlYXJkb3VidHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQm9va21hcmsgZG91YnRzJywgdmFsdWU6ICdib29rbWFya2RvdWJ0cycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1Jlb3BlbiBkb3VidHMnLCB2YWx1ZTogJ3Jlb3BlbmRvdWJ0cycgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0NoZWNrIGxhdGVyJywgdmFsdWU6ICdjaGVja2xhdGVyJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU21hcnQgRmlsdGVycycsIHZhbHVlOiAnc21hcnRmaWx0ZXJzJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIGNvbm5lY3Q6IHtcbiAgICAgICAgbGFiZWw6ICdDb25uZWN0JyxcbiAgICAgICAgdmFsdWU6ICdjb25uZWN0JyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0Nvbm5lY3Quc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0Nvbm5lY3RNb2JpbGUuc3ZnJyxcbiAgICAgICAgYWN0aXZlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL2FjdGl2ZUNvbm5lY3Quc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIHNjaGVkdWxlOiB7XG4gICAgICAgICAgICBsYWJlbDogJ1NjaGVkdWxlJyxcbiAgICAgICAgICAgIHZhbHVlOiAnc2NoZWR1bGUnLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgbm90aWZpY2F0aW9uczoge1xuICAgICAgICAgICAgbGFiZWw6ICdOb3RpZmljYXRpb25zJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbm90aWZpY2F0aW9ucycsXG4gICAgICAgICAgfSxcbiAgICAgICAgICBtZXNzYW5nZXI6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnTWVzc2FuZ2VyJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbWVzc2FuZ2VyJyxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICB9LFxuICBzdHVkZW50OiB7XG4gICAgbGFiZWw6ICdTdHVkZW50JyxcbiAgICB2YWx1ZTogJ3N0dWRlbnQnLFxuICAgIGltZ1VybDogJy9pbWFnZXMvc3R1ZGVudC5zdmcnLFxuICAgIG1vZHVsZXM6IHtcbiAgICAgIG9ubGluZXRlc3Q6IHtcbiAgICAgICAgbGFiZWw6ICdPbmxpbmUgVGVzdHMnLFxuICAgICAgICB2YWx1ZTogJ29ubGluZXRlc3QnLFxuICAgICAgICBpbWFnZVVybDogJy9pbWFnZXMvVGVzdHMuc3ZnJyxcbiAgICAgICAgbW9iaWxlSW1hZ2VVcmw6ICcvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL1Rlc3RzTW9iaWxlLnN2ZycsXG4gICAgICAgIGFjdGl2ZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9hY3RpdmVUZXN0cy5zdmcnLFxuICAgICAgICBzdWJNb2R1bGVzOiB7XG4gICAgICAgICAgdGVzdHM6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnVGVzdHMnLFxuICAgICAgICAgICAgdmFsdWU6ICd0ZXN0cycsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vhc3kgdG8gY2hlY2sgVXBjb21pbmcgVGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnZWFzeXRvY2hlY2t1cGNvbWluZ3Rlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Vhc3kgdG8gYXR0ZW1wdCcsIHZhbHVlOiAnZWFzeXRvYXR0ZW1wdCcgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ0Fuc3dlcmVkIC8gTm90IEFuc3dlcmVkIC8gTm90IHZpc2l0ZWQgbGlzdHMgY2FuIGJlIGNoZWNrZWQgZWFzaWx5JyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ0Fuc3dlcmVkbm90YW5zd2VyZWRub3R2aXNpdGVkbGlzdHNjYW5iZWNoZWNrZWRlYXNpbHknLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6XG4gICAgICAgICAgICAgICAgICAnU3ViamVjdCB3aXNlIHF1ZXN0aW9ucyByZXNwb25zZSBjYW4gYmUgY2hlY2tlZCBlYXNpbHkgYnkgY29sb3VycycsXG4gICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAnc3ViamVjdHdpc2VxdWVzdGlvbnNyZXNwb25zZWNhbmJlY2hlY2tlZGVhc2lseWJ5Y29sb3VycycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdJbnN0YW50IFJlc3VsdHMnLCB2YWx1ZTogJ2luc3RhbnRyZXN1bHRzJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFuYWx5c2lzOiB7XG4gICAgICAgICAgICBsYWJlbDogJ0FuYWx5c2lzJyxcbiAgICAgICAgICAgIHZhbHVlOiAnYW5hbHlzaXMnLFxuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdPdmVyYWxsIFRlc3QgUGVyZm9ybWFuY2UnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnb3ZlcmFsbHRlc3RwZXJmb3JtYW5jZScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0F0dGVtcHQgJiBBY2N1cmFjeSByYXRlIGluIGFsbCB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhdHRlbXB0YWNjdXJhY3lyYXRlaW5hbGx0ZXN0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1Rlc3RzIFBlcmZvcm1hbmNlIGluIGFsbCB0b3BpY3MnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAndGVzdHNwZXJmb3JtYW5jZWluYWxsdG9waWNzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnT3ZlcmFsbCAvIFN1YmplY3Qgd2lzZSBNYXJrcyBUcmVuZCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdvdmVyYWxsc3ViamVjdHdpc2VtYXJrc3RyZW5kJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnT3ZlcmFsbCAvIFN1YmplY3Qgd2lzZSBBdHRlbXB0IGFuZCBBY2N1cmFjeSB0cmVuZCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdvdmVyYWxsc3ViamVjdHdpc2VhdHRlbXB0YW5kYWNjdXJhY3l0cmVuZCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vhc2lseSBmaW5kIGNvbXBsZXRlZCB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdlYXNpbHlmaW5kY29tcGxldGVkdGVzdHMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdJbmRpdmlkdWFsIGFuZCBPdmVyYWxsIFN1bW1hcnknLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnaW5kaXZpZHVhbGFuZG92ZXJhbGxzdW1tYXJ5JyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3ViamVjdCB3aXNlIG1hcmtzIG9mIGFsbCAvIGluZGl2aWR1YWwgdGVzdHMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnc3ViamVjdHdpc2VtYXJrc29mYWxsaW5kaXZpZHVhbHRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQXR0ZW1wdCAmIEFjY3VyYWN5IHJhdGUgaW4gYWxsIHN1YmplY3RzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2F0dGVtcHRhY2N1cmFjeXJhdGVpbmFsbHN1YmplY3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOlxuICAgICAgICAgICAgICAgICAgJ1F1ZXN0aW9ucyBkaXN0cmlidXRpb24gaW4gb3ZlcmFsbCAmIHN1YmplY3Qgd2lzZSBpbiBpbmRpdmlkdWFsIC8gYWxsIHRlc3RzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICdxdWVzdGlvbnNkaXN0cmlidXRpb25pbm92ZXJhbGxzdWJqZWN0d2lzZWluaW5kaXZpZHVhbGFsbHRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVG9waWMgLyBzdWIgdG9waWMgLyBxdWVzdGlvbiBsZXZlbCBhbmFseXNpcycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd0b3BpY3N1YnRvcGljcXVlc3Rpb25sZXZlbGFuYWx5c2lzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUXVlc3Rpb24gd2lzZSBiZWhhdmlvdXIgYW5hbHlzaXMnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncXVlc3Rpb253aXNlYmVoYXZpb3VyYW5hbHlzaXMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdRdWVzdGlvbiAvIHN1YmplY3QgLyBkaWZmaWN1bHQgbGV2ZWwgVGltZSBzcGVudCcsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdxdWVzdGlvbnN1YmplY3RkaWZmaWN1bHRsZXZlbHRpbWVzcGVudCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1RpbWUgc3BlbnQgb24gQ29ycmVjdCAvIHdyb25nIC8gdW5hdHRlbXB0ZWQgcXVlc3Rpb25zJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3RpbWVzcGVudG9uY29ycmVjdHdyb25ndW5hdHRlbXB0ZWRxdWVzdGlvbnMnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQ2FuIENvbXBhcmUgd2l0IFRvcHBlcicsIHZhbHVlOiAnY2FuY29tcGFyZXdpdHRvcHBlcicgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwcmFjdGljZToge1xuICAgICAgICAgICAgbGFiZWw6ICdQcmFjdGljZScsXG4gICAgICAgICAgICB2YWx1ZTogJ3ByYWN0aWNlJyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUHJhY3RpY2UgYXZhaWxhYmxlIGZvciAxMXRoLCAxMnRoIGFuZCBib3RoJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3ByYWN0aWNlYXZhaWxhYmxlZm9yMTF0aDEydGhhbmRib3RoJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VwcG9ydHMgLSBKRUUgTWFpbiAvIE5FRVQgLyBFQU1DRVQnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAnc3VwcG9ydHNqZWVtYWlubmVldGVhbWNldCcsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1VubGltaXRlZCBQcmFjdGljZSB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd1bmxpbWl0ZWRwcmFjdGljZXRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Z1bGwgc3lsbGFidXMgVGVzdHMnLCB2YWx1ZTogJ2Z1bGxzeWxsYWJ1c3Rlc3RzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU3ViamVjdCB3aXNlIHByYWN0aWNlJywgdmFsdWU6ICdzdWJqZWN0d2lzZXByYWN0aWNlJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnY2hhcHRlciB3aXNlIHByYWN0aWNlJywgdmFsdWU6ICdjaGFwdGVyd2lzZXByYWN0aWNlJyB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdRdWVzdGlvbnMgYmFzZWQgcHJhY3RpY2UnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiAncXVlc3Rpb25zYmFzZWRwcmFjdGljZScsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdUaW1lIGJhc2VkIFByYWN0aWNlJywgdmFsdWU6ICd0aW1lYmFzZWRwcmFjdGljZScgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0luc3RhbnQgUmVzdWx0cycsIHZhbHVlOiAnaW5zdGFudHJlc3VsdHMnIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RldGFpbGVkIGFuYWx5c2lzIG9mIGFsbCBwcmFjdGljZSB0ZXN0cycsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdkZXRhaWxlZGFuYWx5c2lzb2ZhbGxwcmFjdGljZXRlc3RzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICBsaXZlY2xhc3Nlczoge1xuICAgICAgICBsYWJlbDogJ0xpdmUgY2xhc3NlcycsXG4gICAgICAgIHZhbHVlOiAnbGl2ZWNsYXNzZXMnLFxuICAgICAgICBpbWFnZVVybDogJy9pbWFnZXMvTGl2ZS5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvTGl2ZU1vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlTGl2ZS5zdmcnLFxuICAgICAgICBzdWJNb2R1bGVzOiB7XG4gICAgICAgICAgbGl2ZToge1xuICAgICAgICAgICAgbGFiZWw6ICdMaXZlJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbGl2ZScsXG4gICAgICAgICAgICBkYXRhOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NpZ25sZSBDbGljayB0byBKb2luIGNsYXNzJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ3NpZ25sZWNsaWNrdG9qb2luY2xhc3MnLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdObyBuZWVkIHRvIGRvd25sb2FkIGFueSBhcHBzICh6b29tIGludGVncmF0ZWQpJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ25vbmVlZHRvZG93bmxvYWRhbnlhcHBzJyxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ1dhdGNoIGxhdGVyJywgdmFsdWU6ICd3YXRjaGxhdGVyJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnU21hcnQgRmlsdGVycycsIHZhbHVlOiAnU21hcnRmaWx0ZXJzJyB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIGFzc2lnbm1lbnRzOiB7XG4gICAgICAgIGxhYmVsOiAnQXNzaWdubWVudHMnLFxuICAgICAgICB2YWx1ZTogJ2Fzc2lnbm1lbnRzJyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0Fzc2lnbm1lbnRzLnN2ZycsXG4gICAgICAgIG1vYmlsZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9Bc3NpZ25tZW50c01vYmlsZS5zdmcnLFxuICAgICAgICBhY3RpdmVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvYWN0aXZlQXNzaWdubWVudHMuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIGFzc2lnbm1lbnQ6IHtcbiAgICAgICAgICAgIGxhYmVsOiAnQXNzaWdubWVudHMnLFxuICAgICAgICAgICAgdmFsdWU6ICdhc3NpZ25tZW50JyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAgeyBsYWJlbDogJ0Vhc3kgc3VibWlzc2lvbicsIHZhbHVlOiAnZWFzeXN1Ym1pc3Npb24nIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Vhc3kgdG8gY2hlY2sgZXZhbHVhdGlvbicsXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdlYXN5dG9jaGVja2V2YWx1YXRpb24nLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIGRvdWJ0czoge1xuICAgICAgICBsYWJlbDogJ0RvdWJ0cycsXG4gICAgICAgIHZhbHVlOiAnZG91YnRzJyxcbiAgICAgICAgaW1hZ2VVcmw6ICcvaW1hZ2VzL0RvdWJ0cy5zdmcnLFxuICAgICAgICBtb2JpbGVJbWFnZVVybDogJy9pbWFnZXMvUHJpY2luZ0RldGFpbHMvRG91YnRzTW9iaWxlLnN2ZycsXG4gICAgICAgIGFjdGl2ZUltYWdlVXJsOiAnL2ltYWdlcy9QcmljaW5nRGV0YWlscy9hY3RpdmVEb3VidHMuc3ZnJyxcbiAgICAgICAgc3ViTW9kdWxlczoge1xuICAgICAgICAgIGRvdWJ0OiB7XG4gICAgICAgICAgICBsYWJlbDogJ0RvdWJ0cycsXG4gICAgICAgICAgICB2YWx1ZTogJ2RvdWJ0JyxcbiAgICAgICAgICAgIGRhdGE6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRWFzeSB0byBhc2sgZG91YnRzIChDaGF0IC8gdXBsb2FkIGltYWdlIC8gaW5zZXJ0IGxpbmspJyxcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2Vhc3l0b2Fza2RvdWJ0cycsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdCb29rbWFyayBkb3VidHMnLCB2YWx1ZTogJ2Jvb2ttYXJrZG91YnRzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnUmVvcGVuIGRvdWJ0cycsIHZhbHVlOiAncmVvcGVuZG91YnRzJyB9LFxuICAgICAgICAgICAgICB7IGxhYmVsOiAnQ2hlY2sgbGF0ZXInLCB2YWx1ZTogJ2NoZWNrbGF0ZXInIH0sXG4gICAgICAgICAgICAgIHsgbGFiZWw6ICdTbWFydCBGaWx0ZXJzJywgdmFsdWU6ICdzbWFydGZpbHRlcnMnIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgIH0sXG4gIH0sXG59O1xuXG5leHBvcnQgY29uc3QgZGVtbyA9ICgpID0+IHt9O1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExpbmsgZnJvbSAnY29tcG9uZW50cy9MaW5rJztcbmltcG9ydCB7IFBsYW5EZXRhaWxzIH0gZnJvbSAnLi9Db25zdGFudHMnO1xuaW1wb3J0IHMgZnJvbSAnLi9QcmljaW5nVmlld0RldGFpbHMuc2Nzcyc7XG5cbmNsYXNzIFByaWNpbmdWaWV3RGV0YWlscyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICByb2xlOiAnYWRtaW4nLFxuICAgICAgYWN0aXZlTW9kdWxlOiAnb25saW5ldGVzdCcsXG4gICAgICBhY3RpdmVTdWJtb2R1bGU6ICd0ZXN0cycsXG4gICAgICBtb2JpbGU6IGZhbHNlLFxuICAgICAgc2hvdzogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuaGFuZGxlUmVzaXplKCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcbiAgfVxuXG4gIGhhbmRsZVJlc2l6ZSA9ICgpID0+IHtcbiAgICBjb25zdCB7IG1vYmlsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICAvLyBjb25zb2xlLmxvZyhcImZ1bmN0aW9uIGNhbGxlZFwiKVxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDk5MCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vYmlsZTogIW1vYmlsZSB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vYmlsZSB9KTtcbiAgICB9XG4gIH07XG5cbiAgc2hvd1JvbGUgPSB2YWx1ZSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBzaG93OiAhdHJ1ZSxcbiAgICB9KTtcbiAgICBjb25zdCB7IHJvbGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgcGxhbiA9IFBsYW5EZXRhaWxzW3ZhbHVlXS5tb2R1bGVzO1xuICAgIGNvbnN0IGZpcnN0ID0gT2JqZWN0LnZhbHVlcyhwbGFuIHx8IHt9KTtcbiAgICBjb25zdCBmaXJzdE1vZCA9IGZpcnN0WzBdLnZhbHVlO1xuICAgIGNvbnN0IHN1Yk1vZCA9IHBsYW5bZmlyc3RNb2RdLnN1Yk1vZHVsZXM7XG4gICAgY29uc3QgZmlyc3RTdWIgPSBPYmplY3QudmFsdWVzKHN1Yk1vZCB8fCB7fSk7XG4gICAgY29uc3QgZmlyc3RPbmUgPSBmaXJzdFN1YlswXS52YWx1ZTtcblxuICAgIGlmICh2YWx1ZSAhPT0gcm9sZSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHJvbGU6IHZhbHVlLFxuICAgICAgICBhY3RpdmVNb2R1bGU6IGZpcnN0TW9kLFxuICAgICAgICBhY3RpdmVTdWJtb2R1bGU6IGZpcnN0T25lLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICByb2xlOiB2YWx1ZSxcbiAgICAgICAgYWN0aXZlTW9kdWxlOiBmaXJzdE1vZCxcbiAgICAgICAgYWN0aXZlU3VibW9kdWxlOiBmaXJzdE9uZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBzaG93TW9kdWxlcyA9IGtleSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgbW9kID0gUGxhbkRldGFpbHNbcm9sZV0ubW9kdWxlcztcbiAgICBjb25zdCBzdWIgPSBtb2Rba2V5XS5zdWJNb2R1bGVzO1xuICAgIGNvbnN0IHN1Ym1vZHVsZSA9IE9iamVjdC52YWx1ZXMoc3ViIHx8IHt9KTtcbiAgICBpZiAoa2V5ICE9PSBhY3RpdmVNb2R1bGUpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVNb2R1bGU6IGtleSwgYWN0aXZlU3VibW9kdWxlOiBzdWJtb2R1bGVbMF0udmFsdWUgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVNb2R1bGU6IGtleSB9KTtcbiAgICB9XG4gIH07XG5cbiAgc2hvd1N1Ym1vZHVsZSA9IHZhbHVlID0+IHtcbiAgICBjb25zdCB7IGFjdGl2ZVN1Ym1vZHVsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAodmFsdWUgIT09IGFjdGl2ZVN1Ym1vZHVsZSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGFjdGl2ZVN1Ym1vZHVsZTogdmFsdWUgfSk7XG4gICAgfVxuICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVTdWJtb2R1bGU6IHZhbHVlIH0pO1xuICB9O1xuXG4gIHNob3dEYXRhID0gdmFsdWUgPT4ge1xuICAgIGNvbnN0IHsgYWN0aXZlU2VjdGlvbiB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAodmFsdWUgIT09IGFjdGl2ZVNlY3Rpb24pIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVTZWN0aW9uOiB2YWx1ZSB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGFjdGl2ZVNlY3Rpb246IHZhbHVlIH0pO1xuICAgIH1cbiAgfTtcblxuICBkaXNwbGF5U2VjdGlvbnMgPSAoKSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUsIGFjdGl2ZVN1Ym1vZHVsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBhY3RpdmVTdWJNb2R1bGVOYW1lID1cbiAgICAgIFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXNbYWN0aXZlTW9kdWxlXS5zdWJNb2R1bGVzW2FjdGl2ZVN1Ym1vZHVsZV0ubGFiZWw7XG4gICAgY29uc3Qgc2VjdGlvbkRhdGEgPVxuICAgICAgUGxhbkRldGFpbHNbcm9sZV0ubW9kdWxlc1thY3RpdmVNb2R1bGVdLnN1Yk1vZHVsZXNbYWN0aXZlU3VibW9kdWxlXTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJtb2R1bGVzU2VjdGlvbkNvbnRhaW5lcn0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25OYW1lfT57YWN0aXZlU3ViTW9kdWxlTmFtZX08L2Rpdj5cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICB7T2JqZWN0LnZhbHVlcyhzZWN0aW9uRGF0YS5kYXRhIHx8IHt9KS5tYXAoKGQsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBiZWxvdyA9IGluZGV4IDwgMTA7XG5cbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmljb25EYXRhQ29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICB7YmVsb3cgJiYgKFxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaWNvbkNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL0ljb25zLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmljb25zfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cInJpZ2h0XCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZGF0YUNvbnRhaW5lcn0+e2QubGFiZWx9PC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICBkaXNwbGF5U2VjdGlvbiA9ICgpID0+IHtcbiAgICBjb25zdCB7IHJvbGUsIGFjdGl2ZU1vZHVsZSwgYWN0aXZlU3VibW9kdWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHNlY3Rpb25EYXRhID1cbiAgICAgIFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXNbYWN0aXZlTW9kdWxlXS5zdWJNb2R1bGVzW2FjdGl2ZVN1Ym1vZHVsZV07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3VibW9kdWxlc1NlY3Rpb25Db250YWluZXIxfT5cbiAgICAgICAge09iamVjdC52YWx1ZXMoc2VjdGlvbkRhdGEuZGF0YSB8fCB7fSkubWFwKChkLCBpbmRleCkgPT4ge1xuICAgICAgICAgIGNvbnN0IGJlbG93ID0gaW5kZXggPj0gMTA7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmljb25EYXRhQ29udGFpbmVyMX0+XG4gICAgICAgICAgICAgIHtiZWxvdyAmJiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaWNvbkNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvSWNvbnMuc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmljb25zfVxuICAgICAgICAgICAgICAgICAgICBhbHQ9XCJyaWdodFwiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZGF0YUNvbnRhaW5lcn0+e2QubGFiZWx9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheVN1Yk1vZHVsZXMgPSAoKSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUsIGFjdGl2ZVN1Ym1vZHVsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBmaWx0ZXIgPSBQbGFuRGV0YWlsc1tyb2xlXS5tb2R1bGVzW2FjdGl2ZU1vZHVsZV0uc3ViTW9kdWxlcztcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViTW9kdWxlQ29udGFpbmVyfT5cbiAgICAgICAge09iamVjdC52YWx1ZXMoZmlsdGVyIHx8IHt9KS5tYXAoZWFjaCA9PiB7XG4gICAgICAgICAgY29uc3QgaXNBY3RpdmUgPSBhY3RpdmVTdWJtb2R1bGUgPT09IGVhY2gudmFsdWU7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN0dWRlbnRNb2R1bGV9PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbWFnZUNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAge2lzQWN0aXZlID8gKFxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1ByaWNpbmdEZXRhaWxzL0Fycm93LnN2Z1wiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5hcnJvd0ljb259XG4gICAgICAgICAgICAgICAgICAgIGFsdD1cImFycm93XCJcbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgIGlzQWN0aXZlXG4gICAgICAgICAgICAgICAgICAgID8gYCR7cy5zdWJNb2R1bGVzfSAke3Muc3ViTW9kdWxlQWN0aXZlfWBcbiAgICAgICAgICAgICAgICAgICAgOiBzLnN1Yk1vZHVsZXNcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zaG93U3VibW9kdWxlKGVhY2gudmFsdWUpO1xuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7ZWFjaC5sYWJlbH1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH07XG5cbiAgc2hvd01vZHVsZSA9IGtleSA9PiB7XG4gICAgY29uc3QgeyByb2xlLCBhY3RpdmVNb2R1bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgbW9kID0gUGxhbkRldGFpbHNbcm9sZV0ubW9kdWxlcztcbiAgICBjb25zdCBzdWIgPSBtb2Rba2V5XS5zdWJNb2R1bGVzO1xuICAgIGNvbnN0IHN1Ym1vZHVsZSA9IE9iamVjdC52YWx1ZXMoc3ViIHx8IHt9KTtcbiAgICBpZiAoa2V5ICE9PSBhY3RpdmVNb2R1bGUpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBhY3RpdmVNb2R1bGU6IGtleSwgYWN0aXZlU3VibW9kdWxlOiBzdWJtb2R1bGVbMF0udmFsdWUgfSk7XG4gICAgICAvKiB0aGlzLnNldFN0YXRlKHByZXZTdGF0ZSA9PiAoe1xuICAgICAgICBzaG93OiAhcHJldlN0YXRlLnNob3csXG4gICAgICB9KSk7ICAgKi9cbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93OiAhdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgYWN0aXZlTW9kdWxlOiBrZXksIGFjdGl2ZVN1Ym1vZHVsZTogc3VibW9kdWxlWzBdLnZhbHVlIH0pO1xuICAgICAgdGhpcy5zZXRTdGF0ZShwcmV2U3RhdGUgPT4gKHtcbiAgICAgICAgc2hvdzogIXByZXZTdGF0ZS5zaG93LFxuICAgICAgfSkpO1xuICAgIH1cbiAgfTtcblxuICBkaXNwbGF5TW9kdWxlID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSwgYWN0aXZlTW9kdWxlLCBzaG93IH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IGZpbHRlciA9IFBsYW5EZXRhaWxzW3JvbGVdLm1vZHVsZXM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlckNvbnRhaW5lcnN9PlxuICAgICAgICB7T2JqZWN0LnZhbHVlcyhmaWx0ZXIpLm1hcChpdGVtID0+IHtcbiAgICAgICAgICBjb25zdCBpc0FjdGl2ZU1vZHVsZSA9IGFjdGl2ZU1vZHVsZSA9PT0gaXRlbS52YWx1ZTtcbiAgICAgICAgICBjb25zdCBzaG93QWxsID0gYWN0aXZlTW9kdWxlID09PSBpdGVtLnZhbHVlO1xuICAgICAgICAgIGNvbnN0IGltYWdlVXJsID0gaXNBY3RpdmVNb2R1bGUgPyBpdGVtLmFjdGl2ZUltYWdlVXJsIDogaXRlbS5pbWFnZVVybDtcbiAgICAgICAgICBjb25zdCBpbWFnZU1vYmlsZVVybCA9XG4gICAgICAgICAgICBpc0FjdGl2ZU1vZHVsZSAmJiAhc2hvdyA/IGl0ZW0ubW9iaWxlSW1hZ2VVcmwgOiBpdGVtLmltYWdlVXJsO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgIGlzQWN0aXZlTW9kdWxlICYmICFzaG93XG4gICAgICAgICAgICAgICAgICAgID8gYCR7cy5tb2R1bGVDb250YWluZXJ9ICR7cy5tb2R1bGVDb250YWluZXJBY3RpdmV9YFxuICAgICAgICAgICAgICAgICAgICA6IHMubW9kdWxlQ29udGFpbmVyXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd01vZHVsZShpdGVtLnZhbHVlKTtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtpbWFnZVVybH1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgIGlzQWN0aXZlTW9kdWxlID8gYCR7cy5pY29ufSAke3MuaWNvbkFjdGl2ZX1gIDogcy5pY29uXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBhbHQ9XCJpbWdcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPXtpbWFnZU1vYmlsZVVybH1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5tb2R1bGVJY29ufVxuICAgICAgICAgICAgICAgICAgYWx0PVwibW9iaWxlSWNvblwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8cFxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICAgICAgaXNBY3RpdmVNb2R1bGUgJiYgIXNob3dcbiAgICAgICAgICAgICAgICAgICAgICA/IGAke3MubW9kdWxlTmFtZX0gJHtzLm1vZHVsZU5hbWVBY3RpdmV9YFxuICAgICAgICAgICAgICAgICAgICAgIDogcy5tb2R1bGVOYW1lXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge2l0ZW0ubGFiZWx9XG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsdXNDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAge2lzQWN0aXZlTW9kdWxlICYmICFzaG93ID8gKFxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9taW51cy5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5wbHVzfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIm1pbnVzXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9wbHVzLnN2Z1wiIGNsYXNzTmFtZT17cy5wbHVzfSBhbHQ9XCJwbHVzXCIgLz5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICB7aXNBY3RpdmVNb2R1bGUgJiYgIXNob3cgJiYgc2hvd0FsbCAmJiAoXG4gICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgIHtPYmplY3QudmFsdWVzKGl0ZW0uc3ViTW9kdWxlcyB8fCB7fSkubWFwKGVhY2ggPT4gKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLnN1Yk1vZHVsZU1vYmlsZUNvbnRhaW5lcn0gY3VzdG9tLXNjcm9sbGJhcmB9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJNb2R1bGVOYW1lfT57ZWFjaC5sYWJlbH08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICB7T2JqZWN0LnZhbHVlcyhlYWNoLmRhdGEgfHwge30pLm1hcChkYXRhID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Ym1vZHVsZURhdGFDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9JY29ucy5zdmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5yaWdodEljb25zfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cInJpZ2h0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3VibW9kdWxlRGF0YX0+e2RhdGEubGFiZWx9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICBkaXNwbGF5TW9kdWxlcyA9ICgpID0+IHtcbiAgICBjb25zdCB7IHJvbGUsIGFjdGl2ZU1vZHVsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBmaWx0ZXIgPSBQbGFuRGV0YWlsc1tyb2xlXS5tb2R1bGVzO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkZXJDb250YWluZXJ9PlxuICAgICAgICB7T2JqZWN0LnZhbHVlcyhmaWx0ZXIpLm1hcChpdGVtID0+IHtcbiAgICAgICAgICBjb25zdCBpc0FjdGl2ZU1vZHVsZSA9IGFjdGl2ZU1vZHVsZSA9PT0gaXRlbS52YWx1ZTtcbiAgICAgICAgICBjb25zdCBpbWFnZVVybCA9IGlzQWN0aXZlTW9kdWxlID8gaXRlbS5hY3RpdmVJbWFnZVVybCA6IGl0ZW0uaW1hZ2VVcmw7XG4gICAgICAgICAgY29uc3QgaW1hZ2VNb2JpbGVVcmwgPSBpc0FjdGl2ZU1vZHVsZVxuICAgICAgICAgICAgPyBpdGVtLm1vYmlsZUltYWdlVXJsXG4gICAgICAgICAgICA6IGl0ZW0uaW1hZ2VVcmw7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgICAgICAgICBpc0FjdGl2ZU1vZHVsZVxuICAgICAgICAgICAgICAgICAgPyBgJHtzLm1vZHVsZUNvbnRhaW5lcn0gJHtzLm1vZHVsZUNvbnRhaW5lckFjdGl2ZX1gXG4gICAgICAgICAgICAgICAgICA6IHMubW9kdWxlQ29udGFpbmVyXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvd01vZHVsZXMoaXRlbS52YWx1ZSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPXtpbWFnZVVybH1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgaXNBY3RpdmVNb2R1bGUgPyBgJHtzLmljb259ICR7cy5pY29uQWN0aXZlfWAgOiBzLmljb25cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYWx0PVwiaW1nXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPGltZyBzcmM9e2ltYWdlTW9iaWxlVXJsfSBjbGFzc05hbWU9e3MubW9kdWxlSWNvbn0gYWx0PVwibW9iaWxlXCIgLz5cbiAgICAgICAgICAgICAgPHBcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgaXNBY3RpdmVNb2R1bGVcbiAgICAgICAgICAgICAgICAgICAgPyBgJHtzLm1vZHVsZU5hbWV9ICR7cy5tb2R1bGVOYW1lQWN0aXZlfWBcbiAgICAgICAgICAgICAgICAgICAgOiBzLm1vZHVsZU5hbWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7aXRlbS5sYWJlbH1cbiAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9O1xuXG4gIGRpc3BsYXlUeXBlT2ZVc2VyID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgcm9sZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGFic0NvbnRhaW5lcn0+XG4gICAgICAgIHtPYmplY3QudmFsdWVzKFBsYW5EZXRhaWxzKS5tYXAoZWFjaCA9PiB7XG4gICAgICAgICAgY29uc3QgaXNBY3RpdmUgPSByb2xlID09PSBlYWNoLnZhbHVlO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17aXNBY3RpdmUgPyBzLmFkbWluQ29udGFpbmVyQWN0aXZlIDogcy5hZG1pbkNvbnRhaW5lcn1cbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvd1JvbGUoZWFjaC52YWx1ZSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7aXNBY3RpdmUgPyAoXG4gICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9TaGFwZS5zdmdcIlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzLnJpZ2h0SWNvbn1cbiAgICAgICAgICAgICAgICAgIGFsdD1cInJpZ2h0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2lzQWN0aXZlID8gcy5hY3RpdmUgOiBzLmFkbWluTGFiZWxDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmltZ0NvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZWFjaC5pbWdVcmx9IGNsYXNzTmFtZT17cy5hZG1pbkltZ30gYWx0PVwic3R1ZGVudFwiIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmFkbWluTGFiZWx9PntlYWNoLmxhYmVsfTwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH07XG5cbiAgZGlzcGxheVBsYW5lcyA9ICgpID0+IDxoMSBjbGFzc05hbWU9e3MucGxhbmV9PkV2ZXJ5IFBsYW4gaW5jbHVkZXM8L2gxPjtcblxuICBkaXNwbGF5UHJpY2luZ0hlYWRlciA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljaW5nSGVhZGVyfT5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnByaWNlX3RpdGxlfT5DaG9vc2UgeW91ciBlZGl0aW9uLjwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wcmljZV90aXRsZX0+VHJ5IGl0IGZyZWUgZm9yIDcgZGF5cy48L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MucHJpY2VfY29udGVudH0+XG4gICAgICAgIEVnbmlmeSBwbGFucyBzdGFydCBhcyBsb3cgYXMgUnMuNDUvLSBwZXIgc3R1ZGVudCBwZXIgbW9udGguXG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwYWx5V2hvQXJlWW91ID0gKCkgPT4gKFxuICAgIDxkaXY+XG4gICAgICA8aDEgY2xhc3NOYW1lPXtzLndob0FyZVlvdX0+V2hvIGFyZSB5b3U/PC9oMT5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwYWx5R29CYWNrTGluayA9ICgpID0+IChcbiAgICA8TGluayB0bz1cIi9wcmljaW5nXCIgY2xhc3NOYW1lPXtzLmdvQmFja0xpbmt9PlxuICAgICAgPHA+YmFjayB0byBwcmljaW5nIGRldGFpbHM8L3A+XG4gICAgPC9MaW5rPlxuICApO1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlc30+XG4gICAgICAgIHt0aGlzLmRpc3BsYXlQcmljaW5nSGVhZGVyKCl9XG4gICAgICAgIHt0aGlzLmRpc3BhbHlHb0JhY2tMaW5rKCl9XG4gICAgICAgIHt0aGlzLmRpc3BhbHlXaG9BcmVZb3UoKX1cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlRGF0YX0+XG4gICAgICAgICAge3RoaXMuZGlzcGxheVR5cGVPZlVzZXIoKX1cbiAgICAgICAgICB7dGhpcy5kaXNwbGF5UGxhbmVzKCl9XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9kdWxlRmxleH0+XG4gICAgICAgICAgICB7dGhpcy5kaXNwbGF5TW9kdWxlcygpfVxuICAgICAgICAgICAge3RoaXMuZGlzcGxheU1vZHVsZSgpfVxuICAgICAgICAgICAgey8qICAge3RoaXMuZGlzcGxheU1vYmlsZU1vZHVsZXMoKX0gICovfVxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViTW9kdWxlc0ZsZXh9PlxuICAgICAgICAgICAgICB7dGhpcy5kaXNwbGF5U3ViTW9kdWxlcygpfVxuICAgICAgICAgICAgICB7dGhpcy5kaXNwbGF5U2VjdGlvbnMoKX1cbiAgICAgICAgICAgICAge3RoaXMuZGlzcGxheVNlY3Rpb24oKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShQcmljaW5nVmlld0RldGFpbHMpO1xuIiwiXG4gICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nVmlld0RldGFpbHMuc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9QcmljaW5nVmlld0RldGFpbHMuc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaWNpbmdWaWV3RGV0YWlscy5zY3NzXCIpO1xuXG4gICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgIH1cblxuICAgICAgICByZW1vdmVDc3MgPSBpbnNlcnRDc3MoY29udGVudCwgeyByZXBsYWNlOiB0cnVlIH0pO1xuICAgICAgfSk7XG4gICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHJlbW92ZUNzcygpOyB9KTtcbiAgICB9XG4gICIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0L0xheW91dCc7XG5pbXBvcnQgUHJpY2luZ1ZpZXdEZXRhaWxzIGZyb20gJy4vUHJpY2luZ1ZpZXdEZXRhaWxzJztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiBbJ0dldFJhbmtzIGJ5IEVnbmlmeTogQXNzZXNzbWVudCAmIEFuYWx5dGljcyBQbGF0Zm9ybSddLFxuICAgIGNodW5rczogWydQcmljaW5nRGV0YWlscyddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dCBmb290ZXJBc2g+XG4gICAgICAgIDxQcmljaW5nVmlld0RldGFpbHMgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQzFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBMURBO0FBZ0VBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUhBO0FBTUE7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBZEE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBSEE7QUFYQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQWJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBWkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQXBCQTtBQXZJQTtBQU5BO0FBc0tBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBSEE7QUFNQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFoQ0E7QUFtQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBSEE7QUFyQkE7QUFwQ0E7QUFOQTtBQXlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBaEJBO0FBREE7QUFOQTtBQStCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUhBO0FBS0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBWkE7QUFEQTtBQU5BO0FBd0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBVEE7QUFOQTtBQXZTQTtBQUpBO0FBa1VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUhBO0FBTUE7QUFFQTtBQUhBO0FBTUE7QUFBQTtBQUFBO0FBcEJBO0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUhBO0FBT0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBOURBO0FBaUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBekJBO0FBekZBO0FBTkE7QUFnSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFGQTtBQUlBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQWJBO0FBREE7QUFOQTtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBTEE7QUFEQTtBQU5BO0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFYQTtBQURBO0FBTkE7QUE5S0E7QUFKQTtBQW5VQTtBQWdoQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hoQkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQWdCQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQXpCQTtBQTJCQTtBQUNBO0FBREE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFwREE7QUFxREE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFoRUE7QUFpRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUF4RUE7QUF5RUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFqRkE7QUFrRkE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBSUE7QUFDQTtBQW5IQTtBQW9IQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUdBO0FBQ0E7QUEvSUE7QUFnSkE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUdBO0FBQ0E7QUFyTEE7QUFzTEE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7O0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQTFNQTtBQTJNQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUdBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBTEE7QUFxQkE7QUFHQTtBQUNBO0FBL1JBO0FBZ1NBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFHQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFHQTtBQUNBO0FBOVVBO0FBK1VBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFHQTtBQUNBO0FBaFhBO0FBaVhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbFhBO0FBbVhBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFOQTtBQUNBO0FBcFhBO0FBK1hBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUNBO0FBaFlBO0FBcVlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQ0E7QUFwWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFGQTtBQVNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUE2WEE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7Ozs7QUFsYUE7QUFDQTtBQW9hQTs7Ozs7OztBQzVhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBWUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==