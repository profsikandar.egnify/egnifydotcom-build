(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PrivacyAndTerms"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/privacy-and-terms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n.PrivacyAndTerms-bannerSection-1i3ow {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.PrivacyAndTerms-heading-13FWP {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.PrivacyAndTerms-introContent-31zzH {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.PrivacyAndTerms-introContent-31zzH div {\n    padding-bottom: 40px;\n  }\n\n.PrivacyAndTerms-commit-3LsC1 {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.PrivacyAndTerms-commitHeading-2iU1G {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1Hc0T {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.PrivacyAndTerms-commitContent-1Hc0T li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .PrivacyAndTerms-heading-13FWP {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .PrivacyAndTerms-heading-13FWP {\n    font-size: 35px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/PrivacyAndTerms.scss"],"names":[],"mappings":";AACA;EACE,oBAAoB;EACpB,kBAAkB;EAClB,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,aAAa;EACb,gBAAgB;EAChB,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,kBAAkB;EAClB,mBAAmB;EACnB,kBAAkB;CACnB;;AAED;IACI,qBAAqB;GACtB;;AAEH;EACE,kBAAkB;EAClB,2CAA2C;CAC5C;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,kBAAkB;EAClB,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,qBAAqB;CACtB;;AAED;EACE,kBAAkB;EAClB,mBAAmB;EACnB,qBAAqB;CACtB;;AAED;IACI,qBAAqB;IACrB,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;IACf,iCAAiC;GAClC;;AAEH;EACE;IACE,gBAAgB;GACjB;CACF;;AAED;EACE;IACE,gBAAgB;GACjB;CACF","file":"PrivacyAndTerms.scss","sourcesContent":["\n.bannerSection {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: -o-linear-gradient(166deg, #af457d, #ec4c6f);\n  background-image: linear-gradient(284deg, #af457d, #ec4c6f);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.heading {\n  height: 58px;\n  font-size: 48px;\n  font-weight: 500;\n  color: #fff;\n}\n\n.introContent {\n  font-size: 20px;\n  line-height: 1.5;\n  color: #5f6368;\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-top: 40px;\n}\n\n.introContent div {\n    padding-bottom: 40px;\n  }\n\n.commit {\n  padding-top: 50px;\n  background-color: rgba(216, 216, 216, 0.2);\n}\n\n.commitHeading {\n  font-size: 32px;\n  font-weight: 500;\n  line-height: 1.25;\n  color: #3e3e5f;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding-bottom: 40px;\n}\n\n.commitContent {\n  padding-left: 15%;\n  padding-right: 15%;\n  padding-bottom: 40px;\n}\n\n.commitContent li {\n    padding-bottom: 20px;\n    font-size: 20px;\n    line-height: 1.5;\n    color: #5f6368;\n    list-style-type: disc !important;\n  }\n\n@media only screen and (max-width: 800px) {\n  .heading {\n    font-size: 40px;\n  }\n}\n\n@media only screen and (max-width: 400px) {\n  .heading {\n    font-size: 35px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"bannerSection": "PrivacyAndTerms-bannerSection-1i3ow",
	"heading": "PrivacyAndTerms-heading-13FWP",
	"introContent": "PrivacyAndTerms-introContent-31zzH",
	"commit": "PrivacyAndTerms-commit-3LsC1",
	"commitHeading": "PrivacyAndTerms-commitHeading-2iU1G",
	"commitContent": "PrivacyAndTerms-commitContent-1Hc0T"
};

/***/ }),

/***/ "./src/routes/privacy-and-terms/PrivacyAndTerms.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/privacy-and-terms/PrivacyAndTerms.scss");
/* harmony import */ var _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/PrivacyAndTerms.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

 // import cx from 'classnames';


 // import Link from 'components/Link';


var introContent = [{
  content: 'Egnify is honored to be entrusted by educators and families to support their educational needs and school operations. We take responsibility to both support the effective use of student information and safeguard student privacy and information security.'
}, {
  content: 'Egnify supports schools – including their teachers, students and parents – to manage student data, carry out school operations, support instruction and learning opportunities, and develop and improve products/services intended for educational/school use. In so doing, it is critical that schools and school service providers build trust by effectively protecting the privacy of student information and communicating with parents about how student information is used and safeguarded.'
}, {
  content: 'We pledge to carry out responsible stewardship and appropriate use of student personal information according to the commitments below and in adherence to all laws applicable to us as school service providers.'
}];
var commitContent = [{
  content: 'Not collect, maintain, use or share student personal information beyond that needed for authorized educational/school purposes, or as authorized by the parent/student.'
}, {
  content: 'Not sell student personal information.'
}, {
  content: 'Not use or disclose student information collected through an educational/school service (whether personal information or otherwise) for behavioral targeting of advertisements to students.'
}, {
  content: 'Not build a personal profile of a student other than for supporting authorized educational/school purposes or as authorized by the parent/student.'
}, {
  content: 'Not make material changes to school service provider consumer privacy policies without first providing prominent notice to the account holder(s) (i.e., the educational institution/agency, or the parent/student when the information is collected directly from the student with student/parent consent) and allowing them choices before data is used in any manner inconsistent with terms they were initially provided; and not make material changes to other policies or practices governing the use of student personal information that are inconsistent with contractual requirements.'
}, {
  content: 'Collect, use, share, and retain student personal information only for purposes for which we were authorized by the educational institution/agency, teacher or the parent/student.'
}, {
  content: 'Disclose clearly in contracts or privacy policies, including in a manner easy for parents to understand, what types of student personal information we collect, if any, and the purposes for which the information we maintain is used or shared with third parties.'
}, {
  content: 'Support access to and correction of student personally identifiable information by the student or their authorized parent, either by assisting the educational institution in meeting its requirements or directly when the information is collected directly from the student with student/parent consent.'
}, {
  content: 'Maintain a comprehensive security program that is reasonably designed to protect the security, privacy, confidentiality, and integrity of student personal information against risks – such as unauthorized access or use, or unintended or inappropriate disclosure – through the use of administrative, technological, and physical safeguards appropriate to the sensitivity of the information.'
}, {
  content: 'Require that our vendors with whom student personal information is shared in order to deliver the educational service, if any, are obligated to implement these same commitments for the given student personal information.'
}, {
  content: 'Allow a successor entity to maintain the student personal information, in the case of our merger or acquisition by another entity, provided the successor entity is subject to these same commitments for the previously collected student personal information.'
}];

var PrivacyAndTerms =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PrivacyAndTerms, _React$Component);

  function PrivacyAndTerms() {
    _classCallCheck(this, PrivacyAndTerms);

    return _possibleConstructorReturn(this, _getPrototypeOf(PrivacyAndTerms).apply(this, arguments));
  }

  _createClass(PrivacyAndTerms, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      react_ga__WEBPACK_IMPORTED_MODULE_1__["default"].initialize(window.App.googleTrackingId, {
        debug: false
      });
      react_ga__WEBPACK_IMPORTED_MODULE_1__["default"].pageview(window.location.href);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.bannerSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, "Privacy Policy")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.introContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, introContent.map(function (content) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 85
          },
          __self: this
        }, content.content);
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commit,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commitHeading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, "We Commit To:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a.commitContent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, commitContent.map(function (content) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 93
          },
          __self: this
        }, content.content);
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, "We use Google Analytics as described in", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://policies.google.com/technologies/partner-sites",
        target: "_blank",
        style: {
          textDecoration: 'underline'
        },
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, "\"How Google uses data when you use our partner's sites or apps.\""), "You can prvent your data from being used by Google Analytics on our websites by installing the Google Analytics opt-out browser add-on. For enhanced privacy purposes,we also employ IP address masking, a technique used to truncate IP addresses collected by Google Analytics and store them in an abbreviated form to prevent them from being traced back to individual users. Portions of our website may also use Google Analytics for Display Advertisers including DoubleClick or Dynamic Remarketing which provide interest-based ads based on your visit to this or other websites. You can use Ads Settings to manage the Google ads you see and opt-out of interest-based ads. We also use Adobe Marketing Cloud as described.You can similary exercise your rights with respect to use of this data as described in the \"Exercising Choice\" section below."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, "Google and its Affiliates may retain and use, subject to the terms of its privacy policy (located at www.google.com/privacy.html), information collected in Your use of the Service. Google will not shareYour Customer Data or any Third Party's Customer Data with any third parties unless Google (i) has Your consent for any Customer Data or any Third Party's consent for the Third Party's Customer Data; (ii) concludes that it is required by law or has a google faith belief that access, preservation or disclosure of Customer Data is reasonably necessary to protect the rights, property or saftey of Google, its users or the public; or (iii) provides Customer Data in certain limited circumstances to third parties to carry out tasks on Google's behalf(e.g., billing or data storage) with strict restrictions that prevent the data from being used or shared except as directed by Google. When this is done, it is subject to agreements that oblige those parties to process Customer Data only on Google's instructions and in compliance with this Agreement and appropriate confidentiality and security measures.")))));
    }
  }]);

  return PrivacyAndTerms;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_PrivacyAndTerms_scss__WEBPACK_IMPORTED_MODULE_3___default.a)(PrivacyAndTerms));

/***/ }),

/***/ "./src/routes/privacy-and-terms/PrivacyAndTerms.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/privacy-and-terms/PrivacyAndTerms.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/privacy-and-terms/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/privacy-and-terms/PrivacyAndTerms.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/privacy-and-terms/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Privacy Policy | Egnify',
              chunks: ['PrivacyAndTerms'],
              content: 'Get to know about our privacy policy.',
              keywords: 'privacy policy',
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 12
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PrivacyAndTerms__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 13
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJpdmFjeUFuZFRlcm1zLmNodW5rLmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcml2YWN5LWFuZC10ZXJtcy9Qcml2YWN5QW5kVGVybXMuc2NzcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3ByaXZhY3ktYW5kLXRlcm1zL1ByaXZhY3lBbmRUZXJtcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL3ByaXZhY3ktYW5kLXRlcm1zL1ByaXZhY3lBbmRUZXJtcy5zY3NzP2U1YzgiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcml2YWN5LWFuZC10ZXJtcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuLlByaXZhY3lBbmRUZXJtcy1iYW5uZXJTZWN0aW9uLTFpM293IHtcXG4gIGJhY2tncm91bmQ6ICMzZTNlNWY7XFxuICBtaW4taGVpZ2h0OiAzNjZweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDE2NmRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMTY2ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyODRkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctMTNGV1Age1xcbiAgaGVpZ2h0OiA1OHB4O1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWludHJvQ29udGVudC0zMXp6SCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWludHJvQ29udGVudC0zMXp6SCBkaXYge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcXG4gIH1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWNvbW1pdC0zTHNDMSB7XFxuICBwYWRkaW5nLXRvcDogNTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjE2LCAyMTYsIDIxNiwgMC4yKTtcXG59XFxuXFxuLlByaXZhY3lBbmRUZXJtcy1jb21taXRIZWFkaW5nLTJpVTFHIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBsaW5lLWhlaWdodDogMS4yNTtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxufVxcblxcbi5Qcml2YWN5QW5kVGVybXMtY29tbWl0Q29udGVudC0xSGMwVCB7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uUHJpdmFjeUFuZFRlcm1zLWNvbW1pdENvbnRlbnQtMUhjMFQgbGkge1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBsaW5lLWhlaWdodDogMS41O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbGlzdC1zdHlsZS10eXBlOiBkaXNjICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xcbiAgLlByaXZhY3lBbmRUZXJtcy1oZWFkaW5nLTEzRldQIHtcXG4gICAgZm9udC1zaXplOiA0MHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQwMHB4KSB7XFxuICAuUHJpdmFjeUFuZFRlcm1zLWhlYWRpbmctMTNGV1Age1xcbiAgICBmb250LXNpemU6IDM1cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcml2YWN5LWFuZC10ZXJtcy9Qcml2YWN5QW5kVGVybXMuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBQ0E7RUFDRSxvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG9FQUFvRTtFQUNwRSwrREFBK0Q7RUFDL0QsNERBQTREO0VBQzVELHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7Q0FDbkI7O0FBRUQ7SUFDSSxxQkFBcUI7R0FDdEI7O0FBRUg7RUFDRSxrQkFBa0I7RUFDbEIsMkNBQTJDO0NBQzVDOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHFCQUFxQjtDQUN0Qjs7QUFFRDtJQUNJLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixpQ0FBaUM7R0FDbEM7O0FBRUg7RUFDRTtJQUNFLGdCQUFnQjtHQUNqQjtDQUNGOztBQUVEO0VBQ0U7SUFDRSxnQkFBZ0I7R0FDakI7Q0FDRlwiLFwiZmlsZVwiOlwiUHJpdmFjeUFuZFRlcm1zLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiXFxuLmJhbm5lclNlY3Rpb24ge1xcbiAgYmFja2dyb3VuZDogIzNlM2U1ZjtcXG4gIG1pbi1oZWlnaHQ6IDM2NnB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoMTY2ZGVnLCAjYWY0NTdkLCAjZWM0YzZmKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudCgxNjZkZWcsICNhZjQ1N2QsICNlYzRjNmYpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDI4NGRlZywgI2FmNDU3ZCwgI2VjNGM2Zik7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5oZWFkaW5nIHtcXG4gIGhlaWdodDogNThweDtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBjb2xvcjogI2ZmZjtcXG59XFxuXFxuLmludHJvQ29udGVudCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcXG4gIHBhZGRpbmctcmlnaHQ6IDE1JTtcXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xcbn1cXG5cXG4uaW50cm9Db250ZW50IGRpdiB7XFxuICAgIHBhZGRpbmctYm90dG9tOiA0MHB4O1xcbiAgfVxcblxcbi5jb21taXQge1xcbiAgcGFkZGluZy10b3A6IDUwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIxNiwgMjE2LCAyMTYsIDAuMik7XFxufVxcblxcbi5jb21taXRIZWFkaW5nIHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBsaW5lLWhlaWdodDogMS4yNTtcXG4gIGNvbG9yOiAjM2UzZTVmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxufVxcblxcbi5jb21taXRDb250ZW50IHtcXG4gIHBhZGRpbmctbGVmdDogMTUlO1xcbiAgcGFkZGluZy1yaWdodDogMTUlO1xcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XFxufVxcblxcbi5jb21taXRDb250ZW50IGxpIHtcXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICAgIGxpc3Qtc3R5bGUtdHlwZTogZGlzYyAhaW1wb3J0YW50O1xcbiAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcXG4gIC5oZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiA0MHB4O1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQwMHB4KSB7XFxuICAuaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMzVweDtcXG4gIH1cXG59XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImJhbm5lclNlY3Rpb25cIjogXCJQcml2YWN5QW5kVGVybXMtYmFubmVyU2VjdGlvbi0xaTNvd1wiLFxuXHRcImhlYWRpbmdcIjogXCJQcml2YWN5QW5kVGVybXMtaGVhZGluZy0xM0ZXUFwiLFxuXHRcImludHJvQ29udGVudFwiOiBcIlByaXZhY3lBbmRUZXJtcy1pbnRyb0NvbnRlbnQtMzF6ekhcIixcblx0XCJjb21taXRcIjogXCJQcml2YWN5QW5kVGVybXMtY29tbWl0LTNMc0MxXCIsXG5cdFwiY29tbWl0SGVhZGluZ1wiOiBcIlByaXZhY3lBbmRUZXJtcy1jb21taXRIZWFkaW5nLTJpVTFHXCIsXG5cdFwiY29tbWl0Q29udGVudFwiOiBcIlByaXZhY3lBbmRUZXJtcy1jb21taXRDb250ZW50LTFIYzBUXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0Jztcbi8vIGltcG9ydCBjeCBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbi8vIGltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgcyBmcm9tICcuL1ByaXZhY3lBbmRUZXJtcy5zY3NzJztcblxuY29uc3QgaW50cm9Db250ZW50ID0gW1xuICB7XG4gICAgY29udGVudDpcbiAgICAgICdFZ25pZnkgaXMgaG9ub3JlZCB0byBiZSBlbnRydXN0ZWQgYnkgZWR1Y2F0b3JzIGFuZCBmYW1pbGllcyB0byBzdXBwb3J0IHRoZWlyIGVkdWNhdGlvbmFsIG5lZWRzIGFuZCBzY2hvb2wgb3BlcmF0aW9ucy4gV2UgdGFrZSByZXNwb25zaWJpbGl0eSB0byBib3RoIHN1cHBvcnQgdGhlIGVmZmVjdGl2ZSB1c2Ugb2Ygc3R1ZGVudCBpbmZvcm1hdGlvbiBhbmQgc2FmZWd1YXJkIHN0dWRlbnQgcHJpdmFjeSBhbmQgaW5mb3JtYXRpb24gc2VjdXJpdHkuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnRWduaWZ5IHN1cHBvcnRzIHNjaG9vbHMg4oCTIGluY2x1ZGluZyB0aGVpciB0ZWFjaGVycywgc3R1ZGVudHMgYW5kIHBhcmVudHMg4oCTIHRvIG1hbmFnZSBzdHVkZW50IGRhdGEsIGNhcnJ5IG91dCBzY2hvb2wgb3BlcmF0aW9ucywgc3VwcG9ydCBpbnN0cnVjdGlvbiBhbmQgbGVhcm5pbmcgb3Bwb3J0dW5pdGllcywgYW5kIGRldmVsb3AgYW5kIGltcHJvdmUgcHJvZHVjdHMvc2VydmljZXMgaW50ZW5kZWQgZm9yIGVkdWNhdGlvbmFsL3NjaG9vbCB1c2UuIEluIHNvIGRvaW5nLCBpdCBpcyBjcml0aWNhbCB0aGF0IHNjaG9vbHMgYW5kIHNjaG9vbCBzZXJ2aWNlIHByb3ZpZGVycyBidWlsZCB0cnVzdCBieSBlZmZlY3RpdmVseSBwcm90ZWN0aW5nIHRoZSBwcml2YWN5IG9mIHN0dWRlbnQgaW5mb3JtYXRpb24gYW5kIGNvbW11bmljYXRpbmcgd2l0aCBwYXJlbnRzIGFib3V0IGhvdyBzdHVkZW50IGluZm9ybWF0aW9uIGlzIHVzZWQgYW5kIHNhZmVndWFyZGVkLicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ1dlIHBsZWRnZSB0byBjYXJyeSBvdXQgcmVzcG9uc2libGUgc3Rld2FyZHNoaXAgYW5kIGFwcHJvcHJpYXRlIHVzZSBvZiBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uIGFjY29yZGluZyB0byB0aGUgY29tbWl0bWVudHMgYmVsb3cgYW5kIGluIGFkaGVyZW5jZSB0byBhbGwgbGF3cyBhcHBsaWNhYmxlIHRvIHVzIGFzIHNjaG9vbCBzZXJ2aWNlIHByb3ZpZGVycy4nLFxuICB9LFxuXTtcblxuY29uc3QgY29tbWl0Q29udGVudCA9IFtcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnTm90IGNvbGxlY3QsIG1haW50YWluLCB1c2Ugb3Igc2hhcmUgc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiBiZXlvbmQgdGhhdCBuZWVkZWQgZm9yIGF1dGhvcml6ZWQgZWR1Y2F0aW9uYWwvc2Nob29sIHB1cnBvc2VzLCBvciBhcyBhdXRob3JpemVkIGJ5IHRoZSBwYXJlbnQvc3R1ZGVudC4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDogJ05vdCBzZWxsIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24uJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnTm90IHVzZSBvciBkaXNjbG9zZSBzdHVkZW50IGluZm9ybWF0aW9uIGNvbGxlY3RlZCB0aHJvdWdoIGFuIGVkdWNhdGlvbmFsL3NjaG9vbCBzZXJ2aWNlICh3aGV0aGVyIHBlcnNvbmFsIGluZm9ybWF0aW9uIG9yIG90aGVyd2lzZSkgZm9yIGJlaGF2aW9yYWwgdGFyZ2V0aW5nIG9mIGFkdmVydGlzZW1lbnRzIHRvIHN0dWRlbnRzLicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ05vdCBidWlsZCBhIHBlcnNvbmFsIHByb2ZpbGUgb2YgYSBzdHVkZW50IG90aGVyIHRoYW4gZm9yIHN1cHBvcnRpbmcgYXV0aG9yaXplZCBlZHVjYXRpb25hbC9zY2hvb2wgcHVycG9zZXMgb3IgYXMgYXV0aG9yaXplZCBieSB0aGUgcGFyZW50L3N0dWRlbnQuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnTm90IG1ha2UgbWF0ZXJpYWwgY2hhbmdlcyB0byBzY2hvb2wgc2VydmljZSBwcm92aWRlciBjb25zdW1lciBwcml2YWN5IHBvbGljaWVzIHdpdGhvdXQgZmlyc3QgcHJvdmlkaW5nIHByb21pbmVudCBub3RpY2UgdG8gdGhlIGFjY291bnQgaG9sZGVyKHMpIChpLmUuLCB0aGUgZWR1Y2F0aW9uYWwgaW5zdGl0dXRpb24vYWdlbmN5LCBvciB0aGUgcGFyZW50L3N0dWRlbnQgd2hlbiB0aGUgaW5mb3JtYXRpb24gaXMgY29sbGVjdGVkIGRpcmVjdGx5IGZyb20gdGhlIHN0dWRlbnQgd2l0aCBzdHVkZW50L3BhcmVudCBjb25zZW50KSBhbmQgYWxsb3dpbmcgdGhlbSBjaG9pY2VzIGJlZm9yZSBkYXRhIGlzIHVzZWQgaW4gYW55IG1hbm5lciBpbmNvbnNpc3RlbnQgd2l0aCB0ZXJtcyB0aGV5IHdlcmUgaW5pdGlhbGx5IHByb3ZpZGVkOyBhbmQgbm90IG1ha2UgbWF0ZXJpYWwgY2hhbmdlcyB0byBvdGhlciBwb2xpY2llcyBvciBwcmFjdGljZXMgZ292ZXJuaW5nIHRoZSB1c2Ugb2Ygc3R1ZGVudCBwZXJzb25hbCBpbmZvcm1hdGlvbiB0aGF0IGFyZSBpbmNvbnNpc3RlbnQgd2l0aCBjb250cmFjdHVhbCByZXF1aXJlbWVudHMuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnQ29sbGVjdCwgdXNlLCBzaGFyZSwgYW5kIHJldGFpbiBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uIG9ubHkgZm9yIHB1cnBvc2VzIGZvciB3aGljaCB3ZSB3ZXJlIGF1dGhvcml6ZWQgYnkgdGhlIGVkdWNhdGlvbmFsIGluc3RpdHV0aW9uL2FnZW5jeSwgdGVhY2hlciBvciB0aGUgcGFyZW50L3N0dWRlbnQuJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnRGlzY2xvc2UgY2xlYXJseSBpbiBjb250cmFjdHMgb3IgcHJpdmFjeSBwb2xpY2llcywgaW5jbHVkaW5nIGluIGEgbWFubmVyIGVhc3kgZm9yIHBhcmVudHMgdG8gdW5kZXJzdGFuZCwgd2hhdCB0eXBlcyBvZiBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uIHdlIGNvbGxlY3QsIGlmIGFueSwgYW5kIHRoZSBwdXJwb3NlcyBmb3Igd2hpY2ggdGhlIGluZm9ybWF0aW9uIHdlIG1haW50YWluIGlzIHVzZWQgb3Igc2hhcmVkIHdpdGggdGhpcmQgcGFydGllcy4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdTdXBwb3J0IGFjY2VzcyB0byBhbmQgY29ycmVjdGlvbiBvZiBzdHVkZW50IHBlcnNvbmFsbHkgaWRlbnRpZmlhYmxlIGluZm9ybWF0aW9uIGJ5IHRoZSBzdHVkZW50IG9yIHRoZWlyIGF1dGhvcml6ZWQgcGFyZW50LCBlaXRoZXIgYnkgYXNzaXN0aW5nIHRoZSBlZHVjYXRpb25hbCBpbnN0aXR1dGlvbiBpbiBtZWV0aW5nIGl0cyByZXF1aXJlbWVudHMgb3IgZGlyZWN0bHkgd2hlbiB0aGUgaW5mb3JtYXRpb24gaXMgY29sbGVjdGVkIGRpcmVjdGx5IGZyb20gdGhlIHN0dWRlbnQgd2l0aCBzdHVkZW50L3BhcmVudCBjb25zZW50LicsXG4gIH0sXG4gIHtcbiAgICBjb250ZW50OlxuICAgICAgJ01haW50YWluIGEgY29tcHJlaGVuc2l2ZSBzZWN1cml0eSBwcm9ncmFtIHRoYXQgaXMgcmVhc29uYWJseSBkZXNpZ25lZCB0byBwcm90ZWN0IHRoZSBzZWN1cml0eSwgcHJpdmFjeSwgY29uZmlkZW50aWFsaXR5LCBhbmQgaW50ZWdyaXR5IG9mIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24gYWdhaW5zdCByaXNrcyDigJMgc3VjaCBhcyB1bmF1dGhvcml6ZWQgYWNjZXNzIG9yIHVzZSwgb3IgdW5pbnRlbmRlZCBvciBpbmFwcHJvcHJpYXRlIGRpc2Nsb3N1cmUg4oCTIHRocm91Z2ggdGhlIHVzZSBvZiBhZG1pbmlzdHJhdGl2ZSwgdGVjaG5vbG9naWNhbCwgYW5kIHBoeXNpY2FsIHNhZmVndWFyZHMgYXBwcm9wcmlhdGUgdG8gdGhlIHNlbnNpdGl2aXR5IG9mIHRoZSBpbmZvcm1hdGlvbi4nLFxuICB9LFxuICB7XG4gICAgY29udGVudDpcbiAgICAgICdSZXF1aXJlIHRoYXQgb3VyIHZlbmRvcnMgd2l0aCB3aG9tIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24gaXMgc2hhcmVkIGluIG9yZGVyIHRvIGRlbGl2ZXIgdGhlIGVkdWNhdGlvbmFsIHNlcnZpY2UsIGlmIGFueSwgYXJlIG9ibGlnYXRlZCB0byBpbXBsZW1lbnQgdGhlc2Ugc2FtZSBjb21taXRtZW50cyBmb3IgdGhlIGdpdmVuIHN0dWRlbnQgcGVyc29uYWwgaW5mb3JtYXRpb24uJyxcbiAgfSxcbiAge1xuICAgIGNvbnRlbnQ6XG4gICAgICAnQWxsb3cgYSBzdWNjZXNzb3IgZW50aXR5IHRvIG1haW50YWluIHRoZSBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uLCBpbiB0aGUgY2FzZSBvZiBvdXIgbWVyZ2VyIG9yIGFjcXVpc2l0aW9uIGJ5IGFub3RoZXIgZW50aXR5LCBwcm92aWRlZCB0aGUgc3VjY2Vzc29yIGVudGl0eSBpcyBzdWJqZWN0IHRvIHRoZXNlIHNhbWUgY29tbWl0bWVudHMgZm9yIHRoZSBwcmV2aW91c2x5IGNvbGxlY3RlZCBzdHVkZW50IHBlcnNvbmFsIGluZm9ybWF0aW9uLicsXG4gIH0sXG5dO1xuXG5jbGFzcyBQcml2YWN5QW5kVGVybXMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBSZWFjdEdBLmluaXRpYWxpemUod2luZG93LkFwcC5nb29nbGVUcmFja2luZ0lkLCB7XG4gICAgICBkZWJ1ZzogZmFsc2UsXG4gICAgfSk7XG4gICAgUmVhY3RHQS5wYWdldmlldyh3aW5kb3cubG9jYXRpb24uaHJlZik7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJhbm5lclNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmd9PlByaXZhY3kgUG9saWN5PC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbnRyb0NvbnRlbnR9PlxuICAgICAgICAgIHtpbnRyb0NvbnRlbnQubWFwKGNvbnRlbnQgPT4gKFxuICAgICAgICAgICAgPGRpdj57Y29udGVudC5jb250ZW50fTwvZGl2PlxuICAgICAgICAgICkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tbWl0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb21taXRIZWFkaW5nfT5XZSBDb21taXQgVG86PC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29tbWl0Q29udGVudH0+XG4gICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgIHtjb21taXRDb250ZW50Lm1hcChjb250ZW50ID0+IChcbiAgICAgICAgICAgICAgICA8bGk+e2NvbnRlbnQuY29udGVudH08L2xpPlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIFdlIHVzZSBHb29nbGUgQW5hbHl0aWNzIGFzIGRlc2NyaWJlZCBpblxuICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9wb2xpY2llcy5nb29nbGUuY29tL3RlY2hub2xvZ2llcy9wYXJ0bmVyLXNpdGVzXCJcbiAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICBzdHlsZT17eyB0ZXh0RGVjb3JhdGlvbjogJ3VuZGVybGluZScgfX1cbiAgICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICZxdW90O0hvdyBHb29nbGUgdXNlcyBkYXRhIHdoZW4geW91IHVzZSBvdXIgcGFydG5lciZhcG9zO3NcbiAgICAgICAgICAgICAgICAgIHNpdGVzIG9yIGFwcHMuJnF1b3Q7XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIFlvdSBjYW4gcHJ2ZW50IHlvdXIgZGF0YSBmcm9tIGJlaW5nIHVzZWQgYnkgR29vZ2xlIEFuYWx5dGljcyBvblxuICAgICAgICAgICAgICAgIG91ciB3ZWJzaXRlcyBieSBpbnN0YWxsaW5nIHRoZSBHb29nbGUgQW5hbHl0aWNzIG9wdC1vdXQgYnJvd3NlclxuICAgICAgICAgICAgICAgIGFkZC1vbi4gRm9yIGVuaGFuY2VkIHByaXZhY3kgcHVycG9zZXMsd2UgYWxzbyBlbXBsb3kgSVAgYWRkcmVzc1xuICAgICAgICAgICAgICAgIG1hc2tpbmcsIGEgdGVjaG5pcXVlIHVzZWQgdG8gdHJ1bmNhdGUgSVAgYWRkcmVzc2VzIGNvbGxlY3RlZCBieVxuICAgICAgICAgICAgICAgIEdvb2dsZSBBbmFseXRpY3MgYW5kIHN0b3JlIHRoZW0gaW4gYW4gYWJicmV2aWF0ZWQgZm9ybSB0b1xuICAgICAgICAgICAgICAgIHByZXZlbnQgdGhlbSBmcm9tIGJlaW5nIHRyYWNlZCBiYWNrIHRvIGluZGl2aWR1YWwgdXNlcnMuXG4gICAgICAgICAgICAgICAgUG9ydGlvbnMgb2Ygb3VyIHdlYnNpdGUgbWF5IGFsc28gdXNlIEdvb2dsZSBBbmFseXRpY3MgZm9yXG4gICAgICAgICAgICAgICAgRGlzcGxheSBBZHZlcnRpc2VycyBpbmNsdWRpbmcgRG91YmxlQ2xpY2sgb3IgRHluYW1pYyBSZW1hcmtldGluZ1xuICAgICAgICAgICAgICAgIHdoaWNoIHByb3ZpZGUgaW50ZXJlc3QtYmFzZWQgYWRzIGJhc2VkIG9uIHlvdXIgdmlzaXQgdG8gdGhpcyBvclxuICAgICAgICAgICAgICAgIG90aGVyIHdlYnNpdGVzLiBZb3UgY2FuIHVzZSBBZHMgU2V0dGluZ3MgdG8gbWFuYWdlIHRoZSBHb29nbGVcbiAgICAgICAgICAgICAgICBhZHMgeW91IHNlZSBhbmQgb3B0LW91dCBvZiBpbnRlcmVzdC1iYXNlZCBhZHMuIFdlIGFsc28gdXNlIEFkb2JlXG4gICAgICAgICAgICAgICAgTWFya2V0aW5nIENsb3VkIGFzIGRlc2NyaWJlZC5Zb3UgY2FuIHNpbWlsYXJ5IGV4ZXJjaXNlIHlvdXJcbiAgICAgICAgICAgICAgICByaWdodHMgd2l0aCByZXNwZWN0IHRvIHVzZSBvZiB0aGlzIGRhdGEgYXMgZGVzY3JpYmVkIGluIHRoZVxuICAgICAgICAgICAgICAgICZxdW90O0V4ZXJjaXNpbmcgQ2hvaWNlJnF1b3Q7IHNlY3Rpb24gYmVsb3cuXG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICBHb29nbGUgYW5kIGl0cyBBZmZpbGlhdGVzIG1heSByZXRhaW4gYW5kIHVzZSwgc3ViamVjdCB0byB0aGVcbiAgICAgICAgICAgICAgICB0ZXJtcyBvZiBpdHMgcHJpdmFjeSBwb2xpY3kgKGxvY2F0ZWQgYXRcbiAgICAgICAgICAgICAgICB3d3cuZ29vZ2xlLmNvbS9wcml2YWN5Lmh0bWwpLCBpbmZvcm1hdGlvbiBjb2xsZWN0ZWQgaW4gWW91ciB1c2VcbiAgICAgICAgICAgICAgICBvZiB0aGUgU2VydmljZS4gR29vZ2xlIHdpbGwgbm90IHNoYXJlWW91ciBDdXN0b21lciBEYXRhIG9yIGFueVxuICAgICAgICAgICAgICAgIFRoaXJkIFBhcnR5JmFwb3M7cyBDdXN0b21lciBEYXRhIHdpdGggYW55IHRoaXJkIHBhcnRpZXMgdW5sZXNzXG4gICAgICAgICAgICAgICAgR29vZ2xlIChpKSBoYXMgWW91ciBjb25zZW50IGZvciBhbnkgQ3VzdG9tZXIgRGF0YSBvciBhbnkgVGhpcmRcbiAgICAgICAgICAgICAgICBQYXJ0eSZhcG9zO3MgY29uc2VudCBmb3IgdGhlIFRoaXJkIFBhcnR5JmFwb3M7cyBDdXN0b21lciBEYXRhO1xuICAgICAgICAgICAgICAgIChpaSkgY29uY2x1ZGVzIHRoYXQgaXQgaXMgcmVxdWlyZWQgYnkgbGF3IG9yIGhhcyBhIGdvb2dsZSBmYWl0aFxuICAgICAgICAgICAgICAgIGJlbGllZiB0aGF0IGFjY2VzcywgcHJlc2VydmF0aW9uIG9yIGRpc2Nsb3N1cmUgb2YgQ3VzdG9tZXIgRGF0YVxuICAgICAgICAgICAgICAgIGlzIHJlYXNvbmFibHkgbmVjZXNzYXJ5IHRvIHByb3RlY3QgdGhlIHJpZ2h0cywgcHJvcGVydHkgb3JcbiAgICAgICAgICAgICAgICBzYWZ0ZXkgb2YgR29vZ2xlLCBpdHMgdXNlcnMgb3IgdGhlIHB1YmxpYzsgb3IgKGlpaSkgcHJvdmlkZXNcbiAgICAgICAgICAgICAgICBDdXN0b21lciBEYXRhIGluIGNlcnRhaW4gbGltaXRlZCBjaXJjdW1zdGFuY2VzIHRvIHRoaXJkIHBhcnRpZXNcbiAgICAgICAgICAgICAgICB0byBjYXJyeSBvdXQgdGFza3Mgb24gR29vZ2xlJmFwb3M7cyBiZWhhbGYoZS5nLiwgYmlsbGluZyBvciBkYXRhXG4gICAgICAgICAgICAgICAgc3RvcmFnZSkgd2l0aCBzdHJpY3QgcmVzdHJpY3Rpb25zIHRoYXQgcHJldmVudCB0aGUgZGF0YSBmcm9tXG4gICAgICAgICAgICAgICAgYmVpbmcgdXNlZCBvciBzaGFyZWQgZXhjZXB0IGFzIGRpcmVjdGVkIGJ5IEdvb2dsZS4gV2hlbiB0aGlzIGlzXG4gICAgICAgICAgICAgICAgZG9uZSwgaXQgaXMgc3ViamVjdCB0byBhZ3JlZW1lbnRzIHRoYXQgb2JsaWdlIHRob3NlIHBhcnRpZXMgdG9cbiAgICAgICAgICAgICAgICBwcm9jZXNzIEN1c3RvbWVyIERhdGEgb25seSBvbiBHb29nbGUmYXBvcztzIGluc3RydWN0aW9ucyBhbmQgaW5cbiAgICAgICAgICAgICAgICBjb21wbGlhbmNlIHdpdGggdGhpcyBBZ3JlZW1lbnQgYW5kIGFwcHJvcHJpYXRlIGNvbmZpZGVudGlhbGl0eVxuICAgICAgICAgICAgICAgIGFuZCBzZWN1cml0eSBtZWFzdXJlcy5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFByaXZhY3lBbmRUZXJtcyk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1ByaXZhY3lBbmRUZXJtcy5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vUHJpdmFjeUFuZFRlcm1zLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcml2YWN5QW5kVGVybXMgZnJvbSAnLi9Qcml2YWN5QW5kVGVybXMnO1xuaW1wb3J0IExheW91dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xheW91dCc7XG5cbmFzeW5jIGZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ1ByaXZhY3kgUG9saWN5IHwgRWduaWZ5JyxcbiAgICBjaHVua3M6IFsnUHJpdmFjeUFuZFRlcm1zJ10sXG4gICAgY29udGVudDogJ0dldCB0byBrbm93IGFib3V0IG91ciBwcml2YWN5IHBvbGljeS4nLFxuICAgIGtleXdvcmRzOiAncHJpdmFjeSBwb2xpY3knLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPFByaXZhY3lBbmRUZXJtcyAvPlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKSxcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFLQTtBQURBO0FBS0E7QUFEQTtBQUtBO0FBREE7QUFDQTtBQUtBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF3QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMEJBOzs7O0FBOUVBO0FBQ0E7QUFnRkE7Ozs7Ozs7QUNySkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FZQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBY0E7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==