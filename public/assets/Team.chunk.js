(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Team"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/team/Team.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Team-heading-3CBSV {\n  font-size: 30px;\n  font-weight: 600;\n  letter-spacing: 1.3px;\n  color: #3e3e5f;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n}\n\n.Team-bannerSection-a9i4l {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: -o-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: linear-gradient(258deg, #ffae71, #ff315e);\n  padding-top: 64px;\n  padding-top: 4rem;\n}\n\n.Team-bannerSection-a9i4l .Team-heading-3CBSV {\n    font-size: 48px;\n    font-size: 3rem;\n    font-weight: 300;\n    color: #fff;\n  }\n\n.Team-bannerSection-a9i4l .Team-description-I_ug0 {\n    font-size: 24px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: normal;\n    letter-spacing: 1px;\n    color: #fff;\n    margin-top: 40px;\n    margin-top: 2.5rem;\n  }\n\n.Team-teamSection-1pCr7 {\n  padding-top: 48px;\n  padding-top: 3rem;\n  padding-bottom: 48px;\n  padding-bottom: 3rem;\n  background: #fff;\n}\n\n.Team-tagLine-2Wznz {\n  font-size: 12px;\n  line-height: 21px;\n  color: #5f6368;\n}\n\n@media only screen and (min-width: 960px) {\n  .Team-adviceMemberWrapper-2mqPb:nth-child(1) {\n    padding-left: 0 !important;\n  }\n\n  .Team-adviceMemberWrapper-2mqPb:nth-child(2) {\n    padding-right: 0 !important;\n  }\n}\n\n.Team-advisoryMemberCard-3sYFo {\n  margin-bottom: 36px;\n  margin-bottom: 2.25rem;\n  min-height: 200px;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n}\n\n.Team-advisoryMemberCard-3sYFo img {\n    width: 200px;\n    height: 200px;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-info-3Y35F {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n    width: calc(100% - 200px);\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-name-3STtX {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.Team-advisoryMemberCard-3sYFo .Team-tagLine-2Wznz {\n    font-size: 12px;\n    height: 24;\n    color: #5f6368;\n    margin-top: 32px;\n    margin-top: 2rem;\n  }\n\n@media only screen and (max-width: 959px) {\n    .Team-teamSection-1pCr7 .Team-heading-3CBSV {\n      text-align: center !important;\n    }\n\n  .Team-advisoryMemberCard-3sYFo {\n    min-height: 150px;\n  }\n\n    .Team-advisoryMemberCard-3sYFo img {\n      width: 150px;\n      height: 150px;\n      -o-object-fit: cover;\n         object-fit: cover;\n      -o-object-position: 100% 0;\n         object-position: 100% 0;\n    }\n\n    .Team-advisoryMemberCard-3sYFo .Team-info-3Y35F {\n      padding: 1.5rem 1rem;\n      width: calc(100% - 150px);\n    }\n}\n\n.Team-teamMemberCard-32YcH {\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.Team-teamMemberCard-32YcH img {\n    height: 200px;\n    width: 200px;\n    border-radius: 50%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.Team-teamMemberCard-32YcH .Team-info-3Y35F {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n  }\n\n.Team-teamMemberCard-32YcH .Team-name-3STtX {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.Team-teamMemberCard-32YcH .Team-title-1fhFk {\n    margin-top: 8px;\n    margin-top: 0.5rem;\n    font-size: 20px;\n    color: #5f6368;\n  }\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/Team.scss"],"names":[],"mappings":"AAAA;EACE,gBAAgB;EAChB,iBAAiB;EACjB,sBAAsB;EACtB,eAAe;EACf,oBAAoB;EACpB,sBAAsB;CACvB;;AAED;EACE,oBAAoB;EACpB,kBAAkB;EAClB,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,kBAAkB;EAClB,kBAAkB;CACnB;;AAED;IACI,gBAAgB;IAChB,gBAAgB;IAChB,iBAAiB;IACjB,YAAY;GACb;;AAEH;IACI,gBAAgB;IAChB,oBAAoB;IACpB,mBAAmB;IACnB,qBAAqB;IACrB,oBAAoB;IACpB,oBAAoB;IACpB,YAAY;IACZ,iBAAiB;IACjB,mBAAmB;GACpB;;AAEH;EACE,kBAAkB;EAClB,kBAAkB;EAClB,qBAAqB;EACrB,qBAAqB;EACrB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;CAChB;;AAED;EACE;IACE,2BAA2B;GAC5B;;EAED;IACE,4BAA4B;GAC7B;CACF;;AAED;EACE,oBAAoB;EACpB,uBAAuB;EACvB,kBAAkB;EAClB,mBAAmB;EACnB,uBAAuB;EACvB,oDAAoD;UAC5C,4CAA4C;CACrD;;AAED;IACI,aAAa;IACb,cAAc;IACd,qBAAqB;OAClB,kBAAkB;IACrB,2BAA2B;OACxB,wBAAwB;GAC5B;;AAEH;IACI,mBAAmB;IACnB,qBAAqB;IACrB,0BAA0B;GAC3B;;AAEH;IACI,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;GAChB;;AAEH;IACI,gBAAgB;IAChB,WAAW;IACX,eAAe;IACf,iBAAiB;IACjB,iBAAiB;GAClB;;AAEH;IACI;MACE,8BAA8B;KAC/B;;EAEH;IACE,kBAAkB;GACnB;;IAEC;MACE,aAAa;MACb,cAAc;MACd,qBAAqB;SAClB,kBAAkB;MACrB,2BAA2B;SACxB,wBAAwB;KAC5B;;IAED;MACE,qBAAqB;MACrB,0BAA0B;KAC3B;CACJ;;AAED;EACE,oBAAoB;EACpB,oBAAoB;EACpB,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;CACpB;;AAED;IACI,cAAc;IACd,aAAa;IACb,mBAAmB;IACnB,qBAAqB;OAClB,kBAAkB;IACrB,2BAA2B;OACxB,wBAAwB;GAC5B;;AAEH;IACI,mBAAmB;IACnB,qBAAqB;GACtB;;AAEH;IACI,gBAAgB;IAChB,iBAAiB;IACjB,eAAe;GAChB;;AAEH;IACI,gBAAgB;IAChB,mBAAmB;IACnB,gBAAgB;IAChB,eAAe;GAChB","file":"Team.scss","sourcesContent":[".heading {\n  font-size: 30px;\n  font-weight: 600;\n  letter-spacing: 1.3px;\n  color: #3e3e5f;\n  margin-bottom: 40px;\n  margin-bottom: 2.5rem;\n}\n\n.bannerSection {\n  background: #3e3e5f;\n  min-height: 366px;\n  background-image: -webkit-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: -o-linear-gradient(192deg, #ffae71, #ff315e);\n  background-image: linear-gradient(258deg, #ffae71, #ff315e);\n  padding-top: 64px;\n  padding-top: 4rem;\n}\n\n.bannerSection .heading {\n    font-size: 48px;\n    font-size: 3rem;\n    font-weight: 300;\n    color: #fff;\n  }\n\n.bannerSection .description {\n    font-size: 24px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: normal;\n    letter-spacing: 1px;\n    color: #fff;\n    margin-top: 40px;\n    margin-top: 2.5rem;\n  }\n\n.teamSection {\n  padding-top: 48px;\n  padding-top: 3rem;\n  padding-bottom: 48px;\n  padding-bottom: 3rem;\n  background: #fff;\n}\n\n.tagLine {\n  font-size: 12px;\n  line-height: 21px;\n  color: #5f6368;\n}\n\n@media only screen and (min-width: 960px) {\n  .adviceMemberWrapper:nth-child(1) {\n    padding-left: 0 !important;\n  }\n\n  .adviceMemberWrapper:nth-child(2) {\n    padding-right: 0 !important;\n  }\n}\n\n.advisoryMemberCard {\n  margin-bottom: 36px;\n  margin-bottom: 2.25rem;\n  min-height: 200px;\n  border-radius: 4px;\n  background-color: #fff;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);\n}\n\n.advisoryMemberCard img {\n    width: 200px;\n    height: 200px;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.advisoryMemberCard .info {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n    width: calc(100% - 200px);\n  }\n\n.advisoryMemberCard .name {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.advisoryMemberCard .tagLine {\n    font-size: 12px;\n    height: 24;\n    color: #5f6368;\n    margin-top: 32px;\n    margin-top: 2rem;\n  }\n\n@media only screen and (max-width: 959px) {\n    .teamSection .heading {\n      text-align: center !important;\n    }\n\n  .advisoryMemberCard {\n    min-height: 150px;\n  }\n\n    .advisoryMemberCard img {\n      width: 150px;\n      height: 150px;\n      -o-object-fit: cover;\n         object-fit: cover;\n      -o-object-position: 100% 0;\n         object-position: 100% 0;\n    }\n\n    .advisoryMemberCard .info {\n      padding: 1.5rem 1rem;\n      width: calc(100% - 150px);\n    }\n}\n\n.teamMemberCard {\n  margin-bottom: 32px;\n  margin-bottom: 2rem;\n  border-radius: 4px;\n  background-color: #fff;\n  text-align: center;\n}\n\n.teamMemberCard img {\n    height: 200px;\n    width: 200px;\n    border-radius: 50%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: 100% 0;\n       object-position: 100% 0;\n  }\n\n.teamMemberCard .info {\n    padding: 24px 32px;\n    padding: 1.5rem 2rem;\n  }\n\n.teamMemberCard .name {\n    font-size: 24px;\n    font-weight: 600;\n    color: #3e3e5f;\n  }\n\n.teamMemberCard .title {\n    margin-top: 8px;\n    margin-top: 0.5rem;\n    font-size: 20px;\n    color: #5f6368;\n  }\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"heading": "Team-heading-3CBSV",
	"bannerSection": "Team-bannerSection-a9i4l",
	"description": "Team-description-I_ug0",
	"teamSection": "Team-teamSection-1pCr7",
	"tagLine": "Team-tagLine-2Wznz",
	"adviceMemberWrapper": "Team-adviceMemberWrapper-2mqPb",
	"advisoryMemberCard": "Team-advisoryMemberCard-3sYFo",
	"info": "Team-info-3Y35F",
	"name": "Team-name-3STtX",
	"teamMemberCard": "Team-teamMemberCard-32YcH",
	"title": "Team-title-1fhFk"
};

/***/ }),

/***/ "./src/routes/team/Team.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var components_RequestDemo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/RequestDemo/RequestDemo.js");
/* harmony import */ var _Team_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/team/Team.scss");
/* harmony import */ var _Team_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Team_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/Team.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import cx from 'classnames';



 // import Link from 'components/Link';


var ADVISORY_BOARD = [{
  name: 'M.Y.S. Prasad',
  tagLine: 'Padmashri Awardee',
  detail1: 'Vice Chancellor, Vignan University',
  detail2: 'Former Director- SDSC, ISRO',
  image: '/team/Padma Shri M.Y.S.Prasad.jpg',
  url: 'https://en.wikipedia.org/wiki/M._Y._S._Prasad'
}, {
  name: 'Dr. T. Hanuman Chowdary',
  tagLine: 'Padmashri Awardee',
  detail1: 'Former IT Advisor, AP Govt.',
  detail2: 'Fellow, Tata Consulting Services (TCS)',
  image: '/team/Padma Shri Dr.T.Hanuman Chowdary.JPG',
  url: 'https://www.bloomberg.com/research/stocks/people/person.asp?personId=765564&privcapId=133103'
}];
var TEAMS = [{
  name: 'Advisory Board',
  members: [{
    name: 'M.Y.S. Prasad',
    title: 'Padmashri Awardee',
    detail1: 'Vice Chancellor, Vignan University',
    detail2: 'Former Director- SDSC, ISRO',
    image: '/team/M.Y.S. Prasad.png'
    /* linkedIn: 'https://en.wikipedia.org/wiki/M._Y._S._Prasad', */

  }, {
    name: 'Dr. T. Hanuman Chowdary',
    title: 'Padmashri Awardee',
    detail1: 'Former IT Advisor, AP Govt.',
    detail2: 'Fellow, Tata Consulting Services (TCS)',
    image: '/team/Padma Shri Dr.T.Hanuman Chowdary.png'
    /* linkedIn:
      'https://www.bloomberg.com/research/stocks/people/person.asp?personId=765564&privcapId=133103', */

  }]
}, {
  name: 'Leadership',
  members: [{
    name: 'Kiran Babu',
    title: 'Founder & CEO',
    image: '/team/Kiran-Babu.png',
    linkedIn: 'https://www.linkedin.com/in/yerranagu/'
  }, {
    name: 'Anjan Goswami',
    title: 'Product',
    image: '/team/Anjan-Goswami.png',
    linkedIn: 'https://www.linkedin.com/in/anjangoswami/'
  }, {
    name: 'Nikhil Sharma',
    title: 'Engineering',
    image: '/team/Nikhil-Sharma.png',
    linkedIn: 'https://www.linkedin.com/in/nykhylsharma/'
  }, {
    name: 'Shiva Krishna',
    title: 'Engineering',
    image: '/team/Shiva.jpg',
    linkedIn: 'https://www.linkedin.com/in/shivakrishnaah/'
  }, {
    name: 'ChandraSekhar',
    title: 'Operations',
    image: '/team/CHANDRA.jpg',
    linkedIn: 'https://www.linkedin.com/in/chandrasekkhar-m-v-b1762b13/'
  }]
}, {
  name: 'Team',
  members: [{
    name: 'LaxmanRao',
    image: '/team/LAXMAN.jpg',
    title: 'Sales'
  }, {
    name: 'Archana',
    image: '/team/Archana.jpg',
    title: 'Sales'
  }, {
    name: 'Vandana',
    image: '/team/Vandana.jpg',
    title: 'Sales'
  }, {
    name: 'Pranab Sarkar',
    title: 'Design',
    image: '/team/pranab.jpg'
    /* linkedIn: 'https://www.linkedin.com/in/pranab-sarkar-b5496699/', */

  }, {
    name: 'Srujan Sagar',
    title: 'Design',
    image: '/team/Srujan-Sagar.png'
    /* linkedIn: 'https://www.linkedin.com/in/srujansagar/', */

  }, {
    name: 'Srikant',
    image: '/team/Srikant 2.jpg',
    title: 'Design'
  }, {
    name: 'Sudhir Suguru',
    title: 'Talent Acquisition',
    image: '/team/SUDHIR.jpg'
    /* linkedIn: 'https://www.linkedin.com/in/sudhirsuguru/', */

  }, {
    name: 'Hari Krishna Salver',
    title: 'Engineering',
    image: '/team/Hari-Krishna-Salver.png'
    /* linkedIn: 'https://www.linkedin.com/in/harikrishnasalver/', */

  }, {
    name: 'Biswa Bhusan',
    image: '/team/biswa.jpg',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/biswa-bhusan-soren-a66583137/', */

  }, {
    name: 'Aslam Shaik',
    title: 'Engineering',
    image: '/team/Aslam-Shaik.png'
    /* linkedIn: 'https://www.linkedin.com/in/aslam-shaik-39a114135/', */

  }, {
    name: 'Vikram Nethi',
    image: '/team/vikram.png',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/vikram-nethi-bb5a88b5', */

  }, {
    name: 'Mohit Agarwal',
    image: '/team/Mohit.jpg',
    title: 'Engineering'
    /* linkedIn: 'https://www.linkedin.com/in/mohit-agarwal-03/', */

  }, {
    name: 'Srikanth',
    image: '/team/Srikant.jpg',
    title: 'Engineering' // linkedIn: 'https://angel.co/lonewolf3739',

  }, {
    name: 'Vikram Somavaram',
    image: '/team/VikramS.jpg',
    title: 'Engineering' // linkedIn: 'https://www.linkedin.com/in/vikram-somavaram-48a15165/',

  }, {
    name: 'Narasimha Maddi',
    title: 'Operations',
    image: '/team/Narasimha-Maddi.png'
  }]
}];

var Team =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Team, _React$Component);

  function Team() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Team);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Team)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "displayAdvisoryMembersSection", function () {
      var view = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row hide",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, "Advisory Board"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, ADVISORY_BOARD.map(function (member) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col xs12 s6 ".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.adviceMemberWrapper),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 193
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.advisoryMemberCard),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 194
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col no-padding",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 195
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: member.url,
          target: "_blank",
          rel: "noreferrer noopener",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 196
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: member.image ? "/images/".concat(member.image) : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
          alt: "",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 201
          },
          __self: this
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col ".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.info),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 211
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.name,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 212
          },
          __self: this
        }, member.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 213
          },
          __self: this
        }, member.tagLine), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 214
          },
          __self: this
        }, " ", member.detail1), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.tagLine,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 215
          },
          __self: this
        }, " ", member.detail2))));
      })));
      return view;
    });

    _defineProperty(_assertThisInitialized(_this), "displayTeamsSection", function () {
      var view = TEAMS.map(function (team) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "row",
          style: {
            marginTop: '2rem'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 228
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 229
          },
          __self: this
        }, team.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "row",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 230
          },
          __self: this
        }, team.members.map(function (member) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: member.name === 'M.Y.S. Prasad' || member.name === 'Dr. T. Hanuman Chowdary' ? "col xs12 s6 ".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamMemberCard) : "col xs12 s4 ".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamMemberCard),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 232
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "col xs12 l12 no-padding",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 240
            },
            __self: this
          }, member.linkedIn ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
            href: member.linkedIn,
            target: "_blank",
            rel: "noreferrer noopener",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 242
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: member.image ? "/images/".concat(member.image) : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
            alt: "",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 247
            },
            __self: this
          })) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: member.image ? "/images/".concat(member.image) : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
            alt: "",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 257
            },
            __self: this
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "col xs12 s12 ".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.info),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 267
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.name, " hide-overflow"),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 268
            },
            __self: this
          }, member.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 269
            },
            __self: this
          }, member.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 270
            },
            __self: this
          }, " ", member.detail1), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.title,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 271
            },
            __self: this
          }, " ", member.detail2)));
        })));
      });
      return view;
    });

    return _this;
  }

  _createClass(Team, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      react_ga__WEBPACK_IMPORTED_MODULE_1__["default"].initialize(window.App.googleTrackingId, {
        debug: false
      });
      react_ga__WEBPACK_IMPORTED_MODULE_1__["default"].pageview(window.location.href);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 283
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.bannerSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 284
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 285
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 286
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col xs12 l6 no-padding",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 288
        },
        __self: this
      }, "Meet the team behind this"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.description,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289
        },
        __self: this
      }, "We believe work is exhilarating when you're surrounded by\n                   extraordinary people who embrace being part of a team."))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
        className: "".concat(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a.teamSection, " container"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 297
        },
        __self: this
      }, this.displayAdvisoryMembersSection(), this.displayTeamsSection()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_RequestDemo__WEBPACK_IMPORTED_MODULE_3__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 301
        },
        __self: this
      }));
    }
  }]);

  return Team;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_Team_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(Team));

/***/ }),

/***/ "./src/routes/team/Team.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/team/Team.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/team/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Team__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/team/Team.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Layout/Layout.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/team/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify',
              chunks: ['Team'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Team__WEBPACK_IMPORTED_MODULE_1__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGVhbS5jaHVuay5qcyIsInNvdXJjZXMiOlsiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvdGVhbS9UZWFtLnNjc3MiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy90ZWFtL1RlYW0uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy90ZWFtL1RlYW0uc2Nzcz83ZTk1IiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvdGVhbS9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLlRlYW0taGVhZGluZy0zQ0JTViB7XFxuICBmb250LXNpemU6IDMwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDEuM3B4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMi41cmVtO1xcbn1cXG5cXG4uVGVhbS1iYW5uZXJTZWN0aW9uLWE5aTRsIHtcXG4gIGJhY2tncm91bmQ6ICMzZTNlNWY7XFxuICBtaW4taGVpZ2h0OiAzNjZweDtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDE5MmRlZywgI2ZmYWU3MSwgI2ZmMzE1ZSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQoMTkyZGVnLCAjZmZhZTcxLCAjZmYzMTVlKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyNThkZWcsICNmZmFlNzEsICNmZjMxNWUpO1xcbiAgcGFkZGluZy10b3A6IDY0cHg7XFxuICBwYWRkaW5nLXRvcDogNHJlbTtcXG59XFxuXFxuLlRlYW0tYmFubmVyU2VjdGlvbi1hOWk0bCAuVGVhbS1oZWFkaW5nLTNDQlNWIHtcXG4gICAgZm9udC1zaXplOiA0OHB4O1xcbiAgICBmb250LXNpemU6IDNyZW07XFxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgfVxcblxcbi5UZWFtLWJhbm5lclNlY3Rpb24tYTlpNGwgLlRlYW0tZGVzY3JpcHRpb24tSV91ZzAge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgICBtYXJnaW4tdG9wOiAyLjVyZW07XFxuICB9XFxuXFxuLlRlYW0tdGVhbVNlY3Rpb24tMXBDcjcge1xcbiAgcGFkZGluZy10b3A6IDQ4cHg7XFxuICBwYWRkaW5nLXRvcDogM3JlbTtcXG4gIHBhZGRpbmctYm90dG9tOiA0OHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDNyZW07XFxuICBiYWNrZ3JvdW5kOiAjZmZmO1xcbn1cXG5cXG4uVGVhbS10YWdMaW5lLTJXem56IHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMXB4O1xcbiAgY29sb3I6ICM1ZjYzNjg7XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTYwcHgpIHtcXG4gIC5UZWFtLWFkdmljZU1lbWJlcldyYXBwZXItMm1xUGI6bnRoLWNoaWxkKDEpIHtcXG4gICAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XFxuICB9XFxuXFxuICAuVGVhbS1hZHZpY2VNZW1iZXJXcmFwcGVyLTJtcVBiOm50aC1jaGlsZCgyKSB7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDAgIWltcG9ydGFudDtcXG4gIH1cXG59XFxuXFxuLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIHtcXG4gIG1hcmdpbi1ib3R0b206IDM2cHg7XFxuICBtYXJnaW4tYm90dG9tOiAyLjI1cmVtO1xcbiAgbWluLWhlaWdodDogMjAwcHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDJweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDJweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xcbn1cXG5cXG4uVGVhbS1hZHZpc29yeU1lbWJlckNhcmQtM3NZRm8gaW1nIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgIC1vLW9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgICAgICBvYmplY3QtcG9zaXRpb246IDEwMCUgMDtcXG4gIH1cXG5cXG4uVGVhbS1hZHZpc29yeU1lbWJlckNhcmQtM3NZRm8gLlRlYW0taW5mby0zWTM1RiB7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgcGFkZGluZzogMS41cmVtIDJyZW07XFxuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAyMDBweCk7XFxuICB9XFxuXFxuLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIC5UZWFtLW5hbWUtM1NUdFgge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgfVxcblxcbi5UZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGbyAuVGVhbS10YWdMaW5lLTJXem56IHtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBoZWlnaHQ6IDI0O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gICAgbWFyZ2luLXRvcDogMzJweDtcXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcXG4gIH1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk1OXB4KSB7XFxuICAgIC5UZWFtLXRlYW1TZWN0aW9uLTFwQ3I3IC5UZWFtLWhlYWRpbmctM0NCU1Yge1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xcbiAgICB9XFxuXFxuICAuVGVhbS1hZHZpc29yeU1lbWJlckNhcmQtM3NZRm8ge1xcbiAgICBtaW4taGVpZ2h0OiAxNTBweDtcXG4gIH1cXG5cXG4gICAgLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIGltZyB7XFxuICAgICAgd2lkdGg6IDE1MHB4O1xcbiAgICAgIGhlaWdodDogMTUwcHg7XFxuICAgICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgLW8tb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgICAgICAgb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgIH1cXG5cXG4gICAgLlRlYW0tYWR2aXNvcnlNZW1iZXJDYXJkLTNzWUZvIC5UZWFtLWluZm8tM1kzNUYge1xcbiAgICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAxNTBweCk7XFxuICAgIH1cXG59XFxuXFxuLlRlYW0tdGVhbU1lbWJlckNhcmQtMzJZY0gge1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uVGVhbS10ZWFtTWVtYmVyQ2FyZC0zMlljSCBpbWcge1xcbiAgICBoZWlnaHQ6IDIwMHB4O1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAtby1vYmplY3QtcG9zaXRpb246IDEwMCUgMDtcXG4gICAgICAgb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICB9XFxuXFxuLlRlYW0tdGVhbU1lbWJlckNhcmQtMzJZY0ggLlRlYW0taW5mby0zWTM1RiB7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgcGFkZGluZzogMS41cmVtIDJyZW07XFxuICB9XFxuXFxuLlRlYW0tdGVhbU1lbWJlckNhcmQtMzJZY0ggLlRlYW0tbmFtZS0zU1R0WCB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgY29sb3I6ICMzZTNlNWY7XFxuICB9XFxuXFxuLlRlYW0tdGVhbU1lbWJlckNhcmQtMzJZY0ggLlRlYW0tdGl0bGUtMWZoRmsge1xcbiAgICBtYXJnaW4tdG9wOiA4cHg7XFxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICBjb2xvcjogIzVmNjM2ODtcXG4gIH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvdGVhbS9UZWFtLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIsb0VBQW9FO0VBQ3BFLCtEQUErRDtFQUMvRCw0REFBNEQ7RUFDNUQsa0JBQWtCO0VBQ2xCLGtCQUFrQjtDQUNuQjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFlBQVk7R0FDYjs7QUFFSDtJQUNJLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0dBQ3BCOztBQUVIO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFO0lBQ0UsMkJBQTJCO0dBQzVCOztFQUVEO0lBQ0UsNEJBQTRCO0dBQzdCO0NBQ0Y7O0FBRUQ7RUFDRSxvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLG9EQUFvRDtVQUM1Qyw0Q0FBNEM7Q0FDckQ7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsY0FBYztJQUNkLHFCQUFxQjtPQUNsQixrQkFBa0I7SUFDckIsMkJBQTJCO09BQ3hCLHdCQUF3QjtHQUM1Qjs7QUFFSDtJQUNJLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsMEJBQTBCO0dBQzNCOztBQUVIO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0dBQ2hCOztBQUVIO0lBQ0ksZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGlCQUFpQjtHQUNsQjs7QUFFSDtJQUNJO01BQ0UsOEJBQThCO0tBQy9COztFQUVIO0lBQ0Usa0JBQWtCO0dBQ25COztJQUVDO01BQ0UsYUFBYTtNQUNiLGNBQWM7TUFDZCxxQkFBcUI7U0FDbEIsa0JBQWtCO01BQ3JCLDJCQUEyQjtTQUN4Qix3QkFBd0I7S0FDNUI7O0lBRUQ7TUFDRSxxQkFBcUI7TUFDckIsMEJBQTBCO0tBQzNCO0NBQ0o7O0FBRUQ7RUFDRSxvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsbUJBQW1CO0NBQ3BCOztBQUVEO0lBQ0ksY0FBYztJQUNkLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIscUJBQXFCO09BQ2xCLGtCQUFrQjtJQUNyQiwyQkFBMkI7T0FDeEIsd0JBQXdCO0dBQzVCOztBQUVIO0lBQ0ksbUJBQW1CO0lBQ25CLHFCQUFxQjtHQUN0Qjs7QUFFSDtJQUNJLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZUFBZTtHQUNoQjs7QUFFSDtJQUNJLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGVBQWU7R0FDaEJcIixcImZpbGVcIjpcIlRlYW0uc2Nzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCIuaGVhZGluZyB7XFxuICBmb250LXNpemU6IDMwcHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDEuM3B4O1xcbiAgY29sb3I6ICMzZTNlNWY7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMi41cmVtO1xcbn1cXG5cXG4uYmFubmVyU2VjdGlvbiB7XFxuICBiYWNrZ3JvdW5kOiAjM2UzZTVmO1xcbiAgbWluLWhlaWdodDogMzY2cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgxOTJkZWcsICNmZmFlNzEsICNmZjMxNWUpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KDE5MmRlZywgI2ZmYWU3MSwgI2ZmMzE1ZSk7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjU4ZGVnLCAjZmZhZTcxLCAjZmYzMTVlKTtcXG4gIHBhZGRpbmctdG9wOiA2NHB4O1xcbiAgcGFkZGluZy10b3A6IDRyZW07XFxufVxcblxcbi5iYW5uZXJTZWN0aW9uIC5oZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiA0OHB4O1xcbiAgICBmb250LXNpemU6IDNyZW07XFxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XFxuICAgIGNvbG9yOiAjZmZmO1xcbiAgfVxcblxcbi5iYW5uZXJTZWN0aW9uIC5kZXNjcmlwdGlvbiB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xcbiAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcXG4gICAgY29sb3I6ICNmZmY7XFxuICAgIG1hcmdpbi10b3A6IDQwcHg7XFxuICAgIG1hcmdpbi10b3A6IDIuNXJlbTtcXG4gIH1cXG5cXG4udGVhbVNlY3Rpb24ge1xcbiAgcGFkZGluZy10b3A6IDQ4cHg7XFxuICBwYWRkaW5nLXRvcDogM3JlbTtcXG4gIHBhZGRpbmctYm90dG9tOiA0OHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDNyZW07XFxuICBiYWNrZ3JvdW5kOiAjZmZmO1xcbn1cXG5cXG4udGFnTGluZSB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBsaW5lLWhlaWdodDogMjFweDtcXG4gIGNvbG9yOiAjNWY2MzY4O1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDk2MHB4KSB7XFxuICAuYWR2aWNlTWVtYmVyV3JhcHBlcjpudGgtY2hpbGQoMSkge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcXG4gIH1cXG5cXG4gIC5hZHZpY2VNZW1iZXJXcmFwcGVyOm50aC1jaGlsZCgyKSB7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDAgIWltcG9ydGFudDtcXG4gIH1cXG59XFxuXFxuLmFkdmlzb3J5TWVtYmVyQ2FyZCB7XFxuICBtYXJnaW4tYm90dG9tOiAzNnB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMi4yNXJlbTtcXG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAycHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAycHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KTtcXG59XFxuXFxuLmFkdmlzb3J5TWVtYmVyQ2FyZCBpbWcge1xcbiAgICB3aWR0aDogMjAwcHg7XFxuICAgIGhlaWdodDogMjAwcHg7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgLW8tb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgICAgIG9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgfVxcblxcbi5hZHZpc29yeU1lbWJlckNhcmQgLmluZm8ge1xcbiAgICBwYWRkaW5nOiAyNHB4IDMycHg7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAycmVtO1xcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMjAwcHgpO1xcbiAgfVxcblxcbi5hZHZpc29yeU1lbWJlckNhcmQgLm5hbWUge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgIGNvbG9yOiAjM2UzZTVmO1xcbiAgfVxcblxcbi5hZHZpc29yeU1lbWJlckNhcmQgLnRhZ0xpbmUge1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICAgIGhlaWdodDogMjQ7XFxuICAgIGNvbG9yOiAjNWY2MzY4O1xcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xcbiAgfVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTU5cHgpIHtcXG4gICAgLnRlYW1TZWN0aW9uIC5oZWFkaW5nIHtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcXG4gICAgfVxcblxcbiAgLmFkdmlzb3J5TWVtYmVyQ2FyZCB7XFxuICAgIG1pbi1oZWlnaHQ6IDE1MHB4O1xcbiAgfVxcblxcbiAgICAuYWR2aXNvcnlNZW1iZXJDYXJkIGltZyB7XFxuICAgICAgd2lkdGg6IDE1MHB4O1xcbiAgICAgIGhlaWdodDogMTUwcHg7XFxuICAgICAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICAgLW8tb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgICAgICAgb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgIH1cXG5cXG4gICAgLmFkdmlzb3J5TWVtYmVyQ2FyZCAuaW5mbyB7XFxuICAgICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE1MHB4KTtcXG4gICAgfVxcbn1cXG5cXG4udGVhbU1lbWJlckNhcmQge1xcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gIG1hcmdpbi1ib3R0b206IDJyZW07XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4udGVhbU1lbWJlckNhcmQgaW1nIHtcXG4gICAgaGVpZ2h0OiAyMDBweDtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xcbiAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gICAgLW8tb2JqZWN0LXBvc2l0aW9uOiAxMDAlIDA7XFxuICAgICAgIG9iamVjdC1wb3NpdGlvbjogMTAwJSAwO1xcbiAgfVxcblxcbi50ZWFtTWVtYmVyQ2FyZCAuaW5mbyB7XFxuICAgIHBhZGRpbmc6IDI0cHggMzJweDtcXG4gICAgcGFkZGluZzogMS41cmVtIDJyZW07XFxuICB9XFxuXFxuLnRlYW1NZW1iZXJDYXJkIC5uYW1lIHtcXG4gICAgZm9udC1zaXplOiAyNHB4O1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICBjb2xvcjogIzNlM2U1ZjtcXG4gIH1cXG5cXG4udGVhbU1lbWJlckNhcmQgLnRpdGxlIHtcXG4gICAgbWFyZ2luLXRvcDogOHB4O1xcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgY29sb3I6ICM1ZjYzNjg7XFxuICB9XFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImhlYWRpbmdcIjogXCJUZWFtLWhlYWRpbmctM0NCU1ZcIixcblx0XCJiYW5uZXJTZWN0aW9uXCI6IFwiVGVhbS1iYW5uZXJTZWN0aW9uLWE5aTRsXCIsXG5cdFwiZGVzY3JpcHRpb25cIjogXCJUZWFtLWRlc2NyaXB0aW9uLUlfdWcwXCIsXG5cdFwidGVhbVNlY3Rpb25cIjogXCJUZWFtLXRlYW1TZWN0aW9uLTFwQ3I3XCIsXG5cdFwidGFnTGluZVwiOiBcIlRlYW0tdGFnTGluZS0yV3puelwiLFxuXHRcImFkdmljZU1lbWJlcldyYXBwZXJcIjogXCJUZWFtLWFkdmljZU1lbWJlcldyYXBwZXItMm1xUGJcIixcblx0XCJhZHZpc29yeU1lbWJlckNhcmRcIjogXCJUZWFtLWFkdmlzb3J5TWVtYmVyQ2FyZC0zc1lGb1wiLFxuXHRcImluZm9cIjogXCJUZWFtLWluZm8tM1kzNUZcIixcblx0XCJuYW1lXCI6IFwiVGVhbS1uYW1lLTNTVHRYXCIsXG5cdFwidGVhbU1lbWJlckNhcmRcIjogXCJUZWFtLXRlYW1NZW1iZXJDYXJkLTMyWWNIXCIsXG5cdFwidGl0bGVcIjogXCJUZWFtLXRpdGxlLTFmaEZrXCJcbn07IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0Jztcbi8vIGltcG9ydCBjeCBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBSZXF1ZXN0RGVtbyBmcm9tICdjb21wb25lbnRzL1JlcXVlc3REZW1vJztcbi8vIGltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluayc7XG5pbXBvcnQgcyBmcm9tICcuL1RlYW0uc2Nzcyc7XG5cbmNvbnN0IEFEVklTT1JZX0JPQVJEID0gW1xuICB7XG4gICAgbmFtZTogJ00uWS5TLiBQcmFzYWQnLFxuICAgIHRhZ0xpbmU6ICdQYWRtYXNocmkgQXdhcmRlZScsXG4gICAgZGV0YWlsMTogJ1ZpY2UgQ2hhbmNlbGxvciwgVmlnbmFuIFVuaXZlcnNpdHknLFxuICAgIGRldGFpbDI6ICdGb3JtZXIgRGlyZWN0b3ItIFNEU0MsIElTUk8nLFxuICAgIGltYWdlOiAnL3RlYW0vUGFkbWEgU2hyaSBNLlkuUy5QcmFzYWQuanBnJyxcbiAgICB1cmw6ICdodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9NLl9ZLl9TLl9QcmFzYWQnLFxuICB9LFxuICB7XG4gICAgbmFtZTogJ0RyLiBULiBIYW51bWFuIENob3dkYXJ5JyxcbiAgICB0YWdMaW5lOiAnUGFkbWFzaHJpIEF3YXJkZWUnLFxuICAgIGRldGFpbDE6ICdGb3JtZXIgSVQgQWR2aXNvciwgQVAgR292dC4nLFxuICAgIGRldGFpbDI6ICdGZWxsb3csIFRhdGEgQ29uc3VsdGluZyBTZXJ2aWNlcyAoVENTKScsXG4gICAgaW1hZ2U6ICcvdGVhbS9QYWRtYSBTaHJpIERyLlQuSGFudW1hbiBDaG93ZGFyeS5KUEcnLFxuICAgIHVybDpcbiAgICAgICdodHRwczovL3d3dy5ibG9vbWJlcmcuY29tL3Jlc2VhcmNoL3N0b2Nrcy9wZW9wbGUvcGVyc29uLmFzcD9wZXJzb25JZD03NjU1NjQmcHJpdmNhcElkPTEzMzEwMycsXG4gIH0sXG5dO1xuXG5jb25zdCBURUFNUyA9IFtcbiAge1xuICAgIG5hbWU6ICdBZHZpc29yeSBCb2FyZCcsXG4gICAgbWVtYmVyczogW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnTS5ZLlMuIFByYXNhZCcsXG4gICAgICAgIHRpdGxlOiAnUGFkbWFzaHJpIEF3YXJkZWUnLFxuICAgICAgICBkZXRhaWwxOiAnVmljZSBDaGFuY2VsbG9yLCBWaWduYW4gVW5pdmVyc2l0eScsXG4gICAgICAgIGRldGFpbDI6ICdGb3JtZXIgRGlyZWN0b3ItIFNEU0MsIElTUk8nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL00uWS5TLiBQcmFzYWQucG5nJyxcbiAgICAgICAgLyogbGlua2VkSW46ICdodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9NLl9ZLl9TLl9QcmFzYWQnLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0RyLiBULiBIYW51bWFuIENob3dkYXJ5JyxcbiAgICAgICAgdGl0bGU6ICdQYWRtYXNocmkgQXdhcmRlZScsXG4gICAgICAgIGRldGFpbDE6ICdGb3JtZXIgSVQgQWR2aXNvciwgQVAgR292dC4nLFxuICAgICAgICBkZXRhaWwyOiAnRmVsbG93LCBUYXRhIENvbnN1bHRpbmcgU2VydmljZXMgKFRDUyknLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1BhZG1hIFNocmkgRHIuVC5IYW51bWFuIENob3dkYXJ5LnBuZycsXG4gICAgICAgIC8qIGxpbmtlZEluOlxuICAgICAgICAgICdodHRwczovL3d3dy5ibG9vbWJlcmcuY29tL3Jlc2VhcmNoL3N0b2Nrcy9wZW9wbGUvcGVyc29uLmFzcD9wZXJzb25JZD03NjU1NjQmcHJpdmNhcElkPTEzMzEwMycsICovXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnTGVhZGVyc2hpcCcsXG4gICAgbWVtYmVyczogW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnS2lyYW4gQmFidScsXG4gICAgICAgIHRpdGxlOiAnRm91bmRlciAmIENFTycsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vS2lyYW4tQmFidS5wbmcnLFxuICAgICAgICBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi95ZXJyYW5hZ3UvJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdBbmphbiBHb3N3YW1pJyxcbiAgICAgICAgdGl0bGU6ICdQcm9kdWN0JyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9Bbmphbi1Hb3N3YW1pLnBuZycsXG4gICAgICAgIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL2FuamFuZ29zd2FtaS8nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ05pa2hpbCBTaGFybWEnLFxuICAgICAgICB0aXRsZTogJ0VuZ2luZWVyaW5nJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9OaWtoaWwtU2hhcm1hLnBuZycsXG4gICAgICAgIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL255a2h5bHNoYXJtYS8nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1NoaXZhIEtyaXNobmEnLFxuICAgICAgICB0aXRsZTogJ0VuZ2luZWVyaW5nJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9TaGl2YS5qcGcnLFxuICAgICAgICBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9zaGl2YWtyaXNobmFhaC8nLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0NoYW5kcmFTZWtoYXInLFxuICAgICAgICB0aXRsZTogJ09wZXJhdGlvbnMnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL0NIQU5EUkEuanBnJyxcbiAgICAgICAgbGlua2VkSW46ICdodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vY2hhbmRyYXNla2toYXItbS12LWIxNzYyYjEzLycsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICBuYW1lOiAnVGVhbScsXG4gICAgbWVtYmVyczogW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnTGF4bWFuUmFvJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9MQVhNQU4uanBnJyxcbiAgICAgICAgdGl0bGU6ICdTYWxlcycsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQXJjaGFuYScsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vQXJjaGFuYS5qcGcnLFxuICAgICAgICB0aXRsZTogJ1NhbGVzJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdWYW5kYW5hJyxcbiAgICAgICAgaW1hZ2U6ICcvdGVhbS9WYW5kYW5hLmpwZycsXG4gICAgICAgIHRpdGxlOiAnU2FsZXMnLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1ByYW5hYiBTYXJrYXInLFxuICAgICAgICB0aXRsZTogJ0Rlc2lnbicsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vcHJhbmFiLmpwZycsXG4gICAgICAgIC8qIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL3ByYW5hYi1zYXJrYXItYjU0OTY2OTkvJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdTcnVqYW4gU2FnYXInLFxuICAgICAgICB0aXRsZTogJ0Rlc2lnbicsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vU3J1amFuLVNhZ2FyLnBuZycsXG4gICAgICAgIC8qIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL3NydWphbnNhZ2FyLycsICovXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU3Jpa2FudCcsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vU3Jpa2FudCAyLmpwZycsXG4gICAgICAgIHRpdGxlOiAnRGVzaWduJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdTdWRoaXIgU3VndXJ1JyxcbiAgICAgICAgdGl0bGU6ICdUYWxlbnQgQWNxdWlzaXRpb24nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1NVREhJUi5qcGcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9zdWRoaXJzdWd1cnUvJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdIYXJpIEtyaXNobmEgU2FsdmVyJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vSGFyaS1LcmlzaG5hLVNhbHZlci5wbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9oYXJpa3Jpc2huYXNhbHZlci8nLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0Jpc3dhIEJodXNhbicsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vYmlzd2EuanBnJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIC8qIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL2Jpc3dhLWJodXNhbi1zb3Jlbi1hNjY1ODMxMzcvJywgKi9cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdBc2xhbSBTaGFpaycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL0FzbGFtLVNoYWlrLnBuZycsXG4gICAgICAgIC8qIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL2FzbGFtLXNoYWlrLTM5YTExNDEzNS8nLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1Zpa3JhbSBOZXRoaScsXG4gICAgICAgIGltYWdlOiAnL3RlYW0vdmlrcmFtLnBuZycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi92aWtyYW0tbmV0aGktYmI1YTg4YjUnLCAqL1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ01vaGl0IEFnYXJ3YWwnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL01vaGl0LmpwZycsXG4gICAgICAgIHRpdGxlOiAnRW5naW5lZXJpbmcnLFxuICAgICAgICAvKiBsaW5rZWRJbjogJ2h0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9tb2hpdC1hZ2Fyd2FsLTAzLycsICovXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU3Jpa2FudGgnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1NyaWthbnQuanBnJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIC8vIGxpbmtlZEluOiAnaHR0cHM6Ly9hbmdlbC5jby9sb25ld29sZjM3MzknLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1Zpa3JhbSBTb21hdmFyYW0nLFxuICAgICAgICBpbWFnZTogJy90ZWFtL1Zpa3JhbVMuanBnJyxcbiAgICAgICAgdGl0bGU6ICdFbmdpbmVlcmluZycsXG4gICAgICAgIC8vIGxpbmtlZEluOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL2luL3Zpa3JhbS1zb21hdmFyYW0tNDhhMTUxNjUvJyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdOYXJhc2ltaGEgTWFkZGknLFxuICAgICAgICB0aXRsZTogJ09wZXJhdGlvbnMnLFxuICAgICAgICBpbWFnZTogJy90ZWFtL05hcmFzaW1oYS1NYWRkaS5wbmcnLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuXTtcblxuY2xhc3MgVGVhbSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIFJlYWN0R0EuaW5pdGlhbGl6ZSh3aW5kb3cuQXBwLmdvb2dsZVRyYWNraW5nSWQsIHtcbiAgICAgIGRlYnVnOiBmYWxzZSxcbiAgICB9KTtcbiAgICBSZWFjdEdBLnBhZ2V2aWV3KHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcbiAgfVxuXG4gIGRpc3BsYXlBZHZpc29yeU1lbWJlcnNTZWN0aW9uID0gKCkgPT4ge1xuICAgIGNvbnN0IHZpZXcgPSAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBoaWRlXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRpbmd9PkFkdmlzb3J5IEJvYXJkPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAge0FEVklTT1JZX0JPQVJELm1hcChtZW1iZXIgPT4gKFxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Bjb2wgeHMxMiBzNiAke3MuYWR2aWNlTWVtYmVyV3JhcHBlcn1gfT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuYWR2aXNvcnlNZW1iZXJDYXJkfWB9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sIG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9e21lbWJlci51cmx9XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbWJlci5pbWFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgICA/IGAvaW1hZ2VzLyR7bWVtYmVyLmltYWdlfWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiAnaHR0cHM6Ly94MS54aW5nYXNzZXRzLmNvbS9hc3NldHMvZnJvbnRlbmRfbWluaWZpZWQvaW1nL3VzZXJzL25vYm9keV9tLm9yaWdpbmFsLmpwZydcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YGNvbCAke3MuaW5mb31gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm5hbWV9PnttZW1iZXIubmFtZX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhZ0xpbmV9PnttZW1iZXIudGFnTGluZX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRhZ0xpbmV9PiB7bWVtYmVyLmRldGFpbDF9PC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50YWdMaW5lfT4ge21lbWJlci5kZXRhaWwyfTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gICAgcmV0dXJuIHZpZXc7XG4gIH07XG5cbiAgZGlzcGxheVRlYW1zU2VjdGlvbiA9ICgpID0+IHtcbiAgICBjb25zdCB2aWV3ID0gVEVBTVMubWFwKHRlYW0gPT4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIiBzdHlsZT17eyBtYXJnaW5Ub3A6ICcycmVtJyB9fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuaGVhZGluZ30+e3RlYW0ubmFtZX08L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICB7dGVhbS5tZW1iZXJzLm1hcChtZW1iZXIgPT4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgIG1lbWJlci5uYW1lID09PSAnTS5ZLlMuIFByYXNhZCcgfHxcbiAgICAgICAgICAgICAgICBtZW1iZXIubmFtZSA9PT0gJ0RyLiBULiBIYW51bWFuIENob3dkYXJ5J1xuICAgICAgICAgICAgICAgICAgPyBgY29sIHhzMTIgczYgJHtzLnRlYW1NZW1iZXJDYXJkfWBcbiAgICAgICAgICAgICAgICAgIDogYGNvbCB4czEyIHM0ICR7cy50ZWFtTWVtYmVyQ2FyZH1gXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wgeHMxMiBsMTIgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgIHttZW1iZXIubGlua2VkSW4gPyAoXG4gICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICBocmVmPXttZW1iZXIubGlua2VkSW59XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbWJlci5pbWFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgICA/IGAvaW1hZ2VzLyR7bWVtYmVyLmltYWdlfWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiAnaHR0cHM6Ly94MS54aW5nYXNzZXRzLmNvbS9hc3NldHMvZnJvbnRlbmRfbWluaWZpZWQvaW1nL3VzZXJzL25vYm9keV9tLm9yaWdpbmFsLmpwZydcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICAgIG1lbWJlci5pbWFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgPyBgL2ltYWdlcy8ke21lbWJlci5pbWFnZX1gXG4gICAgICAgICAgICAgICAgICAgICAgICA6ICdodHRwczovL3gxLnhpbmdhc3NldHMuY29tL2Fzc2V0cy9mcm9udGVuZF9taW5pZmllZC9pbWcvdXNlcnMvbm9ib2R5X20ub3JpZ2luYWwuanBnJ1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YGNvbCB4czEyIHMxMiAke3MuaW5mb31gfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5uYW1lfSBoaWRlLW92ZXJmbG93YH0+e21lbWJlci5uYW1lfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRpdGxlfT57bWVtYmVyLnRpdGxlfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRpdGxlfT4ge21lbWJlci5kZXRhaWwxfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRpdGxlfT4ge21lbWJlci5kZXRhaWwyfTwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICkpO1xuICAgIHJldHVybiB2aWV3O1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPXtzLmJhbm5lclNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbCB4czEyIGw2IG5vLXBhZGRpbmdcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5oZWFkaW5nfT5NZWV0IHRoZSB0ZWFtIGJlaGluZCB0aGlzPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZGVzY3JpcHRpb259PlxuICAgICAgICAgICAgICAgICAge2BXZSBiZWxpZXZlIHdvcmsgaXMgZXhoaWxhcmF0aW5nIHdoZW4geW91J3JlIHN1cnJvdW5kZWQgYnlcbiAgICAgICAgICAgICAgICAgICBleHRyYW9yZGluYXJ5IHBlb3BsZSB3aG8gZW1icmFjZSBiZWluZyBwYXJ0IG9mIGEgdGVhbS5gfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT17YCR7cy50ZWFtU2VjdGlvbn0gY29udGFpbmVyYH0+XG4gICAgICAgICAge3RoaXMuZGlzcGxheUFkdmlzb3J5TWVtYmVyc1NlY3Rpb24oKX1cbiAgICAgICAgICB7dGhpcy5kaXNwbGF5VGVhbXNTZWN0aW9uKCl9XG4gICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgPFJlcXVlc3REZW1vIC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMocykoVGVhbSk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1RlYW0uc2Nzc1wiKTtcbiAgICB2YXIgaW5zZXJ0Q3NzID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL2luc2VydENzcy5qc1wiKTtcblxuICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50OyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9nZXRDc3MgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQudG9TdHJpbmcoKTsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5faW5zZXJ0Q3NzID0gZnVuY3Rpb24ob3B0aW9ucykgeyByZXR1cm4gaW5zZXJ0Q3NzKGNvbnRlbnQsIG9wdGlvbnMpIH07XG4gICAgXG4gICAgLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuICAgIC8vIGh0dHBzOi8vd2VicGFjay5naXRodWIuaW8vZG9jcy9ob3QtbW9kdWxlLXJlcGxhY2VtZW50XG4gICAgLy8gT25seSBhY3RpdmF0ZWQgaW4gYnJvd3NlciBjb250ZXh0XG4gICAgaWYgKG1vZHVsZS5ob3QgJiYgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICB2YXIgcmVtb3ZlQ3NzID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZWFtLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS01LXJ1bGVzLTMhLi9UZWFtLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBUZWFtIGZyb20gJy4vVGVhbSc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTGF5b3V0JztcblxuYXN5bmMgZnVuY3Rpb24gYWN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnRWduaWZ5JyxcbiAgICBjaHVua3M6IFsnVGVhbSddLFxuICAgIGNvbXBvbmVudDogKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPFRlYW0gLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVdBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQU5BO0FBWEE7QUF1QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBM0JBO0FBb0NBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFuRkE7QUFDQTtBQTJGQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF2QkE7QUErQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFLQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBS0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBeENBO0FBSkE7QUFtREE7QUFDQTtBQUNBOzs7Ozs7QUFwR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFnR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7Ozs7QUE3SEE7QUFDQTtBQStIQTs7Ozs7OztBQ2xUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7OztBQVlBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=