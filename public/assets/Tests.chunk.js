(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Tests"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slider-Player/SliderPlayer.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".SliderPlayer-root-1PJ5x {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.SliderPlayer-video-39D4o {\n  width: 400px;\n  height: 400px;\n}\n\n.SliderPlayer-desktopControls-36NJ2 {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.SliderPlayer-controlsWrapper-lXOHa {\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 96px;\n}\n\n.SliderPlayer-controls-1_uIj {\n  width: 240px;\n  margin: 0 16px;\n  margin: 0 1rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.SliderPlayer-outer-U9Tud {\n  width: 240px;\n  background-color: #d8d8d8;\n  height: 4px;\n  position: relative;\n  border-radius: 4px;\n  overflow-x: hidden;\n}\n\n.SliderPlayer-inner-3AXse {\n  position: absolute;\n  height: 4px;\n  top: 0;\n  left: 0;\n  z-index: 1;\n  background-color: #f36;\n  -webkit-transition: all 0.1s ease;\n  -o-transition: all 0.1s ease;\n  transition: all 0.1s ease;\n}\n\n.SliderPlayer-videoTitle-22XxD {\n  font-size: 24px;\n  line-height: 32px;\n  margin-top: 12px;\n  font-weight: 600;\n}\n\n@media only screen and (max-width: 990px) {\n  .SliderPlayer-video-39D4o {\n    width: 100%;\n    height: 100%;\n    max-width: 400px;\n    max-height: 300px;\n  }\n\n  .SliderPlayer-mobileTitlesWrapper-1yran {\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    max-width: 330px;\n    overflow-x: hidden;\n  }\n\n  .SliderPlayer-mobileControls-1h6o7 {\n    width: 100%;\n  }\n\n  .SliderPlayer-videoTitle-22XxD {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 20px;\n    margin-right: 12px;\n    min-width: 100px;\n    -webkit-transition: -webkit-transform 1s ease;\n    transition: -webkit-transform 1s ease;\n    -o-transition: transform 1s ease;\n    transition: transform 1s ease;\n    transition: transform 1s ease, -webkit-transform 1s ease;\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-actTitle-3350y {\n    font-weight: bold;\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut-zfY0h {\n    -webkit-transform: translateX(0);\n        -ms-transform: translateX(0);\n            transform: translateX(0);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut1-2iCv0 {\n    -webkit-transform: translateX(-100%);\n        -ms-transform: translateX(-100%);\n            transform: translateX(-100%);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut2-4nz6i {\n    -webkit-transform: translateX(-210%);\n        -ms-transform: translateX(-210%);\n            transform: translateX(-210%);\n  }\n\n  .SliderPlayer-videoTitle-22XxD.SliderPlayer-slideOut3-1C_fo {\n    -webkit-transform: translateX(-320%);\n        -ms-transform: translateX(-320%);\n            transform: translateX(-320%);\n  }\n\n  .SliderPlayer-controlsWrapper-lXOHa {\n    width: 100%;\n    max-width: 330px;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 0;\n    overflow-x: hidden;\n    margin-top: 32px;\n  }\n\n  .SliderPlayer-controls-1_uIj {\n    width: 200px;\n    margin: 0 12px 0 0;\n  }\n\n  .SliderPlayer-desktopControls-36NJ2 {\n    overflow-x: scroll;\n  }\n\n  .SliderPlayer-outer-U9Tud {\n    width: 100%;\n  }\n\n  .SliderPlayer-outer-U9Tud.SliderPlayer-mobileTracker-mILab {\n    width: 100%;\n    margin: auto;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slider-Player/SliderPlayer.scss"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,iBAAiB;CAClB;;AAED;EACE,aAAa;EACb,eAAe;EACf,eAAe;EACf,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;CACjC;;AAED;EACE,aAAa;EACb,0BAA0B;EAC1B,YAAY;EACZ,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,OAAO;EACP,QAAQ;EACR,WAAW;EACX,uBAAuB;EACvB,kCAAkC;EAClC,6BAA6B;EAC7B,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,iBAAiB;CAClB;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,YAAY;IACZ,iBAAiB;IACjB,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;EAED;IACE,oBAAoB;IACpB,gBAAgB;IAChB,kBAAkB;IAClB,mBAAmB;IACnB,iBAAiB;IACjB,8CAA8C;IAC9C,sCAAsC;IACtC,iCAAiC;IACjC,8BAA8B;IAC9B,yDAAyD;GAC1D;;EAED;IACE,kBAAkB;GACnB;;EAED;IACE,iCAAiC;QAC7B,6BAA6B;YACzB,yBAAyB;GAClC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,qCAAqC;QACjC,iCAAiC;YAC7B,6BAA6B;GACtC;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,2BAA2B;QACvB,uBAAuB;IAC3B,WAAW;IACX,mBAAmB;IACnB,iBAAiB;GAClB;;EAED;IACE,aAAa;IACb,mBAAmB;GACpB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,YAAY;GACb;;EAED;IACE,YAAY;IACZ,aAAa;GACd;CACF","file":"SliderPlayer.scss","sourcesContent":[".root {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.video {\n  width: 400px;\n  height: 400px;\n}\n\n.desktopControls {\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.controlsWrapper {\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 96px;\n}\n\n.controls {\n  width: 240px;\n  margin: 0 16px;\n  margin: 0 1rem;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n}\n\n.outer {\n  width: 240px;\n  background-color: #d8d8d8;\n  height: 4px;\n  position: relative;\n  border-radius: 4px;\n  overflow-x: hidden;\n}\n\n.inner {\n  position: absolute;\n  height: 4px;\n  top: 0;\n  left: 0;\n  z-index: 1;\n  background-color: #f36;\n  -webkit-transition: all 0.1s ease;\n  -o-transition: all 0.1s ease;\n  transition: all 0.1s ease;\n}\n\n.videoTitle {\n  font-size: 24px;\n  line-height: 32px;\n  margin-top: 12px;\n  font-weight: 600;\n}\n\n@media only screen and (max-width: 990px) {\n  .video {\n    width: 100%;\n    height: 100%;\n    max-width: 400px;\n    max-height: 300px;\n  }\n\n  .mobileTitlesWrapper {\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n    max-width: 330px;\n    overflow-x: hidden;\n  }\n\n  .mobileControls {\n    width: 100%;\n  }\n\n  .videoTitle {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 20px;\n    margin-right: 12px;\n    min-width: 100px;\n    -webkit-transition: -webkit-transform 1s ease;\n    transition: -webkit-transform 1s ease;\n    -o-transition: transform 1s ease;\n    transition: transform 1s ease;\n    transition: transform 1s ease, -webkit-transform 1s ease;\n  }\n\n  .videoTitle.actTitle {\n    font-weight: bold;\n  }\n\n  .videoTitle.slideOut {\n    -webkit-transform: translateX(0);\n        -ms-transform: translateX(0);\n            transform: translateX(0);\n  }\n\n  .videoTitle.slideOut1 {\n    -webkit-transform: translateX(-100%);\n        -ms-transform: translateX(-100%);\n            transform: translateX(-100%);\n  }\n\n  .videoTitle.slideOut2 {\n    -webkit-transform: translateX(-210%);\n        -ms-transform: translateX(-210%);\n            transform: translateX(-210%);\n  }\n\n  .videoTitle.slideOut3 {\n    -webkit-transform: translateX(-320%);\n        -ms-transform: translateX(-320%);\n            transform: translateX(-320%);\n  }\n\n  .controlsWrapper {\n    width: 100%;\n    max-width: 330px;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    padding: 0;\n    overflow-x: hidden;\n    margin-top: 32px;\n  }\n\n  .controls {\n    width: 200px;\n    margin: 0 12px 0 0;\n  }\n\n  .desktopControls {\n    overflow-x: scroll;\n  }\n\n  .outer {\n    width: 100%;\n  }\n\n  .outer.mobileTracker {\n    width: 100%;\n    margin: auto;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"root": "SliderPlayer-root-1PJ5x",
	"video": "SliderPlayer-video-39D4o",
	"desktopControls": "SliderPlayer-desktopControls-36NJ2",
	"controlsWrapper": "SliderPlayer-controlsWrapper-lXOHa",
	"controls": "SliderPlayer-controls-1_uIj",
	"outer": "SliderPlayer-outer-U9Tud",
	"inner": "SliderPlayer-inner-3AXse",
	"videoTitle": "SliderPlayer-videoTitle-22XxD",
	"mobileTitlesWrapper": "SliderPlayer-mobileTitlesWrapper-1yran",
	"mobileControls": "SliderPlayer-mobileControls-1h6o7",
	"actTitle": "SliderPlayer-actTitle-3350y",
	"slideOut": "SliderPlayer-slideOut-zfY0h",
	"slideOut1": "SliderPlayer-slideOut1-2iCv0",
	"slideOut2": "SliderPlayer-slideOut2-4nz6i",
	"slideOut3": "SliderPlayer-slideOut3-1C_fo",
	"mobileTracker": "SliderPlayer-mobileTracker-mILab"
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Test/Test.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, ".Test-maxContainer-1_QCO {\n  max-width: 1700px;\n  width: 100%;\n  position: relative;\n  margin: auto;\n}\n\n.Test-marginBottom126-2y-TJ {\n  margin-bottom: 126px;\n}\n\n.Test-reports_sub_title-1zU4V {\n  font-size: 28px;\n  line-height: 32px;\n  font-weight: 600;\n  margin-top: 12px;\n  color: #25282b;\n}\n\n.Test-reports_notice-1k7xY {\n  font-size: 20px;\n  line-height: 32px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-top: 4px;\n  margin-bottom: 12px;\n}\n\n.Test-row1-1pBIZ {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n}\n\n.Test-pointContainer-yl6rU {\n  width: 5%;\n}\n\n.Test-point-2yHMB {\n  margin-bottom: 12px;\n}\n\n.Test-pointText-3omy9 {\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.Test-redDot-3mKH- {\n  width: 8px;\n  height: 8px;\n  border-radius: 4px;\n  background-color: #f36;\n  margin-right: 20px;\n  margin-top: 12px;\n}\n\n.Test-testsImage-3Z2de {\n  width: 72px;\n  height: 72px;\n  margin-top: -8px;\n  margin-right: 24px;\n}\n\n.Test-testsImage-3Z2de img {\n  width: 100%;\n  height: 100%;\n}\n\n.Test-section_container-2wA5m.Test-ashBackground-q1E-3 {\n  background-color: #f7f7f7;\n}\n\n.Test-videoSlider-28_ug {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 80px 64px;\n  padding: 5rem 4rem;\n  background-color: #f7f7f7;\n  position: relative;\n}\n\n.Test-maths-TucEU {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.Test-microscope-HsatG {\n  position: absolute;\n  top: 30%;\n  left: 3%;\n}\n\n.Test-triangle-2BUKY {\n  position: absolute;\n  top: 60%;\n  left: 10%;\n}\n\n.Test-scale-1c9I4 {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n}\n\n.Test-circle-2jW1w {\n  position: absolute;\n  top: 30%;\n  right: 3%;\n}\n\n.Test-chemistry-2eYOW {\n  position: absolute;\n  top: 60%;\n  right: 10%;\n}\n\n.Test-headerbackgroundmobile-3FN-6 {\n  position: absolute;\n  bottom: -56px;\n  right: 0;\n  width: 513px;\n  height: 560px;\n}\n\n.Test-headerbackgroundmobile-3FN-6 img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.Test-section_title-1u6t0 {\n  display: block;\n  margin: 0 0 16px;\n  font-size: 40px;\n  line-height: 48px;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Test-sub_section_title-3VRu3 {\n  font-size: 28px;\n  line-height: 32px;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 12px;\n  margin-top: 56px;\n}\n\n.Test-contentPart-vOxYm {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 45%;\n}\n\n.Test-contentPart-vOxYm .Test-content-2hs9s {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n  }\n\n.Test-contentPart-vOxYm .Test-content-2hs9s .Test-textcontent-3rWf1 {\n      font-size: 16px;\n      line-height: 24px;\n      color: #25282b;\n\n      /* .point {\n        width: 100%;\n        display: flex;\n        align-items: flex-start;\n        height: max-content;\n        margin: 6px 0;\n\n        .reddotwrapper {\n          width: 5%;\n          display: flex;\n          align-items: center;\n          justify-content: flex-start;\n          margin-top: 12px;\n\n          .reddot {\n            width: 8px;\n            height: 8px;\n            background-color: #f36;\n            border-radius: 50%;\n          }\n        }\n\n        .pointText {\n          width: 95%;\n        }\n      } */\n    }\n\n/* .customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 560px;\n\n  .customer_review {\n    order: 0;\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n\n    .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n\n      img {\n        width: 100%;\n        height: 100%;\n        object-fit: contain;\n      }\n    }\n\n    .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n    .authorWrapper {\n      display: flex;\n      flex-direction: column;\n\n      .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n      .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n    }\n\n    .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: flex;\n      flex-direction: row;\n      align-items: center;\n\n      img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        object-fit: contain;\n      }\n    }\n  }\n} */\n\n.Test-availableContainer-3xLT2 {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.Test-availableContainer-3xLT2 .Test-availableRow-2Qync {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-desktopImage-2kXNX {\n      width: 696px;\n      height: 371px;\n    }\n\n.Test-joinCommunity-3LBUs {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-joinFbText-1fdhM {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.Test-joinFbLink-Avcsf {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.Test-availableTitle-3NEfA {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.Test-available-3lWLX {\n  color: #f36;\n}\n\n.Test-availableContentSection-UP4ln {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.Test-platformContainer-2Ohnt {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.Test-platform-kOxAM {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.Test-platformOs-36PkG {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.Test-row-sE-Co {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.Test-store-3Cziz {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.Test-store-3Cziz .Test-playstore-3PKOC {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.Test-store-3Cziz .Test-appstore-1hO16 {\n    width: 136px;\n    height: 40px;\n  }\n\n.Test-scrollTop-14S8C {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.Test-scrollTop-14S8C img {\n    width: 100%;\n    height: 100%;\n  }\n\n.Test-teachContainer-4lLOo {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #f2e5fe;\n  position: relative;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 {\n  width: 316px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n}\n\n.Test-imagePart-FumwY .Test-emptyCard-2MxHA img {\n  width: 100%;\n  height: 100%;\n  // object-fit: contain;\n}\n\n.Test-imagePart-FumwY .Test-topCard-3A_No img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n}\n\n.Test-teachContainer-4lLOo .Test-contentText-2Hp6y {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-requestDemo-EyPHF {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ .Test-whatsapp-3-hIr {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ img {\n  width: 32px;\n  height: 32px;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI .Test-downloadText-2KLuA {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.Test-teachContainer-4lLOo .Test-downloadApp-h4dCI .Test-playStoreIcon-3vbFK {\n  width: 132px;\n  height: 40px;\n}\n\n/* .teachContainer .actionsWrapper {\n  position: absolute;\n  bottom: 26px;\n  width: 100%;\n  display: flex;\n}\n.teachContainer .actionsWrapper .action {\n  width: fit-content;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: 24px;\n}\n.teachContainer .actionsWrapper .action span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  opacity: 0.7;\n  text-align: left;\n  margin-left: 8px;\n}\n.teachContainer .actionsWrapper .action .actionimgbox {\n  width: 24px;\n  height: 24px;\n  background-color: #fff;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.teachContainer .actionsWrapper .action .actionimgbox img {\n  width: 10px;\n  height: 9px;\n} */\n\n.Test-teachContainer-4lLOo .Test-breadcrum-1j15E {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.Test-displayClients-18jr5 span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.Test-teachContainer-4lLOo .Test-breadcrum-1j15E span {\n  font-weight: 600;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP {\n  width: 40px;\n  height: 40px;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachSectionName-1fAAr {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #8800fe;\n  font-weight: bold;\n}\n\n/* .teachContainer .topSection .featuresSection {\n  display: flex;\n  align-items: center;\n  margin-left: 30%;\n}\n.teachContainer .topSection .featuresSection span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  margin-right: 32px;\n  font-weight: 500;\n} */\n\n.Test-displayClients-18jr5 {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.Test-displayClients-18jr5 h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP .Test-client-FzdBZ {\n  width: 172px;\n  height: 112px;\n}\n\n.Test-displayClients-18jr5 span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.Test-displayClients-18jr5 span a:hover {\n  text-decoration: underline;\n}\n\n.Test-achievedContainer-1FrOF {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-clients-2p-cX {\n  background-color: #fff3eb;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-students-3C7ru {\n  background-color: #f7effe;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-tests-2gs17 {\n  background-color: #ffebf0;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-achievedProfile-247hX.Test-questions-3vWWW {\n  background-color: #ebffef;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-questionsHighlight-2_VNQ {\n  color: #00ac26;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-testsHighlight-2bD1f {\n  color: #f36;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-studentsHighlight-3p4cJ {\n  color: #8c00fe;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-highlight-RuaYQ.Test-clientsHighlight-yClDo {\n  color: #f60;\n}\n\n.Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf .Test-card-TZZcR .Test-subText-2Q8Rs {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.Test-section_container-2wA5m {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.Test-section_container-2wA5m .Test-section_contents-2WASW {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.Test-section_container_reverse-1IKFo {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.Test-section_container_reverse-1IKFo .Test-section_contents-2WASW {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.Test-imagePart-FumwY {\n  width: 50%;\n}\n\n.Test-imagePart-FumwY .Test-emptyCard-2MxHA {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  position: relative;\n}\n\n.Test-imagePart-FumwY .Test-topCard-3A_No {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.Test-imagePart-FumwY .Test-bottomCircle-2SECO {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.Test-imagePart-FumwY .Test-topCircle-2B2HP {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-topCircle-2B2HP {\n  top: -44px;\n  right: 50px;\n  background-color: #f2e5fe;\n}\n\n.Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-bottomCircle-2SECO {\n  bottom: -44px;\n  left: 79px;\n  background-color: #ffe0e8;\n}\n\n.Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-topCircle-2B2HP {\n  background-color: #3fc;\n  opacity: 0.3;\n  top: -44px;\n  right: 70px;\n}\n\n.Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-bottomCircle-2SECO {\n  background-color: #ffe0e8;\n  bottom: -44px;\n  left: 40px;\n}\n\n.Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-topCircle-2B2HP {\n  background-color: #0076ff;\n  opacity: 0.2;\n  top: -44px;\n  right: 70px;\n}\n\n.Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-bottomCircle-2SECO {\n  background-color: #feb546;\n  bottom: -44px;\n  left: 40px;\n  opacity: 0.2;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD {\n  width: 45%;\n  margin-bottom: 56px;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_title-1DOvX {\n  font-size: 20px;\n  font-weight: bold;\n  line-height: 30px;\n  margin-bottom: 8px;\n  color: #25282b;\n}\n\n.Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_text-QKFHq {\n  font-size: 16px;\n  line-height: 24px;\n  color: #25282b;\n}\n\n/* .allcustomers p {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n} */\n\n.Test-content-2hs9s p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.Test-hide-3vPQU {\n  display: none;\n}\n\n.Test-show-LTOHo {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n@media only screen and (max-width: 1200px) {\n  .Test-availableContainer-3xLT2 {\n    padding: 5rem 64px 0 64px;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .Test-testsImage-3Z2de {\n    width: 48px;\n    height: 48px;\n    margin: auto;\n  }\n\n  .Test-section_title-1u6t0 {\n    font-size: 24px;\n    line-height: normal;\n    text-align: center;\n    margin-top: 16px;\n    margin-bottom: 24px;\n  }\n\n  .Test-point-2yHMB {\n    margin-bottom: 8px;\n  }\n\n  .Test-redDot-3mKH- {\n    margin-top: 8px;\n  }\n\n  .Test-pointText-3omy9 {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-imagePart-FumwY .Test-bottomCircle-2SECO {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .Test-imagePart-FumwY .Test-topCircle-2B2HP {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-topCircle-2B2HP {\n    top: -24px;\n    right: 50px;\n    background-color: #f2e5fe;\n  }\n\n  .Test-imagePart-FumwY.Test-onlineTests-2i4lu .Test-bottomCircle-2SECO {\n    bottom: -24px;\n    left: 79px;\n    background-color: #ffe0e8;\n  }\n\n  .Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-topCircle-2B2HP {\n    background-color: #3fc;\n    opacity: 0.3;\n    top: -24px;\n    right: 70px;\n  }\n\n  .Test-imagePart-FumwY.Test-ownPapers-1Gy0r .Test-bottomCircle-2SECO {\n    background-color: #ffe0e8;\n    bottom: -24px;\n    left: 40px;\n  }\n\n  .Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-topCircle-2B2HP {\n    background-color: #0076ff;\n    opacity: 0.2;\n    top: -24px;\n    right: 70px;\n  }\n\n  .Test-imagePart-FumwY.Test-questionBank-l-6mN .Test-bottomCircle-2SECO {\n    background-color: #feb546;\n    bottom: -24px;\n    left: 40px;\n    opacity: 0.2;\n  }\n\n  .Test-teachContainer-4lLOo .Test-downloadApp-h4dCI {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .Test-section_container-2wA5m {\n    min-height: 20rem;\n    padding: 2rem 1rem;\n  }\n\n  .Test-section_container-2wA5m .Test-maxContainer-1_QCO {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    max-width: 600px;\n  }\n\n  .Test-section_container-2wA5m .Test-section_contents-2WASW {\n    margin-top: 32px;\n    max-height: none;\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-section_container_reverse-1IKFo {\n    padding: 24px 20px 48px;\n  }\n\n  .Test-section_container_reverse-1IKFo .Test-section_contents-2WASW {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-contentPart-vOxYm {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding-bottom: 0;\n  }\n\n  .Test-contentPart-vOxYm > div:nth-child(2) {\n    width: 100%;\n    max-width: 400px;\n    margin: auto;\n  }\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s {\n    max-width: none;\n  }\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s .Test-textcontent-3rWf1 {\n    display: block;\n    width: 100%;\n    max-width: 600px;\n    text-align: left;\n    font-size: 14px;\n    line-height: 24px;\n    margin: auto;\n  }\n\n  /* .contentPart .content .textcontent .point .reddotwrapper {\n    margin-top: 9px;\n  }\n\n  .contentPart .content .textcontent .point .pointText {\n    text-align: left;\n  } */\n\n  .Test-contentPart-vOxYm .Test-content-2hs9s .Test-section_title-1u6t0 {\n    font-size: 24px;\n    text-align: left;\n    margin-bottom: 16px;\n  }\n\n  .Test-imagePart-FumwY {\n    width: 100%;\n    margin-top: 48px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n  }\n\n  .Test-imagePart-FumwY .Test-emptyCard-2MxHA {\n    width: 100%;\n    max-width: 360px;\n    height: 180px;\n    margin: 0 auto 32px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD {\n    width: 100%;\n    margin-bottom: 32px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD:last-child {\n    margin-bottom: 0;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_title-1DOvX {\n    font-size: 16px;\n    margin-bottom: 12px;\n  }\n\n  .Test-imagePart-FumwY .Test-videoContents-pQqwC .Test-video_section-2C2PD .Test-content_text-QKFHq {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-mobilePapersContainer-1GPOu {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .Test-mobilePapersContainer-1GPOu img {\n    width: 100%;\n    height: 100%;\n    max-width: 360px;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n  .Test-papersWrapper-1aINd {\n    max-width: 360px;\n    margin: auto;\n  }\n\n  .Test-reports_sub_title-1zU4V {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-reports_notice-1k7xY {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .Test-sub_section_title-3VRu3 {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-headerbackgroundmobile-3FN-6 {\n    width: 232px;\n    height: 335px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .Test-displayClients-18jr5 {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .Test-displayClients-18jr5 h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .Test-displayClients-18jr5 .Test-clientsWrapper-2fDeP .Test-client-FzdBZ {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .Test-displayClients-18jr5 span {\n    max-width: 307px;\n  }\n\n  /* .displayClients .clientsWrapper .dots {\n    display: flex;\n  } */\n  .Test-achievedContainer-1FrOF {\n    padding: 1.5rem 1rem;\n    background: url('/images/home/Confetti.svg');\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO {\n    font-size: 1.5rem;\n    padding: 0;\n    margin-bottom: 1.5rem;\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedHeading-rtdeO span::after {\n    content: '\\A';\n    white-space: pre;\n  }\n\n  .Test-achievedContainer-1FrOF .Test-achievedRow-1lCQf {\n    grid-template-columns: 1fr;\n    gap: 0.75rem;\n    max-width: 20.5rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: auto;\n  }\n\n  /* .customers_container {\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    width: 100%;\n    align-items: center;\n\n    .customer_review {\n      order: 2;\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n\n      .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n\n      .authorWrapper {\n        .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n      }\n    }\n  } */\n\n  .Test-teachContainer-4lLOo .Test-maxContainer-1_QCO {\n    position: static;\n    max-width: 450px;\n  }\n\n  .Test-teachContainer-4lLOo .Test-breadcrum-1j15E {\n    display: none;\n    text-align: center;\n  }\n\n  .Test-teachContainer-4lLOo {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .Test-teachContainer-4lLOo .Test-heading-1oNs5 {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n    }\n\n      .Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 {\n        width: 210px;\n      }\n\n        .Test-teachContainer-4lLOo .Test-heading-1oNs5 .Test-learningText-1PBX3 img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .Test-teachContainer-4lLOo .Test-contentText-2Hp6y {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-requestDemo-EyPHF {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ .Test-whatsapp-3-hIr {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .Test-teachContainer-4lLOo .Test-buttonwrapper-kmrpR .Test-whatsappwrapper-3DmTJ img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .Test-teachContainer-4lLOo .Test-topSection-21bV3 {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachImgBox-32tPP {\n          width: 32px;\n          height: 32px;\n        }\n\n        .Test-teachContainer-4lLOo .Test-topSection-21bV3 .Test-teachSection-gnkFu .Test-teachSectionName-1fAAr {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .Test-availableContainer-3xLT2 {\n    padding: 32px 28px;\n    overflow: hidden;\n  }\n\n    .Test-availableContainer-3xLT2 .Test-availableRow-2Qync {\n      grid-template-columns: 1fr;\n    }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-availableTitle-3NEfA {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co {\n        margin: 0;\n        margin-bottom: 32px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt .Test-platform-kOxAM {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt .Test-platformOs-36PkG {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-row-sE-Co .Test-platformContainer-2Ohnt:last-child {\n          margin-right: 0;\n        }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-desktopImage-2kXNX {\n        width: 90%;\n        height: 161px;\n        margin: 0 auto 0 auto;\n      }\n\n      .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz {\n        -ms-flex-pack: center;\n            justify-content: center;\n        margin-bottom: 32px;\n      }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz .Test-playstore-3PKOC {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .Test-availableContainer-3xLT2 .Test-availableRow-2Qync .Test-store-3Cziz .Test-appstore-1hO16 {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .Test-joinCommunity-3LBUs {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .Test-joinFbText-1fdhM {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .Test-joinFbLink-Avcsf {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .Test-videoSlider-28_ug {\n    padding: 3rem 1rem;\n    min-height: 38rem;\n  }\n\n  .Test-maths-TucEU {\n    position: absolute;\n    left: 15%;\n    top: 84%;\n    width: 36px;\n    height: 36px;\n  }\n\n  .Test-microscope-HsatG {\n    position: absolute;\n    top: 87%;\n    left: 45%;\n    width: 40px;\n    height: 48px;\n  }\n\n  .Test-triangle-2BUKY {\n    position: absolute;\n    top: 85%;\n    left: 80%;\n    width: 36px;\n    height: 30px;\n  }\n\n  .Test-scale-1c9I4 {\n    position: absolute;\n    right: 80%;\n    top: 10%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .Test-circle-2jW1w {\n    position: absolute;\n    top: 3%;\n    right: 50%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .Test-chemistry-2eYOW {\n    position: absolute;\n    top: 8%;\n    right: 5%;\n    width: 36px;\n    height: 48px;\n  }\n\n  .Test-scrollTop-14S8C {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/Test.scss"],"names":[],"mappings":"AAAA;EACE,kBAAkB;EAClB,YAAY;EACZ,mBAAmB;EACnB,aAAa;CACd;;AAED;EACE,qBAAqB;CACtB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,iBAAiB;EACjB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,aAAa;EACb,gBAAgB;EAChB,oBAAoB;CACrB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;CACzB;;AAED;EACE,UAAU;CACX;;AAED;EACE,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,mBAAmB;EACnB,uBAAuB;EACvB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,iBAAiB;EACjB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,YAAY;EACZ,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,mBAAmB;EACnB,mBAAmB;EACnB,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,UAAU;EACV,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,SAAS;CACV;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,WAAW;EACX,QAAQ;CACT;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,UAAU;CACX;;AAED;EACE,mBAAmB;EACnB,SAAS;EACT,WAAW;CACZ;;AAED;EACE,mBAAmB;EACnB,cAAc;EACd,SAAS;EACT,aAAa;EACb,cAAc;CACf;;AAED;IACI,YAAY;IACZ,aAAa;IACb,uBAAuB;OACpB,oBAAoB;GACxB;;AAEH;EACE,eAAe;EACf,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;EACpB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,wBAAwB;MACpB,oBAAoB;EACxB,WAAW;CACZ;;AAED;IACI,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;GAClB;;AAEH;MACM,gBAAgB;MAChB,kBAAkB;MAClB,eAAe;;MAEf;;;;;;;;;;;;;;;;;;;;;;;;;UAyBI;KACL;;AAEL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IA0EI;;AAEJ;EACE,+BAA+B;EAC/B,+BAA+B;EAC/B,iBAAiB;EACjB,0BAA0B;CAC3B;;AAED;IACI,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,cAAc;IACd,sCAAsC;IACtC,aAAa;GACd;;AAEH;MACM,aAAa;MACb,cAAc;KACf;;AAEL;EACE,YAAY;EACZ,cAAc;EACd,8FAA8F;EAC9F,oEAAoE;EACpE,+DAA+D;EAC/D,4DAA4D;EAC5D,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;CACb;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,qBAAqB;EACrB,eAAe;EACf,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;CAClB;;AAED;EACE,YAAY;CACb;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;CAC7B;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,mBAAmB;EACnB,mBAAmB;EACnB,mBAAmB;EACnB,sBAAsB;CACvB;;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;EACpB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,sBAAsB;EACtB,sBAAsB;CACvB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,qBAAqB;MACjB,4BAA4B;EAChC,oBAAoB;EACpB,oBAAoB;CACrB;;AAED;IACI,aAAa;IACb,aAAa;IACb,mBAAmB;GACpB;;AAEH;IACI,aAAa;IACb,aAAa;GACd;;AAEH;EACE,gBAAgB;EAChB,YAAY;EACZ,aAAa;EACb,YAAY;EACZ,aAAa;EACb,WAAW;EACX,gBAAgB;CACjB;;AAED;IACI,YAAY;IACZ,aAAa;GACd;;AAEH;EACE,kBAAkB;EAClB,wBAAwB;EACxB,0BAA0B;EAC1B,mBAAmB;CACpB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,iBAAiB;EACjB,mBAAmB;EACnB,iBAAiB;EACjB,kBAAkB;CACnB;;AAED;EACE,aAAa;EACb,4BAA4B;EAC5B,yBAAyB;EACzB,oBAAoB;EACpB,sBAAsB;EACtB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;CACxB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,mBAAmB;EACnB,aAAa;EACb,QAAQ;EACR,YAAY;EACZ,YAAY;EACZ,iBAAiB;CAClB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,oBAAoB;EACpB,mBAAmB;EACnB,iBAAiB;CAClB;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,YAAY;EACZ,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,mBAAmB;EACnB,uBAAuB;EACvB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,gBAAgB;EAChB,YAAY;EACZ,mBAAmB;EACnB,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,qBAAqB;MACjB,4BAA4B;EAChC,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,gBAAgB;EAChB,gBAAgB;EAChB,iBAAiB;EACjB,iBAAiB;EACjB,0BAA0B;EAC1B,qBAAqB;EACrB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,sBAAsB;MAClB,wBAAwB;EAC5B,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,mBAAmB;EACnB,kBAAkB;EAClB,aAAa;CACd;;AAED;EACE,aAAa;EACb,aAAa;CACd;;AAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAiCI;;AAEJ;EACE,gBAAgB;EAChB,kBAAkB;CACnB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;EAClB,YAAY;EACZ,kBAAkB;EAClB,oBAAoB;CACrB;;AAED;EACE,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,iBAAiB;CAClB;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;CACd;;AAED;EACE,YAAY;EACZ,aAAa;EACb,uBAAuB;KACpB,oBAAoB;CACxB;;AAED;EACE,iBAAiB;EACjB,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,kBAAkB;CACnB;;AAED;;;;;;;;;;;IAWI;;AAEJ;EACE,YAAY;EACZ,kBAAkB;EAClB,wBAAwB;EACxB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,qBAAqB;MACjB,4BAA4B;EAChC,0BAA0B;CAC3B;;AAED;;IAEI;;AAEJ;EACE,mBAAmB;EACnB,gBAAgB;EAChB,kBAAkB;EAClB,iBAAiB;EACjB,eAAe;EACf,cAAc;EACd,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,UAAU;EACV,YAAY;EACZ,eAAe;CAChB;;AAED;EACE,aAAa;EACb,cAAc;CACf;;AAED;EACE,sBAAsB;EACtB,qBAAqB;CACtB;;AAED;EACE,2BAA2B;CAC5B;;AAED;EACE,YAAY;EACZ,mBAAmB;EACnB,uDAAuD;EACvD,yBAAyB;EACzB,4BAA4B;EAC5B,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;MACtB,8BAA8B;CACnC;;AAED;EACE,mBAAmB;EACnB,gBAAgB;EAChB,iBAAiB;EACjB,eAAe;EACf,iBAAiB;EACjB,oBAAoB;CACrB;;AAED;EACE,2BAA2B;EAC3B,wBAAwB;EACxB,mBAAmB;EACnB,cAAc;EACd,sCAAsC;EACtC,sCAAsC;EACtC,UAAU;EACV,UAAU;EACV,aAAa;CACd;;AAED;EACE,aAAa;EACb,cAAc;EACd,gBAAgB;EAChB,kBAAkB;EAClB,6BAA6B;EAC7B,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,sBAAsB;EACtB,+DAA+D;UACvD,uDAAuD;EAC/D,uBAAuB;EACvB,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;MACnB,oBAAoB;CACzB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,qBAAqB;EACrB,cAAc;EACd,sBAAsB;MAClB,wBAAwB;EAC5B,uBAAuB;MACnB,oBAAoB;EACxB,oBAAoB;CACrB;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,0BAA0B;CAC3B;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,eAAe;CAChB;;AAED;EACE,YAAY;CACb;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,mBAAmB;CACpB;;AAED;;;;;;;;;;;;;;IAcI;;AAEJ;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,uBAAuB;EACvB,+BAA+B;EAC/B,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,gCAAgC;QAC5B,4BAA4B;IAChC,YAAY;IACZ,uBAAuB;QACnB,+BAA+B;GACpC;;AAEH;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;EAC3B,0BAA0B;EAC1B,+BAA+B;EAC/B,YAAY;EACZ,aAAa;EACb,aAAa;CACd;;AAED;IACI,qBAAqB;IACrB,cAAc;IACd,wBAAwB;QACpB,oBAAoB;IACxB,YAAY;IACZ,uBAAuB;QACnB,+BAA+B;GACpC;;AAEH;EACE,WAAW;CACZ;;AAED;EACE,aAAa;EACb,cAAc;EACd,mBAAmB;EACnB,mBAAmB;CACpB;;AAED;EACE,YAAY;EACZ,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,WAAW;EACX,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,WAAW;EACX,YAAY;EACZ,0BAA0B;CAC3B;;AAED;EACE,cAAc;EACd,WAAW;EACX,0BAA0B;CAC3B;;AAED;EACE,uBAAuB;EACvB,aAAa;EACb,WAAW;EACX,YAAY;CACb;;AAED;EACE,0BAA0B;EAC1B,cAAc;EACd,WAAW;CACZ;;AAED;EACE,0BAA0B;EAC1B,aAAa;EACb,WAAW;EACX,YAAY;CACb;;AAED;EACE,0BAA0B;EAC1B,cAAc;EACd,WAAW;EACX,aAAa;CACd;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,oBAAoB;MAChB,gBAAgB;EACpB,uBAAuB;MACnB,+BAA+B;CACpC;;AAED;EACE,WAAW;EACX,oBAAoB;CACrB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;EACnB,eAAe;CAChB;;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;CAChB;;AAED;;;;;IAKI;;AAEJ;EACE,gBAAgB;EAChB,kBAAkB;EAClB,uBAAuB;EACvB,eAAe;EACf,aAAa;EACb,mBAAmB;CACpB;;AAED;EACE,cAAc;CACf;;AAED;EACE,qBAAqB;EACrB,cAAc;EACd,2BAA2B;MACvB,uBAAuB;CAC5B;;AAED;EACE;IACE,0BAA0B;GAC3B;CACF;;AAED;EACE;IACE,YAAY;IACZ,aAAa;IACb,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,oBAAoB;IACpB,mBAAmB;IACnB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;GACpB;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,YAAY;IACZ,aAAa;GACd;;EAED;IACE,WAAW;IACX,YAAY;IACZ,0BAA0B;GAC3B;;EAED;IACE,cAAc;IACd,WAAW;IACX,0BAA0B;GAC3B;;EAED;IACE,uBAAuB;IACvB,aAAa;IACb,WAAW;IACX,YAAY;GACb;;EAED;IACE,0BAA0B;IAC1B,cAAc;IACd,WAAW;GACZ;;EAED;IACE,0BAA0B;IAC1B,aAAa;IACb,WAAW;IACX,YAAY;GACb;;EAED;IACE,0BAA0B;IAC1B,cAAc;IACd,WAAW;IACX,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,uBAAuB;QACnB,oBAAoB;IACxB,aAAa;GACd;;EAED;IACE,kBAAkB;IAClB,mBAAmB;GACpB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,2BAA2B;QACvB,uBAAuB;IAC3B,iBAAiB;GAClB;;EAED;IACE,iBAAiB;IACjB,iBAAiB;IACjB,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,wBAAwB;GACzB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,YAAY;IACZ,2BAA2B;QACvB,uBAAuB;IAC3B,qBAAqB;QACjB,4BAA4B;IAChC,kBAAkB;GACnB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,gBAAgB;GACjB;;EAED;IACE,eAAe;IACf,YAAY;IACZ,iBAAiB;IACjB,iBAAiB;IACjB,gBAAgB;IAChB,kBAAkB;IAClB,aAAa;GACd;;EAED;;;;;;MAMI;;EAEJ;IACE,gBAAgB;IAChB,iBAAiB;IACjB,oBAAoB;GACrB;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,sBAAsB;QAClB,wBAAwB;IAC5B,2BAA2B;QACvB,uBAAuB;IAC3B,aAAa;GACd;;EAED;IACE,YAAY;IACZ,iBAAiB;IACjB,cAAc;IACd,oBAAoB;GACrB;;EAED;IACE,2BAA2B;QACvB,uBAAuB;GAC5B;;EAED;IACE,YAAY;IACZ,oBAAoB;GACrB;;EAED;IACE,iBAAiB;GAClB;;EAED;IACE,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,qBAAqB;IACrB,cAAc;IACd,uBAAuB;QACnB,oBAAoB;IACxB,sBAAsB;QAClB,wBAAwB;GAC7B;;EAED;IACE,YAAY;IACZ,aAAa;IACb,iBAAiB;IACjB,uBAAuB;OACpB,oBAAoB;GACxB;;EAED;IACE,iBAAiB;IACjB,aAAa;GACd;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,aAAa;IACb,cAAc;IACd,SAAS;IACT,SAAS;IACT,cAAc;IACd,aAAa;GACd;;EAED;IACE,qBAAqB;IACrB,kBAAkB;GACnB;;EAED;IACE,kBAAkB;IAClB,aAAa;IACb,mBAAmB;IACnB,oBAAoB;IACpB,qBAAqB;GACtB;;EAED;IACE,oBAAoB;IACpB,mBAAmB;IACnB,eAAe;IACf,sCAAsC;IACtC,mCAAmC;IACnC,UAAU;IACV,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;GACpB;;EAED;IACE,eAAe;IACf,aAAa;GACd;;EAED;;MAEI;;EAEJ;IACE,iBAAiB;GAClB;;EAED;;MAEI;EACJ;IACE,qBAAqB;IACrB,6CAA6C;GAC9C;;EAED;IACE,kBAAkB;IAClB,WAAW;IACX,sBAAsB;GACvB;;EAED;IACE,cAAc;IACd,iBAAiB;GAClB;;EAED;IACE,2BAA2B;IAC3B,aAAa;IACb,mBAAmB;IACnB,2BAA2B;IAC3B,wBAAwB;IACxB,mBAAmB;IACnB,aAAa;GACd;;EAED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAwCI;;EAEJ;IACE,iBAAiB;IACjB,iBAAiB;GAClB;;EAED;IACE,cAAc;IACd,mBAAmB;GACpB;;EAED;IACE,cAAc;IACd,mBAAmB;IACnB,mBAAmB;GACpB;;IAEC;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,gBAAgB;MAChB,yBAAyB;KAC1B;;MAEC;QACE,aAAa;OACd;;QAEC;UACE,QAAQ;UACR,iBAAiB;SAClB;;IAEL;MACE,gBAAgB;MAChB,kBAAkB;MAClB,mBAAmB;MACnB,iBAAiB;MACjB,yBAAyB;KAC1B;;IAED;MACE,2BAA2B;UACvB,uBAAuB;MAC3B,iBAAiB;KAClB;;MAEC;QACE,kBAAkB;QAClB,gBAAgB;QAChB,kBAAkB;QAClB,gBAAgB;OACjB;;MAED;QACE,iBAAiB;QACjB,oBAAoB;OACrB;;QAEC;UACE,gBAAgB;UAChB,kBAAkB;UAClB,WAAW;UACX,eAAe;SAChB;;QAED;UACE,YAAY;UACZ,aAAa;SACd;;IAEL;MACE,sBAAsB;UAClB,wBAAwB;MAC5B,cAAc;;MAEd;;UAEI;KACL;QACG;UACE,YAAY;UACZ,aAAa;SACd;;QAED;UACE,gBAAgB;UAChB,kBAAkB;SACnB;;EAEP;IACE,mBAAmB;IACnB,iBAAiB;GAClB;;IAEC;MACE,2BAA2B;KAC5B;;MAEC;QACE,gBAAgB;QAChB,mBAAmB;QACnB,oBAAoB;OACrB;;MAED;QACE,UAAU;QACV,oBAAoB;QACpB,sBAAsB;YAClB,wBAAwB;OAC7B;;QAEC;UACE,YAAY;UACZ,qBAAqB;cACjB,4BAA4B;UAChC,mBAAmB;UACnB,iBAAiB;SAClB;;UAEC;YACE,kBAAkB;YAClB,kBAAkB;WACnB;;UAED;YACE,gBAAgB;YAChB,kBAAkB;WACnB;;QAEH;UACE,gBAAgB;SACjB;;MAEH;QACE,WAAW;QACX,cAAc;QACd,sBAAsB;OACvB;;MAED;QACE,sBAAsB;YAClB,wBAAwB;QAC5B,oBAAoB;OACrB;;QAEC;UACE,aAAa;UACb,aAAa;UACb,mBAAmB;SACpB;;QAED;UACE,aAAa;UACb,aAAa;UACb,UAAU;SACX;;EAEP;IACE,2BAA2B;QACvB,uBAAuB;IAC3B,sBAAsB;QAClB,wBAAwB;IAC5B,cAAc;GACf;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;IAClB,gBAAgB;IAChB,oBAAoB;GACrB;;EAED;IACE,mBAAmB;IACnB,gBAAgB;IAChB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,kBAAkB;GACnB;;EAED;IACE,mBAAmB;IACnB,UAAU;IACV,SAAS;IACT,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,SAAS;IACT,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,SAAS;IACT,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,WAAW;IACX,SAAS;IACT,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,WAAW;IACX,YAAY;IACZ,aAAa;GACd;;EAED;IACE,mBAAmB;IACnB,QAAQ;IACR,UAAU;IACV,YAAY;IACZ,aAAa;GACd;;EAED;IACE,YAAY;IACZ,aAAa;IACb,YAAY;IACZ,aAAa;GACd;CACF","file":"Test.scss","sourcesContent":[".maxContainer {\n  max-width: 1700px;\n  width: 100%;\n  position: relative;\n  margin: auto;\n}\n\n.marginBottom126 {\n  margin-bottom: 126px;\n}\n\n.reports_sub_title {\n  font-size: 28px;\n  line-height: 32px;\n  font-weight: 600;\n  margin-top: 12px;\n  color: #25282b;\n}\n\n.reports_notice {\n  font-size: 20px;\n  line-height: 32px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-top: 4px;\n  margin-bottom: 12px;\n}\n\n.row1 {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n}\n\n.pointContainer {\n  width: 5%;\n}\n\n.point {\n  margin-bottom: 12px;\n}\n\n.pointText {\n  font-size: 20px;\n  line-height: 32px;\n}\n\n.redDot {\n  width: 8px;\n  height: 8px;\n  border-radius: 4px;\n  background-color: #f36;\n  margin-right: 20px;\n  margin-top: 12px;\n}\n\n.testsImage {\n  width: 72px;\n  height: 72px;\n  margin-top: -8px;\n  margin-right: 24px;\n}\n\n.testsImage img {\n  width: 100%;\n  height: 100%;\n}\n\n.section_container.ashBackground {\n  background-color: #f7f7f7;\n}\n\n.videoSlider {\n  width: 100%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  padding: 80px 64px;\n  padding: 5rem 4rem;\n  background-color: #f7f7f7;\n  position: relative;\n}\n\n.maths {\n  position: absolute;\n  left: 10%;\n  top: 5%;\n}\n\n.microscope {\n  position: absolute;\n  top: 30%;\n  left: 3%;\n}\n\n.triangle {\n  position: absolute;\n  top: 60%;\n  left: 10%;\n}\n\n.scale {\n  position: absolute;\n  right: 10%;\n  top: 5%;\n}\n\n.circle {\n  position: absolute;\n  top: 30%;\n  right: 3%;\n}\n\n.chemistry {\n  position: absolute;\n  top: 60%;\n  right: 10%;\n}\n\n.headerbackgroundmobile {\n  position: absolute;\n  bottom: -56px;\n  right: 0;\n  width: 513px;\n  height: 560px;\n}\n\n.headerbackgroundmobile img {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n.section_title {\n  display: block;\n  margin: 0 0 16px;\n  font-size: 40px;\n  line-height: 48px;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.sub_section_title {\n  font-size: 28px;\n  line-height: 32px;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 12px;\n  margin-top: 56px;\n}\n\n.contentPart {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  width: 45%;\n}\n\n.contentPart .content {\n    width: 100%;\n    min-width: 290px;\n    max-width: 540px;\n  }\n\n.contentPart .content .textcontent {\n      font-size: 16px;\n      line-height: 24px;\n      color: #25282b;\n\n      /* .point {\n        width: 100%;\n        display: flex;\n        align-items: flex-start;\n        height: max-content;\n        margin: 6px 0;\n\n        .reddotwrapper {\n          width: 5%;\n          display: flex;\n          align-items: center;\n          justify-content: flex-start;\n          margin-top: 12px;\n\n          .reddot {\n            width: 8px;\n            height: 8px;\n            background-color: #f36;\n            border-radius: 50%;\n          }\n        }\n\n        .pointText {\n          width: 95%;\n        }\n      } */\n    }\n\n/* .customers_container {\n  background-color: #f36;\n  color: #fff;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 560px;\n\n  .customer_review {\n    order: 0;\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    padding: 48px 64px;\n    width: 50%;\n\n    .customerLogo {\n      width: 120px;\n      height: 80px;\n      border-radius: 4px;\n      margin-bottom: 24px;\n\n      img {\n        width: 100%;\n        height: 100%;\n        object-fit: contain;\n      }\n    }\n\n    .sriChaitanyaText {\n      font-size: 20px;\n      line-height: 32px;\n      text-align: left;\n      margin-bottom: 32px;\n      max-width: 600px;\n    }\n\n    .authorWrapper {\n      display: flex;\n      flex-direction: column;\n\n      .author_title {\n        font-size: 20px;\n        line-height: 24px;\n        font-weight: 600;\n        color: #fff;\n        margin-bottom: 8px;\n      }\n\n      .about_author {\n        font-size: 16px;\n        line-height: 20px;\n        margin-bottom: 64px;\n      }\n    }\n\n    .allcustomers {\n      font-size: 14px;\n      line-height: 20px;\n      font-weight: 600;\n      color: #fff;\n      display: flex;\n      flex-direction: row;\n      align-items: center;\n\n      img {\n        margin-top: 3px;\n        margin-left: 5px;\n        width: 20px;\n        height: 20px;\n        object-fit: contain;\n      }\n    }\n  }\n} */\n\n.availableContainer {\n  padding: 80px 113px 56px 164px;\n  padding: 5rem 113px 56px 164px;\n  overflow: hidden;\n  background-color: #f7f7f7;\n}\n\n.availableContainer .availableRow {\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    margin: auto;\n  }\n\n.availableContainer .availableRow .desktopImage {\n      width: 696px;\n      height: 371px;\n    }\n\n.joinCommunity {\n  width: 100%;\n  height: 160px;\n  background-image: -webkit-gradient(linear, left bottom, left top, from(#ea4c70), to(#b2457c));\n  background-image: -webkit-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: -o-linear-gradient(bottom, #ea4c70, #b2457c);\n  background-image: linear-gradient(to top, #ea4c70, #b2457c);\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.joinFbText {\n  font-size: 32px;\n  line-height: 48px;\n  font-weight: 600;\n  margin-right: 64px;\n  color: #fff;\n}\n\n.joinFbLink {\n  padding: 16px 40px;\n  background-color: #fff;\n  border-radius: 100px;\n  color: #25282b;\n  font-size: 20px;\n  line-height: 30px;\n  font-weight: 600;\n}\n\n.availableTitle {\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n}\n\n.available {\n  color: #f36;\n}\n\n.availableContentSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: center;\n      justify-content: center;\n}\n\n.platformContainer {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  margin-right: 32px;\n  margin-right: 2rem;\n  margin-bottom: 8px;\n  margin-bottom: 0.5rem;\n}\n\n.platform {\n  font-size: 16px;\n  font-weight: 600;\n  margin: 8px 0 4px 0;\n  color: #25282b;\n}\n\n.platformOs {\n  font-size: 12px;\n  opacity: 0.6;\n}\n\n.row {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  margin: 48px 0 32px 0;\n  margin: 3rem 0 2rem 0;\n}\n\n.store {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  margin-bottom: 64px;\n  margin-bottom: 4rem;\n}\n\n.store .playstore {\n    width: 136px;\n    height: 40px;\n    margin-right: 16px;\n  }\n\n.store .appstore {\n    width: 136px;\n    height: 40px;\n  }\n\n.scrollTop {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  right: 64px;\n  bottom: 64px;\n  z-index: 1;\n  cursor: pointer;\n}\n\n.scrollTop img {\n    width: 100%;\n    height: 100%;\n  }\n\n.teachContainer {\n  min-height: 720px;\n  padding: 16px 64px 56px;\n  background-color: #f2e5fe;\n  position: relative;\n}\n\n.teachContainer .heading {\n  font-size: 48px;\n  line-height: 66px;\n  color: #25282b;\n  max-width: 740px;\n  margin: 12px 0 0 0;\n  text-align: left;\n  font-weight: bold;\n}\n\n.teachContainer .heading .learningText {\n  width: 316px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  display: inline-block;\n  position: relative;\n}\n\n.imagePart .emptyCard img {\n  width: 100%;\n  height: 100%;\n  // object-fit: contain;\n}\n\n.imagePart .topCard img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer .heading .learningText img {\n  position: absolute;\n  bottom: -2px;\n  left: 0;\n  width: 100%;\n  height: 8px;\n  max-width: 308px;\n}\n\n.teachContainer .contentText {\n  font-size: 20px;\n  line-height: 40px;\n  color: #25282b;\n  font-weight: normal;\n  margin: 12px 0 0 0;\n  max-width: 687px;\n}\n\n.teachContainer .buttonwrapper {\n  margin-top: 56px;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .requestDemo {\n  border-radius: 4px;\n  background-color: #3fc;\n  font-size: 20px;\n  font-weight: 600;\n  line-height: 1.5;\n  cursor: pointer;\n  color: #000;\n  padding: 16px 24px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 20px;\n  cursor: pointer;\n  font-weight: 600;\n  line-height: 1.5;\n  color: #25282b !important;\n  margin: 0 8px 0 32px;\n  padding-bottom: 0;\n  opacity: 0.6;\n}\n\n.teachContainer .buttonwrapper .whatsappwrapper img {\n  width: 32px;\n  height: 32px;\n}\n\n.teachContainer .downloadApp {\n  margin-top: 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: start;\n      align-items: flex-start;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.teachContainer .downloadApp .downloadText {\n  text-align: center;\n  margin-bottom: 8px;\n  line-height: 24px;\n  opacity: 0.7;\n}\n\n.teachContainer .downloadApp .playStoreIcon {\n  width: 132px;\n  height: 40px;\n}\n\n/* .teachContainer .actionsWrapper {\n  position: absolute;\n  bottom: 26px;\n  width: 100%;\n  display: flex;\n}\n.teachContainer .actionsWrapper .action {\n  width: fit-content;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: 24px;\n}\n.teachContainer .actionsWrapper .action span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  opacity: 0.7;\n  text-align: left;\n  margin-left: 8px;\n}\n.teachContainer .actionsWrapper .action .actionimgbox {\n  width: 24px;\n  height: 24px;\n  background-color: #fff;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.teachContainer .actionsWrapper .action .actionimgbox img {\n  width: 10px;\n  height: 9px;\n} */\n\n.teachContainer .breadcrum {\n  font-size: 14px;\n  line-height: 20px;\n}\n\n.displayClients span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #0076ff;\n  text-align: right;\n  width: 100%;\n  max-width: 1152px;\n  margin: 12px auto 0;\n}\n\n.teachContainer .breadcrum span {\n  font-weight: 600;\n}\n\n.teachContainer .topSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-top: 56px;\n}\n\n.teachContainer .topSection .teachSection {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.teachContainer .topSection .teachSection .teachImgBox {\n  width: 40px;\n  height: 40px;\n}\n\n.teachContainer .topSection .teachSection .teachImgBox img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.teachContainer .topSection .teachSection .teachSectionName {\n  margin-left: 8px;\n  font-size: 20px;\n  line-height: 30px;\n  color: #8800fe;\n  font-weight: bold;\n}\n\n/* .teachContainer .topSection .featuresSection {\n  display: flex;\n  align-items: center;\n  margin-left: 30%;\n}\n.teachContainer .topSection .featuresSection span {\n  font-size: 14px;\n  line-height: 20px;\n  color: #25282b;\n  margin-right: 32px;\n  font-weight: 500;\n} */\n\n.displayClients {\n  width: 100%;\n  min-height: 464px;\n  padding: 56px 64px 40px;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: start;\n      justify-content: flex-start;\n  background-color: #f7f7f7;\n}\n\n/* .displayClients .dots {\n  display: none;\n} */\n\n.displayClients h3 {\n  text-align: center;\n  font-size: 40px;\n  line-height: 48px;\n  font-weight: 600;\n  color: #25282b;\n  margin-top: 0;\n  margin-bottom: 40px;\n}\n\n.displayClients .clientsWrapper {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(6, 1fr);\n  gap: 24px;\n  gap: 1.5rem;\n  margin: 0 auto;\n}\n\n.displayClients .clientsWrapper .client {\n  width: 172px;\n  height: 112px;\n}\n\n.displayClients span a {\n  text-decoration: none;\n  text-transform: none;\n}\n\n.displayClients span a:hover {\n  text-decoration: underline;\n}\n\n.achievedContainer {\n  width: 100%;\n  padding: 56px 64px;\n  background-image: url('/images/home/new_confetti.svg');\n  background-size: contain;\n  background-position: center;\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.achievedContainer .achievedHeading {\n  text-align: center;\n  font-size: 40px;\n  line-height: 1.2;\n  color: #25282b;\n  font-weight: 600;\n  margin-bottom: 40px;\n}\n\n.achievedContainer .achievedRow {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  // grid-template-rows: repeat(2, 1fr);\n  gap: 16px;\n  gap: 1rem;\n  margin: auto;\n}\n\n.achievedContainer .achievedRow .card {\n  width: 270px;\n  height: 202px;\n  font-size: 24px;\n  font-size: 1.5rem;\n  color: rgba(37, 40, 43, 0.6);\n  line-height: 40px;\n  padding: 24px;\n  padding: 1.5rem;\n  border-radius: 0.5rem;\n  -webkit-box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n          box-shadow: 0 0.25rem 1.5rem 0 rgba(140, 0, 254, 0.16);\n  background-color: #fff;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  -ms-flex-align: center;\n      align-items: center;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile {\n  width: 52px;\n  height: 52px;\n  border-radius: 50%;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: center;\n      justify-content: center;\n  -ms-flex-align: center;\n      align-items: center;\n  margin-bottom: 20px;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.clients {\n  background-color: #fff3eb;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.students {\n  background-color: #f7effe;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.tests {\n  background-color: #ffebf0;\n}\n\n.achievedContainer .achievedRow .card .achievedProfile.questions {\n  background-color: #ebffef;\n}\n\n.achievedContainer .achievedRow .card .highlight {\n  font-size: 36px;\n  font-weight: bold;\n  line-height: 40px;\n  text-align: center;\n}\n\n.achievedContainer .achievedRow .card .highlight.questionsHighlight {\n  color: #00ac26;\n}\n\n.achievedContainer .achievedRow .card .highlight.testsHighlight {\n  color: #f36;\n}\n\n.achievedContainer .achievedRow .card .highlight.studentsHighlight {\n  color: #8c00fe;\n}\n\n.achievedContainer .achievedRow .card .highlight.clientsHighlight {\n  color: #f60;\n}\n\n.achievedContainer .achievedRow .card .subText {\n  font-size: 24px;\n  line-height: 40px;\n  text-align: center;\n}\n\n/* .toggleAtRight {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.toggleAtLeft {\n  width: 100%;\n  padding: 56px 64px 0 64px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n} */\n\n.section_container {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #fff;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.section_container .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row-reverse;\n        flex-direction: row-reverse;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.section_container_reverse {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n  background-color: #f7f7f7;\n  padding: 160px 32px 160px 64px;\n  width: 100%;\n  height: 100%;\n  margin: auto;\n}\n\n.section_container_reverse .section_contents {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: row;\n        flex-direction: row;\n    width: 100%;\n    -ms-flex-pack: justify;\n        justify-content: space-between;\n  }\n\n.imagePart {\n  width: 50%;\n}\n\n.imagePart .emptyCard {\n  width: 606px;\n  height: 341px;\n  border-radius: 8px;\n  position: relative;\n}\n\n.imagePart .topCard {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #fff;\n  z-index: 1;\n  border-radius: 8px;\n}\n\n.imagePart .bottomCircle {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.imagePart .topCircle {\n  position: absolute;\n  width: 88px;\n  height: 88px;\n  border-radius: 50%;\n}\n\n.imagePart.onlineTests .topCircle {\n  top: -44px;\n  right: 50px;\n  background-color: #f2e5fe;\n}\n\n.imagePart.onlineTests .bottomCircle {\n  bottom: -44px;\n  left: 79px;\n  background-color: #ffe0e8;\n}\n\n.imagePart.ownPapers .topCircle {\n  background-color: #3fc;\n  opacity: 0.3;\n  top: -44px;\n  right: 70px;\n}\n\n.imagePart.ownPapers .bottomCircle {\n  background-color: #ffe0e8;\n  bottom: -44px;\n  left: 40px;\n}\n\n.imagePart.questionBank .topCircle {\n  background-color: #0076ff;\n  opacity: 0.2;\n  top: -44px;\n  right: 70px;\n}\n\n.imagePart.questionBank .bottomCircle {\n  background-color: #feb546;\n  bottom: -44px;\n  left: 40px;\n  opacity: 0.2;\n}\n\n.imagePart .videoContents {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -ms-flex-pack: justify;\n      justify-content: space-between;\n}\n\n.imagePart .videoContents .video_section {\n  width: 45%;\n  margin-bottom: 56px;\n}\n\n.imagePart .videoContents .video_section .content_title {\n  font-size: 20px;\n  font-weight: bold;\n  line-height: 30px;\n  margin-bottom: 8px;\n  color: #25282b;\n}\n\n.imagePart .videoContents .video_section .content_text {\n  font-size: 16px;\n  line-height: 24px;\n  color: #25282b;\n}\n\n/* .allcustomers p {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n} */\n\n.content p p {\n  font-size: 16px;\n  line-height: 24px;\n  letter-spacing: 0.48px;\n  color: #25282b;\n  opacity: 0.6;\n  margin-bottom: 8px;\n}\n\n.hide {\n  display: none;\n}\n\n.show {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: column;\n      flex-direction: column;\n}\n\n@media only screen and (max-width: 1200px) {\n  .availableContainer {\n    padding: 5rem 64px 0 64px;\n  }\n}\n\n@media only screen and (max-width: 990px) {\n  .testsImage {\n    width: 48px;\n    height: 48px;\n    margin: auto;\n  }\n\n  .section_title {\n    font-size: 24px;\n    line-height: normal;\n    text-align: center;\n    margin-top: 16px;\n    margin-bottom: 24px;\n  }\n\n  .point {\n    margin-bottom: 8px;\n  }\n\n  .redDot {\n    margin-top: 8px;\n  }\n\n  .pointText {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .imagePart .bottomCircle {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .imagePart .topCircle {\n    position: absolute;\n    width: 48px;\n    height: 48px;\n  }\n\n  .imagePart.onlineTests .topCircle {\n    top: -24px;\n    right: 50px;\n    background-color: #f2e5fe;\n  }\n\n  .imagePart.onlineTests .bottomCircle {\n    bottom: -24px;\n    left: 79px;\n    background-color: #ffe0e8;\n  }\n\n  .imagePart.ownPapers .topCircle {\n    background-color: #3fc;\n    opacity: 0.3;\n    top: -24px;\n    right: 70px;\n  }\n\n  .imagePart.ownPapers .bottomCircle {\n    background-color: #ffe0e8;\n    bottom: -24px;\n    left: 40px;\n  }\n\n  .imagePart.questionBank .topCircle {\n    background-color: #0076ff;\n    opacity: 0.2;\n    top: -24px;\n    right: 70px;\n  }\n\n  .imagePart.questionBank .bottomCircle {\n    background-color: #feb546;\n    bottom: -24px;\n    left: 40px;\n    opacity: 0.2;\n  }\n\n  .teachContainer .downloadApp {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-align: center;\n        align-items: center;\n    margin: auto;\n  }\n\n  .section_container {\n    min-height: 20rem;\n    padding: 2rem 1rem;\n  }\n\n  .section_container .maxContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    max-width: 600px;\n  }\n\n  .section_container .section_contents {\n    margin-top: 32px;\n    max-height: none;\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .section_container_reverse {\n    padding: 24px 20px 48px;\n  }\n\n  .section_container_reverse .section_contents {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .contentPart {\n    width: 100%;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: start;\n        justify-content: flex-start;\n    padding-bottom: 0;\n  }\n\n  .contentPart > div:nth-child(2) {\n    width: 100%;\n    max-width: 400px;\n    margin: auto;\n  }\n\n  .contentPart .content {\n    max-width: none;\n  }\n\n  .contentPart .content .textcontent {\n    display: block;\n    width: 100%;\n    max-width: 600px;\n    text-align: left;\n    font-size: 14px;\n    line-height: 24px;\n    margin: auto;\n  }\n\n  /* .contentPart .content .textcontent .point .reddotwrapper {\n    margin-top: 9px;\n  }\n\n  .contentPart .content .textcontent .point .pointText {\n    text-align: left;\n  } */\n\n  .contentPart .content .section_title {\n    font-size: 24px;\n    text-align: left;\n    margin-bottom: 16px;\n  }\n\n  .imagePart {\n    width: 100%;\n    margin-top: 48px;\n    -ms-flex-pack: center;\n        justify-content: center;\n    -ms-flex-direction: column;\n        flex-direction: column;\n    height: 100%;\n  }\n\n  .imagePart .emptyCard {\n    width: 100%;\n    max-width: 360px;\n    height: 180px;\n    margin: 0 auto 32px;\n  }\n\n  .imagePart .videoContents {\n    -ms-flex-direction: column;\n        flex-direction: column;\n  }\n\n  .imagePart .videoContents .video_section {\n    width: 100%;\n    margin-bottom: 32px;\n  }\n\n  .imagePart .videoContents .video_section:last-child {\n    margin-bottom: 0;\n  }\n\n  .imagePart .videoContents .video_section .content_title {\n    font-size: 16px;\n    margin-bottom: 12px;\n  }\n\n  .imagePart .videoContents .video_section .content_text {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .mobilePapersContainer {\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-align: center;\n        align-items: center;\n    -ms-flex-pack: center;\n        justify-content: center;\n  }\n\n  .mobilePapersContainer img {\n    width: 100%;\n    height: 100%;\n    max-width: 360px;\n    -o-object-fit: contain;\n       object-fit: contain;\n  }\n\n  .papersWrapper {\n    max-width: 360px;\n    margin: auto;\n  }\n\n  .reports_sub_title {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .reports_notice {\n    font-size: 14px;\n    line-height: 24px;\n  }\n\n  .sub_section_title {\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .headerbackgroundmobile {\n    width: 232px;\n    height: 335px;\n    right: 0;\n    left: 0%;\n    bottom: -1rem;\n    margin: auto;\n  }\n\n  .displayClients {\n    padding: 1.5rem 1rem;\n    min-height: 27rem;\n  }\n\n  .displayClients h3 {\n    font-size: 1.5rem;\n    margin: auto;\n    text-align: center;\n    line-height: normal;\n    max-width: 14.875rem;\n  }\n\n  .displayClients .clientsWrapper {\n    padding: 1.5rem 0 0;\n    position: relative;\n    margin: 0 auto;\n    grid-template-columns: repeat(2, 1fr);\n    grid-template-rows: repeat(2, 1fr);\n    gap: 1rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n  }\n\n  .displayClients .clientsWrapper .client {\n    width: 9.75rem;\n    height: 7rem;\n  }\n\n  /* .displayClients .clientsWrapper .client.active {\n    display: block;\n  } */\n\n  .displayClients span {\n    max-width: 307px;\n  }\n\n  /* .displayClients .clientsWrapper .dots {\n    display: flex;\n  } */\n  .achievedContainer {\n    padding: 1.5rem 1rem;\n    background: url('/images/home/Confetti.svg');\n  }\n\n  .achievedContainer .achievedHeading {\n    font-size: 1.5rem;\n    padding: 0;\n    margin-bottom: 1.5rem;\n  }\n\n  .achievedContainer .achievedHeading span::after {\n    content: '\\a';\n    white-space: pre;\n  }\n\n  .achievedContainer .achievedRow {\n    grid-template-columns: 1fr;\n    gap: 0.75rem;\n    max-width: 20.5rem;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: auto;\n  }\n\n  /* .customers_container {\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    width: 100%;\n    align-items: center;\n\n    .customer_review {\n      order: 2;\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      text-align: center;\n      width: 100%;\n      height: 50%;\n      padding: 32px 16px;\n\n      .customerLogo {\n        width: 88px;\n        height: 56px;\n        border-radius: 8px;\n        overflow: hidden;\n        margin-bottom: 32px;\n      }\n\n      .sriChaitanyaText {\n        font-size: 14px;\n        line-height: 24px;\n        text-align: center;\n        max-width: none;\n      }\n\n      .authorWrapper {\n        .about_author {\n          font-size: 14px;\n          line-height: 24px;\n          margin-bottom: 34px;\n        }\n      }\n    }\n  } */\n\n  .teachContainer .maxContainer {\n    position: static;\n    max-width: 450px;\n  }\n\n  .teachContainer .breadcrum {\n    display: none;\n    text-align: center;\n  }\n\n  .teachContainer {\n    height: 68rem;\n    padding: 32px 16px;\n    position: relative;\n  }\n\n    .teachContainer .heading {\n      font-size: 32px;\n      line-height: 48px;\n      text-align: center;\n      max-width: none;\n      margin: 32px auto 0 auto;\n    }\n\n      .teachContainer .heading .learningText {\n        width: 210px;\n      }\n\n        .teachContainer .heading .learningText img {\n          left: 0;\n          min-width: 140px;\n        }\n\n    .teachContainer .contentText {\n      font-size: 16px;\n      line-height: 24px;\n      text-align: center;\n      max-width: 650px;\n      margin: 16px auto 0 auto;\n    }\n\n    .teachContainer .buttonwrapper {\n      -ms-flex-direction: column;\n          flex-direction: column;\n      margin-top: 48px;\n    }\n\n      .teachContainer .buttonwrapper .requestDemo {\n        padding: 8px 16px;\n        font-size: 16px;\n        line-height: 24px;\n        min-width: none;\n      }\n\n      .teachContainer .buttonwrapper .whatsappwrapper {\n        margin-top: 24px;\n        margin-bottom: 40px;\n      }\n\n        .teachContainer .buttonwrapper .whatsappwrapper .whatsapp {\n          font-size: 16px;\n          line-height: 24px;\n          opacity: 1;\n          margin-left: 0;\n        }\n\n        .teachContainer .buttonwrapper .whatsappwrapper img {\n          width: 24px;\n          height: 24px;\n        }\n\n    .teachContainer .topSection {\n      -ms-flex-pack: center;\n          justify-content: center;\n      margin-top: 0;\n\n      /* .featuresSection {\n        display: none;\n      } */\n    }\n        .teachContainer .topSection .teachSection .teachImgBox {\n          width: 32px;\n          height: 32px;\n        }\n\n        .teachContainer .topSection .teachSection .teachSectionName {\n          font-size: 16px;\n          line-height: 24px;\n        }\n\n  .availableContainer {\n    padding: 32px 28px;\n    overflow: hidden;\n  }\n\n    .availableContainer .availableRow {\n      grid-template-columns: 1fr;\n    }\n\n      .availableContainer .availableRow .availableTitle {\n        font-size: 32px;\n        text-align: center;\n        margin-bottom: 32px;\n      }\n\n      .availableContainer .availableRow .row {\n        margin: 0;\n        margin-bottom: 32px;\n        -ms-flex-pack: center;\n            justify-content: center;\n      }\n\n        .availableContainer .availableRow .row .platformContainer {\n          width: 68px;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n          margin-right: 24px;\n          margin-bottom: 0;\n        }\n\n          .availableContainer .availableRow .row .platformContainer .platform {\n            font-size: 13.3px;\n            line-height: 20px;\n          }\n\n          .availableContainer .availableRow .row .platformContainer .platformOs {\n            font-size: 10px;\n            line-height: 15px;\n          }\n\n        .availableContainer .availableRow .row .platformContainer:last-child {\n          margin-right: 0;\n        }\n\n      .availableContainer .availableRow .desktopImage {\n        width: 90%;\n        height: 161px;\n        margin: 0 auto 0 auto;\n      }\n\n      .availableContainer .availableRow .store {\n        -ms-flex-pack: center;\n            justify-content: center;\n        margin-bottom: 32px;\n      }\n\n        .availableContainer .availableRow .store .playstore {\n          width: 140px;\n          height: 42px;\n          margin: 0 16px 0 0;\n        }\n\n        .availableContainer .availableRow .store .appstore {\n          width: 140px;\n          height: 42px;\n          margin: 0;\n        }\n\n  .joinCommunity {\n    -ms-flex-direction: column;\n        flex-direction: column;\n    -ms-flex-pack: center;\n        justify-content: center;\n    height: 144px;\n  }\n\n  .joinFbText {\n    text-align: center;\n    font-size: 20px;\n    line-height: 32px;\n    margin-right: 0;\n    margin-bottom: 16px;\n  }\n\n  .joinFbLink {\n    padding: 12px 32px;\n    font-size: 16px;\n    line-height: 24px;\n  }\n\n  .videoSlider {\n    padding: 3rem 1rem;\n    min-height: 38rem;\n  }\n\n  .maths {\n    position: absolute;\n    left: 15%;\n    top: 84%;\n    width: 36px;\n    height: 36px;\n  }\n\n  .microscope {\n    position: absolute;\n    top: 87%;\n    left: 45%;\n    width: 40px;\n    height: 48px;\n  }\n\n  .triangle {\n    position: absolute;\n    top: 85%;\n    left: 80%;\n    width: 36px;\n    height: 30px;\n  }\n\n  .scale {\n    position: absolute;\n    right: 80%;\n    top: 10%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .circle {\n    position: absolute;\n    top: 3%;\n    right: 50%;\n    width: 40px;\n    height: 30px;\n  }\n\n  .chemistry {\n    position: absolute;\n    top: 8%;\n    right: 5%;\n    width: 36px;\n    height: 48px;\n  }\n\n  .scrollTop {\n    width: 32px;\n    height: 32px;\n    right: 16px;\n    bottom: 16px;\n  }\n}\n"],"sourceRoot":""}]);

// exports
exports.locals = {
	"maxContainer": "Test-maxContainer-1_QCO",
	"marginBottom126": "Test-marginBottom126-2y-TJ",
	"reports_sub_title": "Test-reports_sub_title-1zU4V",
	"reports_notice": "Test-reports_notice-1k7xY",
	"row1": "Test-row1-1pBIZ",
	"pointContainer": "Test-pointContainer-yl6rU",
	"point": "Test-point-2yHMB",
	"pointText": "Test-pointText-3omy9",
	"redDot": "Test-redDot-3mKH-",
	"testsImage": "Test-testsImage-3Z2de",
	"section_container": "Test-section_container-2wA5m",
	"ashBackground": "Test-ashBackground-q1E-3",
	"videoSlider": "Test-videoSlider-28_ug",
	"maths": "Test-maths-TucEU",
	"microscope": "Test-microscope-HsatG",
	"triangle": "Test-triangle-2BUKY",
	"scale": "Test-scale-1c9I4",
	"circle": "Test-circle-2jW1w",
	"chemistry": "Test-chemistry-2eYOW",
	"headerbackgroundmobile": "Test-headerbackgroundmobile-3FN-6",
	"section_title": "Test-section_title-1u6t0",
	"sub_section_title": "Test-sub_section_title-3VRu3",
	"contentPart": "Test-contentPart-vOxYm",
	"content": "Test-content-2hs9s",
	"textcontent": "Test-textcontent-3rWf1",
	"availableContainer": "Test-availableContainer-3xLT2",
	"availableRow": "Test-availableRow-2Qync",
	"desktopImage": "Test-desktopImage-2kXNX",
	"joinCommunity": "Test-joinCommunity-3LBUs",
	"joinFbText": "Test-joinFbText-1fdhM",
	"joinFbLink": "Test-joinFbLink-Avcsf",
	"availableTitle": "Test-availableTitle-3NEfA",
	"available": "Test-available-3lWLX",
	"availableContentSection": "Test-availableContentSection-UP4ln",
	"platformContainer": "Test-platformContainer-2Ohnt",
	"platform": "Test-platform-kOxAM",
	"platformOs": "Test-platformOs-36PkG",
	"row": "Test-row-sE-Co",
	"store": "Test-store-3Cziz",
	"playstore": "Test-playstore-3PKOC",
	"appstore": "Test-appstore-1hO16",
	"scrollTop": "Test-scrollTop-14S8C",
	"teachContainer": "Test-teachContainer-4lLOo",
	"heading": "Test-heading-1oNs5",
	"learningText": "Test-learningText-1PBX3",
	"imagePart": "Test-imagePart-FumwY",
	"emptyCard": "Test-emptyCard-2MxHA",
	"topCard": "Test-topCard-3A_No",
	"contentText": "Test-contentText-2Hp6y",
	"buttonwrapper": "Test-buttonwrapper-kmrpR",
	"requestDemo": "Test-requestDemo-EyPHF",
	"whatsappwrapper": "Test-whatsappwrapper-3DmTJ",
	"whatsapp": "Test-whatsapp-3-hIr",
	"downloadApp": "Test-downloadApp-h4dCI",
	"downloadText": "Test-downloadText-2KLuA",
	"playStoreIcon": "Test-playStoreIcon-3vbFK",
	"breadcrum": "Test-breadcrum-1j15E",
	"displayClients": "Test-displayClients-18jr5",
	"topSection": "Test-topSection-21bV3",
	"teachSection": "Test-teachSection-gnkFu",
	"teachImgBox": "Test-teachImgBox-32tPP",
	"teachSectionName": "Test-teachSectionName-1fAAr",
	"clientsWrapper": "Test-clientsWrapper-2fDeP",
	"client": "Test-client-FzdBZ",
	"achievedContainer": "Test-achievedContainer-1FrOF",
	"achievedHeading": "Test-achievedHeading-rtdeO",
	"achievedRow": "Test-achievedRow-1lCQf",
	"card": "Test-card-TZZcR",
	"achievedProfile": "Test-achievedProfile-247hX",
	"clients": "Test-clients-2p-cX",
	"students": "Test-students-3C7ru",
	"tests": "Test-tests-2gs17",
	"questions": "Test-questions-3vWWW",
	"highlight": "Test-highlight-RuaYQ",
	"questionsHighlight": "Test-questionsHighlight-2_VNQ",
	"testsHighlight": "Test-testsHighlight-2bD1f",
	"studentsHighlight": "Test-studentsHighlight-3p4cJ",
	"clientsHighlight": "Test-clientsHighlight-yClDo",
	"subText": "Test-subText-2Q8Rs",
	"section_contents": "Test-section_contents-2WASW",
	"section_container_reverse": "Test-section_container_reverse-1IKFo",
	"bottomCircle": "Test-bottomCircle-2SECO",
	"topCircle": "Test-topCircle-2B2HP",
	"onlineTests": "Test-onlineTests-2i4lu",
	"ownPapers": "Test-ownPapers-1Gy0r",
	"questionBank": "Test-questionBank-l-6mN",
	"videoContents": "Test-videoContents-pQqwC",
	"video_section": "Test-video_section-2C2PD",
	"content_title": "Test-content_title-1DOvX",
	"content_text": "Test-content_text-QKFHq",
	"hide": "Test-hide-3vPQU",
	"show": "Test-show-LTOHo",
	"mobilePapersContainer": "Test-mobilePapersContainer-1GPOu",
	"papersWrapper": "Test-papersWrapper-1aINd"
};

/***/ }),

/***/ "./src/components/Slider-Player/SliderPlayer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/react-player/lib/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/components/Slider-Player/SliderPlayer.scss");
/* harmony import */ var _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/components/Slider-Player/SliderPlayer.js";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






var move = {
  0: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut,
  1: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut1,
  2: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut2,
  3: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.slideOut3
};

var SliderPlayer = function SliderPlayer(props) {
  var getInnerWidth = function getInnerWidth() {
    var isWindowAvailable = typeof window !== 'undefined';

    if (isWindowAvailable) {
      return window.innerWidth;
    }

    return 1000;
  };

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      active = _useState2[0],
      setActive = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      seekValue = _useState4[0],
      setSeekValue = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isMobile = _useState6[0],
      setIsMobile = _useState6[1];

  var playerRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_toConsumableArray(props.videosList)),
      _useState8 = _slicedToArray(_useState7, 2),
      videos = _useState8[0],
      setVideos = _useState8[1]; // eslint-disable-line


  var videosList = props.videosList;
  var activeVideoObj = isMobile ? videos[active] : videosList[active];
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var resize = function resize() {
      setIsMobile(getInnerWidth() < 991);
    };

    resize();
    window.addEventListener('resize', resize);
    return function () {
      window.removeEventListener('resize', resize);
    };
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_2___default.a, {
    ref: playerRef,
    width: isMobile ? '100%' : '500px',
    height: isMobile ? '267px' : '400px',
    style: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.video,
    url: activeVideoObj.url,
    controls: false,
    config: {
      file: {
        attributes: {
          autoPlay: true,
          muted: true,
          disablePictureInPicture: true
        }
      }
    },
    onProgress: function onProgress(state) {
      var seek = Math.ceil(state.playedSeconds * 100 / playerRef.current.getDuration());
      setSeekValue(seek);
    },
    onEnded: function onEnded() {
      setSeekValue(100);

      if (isMobile) {
        var titles = document.getElementsByClassName(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle); // const lastOne = videos.shift();
        // videos.push(lastOne);

        setSeekValue(0);
        var prevActive = active;
        var currentActive = active + 1 === 4 ? 0 : active + 1;
        setActive(currentActive);
        Array.from(titles).forEach(function (ele) {
          ele.classList.add(move[currentActive]);
          ele.classList.remove(move[prevActive]);
        });
      } else {
        var isLast = active === videosList.length - 1;

        if (isLast) {
          setSeekValue(0);
          setActive(0);
        } else {
          setSeekValue(0);
          setActive(active + 1);
        }
      }
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.controlsWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: this
  }, isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileControls,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.outer, " ").concat(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileTracker),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.inner,
    style: {
      width: "".concat(seekValue, "%")
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.mobileTitlesWrapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: this
  }, videos.map(function (video, index) {
    var act = active === index;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: act ? "".concat(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle, " ").concat(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.actTitle) : "".concat(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, video.title);
  }))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.desktopControls,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: this
  }, videosList.map(function (videoObj, index) {
    var currentSeek = index === active ? seekValue : 0;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.controls,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, !isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.outer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.inner,
      style: {
        width: "".concat(currentSeek, "%")
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    })) : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: _SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a.videoTitle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135
      },
      __self: this
    }, videoObj.title));
  }))));
};

SliderPlayer.propTypes = {
  videosList: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string]))
};
SliderPlayer.defaultProps = {
  videosList: []
};
/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_SliderPlayer_scss__WEBPACK_IMPORTED_MODULE_4___default.a)(SliderPlayer));

/***/ }),

/***/ "./src/components/Slider-Player/SliderPlayer.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/components/Slider-Player/SliderPlayer.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/Test.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_Link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/components/Link/Link.js");
/* harmony import */ var components_Slider_Player_SliderPlayer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/components/Slider-Player/SliderPlayer.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./src/routes/products/get-ranks/Test/constants.js");
/* harmony import */ var _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./src/routes/products/get-ranks/GetRanksConstants.js");
/* harmony import */ var _Test_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./src/routes/products/get-ranks/Test/Test.scss");
/* harmony import */ var _Test_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Test_scss__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/Test.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var Test =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Test, _React$Component);

  // Toggle Button Code

  /* displayToggle = () => (
    <div
      role="presentation"
      className={
        this.state.liveToggle
          ? `${s.toggle_outer} ${s.toggle_on}`
          : s.toggle_outer
      }
    >
      <div className={s.toggle_inner} />
    </div>
  ); */
  function Test(props) {
    var _this;

    _classCallCheck(this, Test);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Test).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleResize", function () {
      if (window.innerWidth < 991) {
        _this.setState({
          isMobile: true
        });
      } else {
        _this.setState({
          isMobile: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleScroll", function () {
      if (window.scrollY > 500) {
        _this.setState({
          showScroll: true
        });
      } else {
        _this.setState({
          showScroll: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleScrollTop", function () {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });

      _this.setState({
        showScroll: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "displayTeachSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.breadcrum,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, "Home / ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, "Online Tests")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachImgBox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/New SubMenu Items/Test/test.svg",
        alt: "teach",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.teachSectionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }, "Online Tests"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.heading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, "Plan, design, deliver and mark\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.learningText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, "assessments"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/live classes/underscore_for_title.png",
        alt: "underscore",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      })), "\xA0seamlessly - onsite or remotely."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, "An end-to-end assessment platform that support teachers, data entry operators with cutting edge features that cover all your assessment needs entirely on-screen."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.buttonwrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.requestDemo,
        onClick: _this.handleShowTrial,
        role: "presentation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, "GET SUBSCRIPTION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.whatsappwrapper,
        href: "https://api.whatsapp.com/send?phone=918800764909&text=Hi GetRanks, I would like to know more about your Online Platform.",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.whatsapp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, "Chat on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/whatsapp_logo.svg",
        alt: "whatsapp",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.downloadApp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.downloadText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }, "Download the app"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.playStoreIcon,
        src: "/images/home/platforms/playStore.webp",
        alt: "play_store",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.headerbackgroundmobile,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/hero_img.webp",
        alt: "mobile_background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayClients", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.displayClients,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }, "Trusted by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }), "leading Educational Institutions"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clientsWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["HOME_CLIENTS_UPDATED"].map(function (client) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.client,
          key: client.id,
          src: client.icon,
          alt: "clinet",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 184
          },
          __self: this
        });
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
        to: "/customers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, "click here for more...")));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAchievedSoFar", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 199
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedHeading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 200
        },
        __self: this
      }, "What we have achieved ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      }), "so far"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(255, 102, 0, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clients),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/clients.svg",
        alt: "profile",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.clientsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, "150+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, "Clients")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(140, 0, 254, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.students),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 219
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/students.svg",
        alt: "students",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 220
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.studentsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 222
        },
        __self: this
      }, "3 Lakh+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 225
        },
        __self: this
      }, "Students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 115, 255, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 227
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.tests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/tests.svg",
        alt: "tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 232
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 234
        },
        __self: this
      }, "3 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 237
        },
        __self: this
      }, "Tests Attempted")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.card,
        style: {
          boxShadow: '0 4px 32px 0 rgba(0, 172, 38, 0.2)'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.achievedProfile, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questions),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/what-achieved/questions.svg",
        alt: "questions",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 244
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.highlight, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questionsHighlight),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 249
        },
        __self: this
      }, "20 Million+"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.subText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252
        },
        __self: this
      }, "Questions Solved"))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayPlayerSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoSlider,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 259
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Slider_Player_SliderPlayer__WEBPACK_IMPORTED_MODULE_3__["default"], {
        videosList: _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["TEST_VIDEOS"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 260
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maths,
        src: "/images/icons/subject_icons/maths.svg",
        alt: "maths",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 261
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.microscope,
        src: "/images/icons/subject_icons/microscope.svg",
        alt: "microscope",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.triangle,
        src: "/images/icons/subject_icons/triangle.svg",
        alt: "triangle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.scale,
        src: "/images/icons/subject_icons/scale.svg",
        alt: "scale",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 276
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.circle,
        src: "/images/icons/subject_icons/circle.svg",
        alt: "circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.chemistry,
        src: "/images/icons/subject_icons/chemistry.svg",
        alt: "chemistry",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 286
        },
        __self: this
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "displayOnlineTests", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 296
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 297
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 298
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 299
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/Tests_HeadSection.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 300
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 307
        },
        __self: this
      }, "Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_4__["onlineTestPoints"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 310
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 311
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 312
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 314
          },
          __self: this
        }, point.text));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 320
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 321
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 322
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/online-tests.webp",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 323
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 325
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 326
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayOwnPapers", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 334
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 335
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 336
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 337
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 338
        },
        __self: this
      }, "Upload your own Question papers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.papersWrapper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 341
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_4__["ownPapers"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 343
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 344
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 345
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 347
          },
          __self: this
        }, point.text));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ownPapers),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 353
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 354
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 355
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/own-papers.webp",
        alt: "own-papers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 356
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 358
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 359
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayQuestionBank", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 367
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 368
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 369
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 371
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/Tests_HeadSection.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 372
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 378
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 379
        },
        __self: this
      }, "Inbuilt Questions Bank "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 380
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_4__["questionBank"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 382
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 383
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 384
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 386
          },
          __self: this
        }, point.text));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.questionBank),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 392
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 393
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 394
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/questionBank.webp",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 395
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 397
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 398
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayPapers", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 406
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 407
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 408
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 409
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 410
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/Analysis_HeadSection.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 411
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 417
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 418
        },
        __self: this
      }, "Papers"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 419
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_4__["papers"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 421
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 422
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 423
          },
          __self: this
        })), point.subPoints ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 426
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 427
          },
          __self: this
        }, point.text), point.subPoints.map(function (text) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 429
            },
            __self: this
          }, text);
        })) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 433
          },
          __self: this
        }, point.text));
      })))), !_this.state.isMobile ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 441
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 442
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 443
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/analysis-card.svg",
        alt: "papers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 444
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 446
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 447
        },
        __self: this
      }))) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.mobilePapersContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 451
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/mobile_papers.svg",
        alt: "moble_papers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 452
        },
        __self: this
      }))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAnalysis", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container_reverse,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 460
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 461
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 462
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 463
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 464
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/Analysis_HeadSection.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 465
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 471
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 472
        },
        __self: this
      }, "Analysis"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 473
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, "Marks-based Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["marksBasedAnalysis"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 476
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 477
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 478
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 480
          },
          __self: this
        }, point.text));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 483
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126, " ").concat(_this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 484
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 489
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/marksAnalysis.webp",
        alt: "marks-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 490
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 495
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 499
        },
        __self: this
      }, "Comparision Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["comparisionAnalysis"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 501
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 502
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 503
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 505
          },
          __self: this
        }, point.text));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 508
        },
        __self: this
      }, "Error Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["errorAnalysis"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 510
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 511
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 512
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 514
          },
          __self: this
        }, point.text));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 517
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126, " ").concat(_this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 518
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 523
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/conceptAnalysis.webp",
        alt: "concept-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 524
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 529
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 530
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.sub_section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 533
        },
        __self: this
      }, "Concept-based Analysis"), _constants__WEBPACK_IMPORTED_MODULE_4__["conceptAnalysis"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 535
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 536
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 537
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 539
          },
          __self: this
        }, point.text));
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 542
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126, " ").concat(_this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 543
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 548
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/errorAnalysis.webp",
        alt: "error-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 549
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 554
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 555
        },
        __self: this
      })))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests, " ").concat(_this.state.isMobile ? _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hide : _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.show),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 561
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 566
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 567
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/marksAnalysis.webp",
        alt: "marks-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 568
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 570
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 571
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.marginBottom126),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 573
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 574
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/conceptAnalysis.webp",
        alt: "concept-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 575
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 580
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 581
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 583
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 584
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/errorAnalysis.webp",
        alt: "error-analysis",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 585
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 587
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 588
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayReports", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 596
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 597
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 598
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 599
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.testsImage,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 600
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/Reports_HeadSection.svg",
        alt: "online-tests",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 601
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 607
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 608
        },
        __self: this
      }, "Reports"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.reports_sub_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 609
        },
        __self: this
      }, "9 Informative Reports"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.reports_notice,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 610
        },
        __self: this
      }, "(Downloadable for Notice Board)"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 613
        },
        __self: this
      }, _constants__WEBPACK_IMPORTED_MODULE_4__["reports"].map(function (point) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row1, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.point),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 615
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 616
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.redDot,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 617
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pointText,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 619
          },
          __self: this
        }, point.text));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.onlineTests),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 625
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 626
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 627
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/reports.webp",
        alt: "reports",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 628
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.bottomCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 630
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.topCircle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 631
        },
        __self: this
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayTestsSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 639
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 640
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 641
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 642
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 643
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 646
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " custom-scrollbar"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 654
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 655
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/demo.png",
        alt: "demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 656
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 658
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 659
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 660
        },
        __self: this
      }, "Independence"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 661
        },
        __self: this
      }, "Your Zoom account will be used. All the features and provisions of your Zoom account shall be available with an added advantage of login restrictions only for selected students.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 667
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 668
        },
        __self: this
      }, "Flexibility"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 669
        },
        __self: this
      }, "Any number of Zoom accounts can be used. Different Zoom accounts can be mapped with different Class Sessions thus allowing multiple teachers to use their personal accounts.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 675
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 676
        },
        __self: this
      }, "Secure Access"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 677
        },
        __self: this
      }, "Only selected students who have access to the App can now access Zoom class, no need to share Meeting ID everytime, No random access.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 683
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 684
        },
        __self: this
      }, "Multiple uses of Zoom"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 685
        },
        __self: this
      }, "Zoom facility available for live Class and Doubts modules. Soon Zoom is being integrated to the online Exam module for online Exam proctoring."))))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAnalysisSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ashBackground),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 699
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 700
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 701
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 702
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 703
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 706
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " custom-scrollbar"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 714
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 715
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/demo.png",
        alt: "demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 716
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 718
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 719
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 720
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 723
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 730
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 731
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 734
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 741
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 742
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 745
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 752
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 753
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 756
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayPractice", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 770
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 771
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 772
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 773
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 774
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 777
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " custom-scrollbar"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 796
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 797
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/demo.png",
        alt: "demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 798
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 800
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 801
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 802
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 805
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 812
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 813
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 816
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 823
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 824
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 827
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 834
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 835
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 838
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayPapersSection", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_container, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.ashBackground),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 851
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_contents, " ").concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.maxContainer),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 852
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.contentPart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 853
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 854
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.section_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 855
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.textcontent,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 858
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.imagePart, " custom-scrollbar"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 877
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.emptyCard,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 878
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/Test/demo.png",
        alt: "demo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 879
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.videoContents,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 881
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 882
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 883
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 886
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 893
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 894
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 897
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 904
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 905
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 908
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.video_section,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 915
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_title,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 916
        },
        __self: this
      }, "Conduct Offline/Online Tests"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.content_text,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 919
        },
        __self: this
      }, "Getranks comes with the capability where just like the actual JEE exam, the test can be attempted simultaneously in both online and offline modes and the analysis is generated as a whole for all the students"))))));
    });

    _defineProperty(_assertThisInitialized(_this), "displayAvailableOn", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableContainer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 932
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableRow,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 933
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableContentSection,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 934
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.availableTitle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 935
        },
        __self: this
      }, "We\u2019re ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 936
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.available,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 937
        },
        __self: this
      }, "available"), " on"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.row,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 939
        },
        __self: this
      }, _GetRanksConstants__WEBPACK_IMPORTED_MODULE_5__["PLATFORMS"].map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platformContainer,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 941
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: item.icon,
          alt: "",
          height: "40px",
          width: "40px",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 942
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platform,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 943
          },
          __self: this
        }, item.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.platformOs,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 944
          },
          __self: this
        }, item.available));
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.store,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 948
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://play.google.com/store/apps/details?id=com.egnify.getranks&hl=en_IN&gl=US",
        target: "_blank",
        rel: "noopener noreferrer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 949
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/playStore.webp",
        alt: "Play Store",
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.playstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 954
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/platforms/appStore.webp",
        alt: "App Store",
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.appstore,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 960
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.desktopImage,
        src: "/images/home/platforms/platforms_v2.webp",
        alt: "platform",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 968
        },
        __self: this
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "displayJoinFacebook", function () {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinCommunity,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 978
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinFbText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 979
        },
        __self: this
      }, "Join our Facebook Community"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.joinFbLink,
        href: "https://www.facebook.com/Egnify/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 980
        },
        __self: this
      }, "Join Now"));
    });

    _defineProperty(_assertThisInitialized(_this), "displayScrollToTop", function () {
      var showScroll = _this.state.showScroll;
      return showScroll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: _Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a.scrollTop,
        role: "presentation",
        onClick: function onClick() {
          _this.handleScrollTop();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 990
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/images/home/scrollTop.svg",
        alt: "scrollTop",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 997
        },
        __self: this
      }));
    });

    _this.state = {
      showScroll: false,
      isMobile: false
    };
    return _this;
  }

  _createClass(Test, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleScroll();
      this.handleResize();
      window.addEventListener('scroll', this.handleScroll);
      window.addEventListener('resize', this.handleResize);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
      window.removeEventListener('resize', this.handleResize);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 1005
        },
        __self: this
      }, this.displayTeachSection(), this.displayClients(), this.displayAchievedSoFar(), this.displayOnlineTests(), this.displayOwnPapers(), this.displayQuestionBank(), this.displayPapers(), this.displayAnalysis(), this.displayReports(), this.displayAvailableOn(), this.displayJoinFacebook(), this.displayScrollToTop());
    }
  }]);

  return Test;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_1___default()(_Test_scss__WEBPACK_IMPORTED_MODULE_6___default.a)(Test));

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/Test.scss":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/products/get-ranks/Test/Test.scss");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/constants.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onlineTestPoints", function() { return onlineTestPoints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ownPapers", function() { return ownPapers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "questionBank", function() { return questionBank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "marksBasedAnalysis", function() { return marksBasedAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "comparisionAnalysis", function() { return comparisionAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorAnalysis", function() { return errorAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conceptAnalysis", function() { return conceptAnalysis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "papers", function() { return papers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reports", function() { return reports; });
var onlineTestPoints = [{
  text: 'Unlimitted test creation'
}, {
  text: 'Instant test result and Reports available online & offline - Rank list, Analysis, and Reports after every test.'
}, {
  text: 'Pre-defined marking schemes'
}, {
  text: 'Custom marking schemes'
}, {
  text: 'Getranks supports all 22 types of JEE avanced Marking Schemes starting from 2009 - 2019'
}, {
  text: 'Grace period option available'
}, {
  text: 'Live Attendance'
}];
var ownPapers = [{
  text: 'Upload your question paper online from word document within 5 mins with a single click'
}, {
  text: 'Getranks Parser can convert text / images / equations / graphs into your questions.'
}];
var questionBank = [{
  text: 'Question bank management system with 1.5 Lakh Questions organized by topic and difficulty for JEE, NEET, EAMCET and IPE.'
}, {
  text: 'All Types of Questions as per latest pattern'
}, {
  text: 'Questions are provided with Answer Key and Step by Step Detail Solution.'
}];
var marksBasedAnalysis = [{
  text: 'Marks distribution graph'
}, {
  text: 'Cut-off based analysis'
}, {
  text: 'Student-wise marks analysis'
}];
var comparisionAnalysis = [{
  text: 'Historic test averages'
}, {
  text: 'Student performance profile'
}, {
  text: 'Subject top marks vs topper'
}];
var errorAnalysis = [{
  text: 'Question error count'
}, {
  text: 'Students error mapping'
}, {
  text: 'Question paper referencing'
}];
var conceptAnalysis = [{
  text: 'Topic based analysis'
}, {
  text: 'Sub-topic based analysis'
}, {
  text: 'Difficulty level analysis'
}];
var papers = [{
  text: 'More than 1,50,000+ ready to use Comprehensive Question Bank and each question has been set with appropriate Difficulty Level.'
}, {
  text: 'All Types of Questions as per latest pattern:',
  subPoints: ['a)	Single correct, Multiple correct questions, Integer type, Matrix type,', 'b)	Assertion Reasoning and Paragraph Type']
}, {
  text: 'Design personalized question paper/s with Institute Name, Logo & Watermark'
}, {
  text: 'Questions are provided with Answer Key and Step by Step Detail Solution.'
}];
var reports = [{
  text: 'Test results'
}, {
  text: 'Student response report'
}, {
  text: 'Student response count'
}, {
  text: 'Marks distribution'
}, {
  text: 'Error count'
}, {
  text: 'Top marks'
}, {
  text: 'Student performance trend'
}, {
  text: 'Averages comparison'
}, {
  text: 'Concept based report'
}];

/***/ }),

/***/ "./src/routes/products/get-ranks/Test/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/components/Layout/Layout.js");
/* harmony import */ var _Test__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./src/routes/products/get-ranks/Test/Test.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/products/get-ranks/Test/index.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function action() {
  return _action.apply(this, arguments);
}

function _action() {
  _action = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", {
              title: 'Egnify teaching made easy',
              chunks: ['Tests'],
              component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
                footerAsh: true,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 10
                },
                __self: this
              }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Test__WEBPACK_IMPORTED_MODULE_2__["default"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 11
                },
                __self: this
              }))
            });

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _action.apply(this, arguments);
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGVzdHMuY2h1bmsuanMiLCJzb3VyY2VzIjpbIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY29tcG9uZW50cy9TbGlkZXItUGxheWVyL1NsaWRlclBsYXllci5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvcHJvZHVjdHMvZ2V0LXJhbmtzL1Rlc3QvVGVzdC5zY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL1NsaWRlci1QbGF5ZXIvU2xpZGVyUGxheWVyLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL1NsaWRlci1QbGF5ZXIvU2xpZGVyUGxheWVyLnNjc3M/YzU5MiIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZXN0L1Rlc3QuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVzdC9UZXN0LnNjc3M/ZWM5MyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZXN0L2NvbnN0YW50cy5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL3Byb2R1Y3RzL2dldC1yYW5rcy9UZXN0L2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuU2xpZGVyUGxheWVyLXJvb3QtMVBKNXgge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi5TbGlkZXJQbGF5ZXItdmlkZW8tMzlENG8ge1xcbiAgd2lkdGg6IDQwMHB4O1xcbiAgaGVpZ2h0OiA0MDBweDtcXG59XFxuXFxuLlNsaWRlclBsYXllci1kZXNrdG9wQ29udHJvbHMtMzZOSjIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG5cXG4uU2xpZGVyUGxheWVyLWNvbnRyb2xzV3JhcHBlci1sWE9IYSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW4tdG9wOiA5NnB4O1xcbn1cXG5cXG4uU2xpZGVyUGxheWVyLWNvbnRyb2xzLTFfdUlqIHtcXG4gIHdpZHRoOiAyNDBweDtcXG4gIG1hcmdpbjogMCAxNnB4O1xcbiAgbWFyZ2luOiAwIDFyZW07XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbn1cXG5cXG4uU2xpZGVyUGxheWVyLW91dGVyLVU5VHVkIHtcXG4gIHdpZHRoOiAyNDBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNkOGQ4ZDg7XFxuICBoZWlnaHQ6IDRweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcXG59XFxuXFxuLlNsaWRlclBsYXllci1pbm5lci0zQVhzZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBoZWlnaHQ6IDRweDtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICB6LWluZGV4OiAxO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMXMgZWFzZTtcXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjFzIGVhc2U7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlO1xcbn1cXG5cXG4uU2xpZGVyUGxheWVyLXZpZGVvVGl0bGUtMjJYeEQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLlNsaWRlclBsYXllci12aWRlby0zOUQ0byB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNDAwcHg7XFxuICAgIG1heC1oZWlnaHQ6IDMwMHB4O1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci1tb2JpbGVUaXRsZXNXcmFwcGVyLTF5cmFuIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDMzMHB4O1xcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLW1vYmlsZUNvbnRyb2xzLTFoNm83IHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLXZpZGVvVGl0bGUtMjJYeEQge1xcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XFxuICAgIG1pbi13aWR0aDogMTAwcHg7XFxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gMXMgZWFzZTtcXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gMXMgZWFzZTtcXG4gICAgLW8tdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXMgZWFzZSwgLXdlYmtpdC10cmFuc2Zvcm0gMXMgZWFzZTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RC5TbGlkZXJQbGF5ZXItYWN0VGl0bGUtMzM1MHkge1xcbiAgICBmb250LXdlaWdodDogYm9sZDtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RC5TbGlkZXJQbGF5ZXItc2xpZGVPdXQtemZZMGgge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci12aWRlb1RpdGxlLTIyWHhELlNsaWRlclBsYXllci1zbGlkZU91dDEtMmlDdjAge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci12aWRlb1RpdGxlLTIyWHhELlNsaWRlclBsYXllci1zbGlkZU91dDItNG56Nmkge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMjEwJSk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0yMTAlKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTIxMCUpO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci12aWRlb1RpdGxlLTIyWHhELlNsaWRlclBsYXllci1zbGlkZU91dDMtMUNfZm8ge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMzIwJSk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0zMjAlKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTMyMCUpO1xcbiAgfVxcblxcbiAgLlNsaWRlclBsYXllci1jb250cm9sc1dyYXBwZXItbFhPSGEge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiAzMzBweDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBwYWRkaW5nOiAwO1xcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLWNvbnRyb2xzLTFfdUlqIHtcXG4gICAgd2lkdGg6IDIwMHB4O1xcbiAgICBtYXJnaW46IDAgMTJweCAwIDA7XFxuICB9XFxuXFxuICAuU2xpZGVyUGxheWVyLWRlc2t0b3BDb250cm9scy0zNk5KMiB7XFxuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItb3V0ZXItVTlUdWQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5TbGlkZXJQbGF5ZXItb3V0ZXItVTlUdWQuU2xpZGVyUGxheWVyLW1vYmlsZVRyYWNrZXItbUlMYWIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL1NsaWRlci1QbGF5ZXIvU2xpZGVyUGxheWVyLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2YsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixxQkFBcUI7TUFDakIsNEJBQTRCO0NBQ2pDOztBQUVEO0VBQ0UsYUFBYTtFQUNiLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLE9BQU87RUFDUCxRQUFRO0VBQ1IsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixrQ0FBa0M7RUFDbEMsNkJBQTZCO0VBQzdCLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsOENBQThDO0lBQzlDLHNDQUFzQztJQUN0QyxpQ0FBaUM7SUFDakMsOEJBQThCO0lBQzlCLHlEQUF5RDtHQUMxRDs7RUFFRDtJQUNFLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGlDQUFpQztRQUM3Qiw2QkFBNkI7WUFDekIseUJBQXlCO0dBQ2xDOztFQUVEO0lBQ0UscUNBQXFDO1FBQ2pDLGlDQUFpQztZQUM3Qiw2QkFBNkI7R0FDdEM7O0VBRUQ7SUFDRSxxQ0FBcUM7UUFDakMsaUNBQWlDO1lBQzdCLDZCQUE2QjtHQUN0Qzs7RUFFRDtJQUNFLHFDQUFxQztRQUNqQyxpQ0FBaUM7WUFDN0IsNkJBQTZCO0dBQ3RDOztFQUVEO0lBQ0UsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsYUFBYTtJQUNiLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0dBQ2Q7Q0FDRlwiLFwiZmlsZVwiOlwiU2xpZGVyUGxheWVyLnNjc3NcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLnJvb3Qge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi52aWRlbyB7XFxuICB3aWR0aDogNDAwcHg7XFxuICBoZWlnaHQ6IDQwMHB4O1xcbn1cXG5cXG4uZGVza3RvcENvbnRyb2xzIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuXFxuLmNvbnRyb2xzV3JhcHBlciB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW4tdG9wOiA5NnB4O1xcbn1cXG5cXG4uY29udHJvbHMge1xcbiAgd2lkdGg6IDI0MHB4O1xcbiAgbWFyZ2luOiAwIDE2cHg7XFxuICBtYXJnaW46IDAgMXJlbTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxufVxcblxcbi5vdXRlciB7XFxuICB3aWR0aDogMjQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDhkOGQ4O1xcbiAgaGVpZ2h0OiA0cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxufVxcblxcbi5pbm5lciB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBoZWlnaHQ6IDRweDtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICB6LWluZGV4OiAxO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMXMgZWFzZTtcXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjFzIGVhc2U7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlO1xcbn1cXG5cXG4udmlkZW9UaXRsZSB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MHB4KSB7XFxuICAudmlkZW8ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xcbiAgICBtYXgtaGVpZ2h0OiAzMDBweDtcXG4gIH1cXG5cXG4gIC5tb2JpbGVUaXRsZXNXcmFwcGVyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDMzMHB4O1xcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICB9XFxuXFxuICAubW9iaWxlQ29udHJvbHMge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC52aWRlb1RpdGxlIHtcXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gICAgZm9udC1zaXplOiAxMnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xcbiAgICBtaW4td2lkdGg6IDEwMHB4O1xcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICAgIC1vLXRyYW5zaXRpb246IHRyYW5zZm9ybSAxcyBlYXNlO1xcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXMgZWFzZTtcXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzIGVhc2UsIC13ZWJraXQtdHJhbnNmb3JtIDFzIGVhc2U7XFxuICB9XFxuXFxuICAudmlkZW9UaXRsZS5hY3RUaXRsZSB7XFxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgfVxcblxcbiAgLnZpZGVvVGl0bGUuc2xpZGVPdXQge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xcbiAgfVxcblxcbiAgLnZpZGVvVGl0bGUuc2xpZGVPdXQxIHtcXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XFxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcXG4gIH1cXG5cXG4gIC52aWRlb1RpdGxlLnNsaWRlT3V0MiB7XFxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0yMTAlKTtcXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTIxMCUpO1xcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMjEwJSk7XFxuICB9XFxuXFxuICAudmlkZW9UaXRsZS5zbGlkZU91dDMge1xcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMzIwJSk7XFxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0zMjAlKTtcXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTMyMCUpO1xcbiAgfVxcblxcbiAgLmNvbnRyb2xzV3JhcHBlciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDMzMHB4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcXG4gICAgbWFyZ2luLXRvcDogMzJweDtcXG4gIH1cXG5cXG4gIC5jb250cm9scyB7XFxuICAgIHdpZHRoOiAyMDBweDtcXG4gICAgbWFyZ2luOiAwIDEycHggMCAwO1xcbiAgfVxcblxcbiAgLmRlc2t0b3BDb250cm9scyB7XFxuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcXG4gIH1cXG5cXG4gIC5vdXRlciB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLm91dGVyLm1vYmlsZVRyYWNrZXIge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwicm9vdFwiOiBcIlNsaWRlclBsYXllci1yb290LTFQSjV4XCIsXG5cdFwidmlkZW9cIjogXCJTbGlkZXJQbGF5ZXItdmlkZW8tMzlENG9cIixcblx0XCJkZXNrdG9wQ29udHJvbHNcIjogXCJTbGlkZXJQbGF5ZXItZGVza3RvcENvbnRyb2xzLTM2TkoyXCIsXG5cdFwiY29udHJvbHNXcmFwcGVyXCI6IFwiU2xpZGVyUGxheWVyLWNvbnRyb2xzV3JhcHBlci1sWE9IYVwiLFxuXHRcImNvbnRyb2xzXCI6IFwiU2xpZGVyUGxheWVyLWNvbnRyb2xzLTFfdUlqXCIsXG5cdFwib3V0ZXJcIjogXCJTbGlkZXJQbGF5ZXItb3V0ZXItVTlUdWRcIixcblx0XCJpbm5lclwiOiBcIlNsaWRlclBsYXllci1pbm5lci0zQVhzZVwiLFxuXHRcInZpZGVvVGl0bGVcIjogXCJTbGlkZXJQbGF5ZXItdmlkZW9UaXRsZS0yMlh4RFwiLFxuXHRcIm1vYmlsZVRpdGxlc1dyYXBwZXJcIjogXCJTbGlkZXJQbGF5ZXItbW9iaWxlVGl0bGVzV3JhcHBlci0xeXJhblwiLFxuXHRcIm1vYmlsZUNvbnRyb2xzXCI6IFwiU2xpZGVyUGxheWVyLW1vYmlsZUNvbnRyb2xzLTFoNm83XCIsXG5cdFwiYWN0VGl0bGVcIjogXCJTbGlkZXJQbGF5ZXItYWN0VGl0bGUtMzM1MHlcIixcblx0XCJzbGlkZU91dFwiOiBcIlNsaWRlclBsYXllci1zbGlkZU91dC16ZlkwaFwiLFxuXHRcInNsaWRlT3V0MVwiOiBcIlNsaWRlclBsYXllci1zbGlkZU91dDEtMmlDdjBcIixcblx0XCJzbGlkZU91dDJcIjogXCJTbGlkZXJQbGF5ZXItc2xpZGVPdXQyLTRuejZpXCIsXG5cdFwic2xpZGVPdXQzXCI6IFwiU2xpZGVyUGxheWVyLXNsaWRlT3V0My0xQ19mb1wiLFxuXHRcIm1vYmlsZVRyYWNrZXJcIjogXCJTbGlkZXJQbGF5ZXItbW9iaWxlVHJhY2tlci1tSUxhYlwiXG59OyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuVGVzdC1tYXhDb250YWluZXItMV9RQ08ge1xcbiAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLlRlc3QtbWFyZ2luQm90dG9tMTI2LTJ5LVRKIHtcXG4gIG1hcmdpbi1ib3R0b206IDEyNnB4O1xcbn1cXG5cXG4uVGVzdC1yZXBvcnRzX3N1Yl90aXRsZS0xelU0ViB7XFxuICBmb250LXNpemU6IDI4cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tdG9wOiAxMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5UZXN0LXJlcG9ydHNfbm90aWNlLTFrN3hZIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tdG9wOiA0cHg7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbn1cXG5cXG4uVGVzdC1yb3cxLTFwQklaIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxufVxcblxcbi5UZXN0LXBvaW50Q29udGFpbmVyLXlsNnJVIHtcXG4gIHdpZHRoOiA1JTtcXG59XFxuXFxuLlRlc3QtcG9pbnQtMnlITUIge1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLlRlc3QtcG9pbnRUZXh0LTNvbXk5IHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbn1cXG5cXG4uVGVzdC1yZWREb3QtM21LSC0ge1xcbiAgd2lkdGg6IDhweDtcXG4gIGhlaWdodDogOHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gIG1hcmdpbi1yaWdodDogMjBweDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxufVxcblxcbi5UZXN0LXRlc3RzSW1hZ2UtM1oyZGUge1xcbiAgd2lkdGg6IDcycHg7XFxuICBoZWlnaHQ6IDcycHg7XFxuICBtYXJnaW4tdG9wOiAtOHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbn1cXG5cXG4uVGVzdC10ZXN0c0ltYWdlLTNaMmRlIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG59XFxuXFxuLlRlc3Qtc2VjdGlvbl9jb250YWluZXItMndBNW0uVGVzdC1hc2hCYWNrZ3JvdW5kLXExRS0zIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5UZXN0LXZpZGVvU2xpZGVyLTI4X3VnIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgcGFkZGluZzogODBweCA2NHB4O1xcbiAgcGFkZGluZzogNXJlbSA0cmVtO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLlRlc3QtbWF0aHMtVHVjRVUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMTAlO1xcbiAgdG9wOiA1JTtcXG59XFxuXFxuLlRlc3QtbWljcm9zY29wZS1Ic2F0RyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDMwJTtcXG4gIGxlZnQ6IDMlO1xcbn1cXG5cXG4uVGVzdC10cmlhbmdsZS0yQlVLWSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDYwJTtcXG4gIGxlZnQ6IDEwJTtcXG59XFxuXFxuLlRlc3Qtc2NhbGUtMWM5STQge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDEwJTtcXG4gIHRvcDogNSU7XFxufVxcblxcbi5UZXN0LWNpcmNsZS0yalcxdyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDMwJTtcXG4gIHJpZ2h0OiAzJTtcXG59XFxuXFxuLlRlc3QtY2hlbWlzdHJ5LTJlWU9XIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogNjAlO1xcbiAgcmlnaHQ6IDEwJTtcXG59XFxuXFxuLlRlc3QtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0zRk4tNiB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IC01NnB4O1xcbiAgcmlnaHQ6IDA7XFxuICB3aWR0aDogNTEzcHg7XFxuICBoZWlnaHQ6IDU2MHB4O1xcbn1cXG5cXG4uVGVzdC1oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlLTNGTi02IGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICB9XFxuXFxuLlRlc3Qtc2VjdGlvbl90aXRsZS0xdTZ0MCB7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG1hcmdpbjogMCAwIDE2cHg7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLlRlc3Qtc3ViX3NlY3Rpb25fdGl0bGUtM1ZSdTMge1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgbWFyZ2luLXRvcDogNTZweDtcXG59XFxuXFxuLlRlc3QtY29udGVudFBhcnQtdk94WW0ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHdpZHRoOiA0NSU7XFxufVxcblxcbi5UZXN0LWNvbnRlbnRQYXJ0LXZPeFltIC5UZXN0LWNvbnRlbnQtMmhzOXMge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWluLXdpZHRoOiAyOTBweDtcXG4gICAgbWF4LXdpZHRoOiA1NDBweDtcXG4gIH1cXG5cXG4uVGVzdC1jb250ZW50UGFydC12T3hZbSAuVGVzdC1jb250ZW50LTJoczlzIC5UZXN0LXRleHRjb250ZW50LTNyV2YxIHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgY29sb3I6ICMyNTI4MmI7XFxuXFxuICAgICAgLyogLnBvaW50IHtcXG4gICAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgaGVpZ2h0OiBtYXgtY29udGVudDtcXG4gICAgICAgIG1hcmdpbjogNnB4IDA7XFxuXFxuICAgICAgICAucmVkZG90d3JhcHBlciB7XFxuICAgICAgICAgIHdpZHRoOiA1JTtcXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xcblxcbiAgICAgICAgICAucmVkZG90IHtcXG4gICAgICAgICAgICB3aWR0aDogOHB4O1xcbiAgICAgICAgICAgIGhlaWdodDogOHB4O1xcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgICB9XFxuICAgICAgICB9XFxuXFxuICAgICAgICAucG9pbnRUZXh0IHtcXG4gICAgICAgICAgd2lkdGg6IDk1JTtcXG4gICAgICAgIH1cXG4gICAgICB9ICovXFxuICAgIH1cXG5cXG4vKiAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgY29sb3I6ICNmZmY7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgaGVpZ2h0OiA1NjBweDtcXG5cXG4gIC5jdXN0b21lcl9yZXZpZXcge1xcbiAgICBvcmRlcjogMDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgIHBhZGRpbmc6IDQ4cHggNjRweDtcXG4gICAgd2lkdGg6IDUwJTtcXG5cXG4gICAgLmN1c3RvbWVyTG9nbyB7XFxuICAgICAgd2lkdGg6IDEyMHB4O1xcbiAgICAgIGhlaWdodDogODBweDtcXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG5cXG4gICAgICBpbWcge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBoZWlnaHQ6IDEwMCU7XFxuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG4gICAgfVxcblxcbiAgICAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgZm9udC1zaXplOiAyMHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICBtYXgtd2lkdGg6IDYwMHB4O1xcbiAgICB9XFxuXFxuICAgIC5hdXRob3JXcmFwcGVyIHtcXG4gICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuXFxuICAgICAgLmF1dGhvcl90aXRsZSB7XFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgICBjb2xvcjogI2ZmZjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmFib3V0X2F1dGhvciB7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDY0cHg7XFxuICAgICAgfVxcbiAgICB9XFxuXFxuICAgIC5hbGxjdXN0b21lcnMge1xcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcXG4gICAgICBmb250LXdlaWdodDogNjAwO1xcbiAgICAgIGNvbG9yOiAjZmZmO1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcblxcbiAgICAgIGltZyB7XFxuICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XFxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xcbiAgICAgICAgd2lkdGg6IDIwcHg7XFxuICAgICAgICBoZWlnaHQ6IDIwcHg7XFxuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgIH1cXG4gICAgfVxcbiAgfVxcbn0gKi9cXG5cXG4uVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIge1xcbiAgcGFkZGluZzogODBweCAxMTNweCA1NnB4IDE2NHB4O1xcbiAgcGFkZGluZzogNXJlbSAxMTNweCA1NnB4IDE2NHB4O1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMge1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gICAgZGlzcGxheTogZ3JpZDtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbi5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3QtZGVza3RvcEltYWdlLTJrWE5YIHtcXG4gICAgICB3aWR0aDogNjk2cHg7XFxuICAgICAgaGVpZ2h0OiAzNzFweDtcXG4gICAgfVxcblxcbi5UZXN0LWpvaW5Db21tdW5pdHktM0xCVXMge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDE2MHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgYm90dG9tLCBsZWZ0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LWpvaW5GYlRleHQtMWZkaE0ge1xcbiAgZm9udC1zaXplOiAzMnB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBmb250LXdlaWdodDogNjAwO1xcbiAgbWFyZ2luLXJpZ2h0OiA2NHB4O1xcbiAgY29sb3I6ICNmZmY7XFxufVxcblxcbi5UZXN0LWpvaW5GYkxpbmstQXZjc2Yge1xcbiAgcGFkZGluZzogMTZweCA0MHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5UZXN0LWF2YWlsYWJsZVRpdGxlLTNORWZBIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5UZXN0LWF2YWlsYWJsZS0zbFdMWCB7XFxuICBjb2xvcjogI2YzNjtcXG59XFxuXFxuLlRlc3QtYXZhaWxhYmxlQ29udGVudFNlY3Rpb24tVVA0bG4ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG59XFxuXFxuLlRlc3QtcGxhdGZvcm1Db250YWluZXItMk9obnQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIG1hcmdpbi1yaWdodDogMnJlbTtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG59XFxuXFxuLlRlc3QtcGxhdGZvcm0ta094QU0ge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbjogOHB4IDAgNHB4IDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLlRlc3QtcGxhdGZvcm1Pcy0zNlBrRyB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi5UZXN0LXJvdy1zRS1DbyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIG1hcmdpbjogNDhweCAwIDMycHggMDtcXG4gIG1hcmdpbjogM3JlbSAwIDJyZW0gMDtcXG59XFxuXFxuLlRlc3Qtc3RvcmUtM0N6aXoge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICBtYXJnaW4tYm90dG9tOiA2NHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogNHJlbTtcXG59XFxuXFxuLlRlc3Qtc3RvcmUtM0N6aXogLlRlc3QtcGxheXN0b3JlLTNQS09DIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIH1cXG5cXG4uVGVzdC1zdG9yZS0zQ3ppeiAuVGVzdC1hcHBzdG9yZS0xaE8xNiB7XFxuICAgIHdpZHRoOiAxMzZweDtcXG4gICAgaGVpZ2h0OiA0MHB4O1xcbiAgfVxcblxcbi5UZXN0LXNjcm9sbFRvcC0xNFM4QyB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLlRlc3Qtc2Nyb2xsVG9wLTE0UzhDIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICB9XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28ge1xcbiAgbWluLWhlaWdodDogNzIwcHg7XFxuICBwYWRkaW5nOiAxNnB4IDY0cHggNTZweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWhlYWRpbmctMW9OczUge1xcbiAgZm9udC1zaXplOiA0OHB4O1xcbiAgbGluZS1oZWlnaHQ6IDY2cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1heC13aWR0aDogNzQwcHg7XFxuICBtYXJnaW46IDEycHggMCAwIDA7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWhlYWRpbmctMW9OczUgLlRlc3QtbGVhcm5pbmdUZXh0LTFQQlgzIHtcXG4gIHdpZHRoOiAzMTZweDtcXG4gIGhlaWdodDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIGhlaWdodDogLW1vei1maXQtY29udGVudDtcXG4gIGhlaWdodDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC1lbXB0eUNhcmQtMk14SEEgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLy8gb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXRvcENhcmQtM0FfTm8gaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLW8tb2JqZWN0LWZpdDogY29udGFpbjtcXG4gICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWhlYWRpbmctMW9OczUgLlRlc3QtbGVhcm5pbmdUZXh0LTFQQlgzIGltZyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBib3R0b206IC0ycHg7XFxuICBsZWZ0OiAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDhweDtcXG4gIG1heC13aWR0aDogMzA4cHg7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWNvbnRlbnRUZXh0LTJIcDZ5IHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgbWFyZ2luOiAxMnB4IDAgMCAwO1xcbiAgbWF4LXdpZHRoOiA2ODdweDtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiB7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3QtcmVxdWVzdERlbW8tRXlQSEYge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1idXR0b253cmFwcGVyLWttcnBSIC5UZXN0LXdoYXRzYXBwd3JhcHBlci0zRG1USiB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1idXR0b253cmFwcGVyLWttcnBSIC5UZXN0LXdoYXRzYXBwd3JhcHBlci0zRG1USiAuVGVzdC13aGF0c2FwcC0zLWhJciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XFxuICBjb2xvcjogIzI1MjgyYiAhaW1wb3J0YW50O1xcbiAgbWFyZ2luOiAwIDhweCAwIDMycHg7XFxuICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiAuVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEogaW1nIHtcXG4gIHdpZHRoOiAzMnB4O1xcbiAgaGVpZ2h0OiAzMnB4O1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1kb3dubG9hZEFwcC1oNGRDSSB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWRvd25sb2FkQXBwLWg0ZENJIC5UZXN0LWRvd25sb2FkVGV4dC0yS0x1QSB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtZG93bmxvYWRBcHAtaDRkQ0kgLlRlc3QtcGxheVN0b3JlSWNvbi0zdmJGSyB7XFxuICB3aWR0aDogMTMycHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi8qIC50ZWFjaENvbnRhaW5lciAuYWN0aW9uc1dyYXBwZXIge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAyNnB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24ge1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24gLmFjdGlvbmltZ2JveCB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24gLmFjdGlvbmltZ2JveCBpbWcge1xcbiAgd2lkdGg6IDEwcHg7XFxuICBoZWlnaHQ6IDlweDtcXG59ICovXFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnJlYWRjcnVtLTFqMTVFIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbn1cXG5cXG4uVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1icmVhZGNydW0tMWoxNUUgc3BhbiB7XFxuICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi10b3A6IDU2cHg7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LXRvcFNlY3Rpb24tMjFiVjMgLlRlc3QtdGVhY2hTZWN0aW9uLWdua0Z1IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtdG9wU2VjdGlvbi0yMWJWMyAuVGVzdC10ZWFjaFNlY3Rpb24tZ25rRnUgLlRlc3QtdGVhY2hJbWdCb3gtMzJ0UFAge1xcbiAgd2lkdGg6IDQwcHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LXRvcFNlY3Rpb24tMjFiVjMgLlRlc3QtdGVhY2hTZWN0aW9uLWdua0Z1IC5UZXN0LXRlYWNoSW1nQm94LTMydFBQIGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4uVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIC5UZXN0LXRlYWNoU2VjdGlvbi1nbmtGdSAuVGVzdC10ZWFjaFNlY3Rpb25OYW1lLTFmQUFyIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIGNvbG9yOiAjODgwMGZlO1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcblxcbi8qIC50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWxlZnQ6IDMwJTtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC5mZWF0dXJlc1NlY3Rpb24gc3BhbiB7XFxuICBmb250LXNpemU6IDE0cHg7XFxuICBsaW5lLWhlaWdodDogMjBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgbWFyZ2luLXJpZ2h0OiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG59ICovXFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBtaW4taGVpZ2h0OiA0NjRweDtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi8qIC5kaXNwbGF5Q2xpZW50cyAuZG90cyB7XFxuICBkaXNwbGF5OiBub25lO1xcbn0gKi9cXG5cXG4uVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSAuVGVzdC1jbGllbnRzV3JhcHBlci0yZkRlUCB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZ3JpZDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDYsIDFmcik7XFxuICBnYXA6IDI0cHg7XFxuICBnYXA6IDEuNXJlbTtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbn1cXG5cXG4uVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSAuVGVzdC1jbGllbnRzV3JhcHBlci0yZkRlUCAuVGVzdC1jbGllbnQtRnpkQloge1xcbiAgd2lkdGg6IDE3MnB4O1xcbiAgaGVpZ2h0OiAxMTJweDtcXG59XFxuXFxuLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSBzcGFuIGE6aG92ZXIge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvaW1hZ2VzL2hvbWUvbmV3X2NvbmZldHRpLnN2ZycpO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAtbXMtZmxleC1wYWNrOiBkaXN0cmlidXRlO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZEhlYWRpbmctcnRkZU8ge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDEuMjtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICBnYXA6IDE2cHg7XFxuICBnYXA6IDFyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1Ige1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiAyMDJweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHBhZGRpbmc6IDEuNXJlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1hY2hpZXZlZFByb2ZpbGUtMjQ3aFgge1xcbiAgd2lkdGg6IDUycHg7XFxuICBoZWlnaHQ6IDUycHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWFjaGlldmVkUHJvZmlsZS0yNDdoWC5UZXN0LWNsaWVudHMtMnAtY1gge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG59XFxuXFxuLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRSb3ctMWxDUWYgLlRlc3QtY2FyZC1UWlpjUiAuVGVzdC1hY2hpZXZlZFByb2ZpbGUtMjQ3aFguVGVzdC1zdHVkZW50cy0zQzdydSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlZmZlO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWFjaGlldmVkUHJvZmlsZS0yNDdoWC5UZXN0LXRlc3RzLTJnczE3IHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmViZjA7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3QtYWNoaWV2ZWRQcm9maWxlLTI0N2hYLlRlc3QtcXVlc3Rpb25zLTN2V1dXIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3QtaGlnaGxpZ2h0LVJ1YVlRIHtcXG4gIGZvbnQtc2l6ZTogMzZweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3QtaGlnaGxpZ2h0LVJ1YVlRLlRlc3QtcXVlc3Rpb25zSGlnaGxpZ2h0LTJfVk5RIHtcXG4gIGNvbG9yOiAjMDBhYzI2O1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWhpZ2hsaWdodC1SdWFZUS5UZXN0LXRlc3RzSGlnaGxpZ2h0LTJiRDFmIHtcXG4gIGNvbG9yOiAjZjM2O1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWhpZ2hsaWdodC1SdWFZUS5UZXN0LXN0dWRlbnRzSGlnaGxpZ2h0LTNwNGNKIHtcXG4gIGNvbG9yOiAjOGMwMGZlO1xcbn1cXG5cXG4uVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiAuVGVzdC1jYXJkLVRaWmNSIC5UZXN0LWhpZ2hsaWdodC1SdWFZUS5UZXN0LWNsaWVudHNIaWdobGlnaHQteUNsRG8ge1xcbiAgY29sb3I6ICNmNjA7XFxufVxcblxcbi5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkUm93LTFsQ1FmIC5UZXN0LWNhcmQtVFpaY1IgLlRlc3Qtc3ViVGV4dC0yUThScyB7XFxuICBmb250LXNpemU6IDI0cHg7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLyogLnRvZ2dsZUF0UmlnaHQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHggMCA2NHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xcbn1cXG5cXG4udG9nZ2xlQXRMZWZ0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDAgNjRweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbn0gKi9cXG5cXG4uVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIHBhZGRpbmc6IDE2MHB4IDMycHggMTYwcHggNjRweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bSAuVGVzdC1zZWN0aW9uX2NvbnRlbnRzLTJXQVNXIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgfVxcblxcbi5UZXN0LXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMUlLRm8ge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwYWRkaW5nOiAxNjBweCAzMnB4IDE2MHB4IDY0cHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLlRlc3Qtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xSUtGbyAuVGVzdC1zZWN0aW9uX2NvbnRlbnRzLTJXQVNXIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxuICB9XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIHtcXG4gIHdpZHRoOiA1MCU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC1lbXB0eUNhcmQtMk14SEEge1xcbiAgd2lkdGg6IDYwNnB4O1xcbiAgaGVpZ2h0OiAzNDFweDtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXRvcENhcmQtM0FfTm8ge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgei1pbmRleDogMTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogODhweDtcXG4gIGhlaWdodDogODhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXRvcENpcmNsZS0yQjJIUCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogODhweDtcXG4gIGhlaWdodDogODhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb25saW5lVGVzdHMtMmk0bHUgLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gIHRvcDogLTQ0cHg7XFxuICByaWdodDogNTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW9ubGluZVRlc3RzLTJpNGx1IC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICBib3R0b206IC00NHB4O1xcbiAgbGVmdDogNzlweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUwZTg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LW93blBhcGVycy0xR3kwciAuVGVzdC10b3BDaXJjbGUtMkIySFAge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIG9wYWNpdHk6IDAuMztcXG4gIHRvcDogLTQ0cHg7XFxuICByaWdodDogNzBweDtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb3duUGFwZXJzLTFHeTByIC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbiAgYm90dG9tOiAtNDRweDtcXG4gIGxlZnQ6IDQwcHg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LXF1ZXN0aW9uQmFuay1sLTZtTiAuVGVzdC10b3BDaXJjbGUtMkIySFAge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzZmZjtcXG4gIG9wYWNpdHk6IDAuMjtcXG4gIHRvcDogLTQ0cHg7XFxuICByaWdodDogNzBweDtcXG59XFxuXFxuLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3QtcXVlc3Rpb25CYW5rLWwtNm1OIC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmViNTQ2O1xcbiAgYm90dG9tOiAtNDRweDtcXG4gIGxlZnQ6IDQwcHg7XFxuICBvcGFjaXR5OiAwLjI7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIC5UZXN0LXZpZGVvX3NlY3Rpb24tMkMyUEQge1xcbiAgd2lkdGg6IDQ1JTtcXG4gIG1hcmdpbi1ib3R0b206IDU2cHg7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIC5UZXN0LXZpZGVvX3NlY3Rpb24tMkMyUEQgLlRlc3QtY29udGVudF90aXRsZS0xRE92WCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC12aWRlb0NvbnRlbnRzLXBRcXdDIC5UZXN0LXZpZGVvX3NlY3Rpb24tMkMyUEQgLlRlc3QtY29udGVudF90ZXh0LVFLRkhxIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi8qIC5hbGxjdXN0b21lcnMgcCB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59ICovXFxuXFxuLlRlc3QtY29udGVudC0yaHM5cyBwIHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBsZXR0ZXItc3BhY2luZzogMC40OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5UZXN0LWhpZGUtM3ZQUVUge1xcbiAgZGlzcGxheTogbm9uZTtcXG59XFxuXFxuLlRlc3Qtc2hvdy1MVE9IbyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbn1cXG5cXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xcbiAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIHtcXG4gICAgcGFkZGluZzogNXJlbSA2NHB4IDAgNjRweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLlRlc3QtdGVzdHNJbWFnZS0zWjJkZSB7XFxuICAgIHdpZHRoOiA0OHB4O1xcbiAgICBoZWlnaHQ6IDQ4cHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZXN0LXNlY3Rpb25fdGl0bGUtMXU2dDAge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbWFyZ2luLXRvcDogMTZweDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXBvaW50LTJ5SE1CIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtcmVkRG90LTNtS0gtIHtcXG4gICAgbWFyZ2luLXRvcDogOHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtcG9pbnRUZXh0LTNvbXk5IHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WSAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHdpZHRoOiA0OHB4O1xcbiAgICBoZWlnaHQ6IDQ4cHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb25saW5lVGVzdHMtMmk0bHUgLlRlc3QtdG9wQ2lyY2xlLTJCMkhQIHtcXG4gICAgdG9wOiAtMjRweDtcXG4gICAgcmlnaHQ6IDUwcHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1vbmxpbmVUZXN0cy0yaTRsdSAuVGVzdC1ib3R0b21DaXJjbGUtMlNFQ08ge1xcbiAgICBib3R0b206IC0yNHB4O1xcbiAgICBsZWZ0OiA3OXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3Qtb3duUGFwZXJzLTFHeTByIC5UZXN0LXRvcENpcmNsZS0yQjJIUCB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgIG9wYWNpdHk6IDAuMztcXG4gICAgdG9wOiAtMjRweDtcXG4gICAgcmlnaHQ6IDcwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kuVGVzdC1vd25QYXBlcnMtMUd5MHIgLlRlc3QtYm90dG9tQ2lyY2xlLTJTRUNPIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgYm90dG9tOiAtMjRweDtcXG4gICAgbGVmdDogNDBweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWltYWdlUGFydC1GdW13WS5UZXN0LXF1ZXN0aW9uQmFuay1sLTZtTiAuVGVzdC10b3BDaXJjbGUtMkIySFAge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICBvcGFjaXR5OiAwLjI7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA3MHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZLlRlc3QtcXVlc3Rpb25CYW5rLWwtNm1OIC5UZXN0LWJvdHRvbUNpcmNsZS0yU0VDTyB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgIGJvdHRvbTogLTI0cHg7XFxuICAgIGxlZnQ6IDQwcHg7XFxuICAgIG9wYWNpdHk6IDAuMjtcXG4gIH1cXG5cXG4gIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWRvd25sb2FkQXBwLWg0ZENJIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZXN0LXNlY3Rpb25fY29udGFpbmVyLTJ3QTVtIHtcXG4gICAgbWluLWhlaWdodDogMjByZW07XFxuICAgIHBhZGRpbmc6IDJyZW0gMXJlbTtcXG4gIH1cXG5cXG4gIC5UZXN0LXNlY3Rpb25fY29udGFpbmVyLTJ3QTVtIC5UZXN0LW1heENvbnRhaW5lci0xX1FDTyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1zZWN0aW9uX2NvbnRhaW5lci0yd0E1bSAuVGVzdC1zZWN0aW9uX2NvbnRlbnRzLTJXQVNXIHtcXG4gICAgbWFyZ2luLXRvcDogMzJweDtcXG4gICAgbWF4LWhlaWdodDogbm9uZTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLlRlc3Qtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xSUtGbyB7XFxuICAgIHBhZGRpbmc6IDI0cHggMjBweCA0OHB4O1xcbiAgfVxcblxcbiAgLlRlc3Qtc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZS0xSUtGbyAuVGVzdC1zZWN0aW9uX2NvbnRlbnRzLTJXQVNXIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLlRlc3QtY29udGVudFBhcnQtdk94WW0ge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZy1ib3R0b206IDA7XFxuICB9XFxuXFxuICAuVGVzdC1jb250ZW50UGFydC12T3hZbSA+IGRpdjpudGgtY2hpbGQoMikge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiA0MDBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLlRlc3QtY29udGVudFBhcnQtdk94WW0gLlRlc3QtY29udGVudC0yaHM5cyB7XFxuICAgIG1heC13aWR0aDogbm9uZTtcXG4gIH1cXG5cXG4gIC5UZXN0LWNvbnRlbnRQYXJ0LXZPeFltIC5UZXN0LWNvbnRlbnQtMmhzOXMgLlRlc3QtdGV4dGNvbnRlbnQtM3JXZjEge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC8qIC5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQgLnBvaW50IC5yZWRkb3R3cmFwcGVyIHtcXG4gICAgbWFyZ2luLXRvcDogOXB4O1xcbiAgfVxcblxcbiAgLmNvbnRlbnRQYXJ0IC5jb250ZW50IC50ZXh0Y29udGVudCAucG9pbnQgLnBvaW50VGV4dCB7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICB9ICovXFxuXFxuICAuVGVzdC1jb250ZW50UGFydC12T3hZbSAuVGVzdC1jb250ZW50LTJoczlzIC5UZXN0LXNlY3Rpb25fdGl0bGUtMXU2dDAge1xcbiAgICBmb250LXNpemU6IDI0cHg7XFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LWVtcHR5Q2FyZC0yTXhIQSB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICBoZWlnaHQ6IDE4MHB4O1xcbiAgICBtYXJnaW46IDAgYXV0byAzMnB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXZpZGVvQ29udGVudHMtcFFxd0Mge1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyAuVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICB9XFxuXFxuICAuVGVzdC1pbWFnZVBhcnQtRnVtd1kgLlRlc3QtdmlkZW9Db250ZW50cy1wUXF3QyAuVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEOmxhc3QtY2hpbGQge1xcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXZpZGVvQ29udGVudHMtcFFxd0MgLlRlc3QtdmlkZW9fc2VjdGlvbi0yQzJQRCAuVGVzdC1jb250ZW50X3RpdGxlLTFET3ZYIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbiAgfVxcblxcbiAgLlRlc3QtaW1hZ2VQYXJ0LUZ1bXdZIC5UZXN0LXZpZGVvQ29udGVudHMtcFFxd0MgLlRlc3QtdmlkZW9fc2VjdGlvbi0yQzJQRCAuVGVzdC1jb250ZW50X3RleHQtUUtGSHEge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtbW9iaWxlUGFwZXJzQ29udGFpbmVyLTFHUE91IHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5UZXN0LW1vYmlsZVBhcGVyc0NvbnRhaW5lci0xR1BPdSBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xcbiAgICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbiAgfVxcblxcbiAgLlRlc3QtcGFwZXJzV3JhcHBlci0xYUlOZCB7XFxuICAgIG1heC13aWR0aDogMzYwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5UZXN0LXJlcG9ydHNfc3ViX3RpdGxlLTF6VTRWIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXJlcG9ydHNfbm90aWNlLTFrN3hZIHtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXN1Yl9zZWN0aW9uX3RpdGxlLTNWUnUzIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5UZXN0LWhlYWRlcmJhY2tncm91bmRtb2JpbGUtM0ZOLTYge1xcbiAgICB3aWR0aDogMjMycHg7XFxuICAgIGhlaWdodDogMzM1cHg7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICBsZWZ0OiAwJTtcXG4gICAgYm90dG9tOiAtMXJlbTtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUge1xcbiAgICBwYWRkaW5nOiAxLjVyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMjdyZW07XFxuICB9XFxuXFxuICAuVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSBoMyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWF4LXdpZHRoOiAxNC44NzVyZW07XFxuICB9XFxuXFxuICAuVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSAuVGVzdC1jbGllbnRzV3JhcHBlci0yZkRlUCB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAwIDA7XFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gICAgbWFyZ2luOiAwIGF1dG87XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICAgIGdhcDogMXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICB9XFxuXFxuICAuVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNSAuVGVzdC1jbGllbnRzV3JhcHBlci0yZkRlUCAuVGVzdC1jbGllbnQtRnpkQloge1xcbiAgICB3aWR0aDogOS43NXJlbTtcXG4gICAgaGVpZ2h0OiA3cmVtO1xcbiAgfVxcblxcbiAgLyogLmRpc3BsYXlDbGllbnRzIC5jbGllbnRzV3JhcHBlciAuY2xpZW50LmFjdGl2ZSB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgfSAqL1xcblxcbiAgLlRlc3QtZGlzcGxheUNsaWVudHMtMThqcjUgc3BhbiB7XFxuICAgIG1heC13aWR0aDogMzA3cHg7XFxuICB9XFxuXFxuICAvKiAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5kb3RzIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gIH0gKi9cXG4gIC5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDFyZW07XFxuICAgIGJhY2tncm91bmQ6IHVybCgnL2ltYWdlcy9ob21lL0NvbmZldHRpLnN2ZycpO1xcbiAgfVxcblxcbiAgLlRlc3QtYWNoaWV2ZWRDb250YWluZXItMUZyT0YgLlRlc3QtYWNoaWV2ZWRIZWFkaW5nLXJ0ZGVPIHtcXG4gICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcXG4gIH1cXG5cXG4gIC5UZXN0LWFjaGlldmVkQ29udGFpbmVyLTFGck9GIC5UZXN0LWFjaGlldmVkSGVhZGluZy1ydGRlTyBzcGFuOjphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICdcXFxcQSc7XFxuICAgIHdoaXRlLXNwYWNlOiBwcmU7XFxuICB9XFxuXFxuICAuVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRiAuVGVzdC1hY2hpZXZlZFJvdy0xbENRZiB7XFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyO1xcbiAgICBnYXA6IDAuNzVyZW07XFxuICAgIG1heC13aWR0aDogMjAuNXJlbTtcXG4gICAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC8qIC5jdXN0b21lcnNfY29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG5cXG4gICAgLmN1c3RvbWVyX3JldmlldyB7XFxuICAgICAgb3JkZXI6IDI7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIHdpZHRoOiAxMDAlO1xcbiAgICAgIGhlaWdodDogNTAlO1xcbiAgICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG5cXG4gICAgICAuY3VzdG9tZXJMb2dvIHtcXG4gICAgICAgIHdpZHRoOiA4OHB4O1xcbiAgICAgICAgaGVpZ2h0OiA1NnB4O1xcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5zcmlDaGFpdGFueWFUZXh0IHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICAgIH1cXG5cXG4gICAgICAuYXV0aG9yV3JhcHBlciB7XFxuICAgICAgICAuYWJvdXRfYXV0aG9yIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMzRweDtcXG4gICAgICAgIH1cXG4gICAgICB9XFxuICAgIH1cXG4gIH0gKi9cXG5cXG4gIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LW1heENvbnRhaW5lci0xX1FDTyB7XFxuICAgIHBvc2l0aW9uOiBzdGF0aWM7XFxuICAgIG1heC13aWR0aDogNDUwcHg7XFxuICB9XFxuXFxuICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1icmVhZGNydW0tMWoxNUUge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyB7XFxuICAgIGhlaWdodDogNjhyZW07XFxuICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgfVxcblxcbiAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1oZWFkaW5nLTFvTnM1IHtcXG4gICAgICBmb250LXNpemU6IDMycHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICBtYXJnaW46IDMycHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC1oZWFkaW5nLTFvTnM1IC5UZXN0LWxlYXJuaW5nVGV4dC0xUEJYMyB7XFxuICAgICAgICB3aWR0aDogMjEwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtaGVhZGluZy0xb05zNSAuVGVzdC1sZWFybmluZ1RleHQtMVBCWDMgaW1nIHtcXG4gICAgICAgICAgbGVmdDogMDtcXG4gICAgICAgICAgbWluLXdpZHRoOiAxNDBweDtcXG4gICAgICAgIH1cXG5cXG4gICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtY29udGVudFRleHQtMkhwNnkge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgbWF4LXdpZHRoOiA2NTBweDtcXG4gICAgICBtYXJnaW46IDE2cHggYXV0byAwIGF1dG87XFxuICAgIH1cXG5cXG4gICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgfVxcblxcbiAgICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3QtcmVxdWVzdERlbW8tRXlQSEYge1xcbiAgICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1pbi13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiAuVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEoge1xcbiAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlc3QtdGVhY2hDb250YWluZXItNGxMT28gLlRlc3QtYnV0dG9ud3JhcHBlci1rbXJwUiAuVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEogLlRlc3Qtd2hhdHNhcHAtMy1oSXIge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgICBvcGFjaXR5OiAxO1xcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LWJ1dHRvbndyYXBwZXIta21ycFIgLlRlc3Qtd2hhdHNhcHB3cmFwcGVyLTNEbVRKIGltZyB7XFxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDI0cHg7XFxuICAgICAgICB9XFxuXFxuICAgIC5UZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vIC5UZXN0LXRvcFNlY3Rpb24tMjFiVjMge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIC5UZXN0LXRlYWNoU2VjdGlvbi1nbmtGdSAuVGVzdC10ZWFjaEltZ0JveC0zMnRQUCB7XFxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDMycHg7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAuVGVzdC10ZWFjaENvbnRhaW5lci00bExPbyAuVGVzdC10b3BTZWN0aW9uLTIxYlYzIC5UZXN0LXRlYWNoU2VjdGlvbi1nbmtGdSAuVGVzdC10ZWFjaFNlY3Rpb25OYW1lLTFmQUFyIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIH1cXG5cXG4gIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiB7XFxuICAgIHBhZGRpbmc6IDMycHggMjhweDtcXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIH1cXG5cXG4gICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyB7XFxuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnI7XFxuICAgIH1cXG5cXG4gICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LWF2YWlsYWJsZVRpdGxlLTNORWZBIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtcm93LXNFLUNvIHtcXG4gICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgfVxcblxcbiAgICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1yb3ctc0UtQ28gLlRlc3QtcGxhdGZvcm1Db250YWluZXItMk9obnQge1xcbiAgICAgICAgICB3aWR0aDogNjhweDtcXG4gICAgICAgICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1yb3ctc0UtQ28gLlRlc3QtcGxhdGZvcm1Db250YWluZXItMk9obnQgLlRlc3QtcGxhdGZvcm0ta094QU0ge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuM3B4O1xcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgICB9XFxuXFxuICAgICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtcm93LXNFLUNvIC5UZXN0LXBsYXRmb3JtQ29udGFpbmVyLTJPaG50IC5UZXN0LXBsYXRmb3JtT3MtMzZQa0cge1xcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcXG4gICAgICAgICAgfVxcblxcbiAgICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1yb3ctc0UtQ28gLlRlc3QtcGxhdGZvcm1Db250YWluZXItMk9obnQ6bGFzdC1jaGlsZCB7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAuVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDIgLlRlc3QtYXZhaWxhYmxlUm93LTJReW5jIC5UZXN0LWRlc2t0b3BJbWFnZS0ya1hOWCB7XFxuICAgICAgICB3aWR0aDogOTAlO1xcbiAgICAgICAgaGVpZ2h0OiAxNjFweDtcXG4gICAgICAgIG1hcmdpbjogMCBhdXRvIDAgYXV0bztcXG4gICAgICB9XFxuXFxuICAgICAgLlRlc3QtYXZhaWxhYmxlQ29udGFpbmVyLTN4TFQyIC5UZXN0LWF2YWlsYWJsZVJvdy0yUXluYyAuVGVzdC1zdG9yZS0zQ3ppeiB7XFxuICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtc3RvcmUtM0N6aXogLlRlc3QtcGxheXN0b3JlLTNQS09DIHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMCAxNnB4IDAgMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5UZXN0LWF2YWlsYWJsZUNvbnRhaW5lci0zeExUMiAuVGVzdC1hdmFpbGFibGVSb3ctMlF5bmMgLlRlc3Qtc3RvcmUtM0N6aXogLlRlc3QtYXBwc3RvcmUtMWhPMTYge1xcbiAgICAgICAgICB3aWR0aDogMTQwcHg7XFxuICAgICAgICAgIGhlaWdodDogNDJweDtcXG4gICAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgfVxcblxcbiAgLlRlc3Qtam9pbkNvbW11bml0eS0zTEJVcyB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIGhlaWdodDogMTQ0cHg7XFxuICB9XFxuXFxuICAuVGVzdC1qb2luRmJUZXh0LTFmZGhNIHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuVGVzdC1qb2luRmJMaW5rLUF2Y3NmIHtcXG4gICAgcGFkZGluZzogMTJweCAzMnB4O1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtdmlkZW9TbGlkZXItMjhfdWcge1xcbiAgICBwYWRkaW5nOiAzcmVtIDFyZW07XFxuICAgIG1pbi1oZWlnaHQ6IDM4cmVtO1xcbiAgfVxcblxcbiAgLlRlc3QtbWF0aHMtVHVjRVUge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGxlZnQ6IDE1JTtcXG4gICAgdG9wOiA4NCU7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDM2cHg7XFxuICB9XFxuXFxuICAuVGVzdC1taWNyb3Njb3BlLUhzYXRHIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDg3JTtcXG4gICAgbGVmdDogNDUlO1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLlRlc3QtdHJpYW5nbGUtMkJVS1kge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogODUlO1xcbiAgICBsZWZ0OiA4MCU7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDMwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1zY2FsZS0xYzlJNCB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcmlnaHQ6IDgwJTtcXG4gICAgdG9wOiAxMCU7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDMwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1jaXJjbGUtMmpXMXcge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogMyU7XFxuICAgIHJpZ2h0OiA1MCU7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDMwcHg7XFxuICB9XFxuXFxuICAuVGVzdC1jaGVtaXN0cnktMmVZT1cge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIHRvcDogOCU7XFxuICAgIHJpZ2h0OiA1JTtcXG4gICAgd2lkdGg6IDM2cHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC5UZXN0LXNjcm9sbFRvcC0xNFM4QyB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9wcm9kdWN0cy9nZXQtcmFua3MvVGVzdC9UZXN0LnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx3QkFBd0I7TUFDcEIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsVUFBVTtDQUNYOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFFBQVE7Q0FDVDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsU0FBUztDQUNWOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFFBQVE7Q0FDVDs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsVUFBVTtDQUNYOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0NBQ1o7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsY0FBYztFQUNkLFNBQVM7RUFDVCxhQUFhO0VBQ2IsY0FBYztDQUNmOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYix1QkFBdUI7T0FDcEIsb0JBQW9CO0dBQ3hCOztBQUVIO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsd0JBQXdCO01BQ3BCLG9CQUFvQjtFQUN4QixXQUFXO0NBQ1o7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjtHQUNsQjs7QUFFSDtNQUNNLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsZUFBZTs7TUFFZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztVQXlCSTtLQUNMOztBQUVMOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTBFSTs7QUFFSjtFQUNFLCtCQUErQjtFQUMvQiwrQkFBK0I7RUFDL0IsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7QUFFRDtJQUNJLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMsYUFBYTtHQUNkOztBQUVIO01BQ00sYUFBYTtNQUNiLGNBQWM7S0FDZjs7QUFFTDtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QsOEZBQThGO0VBQzlGLG9FQUFvRTtFQUNwRSwrREFBK0Q7RUFDL0QsNERBQTREO0VBQzVELHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1Qix1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtFQUMzQixzQkFBc0I7TUFDbEIsd0JBQXdCO0NBQzdCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0I7Q0FDdkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsb0JBQW9CO01BQ2hCLGdCQUFnQjtFQUNwQixzQkFBc0I7RUFDdEIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxvQkFBb0I7TUFDaEIsZ0JBQWdCO0VBQ3BCLHFCQUFxQjtNQUNqQiw0QkFBNEI7RUFDaEMsb0JBQW9CO0VBQ3BCLG9CQUFvQjtDQUNyQjs7QUFFRDtJQUNJLGFBQWE7SUFDYixhQUFhO0lBQ2IsbUJBQW1CO0dBQ3BCOztBQUVIO0lBQ0ksYUFBYTtJQUNiLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osYUFBYTtFQUNiLFlBQVk7RUFDWixhQUFhO0VBQ2IsV0FBVztFQUNYLGdCQUFnQjtDQUNqQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0FBRUg7RUFDRSxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLDBCQUEwQjtFQUMxQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLHlCQUF5QjtFQUN6QixvQkFBb0I7RUFDcEIsc0JBQXNCO0VBQ3RCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYix1QkFBdUI7S0FDcEIsb0JBQW9CO0NBQ3hCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixRQUFRO0VBQ1IsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLFlBQVk7RUFDWixxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsMkJBQTJCO0VBQzNCLHdCQUF3QjtFQUN4QixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxxQkFBcUI7TUFDakIsNEJBQTRCO0VBQ2hDLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0Isc0JBQXNCO01BQ2xCLHdCQUF3QjtFQUM1QiwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGFBQWE7RUFDYixhQUFhO0NBQ2Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQWlDSTs7QUFFSjtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHVCQUF1QjtNQUNuQixvQkFBb0I7RUFDeEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0NBQ3pCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsdUJBQXVCO0tBQ3BCLG9CQUFvQjtDQUN4Qjs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0FBRUQ7Ozs7Ozs7Ozs7O0lBV0k7O0FBRUo7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IscUJBQXFCO01BQ2pCLDRCQUE0QjtFQUNoQywwQkFBMEI7Q0FDM0I7O0FBRUQ7O0lBRUk7O0FBRUo7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7RUFDZCxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixZQUFZO0VBQ1osZUFBZTtDQUNoQjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxzQkFBc0I7RUFDdEIscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix1REFBdUQ7RUFDdkQseUJBQXlCO0VBQ3pCLDRCQUE0QjtFQUM1Qix1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLDBCQUEwQjtNQUN0Qiw4QkFBOEI7Q0FDbkM7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLDJCQUEyQjtFQUMzQix3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQ0FBc0M7RUFDdEMsc0NBQXNDO0VBQ3RDLFVBQVU7RUFDVixVQUFVO0VBQ1YsYUFBYTtDQUNkOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsK0RBQStEO1VBQ3ZELHVEQUF1RDtFQUMvRCx1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtNQUNuQixvQkFBb0I7Q0FDekI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsdUJBQXVCO01BQ25CLG9CQUFvQjtFQUN4QixvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCOztBQUVEOzs7Ozs7Ozs7Ozs7OztJQWNJOztBQUVKO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7TUFDdkIsdUJBQXVCO0VBQzNCLHVCQUF1QjtFQUN2QiwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLGFBQWE7RUFDYixhQUFhO0NBQ2Q7O0FBRUQ7SUFDSSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLGdDQUFnQztRQUM1Qiw0QkFBNEI7SUFDaEMsWUFBWTtJQUNaLHVCQUF1QjtRQUNuQiwrQkFBK0I7R0FDcEM7O0FBRUg7RUFDRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtNQUN2Qix1QkFBdUI7RUFDM0IsMEJBQTBCO0VBQzFCLCtCQUErQjtFQUMvQixZQUFZO0VBQ1osYUFBYTtFQUNiLGFBQWE7Q0FDZDs7QUFFRDtJQUNJLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2Qsd0JBQXdCO1FBQ3BCLG9CQUFvQjtJQUN4QixZQUFZO0lBQ1osdUJBQXVCO1FBQ25CLCtCQUErQjtHQUNwQzs7QUFFSDtFQUNFLFdBQVc7Q0FDWjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2IsV0FBVztFQUNYLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQixjQUFjO0VBQ2QsV0FBVztDQUNaOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixXQUFXO0VBQ1gsWUFBWTtDQUNiOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLGNBQWM7RUFDZCxXQUFXO0VBQ1gsYUFBYTtDQUNkOztBQUVEO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxvQkFBb0I7TUFDaEIsZ0JBQWdCO0VBQ3BCLHVCQUF1QjtNQUNuQiwrQkFBK0I7Q0FDcEM7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7Q0FDaEI7O0FBRUQ7Ozs7O0lBS0k7O0FBRUo7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsYUFBYTtFQUNiLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCO01BQ3ZCLHVCQUF1QjtDQUM1Qjs7QUFFRDtFQUNFO0lBQ0UsMEJBQTBCO0dBQzNCO0NBQ0Y7O0FBRUQ7RUFDRTtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsYUFBYTtHQUNkOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWiwwQkFBMEI7R0FDM0I7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsV0FBVztJQUNYLDBCQUEwQjtHQUMzQjs7RUFFRDtJQUNFLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsV0FBVztJQUNYLFlBQVk7R0FDYjs7RUFFRDtJQUNFLDBCQUEwQjtJQUMxQixjQUFjO0lBQ2QsV0FBVztHQUNaOztFQUVEO0lBQ0UsMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixXQUFXO0lBQ1gsWUFBWTtHQUNiOztFQUVEO0lBQ0UsMEJBQTBCO0lBQzFCLGNBQWM7SUFDZCxXQUFXO0lBQ1gsYUFBYTtHQUNkOztFQUVEO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsYUFBYTtHQUNkOztFQUVEO0lBQ0Usa0JBQWtCO0lBQ2xCLG1CQUFtQjtHQUNwQjs7RUFFRDtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLDJCQUEyQjtRQUN2Qix1QkFBdUI7R0FDNUI7O0VBRUQ7SUFDRSx3QkFBd0I7R0FDekI7O0VBRUQ7SUFDRSwyQkFBMkI7UUFDdkIsdUJBQXVCO0dBQzVCOztFQUVEO0lBQ0UsWUFBWTtJQUNaLDJCQUEyQjtRQUN2Qix1QkFBdUI7SUFDM0IscUJBQXFCO1FBQ2pCLDRCQUE0QjtJQUNoQyxrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLGdCQUFnQjtHQUNqQjs7RUFFRDtJQUNFLGVBQWU7SUFDZixZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGFBQWE7R0FDZDs7RUFFRDs7Ozs7O01BTUk7O0VBRUo7SUFDRSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsc0JBQXNCO1FBQ2xCLHdCQUF3QjtJQUM1QiwyQkFBMkI7UUFDdkIsdUJBQXVCO0lBQzNCLGFBQWE7R0FDZDs7RUFFRDtJQUNFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsY0FBYztJQUNkLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLDJCQUEyQjtRQUN2Qix1QkFBdUI7R0FDNUI7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osb0JBQW9CO0dBQ3JCOztFQUVEO0lBQ0UsaUJBQWlCO0dBQ2xCOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtHQUNyQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHVCQUF1QjtRQUNuQixvQkFBb0I7SUFDeEIsc0JBQXNCO1FBQ2xCLHdCQUF3QjtHQUM3Qjs7RUFFRDtJQUNFLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLHVCQUF1QjtPQUNwQixvQkFBb0I7R0FDeEI7O0VBRUQ7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtHQUNkOztFQUVEO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLGdCQUFnQjtJQUNoQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0UsYUFBYTtJQUNiLGNBQWM7SUFDZCxTQUFTO0lBQ1QsU0FBUztJQUNULGNBQWM7SUFDZCxhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxxQkFBcUI7SUFDckIsa0JBQWtCO0dBQ25COztFQUVEO0lBQ0Usa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHNDQUFzQztJQUN0QyxtQ0FBbUM7SUFDbkMsVUFBVTtJQUNWLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsZUFBZTtJQUNmLGFBQWE7R0FDZDs7RUFFRDs7TUFFSTs7RUFFSjtJQUNFLGlCQUFpQjtHQUNsQjs7RUFFRDs7TUFFSTtFQUNKO0lBQ0UscUJBQXFCO0lBQ3JCLDZDQUE2QztHQUM5Qzs7RUFFRDtJQUNFLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsc0JBQXNCO0dBQ3ZCOztFQUVEO0lBQ0UsY0FBYztJQUNkLGlCQUFpQjtHQUNsQjs7RUFFRDtJQUNFLDJCQUEyQjtJQUMzQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFDeEIsbUJBQW1CO0lBQ25CLGFBQWE7R0FDZDs7RUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQXdDSTs7RUFFSjtJQUNFLGlCQUFpQjtJQUNqQixpQkFBaUI7R0FDbEI7O0VBRUQ7SUFDRSxjQUFjO0lBQ2QsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixtQkFBbUI7R0FDcEI7O0lBRUM7TUFDRSxnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIseUJBQXlCO0tBQzFCOztNQUVDO1FBQ0UsYUFBYTtPQUNkOztRQUVDO1VBQ0UsUUFBUTtVQUNSLGlCQUFpQjtTQUNsQjs7SUFFTDtNQUNFLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLGlCQUFpQjtNQUNqQix5QkFBeUI7S0FDMUI7O0lBRUQ7TUFDRSwyQkFBMkI7VUFDdkIsdUJBQXVCO01BQzNCLGlCQUFpQjtLQUNsQjs7TUFFQztRQUNFLGtCQUFrQjtRQUNsQixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGdCQUFnQjtPQUNqQjs7TUFFRDtRQUNFLGlCQUFpQjtRQUNqQixvQkFBb0I7T0FDckI7O1FBRUM7VUFDRSxnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLFdBQVc7VUFDWCxlQUFlO1NBQ2hCOztRQUVEO1VBQ0UsWUFBWTtVQUNaLGFBQWE7U0FDZDs7SUFFTDtNQUNFLHNCQUFzQjtVQUNsQix3QkFBd0I7TUFDNUIsY0FBYzs7TUFFZDs7VUFFSTtLQUNMO1FBQ0c7VUFDRSxZQUFZO1VBQ1osYUFBYTtTQUNkOztRQUVEO1VBQ0UsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtTQUNuQjs7RUFFUDtJQUNFLG1CQUFtQjtJQUNuQixpQkFBaUI7R0FDbEI7O0lBRUM7TUFDRSwyQkFBMkI7S0FDNUI7O01BRUM7UUFDRSxnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLG9CQUFvQjtPQUNyQjs7TUFFRDtRQUNFLFVBQVU7UUFDVixvQkFBb0I7UUFDcEIsc0JBQXNCO1lBQ2xCLHdCQUF3QjtPQUM3Qjs7UUFFQztVQUNFLFlBQVk7VUFDWixxQkFBcUI7Y0FDakIsNEJBQTRCO1VBQ2hDLG1CQUFtQjtVQUNuQixpQkFBaUI7U0FDbEI7O1VBRUM7WUFDRSxrQkFBa0I7WUFDbEIsa0JBQWtCO1dBQ25COztVQUVEO1lBQ0UsZ0JBQWdCO1lBQ2hCLGtCQUFrQjtXQUNuQjs7UUFFSDtVQUNFLGdCQUFnQjtTQUNqQjs7TUFFSDtRQUNFLFdBQVc7UUFDWCxjQUFjO1FBQ2Qsc0JBQXNCO09BQ3ZCOztNQUVEO1FBQ0Usc0JBQXNCO1lBQ2xCLHdCQUF3QjtRQUM1QixvQkFBb0I7T0FDckI7O1FBRUM7VUFDRSxhQUFhO1VBQ2IsYUFBYTtVQUNiLG1CQUFtQjtTQUNwQjs7UUFFRDtVQUNFLGFBQWE7VUFDYixhQUFhO1VBQ2IsVUFBVTtTQUNYOztFQUVQO0lBQ0UsMkJBQTJCO1FBQ3ZCLHVCQUF1QjtJQUMzQixzQkFBc0I7UUFDbEIsd0JBQXdCO0lBQzVCLGNBQWM7R0FDZjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtHQUNuQjs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixrQkFBa0I7R0FDbkI7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFNBQVM7SUFDVCxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxVQUFVO0lBQ1YsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFNBQVM7SUFDVCxZQUFZO0lBQ1osYUFBYTtHQUNkOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7R0FDZDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7SUFDWixhQUFhO0dBQ2Q7O0VBRUQ7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixhQUFhO0dBQ2Q7Q0FDRlwiLFwiZmlsZVwiOlwiVGVzdC5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIi5tYXhDb250YWluZXIge1xcbiAgbWF4LXdpZHRoOiAxNzAwcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIG1hcmdpbjogYXV0bztcXG59XFxuXFxuLm1hcmdpbkJvdHRvbTEyNiB7XFxuICBtYXJnaW4tYm90dG9tOiAxMjZweDtcXG59XFxuXFxuLnJlcG9ydHNfc3ViX3RpdGxlIHtcXG4gIGZvbnQtc2l6ZTogMjhweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi10b3A6IDEycHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLnJlcG9ydHNfbm90aWNlIHtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tdG9wOiA0cHg7XFxuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xcbn1cXG5cXG4ucm93MSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbn1cXG5cXG4ucG9pbnRDb250YWluZXIge1xcbiAgd2lkdGg6IDUlO1xcbn1cXG5cXG4ucG9pbnQge1xcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcXG59XFxuXFxuLnBvaW50VGV4dCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG59XFxuXFxuLnJlZERvdCB7XFxuICB3aWR0aDogOHB4O1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM2O1xcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xcbiAgbWFyZ2luLXRvcDogMTJweDtcXG59XFxuXFxuLnRlc3RzSW1hZ2Uge1xcbiAgd2lkdGg6IDcycHg7XFxuICBoZWlnaHQ6IDcycHg7XFxuICBtYXJnaW4tdG9wOiAtOHB4O1xcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xcbn1cXG5cXG4udGVzdHNJbWFnZSBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbi5zZWN0aW9uX2NvbnRhaW5lci5hc2hCYWNrZ3JvdW5kIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi52aWRlb1NsaWRlciB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDgwcHggNjRweDtcXG4gIHBhZGRpbmc6IDVyZW0gNHJlbTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5tYXRocyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBsZWZ0OiAxMCU7XFxuICB0b3A6IDUlO1xcbn1cXG5cXG4ubWljcm9zY29wZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDMwJTtcXG4gIGxlZnQ6IDMlO1xcbn1cXG5cXG4udHJpYW5nbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA2MCU7XFxuICBsZWZ0OiAxMCU7XFxufVxcblxcbi5zY2FsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICByaWdodDogMTAlO1xcbiAgdG9wOiA1JTtcXG59XFxuXFxuLmNpcmNsZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDMwJTtcXG4gIHJpZ2h0OiAzJTtcXG59XFxuXFxuLmNoZW1pc3RyeSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDYwJTtcXG4gIHJpZ2h0OiAxMCU7XFxufVxcblxcbi5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogLTU2cHg7XFxuICByaWdodDogMDtcXG4gIHdpZHRoOiA1MTNweDtcXG4gIGhlaWdodDogNTYwcHg7XFxufVxcblxcbi5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICB9XFxuXFxuLnNlY3Rpb25fdGl0bGUge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgMCAxNnB4O1xcbiAgZm9udC1zaXplOiA0MHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQ4cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5zdWJfc2VjdGlvbl90aXRsZSB7XFxuICBmb250LXNpemU6IDI4cHg7XFxuICBsaW5lLWhlaWdodDogMzJweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbn1cXG5cXG4uY29udGVudFBhcnQge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIHdpZHRoOiA0NSU7XFxufVxcblxcbi5jb250ZW50UGFydCAuY29udGVudCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtaW4td2lkdGg6IDI5MHB4O1xcbiAgICBtYXgtd2lkdGg6IDU0MHB4O1xcbiAgfVxcblxcbi5jb250ZW50UGFydCAuY29udGVudCAudGV4dGNvbnRlbnQge1xcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICBjb2xvcjogIzI1MjgyYjtcXG5cXG4gICAgICAvKiAucG9pbnQge1xcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxuICAgICAgICBoZWlnaHQ6IG1heC1jb250ZW50O1xcbiAgICAgICAgbWFyZ2luOiA2cHggMDtcXG5cXG4gICAgICAgIC5yZWRkb3R3cmFwcGVyIHtcXG4gICAgICAgICAgd2lkdGg6IDUlO1xcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgICAgIG1hcmdpbi10b3A6IDEycHg7XFxuXFxuICAgICAgICAgIC5yZWRkb3Qge1xcbiAgICAgICAgICAgIHdpZHRoOiA4cHg7XFxuICAgICAgICAgICAgaGVpZ2h0OiA4cHg7XFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzNjtcXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgICAgICAgIH1cXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5wb2ludFRleHQge1xcbiAgICAgICAgICB3aWR0aDogOTUlO1xcbiAgICAgICAgfVxcbiAgICAgIH0gKi9cXG4gICAgfVxcblxcbi8qIC5jdXN0b21lcnNfY29udGFpbmVyIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzY7XFxuICBjb2xvcjogI2ZmZjtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBoZWlnaHQ6IDU2MHB4O1xcblxcbiAgLmN1c3RvbWVyX3JldmlldyB7XFxuICAgIG9yZGVyOiAwO1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gICAgcGFkZGluZzogNDhweCA2NHB4O1xcbiAgICB3aWR0aDogNTAlO1xcblxcbiAgICAuY3VzdG9tZXJMb2dvIHtcXG4gICAgICB3aWR0aDogMTIwcHg7XFxuICAgICAgaGVpZ2h0OiA4MHB4O1xcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcblxcbiAgICAgIGltZyB7XFxuICAgICAgICB3aWR0aDogMTAwJTtcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcbiAgICB9XFxuXFxuICAgIC5zcmlDaGFpdGFueWFUZXh0IHtcXG4gICAgICBmb250LXNpemU6IDIwcHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XFxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIG1heC13aWR0aDogNjAwcHg7XFxuICAgIH1cXG5cXG4gICAgLmF1dGhvcldyYXBwZXIge1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG5cXG4gICAgICAuYXV0aG9yX3RpdGxlIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gICAgICAgIGNvbG9yOiAjZmZmO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuYWJvdXRfYXV0aG9yIHtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gICAgICB9XFxuICAgIH1cXG5cXG4gICAgLmFsbGN1c3RvbWVycyB7XFxuICAgICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICAgICAgY29sb3I6ICNmZmY7XFxuICAgICAgZGlzcGxheTogZmxleDtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgICAgaW1nIHtcXG4gICAgICAgIG1hcmdpbi10b3A6IDNweDtcXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XFxuICAgICAgICB3aWR0aDogMjBweDtcXG4gICAgICAgIGhlaWdodDogMjBweDtcXG4gICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgfVxcbiAgICB9XFxuICB9XFxufSAqL1xcblxcbi5hdmFpbGFibGVDb250YWluZXIge1xcbiAgcGFkZGluZzogODBweCAxMTNweCA1NnB4IDE2NHB4O1xcbiAgcGFkZGluZzogNXJlbSAxMTNweCA1NnB4IDE2NHB4O1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XFxufVxcblxcbi5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyB7XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgyLCAxZnIpO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5kZXNrdG9wSW1hZ2Uge1xcbiAgICAgIHdpZHRoOiA2OTZweDtcXG4gICAgICBoZWlnaHQ6IDM3MXB4O1xcbiAgICB9XFxuXFxuLmpvaW5Db21tdW5pdHkge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDE2MHB4O1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgYm90dG9tLCBsZWZ0IHRvcCwgZnJvbSgjZWE0YzcwKSwgdG8oI2IyNDU3YykpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLCAjZWE0YzcwLCAjYjI0NTdjKTtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IC1vLWxpbmVhci1ncmFkaWVudChib3R0b20sICNlYTRjNzAsICNiMjQ1N2MpO1xcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2VhNGM3MCwgI2IyNDU3Yyk7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi5qb2luRmJUZXh0IHtcXG4gIGZvbnQtc2l6ZTogMzJweDtcXG4gIGxpbmUtaGVpZ2h0OiA0OHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1yaWdodDogNjRweDtcXG4gIGNvbG9yOiAjZmZmO1xcbn1cXG5cXG4uam9pbkZiTGluayB7XFxuICBwYWRkaW5nOiAxNnB4IDQwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG59XFxuXFxuLmF2YWlsYWJsZVRpdGxlIHtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5hdmFpbGFibGUge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5hdmFpbGFibGVDb250ZW50U2VjdGlvbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbn1cXG5cXG4ucGxhdGZvcm1Db250YWluZXIge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIG1hcmdpbi1yaWdodDogMnJlbTtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcXG59XFxuXFxuLnBsYXRmb3JtIHtcXG4gIGZvbnQtc2l6ZTogMTZweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW46IDhweCAwIDRweCAwO1xcbiAgY29sb3I6ICMyNTI4MmI7XFxufVxcblxcbi5wbGF0Zm9ybU9zIHtcXG4gIGZvbnQtc2l6ZTogMTJweDtcXG4gIG9wYWNpdHk6IDAuNjtcXG59XFxuXFxuLnJvdyB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC13cmFwOiB3cmFwO1xcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcXG4gIG1hcmdpbjogNDhweCAwIDMycHggMDtcXG4gIG1hcmdpbjogM3JlbSAwIDJyZW0gMDtcXG59XFxuXFxuLnN0b3JlIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgbWFyZ2luLWJvdHRvbTogNjRweDtcXG4gIG1hcmdpbi1ib3R0b206IDRyZW07XFxufVxcblxcbi5zdG9yZSAucGxheXN0b3JlIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICAgIG1hcmdpbi1yaWdodDogMTZweDtcXG4gIH1cXG5cXG4uc3RvcmUgLmFwcHN0b3JlIHtcXG4gICAgd2lkdGg6IDEzNnB4O1xcbiAgICBoZWlnaHQ6IDQwcHg7XFxuICB9XFxuXFxuLnNjcm9sbFRvcCB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG4gIHJpZ2h0OiA2NHB4O1xcbiAgYm90dG9tOiA2NHB4O1xcbiAgei1pbmRleDogMTtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuLnNjcm9sbFRvcCBpbWcge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbi50ZWFjaENvbnRhaW5lciB7XFxuICBtaW4taGVpZ2h0OiA3MjBweDtcXG4gIHBhZGRpbmc6IDE2cHggNjRweCA1NnB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZTVmZTtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIHtcXG4gIGZvbnQtc2l6ZTogNDhweDtcXG4gIGxpbmUtaGVpZ2h0OiA2NnB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBtYXgtd2lkdGg6IDc0MHB4O1xcbiAgbWFyZ2luOiAxMnB4IDAgMCAwO1xcbiAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCB7XFxuICB3aWR0aDogMzE2cHg7XFxuICBoZWlnaHQ6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IC1tb3otZml0LWNvbnRlbnQ7XFxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC5lbXB0eUNhcmQgaW1nIHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgLy8gb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLmltYWdlUGFydCAudG9wQ2FyZCBpbWcge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICAtby1vYmplY3QtZml0OiBjb250YWluO1xcbiAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5oZWFkaW5nIC5sZWFybmluZ1RleHQgaW1nIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvdHRvbTogLTJweDtcXG4gIGxlZnQ6IDA7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogOHB4O1xcbiAgbWF4LXdpZHRoOiAzMDhweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5jb250ZW50VGV4dCB7XFxuICBmb250LXNpemU6IDIwcHg7XFxuICBsaW5lLWhlaWdodDogNDBweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcXG4gIG1hcmdpbjogMTJweCAwIDAgMDtcXG4gIG1heC13aWR0aDogNjg3cHg7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciB7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAucmVxdWVzdERlbW8ge1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmYztcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY29sb3I6ICMwMDA7XFxuICBwYWRkaW5nOiAxNnB4IDI0cHg7XFxuICB3aWR0aDogLXdlYmtpdC1tYXgtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LW1heC1jb250ZW50O1xcbiAgd2lkdGg6IG1heC1jb250ZW50O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciB7XFxuICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gIHdpZHRoOiAtbW96LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciAud2hhdHNhcHAge1xcbiAgd2lkdGg6IC13ZWJraXQtZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIGZvbnQtc2l6ZTogMjBweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBsaW5lLWhlaWdodDogMS41O1xcbiAgY29sb3I6ICMyNTI4MmIgIWltcG9ydGFudDtcXG4gIG1hcmdpbjogMCA4cHggMCAzMnB4O1xcbiAgcGFkZGluZy1ib3R0b206IDA7XFxuICBvcGFjaXR5OiAwLjY7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAud2hhdHNhcHB3cmFwcGVyIGltZyB7XFxuICB3aWR0aDogMzJweDtcXG4gIGhlaWdodDogMzJweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5kb3dubG9hZEFwcCB7XFxuICBtYXJnaW4tdG9wOiA0MHB4O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBzdGFydDtcXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAgLmRvd25sb2FkVGV4dCB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxuICBsaW5lLWhlaWdodDogMjRweDtcXG4gIG9wYWNpdHk6IDAuNztcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC5kb3dubG9hZEFwcCAucGxheVN0b3JlSWNvbiB7XFxuICB3aWR0aDogMTMycHg7XFxuICBoZWlnaHQ6IDQwcHg7XFxufVxcblxcbi8qIC50ZWFjaENvbnRhaW5lciAuYWN0aW9uc1dyYXBwZXIge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm90dG9tOiAyNnB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24ge1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1yaWdodDogMjRweDtcXG59XFxuLnRlYWNoQ29udGFpbmVyIC5hY3Rpb25zV3JhcHBlciAuYWN0aW9uIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG9wYWNpdHk6IDAuNztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24gLmFjdGlvbmltZ2JveCB7XFxuICB3aWR0aDogMjRweDtcXG4gIGhlaWdodDogMjRweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG4udGVhY2hDb250YWluZXIgLmFjdGlvbnNXcmFwcGVyIC5hY3Rpb24gLmFjdGlvbmltZ2JveCBpbWcge1xcbiAgd2lkdGg6IDEwcHg7XFxuICBoZWlnaHQ6IDlweDtcXG59ICovXFxuXFxuLnRlYWNoQ29udGFpbmVyIC5icmVhZGNydW0ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyBzcGFuIHtcXG4gIGZvbnQtc2l6ZTogMTRweDtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgY29sb3I6ICMwMDc2ZmY7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMTUycHg7XFxuICBtYXJnaW46IDEycHggYXV0byAwO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLmJyZWFkY3J1bSBzcGFuIHtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiA1NnB4O1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxufVxcblxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAudGVhY2hTZWN0aW9uIC50ZWFjaEltZ0JveCB7XFxuICB3aWR0aDogNDBweDtcXG4gIGhlaWdodDogNDBweDtcXG59XFxuXFxuLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoSW1nQm94IGltZyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICBvYmplY3QtZml0OiBjb250YWluO1xcbn1cXG5cXG4udGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hTZWN0aW9uTmFtZSB7XFxuICBtYXJnaW4tbGVmdDogOHB4O1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XFxuICBjb2xvcjogIzg4MDBmZTtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4vKiAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLmZlYXR1cmVzU2VjdGlvbiB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1hcmdpbi1sZWZ0OiAzMCU7XFxufVxcbi50ZWFjaENvbnRhaW5lciAudG9wU2VjdGlvbiAuZmVhdHVyZXNTZWN0aW9uIHNwYW4ge1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi1yaWdodDogMzJweDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxufSAqL1xcblxcbi5kaXNwbGF5Q2xpZW50cyB7XFxuICB3aWR0aDogMTAwJTtcXG4gIG1pbi1oZWlnaHQ6IDQ2NHB4O1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDQwcHg7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG59XFxuXFxuLyogLmRpc3BsYXlDbGllbnRzIC5kb3RzIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufSAqL1xcblxcbi5kaXNwbGF5Q2xpZW50cyBoMyB7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDQwcHg7XFxuICBsaW5lLWhlaWdodDogNDhweDtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIG1hcmdpbi10b3A6IDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNiwgMWZyKTtcXG4gIGdhcDogMjRweDtcXG4gIGdhcDogMS41cmVtO1xcbiAgbWFyZ2luOiAwIGF1dG87XFxufVxcblxcbi5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICB3aWR0aDogMTcycHg7XFxuICBoZWlnaHQ6IDExMnB4O1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xcbn1cXG5cXG4uZGlzcGxheUNsaWVudHMgc3BhbiBhOmhvdmVyIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBwYWRkaW5nOiA1NnB4IDY0cHg7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9pbWFnZXMvaG9tZS9uZXdfY29uZmV0dGkuc3ZnJyk7XFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XFxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LXBhY2s6IGRpc3RyaWJ1dGU7XFxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGZvbnQtc2l6ZTogNDBweDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjI7XFxuICBjb2xvcjogIzI1MjgyYjtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IHtcXG4gIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBkaXNwbGF5OiBncmlkO1xcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcXG4gIC8vIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDIsIDFmcik7XFxuICBnYXA6IDE2cHg7XFxuICBnYXA6IDFyZW07XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQge1xcbiAgd2lkdGg6IDI3MHB4O1xcbiAgaGVpZ2h0OiAyMDJweDtcXG4gIGZvbnQtc2l6ZTogMjRweDtcXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgY29sb3I6IHJnYmEoMzcsIDQwLCA0MywgMC42KTtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgcGFkZGluZzogMjRweDtcXG4gIHBhZGRpbmc6IDEuNXJlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCAwLjI1cmVtIDEuNXJlbSAwIHJnYmEoMTQwLCAwLCAyNTQsIDAuMTYpO1xcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAuMjVyZW0gMS41cmVtIDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4xNik7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlIHtcXG4gIHdpZHRoOiA1MnB4O1xcbiAgaGVpZ2h0OiA1MnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLmNsaWVudHMge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjNlYjtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuYWNoaWV2ZWRQcm9maWxlLnN0dWRlbnRzIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2VmZmU7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmFjaGlldmVkUHJvZmlsZS50ZXN0cyB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlYmYwO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5hY2hpZXZlZFByb2ZpbGUucXVlc3Rpb25zIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYmZmZWY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodCB7XFxuICBmb250LXNpemU6IDM2cHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQucXVlc3Rpb25zSGlnaGxpZ2h0IHtcXG4gIGNvbG9yOiAjMDBhYzI2O1xcbn1cXG5cXG4uYWNoaWV2ZWRDb250YWluZXIgLmFjaGlldmVkUm93IC5jYXJkIC5oaWdobGlnaHQudGVzdHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmMzY7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLmhpZ2hsaWdodC5zdHVkZW50c0hpZ2hsaWdodCB7XFxuICBjb2xvcjogIzhjMDBmZTtcXG59XFxuXFxuLmFjaGlldmVkQ29udGFpbmVyIC5hY2hpZXZlZFJvdyAuY2FyZCAuaGlnaGxpZ2h0LmNsaWVudHNIaWdobGlnaHQge1xcbiAgY29sb3I6ICNmNjA7XFxufVxcblxcbi5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cgLmNhcmQgLnN1YlRleHQge1xcbiAgZm9udC1zaXplOiAyNHB4O1xcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi8qIC50b2dnbGVBdFJpZ2h0IHtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogNTZweCA2NHB4IDAgNjRweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcXG59XFxuXFxuLnRvZ2dsZUF0TGVmdCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDU2cHggNjRweCAwIDY0cHg7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG59ICovXFxuXFxuLnNlY3Rpb25fY29udGFpbmVyIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgcGFkZGluZzogMTYwcHggMzJweCAxNjBweCA2NHB4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBtYXJnaW46IGF1dG87XFxufVxcblxcbi5zZWN0aW9uX2NvbnRhaW5lciAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIH1cXG5cXG4uc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSB7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcXG4gIHBhZGRpbmc6IDE2MHB4IDMycHggMTYwcHggNjRweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWFyZ2luOiBhdXRvO1xcbn1cXG5cXG4uc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgfVxcblxcbi5pbWFnZVBhcnQge1xcbiAgd2lkdGg6IDUwJTtcXG59XFxuXFxuLmltYWdlUGFydCAuZW1wdHlDYXJkIHtcXG4gIHdpZHRoOiA2MDZweDtcXG4gIGhlaWdodDogMzQxcHg7XFxuICBib3JkZXItcmFkaXVzOiA4cHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcblxcbi5pbWFnZVBhcnQgLnRvcENhcmQge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgei1pbmRleDogMTtcXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcXG59XFxuXFxuLmltYWdlUGFydCAuYm90dG9tQ2lyY2xlIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA4OHB4O1xcbiAgaGVpZ2h0OiA4OHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC50b3BDaXJjbGUge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDg4cHg7XFxuICBoZWlnaHQ6IDg4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxufVxcblxcbi5pbWFnZVBhcnQub25saW5lVGVzdHMgLnRvcENpcmNsZSB7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDUwcHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlNWZlO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0Lm9ubGluZVRlc3RzIC5ib3R0b21DaXJjbGUge1xcbiAgYm90dG9tOiAtNDRweDtcXG4gIGxlZnQ6IDc5cHg7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbn1cXG5cXG4uaW1hZ2VQYXJ0Lm93blBhcGVycyAudG9wQ2lyY2xlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICBvcGFjaXR5OiAwLjM7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDcwcHg7XFxufVxcblxcbi5pbWFnZVBhcnQub3duUGFwZXJzIC5ib3R0b21DaXJjbGUge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gIGJvdHRvbTogLTQ0cHg7XFxuICBsZWZ0OiA0MHB4O1xcbn1cXG5cXG4uaW1hZ2VQYXJ0LnF1ZXN0aW9uQmFuayAudG9wQ2lyY2xlIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDc2ZmY7XFxuICBvcGFjaXR5OiAwLjI7XFxuICB0b3A6IC00NHB4O1xcbiAgcmlnaHQ6IDcwcHg7XFxufVxcblxcbi5pbWFnZVBhcnQucXVlc3Rpb25CYW5rIC5ib3R0b21DaXJjbGUge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlYjU0NjtcXG4gIGJvdHRvbTogLTQ0cHg7XFxuICBsZWZ0OiA0MHB4O1xcbiAgb3BhY2l0eTogMC4yO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XFxuICAgICAgZmxleC13cmFwOiB3cmFwO1xcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5pbWFnZVBhcnQgLnZpZGVvQ29udGVudHMgLnZpZGVvX3NlY3Rpb24ge1xcbiAgd2lkdGg6IDQ1JTtcXG4gIG1hcmdpbi1ib3R0b206IDU2cHg7XFxufVxcblxcbi5pbWFnZVBhcnQgLnZpZGVvQ29udGVudHMgLnZpZGVvX3NlY3Rpb24gLmNvbnRlbnRfdGl0bGUge1xcbiAgZm9udC1zaXplOiAyMHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBsaW5lLWhlaWdodDogMzBweDtcXG4gIG1hcmdpbi1ib3R0b206IDhweDtcXG4gIGNvbG9yOiAjMjUyODJiO1xcbn1cXG5cXG4uaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIC52aWRlb19zZWN0aW9uIC5jb250ZW50X3RleHQge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBjb2xvcjogIzI1MjgyYjtcXG59XFxuXFxuLyogLmFsbGN1c3RvbWVycyBwIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn0gKi9cXG5cXG4uY29udGVudCBwIHAge1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICBsZXR0ZXItc3BhY2luZzogMC40OHB4O1xcbiAgY29sb3I6ICMyNTI4MmI7XFxuICBvcGFjaXR5OiAwLjY7XFxuICBtYXJnaW4tYm90dG9tOiA4cHg7XFxufVxcblxcbi5oaWRlIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufVxcblxcbi5zaG93IHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcblxcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XFxuICAuYXZhaWxhYmxlQ29udGFpbmVyIHtcXG4gICAgcGFkZGluZzogNXJlbSA2NHB4IDAgNjRweDtcXG4gIH1cXG59XFxuXFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTBweCkge1xcbiAgLnRlc3RzSW1hZ2Uge1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAuc2VjdGlvbl90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xcbiAgfVxcblxcbiAgLnBvaW50IHtcXG4gICAgbWFyZ2luLWJvdHRvbTogOHB4O1xcbiAgfVxcblxcbiAgLnJlZERvdCB7XFxuICAgIG1hcmdpbi10b3A6IDhweDtcXG4gIH1cXG5cXG4gIC5wb2ludFRleHQge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAuYm90dG9tQ2lyY2xlIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAudG9wQ2lyY2xlIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogNDhweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydC5vbmxpbmVUZXN0cyAudG9wQ2lyY2xlIHtcXG4gICAgdG9wOiAtMjRweDtcXG4gICAgcmlnaHQ6IDUwcHg7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmU1ZmU7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0Lm9ubGluZVRlc3RzIC5ib3R0b21DaXJjbGUge1xcbiAgICBib3R0b206IC0yNHB4O1xcbiAgICBsZWZ0OiA3OXB4O1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlMGU4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydC5vd25QYXBlcnMgLnRvcENpcmNsZSB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzZmM7XFxuICAgIG9wYWNpdHk6IDAuMztcXG4gICAgdG9wOiAtMjRweDtcXG4gICAgcmlnaHQ6IDcwcHg7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0Lm93blBhcGVycyAuYm90dG9tQ2lyY2xlIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTBlODtcXG4gICAgYm90dG9tOiAtMjRweDtcXG4gICAgbGVmdDogNDBweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQucXVlc3Rpb25CYW5rIC50b3BDaXJjbGUge1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3NmZmO1xcbiAgICBvcGFjaXR5OiAwLjI7XFxuICAgIHRvcDogLTI0cHg7XFxuICAgIHJpZ2h0OiA3MHB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydC5xdWVzdGlvbkJhbmsgLmJvdHRvbUNpcmNsZSB7XFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWI1NDY7XFxuICAgIGJvdHRvbTogLTI0cHg7XFxuICAgIGxlZnQ6IDQwcHg7XFxuICAgIG9wYWNpdHk6IDAuMjtcXG4gIH1cXG5cXG4gIC50ZWFjaENvbnRhaW5lciAuZG93bmxvYWRBcHAge1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLnNlY3Rpb25fY29udGFpbmVyIHtcXG4gICAgbWluLWhlaWdodDogMjByZW07XFxuICAgIHBhZGRpbmc6IDJyZW0gMXJlbTtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lciAubWF4Q29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lciAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIG1hcmdpbi10b3A6IDMycHg7XFxuICAgIG1heC1oZWlnaHQ6IG5vbmU7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlIHtcXG4gICAgcGFkZGluZzogMjRweCAyMHB4IDQ4cHg7XFxuICB9XFxuXFxuICAuc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZSAuc2VjdGlvbl9jb250ZW50cyB7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIH1cXG5cXG4gIC5jb250ZW50UGFydCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5jb250ZW50UGFydCA+IGRpdjpudGgtY2hpbGQoMikge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiA0MDBweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLmNvbnRlbnRQYXJ0IC5jb250ZW50IHtcXG4gICAgbWF4LXdpZHRoOiBub25lO1xcbiAgfVxcblxcbiAgLmNvbnRlbnRQYXJ0IC5jb250ZW50IC50ZXh0Y29udGVudCB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWF4LXdpZHRoOiA2MDBweDtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgZm9udC1zaXplOiAxNHB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgbWFyZ2luOiBhdXRvO1xcbiAgfVxcblxcbiAgLyogLmNvbnRlbnRQYXJ0IC5jb250ZW50IC50ZXh0Y29udGVudCAucG9pbnQgLnJlZGRvdHdyYXBwZXIge1xcbiAgICBtYXJnaW4tdG9wOiA5cHg7XFxuICB9XFxuXFxuICAuY29udGVudFBhcnQgLmNvbnRlbnQgLnRleHRjb250ZW50IC5wb2ludCAucG9pbnRUZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gIH0gKi9cXG5cXG4gIC5jb250ZW50UGFydCAuY29udGVudCAuc2VjdGlvbl90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMjRweDtcXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAuZW1wdHlDYXJkIHtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIG1heC13aWR0aDogMzYwcHg7XFxuICAgIGhlaWdodDogMTgwcHg7XFxuICAgIG1hcmdpbjogMCBhdXRvIDMycHg7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAudmlkZW9Db250ZW50cyAudmlkZW9fc2VjdGlvbiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgfVxcblxcbiAgLmltYWdlUGFydCAudmlkZW9Db250ZW50cyAudmlkZW9fc2VjdGlvbjpsYXN0LWNoaWxkIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gIH1cXG5cXG4gIC5pbWFnZVBhcnQgLnZpZGVvQ29udGVudHMgLnZpZGVvX3NlY3Rpb24gLmNvbnRlbnRfdGl0bGUge1xcbiAgICBmb250LXNpemU6IDE2cHg7XFxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XFxuICB9XFxuXFxuICAuaW1hZ2VQYXJ0IC52aWRlb0NvbnRlbnRzIC52aWRlb19zZWN0aW9uIC5jb250ZW50X3RleHQge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLm1vYmlsZVBhcGVyc0NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICB9XFxuXFxuICAubW9iaWxlUGFwZXJzQ29udGFpbmVyIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIG1heC13aWR0aDogMzYwcHg7XFxuICAgIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XFxuICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XFxuICB9XFxuXFxuICAucGFwZXJzV3JhcHBlciB7XFxuICAgIG1heC13aWR0aDogMzYwcHg7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5yZXBvcnRzX3N1Yl90aXRsZSB7XFxuICAgIGZvbnQtc2l6ZTogMTZweDtcXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICB9XFxuXFxuICAucmVwb3J0c19ub3RpY2Uge1xcbiAgICBmb250LXNpemU6IDE0cHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgfVxcblxcbiAgLnN1Yl9zZWN0aW9uX3RpdGxlIHtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC5oZWFkZXJiYWNrZ3JvdW5kbW9iaWxlIHtcXG4gICAgd2lkdGg6IDIzMnB4O1xcbiAgICBoZWlnaHQ6IDMzNXB4O1xcbiAgICByaWdodDogMDtcXG4gICAgbGVmdDogMCU7XFxuICAgIGJvdHRvbTogLTFyZW07XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBtaW4taGVpZ2h0OiAyN3JlbTtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyBoMyB7XFxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xcbiAgICBtYXJnaW46IGF1dG87XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcXG4gICAgbWF4LXdpZHRoOiAxNC44NzVyZW07XFxuICB9XFxuXFxuICAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIHtcXG4gICAgcGFkZGluZzogMS41cmVtIDAgMDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICBtYXJnaW46IDAgYXV0bztcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiByZXBlYXQoMiwgMWZyKTtcXG4gICAgZ2FwOiAxcmVtO1xcbiAgICB3aWR0aDogLXdlYmtpdC1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IC1tb3otZml0LWNvbnRlbnQ7XFxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcXG4gIH1cXG5cXG4gIC5kaXNwbGF5Q2xpZW50cyAuY2xpZW50c1dyYXBwZXIgLmNsaWVudCB7XFxuICAgIHdpZHRoOiA5Ljc1cmVtO1xcbiAgICBoZWlnaHQ6IDdyZW07XFxuICB9XFxuXFxuICAvKiAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5jbGllbnQuYWN0aXZlIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICB9ICovXFxuXFxuICAuZGlzcGxheUNsaWVudHMgc3BhbiB7XFxuICAgIG1heC13aWR0aDogMzA3cHg7XFxuICB9XFxuXFxuICAvKiAuZGlzcGxheUNsaWVudHMgLmNsaWVudHNXcmFwcGVyIC5kb3RzIHtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gIH0gKi9cXG4gIC5hY2hpZXZlZENvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDEuNXJlbSAxcmVtO1xcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJy9pbWFnZXMvaG9tZS9Db25mZXR0aS5zdmcnKTtcXG4gIH1cXG5cXG4gIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHtcXG4gICAgZm9udC1zaXplOiAxLjVyZW07XFxuICAgIHBhZGRpbmc6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcXG4gIH1cXG5cXG4gIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRIZWFkaW5nIHNwYW46OmFmdGVyIHtcXG4gICAgY29udGVudDogJ1xcXFxhJztcXG4gICAgd2hpdGUtc3BhY2U6IHByZTtcXG4gIH1cXG5cXG4gIC5hY2hpZXZlZENvbnRhaW5lciAuYWNoaWV2ZWRSb3cge1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgZ2FwOiAwLjc1cmVtO1xcbiAgICBtYXgtd2lkdGg6IDIwLjVyZW07XFxuICAgIHdpZHRoOiAtd2Via2l0LWZpdC1jb250ZW50O1xcbiAgICB3aWR0aDogLW1vei1maXQtY29udGVudDtcXG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgICBtYXJnaW46IGF1dG87XFxuICB9XFxuXFxuICAvKiAuY3VzdG9tZXJzX2NvbnRhaW5lciB7XFxuICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuXFxuICAgIC5jdXN0b21lcl9yZXZpZXcge1xcbiAgICAgIG9yZGVyOiAyO1xcbiAgICAgIGRpc3BsYXk6IGZsZXg7XFxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICB3aWR0aDogMTAwJTtcXG4gICAgICBoZWlnaHQ6IDUwJTtcXG4gICAgICBwYWRkaW5nOiAzMnB4IDE2cHg7XFxuXFxuICAgICAgLmN1c3RvbWVyTG9nbyB7XFxuICAgICAgICB3aWR0aDogODhweDtcXG4gICAgICAgIGhlaWdodDogNTZweDtcXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMnB4O1xcbiAgICAgIH1cXG5cXG4gICAgICAuc3JpQ2hhaXRhbnlhVGV4dCB7XFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIG1heC13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLmF1dGhvcldyYXBwZXIge1xcbiAgICAgICAgLmFib3V0X2F1dGhvciB7XFxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDM0cHg7XFxuICAgICAgICB9XFxuICAgICAgfVxcbiAgICB9XFxuICB9ICovXFxuXFxuICAudGVhY2hDb250YWluZXIgLm1heENvbnRhaW5lciB7XFxuICAgIHBvc2l0aW9uOiBzdGF0aWM7XFxuICAgIG1heC13aWR0aDogNDUwcHg7XFxuICB9XFxuXFxuICAudGVhY2hDb250YWluZXIgLmJyZWFkY3J1bSB7XFxuICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC50ZWFjaENvbnRhaW5lciB7XFxuICAgIGhlaWdodDogNjhyZW07XFxuICAgIHBhZGRpbmc6IDMycHggMTZweDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLmhlYWRpbmcge1xcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcXG4gICAgICBsaW5lLWhlaWdodDogNDhweDtcXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgICAgbWF4LXdpZHRoOiBub25lO1xcbiAgICAgIG1hcmdpbjogMzJweCBhdXRvIDAgYXV0bztcXG4gICAgfVxcblxcbiAgICAgIC50ZWFjaENvbnRhaW5lciAuaGVhZGluZyAubGVhcm5pbmdUZXh0IHtcXG4gICAgICAgIHdpZHRoOiAyMTBweDtcXG4gICAgICB9XFxuXFxuICAgICAgICAudGVhY2hDb250YWluZXIgLmhlYWRpbmcgLmxlYXJuaW5nVGV4dCBpbWcge1xcbiAgICAgICAgICBsZWZ0OiAwO1xcbiAgICAgICAgICBtaW4td2lkdGg6IDE0MHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLmNvbnRlbnRUZXh0IHtcXG4gICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7XFxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgIG1heC13aWR0aDogNjUwcHg7XFxuICAgICAgbWFyZ2luOiAxNnB4IGF1dG8gMCBhdXRvO1xcbiAgICB9XFxuXFxuICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciB7XFxuICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgbWFyZ2luLXRvcDogNDhweDtcXG4gICAgfVxcblxcbiAgICAgIC50ZWFjaENvbnRhaW5lciAuYnV0dG9ud3JhcHBlciAucmVxdWVzdERlbW8ge1xcbiAgICAgICAgcGFkZGluZzogOHB4IDE2cHg7XFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgIG1pbi13aWR0aDogbm9uZTtcXG4gICAgICB9XFxuXFxuICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIge1xcbiAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC5idXR0b253cmFwcGVyIC53aGF0c2FwcHdyYXBwZXIgLndoYXRzYXBwIHtcXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gICAgICAgICAgb3BhY2l0eTogMTtcXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XFxuICAgICAgICB9XFxuXFxuICAgICAgICAudGVhY2hDb250YWluZXIgLmJ1dHRvbndyYXBwZXIgLndoYXRzYXBwd3JhcHBlciBpbWcge1xcbiAgICAgICAgICB3aWR0aDogMjRweDtcXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24ge1xcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICAgICAgbWFyZ2luLXRvcDogMDtcXG5cXG4gICAgICAvKiAuZmVhdHVyZXNTZWN0aW9uIHtcXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XFxuICAgICAgfSAqL1xcbiAgICB9XFxuICAgICAgICAudGVhY2hDb250YWluZXIgLnRvcFNlY3Rpb24gLnRlYWNoU2VjdGlvbiAudGVhY2hJbWdCb3gge1xcbiAgICAgICAgICB3aWR0aDogMzJweDtcXG4gICAgICAgICAgaGVpZ2h0OiAzMnB4O1xcbiAgICAgICAgfVxcblxcbiAgICAgICAgLnRlYWNoQ29udGFpbmVyIC50b3BTZWN0aW9uIC50ZWFjaFNlY3Rpb24gLnRlYWNoU2VjdGlvbk5hbWUge1xcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XFxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xcbiAgICAgICAgfVxcblxcbiAgLmF2YWlsYWJsZUNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmc6IDMycHggMjhweDtcXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIH1cXG5cXG4gICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IHtcXG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmcjtcXG4gICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuYXZhaWxhYmxlVGl0bGUge1xcbiAgICAgICAgZm9udC1zaXplOiAzMnB4O1xcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICB9XFxuXFxuICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cge1xcbiAgICAgICAgbWFyZ2luOiAwO1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICB9XFxuXFxuICAgICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnJvdyAucGxhdGZvcm1Db250YWluZXIge1xcbiAgICAgICAgICB3aWR0aDogNjhweDtcXG4gICAgICAgICAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XFxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XFxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMjRweDtcXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIC5wbGF0Zm9ybSB7XFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMy4zcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5yb3cgLnBsYXRmb3JtQ29udGFpbmVyIC5wbGF0Zm9ybU9zIHtcXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XFxuICAgICAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAucm93IC5wbGF0Zm9ybUNvbnRhaW5lcjpsYXN0LWNoaWxkIHtcXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xcbiAgICAgICAgfVxcblxcbiAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuZGVza3RvcEltYWdlIHtcXG4gICAgICAgIHdpZHRoOiA5MCU7XFxuICAgICAgICBoZWlnaHQ6IDE2MXB4O1xcbiAgICAgICAgbWFyZ2luOiAwIGF1dG8gMCBhdXRvO1xcbiAgICAgIH1cXG5cXG4gICAgICAuYXZhaWxhYmxlQ29udGFpbmVyIC5hdmFpbGFibGVSb3cgLnN0b3JlIHtcXG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMycHg7XFxuICAgICAgfVxcblxcbiAgICAgICAgLmF2YWlsYWJsZUNvbnRhaW5lciAuYXZhaWxhYmxlUm93IC5zdG9yZSAucGxheXN0b3JlIHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMCAxNnB4IDAgMDtcXG4gICAgICAgIH1cXG5cXG4gICAgICAgIC5hdmFpbGFibGVDb250YWluZXIgLmF2YWlsYWJsZVJvdyAuc3RvcmUgLmFwcHN0b3JlIHtcXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xcbiAgICAgICAgICBoZWlnaHQ6IDQycHg7XFxuICAgICAgICAgIG1hcmdpbjogMDtcXG4gICAgICAgIH1cXG5cXG4gIC5qb2luQ29tbXVuaXR5IHtcXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gICAgaGVpZ2h0OiAxNDRweDtcXG4gIH1cXG5cXG4gIC5qb2luRmJUZXh0IHtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgICBmb250LXNpemU6IDIwcHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XFxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XFxuICB9XFxuXFxuICAuam9pbkZiTGluayB7XFxuICAgIHBhZGRpbmc6IDEycHggMzJweDtcXG4gICAgZm9udC1zaXplOiAxNnB4O1xcbiAgICBsaW5lLWhlaWdodDogMjRweDtcXG4gIH1cXG5cXG4gIC52aWRlb1NsaWRlciB7XFxuICAgIHBhZGRpbmc6IDNyZW0gMXJlbTtcXG4gICAgbWluLWhlaWdodDogMzhyZW07XFxuICB9XFxuXFxuICAubWF0aHMge1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGxlZnQ6IDE1JTtcXG4gICAgdG9wOiA4NCU7XFxuICAgIHdpZHRoOiAzNnB4O1xcbiAgICBoZWlnaHQ6IDM2cHg7XFxuICB9XFxuXFxuICAubWljcm9zY29wZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA4NyU7XFxuICAgIGxlZnQ6IDQ1JTtcXG4gICAgd2lkdGg6IDQwcHg7XFxuICAgIGhlaWdodDogNDhweDtcXG4gIH1cXG5cXG4gIC50cmlhbmdsZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA4NSU7XFxuICAgIGxlZnQ6IDgwJTtcXG4gICAgd2lkdGg6IDM2cHg7XFxuICAgIGhlaWdodDogMzBweDtcXG4gIH1cXG5cXG4gIC5zY2FsZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgcmlnaHQ6IDgwJTtcXG4gICAgdG9wOiAxMCU7XFxuICAgIHdpZHRoOiA0MHB4O1xcbiAgICBoZWlnaHQ6IDMwcHg7XFxuICB9XFxuXFxuICAuY2lyY2xlIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDMlO1xcbiAgICByaWdodDogNTAlO1xcbiAgICB3aWR0aDogNDBweDtcXG4gICAgaGVpZ2h0OiAzMHB4O1xcbiAgfVxcblxcbiAgLmNoZW1pc3RyeSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA4JTtcXG4gICAgcmlnaHQ6IDUlO1xcbiAgICB3aWR0aDogMzZweDtcXG4gICAgaGVpZ2h0OiA0OHB4O1xcbiAgfVxcblxcbiAgLnNjcm9sbFRvcCB7XFxuICAgIHdpZHRoOiAzMnB4O1xcbiAgICBoZWlnaHQ6IDMycHg7XFxuICAgIHJpZ2h0OiAxNnB4O1xcbiAgICBib3R0b206IDE2cHg7XFxuICB9XFxufVxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJtYXhDb250YWluZXJcIjogXCJUZXN0LW1heENvbnRhaW5lci0xX1FDT1wiLFxuXHRcIm1hcmdpbkJvdHRvbTEyNlwiOiBcIlRlc3QtbWFyZ2luQm90dG9tMTI2LTJ5LVRKXCIsXG5cdFwicmVwb3J0c19zdWJfdGl0bGVcIjogXCJUZXN0LXJlcG9ydHNfc3ViX3RpdGxlLTF6VTRWXCIsXG5cdFwicmVwb3J0c19ub3RpY2VcIjogXCJUZXN0LXJlcG9ydHNfbm90aWNlLTFrN3hZXCIsXG5cdFwicm93MVwiOiBcIlRlc3Qtcm93MS0xcEJJWlwiLFxuXHRcInBvaW50Q29udGFpbmVyXCI6IFwiVGVzdC1wb2ludENvbnRhaW5lci15bDZyVVwiLFxuXHRcInBvaW50XCI6IFwiVGVzdC1wb2ludC0yeUhNQlwiLFxuXHRcInBvaW50VGV4dFwiOiBcIlRlc3QtcG9pbnRUZXh0LTNvbXk5XCIsXG5cdFwicmVkRG90XCI6IFwiVGVzdC1yZWREb3QtM21LSC1cIixcblx0XCJ0ZXN0c0ltYWdlXCI6IFwiVGVzdC10ZXN0c0ltYWdlLTNaMmRlXCIsXG5cdFwic2VjdGlvbl9jb250YWluZXJcIjogXCJUZXN0LXNlY3Rpb25fY29udGFpbmVyLTJ3QTVtXCIsXG5cdFwiYXNoQmFja2dyb3VuZFwiOiBcIlRlc3QtYXNoQmFja2dyb3VuZC1xMUUtM1wiLFxuXHRcInZpZGVvU2xpZGVyXCI6IFwiVGVzdC12aWRlb1NsaWRlci0yOF91Z1wiLFxuXHRcIm1hdGhzXCI6IFwiVGVzdC1tYXRocy1UdWNFVVwiLFxuXHRcIm1pY3Jvc2NvcGVcIjogXCJUZXN0LW1pY3Jvc2NvcGUtSHNhdEdcIixcblx0XCJ0cmlhbmdsZVwiOiBcIlRlc3QtdHJpYW5nbGUtMkJVS1lcIixcblx0XCJzY2FsZVwiOiBcIlRlc3Qtc2NhbGUtMWM5STRcIixcblx0XCJjaXJjbGVcIjogXCJUZXN0LWNpcmNsZS0yalcxd1wiLFxuXHRcImNoZW1pc3RyeVwiOiBcIlRlc3QtY2hlbWlzdHJ5LTJlWU9XXCIsXG5cdFwiaGVhZGVyYmFja2dyb3VuZG1vYmlsZVwiOiBcIlRlc3QtaGVhZGVyYmFja2dyb3VuZG1vYmlsZS0zRk4tNlwiLFxuXHRcInNlY3Rpb25fdGl0bGVcIjogXCJUZXN0LXNlY3Rpb25fdGl0bGUtMXU2dDBcIixcblx0XCJzdWJfc2VjdGlvbl90aXRsZVwiOiBcIlRlc3Qtc3ViX3NlY3Rpb25fdGl0bGUtM1ZSdTNcIixcblx0XCJjb250ZW50UGFydFwiOiBcIlRlc3QtY29udGVudFBhcnQtdk94WW1cIixcblx0XCJjb250ZW50XCI6IFwiVGVzdC1jb250ZW50LTJoczlzXCIsXG5cdFwidGV4dGNvbnRlbnRcIjogXCJUZXN0LXRleHRjb250ZW50LTNyV2YxXCIsXG5cdFwiYXZhaWxhYmxlQ29udGFpbmVyXCI6IFwiVGVzdC1hdmFpbGFibGVDb250YWluZXItM3hMVDJcIixcblx0XCJhdmFpbGFibGVSb3dcIjogXCJUZXN0LWF2YWlsYWJsZVJvdy0yUXluY1wiLFxuXHRcImRlc2t0b3BJbWFnZVwiOiBcIlRlc3QtZGVza3RvcEltYWdlLTJrWE5YXCIsXG5cdFwiam9pbkNvbW11bml0eVwiOiBcIlRlc3Qtam9pbkNvbW11bml0eS0zTEJVc1wiLFxuXHRcImpvaW5GYlRleHRcIjogXCJUZXN0LWpvaW5GYlRleHQtMWZkaE1cIixcblx0XCJqb2luRmJMaW5rXCI6IFwiVGVzdC1qb2luRmJMaW5rLUF2Y3NmXCIsXG5cdFwiYXZhaWxhYmxlVGl0bGVcIjogXCJUZXN0LWF2YWlsYWJsZVRpdGxlLTNORWZBXCIsXG5cdFwiYXZhaWxhYmxlXCI6IFwiVGVzdC1hdmFpbGFibGUtM2xXTFhcIixcblx0XCJhdmFpbGFibGVDb250ZW50U2VjdGlvblwiOiBcIlRlc3QtYXZhaWxhYmxlQ29udGVudFNlY3Rpb24tVVA0bG5cIixcblx0XCJwbGF0Zm9ybUNvbnRhaW5lclwiOiBcIlRlc3QtcGxhdGZvcm1Db250YWluZXItMk9obnRcIixcblx0XCJwbGF0Zm9ybVwiOiBcIlRlc3QtcGxhdGZvcm0ta094QU1cIixcblx0XCJwbGF0Zm9ybU9zXCI6IFwiVGVzdC1wbGF0Zm9ybU9zLTM2UGtHXCIsXG5cdFwicm93XCI6IFwiVGVzdC1yb3ctc0UtQ29cIixcblx0XCJzdG9yZVwiOiBcIlRlc3Qtc3RvcmUtM0N6aXpcIixcblx0XCJwbGF5c3RvcmVcIjogXCJUZXN0LXBsYXlzdG9yZS0zUEtPQ1wiLFxuXHRcImFwcHN0b3JlXCI6IFwiVGVzdC1hcHBzdG9yZS0xaE8xNlwiLFxuXHRcInNjcm9sbFRvcFwiOiBcIlRlc3Qtc2Nyb2xsVG9wLTE0UzhDXCIsXG5cdFwidGVhY2hDb250YWluZXJcIjogXCJUZXN0LXRlYWNoQ29udGFpbmVyLTRsTE9vXCIsXG5cdFwiaGVhZGluZ1wiOiBcIlRlc3QtaGVhZGluZy0xb05zNVwiLFxuXHRcImxlYXJuaW5nVGV4dFwiOiBcIlRlc3QtbGVhcm5pbmdUZXh0LTFQQlgzXCIsXG5cdFwiaW1hZ2VQYXJ0XCI6IFwiVGVzdC1pbWFnZVBhcnQtRnVtd1lcIixcblx0XCJlbXB0eUNhcmRcIjogXCJUZXN0LWVtcHR5Q2FyZC0yTXhIQVwiLFxuXHRcInRvcENhcmRcIjogXCJUZXN0LXRvcENhcmQtM0FfTm9cIixcblx0XCJjb250ZW50VGV4dFwiOiBcIlRlc3QtY29udGVudFRleHQtMkhwNnlcIixcblx0XCJidXR0b253cmFwcGVyXCI6IFwiVGVzdC1idXR0b253cmFwcGVyLWttcnBSXCIsXG5cdFwicmVxdWVzdERlbW9cIjogXCJUZXN0LXJlcXVlc3REZW1vLUV5UEhGXCIsXG5cdFwid2hhdHNhcHB3cmFwcGVyXCI6IFwiVGVzdC13aGF0c2FwcHdyYXBwZXItM0RtVEpcIixcblx0XCJ3aGF0c2FwcFwiOiBcIlRlc3Qtd2hhdHNhcHAtMy1oSXJcIixcblx0XCJkb3dubG9hZEFwcFwiOiBcIlRlc3QtZG93bmxvYWRBcHAtaDRkQ0lcIixcblx0XCJkb3dubG9hZFRleHRcIjogXCJUZXN0LWRvd25sb2FkVGV4dC0yS0x1QVwiLFxuXHRcInBsYXlTdG9yZUljb25cIjogXCJUZXN0LXBsYXlTdG9yZUljb24tM3ZiRktcIixcblx0XCJicmVhZGNydW1cIjogXCJUZXN0LWJyZWFkY3J1bS0xajE1RVwiLFxuXHRcImRpc3BsYXlDbGllbnRzXCI6IFwiVGVzdC1kaXNwbGF5Q2xpZW50cy0xOGpyNVwiLFxuXHRcInRvcFNlY3Rpb25cIjogXCJUZXN0LXRvcFNlY3Rpb24tMjFiVjNcIixcblx0XCJ0ZWFjaFNlY3Rpb25cIjogXCJUZXN0LXRlYWNoU2VjdGlvbi1nbmtGdVwiLFxuXHRcInRlYWNoSW1nQm94XCI6IFwiVGVzdC10ZWFjaEltZ0JveC0zMnRQUFwiLFxuXHRcInRlYWNoU2VjdGlvbk5hbWVcIjogXCJUZXN0LXRlYWNoU2VjdGlvbk5hbWUtMWZBQXJcIixcblx0XCJjbGllbnRzV3JhcHBlclwiOiBcIlRlc3QtY2xpZW50c1dyYXBwZXItMmZEZVBcIixcblx0XCJjbGllbnRcIjogXCJUZXN0LWNsaWVudC1GemRCWlwiLFxuXHRcImFjaGlldmVkQ29udGFpbmVyXCI6IFwiVGVzdC1hY2hpZXZlZENvbnRhaW5lci0xRnJPRlwiLFxuXHRcImFjaGlldmVkSGVhZGluZ1wiOiBcIlRlc3QtYWNoaWV2ZWRIZWFkaW5nLXJ0ZGVPXCIsXG5cdFwiYWNoaWV2ZWRSb3dcIjogXCJUZXN0LWFjaGlldmVkUm93LTFsQ1FmXCIsXG5cdFwiY2FyZFwiOiBcIlRlc3QtY2FyZC1UWlpjUlwiLFxuXHRcImFjaGlldmVkUHJvZmlsZVwiOiBcIlRlc3QtYWNoaWV2ZWRQcm9maWxlLTI0N2hYXCIsXG5cdFwiY2xpZW50c1wiOiBcIlRlc3QtY2xpZW50cy0ycC1jWFwiLFxuXHRcInN0dWRlbnRzXCI6IFwiVGVzdC1zdHVkZW50cy0zQzdydVwiLFxuXHRcInRlc3RzXCI6IFwiVGVzdC10ZXN0cy0yZ3MxN1wiLFxuXHRcInF1ZXN0aW9uc1wiOiBcIlRlc3QtcXVlc3Rpb25zLTN2V1dXXCIsXG5cdFwiaGlnaGxpZ2h0XCI6IFwiVGVzdC1oaWdobGlnaHQtUnVhWVFcIixcblx0XCJxdWVzdGlvbnNIaWdobGlnaHRcIjogXCJUZXN0LXF1ZXN0aW9uc0hpZ2hsaWdodC0yX1ZOUVwiLFxuXHRcInRlc3RzSGlnaGxpZ2h0XCI6IFwiVGVzdC10ZXN0c0hpZ2hsaWdodC0yYkQxZlwiLFxuXHRcInN0dWRlbnRzSGlnaGxpZ2h0XCI6IFwiVGVzdC1zdHVkZW50c0hpZ2hsaWdodC0zcDRjSlwiLFxuXHRcImNsaWVudHNIaWdobGlnaHRcIjogXCJUZXN0LWNsaWVudHNIaWdobGlnaHQteUNsRG9cIixcblx0XCJzdWJUZXh0XCI6IFwiVGVzdC1zdWJUZXh0LTJROFJzXCIsXG5cdFwic2VjdGlvbl9jb250ZW50c1wiOiBcIlRlc3Qtc2VjdGlvbl9jb250ZW50cy0yV0FTV1wiLFxuXHRcInNlY3Rpb25fY29udGFpbmVyX3JldmVyc2VcIjogXCJUZXN0LXNlY3Rpb25fY29udGFpbmVyX3JldmVyc2UtMUlLRm9cIixcblx0XCJib3R0b21DaXJjbGVcIjogXCJUZXN0LWJvdHRvbUNpcmNsZS0yU0VDT1wiLFxuXHRcInRvcENpcmNsZVwiOiBcIlRlc3QtdG9wQ2lyY2xlLTJCMkhQXCIsXG5cdFwib25saW5lVGVzdHNcIjogXCJUZXN0LW9ubGluZVRlc3RzLTJpNGx1XCIsXG5cdFwib3duUGFwZXJzXCI6IFwiVGVzdC1vd25QYXBlcnMtMUd5MHJcIixcblx0XCJxdWVzdGlvbkJhbmtcIjogXCJUZXN0LXF1ZXN0aW9uQmFuay1sLTZtTlwiLFxuXHRcInZpZGVvQ29udGVudHNcIjogXCJUZXN0LXZpZGVvQ29udGVudHMtcFFxd0NcIixcblx0XCJ2aWRlb19zZWN0aW9uXCI6IFwiVGVzdC12aWRlb19zZWN0aW9uLTJDMlBEXCIsXG5cdFwiY29udGVudF90aXRsZVwiOiBcIlRlc3QtY29udGVudF90aXRsZS0xRE92WFwiLFxuXHRcImNvbnRlbnRfdGV4dFwiOiBcIlRlc3QtY29udGVudF90ZXh0LVFLRkhxXCIsXG5cdFwiaGlkZVwiOiBcIlRlc3QtaGlkZS0zdlBRVVwiLFxuXHRcInNob3dcIjogXCJUZXN0LXNob3ctTFRPSG9cIixcblx0XCJtb2JpbGVQYXBlcnNDb250YWluZXJcIjogXCJUZXN0LW1vYmlsZVBhcGVyc0NvbnRhaW5lci0xR1BPdVwiLFxuXHRcInBhcGVyc1dyYXBwZXJcIjogXCJUZXN0LXBhcGVyc1dyYXBwZXItMWFJTmRcIlxufTsiLCJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZVJlZiwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXIvbGliL3dpdGhTdHlsZXMnO1xuaW1wb3J0IFJlYWN0UGxheWVyIGZyb20gJ3JlYWN0LXBsYXllcic7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHMgZnJvbSAnLi9TbGlkZXJQbGF5ZXIuc2Nzcyc7XG5cbmNvbnN0IG1vdmUgPSB7XG4gIDA6IHMuc2xpZGVPdXQsXG4gIDE6IHMuc2xpZGVPdXQxLFxuICAyOiBzLnNsaWRlT3V0MixcbiAgMzogcy5zbGlkZU91dDMsXG59O1xuXG5jb25zdCBTbGlkZXJQbGF5ZXIgPSBwcm9wcyA9PiB7XG4gIGNvbnN0IGdldElubmVyV2lkdGggPSAoKSA9PiB7XG4gICAgY29uc3QgaXNXaW5kb3dBdmFpbGFibGUgPSB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJztcbiAgICBpZiAoaXNXaW5kb3dBdmFpbGFibGUpIHtcbiAgICAgIHJldHVybiB3aW5kb3cuaW5uZXJXaWR0aDtcbiAgICB9XG4gICAgcmV0dXJuIDEwMDA7XG4gIH07XG5cbiAgY29uc3QgW2FjdGl2ZSwgc2V0QWN0aXZlXSA9IHVzZVN0YXRlKDApO1xuICBjb25zdCBbc2Vla1ZhbHVlLCBzZXRTZWVrVmFsdWVdID0gdXNlU3RhdGUoMCk7XG4gIGNvbnN0IFtpc01vYmlsZSwgc2V0SXNNb2JpbGVdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBwbGF5ZXJSZWYgPSB1c2VSZWYobnVsbCk7XG4gIGNvbnN0IFt2aWRlb3MsIHNldFZpZGVvc10gPSB1c2VTdGF0ZShbLi4ucHJvcHMudmlkZW9zTGlzdF0pOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gIGNvbnN0IHsgdmlkZW9zTGlzdCB9ID0gcHJvcHM7XG4gIGNvbnN0IGFjdGl2ZVZpZGVvT2JqID0gaXNNb2JpbGUgPyB2aWRlb3NbYWN0aXZlXSA6IHZpZGVvc0xpc3RbYWN0aXZlXTtcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGNvbnN0IHJlc2l6ZSA9ICgpID0+IHtcbiAgICAgIHNldElzTW9iaWxlKGdldElubmVyV2lkdGgoKSA8IDk5MSk7XG4gICAgfTtcbiAgICByZXNpemUoKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgcmVzaXplKTtcbiAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHJlc2l6ZSk7XG4gICAgfTtcbiAgfSwgW10pO1xuXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Mucm9vdH0+XG4gICAgICA8UmVhY3RQbGF5ZXJcbiAgICAgICAgcmVmPXtwbGF5ZXJSZWZ9XG4gICAgICAgIHdpZHRoPXtpc01vYmlsZSA/ICcxMDAlJyA6ICc1MDBweCd9XG4gICAgICAgIGhlaWdodD17aXNNb2JpbGUgPyAnMjY3cHgnIDogJzQwMHB4J31cbiAgICAgICAgc3R5bGU9e3MudmlkZW99XG4gICAgICAgIHVybD17YWN0aXZlVmlkZW9PYmoudXJsfVxuICAgICAgICBjb250cm9scz17ZmFsc2V9XG4gICAgICAgIGNvbmZpZz17e1xuICAgICAgICAgIGZpbGU6IHtcbiAgICAgICAgICAgIGF0dHJpYnV0ZXM6IHtcbiAgICAgICAgICAgICAgYXV0b1BsYXk6IHRydWUsXG4gICAgICAgICAgICAgIG11dGVkOiB0cnVlLFxuICAgICAgICAgICAgICBkaXNhYmxlUGljdHVyZUluUGljdHVyZTogdHJ1ZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfX1cbiAgICAgICAgb25Qcm9ncmVzcz17c3RhdGUgPT4ge1xuICAgICAgICAgIGNvbnN0IHNlZWsgPSBNYXRoLmNlaWwoXG4gICAgICAgICAgICAoc3RhdGUucGxheWVkU2Vjb25kcyAqIDEwMCkgLyBwbGF5ZXJSZWYuY3VycmVudC5nZXREdXJhdGlvbigpLFxuICAgICAgICAgICk7XG4gICAgICAgICAgc2V0U2Vla1ZhbHVlKHNlZWspO1xuICAgICAgICB9fVxuICAgICAgICBvbkVuZGVkPXsoKSA9PiB7XG4gICAgICAgICAgc2V0U2Vla1ZhbHVlKDEwMCk7XG4gICAgICAgICAgaWYgKGlzTW9iaWxlKSB7XG4gICAgICAgICAgICBjb25zdCB0aXRsZXMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKHMudmlkZW9UaXRsZSk7XG4gICAgICAgICAgICAvLyBjb25zdCBsYXN0T25lID0gdmlkZW9zLnNoaWZ0KCk7XG4gICAgICAgICAgICAvLyB2aWRlb3MucHVzaChsYXN0T25lKTtcbiAgICAgICAgICAgIHNldFNlZWtWYWx1ZSgwKTtcbiAgICAgICAgICAgIGNvbnN0IHByZXZBY3RpdmUgPSBhY3RpdmU7XG4gICAgICAgICAgICBjb25zdCBjdXJyZW50QWN0aXZlID0gYWN0aXZlICsgMSA9PT0gNCA/IDAgOiBhY3RpdmUgKyAxO1xuICAgICAgICAgICAgc2V0QWN0aXZlKGN1cnJlbnRBY3RpdmUpO1xuICAgICAgICAgICAgQXJyYXkuZnJvbSh0aXRsZXMpLmZvckVhY2goZWxlID0+IHtcbiAgICAgICAgICAgICAgZWxlLmNsYXNzTGlzdC5hZGQobW92ZVtjdXJyZW50QWN0aXZlXSk7XG4gICAgICAgICAgICAgIGVsZS5jbGFzc0xpc3QucmVtb3ZlKG1vdmVbcHJldkFjdGl2ZV0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnN0IGlzTGFzdCA9IGFjdGl2ZSA9PT0gdmlkZW9zTGlzdC5sZW5ndGggLSAxO1xuICAgICAgICAgICAgaWYgKGlzTGFzdCkge1xuICAgICAgICAgICAgICBzZXRTZWVrVmFsdWUoMCk7XG4gICAgICAgICAgICAgIHNldEFjdGl2ZSgwKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHNldFNlZWtWYWx1ZSgwKTtcbiAgICAgICAgICAgICAgc2V0QWN0aXZlKGFjdGl2ZSArIDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfX1cbiAgICAgIC8+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250cm9sc1dyYXBwZXJ9PlxuICAgICAgICB7aXNNb2JpbGUgPyAoXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubW9iaWxlQ29udHJvbHN9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mub3V0ZXJ9ICR7cy5tb2JpbGVUcmFja2VyfWB9PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5pbm5lcn0gc3R5bGU9e3sgd2lkdGg6IGAke3NlZWtWYWx1ZX0lYCB9fSAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5tb2JpbGVUaXRsZXNXcmFwcGVyfT5cbiAgICAgICAgICAgICAge3ZpZGVvcy5tYXAoKHZpZGVvLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGFjdCA9IGFjdGl2ZSA9PT0gaW5kZXg7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgICAgYWN0ID8gYCR7cy52aWRlb1RpdGxlfSAke3MuYWN0VGl0bGV9YCA6IGAke3MudmlkZW9UaXRsZX1gXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAge3ZpZGVvLnRpdGxlfVxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkgOiAoXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZGVza3RvcENvbnRyb2xzfT5cbiAgICAgICAgICAgIHt2aWRlb3NMaXN0Lm1hcCgodmlkZW9PYmosIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRTZWVrID0gaW5kZXggPT09IGFjdGl2ZSA/IHNlZWtWYWx1ZSA6IDA7XG4gICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udHJvbHN9PlxuICAgICAgICAgICAgICAgICAgeyFpc01vYmlsZSA/IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mub3V0ZXJ9PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5pbm5lcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiBgJHtjdXJyZW50U2Vla30lYCB9fVxuICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICAgICAgICB7LyogPGlucHV0XG4gICAgICAgICAgICAgICAgICAgIHJlZj17aW5kZXggPT09IGFjdGl2ZSA/IHNsaWRlclJlZiA6IG51bGx9XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5zZWVrQmFyfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Y3VycmVudFNlZWt9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJyYW5nZVwiXG4gICAgICAgICAgICAgICAgICAgIG1pbj17MH1cbiAgICAgICAgICAgICAgICAgICAgbWF4PXsxMDB9XG4gICAgICAgICAgICAgICAgICAvPiAqL31cbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy52aWRlb1RpdGxlfT57dmlkZW9PYmoudGl0bGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICl9XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbn07XG5TbGlkZXJQbGF5ZXIucHJvcFR5cGVzID0ge1xuICB2aWRlb3NMaXN0OiBQcm9wVHlwZXMuYXJyYXlPZihcbiAgICBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubnVtYmVyLCBQcm9wVHlwZXMuc3RyaW5nXSksXG4gICksXG59O1xuU2xpZGVyUGxheWVyLmRlZmF1bHRQcm9wcyA9IHtcbiAgdmlkZW9zTGlzdDogW10sXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKFNsaWRlclBsYXllcik7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1NsaWRlclBsYXllci5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1NsaWRlclBsYXllci5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vU2xpZGVyUGxheWVyLnNjc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi93aXRoU3R5bGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ2NvbXBvbmVudHMvTGluay9MaW5rJztcbmltcG9ydCBTbGlkZXJQbGF5ZXIgZnJvbSAnY29tcG9uZW50cy9TbGlkZXItUGxheWVyL1NsaWRlclBsYXllcic7XG5pbXBvcnQge1xuICBvbmxpbmVUZXN0UG9pbnRzLFxuICBvd25QYXBlcnMsXG4gIHF1ZXN0aW9uQmFuayxcbiAgbWFya3NCYXNlZEFuYWx5c2lzLFxuICBlcnJvckFuYWx5c2lzLFxuICBjb21wYXJpc2lvbkFuYWx5c2lzLFxuICBjb25jZXB0QW5hbHlzaXMsXG4gIHBhcGVycyxcbiAgcmVwb3J0cyxcbn0gZnJvbSAnLi9jb25zdGFudHMnO1xuaW1wb3J0IHtcbiAgUExBVEZPUk1TLFxuICBIT01FX0NMSUVOVFNfVVBEQVRFRCxcbiAgVEVTVF9WSURFT1MsXG59IGZyb20gJy4uL0dldFJhbmtzQ29uc3RhbnRzJztcbmltcG9ydCBzIGZyb20gJy4vVGVzdC5zY3NzJztcblxuY2xhc3MgVGVzdCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIC8vIFRvZ2dsZSBCdXR0b24gQ29kZVxuICAvKiBkaXNwbGF5VG9nZ2xlID0gKCkgPT4gKFxuICAgIDxkaXZcbiAgICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgICAgY2xhc3NOYW1lPXtcbiAgICAgICAgdGhpcy5zdGF0ZS5saXZlVG9nZ2xlXG4gICAgICAgICAgPyBgJHtzLnRvZ2dsZV9vdXRlcn0gJHtzLnRvZ2dsZV9vbn1gXG4gICAgICAgICAgOiBzLnRvZ2dsZV9vdXRlclxuICAgICAgfVxuICAgID5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvZ2dsZV9pbm5lcn0gLz5cbiAgICA8L2Rpdj5cbiAgKTsgKi9cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgICAgaXNNb2JpbGU6IGZhbHNlLFxuICAgIH07XG4gIH1cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5oYW5kbGVTY3JvbGwoKTtcbiAgICB0aGlzLmhhbmRsZVJlc2l6ZSgpO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcbiAgfVxuXG4gIGhhbmRsZVJlc2l6ZSA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTEpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBpc01vYmlsZTogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaXNNb2JpbGU6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICBpZiAod2luZG93LnNjcm9sbFkgPiA1MDApIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiB0cnVlLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzaG93U2Nyb2xsOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVTY3JvbGxUb3AgPSAoKSA9PiB7XG4gICAgd2luZG93LnNjcm9sbFRvKHtcbiAgICAgIHRvcDogMCxcbiAgICAgIGJlaGF2aW9yOiAnc21vb3RoJyxcbiAgICB9KTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dTY3JvbGw6IGZhbHNlLFxuICAgIH0pO1xuICB9O1xuICAvLyBUZWFjaCBTZWN0aW9uXG4gIGRpc3BsYXlUZWFjaFNlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MubWF4Q29udGFpbmVyfT5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmJyZWFkY3J1bX0+XG4gICAgICAgICAgSG9tZSAvIDxzcGFuPk9ubGluZSBUZXN0czwvc3Bhbj5cbiAgICAgICAgPC9zcGFuPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BTZWN0aW9ufT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZWFjaFNlY3Rpb259PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGVhY2hJbWdCb3h9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL05ldyBTdWJNZW51IEl0ZW1zL1Rlc3QvdGVzdC5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cInRlYWNoXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLnRlYWNoU2VjdGlvbk5hbWV9Pk9ubGluZSBUZXN0czwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxoMiBjbGFzc05hbWU9e3MuaGVhZGluZ30+XG4gICAgICAgICAgUGxhbiwgZGVzaWduLCBkZWxpdmVyIGFuZCBtYXJrJm5ic3A7XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MubGVhcm5pbmdUZXh0fT5cbiAgICAgICAgICAgIDxzcGFuPmFzc2Vzc21lbnRzPC9zcGFuPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvbGl2ZSBjbGFzc2VzL3VuZGVyc2NvcmVfZm9yX3RpdGxlLnBuZ1wiXG4gICAgICAgICAgICAgIGFsdD1cInVuZGVyc2NvcmVcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAmbmJzcDtzZWFtbGVzc2x5IC0gb25zaXRlIG9yIHJlbW90ZWx5LlxuICAgICAgICA8L2gyPlxuICAgICAgICA8cCBjbGFzc05hbWU9e3MuY29udGVudFRleHR9PlxuICAgICAgICAgIEFuIGVuZC10by1lbmQgYXNzZXNzbWVudCBwbGF0Zm9ybSB0aGF0IHN1cHBvcnQgdGVhY2hlcnMsIGRhdGEgZW50cnlcbiAgICAgICAgICBvcGVyYXRvcnMgd2l0aCBjdXR0aW5nIGVkZ2UgZmVhdHVyZXMgdGhhdCBjb3ZlciBhbGwgeW91ciBhc3Nlc3NtZW50XG4gICAgICAgICAgbmVlZHMgZW50aXJlbHkgb24tc2NyZWVuLlxuICAgICAgICA8L3A+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJ1dHRvbndyYXBwZXJ9PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT17cy5yZXF1ZXN0RGVtb31cbiAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuaGFuZGxlU2hvd1RyaWFsfVxuICAgICAgICAgICAgcm9sZT1cInByZXNlbnRhdGlvblwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgR0VUIFNVQlNDUklQVElPTlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxhXG4gICAgICAgICAgICBjbGFzc05hbWU9e3Mud2hhdHNhcHB3cmFwcGVyfVxuICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vYXBpLndoYXRzYXBwLmNvbS9zZW5kP3Bob25lPTkxODgwMDc2NDkwOSZ0ZXh0PUhpIEdldFJhbmtzLCBJIHdvdWxkIGxpa2UgdG8ga25vdyBtb3JlIGFib3V0IHlvdXIgT25saW5lIFBsYXRmb3JtLlwiXG4gICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mud2hhdHNhcHB9PkNoYXQgb248L2Rpdj5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9ob21lL3doYXRzYXBwX2xvZ28uc3ZnXCIgYWx0PVwid2hhdHNhcHBcIiAvPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT17cy5hY3Rpb25zV3JhcHBlcn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWN0aW9ufT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjdGlvbmltZ2JveH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L3BsYXlfcHVycGxlLnN2Z1wiIGFsdD1cIndhdGNoIHZpZGVvXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHNwYW4+V2F0Y2ggdmlkZW88L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWN0aW9ufT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjdGlvbmltZ2JveH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2Jyb2NodXJlX3B1cnBsZS5zdmdcIiBhbHQ9XCJicm9jaHVyZVwiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxzcGFuPkRvd25sb2FkIGJyb2NodXJlPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj4gKi99XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmRvd25sb2FkQXBwfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5kb3dubG9hZFRleHR9PkRvd25sb2FkIHRoZSBhcHA8L2Rpdj5cbiAgICAgICAgICA8YVxuICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vcGxheS5nb29nbGUuY29tL3N0b3JlL2FwcHMvZGV0YWlscz9pZD1jb20uZWduaWZ5LmdldHJhbmtzJmhsPWVuX0lOJmdsPVVTXCJcbiAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5wbGF5U3RvcmVJY29ufVxuICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL2hvbWUvcGxhdGZvcm1zL3BsYXlTdG9yZS53ZWJwXCJcbiAgICAgICAgICAgICAgYWx0PVwicGxheV9zdG9yZVwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmhlYWRlcmJhY2tncm91bmRtb2JpbGV9PlxuICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2hlcm9faW1nLndlYnBcIiBhbHQ9XCJtb2JpbGVfYmFja2dyb3VuZFwiIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheUNsaWVudHMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3MuZGlzcGxheUNsaWVudHN9PlxuICAgICAgPGgzPlxuICAgICAgICBUcnVzdGVkIGJ5IDxzcGFuIC8+XG4gICAgICAgIGxlYWRpbmcgRWR1Y2F0aW9uYWwgSW5zdGl0dXRpb25zXG4gICAgICA8L2gzPlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY2xpZW50c1dyYXBwZXJ9PlxuICAgICAgICB7SE9NRV9DTElFTlRTX1VQREFURUQubWFwKGNsaWVudCA9PiAoXG4gICAgICAgICAgPGltZ1xuICAgICAgICAgICAgY2xhc3NOYW1lPXtzLmNsaWVudH1cbiAgICAgICAgICAgIGtleT17Y2xpZW50LmlkfVxuICAgICAgICAgICAgc3JjPXtjbGllbnQuaWNvbn1cbiAgICAgICAgICAgIGFsdD1cImNsaW5ldFwiXG4gICAgICAgICAgLz5cbiAgICAgICAgKSl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxzcGFuPlxuICAgICAgICA8TGluayB0bz1cIi9jdXN0b21lcnNcIj5jbGljayBoZXJlIGZvciBtb3JlLi4uPC9MaW5rPlxuICAgICAgPC9zcGFuPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlBY2hpZXZlZFNvRmFyID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkQ29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmFjaGlldmVkSGVhZGluZ30+XG4gICAgICAgIFdoYXQgd2UgaGF2ZSBhY2hpZXZlZCA8c3BhbiAvPlxuICAgICAgICBzbyBmYXJcbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYWNoaWV2ZWRSb3d9PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMjU1LCAxMDIsIDAsIDAuMiknIH19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5hY2hpZXZlZFByb2ZpbGV9ICR7cy5jbGllbnRzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9jbGllbnRzLnN2Z1wiIGFsdD1cInByb2ZpbGVcIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5jbGllbnRzSGlnaGxpZ2h0fWB9PjE1MCs8L3NwYW4+XG4gICAgICAgICAgPHNwYW4+Q2xpZW50czwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgxNDAsIDAsIDI1NCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnN0dWRlbnRzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC9zdHVkZW50cy5zdmdcIiBhbHQ9XCJzdHVkZW50c1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnN0dWRlbnRzSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMyBMYWtoK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5TdHVkZW50czwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e3MuY2FyZH1cbiAgICAgICAgICBzdHlsZT17eyBib3hTaGFkb3c6ICcwIDRweCAzMnB4IDAgcmdiYSgwLCAxMTUsIDI1NSwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnRlc3RzfWB9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2hvbWUvd2hhdC1hY2hpZXZlZC90ZXN0cy5zdmdcIiBhbHQ9XCJ0ZXN0c1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtzLmhpZ2hsaWdodH0gJHtzLnRlc3RzSGlnaGxpZ2h0fWB9PlxuICAgICAgICAgICAgMyBNaWxsaW9uK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3Bhbj5UZXN0cyBBdHRlbXB0ZWQ8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLmNhcmR9XG4gICAgICAgICAgc3R5bGU9e3sgYm94U2hhZG93OiAnMCA0cHggMzJweCAwIHJnYmEoMCwgMTcyLCAzOCwgMC4yKScgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmFjaGlldmVkUHJvZmlsZX0gJHtzLnF1ZXN0aW9uc31gfT5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9ob21lL3doYXQtYWNoaWV2ZWQvcXVlc3Rpb25zLnN2Z1wiXG4gICAgICAgICAgICAgIGFsdD1cInF1ZXN0aW9uc1wiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cy5oaWdobGlnaHR9ICR7cy5xdWVzdGlvbnNIaWdobGlnaHR9YH0+XG4gICAgICAgICAgICAyMCBNaWxsaW9uK1xuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc3ViVGV4dH0+UXVlc3Rpb25zIFNvbHZlZDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5UGxheWVyU2VjdGlvbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb1NsaWRlcn0+XG4gICAgICA8U2xpZGVyUGxheWVyIHZpZGVvc0xpc3Q9e1RFU1RfVklERU9TfSAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MubWF0aHN9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy9tYXRocy5zdmdcIlxuICAgICAgICBhbHQ9XCJtYXRoc1wiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MubWljcm9zY29wZX1cbiAgICAgICAgc3JjPVwiL2ltYWdlcy9pY29ucy9zdWJqZWN0X2ljb25zL21pY3Jvc2NvcGUuc3ZnXCJcbiAgICAgICAgYWx0PVwibWljcm9zY29wZVwiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MudHJpYW5nbGV9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy90cmlhbmdsZS5zdmdcIlxuICAgICAgICBhbHQ9XCJ0cmlhbmdsZVwiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3Muc2NhbGV9XG4gICAgICAgIHNyYz1cIi9pbWFnZXMvaWNvbnMvc3ViamVjdF9pY29ucy9zY2FsZS5zdmdcIlxuICAgICAgICBhbHQ9XCJzY2FsZVwiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MuY2lyY2xlfVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvY2lyY2xlLnN2Z1wiXG4gICAgICAgIGFsdD1cImNpcmNsZVwiXG4gICAgICAvPlxuICAgICAgPGltZ1xuICAgICAgICBjbGFzc05hbWU9e3MuY2hlbWlzdHJ5fVxuICAgICAgICBzcmM9XCIvaW1hZ2VzL2ljb25zL3N1YmplY3RfaWNvbnMvY2hlbWlzdHJ5LnN2Z1wiXG4gICAgICAgIGFsdD1cImNoZW1pc3RyeVwiXG4gICAgICAvPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlPbmxpbmVUZXN0cyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RzSW1hZ2V9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L1Rlc3RzX0hlYWRTZWN0aW9uLnN2Z1wiXG4gICAgICAgICAgICAgICAgYWx0PVwib25saW5lLXRlc3RzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5PbmxpbmUgVGVzdHM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIHtvbmxpbmVUZXN0UG9pbnRzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5vbmxpbmVUZXN0c31gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L29ubGluZS10ZXN0cy53ZWJwXCIgYWx0PVwib25saW5lLXRlc3RzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlPd25QYXBlcnMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250ZW50c30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+XG4gICAgICAgICAgICAgIFVwbG9hZCB5b3VyIG93biBRdWVzdGlvbiBwYXBlcnNcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGFwZXJzV3JhcHBlcn0+XG4gICAgICAgICAgICAgIHtvd25QYXBlcnMubWFwKHBvaW50ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb3cxfSAke3MucG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludENvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZERvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm93blBhcGVyc31gfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L293bi1wYXBlcnMud2VicFwiIGFsdD1cIm93bi1wYXBlcnNcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVF1ZXN0aW9uQmFuayA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcl9yZXZlcnNlfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RzSW1hZ2V9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L1Rlc3RzX0hlYWRTZWN0aW9uLnN2Z1wiXG4gICAgICAgICAgICAgICAgYWx0PVwib25saW5lLXRlc3RzXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5JbmJ1aWx0IFF1ZXN0aW9ucyBCYW5rIDwvZGl2PlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAge3F1ZXN0aW9uQmFuay5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3MucXVlc3Rpb25CYW5rfWB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvcXVlc3Rpb25CYW5rLndlYnBcIiBhbHQ9XCJvbmxpbmUtdGVzdHNcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG5cbiAgZGlzcGxheVBhcGVycyA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRhaW5lcn0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0c0ltYWdlfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9BbmFseXNpc19IZWFkU2VjdGlvbi5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+UGFwZXJzPC9kaXY+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICB7cGFwZXJzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIHtwb2ludC5zdWJQb2ludHMgPyAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICB7cG9pbnQuc3ViUG9pbnRzLm1hcCh0ZXh0ID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3RleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7IXRoaXMuc3RhdGUuaXNNb2JpbGUgPyAoXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9YH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9hbmFseXNpcy1jYXJkLnN2Z1wiIGFsdD1cInBhcGVyc1wiIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApIDogKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLm1vYmlsZVBhcGVyc0NvbnRhaW5lcn0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9tb2JpbGVfcGFwZXJzLnN2Z1wiIGFsdD1cIm1vYmxlX3BhcGVyc1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICl9XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcblxuICBkaXNwbGF5QW5hbHlzaXMgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl9jb250YWluZXJfcmV2ZXJzZX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zZWN0aW9uX2NvbnRlbnRzfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudFBhcnR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50ZXN0c0ltYWdlfT5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9BbmFseXNpc19IZWFkU2VjdGlvbi5zdmdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIm9ubGluZS10ZXN0c1wiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+QW5hbHlzaXM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnN1Yl9zZWN0aW9uX3RpdGxlfT5NYXJrcy1iYXNlZCBBbmFseXNpczwvZGl2PlxuICAgICAgICAgICAgICB7bWFya3NCYXNlZEFuYWx5c2lzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9ICR7cy5vbmxpbmVUZXN0c31gfT5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuZW1wdHlDYXJkfSAke3MubWFyZ2luQm90dG9tMTI2fSAke1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmlzTW9iaWxlID8gcy5zaG93IDogcy5oaWRlXG4gICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvVGVzdC9tYXJrc0FuYWx5c2lzLndlYnBcIlxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIm1hcmtzLWFuYWx5c2lzXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViX3NlY3Rpb25fdGl0bGV9PkNvbXBhcmlzaW9uIEFuYWx5c2lzPC9kaXY+XG4gICAgICAgICAgICAgIHtjb21wYXJpc2lvbkFuYWx5c2lzLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3Mucm93MX0gJHtzLnBvaW50fWB9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRDb250YWluZXJ9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5yZWREb3R9IC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50VGV4dH0+e3BvaW50LnRleHR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdWJfc2VjdGlvbl90aXRsZX0+RXJyb3IgQW5hbHlzaXM8L2Rpdj5cbiAgICAgICAgICAgICAge2Vycm9yQW5hbHlzaXMubWFwKHBvaW50ID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5yb3cxfSAke3MucG9pbnR9YH0+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludENvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZERvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQudGV4dH08L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gJHtzLm9ubGluZVRlc3RzfWB9PlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7cy5lbXB0eUNhcmR9ICR7cy5tYXJnaW5Cb3R0b20xMjZ9ICR7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuaXNNb2JpbGUgPyBzLnNob3cgOiBzLmhpZGVcbiAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L2NvbmNlcHRBbmFseXNpcy53ZWJwXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJjb25jZXB0LWFuYWx5c2lzXCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Muc3ViX3NlY3Rpb25fdGl0bGV9PkNvbmNlcHQtYmFzZWQgQW5hbHlzaXM8L2Rpdj5cbiAgICAgICAgICAgICAge2NvbmNlcHRBbmFseXNpcy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9YH0+XG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtzLmVtcHR5Q2FyZH0gJHtzLm1hcmdpbkJvdHRvbTEyNn0gJHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5pc01vYmlsZSA/IHMuc2hvdyA6IHMuaGlkZVxuICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1Rlc3QvZXJyb3JBbmFseXNpcy53ZWJwXCJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJlcnJvci1hbmFseXNpc1wiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmJvdHRvbUNpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENpcmNsZX0gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9ICR7XG4gICAgICAgICAgICB0aGlzLnN0YXRlLmlzTW9iaWxlID8gcy5oaWRlIDogcy5zaG93XG4gICAgICAgICAgfWB9XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5lbXB0eUNhcmR9ICR7cy5tYXJnaW5Cb3R0b20xMjZ9YH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDYXJkfT5cbiAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvbWFya3NBbmFseXNpcy53ZWJwXCIgYWx0PVwibWFya3MtYW5hbHlzaXNcIiAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZW1wdHlDYXJkfSAke3MubWFyZ2luQm90dG9tMTI2fWB9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzcmM9XCIvaW1hZ2VzL1Rlc3QvY29uY2VwdEFuYWx5c2lzLndlYnBcIlxuICAgICAgICAgICAgICAgIGFsdD1cImNvbmNlcHQtYW5hbHlzaXNcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5ib3R0b21DaXJjbGV9IC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy50b3BDaXJjbGV9IC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuZW1wdHlDYXJkfWB9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2FyZH0+XG4gICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2Vycm9yQW5hbHlzaXMud2VicFwiIGFsdD1cImVycm9yLWFuYWx5c2lzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlSZXBvcnRzID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGFpbmVyfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fY29udGVudHN9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRlc3RzSW1hZ2V9PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3JjPVwiL2ltYWdlcy9UZXN0L1JlcG9ydHNfSGVhZFNlY3Rpb24uc3ZnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJvbmxpbmUtdGVzdHNcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnNlY3Rpb25fdGl0bGV9PlJlcG9ydHM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlcG9ydHNfc3ViX3RpdGxlfT45IEluZm9ybWF0aXZlIFJlcG9ydHM8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlcG9ydHNfbm90aWNlfT5cbiAgICAgICAgICAgICAgKERvd25sb2FkYWJsZSBmb3IgTm90aWNlIEJvYXJkKVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICB7cmVwb3J0cy5tYXAocG9pbnQgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnJvdzF9ICR7cy5wb2ludH1gfT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBvaW50Q29udGFpbmVyfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkRG90fSAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wb2ludFRleHR9Pntwb2ludC50ZXh0fTwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSAke3Mub25saW5lVGVzdHN9YH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRvcENhcmR9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9yZXBvcnRzLndlYnBcIiBhbHQ9XCJyZXBvcnRzXCIgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYm90dG9tQ2lyY2xlfSAvPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudG9wQ2lyY2xlfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlUZXN0c1NlY3Rpb24gPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e2Ake3Muc2VjdGlvbl9jb250YWluZXJ9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRlbnRzfSAke3MubWF4Q29udGFpbmVyfWB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50UGFydH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3Muc2VjdGlvbl90aXRsZX0+XG4gICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnRleHRjb250ZW50fT5cbiAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbCBKRUVcbiAgICAgICAgICAgICAgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoIG9ubGluZSBhbmRcbiAgICAgICAgICAgICAgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhIHdob2xlIGZvciBhbGwgdGhlXG4gICAgICAgICAgICAgIHN0dWRlbnRzXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLmltYWdlUGFydH0gY3VzdG9tLXNjcm9sbGJhcmB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmVtcHR5Q2FyZH0+XG4gICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvVGVzdC9kZW1vLnBuZ1wiIGFsdD1cImRlbW9cIiAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvQ29udGVudHN9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5JbmRlcGVuZGVuY2U8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgWW91ciBab29tIGFjY291bnQgd2lsbCBiZSB1c2VkLiBBbGwgdGhlIGZlYXR1cmVzIGFuZCBwcm92aXNpb25zXG4gICAgICAgICAgICAgICAgb2YgeW91ciBab29tIGFjY291bnQgc2hhbGwgYmUgYXZhaWxhYmxlIHdpdGggYW4gYWRkZWQgYWR2YW50YWdlXG4gICAgICAgICAgICAgICAgb2YgbG9naW4gcmVzdHJpY3Rpb25zIG9ubHkgZm9yIHNlbGVjdGVkIHN0dWRlbnRzLlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5GbGV4aWJpbGl0eTwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBBbnkgbnVtYmVyIG9mIFpvb20gYWNjb3VudHMgY2FuIGJlIHVzZWQuIERpZmZlcmVudCBab29tIGFjY291bnRzXG4gICAgICAgICAgICAgICAgY2FuIGJlIG1hcHBlZCB3aXRoIGRpZmZlcmVudCBDbGFzcyBTZXNzaW9ucyB0aHVzIGFsbG93aW5nXG4gICAgICAgICAgICAgICAgbXVsdGlwbGUgdGVhY2hlcnMgdG8gdXNlIHRoZWlyIHBlcnNvbmFsIGFjY291bnRzLlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5TZWN1cmUgQWNjZXNzPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIE9ubHkgc2VsZWN0ZWQgc3R1ZGVudHMgd2hvIGhhdmUgYWNjZXNzIHRvIHRoZSBBcHAgY2FuIG5vdyBhY2Nlc3NcbiAgICAgICAgICAgICAgICBab29tIGNsYXNzLCBubyBuZWVkIHRvIHNoYXJlIE1lZXRpbmcgSUQgZXZlcnl0aW1lLCBObyByYW5kb21cbiAgICAgICAgICAgICAgICBhY2Nlc3MuXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9Pk11bHRpcGxlIHVzZXMgb2YgWm9vbTwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBab29tIGZhY2lsaXR5IGF2YWlsYWJsZSBmb3IgbGl2ZSBDbGFzcyBhbmQgRG91YnRzIG1vZHVsZXMuIFNvb25cbiAgICAgICAgICAgICAgICBab29tIGlzIGJlaW5nIGludGVncmF0ZWQgdG8gdGhlIG9ubGluZSBFeGFtIG1vZHVsZSBmb3Igb25saW5lXG4gICAgICAgICAgICAgICAgRXhhbSBwcm9jdG9yaW5nLlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIC8vIExlY3R1cmUgc2VjdGlvblxuICBkaXNwbGF5QW5hbHlzaXNTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGFpbmVyfSAke3MuYXNoQmFja2dyb3VuZH1gfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGVudHN9ICR7cy5tYXhDb250YWluZXJ9YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5cbiAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsIEpFRVxuICAgICAgICAgICAgICBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGggb25saW5lIGFuZFxuICAgICAgICAgICAgICBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGEgd2hvbGUgZm9yIGFsbCB0aGVcbiAgICAgICAgICAgICAgc3R1ZGVudHNcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3MuaW1hZ2VQYXJ0fSBjdXN0b20tc2Nyb2xsYmFyYH0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuZW1wdHlDYXJkfT5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9UZXN0L2RlbW8ucG5nXCIgYWx0PVwiZGVtb1wiIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9Db250ZW50c30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlQcmFjdGljZSA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5zZWN0aW9uX2NvbnRhaW5lcn1gfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGVudHN9ICR7cy5tYXhDb250YWluZXJ9YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5cbiAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsIEpFRVxuICAgICAgICAgICAgICBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGggb25saW5lIGFuZFxuICAgICAgICAgICAgICBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGEgd2hvbGUgZm9yIGFsbCB0aGVcbiAgICAgICAgICAgICAgc3R1ZGVudHNcbiAgICAgICAgICAgICAgey8qIHtURVNUU19LRVlfUE9JTlRTLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBrZXk9e3BvaW50LmlkfSBjbGFzc05hbWU9e3MucG9pbnR9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkZG90d3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZGRvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQucG9pbnR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfSAqL31cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgey8qIDxhIGNsYXNzTmFtZT17YCR7cy52aWV3bW9yZX0gJHtzLm1hcmdpbkxlZnR9YH0gaHJlZj1cIi8jXCI+XG4gICAgICAgICAgICAgIFZpZXcgbW9yZSA8aW1nIHNyYz1cImltYWdlcy9pY29ucy9yaWdodGFycm93X2JsdWUuc3ZnXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICAgIDwvYT4gKi99XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9IGN1c3RvbS1zY3JvbGxiYXJgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvZGVtby5wbmdcIiBhbHQ9XCJkZW1vXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb0NvbnRlbnRzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG4gIGRpc3BsYXlQYXBlcnNTZWN0aW9uID0gKCkgPT4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGFpbmVyfSAke3MuYXNoQmFja2dyb3VuZH1gfT5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtzLnNlY3Rpb25fY29udGVudHN9ICR7cy5tYXhDb250YWluZXJ9YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRQYXJ0fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50fT5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5zZWN0aW9uX3RpdGxlfT5cbiAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudGV4dGNvbnRlbnR9PlxuICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsIEpFRVxuICAgICAgICAgICAgICBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGggb25saW5lIGFuZFxuICAgICAgICAgICAgICBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGEgd2hvbGUgZm9yIGFsbCB0aGVcbiAgICAgICAgICAgICAgc3R1ZGVudHNcbiAgICAgICAgICAgICAgey8qIHtURVNUU19LRVlfUE9JTlRTLm1hcChwb2ludCA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBrZXk9e3BvaW50LmlkfSBjbGFzc05hbWU9e3MucG9pbnR9PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucmVkZG90d3JhcHBlcn0+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnJlZGRvdH0gLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucG9pbnRUZXh0fT57cG9pbnQucG9pbnR9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfSAqL31cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgey8qIDxhIGNsYXNzTmFtZT17YCR7cy52aWV3bW9yZX0gJHtzLm1hcmdpbkxlZnR9YH0gaHJlZj1cIi8jXCI+XG4gICAgICAgICAgICAgIFZpZXcgbW9yZSA8aW1nIHNyYz1cImltYWdlcy9pY29ucy9yaWdodGFycm93X2JsdWUuc3ZnXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICAgIDwvYT4gKi99XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cy5pbWFnZVBhcnR9IGN1c3RvbS1zY3JvbGxiYXJgfT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5lbXB0eUNhcmR9PlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL1Rlc3QvZGVtby5wbmdcIiBhbHQ9XCJkZW1vXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb0NvbnRlbnRzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy52aWRlb19zZWN0aW9ufT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGl0bGV9PlxuICAgICAgICAgICAgICAgIENvbmR1Y3QgT2ZmbGluZS9PbmxpbmUgVGVzdHNcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5jb250ZW50X3RleHR9PlxuICAgICAgICAgICAgICAgIEdldHJhbmtzIGNvbWVzIHdpdGggdGhlIGNhcGFiaWxpdHkgd2hlcmUganVzdCBsaWtlIHRoZSBhY3R1YWxcbiAgICAgICAgICAgICAgICBKRUUgZXhhbSwgdGhlIHRlc3QgY2FuIGJlIGF0dGVtcHRlZCBzaW11bHRhbmVvdXNseSBpbiBib3RoXG4gICAgICAgICAgICAgICAgb25saW5lIGFuZCBvZmZsaW5lIG1vZGVzIGFuZCB0aGUgYW5hbHlzaXMgaXMgZ2VuZXJhdGVkIGFzIGFcbiAgICAgICAgICAgICAgICB3aG9sZSBmb3IgYWxsIHRoZSBzdHVkZW50c1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MudmlkZW9fc2VjdGlvbn0+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17cy5jb250ZW50X3RpdGxlfT5cbiAgICAgICAgICAgICAgICBDb25kdWN0IE9mZmxpbmUvT25saW5lIFRlc3RzXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuY29udGVudF90ZXh0fT5cbiAgICAgICAgICAgICAgICBHZXRyYW5rcyBjb21lcyB3aXRoIHRoZSBjYXBhYmlsaXR5IHdoZXJlIGp1c3QgbGlrZSB0aGUgYWN0dWFsXG4gICAgICAgICAgICAgICAgSkVFIGV4YW0sIHRoZSB0ZXN0IGNhbiBiZSBhdHRlbXB0ZWQgc2ltdWx0YW5lb3VzbHkgaW4gYm90aFxuICAgICAgICAgICAgICAgIG9ubGluZSBhbmQgb2ZmbGluZSBtb2RlcyBhbmQgdGhlIGFuYWx5c2lzIGlzIGdlbmVyYXRlZCBhcyBhXG4gICAgICAgICAgICAgICAgd2hvbGUgZm9yIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnZpZGVvX3NlY3Rpb259PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuY29udGVudF90aXRsZX0+XG4gICAgICAgICAgICAgICAgQ29uZHVjdCBPZmZsaW5lL09ubGluZSBUZXN0c1xuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmNvbnRlbnRfdGV4dH0+XG4gICAgICAgICAgICAgICAgR2V0cmFua3MgY29tZXMgd2l0aCB0aGUgY2FwYWJpbGl0eSB3aGVyZSBqdXN0IGxpa2UgdGhlIGFjdHVhbFxuICAgICAgICAgICAgICAgIEpFRSBleGFtLCB0aGUgdGVzdCBjYW4gYmUgYXR0ZW1wdGVkIHNpbXVsdGFuZW91c2x5IGluIGJvdGhcbiAgICAgICAgICAgICAgICBvbmxpbmUgYW5kIG9mZmxpbmUgbW9kZXMgYW5kIHRoZSBhbmFseXNpcyBpcyBnZW5lcmF0ZWQgYXMgYVxuICAgICAgICAgICAgICAgIHdob2xlIGZvciBhbGwgdGhlIHN0dWRlbnRzXG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG4gIGRpc3BsYXlBdmFpbGFibGVPbiA9ICgpID0+IChcbiAgICA8ZGl2IGNsYXNzTmFtZT17cy5hdmFpbGFibGVDb250YWluZXJ9PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlUm93fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlQ29udGVudFNlY3Rpb259PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLmF2YWlsYWJsZVRpdGxlfT5cbiAgICAgICAgICAgIFdl4oCZcmUgPGJyIC8+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3MuYXZhaWxhYmxlfT5hdmFpbGFibGU8L3NwYW4+IG9uXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Mucm93fT5cbiAgICAgICAgICAgIHtQTEFURk9STVMubWFwKGl0ZW0gPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5wbGF0Zm9ybUNvbnRhaW5lcn0+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2l0ZW0uaWNvbn0gYWx0PVwiXCIgaGVpZ2h0PVwiNDBweFwiIHdpZHRoPVwiNDBweFwiIC8+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3MucGxhdGZvcm19PntpdGVtLmxhYmVsfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzLnBsYXRmb3JtT3N9PntpdGVtLmF2YWlsYWJsZX08L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cy5zdG9yZX0+XG4gICAgICAgICAgICA8YVxuICAgICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9wbGF5Lmdvb2dsZS5jb20vc3RvcmUvYXBwcy9kZXRhaWxzP2lkPWNvbS5lZ25pZnkuZ2V0cmFua3MmaGw9ZW5fSU4mZ2w9VVNcIlxuICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxheVN0b3JlLndlYnBcIlxuICAgICAgICAgICAgICAgIGFsdD1cIlBsYXkgU3RvcmVcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5wbGF5c3RvcmV9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvYXBwU3RvcmUud2VicFwiXG4gICAgICAgICAgICAgIGFsdD1cIkFwcCBTdG9yZVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17cy5hcHBzdG9yZX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxpbWdcbiAgICAgICAgICBjbGFzc05hbWU9e3MuZGVza3RvcEltYWdlfVxuICAgICAgICAgIHNyYz1cIi9pbWFnZXMvaG9tZS9wbGF0Zm9ybXMvcGxhdGZvcm1zX3YyLndlYnBcIlxuICAgICAgICAgIGFsdD1cInBsYXRmb3JtXCJcbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlKb2luRmFjZWJvb2sgPSAoKSA9PiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e3Muam9pbkNvbW11bml0eX0+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e3Muam9pbkZiVGV4dH0+Sm9pbiBvdXIgRmFjZWJvb2sgQ29tbXVuaXR5PC9zcGFuPlxuICAgICAgPGEgY2xhc3NOYW1lPXtzLmpvaW5GYkxpbmt9IGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vRWduaWZ5L1wiPlxuICAgICAgICBKb2luIE5vd1xuICAgICAgPC9hPlxuICAgIDwvZGl2PlxuICApO1xuXG4gIGRpc3BsYXlTY3JvbGxUb1RvcCA9ICgpID0+IHtcbiAgICBjb25zdCB7IHNob3dTY3JvbGwgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIHNob3dTY3JvbGwgJiYgKFxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3NOYW1lPXtzLnNjcm9sbFRvcH1cbiAgICAgICAgICByb2xlPVwicHJlc2VudGF0aW9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbFRvcCgpO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaG9tZS9zY3JvbGxUb3Auc3ZnXCIgYWx0PVwic2Nyb2xsVG9wXCIgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIHt0aGlzLmRpc3BsYXlUZWFjaFNlY3Rpb24oKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUNsaWVudHMoKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUFjaGlldmVkU29GYXIoKX1cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheVBsYXllclNlY3Rpb24oKSAqL31cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheVRlc3RzU2VjdGlvbigpICovfVxuICAgICAgICB7dGhpcy5kaXNwbGF5T25saW5lVGVzdHMoKX1cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheUFuYWx5c2lzKCkgKi99XG4gICAgICAgIHt0aGlzLmRpc3BsYXlPd25QYXBlcnMoKX1cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheVByYWN0aWNlKCkgKi99XG4gICAgICAgIHt0aGlzLmRpc3BsYXlRdWVzdGlvbkJhbmsoKX1cbiAgICAgICAgey8qIHRoaXMuZGlzcGxheVBhcGVycygpICovfVxuICAgICAgICB7dGhpcy5kaXNwbGF5UGFwZXJzKCl9XG4gICAgICAgIHt0aGlzLmRpc3BsYXlBbmFseXNpcygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5UmVwb3J0cygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5QXZhaWxhYmxlT24oKX1cbiAgICAgICAge3RoaXMuZGlzcGxheUpvaW5GYWNlYm9vaygpfVxuICAgICAgICB7dGhpcy5kaXNwbGF5U2Nyb2xsVG9Ub3AoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzKShUZXN0KTtcbiIsIlxuICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vVGVzdC5zY3NzXCIpO1xuICAgIHZhciBpbnNlcnRDc3MgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9pc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvaW5zZXJ0Q3NzLmpzXCIpO1xuXG4gICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENvbnRlbnQgPSBmdW5jdGlvbigpIHsgcmV0dXJuIGNvbnRlbnQ7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2dldENzcyA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudC50b1N0cmluZygpOyB9O1xuICAgIG1vZHVsZS5leHBvcnRzLl9pbnNlcnRDc3MgPSBmdW5jdGlvbihvcHRpb25zKSB7IHJldHVybiBpbnNlcnRDc3MoY29udGVudCwgb3B0aW9ucykgfTtcbiAgICBcbiAgICAvLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG4gICAgLy8gaHR0cHM6Ly93ZWJwYWNrLmdpdGh1Yi5pby9kb2NzL2hvdC1tb2R1bGUtcmVwbGFjZW1lbnRcbiAgICAvLyBPbmx5IGFjdGl2YXRlZCBpbiBicm93c2VyIGNvbnRleHRcbiAgICBpZiAobW9kdWxlLmhvdCAmJiB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHZhciByZW1vdmVDc3MgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1Rlc3Quc2Nzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL1Rlc3Quc2Nzc1wiKTtcblxuICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtb3ZlQ3NzID0gaW5zZXJ0Q3NzKGNvbnRlbnQsIHsgcmVwbGFjZTogdHJ1ZSB9KTtcbiAgICAgIH0pO1xuICAgICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyByZW1vdmVDc3MoKTsgfSk7XG4gICAgfVxuICAiLCJleHBvcnQgY29uc3Qgb25saW5lVGVzdFBvaW50cyA9IFtcbiAge1xuICAgIHRleHQ6ICdVbmxpbWl0dGVkIHRlc3QgY3JlYXRpb24nLFxuICB9LFxuICB7XG4gICAgdGV4dDpcbiAgICAgICdJbnN0YW50IHRlc3QgcmVzdWx0IGFuZCBSZXBvcnRzIGF2YWlsYWJsZSBvbmxpbmUgJiBvZmZsaW5lIC0gUmFuayBsaXN0LCBBbmFseXNpcywgYW5kIFJlcG9ydHMgYWZ0ZXIgZXZlcnkgdGVzdC4nLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1ByZS1kZWZpbmVkIG1hcmtpbmcgc2NoZW1lcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnQ3VzdG9tIG1hcmtpbmcgc2NoZW1lcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OlxuICAgICAgJ0dldHJhbmtzIHN1cHBvcnRzIGFsbCAyMiB0eXBlcyBvZiBKRUUgYXZhbmNlZCBNYXJraW5nIFNjaGVtZXMgc3RhcnRpbmcgZnJvbSAyMDA5IC0gMjAxOScsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnR3JhY2UgcGVyaW9kIG9wdGlvbiBhdmFpbGFibGUnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0xpdmUgQXR0ZW5kYW5jZScsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3Qgb3duUGFwZXJzID0gW1xuICB7XG4gICAgdGV4dDpcbiAgICAgICdVcGxvYWQgeW91ciBxdWVzdGlvbiBwYXBlciBvbmxpbmUgZnJvbSB3b3JkIGRvY3VtZW50IHdpdGhpbiA1IG1pbnMgd2l0aCBhIHNpbmdsZSBjbGljaycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OlxuICAgICAgJ0dldHJhbmtzIFBhcnNlciBjYW4gY29udmVydCB0ZXh0IC8gaW1hZ2VzIC8gZXF1YXRpb25zIC8gZ3JhcGhzIGludG8geW91ciBxdWVzdGlvbnMuJyxcbiAgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBxdWVzdGlvbkJhbmsgPSBbXG4gIHtcbiAgICB0ZXh0OlxuICAgICAgJ1F1ZXN0aW9uIGJhbmsgbWFuYWdlbWVudCBzeXN0ZW0gd2l0aCAxLjUgTGFraCBRdWVzdGlvbnMgb3JnYW5pemVkIGJ5IHRvcGljIGFuZCBkaWZmaWN1bHR5IGZvciBKRUUsIE5FRVQsIEVBTUNFVCBhbmQgSVBFLicsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnQWxsIFR5cGVzIG9mIFF1ZXN0aW9ucyBhcyBwZXIgbGF0ZXN0IHBhdHRlcm4nLFxuICB9LFxuICB7XG4gICAgdGV4dDpcbiAgICAgICdRdWVzdGlvbnMgYXJlIHByb3ZpZGVkIHdpdGggQW5zd2VyIEtleSBhbmQgU3RlcCBieSBTdGVwIERldGFpbCBTb2x1dGlvbi4nLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IG1hcmtzQmFzZWRBbmFseXNpcyA9IFtcbiAge1xuICAgIHRleHQ6ICdNYXJrcyBkaXN0cmlidXRpb24gZ3JhcGgnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0N1dC1vZmYgYmFzZWQgYW5hbHlzaXMnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1N0dWRlbnQtd2lzZSBtYXJrcyBhbmFseXNpcycsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgY29tcGFyaXNpb25BbmFseXNpcyA9IFtcbiAge1xuICAgIHRleHQ6ICdIaXN0b3JpYyB0ZXN0IGF2ZXJhZ2VzJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdTdHVkZW50IHBlcmZvcm1hbmNlIHByb2ZpbGUnLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ1N1YmplY3QgdG9wIG1hcmtzIHZzIHRvcHBlcicsXG4gIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgZXJyb3JBbmFseXNpcyA9IFtcbiAge1xuICAgIHRleHQ6ICdRdWVzdGlvbiBlcnJvciBjb3VudCcsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3R1ZGVudHMgZXJyb3IgbWFwcGluZycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnUXVlc3Rpb24gcGFwZXIgcmVmZXJlbmNpbmcnLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IGNvbmNlcHRBbmFseXNpcyA9IFtcbiAge1xuICAgIHRleHQ6ICdUb3BpYyBiYXNlZCBhbmFseXNpcycsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3ViLXRvcGljIGJhc2VkIGFuYWx5c2lzJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdEaWZmaWN1bHR5IGxldmVsIGFuYWx5c2lzJyxcbiAgfSxcbl07XG5leHBvcnQgY29uc3QgcGFwZXJzID0gW1xuICB7XG4gICAgdGV4dDpcbiAgICAgICdNb3JlIHRoYW4gMSw1MCwwMDArIHJlYWR5IHRvIHVzZSBDb21wcmVoZW5zaXZlIFF1ZXN0aW9uIEJhbmsgYW5kIGVhY2ggcXVlc3Rpb24gaGFzIGJlZW4gc2V0IHdpdGggYXBwcm9wcmlhdGUgRGlmZmljdWx0eSBMZXZlbC4nLFxuICB9LFxuICB7XG4gICAgdGV4dDogJ0FsbCBUeXBlcyBvZiBRdWVzdGlvbnMgYXMgcGVyIGxhdGVzdCBwYXR0ZXJuOicsXG4gICAgc3ViUG9pbnRzOiBbXG4gICAgICAnYSlcdFNpbmdsZSBjb3JyZWN0LCBNdWx0aXBsZSBjb3JyZWN0IHF1ZXN0aW9ucywgSW50ZWdlciB0eXBlLCBNYXRyaXggdHlwZSwnLFxuICAgICAgJ2IpXHRBc3NlcnRpb24gUmVhc29uaW5nIGFuZCBQYXJhZ3JhcGggVHlwZScsXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIHRleHQ6XG4gICAgICAnRGVzaWduIHBlcnNvbmFsaXplZCBxdWVzdGlvbiBwYXBlci9zIHdpdGggSW5zdGl0dXRlIE5hbWUsIExvZ28gJiBXYXRlcm1hcmsnLFxuICB9LFxuICB7XG4gICAgdGV4dDpcbiAgICAgICdRdWVzdGlvbnMgYXJlIHByb3ZpZGVkIHdpdGggQW5zd2VyIEtleSBhbmQgU3RlcCBieSBTdGVwIERldGFpbCBTb2x1dGlvbi4nLFxuICB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IHJlcG9ydHMgPSBbXG4gIHtcbiAgICB0ZXh0OiAnVGVzdCByZXN1bHRzJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdTdHVkZW50IHJlc3BvbnNlIHJlcG9ydCcsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnU3R1ZGVudCByZXNwb25zZSBjb3VudCcsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnTWFya3MgZGlzdHJpYnV0aW9uJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdFcnJvciBjb3VudCcsXG4gIH0sXG4gIHtcbiAgICB0ZXh0OiAnVG9wIG1hcmtzJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdTdHVkZW50IHBlcmZvcm1hbmNlIHRyZW5kJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdBdmVyYWdlcyBjb21wYXJpc29uJyxcbiAgfSxcbiAge1xuICAgIHRleHQ6ICdDb25jZXB0IGJhc2VkIHJlcG9ydCcsXG4gIH0sXG5dO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMYXlvdXQgZnJvbSAnY29tcG9uZW50cy9MYXlvdXQvTGF5b3V0JztcbmltcG9ydCBUZXN0c01vZHVsZSBmcm9tICcuL1Rlc3QnO1xuXG5hc3luYyBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgdGl0bGU6ICdFZ25pZnkgdGVhY2hpbmcgbWFkZSBlYXN5JyxcbiAgICBjaHVua3M6IFsnVGVzdHMnXSxcbiAgICBjb21wb25lbnQ6IChcbiAgICAgIDxMYXlvdXQgZm9vdGVyQXNoPlxuICAgICAgICA8VGVzdHNNb2R1bGUgLz5cbiAgICAgIDwvTGF5b3V0PlxuICAgICksXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFjdGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVdBO0FBQ0E7QUFiQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQWVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBREE7QUFTQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTlDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQU1BO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBREE7QUFJQTs7Ozs7OztBQ3pKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFXQTtBQUtBO0FBQ0E7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBQUE7Ozs7Ozs7Ozs7OztBQVlBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFvQkE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQTlCQTtBQWdDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBMUNBO0FBNENBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBcERBO0FBcURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWhGQTtBQUNBO0FBdERBO0FBMklBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBakJBO0FBQ0E7QUE1SUE7QUFpS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF0REE7QUFDQTtBQWxLQTtBQTZOQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE1QkE7QUFDQTtBQTlOQTtBQWlRQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFoQ0E7QUFDQTtBQWxRQTtBQXdTQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTFCQTtBQUNBO0FBelNBO0FBeVVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWhDQTtBQUNBO0FBMVVBO0FBZ1hBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFiQTtBQXFCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQS9DQTtBQUNBO0FBalhBO0FBc2FBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaklBO0FBQ0E7QUF2YUE7QUE4aUJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFwQ0E7QUFDQTtBQS9pQkE7QUF5bEJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQS9DQTtBQUNBO0FBMWxCQTtBQXFwQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMURBO0FBQ0E7QUF0cEJBO0FBNHRCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW1CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBckVBO0FBQ0E7QUE3dEJBO0FBNnlCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW1CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBckVBO0FBQ0E7QUE5eUJBO0FBODNCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBckNBO0FBQ0E7QUEvM0JBO0FBNDZCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEE7QUFDQTtBQTc2QkE7QUFxN0JBO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFuOEJBO0FBQ0E7QUFDQTtBQUZBO0FBRkE7QUFNQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFxN0JBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFvQkE7Ozs7QUExK0JBO0FBQ0E7QUE0K0JBOzs7Ozs7O0FDbmdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7O0FDN0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFFQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUtBO0FBREE7QUFJQTtBQURBO0FBTUE7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFLQTtBQUVBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUtBO0FBRUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBS0E7QUFFQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUVBO0FBREE7QUFLQTtBQUNBO0FBRkE7QUFRQTtBQURBO0FBS0E7QUFEQTtBQU1BO0FBRUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7OztBQVlBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=