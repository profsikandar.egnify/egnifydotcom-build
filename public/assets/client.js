/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"client": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + ({"Assignments":"Assignments","Connect":"Connect","Customers":"Customers","Doubts":"Doubts","HowWorks":"HowWorks","Live Classes":"Live Classes","CancellationAndReturn":"CancellationAndReturn","Careers":"Careers","Culture":"Culture","LaunchingSoon":"LaunchingSoon","PricingDetails":"PricingDetails","PrivacyAndTerms":"PrivacyAndTerms","TermsAndConditions":"TermsAndConditions","qms":"qms","Aboutus":"Aboutus","GetRanks":"GetRanks","Press":"Press","Pricing":"Pricing","Tests":"Tests","Acads~Jeet~Otp~Team":"Acads~Jeet~Otp~Team","Acads~Jeet~Otp":"Acads~Jeet~Otp","Acads":"Acads","Jeet":"Jeet","Otp":"Otp","Team":"Team","Score":"Score"}[chunkId]||chunkId) + ".chunk.js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/error/ErrorPage.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "html {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 0 32px;\n  padding: 0 2rem;\n  height: 100%;\n  font-family: sans-serif;\n  text-align: center;\n  color: #888;\n}\n\nbody {\n  margin: 0;\n}\n\nh1 {\n  font-weight: 400;\n  color: #555;\n}\n\npre {\n  white-space: pre-wrap;\n  text-align: left;\n}\n", "", {"version":3,"sources":["/home/praveen/Desktop/Egnify project/egnify-website/src/routes/error/ErrorPage.css"],"names":[],"mappings":"AAAA;EACE,qBAAqB;EACrB,cAAc;EACd,uBAAuB;MACnB,oBAAoB;EACxB,sBAAsB;MAClB,wBAAwB;EAC5B,gBAAgB;EAChB,gBAAgB;EAChB,aAAa;EACb,wBAAwB;EACxB,mBAAmB;EACnB,YAAY;CACb;;AAED;EACE,UAAU;CACX;;AAED;EACE,iBAAiB;EACjB,YAAY;CACb;;AAED;EACE,sBAAsB;EACtB,iBAAiB;CAClB","file":"ErrorPage.css","sourcesContent":["html {\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-align: center;\n      align-items: center;\n  -ms-flex-pack: center;\n      justify-content: center;\n  padding: 0 32px;\n  padding: 0 2rem;\n  height: 100%;\n  font-family: sans-serif;\n  text-align: center;\n  color: #888;\n}\n\nbody {\n  margin: 0;\n}\n\nh1 {\n  font-weight: 400;\n  color: #555;\n}\n\npre {\n  white-space: pre-wrap;\n  text-align: left;\n}\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./src/DOMUtils.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateTag", function() { return updateTag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateMeta", function() { return updateMeta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCustomMeta", function() { return updateCustomMeta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateLink", function() { return updateLink; });
function updateTag(tagName, keyName, keyValue, attrName, attrValue) {
  var node = document.head.querySelector("".concat(tagName, "[").concat(keyName, "=\"").concat(keyValue, "\"]"));
  if (node && node.getAttribute(attrName) === attrValue) return; // Remove and create a new tag in order to make it work with bookmarks in Safari

  if (node) {
    node.parentNode.removeChild(node);
  }

  if (typeof attrValue === 'string') {
    var nextNode = document.createElement(tagName);
    nextNode.setAttribute(keyName, keyValue);
    nextNode.setAttribute(attrName, attrValue);
    document.head.appendChild(nextNode);
  }
}
function updateMeta(name, content) {
  updateTag('meta', 'name', name, 'content', content);
}
function updateCustomMeta(property, content) {
  updateTag('meta', 'property', property, 'content', content);
}
function updateLink(rel, href) {
  updateTag('link', 'rel', rel, 'href', href);
}

/***/ }),

/***/ "./src/client.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/whatwg-fetch/fetch.js");
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(whatwg_fetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_deep_force_update__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./node_modules/react-deep-force-update/lib/index.js");
/* harmony import */ var react_deep_force_update__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_deep_force_update__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./node_modules/query-string/index.js");
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(query_string__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./node_modules/history/esm/history.js");
/* harmony import */ var _components_App__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./src/components/App.js");
/* harmony import */ var _createFetch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./src/createFetch.js");
/* harmony import */ var _history__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("./src/history.js");
/* harmony import */ var _DOMUtils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("./src/DOMUtils.js");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("./src/router.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/client.js";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }












 // Global (context) variables that can be easily accessed from any React component
// https://facebook.github.io/react/docs/context.html

var context = {
  // Enables critical path CSS rendering
  // https://github.com/kriasoft/isomorphic-style-loader
  insertCss: function insertCss() {
    for (var _len = arguments.length, styles = new Array(_len), _key = 0; _key < _len; _key++) {
      styles[_key] = arguments[_key];
    }

    // eslint-disable-next-line no-underscore-dangle
    var removeCss = styles.map(function (x) {
      return x._insertCss();
    });
    return function () {
      removeCss.forEach(function (f) {
        return f();
      });
    };
  },
  // Universal HTTP client
  fetch: Object(_createFetch__WEBPACK_IMPORTED_MODULE_8__["default"])(fetch, {
    baseUrl: window.App.apiUrl
  }),
  GRAPHQL_API_URL: "".concat(window.App.serverUrl)
};
var container = document.getElementById('app');
var currentLocation = _history__WEBPACK_IMPORTED_MODULE_9__["default"].location;
var appInstance;
var scrollPositionsHistory = {}; // Re-render the app when window.location changes

function onLocationChange(_x, _x2) {
  return _onLocationChange.apply(this, arguments);
} // Handle client-side navigation by using HTML5 History API
// For more information visit https://github.com/mjackson/history#readme


function _onLocationChange() {
  _onLocationChange = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(location, action) {
    var isInitialRender, route, renderReactApp;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // Remember the latest scroll position for the previous location
            scrollPositionsHistory[currentLocation.key] = {
              scrollX: window.pageXOffset,
              scrollY: window.pageYOffset
            }; // Delete stored scroll position for next page if any

            if (action === 'PUSH') {
              delete scrollPositionsHistory[location.key];
            }

            currentLocation = location;
            isInitialRender = !action;
            _context.prev = 4;
            context.pathname = location.pathname;
            context.query = query_string__WEBPACK_IMPORTED_MODULE_4___default.a.parse(location.search); // Traverses the list of routes in the order they are defined until
            // it finds the first route that matches provided URL path string
            // and whose action method returns anything other than `undefined`.

            _context.next = 9;
            return _router__WEBPACK_IMPORTED_MODULE_11__["default"].resolve(context);

          case 9:
            route = _context.sent;

            if (!(currentLocation.key !== location.key)) {
              _context.next = 12;
              break;
            }

            return _context.abrupt("return");

          case 12:
            if (!route.redirect) {
              _context.next = 15;
              break;
            }

            _history__WEBPACK_IMPORTED_MODULE_9__["default"].replace(route.redirect);
            return _context.abrupt("return");

          case 15:
            renderReactApp = isInitialRender ? react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.hydrate : react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.render;
            appInstance = renderReactApp(react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_App__WEBPACK_IMPORTED_MODULE_7__["default"], {
              context: context,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 74
              },
              __self: this
            }, route.component), container, function () {
              if (isInitialRender) {
                // Switch off the native scroll restoration behavior and handle it manually
                // https://developers.google.com/web/updates/2015/09/history-api-scroll-restoration
                if (window.history && 'scrollRestoration' in window.history) {
                  window.history.scrollRestoration = 'manual';
                }

                var elem = document.getElementById('css');
                if (elem) elem.parentNode.removeChild(elem);
                return;
              }

              document.title = route.title;
              Object(_DOMUtils__WEBPACK_IMPORTED_MODULE_10__["updateMeta"])('description', route.description); // Update necessary tags in <head> at runtime here, ie:
              // updateMeta('keywords', route.keywords);
              // updateCustomMeta('og:url', route.canonicalUrl);
              // updateCustomMeta('og:image', route.imageUrl);
              // updateLink('canonical', route.canonicalUrl);
              // etc.

              var scrollX = 0;
              var scrollY = 0;
              var pos = scrollPositionsHistory[location.key];

              if (pos) {
                scrollX = pos.scrollX;
                scrollY = pos.scrollY;
              } else {
                var targetHash = location.hash.substr(1);

                if (targetHash) {
                  var target = document.getElementById(targetHash);

                  if (target) {
                    scrollY = window.pageYOffset + target.getBoundingClientRect().top;
                  }
                }
              }

              react_ga__WEBPACK_IMPORTED_MODULE_5__["default"].initialize('UA-203324633-1', {
                debug: false
              }); // Restore the scroll position if it was saved into the state
              // or scroll to the given #hash anchor
              // or scroll to top of the page

              window.scrollTo(scrollX, scrollY); // Google Analytics tracking. Don't send 'pageview' event after
              // the initial rendering, as it was already sent

              if (window.ga) {
                react_ga__WEBPACK_IMPORTED_MODULE_5__["default"].pageview(window.location.href);
                window.ga('send', 'pageview', Object(history__WEBPACK_IMPORTED_MODULE_6__["createPath"])(location));
              }
            });
            _context.next = 25;
            break;

          case 19:
            _context.prev = 19;
            _context.t0 = _context["catch"](4);

            if (false) {}

            throw _context.t0;

          case 23:
            console.error(_context.t0); // Do a full page reload if error occurs during client-side navigation

            if (!isInitialRender && currentLocation.key === location.key) {
              console.error('RSK will reload your page after error');
              window.location.reload();
            }

          case 25:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[4, 19]]);
  }));
  return _onLocationChange.apply(this, arguments);
}

_history__WEBPACK_IMPORTED_MODULE_9__["default"].listen(onLocationChange);
onLocationChange(currentLocation); // Enable Hot Module Replacement (HMR)

if (false) {}

/***/ }),

/***/ "./src/components/App.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var ContextType = {
  // Enables critical path CSS rendering
  // https://github.com/kriasoft/isomorphic-style-loader
  insertCss: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  // Universal HTTP client
  fetch: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  pathname: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  query: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  GRAPHQL_API_URL: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
};
/**
 * The top-level React component setting context (global) variables
 * that can be accessed from all the child components.
 *
 * https://facebook.github.io/react/docs/context.html
 *
 * Usage example:
 *
 *   const context = {
 *     history: createBrowserHistory(),
 *     store: createStore(),
 *   };
 *
 *   ReactDOM.render(
 *     <App context={context}>
 *       <Layout>
 *         <LandingPage />
 *       </Layout>
 *     </App>,
 *     container,
 *   );
 */

var App =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(App, _React$PureComponent);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, _getPrototypeOf(App).apply(this, arguments));
  }

  _createClass(App, [{
    key: "getChildContext",
    value: function getChildContext() {
      return this.props.context;
    }
  }, {
    key: "render",
    value: function render() {
      // NOTE: If you need to add or modify header, footer etc. of the app,
      // please do that inside the Layout component.
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.only(this.props.children);
    }
  }]);

  return App;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.PureComponent);

_defineProperty(App, "propTypes", {
  context: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape(ContextType).isRequired,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.element.isRequired
});

_defineProperty(App, "childContextTypes", ContextType);

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/createFetch.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Creates a wrapper function around the HTML5 Fetch API that provides
 * default arguments to fetch(...) and is intended to reduce the amount
 * of boilerplate code in the application.
 * https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch
 */
function createFetch(fetch, _ref) {
  var baseUrl = _ref.baseUrl,
      cookie = _ref.cookie,
      schema = _ref.schema,
      graphql = _ref.graphql;
  // NOTE: Tweak the default options to suite your application needs
  var defaults = {
    method: 'POST',
    // handy with GraphQL backends
    mode: baseUrl ? 'cors' : 'same-origin',
    credentials: baseUrl ? 'include' : 'same-origin',
    headers: _objectSpread({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }, cookie ? {
      Cookie: cookie
    } : null)
  };
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(url, options) {
        var isGraphQL, query, result;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                isGraphQL = url.startsWith('/graphql');

                if (!(schema && graphql && isGraphQL)) {
                  _context.next = 7;
                  break;
                }

                // We're SSR, so route the graphql internal to avoid latency
                query = JSON.parse(options.body);
                _context.next = 5;
                return graphql(schema, query.query, {
                  request: {}
                }, // fill in request vars needed by graphql
                null, query.variables);

              case 5:
                result = _context.sent;
                return _context.abrupt("return", Promise.resolve({
                  status: result.errors ? 400 : 200,
                  json: function json() {
                    return Promise.resolve(result);
                  }
                }));

              case 7:
                return _context.abrupt("return", isGraphQL || url.startsWith('/api') ? fetch("".concat(baseUrl).concat(url), _objectSpread({}, defaults, {}, options, {
                  headers: _objectSpread({}, defaults.headers, {}, options && options.headers)
                })) : fetch(url, options));

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x, _x2) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
}

/* harmony default export */ __webpack_exports__["default"] = (createFetch);

/***/ }),

/***/ "./src/history.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/history/createBrowserHistory.js");
/* harmony import */ var history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0__);
 // Navigation manager, e.g. history.push('/home')
// https://github.com/mjackson/history

/* harmony default export */ __webpack_exports__["default"] = ( true && history_createBrowserHistory__WEBPACK_IMPORTED_MODULE_0___default()());

/***/ }),

/***/ "./src/router.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var universal_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/universal-router/module.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/index.js");


/* harmony default export */ __webpack_exports__["default"] = (new universal_router__WEBPACK_IMPORTED_MODULE_0__["default"](_routes__WEBPACK_IMPORTED_MODULE_1__["default"], {
  resolveRoute: function resolveRoute(context, params) {
    if (typeof context.route.load === 'function') {
      return context.route.load().then(function (action) {
        return action["default"](context, params);
      });
    }

    if (typeof context.route.action === 'function') {
      return context.route.action(context, params);
    }

    return undefined;
  }
}));

/***/ }),

/***/ "./src/routes/error/ErrorPage.css":
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__("./node_modules/css-loader/index.js?!./node_modules/postcss-loader/lib/index.js?!./src/routes/error/ErrorPage.css");
    var insertCss = __webpack_require__("./node_modules/isomorphic-style-loader/lib/insertCss.js");

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./src/routes/error/ErrorPage.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorPageWithoutStyle", function() { return ErrorPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/isomorphic-style-loader/lib/withStyles.js");
/* harmony import */ var isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ErrorPage_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./src/routes/error/ErrorPage.css");
/* harmony import */ var _ErrorPage_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ErrorPage_css__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/error/ErrorPage.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var ErrorPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ErrorPage, _React$Component);

  function ErrorPage() {
    _classCallCheck(this, ErrorPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(ErrorPage).apply(this, arguments));
  }

  _createClass(ErrorPage, [{
    key: "render",
    value: function render() {
      if ( true && this.props.error) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 22
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 23
          },
          __self: this
        }, this.props.error.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("pre", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 24
          },
          __self: this
        }, this.props.error.stack));
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      }, "Error"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        },
        __self: this
      }, "Sorry, a critical error occurred on this page."));
    }
  }]);

  return ErrorPage;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(ErrorPage, "propTypes", {
  error: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    name: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
    message: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
    stack: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
  })
});

_defineProperty(ErrorPage, "defaultProps", {
  error: null
});


/* harmony default export */ __webpack_exports__["default"] = (isomorphic_style_loader_lib_withStyles__WEBPACK_IMPORTED_MODULE_2___default()(_ErrorPage_css__WEBPACK_IMPORTED_MODULE_3___default.a)(ErrorPage));

/***/ }),

/***/ "./src/routes/error/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ErrorPage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./src/routes/error/ErrorPage.js");
var _jsxFileName = "/home/praveen/Desktop/Egnify project/egnify-website/src/routes/error/index.js";



function action() {
  return {
    title: 'Demo Error',
    component: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ErrorPage__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7
      },
      __self: this
    })
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),

/***/ "./src/routes/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* eslint-disable global-require */
// The top-level (parent) route
var routes = {
  path: '',
  // Keep in mind, routes are evaluated in order
  children: [{
    path: '',
    load: function load() {
      return Promise.all(/* import() | GetRanks */[__webpack_require__.e(2), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("GetRanks")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/index.js"));
    }
  }, // {
  //   path: '/home',
  //   load: () => import(/* webpackChunkName: 'home' */ './home'),
  // },
  // {
  //   path: '/culture',
  //   load: () => import(/* webpackChunkName: 'Culture' */ './culture'),
  // },
  {
    path: '/request-demo',
    load: function load() {
      return Promise.all(/* import() */[__webpack_require__.e(2), __webpack_require__.e(3), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(4)]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/new-requestdemo/index.js"));
    }
  }, {
    path: '/company/careers',
    load: function load() {
      return Promise.all(/* import() | Careers */[__webpack_require__.e(0), __webpack_require__.e("Careers")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/careers/index.js"));
    }
  }, {
    path: '/customers',
    load: function load() {
      return Promise.all(/* import() | Customers */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Customers")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/customers/index.js"));
    }
  }, {
    path: '/pricing',
    load: function load() {
      return Promise.all(/* import() | Pricing */[__webpack_require__.e(2), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Pricing")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/pricing/index.js"));
    }
  }, {
    path: '/pricing/viewDetails',
    load: function load() {
      return Promise.all(/* import() | PricingDetails */[__webpack_require__.e(0), __webpack_require__.e("PricingDetails")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/PricingViewDetails/index.js"));
    }
  }, {
    path: '/company/aboutus',
    load: function load() {
      return Promise.all(/* import() | Aboutus */[__webpack_require__.e(2), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Aboutus")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/aboutus/index.js"));
    }
  }, {
    path: '/company/culture',
    load: function load() {
      return Promise.all(/* import() | Culture */[__webpack_require__.e(0), __webpack_require__.e("Culture")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/culture/index.js"));
    }
  }, {
    path: '/company/press',
    load: function load() {
      return Promise.all(/* import() | Press */[__webpack_require__.e(2), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Press")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/press/index.js"));
    }
  }, {
    path: '/how-egnify-works',
    load: function load() {
      return Promise.all(/* import() | HowWorks */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("HowWorks")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/HowWorks/index.js"));
    }
  }, {
    path: '/modules/tests',
    load: function load() {
      return Promise.all(/* import() | Tests */[__webpack_require__.e(2), __webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Tests")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/Test/index.js"));
    }
  }, {
    path: '/modules/liveclasses',
    load: function load() {
      return Promise.all(/* import() | Live Classes */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Live Classes")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/Teach/index.js"));
    }
  }, {
    path: '/modules/doubts',
    load: function load() {
      return Promise.all(/* import() | Doubts */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Doubts")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/Doubts/index.js"));
    }
  }, {
    path: '/modules/assignments',
    load: function load() {
      return Promise.all(/* import() | Assignments */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Assignments")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/Assignments/index.js"));
    }
  }, {
    path: '/modules/connect',
    load: function load() {
      return Promise.all(/* import() | Connect */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e("Connect")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/Connect/index.js"));
    }
  }, {
    path: '/team',
    load: function load() {
      return Promise.all(/* import() | Team */[__webpack_require__.e(3), __webpack_require__.e(0), __webpack_require__.e("Acads~Jeet~Otp~Team"), __webpack_require__.e("Team")]).then(__webpack_require__.bind(null, "./src/routes/team/index.js"));
    }
  }, {
    path: '/products/jeet',
    load: function load() {
      return Promise.all(/* import() | Jeet */[__webpack_require__.e(3), __webpack_require__.e(0), __webpack_require__.e("Acads~Jeet~Otp~Team"), __webpack_require__.e("Acads~Jeet~Otp"), __webpack_require__.e("Jeet")]).then(__webpack_require__.bind(null, "./src/routes/products/jeet/index.js"));
    }
  }, {
    path: '/products/acads',
    load: function load() {
      return Promise.all(/* import() | Acads */[__webpack_require__.e(3), __webpack_require__.e(0), __webpack_require__.e("Acads~Jeet~Otp~Team"), __webpack_require__.e("Acads~Jeet~Otp"), __webpack_require__.e("Acads")]).then(__webpack_require__.bind(null, "./src/routes/products/acads/index.js"));
    }
  }, {
    path: '/products/prep',
    load: function load() {
      return Promise.all(/* import() | Otp */[__webpack_require__.e(3), __webpack_require__.e(0), __webpack_require__.e("Acads~Jeet~Otp~Team"), __webpack_require__.e("Acads~Jeet~Otp"), __webpack_require__.e("Otp")]).then(__webpack_require__.bind(null, "./src/routes/products/otp/index.js"));
    }
  }, {
    path: '/products/qms',
    load: function load() {
      return Promise.all(/* import() | qms */[__webpack_require__.e(0), __webpack_require__.e("qms")]).then(__webpack_require__.bind(null, "./src/routes/products/qms/index.js"));
    }
  }, // {
  //   path: '/request-demo',
  //   load: () =>
  //     import(/* webpackChunkName: 'RequestDemo' */ './request-demo'),
  // },
  {
    path: '/privacy-and-terms',
    load: function load() {
      return Promise.all(/* import() | PrivacyAndTerms */[__webpack_require__.e(0), __webpack_require__.e("PrivacyAndTerms")]).then(__webpack_require__.bind(null, "./src/routes/privacy-and-terms/index.js"));
    }
  }, {
    path: '/terms-and-conditions',
    load: function load() {
      return Promise.all(/* import() | TermsAndConditions */[__webpack_require__.e(0), __webpack_require__.e("TermsAndConditions")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/privacyAndTerms/index.js"));
    }
  }, {
    path: '/cancellation-and-return',
    load: function load() {
      return Promise.all(/* import() | CancellationAndReturn */[__webpack_require__.e(0), __webpack_require__.e("CancellationAndReturn")]).then(__webpack_require__.bind(null, "./src/routes/products/get-ranks/cancellationAndReturn/index.js"));
    }
  }, {
    path: '/score',
    load: function load() {
      return __webpack_require__.e(/* import() | Score */ "Score").then(__webpack_require__.bind(null, "./src/routes/score/index.js"));
    }
  }, // Wildcard routes, e.g. { path: '(.*)', ... } (must go last)
  {
    path: '(.*)',
    load: function load() {
      return Promise.all(/* import() | LaunchingSoon */[__webpack_require__.e(0), __webpack_require__.e("LaunchingSoon")]).then(__webpack_require__.bind(null, "./src/routes/launching-soon/index.js"));
    }
  } // {
  //   path: '(.*)',
  //   load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
  // },
  ],
  action: function () {
    var _action = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(_ref) {
      var next, route;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              next = _ref.next;
              _context.next = 3;
              return next();

            case 3:
              route = _context.sent;
              // Provide default values for title, description etc.
              route.title = "".concat(route.title || 'Untitled Page');
              route.description = route.description || '';
              return _context.abrupt("return", route);

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function action(_x) {
      return _action.apply(this, arguments);
    }

    return action;
  }()
}; // The error page is available by permanent url for development mode

if (true) {
  routes.children.unshift({
    path: '/error',
    action: __webpack_require__("./src/routes/error/index.js")["default"]
  });
}

/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./node_modules/@babel/polyfill/lib/index.js");
module.exports = __webpack_require__("./src/client.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlcyI6WyIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvd2VicGFjay9ib290c3RyYXAiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9lcnJvci9FcnJvclBhZ2UuY3NzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9ET01VdGlscy5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY2xpZW50LmpzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9jb21wb25lbnRzL0FwcC5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvY3JlYXRlRmV0Y2guanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL2hpc3RvcnkuanMiLCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL2Vycm9yL0Vycm9yUGFnZS5jc3M/Yjg3NCIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL2Vycm9yL0Vycm9yUGFnZS5qcyIsIi9ob21lL3ByYXZlZW4vRGVza3RvcC9FZ25pZnkgcHJvamVjdC9lZ25pZnktd2Vic2l0ZS9zcmMvcm91dGVzL2Vycm9yL2luZGV4LmpzIiwiL2hvbWUvcHJhdmVlbi9EZXNrdG9wL0VnbmlmeSBwcm9qZWN0L2VnbmlmeS13ZWJzaXRlL3NyYy9yb3V0ZXMvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImNsaWVudFwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIHNjcmlwdCBwYXRoIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBqc29ucFNjcmlwdFNyYyhjaHVua0lkKSB7XG4gXHRcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fLnAgKyBcIlwiICsgKHtcIkFzc2lnbm1lbnRzXCI6XCJBc3NpZ25tZW50c1wiLFwiQ29ubmVjdFwiOlwiQ29ubmVjdFwiLFwiQ3VzdG9tZXJzXCI6XCJDdXN0b21lcnNcIixcIkRvdWJ0c1wiOlwiRG91YnRzXCIsXCJIb3dXb3Jrc1wiOlwiSG93V29ya3NcIixcIkxpdmUgQ2xhc3Nlc1wiOlwiTGl2ZSBDbGFzc2VzXCIsXCJDYW5jZWxsYXRpb25BbmRSZXR1cm5cIjpcIkNhbmNlbGxhdGlvbkFuZFJldHVyblwiLFwiQ2FyZWVyc1wiOlwiQ2FyZWVyc1wiLFwiQ3VsdHVyZVwiOlwiQ3VsdHVyZVwiLFwiTGF1bmNoaW5nU29vblwiOlwiTGF1bmNoaW5nU29vblwiLFwiUHJpY2luZ0RldGFpbHNcIjpcIlByaWNpbmdEZXRhaWxzXCIsXCJQcml2YWN5QW5kVGVybXNcIjpcIlByaXZhY3lBbmRUZXJtc1wiLFwiVGVybXNBbmRDb25kaXRpb25zXCI6XCJUZXJtc0FuZENvbmRpdGlvbnNcIixcInFtc1wiOlwicW1zXCIsXCJBYm91dHVzXCI6XCJBYm91dHVzXCIsXCJHZXRSYW5rc1wiOlwiR2V0UmFua3NcIixcIlByZXNzXCI6XCJQcmVzc1wiLFwiUHJpY2luZ1wiOlwiUHJpY2luZ1wiLFwiVGVzdHNcIjpcIlRlc3RzXCIsXCJBY2Fkc35KZWV0fk90cH5UZWFtXCI6XCJBY2Fkc35KZWV0fk90cH5UZWFtXCIsXCJBY2Fkc35KZWV0fk90cFwiOlwiQWNhZHN+SmVldH5PdHBcIixcIkFjYWRzXCI6XCJBY2Fkc1wiLFwiSmVldFwiOlwiSmVldFwiLFwiT3RwXCI6XCJPdHBcIixcIlRlYW1cIjpcIlRlYW1cIixcIlNjb3JlXCI6XCJTY29yZVwifVtjaHVua0lkXXx8Y2h1bmtJZCkgKyBcIi5jaHVuay5qc1wiXG4gXHR9XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuIFx0Ly8gVGhpcyBmaWxlIGNvbnRhaW5zIG9ubHkgdGhlIGVudHJ5IGNodW5rLlxuIFx0Ly8gVGhlIGNodW5rIGxvYWRpbmcgZnVuY3Rpb24gZm9yIGFkZGl0aW9uYWwgY2h1bmtzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmUgPSBmdW5jdGlvbiByZXF1aXJlRW5zdXJlKGNodW5rSWQpIHtcbiBcdFx0dmFyIHByb21pc2VzID0gW107XG5cblxuIFx0XHQvLyBKU09OUCBjaHVuayBsb2FkaW5nIGZvciBqYXZhc2NyaXB0XG5cbiBcdFx0dmFyIGluc3RhbGxlZENodW5rRGF0YSA9IGluc3RhbGxlZENodW5rc1tjaHVua0lkXTtcbiBcdFx0aWYoaW5zdGFsbGVkQ2h1bmtEYXRhICE9PSAwKSB7IC8vIDAgbWVhbnMgXCJhbHJlYWR5IGluc3RhbGxlZFwiLlxuXG4gXHRcdFx0Ly8gYSBQcm9taXNlIG1lYW5zIFwiY3VycmVudGx5IGxvYWRpbmdcIi5cbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua0RhdGEpIHtcbiBcdFx0XHRcdHByb21pc2VzLnB1c2goaW5zdGFsbGVkQ2h1bmtEYXRhWzJdKTtcbiBcdFx0XHR9IGVsc2Uge1xuIFx0XHRcdFx0Ly8gc2V0dXAgUHJvbWlzZSBpbiBjaHVuayBjYWNoZVxuIFx0XHRcdFx0dmFyIHByb21pc2UgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcbiBcdFx0XHRcdFx0aW5zdGFsbGVkQ2h1bmtEYXRhID0gaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gW3Jlc29sdmUsIHJlamVjdF07XG4gXHRcdFx0XHR9KTtcbiBcdFx0XHRcdHByb21pc2VzLnB1c2goaW5zdGFsbGVkQ2h1bmtEYXRhWzJdID0gcHJvbWlzZSk7XG5cbiBcdFx0XHRcdC8vIHN0YXJ0IGNodW5rIGxvYWRpbmdcbiBcdFx0XHRcdHZhciBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiBcdFx0XHRcdHZhciBvblNjcmlwdENvbXBsZXRlO1xuXG4gXHRcdFx0XHRzY3JpcHQuY2hhcnNldCA9ICd1dGYtOCc7XG4gXHRcdFx0XHRzY3JpcHQudGltZW91dCA9IDEyMDtcbiBcdFx0XHRcdGlmIChfX3dlYnBhY2tfcmVxdWlyZV9fLm5jKSB7XG4gXHRcdFx0XHRcdHNjcmlwdC5zZXRBdHRyaWJ1dGUoXCJub25jZVwiLCBfX3dlYnBhY2tfcmVxdWlyZV9fLm5jKTtcbiBcdFx0XHRcdH1cbiBcdFx0XHRcdHNjcmlwdC5zcmMgPSBqc29ucFNjcmlwdFNyYyhjaHVua0lkKTtcblxuIFx0XHRcdFx0Ly8gY3JlYXRlIGVycm9yIGJlZm9yZSBzdGFjayB1bndvdW5kIHRvIGdldCB1c2VmdWwgc3RhY2t0cmFjZSBsYXRlclxuIFx0XHRcdFx0dmFyIGVycm9yID0gbmV3IEVycm9yKCk7XG4gXHRcdFx0XHRvblNjcmlwdENvbXBsZXRlID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gXHRcdFx0XHRcdC8vIGF2b2lkIG1lbSBsZWFrcyBpbiBJRS5cbiBcdFx0XHRcdFx0c2NyaXB0Lm9uZXJyb3IgPSBzY3JpcHQub25sb2FkID0gbnVsbDtcbiBcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xuIFx0XHRcdFx0XHR2YXIgY2h1bmsgPSBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF07XG4gXHRcdFx0XHRcdGlmKGNodW5rICE9PSAwKSB7XG4gXHRcdFx0XHRcdFx0aWYoY2h1bmspIHtcbiBcdFx0XHRcdFx0XHRcdHZhciBlcnJvclR5cGUgPSBldmVudCAmJiAoZXZlbnQudHlwZSA9PT0gJ2xvYWQnID8gJ21pc3NpbmcnIDogZXZlbnQudHlwZSk7XG4gXHRcdFx0XHRcdFx0XHR2YXIgcmVhbFNyYyA9IGV2ZW50ICYmIGV2ZW50LnRhcmdldCAmJiBldmVudC50YXJnZXQuc3JjO1xuIFx0XHRcdFx0XHRcdFx0ZXJyb3IubWVzc2FnZSA9ICdMb2FkaW5nIGNodW5rICcgKyBjaHVua0lkICsgJyBmYWlsZWQuXFxuKCcgKyBlcnJvclR5cGUgKyAnOiAnICsgcmVhbFNyYyArICcpJztcbiBcdFx0XHRcdFx0XHRcdGVycm9yLm5hbWUgPSAnQ2h1bmtMb2FkRXJyb3InO1xuIFx0XHRcdFx0XHRcdFx0ZXJyb3IudHlwZSA9IGVycm9yVHlwZTtcbiBcdFx0XHRcdFx0XHRcdGVycm9yLnJlcXVlc3QgPSByZWFsU3JjO1xuIFx0XHRcdFx0XHRcdFx0Y2h1bmtbMV0oZXJyb3IpO1xuIFx0XHRcdFx0XHRcdH1cbiBcdFx0XHRcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSB1bmRlZmluZWQ7XG4gXHRcdFx0XHRcdH1cbiBcdFx0XHRcdH07XG4gXHRcdFx0XHR2YXIgdGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiBcdFx0XHRcdFx0b25TY3JpcHRDb21wbGV0ZSh7IHR5cGU6ICd0aW1lb3V0JywgdGFyZ2V0OiBzY3JpcHQgfSk7XG4gXHRcdFx0XHR9LCAxMjAwMDApO1xuIFx0XHRcdFx0c2NyaXB0Lm9uZXJyb3IgPSBzY3JpcHQub25sb2FkID0gb25TY3JpcHRDb21wbGV0ZTtcbiBcdFx0XHRcdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQoc2NyaXB0KTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0cmV0dXJuIFByb21pc2UuYWxsKHByb21pc2VzKTtcbiBcdH07XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvYXNzZXRzL1wiO1xuXG4gXHQvLyBvbiBlcnJvciBmdW5jdGlvbiBmb3IgYXN5bmMgbG9hZGluZ1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vZSA9IGZ1bmN0aW9uKGVycikgeyBjb25zb2xlLmVycm9yKGVycik7IHRocm93IGVycjsgfTtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goWzAsXCJ2ZW5kb3JzXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcImh0bWwge1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcGFkZGluZzogMCAzMnB4O1xcbiAgcGFkZGluZzogMCAycmVtO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBjb2xvcjogIzg4ODtcXG59XFxuXFxuYm9keSB7XFxuICBtYXJnaW46IDA7XFxufVxcblxcbmgxIHtcXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XFxuICBjb2xvcjogIzU1NTtcXG59XFxuXFxucHJlIHtcXG4gIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvaG9tZS9wcmF2ZWVuL0Rlc2t0b3AvRWduaWZ5IHByb2plY3QvZWduaWZ5LXdlYnNpdGUvc3JjL3JvdXRlcy9lcnJvci9FcnJvclBhZ2UuY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCx1QkFBdUI7TUFDbkIsb0JBQW9CO0VBQ3hCLHNCQUFzQjtNQUNsQix3QkFBd0I7RUFDNUIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLG1CQUFtQjtFQUNuQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxVQUFVO0NBQ1g7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsWUFBWTtDQUNiOztBQUVEO0VBQ0Usc0JBQXNCO0VBQ3RCLGlCQUFpQjtDQUNsQlwiLFwiZmlsZVwiOlwiRXJyb3JQYWdlLmNzc1wiLFwic291cmNlc0NvbnRlbnRcIjpbXCJodG1sIHtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDAgMzJweDtcXG4gIHBhZGRpbmc6IDAgMnJlbTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6ICM4ODg7XFxufVxcblxcbmJvZHkge1xcbiAgbWFyZ2luOiAwO1xcbn1cXG5cXG5oMSB7XFxuICBmb250LXdlaWdodDogNDAwO1xcbiAgY29sb3I6ICM1NTU7XFxufVxcblxcbnByZSB7XFxuICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XFxuICB0ZXh0LWFsaWduOiBsZWZ0O1xcbn1cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuIiwiZXhwb3J0IGZ1bmN0aW9uIHVwZGF0ZVRhZyh0YWdOYW1lLCBrZXlOYW1lLCBrZXlWYWx1ZSwgYXR0ck5hbWUsIGF0dHJWYWx1ZSkge1xuICBjb25zdCBub2RlID0gZG9jdW1lbnQuaGVhZC5xdWVyeVNlbGVjdG9yKFxuICAgIGAke3RhZ05hbWV9WyR7a2V5TmFtZX09XCIke2tleVZhbHVlfVwiXWAsXG4gICk7XG4gIGlmIChub2RlICYmIG5vZGUuZ2V0QXR0cmlidXRlKGF0dHJOYW1lKSA9PT0gYXR0clZhbHVlKSByZXR1cm47XG5cbiAgLy8gUmVtb3ZlIGFuZCBjcmVhdGUgYSBuZXcgdGFnIGluIG9yZGVyIHRvIG1ha2UgaXQgd29yayB3aXRoIGJvb2ttYXJrcyBpbiBTYWZhcmlcbiAgaWYgKG5vZGUpIHtcbiAgICBub2RlLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQobm9kZSk7XG4gIH1cbiAgaWYgKHR5cGVvZiBhdHRyVmFsdWUgPT09ICdzdHJpbmcnKSB7XG4gICAgY29uc3QgbmV4dE5vZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRhZ05hbWUpO1xuICAgIG5leHROb2RlLnNldEF0dHJpYnV0ZShrZXlOYW1lLCBrZXlWYWx1ZSk7XG4gICAgbmV4dE5vZGUuc2V0QXR0cmlidXRlKGF0dHJOYW1lLCBhdHRyVmFsdWUpO1xuICAgIGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQobmV4dE5vZGUpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB1cGRhdGVNZXRhKG5hbWUsIGNvbnRlbnQpIHtcbiAgdXBkYXRlVGFnKCdtZXRhJywgJ25hbWUnLCBuYW1lLCAnY29udGVudCcsIGNvbnRlbnQpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdXBkYXRlQ3VzdG9tTWV0YShwcm9wZXJ0eSwgY29udGVudCkge1xuICB1cGRhdGVUYWcoJ21ldGEnLCAncHJvcGVydHknLCBwcm9wZXJ0eSwgJ2NvbnRlbnQnLCBjb250ZW50KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHVwZGF0ZUxpbmsocmVsLCBocmVmKSB7XG4gIHVwZGF0ZVRhZygnbGluaycsICdyZWwnLCByZWwsICdocmVmJywgaHJlZik7XG59XG4iLCJpbXBvcnQgJ3doYXR3Zy1mZXRjaCc7XG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgZGVlcEZvcmNlVXBkYXRlIGZyb20gJ3JlYWN0LWRlZXAtZm9yY2UtdXBkYXRlJztcbmltcG9ydCBxdWVyeVN0cmluZyBmcm9tICdxdWVyeS1zdHJpbmcnO1xuaW1wb3J0IFJlYWN0R0EgZnJvbSAncmVhY3QtZ2EnO1xuaW1wb3J0IHsgY3JlYXRlUGF0aCB9IGZyb20gJ2hpc3RvcnknO1xuaW1wb3J0IEFwcCBmcm9tICcuL2NvbXBvbmVudHMvQXBwJztcbmltcG9ydCBjcmVhdGVGZXRjaCBmcm9tICcuL2NyZWF0ZUZldGNoJztcbmltcG9ydCBoaXN0b3J5IGZyb20gJy4vaGlzdG9yeSc7XG5pbXBvcnQgeyB1cGRhdGVNZXRhIH0gZnJvbSAnLi9ET01VdGlscyc7XG5pbXBvcnQgcm91dGVyIGZyb20gJy4vcm91dGVyJztcblxuLy8gR2xvYmFsIChjb250ZXh0KSB2YXJpYWJsZXMgdGhhdCBjYW4gYmUgZWFzaWx5IGFjY2Vzc2VkIGZyb20gYW55IFJlYWN0IGNvbXBvbmVudFxuLy8gaHR0cHM6Ly9mYWNlYm9vay5naXRodWIuaW8vcmVhY3QvZG9jcy9jb250ZXh0Lmh0bWxcbmNvbnN0IGNvbnRleHQgPSB7XG4gIC8vIEVuYWJsZXMgY3JpdGljYWwgcGF0aCBDU1MgcmVuZGVyaW5nXG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9rcmlhc29mdC9pc29tb3JwaGljLXN0eWxlLWxvYWRlclxuICBpbnNlcnRDc3M6ICguLi5zdHlsZXMpID0+IHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZXJzY29yZS1kYW5nbGVcbiAgICBjb25zdCByZW1vdmVDc3MgPSBzdHlsZXMubWFwKHggPT4geC5faW5zZXJ0Q3NzKCkpO1xuICAgIHJldHVybiAoKSA9PiB7XG4gICAgICByZW1vdmVDc3MuZm9yRWFjaChmID0+IGYoKSk7XG4gICAgfTtcbiAgfSxcbiAgLy8gVW5pdmVyc2FsIEhUVFAgY2xpZW50XG4gIGZldGNoOiBjcmVhdGVGZXRjaChmZXRjaCwge1xuICAgIGJhc2VVcmw6IHdpbmRvdy5BcHAuYXBpVXJsLFxuICB9KSxcbiAgR1JBUEhRTF9BUElfVVJMOiBgJHt3aW5kb3cuQXBwLnNlcnZlclVybH1gLFxufTtcblxuY29uc3QgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FwcCcpO1xubGV0IGN1cnJlbnRMb2NhdGlvbiA9IGhpc3RvcnkubG9jYXRpb247XG5sZXQgYXBwSW5zdGFuY2U7XG5cbmNvbnN0IHNjcm9sbFBvc2l0aW9uc0hpc3RvcnkgPSB7fTtcblxuLy8gUmUtcmVuZGVyIHRoZSBhcHAgd2hlbiB3aW5kb3cubG9jYXRpb24gY2hhbmdlc1xuYXN5bmMgZnVuY3Rpb24gb25Mb2NhdGlvbkNoYW5nZShsb2NhdGlvbiwgYWN0aW9uKSB7XG4gIC8vIFJlbWVtYmVyIHRoZSBsYXRlc3Qgc2Nyb2xsIHBvc2l0aW9uIGZvciB0aGUgcHJldmlvdXMgbG9jYXRpb25cbiAgc2Nyb2xsUG9zaXRpb25zSGlzdG9yeVtjdXJyZW50TG9jYXRpb24ua2V5XSA9IHtcbiAgICBzY3JvbGxYOiB3aW5kb3cucGFnZVhPZmZzZXQsXG4gICAgc2Nyb2xsWTogd2luZG93LnBhZ2VZT2Zmc2V0LFxuICB9O1xuICAvLyBEZWxldGUgc3RvcmVkIHNjcm9sbCBwb3NpdGlvbiBmb3IgbmV4dCBwYWdlIGlmIGFueVxuICBpZiAoYWN0aW9uID09PSAnUFVTSCcpIHtcbiAgICBkZWxldGUgc2Nyb2xsUG9zaXRpb25zSGlzdG9yeVtsb2NhdGlvbi5rZXldO1xuICB9XG4gIGN1cnJlbnRMb2NhdGlvbiA9IGxvY2F0aW9uO1xuXG4gIGNvbnN0IGlzSW5pdGlhbFJlbmRlciA9ICFhY3Rpb247XG4gIHRyeSB7XG4gICAgY29udGV4dC5wYXRobmFtZSA9IGxvY2F0aW9uLnBhdGhuYW1lO1xuICAgIGNvbnRleHQucXVlcnkgPSBxdWVyeVN0cmluZy5wYXJzZShsb2NhdGlvbi5zZWFyY2gpO1xuXG4gICAgLy8gVHJhdmVyc2VzIHRoZSBsaXN0IG9mIHJvdXRlcyBpbiB0aGUgb3JkZXIgdGhleSBhcmUgZGVmaW5lZCB1bnRpbFxuICAgIC8vIGl0IGZpbmRzIHRoZSBmaXJzdCByb3V0ZSB0aGF0IG1hdGNoZXMgcHJvdmlkZWQgVVJMIHBhdGggc3RyaW5nXG4gICAgLy8gYW5kIHdob3NlIGFjdGlvbiBtZXRob2QgcmV0dXJucyBhbnl0aGluZyBvdGhlciB0aGFuIGB1bmRlZmluZWRgLlxuICAgIGNvbnN0IHJvdXRlID0gYXdhaXQgcm91dGVyLnJlc29sdmUoY29udGV4dCk7XG5cbiAgICAvLyBQcmV2ZW50IG11bHRpcGxlIHBhZ2UgcmVuZGVycyBkdXJpbmcgdGhlIHJvdXRpbmcgcHJvY2Vzc1xuICAgIGlmIChjdXJyZW50TG9jYXRpb24ua2V5ICE9PSBsb2NhdGlvbi5rZXkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAocm91dGUucmVkaXJlY3QpIHtcbiAgICAgIGhpc3RvcnkucmVwbGFjZShyb3V0ZS5yZWRpcmVjdCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgcmVuZGVyUmVhY3RBcHAgPSBpc0luaXRpYWxSZW5kZXIgPyBSZWFjdERPTS5oeWRyYXRlIDogUmVhY3RET00ucmVuZGVyO1xuICAgIGFwcEluc3RhbmNlID0gcmVuZGVyUmVhY3RBcHAoXG4gICAgICA8QXBwIGNvbnRleHQ9e2NvbnRleHR9Pntyb3V0ZS5jb21wb25lbnR9PC9BcHA+LFxuICAgICAgY29udGFpbmVyLFxuICAgICAgKCkgPT4ge1xuICAgICAgICBpZiAoaXNJbml0aWFsUmVuZGVyKSB7XG4gICAgICAgICAgLy8gU3dpdGNoIG9mZiB0aGUgbmF0aXZlIHNjcm9sbCByZXN0b3JhdGlvbiBiZWhhdmlvciBhbmQgaGFuZGxlIGl0IG1hbnVhbGx5XG4gICAgICAgICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXJzLmdvb2dsZS5jb20vd2ViL3VwZGF0ZXMvMjAxNS8wOS9oaXN0b3J5LWFwaS1zY3JvbGwtcmVzdG9yYXRpb25cbiAgICAgICAgICBpZiAod2luZG93Lmhpc3RvcnkgJiYgJ3Njcm9sbFJlc3RvcmF0aW9uJyBpbiB3aW5kb3cuaGlzdG9yeSkge1xuICAgICAgICAgICAgd2luZG93Lmhpc3Rvcnkuc2Nyb2xsUmVzdG9yYXRpb24gPSAnbWFudWFsJztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb25zdCBlbGVtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NzcycpO1xuICAgICAgICAgIGlmIChlbGVtKSBlbGVtLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZWxlbSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgZG9jdW1lbnQudGl0bGUgPSByb3V0ZS50aXRsZTtcblxuICAgICAgICB1cGRhdGVNZXRhKCdkZXNjcmlwdGlvbicsIHJvdXRlLmRlc2NyaXB0aW9uKTtcbiAgICAgICAgLy8gVXBkYXRlIG5lY2Vzc2FyeSB0YWdzIGluIDxoZWFkPiBhdCBydW50aW1lIGhlcmUsIGllOlxuICAgICAgICAvLyB1cGRhdGVNZXRhKCdrZXl3b3JkcycsIHJvdXRlLmtleXdvcmRzKTtcbiAgICAgICAgLy8gdXBkYXRlQ3VzdG9tTWV0YSgnb2c6dXJsJywgcm91dGUuY2Fub25pY2FsVXJsKTtcbiAgICAgICAgLy8gdXBkYXRlQ3VzdG9tTWV0YSgnb2c6aW1hZ2UnLCByb3V0ZS5pbWFnZVVybCk7XG4gICAgICAgIC8vIHVwZGF0ZUxpbmsoJ2Nhbm9uaWNhbCcsIHJvdXRlLmNhbm9uaWNhbFVybCk7XG4gICAgICAgIC8vIGV0Yy5cblxuICAgICAgICBsZXQgc2Nyb2xsWCA9IDA7XG4gICAgICAgIGxldCBzY3JvbGxZID0gMDtcbiAgICAgICAgY29uc3QgcG9zID0gc2Nyb2xsUG9zaXRpb25zSGlzdG9yeVtsb2NhdGlvbi5rZXldO1xuICAgICAgICBpZiAocG9zKSB7XG4gICAgICAgICAgc2Nyb2xsWCA9IHBvcy5zY3JvbGxYO1xuICAgICAgICAgIHNjcm9sbFkgPSBwb3Muc2Nyb2xsWTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zdCB0YXJnZXRIYXNoID0gbG9jYXRpb24uaGFzaC5zdWJzdHIoMSk7XG4gICAgICAgICAgaWYgKHRhcmdldEhhc2gpIHtcbiAgICAgICAgICAgIGNvbnN0IHRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldEhhc2gpO1xuICAgICAgICAgICAgaWYgKHRhcmdldCkge1xuICAgICAgICAgICAgICBzY3JvbGxZID0gd2luZG93LnBhZ2VZT2Zmc2V0ICsgdGFyZ2V0LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBSZWFjdEdBLmluaXRpYWxpemUoJ1VBLTIwMzMyNDYzMy0xJywge1xuICAgICAgICAgIGRlYnVnOiBmYWxzZSxcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIFJlc3RvcmUgdGhlIHNjcm9sbCBwb3NpdGlvbiBpZiBpdCB3YXMgc2F2ZWQgaW50byB0aGUgc3RhdGVcbiAgICAgICAgLy8gb3Igc2Nyb2xsIHRvIHRoZSBnaXZlbiAjaGFzaCBhbmNob3JcbiAgICAgICAgLy8gb3Igc2Nyb2xsIHRvIHRvcCBvZiB0aGUgcGFnZVxuICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oc2Nyb2xsWCwgc2Nyb2xsWSk7XG5cbiAgICAgICAgLy8gR29vZ2xlIEFuYWx5dGljcyB0cmFja2luZy4gRG9uJ3Qgc2VuZCAncGFnZXZpZXcnIGV2ZW50IGFmdGVyXG4gICAgICAgIC8vIHRoZSBpbml0aWFsIHJlbmRlcmluZywgYXMgaXQgd2FzIGFscmVhZHkgc2VudFxuICAgICAgICBpZiAod2luZG93LmdhKSB7XG4gICAgICAgICAgUmVhY3RHQS5wYWdldmlldyh3aW5kb3cubG9jYXRpb24uaHJlZik7XG4gICAgICAgICAgd2luZG93LmdhKCdzZW5kJywgJ3BhZ2V2aWV3JywgY3JlYXRlUGF0aChsb2NhdGlvbikpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICk7XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgaWYgKF9fREVWX18pIHtcbiAgICAgIHRocm93IGVycm9yO1xuICAgIH1cblxuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgLy8gRG8gYSBmdWxsIHBhZ2UgcmVsb2FkIGlmIGVycm9yIG9jY3VycyBkdXJpbmcgY2xpZW50LXNpZGUgbmF2aWdhdGlvblxuICAgIGlmICghaXNJbml0aWFsUmVuZGVyICYmIGN1cnJlbnRMb2NhdGlvbi5rZXkgPT09IGxvY2F0aW9uLmtleSkge1xuICAgICAgY29uc29sZS5lcnJvcignUlNLIHdpbGwgcmVsb2FkIHlvdXIgcGFnZSBhZnRlciBlcnJvcicpO1xuICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xuICAgIH1cbiAgfVxufVxuXG4vLyBIYW5kbGUgY2xpZW50LXNpZGUgbmF2aWdhdGlvbiBieSB1c2luZyBIVE1MNSBIaXN0b3J5IEFQSVxuLy8gRm9yIG1vcmUgaW5mb3JtYXRpb24gdmlzaXQgaHR0cHM6Ly9naXRodWIuY29tL21qYWNrc29uL2hpc3RvcnkjcmVhZG1lXG5oaXN0b3J5Lmxpc3RlbihvbkxvY2F0aW9uQ2hhbmdlKTtcbm9uTG9jYXRpb25DaGFuZ2UoY3VycmVudExvY2F0aW9uKTtcblxuLy8gRW5hYmxlIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnQgKEhNUilcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KCcuL3JvdXRlcicsICgpID0+IHtcbiAgICBpZiAoYXBwSW5zdGFuY2UgJiYgYXBwSW5zdGFuY2UudXBkYXRlci5pc01vdW50ZWQoYXBwSW5zdGFuY2UpKSB7XG4gICAgICAvLyBGb3JjZS11cGRhdGUgdGhlIHdob2xlIHRyZWUsIGluY2x1ZGluZyBjb21wb25lbnRzIHRoYXQgcmVmdXNlIHRvIHVwZGF0ZVxuICAgICAgZGVlcEZvcmNlVXBkYXRlKGFwcEluc3RhbmNlKTtcbiAgICB9XG5cbiAgICBvbkxvY2F0aW9uQ2hhbmdlKGN1cnJlbnRMb2NhdGlvbik7XG4gIH0pO1xufVxuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmNvbnN0IENvbnRleHRUeXBlID0ge1xuICAvLyBFbmFibGVzIGNyaXRpY2FsIHBhdGggQ1NTIHJlbmRlcmluZ1xuICAvLyBodHRwczovL2dpdGh1Yi5jb20va3JpYXNvZnQvaXNvbW9ycGhpYy1zdHlsZS1sb2FkZXJcbiAgaW5zZXJ0Q3NzOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICAvLyBVbml2ZXJzYWwgSFRUUCBjbGllbnRcbiAgZmV0Y2g6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIHBhdGhuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIHF1ZXJ5OiBQcm9wVHlwZXMub2JqZWN0LFxuICBHUkFQSFFMX0FQSV9VUkw6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbn07XG5cbi8qKlxuICogVGhlIHRvcC1sZXZlbCBSZWFjdCBjb21wb25lbnQgc2V0dGluZyBjb250ZXh0IChnbG9iYWwpIHZhcmlhYmxlc1xuICogdGhhdCBjYW4gYmUgYWNjZXNzZWQgZnJvbSBhbGwgdGhlIGNoaWxkIGNvbXBvbmVudHMuXG4gKlxuICogaHR0cHM6Ly9mYWNlYm9vay5naXRodWIuaW8vcmVhY3QvZG9jcy9jb250ZXh0Lmh0bWxcbiAqXG4gKiBVc2FnZSBleGFtcGxlOlxuICpcbiAqICAgY29uc3QgY29udGV4dCA9IHtcbiAqICAgICBoaXN0b3J5OiBjcmVhdGVCcm93c2VySGlzdG9yeSgpLFxuICogICAgIHN0b3JlOiBjcmVhdGVTdG9yZSgpLFxuICogICB9O1xuICpcbiAqICAgUmVhY3RET00ucmVuZGVyKFxuICogICAgIDxBcHAgY29udGV4dD17Y29udGV4dH0+XG4gKiAgICAgICA8TGF5b3V0PlxuICogICAgICAgICA8TGFuZGluZ1BhZ2UgLz5cbiAqICAgICAgIDwvTGF5b3V0PlxuICogICAgIDwvQXBwPixcbiAqICAgICBjb250YWluZXIsXG4gKiAgICk7XG4gKi9cbmNsYXNzIEFwcCBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNvbnRleHQ6IFByb3BUeXBlcy5zaGFwZShDb250ZXh0VHlwZSkuaXNSZXF1aXJlZCxcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLmVsZW1lbnQuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBzdGF0aWMgY2hpbGRDb250ZXh0VHlwZXMgPSBDb250ZXh0VHlwZTtcblxuICBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMuY29udGV4dDtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICAvLyBOT1RFOiBJZiB5b3UgbmVlZCB0byBhZGQgb3IgbW9kaWZ5IGhlYWRlciwgZm9vdGVyIGV0Yy4gb2YgdGhlIGFwcCxcbiAgICAvLyBwbGVhc2UgZG8gdGhhdCBpbnNpZGUgdGhlIExheW91dCBjb21wb25lbnQuXG4gICAgcmV0dXJuIFJlYWN0LkNoaWxkcmVuLm9ubHkodGhpcy5wcm9wcy5jaGlsZHJlbik7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQXBwO1xuIiwiLyogQGZsb3cgKi9cblxuaW1wb3J0IHR5cGUgeyBncmFwaHFsIGFzIGdyYXBocVR5cGUsIEdyYXBoUUxTY2hlbWEgfSBmcm9tICdncmFwaHFsJztcblxudHlwZSBGZXRjaCA9ICh1cmw6IHN0cmluZywgb3B0aW9uczogP2FueSkgPT4gUHJvbWlzZTxhbnk+O1xuXG50eXBlIE9wdGlvbnMgPSB7XG4gIGJhc2VVcmw6IHN0cmluZyxcbiAgY29va2llPzogc3RyaW5nLFxuICBzY2hlbWE/OiBHcmFwaFFMU2NoZW1hLFxuICBncmFwaHFsPzogZ3JhcGhxVHlwZSxcbn07XG5cbi8qKlxuICogQ3JlYXRlcyBhIHdyYXBwZXIgZnVuY3Rpb24gYXJvdW5kIHRoZSBIVE1MNSBGZXRjaCBBUEkgdGhhdCBwcm92aWRlc1xuICogZGVmYXVsdCBhcmd1bWVudHMgdG8gZmV0Y2goLi4uKSBhbmQgaXMgaW50ZW5kZWQgdG8gcmVkdWNlIHRoZSBhbW91bnRcbiAqIG9mIGJvaWxlcnBsYXRlIGNvZGUgaW4gdGhlIGFwcGxpY2F0aW9uLlxuICogaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZG9jcy9XZWIvQVBJL0ZldGNoX0FQSS9Vc2luZ19GZXRjaFxuICovXG5mdW5jdGlvbiBjcmVhdGVGZXRjaChcbiAgZmV0Y2g6IEZldGNoLFxuICB7IGJhc2VVcmwsIGNvb2tpZSwgc2NoZW1hLCBncmFwaHFsIH06IE9wdGlvbnMsXG4pIHtcbiAgLy8gTk9URTogVHdlYWsgdGhlIGRlZmF1bHQgb3B0aW9ucyB0byBzdWl0ZSB5b3VyIGFwcGxpY2F0aW9uIG5lZWRzXG4gIGNvbnN0IGRlZmF1bHRzID0ge1xuICAgIG1ldGhvZDogJ1BPU1QnLCAvLyBoYW5keSB3aXRoIEdyYXBoUUwgYmFja2VuZHNcbiAgICBtb2RlOiBiYXNlVXJsID8gJ2NvcnMnIDogJ3NhbWUtb3JpZ2luJyxcbiAgICBjcmVkZW50aWFsczogYmFzZVVybCA/ICdpbmNsdWRlJyA6ICdzYW1lLW9yaWdpbicsXG4gICAgaGVhZGVyczoge1xuICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgLi4uKGNvb2tpZSA/IHsgQ29va2llOiBjb29raWUgfSA6IG51bGwpLFxuICAgIH0sXG4gIH07XG5cbiAgcmV0dXJuIGFzeW5jICh1cmw6IHN0cmluZywgb3B0aW9uczogYW55KSA9PiB7XG4gICAgY29uc3QgaXNHcmFwaFFMID0gdXJsLnN0YXJ0c1dpdGgoJy9ncmFwaHFsJyk7XG4gICAgaWYgKHNjaGVtYSAmJiBncmFwaHFsICYmIGlzR3JhcGhRTCkge1xuICAgICAgLy8gV2UncmUgU1NSLCBzbyByb3V0ZSB0aGUgZ3JhcGhxbCBpbnRlcm5hbCB0byBhdm9pZCBsYXRlbmN5XG4gICAgICBjb25zdCBxdWVyeSA9IEpTT04ucGFyc2Uob3B0aW9ucy5ib2R5KTtcbiAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGdyYXBocWwoXG4gICAgICAgIHNjaGVtYSxcbiAgICAgICAgcXVlcnkucXVlcnksXG4gICAgICAgIHsgcmVxdWVzdDoge30gfSwgLy8gZmlsbCBpbiByZXF1ZXN0IHZhcnMgbmVlZGVkIGJ5IGdyYXBocWxcbiAgICAgICAgbnVsbCxcbiAgICAgICAgcXVlcnkudmFyaWFibGVzLFxuICAgICAgKTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoe1xuICAgICAgICBzdGF0dXM6IHJlc3VsdC5lcnJvcnMgPyA0MDAgOiAyMDAsXG4gICAgICAgIGpzb246ICgpID0+IFByb21pc2UucmVzb2x2ZShyZXN1bHQpLFxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGlzR3JhcGhRTCB8fCB1cmwuc3RhcnRzV2l0aCgnL2FwaScpXG4gICAgICA/IGZldGNoKGAke2Jhc2VVcmx9JHt1cmx9YCwge1xuICAgICAgICAgIC4uLmRlZmF1bHRzLFxuICAgICAgICAgIC4uLm9wdGlvbnMsXG4gICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgLi4uZGVmYXVsdHMuaGVhZGVycyxcbiAgICAgICAgICAgIC4uLihvcHRpb25zICYmIG9wdGlvbnMuaGVhZGVycyksXG4gICAgICAgICAgfSxcbiAgICAgICAgfSlcbiAgICAgIDogZmV0Y2godXJsLCBvcHRpb25zKTtcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlRmV0Y2g7XG4iLCJpbXBvcnQgY3JlYXRlQnJvd3Nlckhpc3RvcnkgZnJvbSAnaGlzdG9yeS9jcmVhdGVCcm93c2VySGlzdG9yeSc7XG5cbi8vIE5hdmlnYXRpb24gbWFuYWdlciwgZS5nLiBoaXN0b3J5LnB1c2goJy9ob21lJylcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tamFja3Nvbi9oaXN0b3J5XG5leHBvcnQgZGVmYXVsdCBwcm9jZXNzLmVudi5CUk9XU0VSICYmIGNyZWF0ZUJyb3dzZXJIaXN0b3J5KCk7XG4iLCJpbXBvcnQgVW5pdmVyc2FsUm91dGVyIGZyb20gJ3VuaXZlcnNhbC1yb3V0ZXInO1xuaW1wb3J0IHJvdXRlcyBmcm9tICcuL3JvdXRlcyc7XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBVbml2ZXJzYWxSb3V0ZXIocm91dGVzLCB7XG4gIHJlc29sdmVSb3V0ZShjb250ZXh0LCBwYXJhbXMpIHtcbiAgICBpZiAodHlwZW9mIGNvbnRleHQucm91dGUubG9hZCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmV0dXJuIGNvbnRleHQucm91dGVcbiAgICAgICAgLmxvYWQoKVxuICAgICAgICAudGhlbihhY3Rpb24gPT4gYWN0aW9uLmRlZmF1bHQoY29udGV4dCwgcGFyYW1zKSk7XG4gICAgfVxuICAgIGlmICh0eXBlb2YgY29udGV4dC5yb3V0ZS5hY3Rpb24gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHJldHVybiBjb250ZXh0LnJvdXRlLmFjdGlvbihjb250ZXh0LCBwYXJhbXMpO1xuICAgIH1cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9LFxufSk7XG4iLCJcbiAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0Vycm9yUGFnZS5jc3NcIik7XG4gICAgdmFyIGluc2VydENzcyA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2lzb21vcnBoaWMtc3R5bGUtbG9hZGVyL2xpYi9pbnNlcnRDc3MuanNcIik7XG5cbiAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgfVxuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q29udGVudCA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gY29udGVudDsgfTtcbiAgICBtb2R1bGUuZXhwb3J0cy5fZ2V0Q3NzID0gZnVuY3Rpb24oKSB7IHJldHVybiBjb250ZW50LnRvU3RyaW5nKCk7IH07XG4gICAgbW9kdWxlLmV4cG9ydHMuX2luc2VydENzcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHsgcmV0dXJuIGluc2VydENzcyhjb250ZW50LCBvcHRpb25zKSB9O1xuICAgIFxuICAgIC8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbiAgICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudFxuICAgIC8vIE9ubHkgYWN0aXZhdGVkIGluIGJyb3dzZXIgY29udGV4dFxuICAgIGlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCkge1xuICAgICAgdmFyIHJlbW92ZUNzcyA9IGZ1bmN0aW9uKCkge307XG4gICAgICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0yIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tNS1ydWxlcy0zIS4vRXJyb3JQYWdlLmNzc1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTUtcnVsZXMtMyEuL0Vycm9yUGFnZS5jc3NcIik7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlbW92ZUNzcyA9IGluc2VydENzcyhjb250ZW50LCB7IHJlcGxhY2U6IHRydWUgfSk7XG4gICAgICB9KTtcbiAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgcmVtb3ZlQ3NzKCk7IH0pO1xuICAgIH1cbiAgIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdpc29tb3JwaGljLXN0eWxlLWxvYWRlci9saWIvd2l0aFN0eWxlcyc7XG5pbXBvcnQgcyBmcm9tICcuL0Vycm9yUGFnZS5jc3MnO1xuXG5jbGFzcyBFcnJvclBhZ2UgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGVycm9yOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgbmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgICAgbWVzc2FnZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgICAgc3RhY2s6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICB9KSxcbiAgfTtcblxuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIGVycm9yOiBudWxsLFxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBpZiAoX19ERVZfXyAmJiB0aGlzLnByb3BzLmVycm9yKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxoMT57dGhpcy5wcm9wcy5lcnJvci5uYW1lfTwvaDE+XG4gICAgICAgICAgPHByZT57dGhpcy5wcm9wcy5lcnJvci5zdGFja308L3ByZT5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8aDE+RXJyb3I8L2gxPlxuICAgICAgICA8cD5Tb3JyeSwgYSBjcml0aWNhbCBlcnJvciBvY2N1cnJlZCBvbiB0aGlzIHBhZ2UuPC9wPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBFcnJvclBhZ2UgYXMgRXJyb3JQYWdlV2l0aG91dFN0eWxlIH07XG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHMpKEVycm9yUGFnZSk7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IEVycm9yUGFnZSBmcm9tICcuL0Vycm9yUGFnZSc7XG5cbmZ1bmN0aW9uIGFjdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJ0RlbW8gRXJyb3InLFxuICAgIGNvbXBvbmVudDogPEVycm9yUGFnZSAvPixcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYWN0aW9uO1xuIiwiLyogZXNsaW50LWRpc2FibGUgZ2xvYmFsLXJlcXVpcmUgKi9cblxuLy8gVGhlIHRvcC1sZXZlbCAocGFyZW50KSByb3V0ZVxuY29uc3Qgcm91dGVzID0ge1xuICBwYXRoOiAnJyxcblxuICAvLyBLZWVwIGluIG1pbmQsIHJvdXRlcyBhcmUgZXZhbHVhdGVkIGluIG9yZGVyXG4gIGNoaWxkcmVuOiBbXG4gICAge1xuICAgICAgcGF0aDogJycsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ0dldFJhbmtzJyAqLyAnLi9wcm9kdWN0cy9nZXQtcmFua3MnKSxcbiAgICB9LFxuICAgIC8vIHtcbiAgICAvLyAgIHBhdGg6ICcvaG9tZScsXG4gICAgLy8gICBsb2FkOiAoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ2hvbWUnICovICcuL2hvbWUnKSxcbiAgICAvLyB9LFxuICAgIC8vIHtcbiAgICAvLyAgIHBhdGg6ICcvY3VsdHVyZScsXG4gICAgLy8gICBsb2FkOiAoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ0N1bHR1cmUnICovICcuL2N1bHR1cmUnKSxcbiAgICAvLyB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvcmVxdWVzdC1kZW1vJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydChcbiAgICAgICAgICAvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnUmVxdWVzdERlbW8gKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL25ldy1yZXF1ZXN0ZGVtbydcbiAgICAgICAgKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvY29tcGFueS9jYXJlZXJzJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydChcbiAgICAgICAgICAvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnQ2FyZWVycycgKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL2NhcmVlcnMnXG4gICAgICAgICksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL2N1c3RvbWVycycsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoXG4gICAgICAgICAgLyogd2VicGFja0NodW5rTmFtZTogJ0N1c3RvbWVycycgKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL2N1c3RvbWVycydcbiAgICAgICAgKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvcHJpY2luZycsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoXG4gICAgICAgICAgLyogd2VicGFja0NodW5rTmFtZTogJ1ByaWNpbmcnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9wcmljaW5nJ1xuICAgICAgICApLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9wcmljaW5nL3ZpZXdEZXRhaWxzJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydChcbiAgICAgICAgICAvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnUHJpY2luZ0RldGFpbHMnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9QcmljaW5nVmlld0RldGFpbHMvJ1xuICAgICAgICApLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9jb21wYW55L2Fib3V0dXMnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KFxuICAgICAgICAgIC8qIHdlYnBhY2tDaHVua05hbWU6ICdBYm91dHVzJyAqLyAnLi9wcm9kdWN0cy9nZXQtcmFua3MvYWJvdXR1cydcbiAgICAgICAgKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvY29tcGFueS9jdWx0dXJlJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydChcbiAgICAgICAgICAvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnQ3VsdHVyZScgKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL2N1bHR1cmUnXG4gICAgICAgICksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL2NvbXBhbnkvcHJlc3MnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6ICdQcmVzcycgKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL3ByZXNzJyksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL2hvdy1lZ25pZnktd29ya3MnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KFxuICAgICAgICAgIC8qIHdlYnBhY2tDaHVua05hbWU6ICdIb3dXb3JrcycgKi8gJy4vcHJvZHVjdHMvZ2V0LXJhbmtzL0hvd1dvcmtzJ1xuICAgICAgICApLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9tb2R1bGVzL3Rlc3RzJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnVGVzdHMnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9UZXN0JyksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL21vZHVsZXMvbGl2ZWNsYXNzZXMnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KFxuICAgICAgICAgIC8qIHdlYnBhY2tDaHVua05hbWU6ICdMaXZlIENsYXNzZXMnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9UZWFjaCdcbiAgICAgICAgKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvbW9kdWxlcy9kb3VidHMnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6ICdEb3VidHMnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9Eb3VidHMnKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvbW9kdWxlcy9hc3NpZ25tZW50cycsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoXG4gICAgICAgICAgLyogd2VicGFja0NodW5rTmFtZTogJ0Fzc2lnbm1lbnRzJyAqLyAnLi9wcm9kdWN0cy9nZXQtcmFua3MvQXNzaWdubWVudHMnXG4gICAgICAgICksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL21vZHVsZXMvY29ubmVjdCcsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoXG4gICAgICAgICAgLyogd2VicGFja0NodW5rTmFtZTogJ0Nvbm5lY3QnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9Db25uZWN0J1xuICAgICAgICApLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy90ZWFtJyxcbiAgICAgIGxvYWQ6ICgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnVGVhbScgKi8gJy4vdGVhbScpLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9wcm9kdWN0cy9qZWV0JyxcbiAgICAgIGxvYWQ6ICgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnSmVldCcgKi8gJy4vcHJvZHVjdHMvamVldCcpLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9wcm9kdWN0cy9hY2FkcycsXG4gICAgICBsb2FkOiAoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ0FjYWRzJyAqLyAnLi9wcm9kdWN0cy9hY2FkcycpLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9wcm9kdWN0cy9wcmVwJyxcbiAgICAgIGxvYWQ6ICgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnT3RwJyAqLyAnLi9wcm9kdWN0cy9vdHAnKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvcHJvZHVjdHMvcW1zJyxcbiAgICAgIGxvYWQ6ICgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAncW1zJyAqLyAnLi9wcm9kdWN0cy9xbXMnKSxcbiAgICB9LFxuICAgIC8vIHtcbiAgICAvLyAgIHBhdGg6ICcvcmVxdWVzdC1kZW1vJyxcbiAgICAvLyAgIGxvYWQ6ICgpID0+XG4gICAgLy8gICAgIGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnUmVxdWVzdERlbW8nICovICcuL3JlcXVlc3QtZGVtbycpLFxuICAgIC8vIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9wcml2YWN5LWFuZC10ZXJtcycsXG4gICAgICBsb2FkOiAoKSA9PlxuICAgICAgICBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ1ByaXZhY3lBbmRUZXJtcycgKi8gJy4vcHJpdmFjeS1hbmQtdGVybXMnKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHBhdGg6ICcvdGVybXMtYW5kLWNvbmRpdGlvbnMnLFxuICAgICAgbG9hZDogKCkgPT5cbiAgICAgICAgaW1wb3J0KFxuICAgICAgICAgIC8qIHdlYnBhY2tDaHVua05hbWU6ICdUZXJtc0FuZENvbmRpdGlvbnMnICovICcuL3Byb2R1Y3RzL2dldC1yYW5rcy9wcml2YWN5QW5kVGVybXMnXG4gICAgICAgICksXG4gICAgfSxcbiAgICB7XG4gICAgICBwYXRoOiAnL2NhbmNlbGxhdGlvbi1hbmQtcmV0dXJuJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydChcbiAgICAgICAgICAvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnQ2FuY2VsbGF0aW9uQW5kUmV0dXJuJyAqLyAnLi9wcm9kdWN0cy9nZXQtcmFua3MvY2FuY2VsbGF0aW9uQW5kUmV0dXJuJ1xuICAgICAgICApLFxuICAgIH0sXG4gICAge1xuICAgICAgcGF0aDogJy9zY29yZScsXG4gICAgICBsb2FkOiAoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogJ1Njb3JlJyAqLyAnLi9zY29yZScpLFxuICAgIH0sXG4gICAgLy8gV2lsZGNhcmQgcm91dGVzLCBlLmcuIHsgcGF0aDogJyguKiknLCAuLi4gfSAobXVzdCBnbyBsYXN0KVxuICAgIHtcbiAgICAgIHBhdGg6ICcoLiopJyxcbiAgICAgIGxvYWQ6ICgpID0+XG4gICAgICAgIGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnTGF1bmNoaW5nU29vbicgKi8gJy4vbGF1bmNoaW5nLXNvb24nKSxcbiAgICB9LFxuICAgIC8vIHtcbiAgICAvLyAgIHBhdGg6ICcoLiopJyxcbiAgICAvLyAgIGxvYWQ6ICgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiAnbm90LWZvdW5kJyAqLyAnLi9ub3QtZm91bmQnKSxcbiAgICAvLyB9LFxuICBdLFxuXG4gIGFzeW5jIGFjdGlvbih7IG5leHQgfSkge1xuICAgIC8vIEV4ZWN1dGUgZWFjaCBjaGlsZCByb3V0ZSB1bnRpbCBvbmUgb2YgdGhlbSByZXR1cm4gdGhlIHJlc3VsdFxuICAgIGNvbnN0IHJvdXRlID0gYXdhaXQgbmV4dCgpO1xuICAgIC8vIFByb3ZpZGUgZGVmYXVsdCB2YWx1ZXMgZm9yIHRpdGxlLCBkZXNjcmlwdGlvbiBldGMuXG4gICAgcm91dGUudGl0bGUgPSBgJHtyb3V0ZS50aXRsZSB8fCAnVW50aXRsZWQgUGFnZSd9YDtcbiAgICByb3V0ZS5kZXNjcmlwdGlvbiA9IHJvdXRlLmRlc2NyaXB0aW9uIHx8ICcnO1xuXG4gICAgcmV0dXJuIHJvdXRlO1xuICB9LFxufTtcblxuLy8gVGhlIGVycm9yIHBhZ2UgaXMgYXZhaWxhYmxlIGJ5IHBlcm1hbmVudCB1cmwgZm9yIGRldmVsb3BtZW50IG1vZGVcbmlmIChfX0RFVl9fKSB7XG4gIHJvdXRlcy5jaGlsZHJlbi51bnNoaWZ0KHtcbiAgICBwYXRoOiAnL2Vycm9yJyxcbiAgICBhY3Rpb246IHJlcXVpcmUoJy4vZXJyb3InKS5kZWZhdWx0LFxuICB9KTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgcm91dGVzO1xuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUM1TkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFkQTtBQWlCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7O0FBMEdBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBN0dBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQVpBO0FBY0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQXBCQTtBQUFBO0FBQ0E7QUFEQTtBQW9CQTtBQUNBO0FBckJBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBMkJBO0FBNUJBO0FBQ0E7QUFEQTtBQWdDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBekZBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQWdHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7OztBQTRHQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkpBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVdBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQTs7Ozs7Ozs7Ozs7OztBQVFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQWhCQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBRkE7QUFDQTtBQUZBO0FBQ0E7QUFrQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxQ0E7Ozs7OztBQU1BO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBQUE7QUFQQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFKQTtBQUFBO0FBUUE7QUFBQTtBQUNBO0FBQ0E7QUFWQTtBQUtBO0FBTEE7QUFhQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQ0E7QUFiQTtBQUFBO0FBc0JBO0FBSEE7QUFDQTtBQXBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTZCQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbEVBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBOzs7Ozs7OztBQ0pBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBWEE7Ozs7Ozs7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQVlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7Ozs7QUE3QkE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBQ0E7QUFGQTtBQVVBO0FBREE7QUFDQTtBQXNCQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDdENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNWQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUEscVBBQ0E7QUFEQTtBQUZBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQSwyUUFFQTtBQUZBO0FBRkE7QUFRQTtBQUNBO0FBQUEsdU1BRUE7QUFGQTtBQUZBO0FBUUE7QUFDQTtBQUFBLHVPQUVBO0FBRkE7QUFGQTtBQVFBO0FBQ0E7QUFBQSwyUEFFQTtBQUZBO0FBRkE7QUFRQTtBQUNBO0FBQUEsZ09BRUE7QUFGQTtBQUZBO0FBUUE7QUFDQTtBQUFBLDJQQUVBO0FBRkE7QUFGQTtBQVFBO0FBQ0E7QUFBQSx1TUFFQTtBQUZBO0FBRkE7QUFRQTtBQUNBO0FBQUEscVBBQ0E7QUFEQTtBQUZBO0FBTUE7QUFDQTtBQUFBLG9PQUVBO0FBRkE7QUFGQTtBQVFBO0FBQ0E7QUFBQSxvUEFDQTtBQURBO0FBRkE7QUFNQTtBQUNBO0FBQUEseU9BRUE7QUFGQTtBQUZBO0FBUUE7QUFDQTtBQUFBLDhOQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFBQSw2T0FFQTtBQUZBO0FBRkE7QUFRQTtBQUNBO0FBQUEsaU9BRUE7QUFGQTtBQUZBO0FBUUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFLQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBLDhNQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFBQSxxT0FFQTtBQUZBO0FBRkE7QUFRQTtBQUNBO0FBQUEsaVBBRUE7QUFGQTtBQUZBO0FBUUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUFBLHVNQUNBO0FBREE7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQW5LQTtBQXNLQTtBQTFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMEtBO0FBMUtBO0FBQUE7QUFDQTtBQURBO0FBNEtBO0FBQ0E7QUFDQTtBQUNBO0FBL0tBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQXFMQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0EiLCJzb3VyY2VSb290IjoiIn0=